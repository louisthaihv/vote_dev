<?php

namespace docomo\Models\Codes;

class QuestionStatus
{
    public static $label = array(
        QUESTION_STATUS_DRAFT                => '下書き',
        QUESTION_STATUS_REJECT       => '下書き（差戻し）',
        QUESTION_STATUS_REQUEST_APPROVE      => '承認申請中',
        QUESTION_STATUS_WAITING       => '受付準備中',
        QUESTION_STATUS_READY => '受付中',
        QUESTION_STATUS_FINISH     => '受付終了',
    );
}
