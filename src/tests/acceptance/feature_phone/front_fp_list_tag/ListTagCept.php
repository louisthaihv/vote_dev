<?php
$I = new AcceptanceTester\CommonSteps($scenario);
$I->wantTo('check List Tag By Keyword');

$I->amOnPage('/');

$keyword = array(
                1=>array('keyword_name' => 'keyword_test_1' ,'is_visible' => '1'),
                2=>array('keyword_name' => 'keyword_test_2' ,'is_visible' => '1'),
            );
$question = array(
                1=>array(
                        'genre_parent_id'=>'1',
                        'genre_child_id'=>'1',
                        'status'=>'5',
                        'title'=>'title_1',
                        'vote_date_from'=>'2015-01-09 03:40:52',
                        'vote_date_to'=>'2015-01-31 00:00:00',
                        'baseline_date'=>'2015-01-09 03:40:52',
                        'viewable_type'=>'1',
                        'visible_device_i'=>'1',
                        'visible_device_d'=>'1',
                        'summary_type'=>'1',
                        'average_unit'=>'1',
                        'is_visible_other'=>'1',
                        'shuffle_choice'=>'1',
                        'navigation_title'=>'1',
                        'account_id'=>'1',
                        ),
                2=>array(
                        'genre_parent_id'=>'2',
                        'genre_child_id'=>'2',
                        'status'=>'6',
                        'title'=>'title_2',
                        'vote_date_from'=>'2015-01-09 03:40:52',
                        'vote_date_to'=>'2015-01-31 00:00:00',
                        'baseline_date'=>'2015-01-11 03:40:52',
                        'viewable_type'=>'1',
                        'visible_device_i'=>'1',
                        'visible_device_d'=>'1',
                        'summary_type'=>'1',
                        'average_unit'=>'1',
                        'is_visible_other'=>'1',
                        'shuffle_choice'=>'1',
                        'navigation_title'=>'1',
                        'account_id'=>'1',
                        ),

            );
$question_tag = array(
                    1=> array('question_id'=>1, 'keyword_id'=>1),
                    2=> array('question_id'=>2, 'keyword_id'=>2),
                );
$genre_parent = array(
                    1=>array(
                        'genre_parent_name'=>'genre_parent_1',
                        'parent_is_visible'=>1,
                        'genre_parent_directory'=>'dir_1',
                        'parent_sort_order'=>10,
                        ),
                    2=>array(
                        'genre_parent_name'=>'genre_parent_2',
                        'parent_is_visible'=>1,
                        'genre_parent_directory'=>'dir_2',
                        'parent_sort_order'=>20,
                        ),
                );
$genre_child = array(
                    1=>array(
                        'genre_parent_id'=>1,
                        'genre_child_name'=>'genre_child_name_1',
                        'child_is_visible'=>1,
                        'genre_child_directory'=>'child_dir_1',
                        'child_sort_order'=>10
                        ),
                    2=>array(
                        'genre_parent_id'=>2,
                        'genre_child_name'=>'genre_child_name_2',
                        'child_is_visible'=>1,
                        'genre_child_directory'=>'child_dir_2',
                        'child_sort_order'=>20
                        ),
                );
$answers = array(
                1=>array(
                        'question_id' => '1',
                        'user_id' => '1',
                        'choice_id' => '1',
                        'age' => '25',
                        'gender' => '1',
                        'local' => '1',
                        'answer_date' => '2015-01-05 00:00:00',
                        ),
                2=>array(
                        'question_id' => '2',
                        'user_id' => '1',
                        'choice_id' => '1',
                        'age' => '25',
                        'gender' => '1',
                        'local' => '1',
                        'answer_date' => '2015-01-05 00:00:00',
                        ),

                );
$I->createDbAnswers($answers);
$I->createDbGenreParent($genre_parent);
$I->createDbGenreChild($genre_child);
$I->createDbQuestionTags($question_tag);
$I->createDbKeyword($keyword);
$I->createDbQuestion($question);
$I->checkListTagKeyword($keyword);
$I->checkListTagQuestion($question);
