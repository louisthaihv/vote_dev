@section('head')
<link rel="stylesheet" href="{{ url('common/admin/css/jquery-ui-1.8.23.custom.css') }}" type="text/css" />
<script type="text/javascript" src=" {{ url('common/admin/js/jquery-ui-1.8.23.custom.min.js') }}"></script>
<script type="text/javascript" src=" {{ url('common/admin/js/jquery.ui.datepicker-ja.js') }}"></script>
@stop

@section('script')
<script type="text/javascript">
$(window).load(function(){

});
</script>
@stop

@include('admin.share.errors', ['errorList' => $errors])
{{ Form::model($upload, ['url' => '/manage/upload/store', 'method' => 'post', 'files' => true]) }}
    <table class="formTable">
        <tr>
            <th>アップロード画像<span class="require">（必須）</span></th>
            <td>@include('layouts.elements.displays.file_uploader', ['uploader_id' => 'upload_file', 'db_file' => $upload->file_path])</td>
        </tr>
    </table>
    <div class="tcenter m-margint">
        <button type="submit" name="commit" class="commitBtn">送信</button>
    </div>
{{ Form::close() }}