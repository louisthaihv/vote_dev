<?php

namespace docomo\Models\Codes;

class SummaryType
{
    public static $label = array(
        SUMMARY_NORMAL  => '通常',
        SUMMARY_AVERAGE => '平均値集計'
    );
}
