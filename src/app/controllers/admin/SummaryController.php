<?php

namespace Admin;

use docomo\Models\Attributes\Attribute;
use docomo\Models\Attributes\AttributeValue;
use docomo\Models\Codes\QuestionStatus;
use docomo\Models\Codes\QuestionViewable;
use docomo\Models\Forms\SummaryConditionForm;
use docomo\Models\Genres\ChildGenre;
use docomo\Models\Genres\ParentGenre;
use docomo\Models\Summary\DailyVote;
use docomo\Models\Summary\DailyVoteRanking;

\View::composer([
    'admin.summary.vote.ranking'
], function($view) {
    $view->with('genres', ParentGenre::getListForDDLWithChildren());
});
class SummaryController extends \BaseController
{
    public function chmodLog()
    {
        $date = new \DateTime();
        echo app_path() . '/storage/logs/laravel-' . $date->format('Y-m-d') . '.log';
        echo chmod(app_path() . '/storage/logs/laravel-' . $date->format('Y-m-d') . '.log', 0666);
    }

    public function dailyVote()
    {
        $conditions = SummaryConditionForm::fill();
        $summaryData = $conditions->getDailyVote();
        $head = new DailyVote();
        if (isset($summaryData)) {
            $head->total = 0;
            $head->fp = 0;
            $head->sp = 0;
            foreach ($summaryData as &$record) {
                $head->total += $record->total;
                $head->fp += $record->fp;
                $head->sp += $record->sp;
            }
        }

        if (\Input::get('mode') === 'download') {
            return $this->createDownloadFile([
                'summary_date' => '日',
                'total' => '得票数',
                'fp' => 'i版',
                'sp' => 'd版'
            ],
                $summaryData,
                'daily_vote_' . $conditions->date_from . '_' . $conditions->date_to,
                '日別得票数'
            );
        }
        return \View::make('admin.summary.daily.vote', compact('conditions', 'summaryData', 'head'));
    }

    public function voteRanking()
    {
        $conditions = SummaryConditionForm::fill();
        $summaryData = $conditions->getVoteRanking();

        if (\Input::get('mode') === 'download') {
            return $this->createDownloadFile([
                'id' => 'ID',
                'title' => 'タイトル',
                'getChoiceCount()' => '選択肢数',
                'getChildGenreName()' => '小ジャンル名',
                'vote_date_from' => '受付開始',
                'vote_date_to' => '受付終了',
                'status' => 'ステータス',
                'vote_count' => '票数',
                (\Auth::user()->havePermission('is_visible_register')) ? 'getAccountName()' : false => '登録者',
                'viewable_type' => '表示'
            ],
                $summaryData,
                'vote_ranking_' . $conditions->date_from . '_' . $conditions->date_to,
                '得票数ランキング'
            );
        }
        return \View::make('admin.summary.vote.ranking', compact('conditions', 'summaryData'));
    }
    public function voteGenre()
    {
        $conditions = SummaryConditionForm::fill();
        $data = $conditions->getVoteGenre();
        $parents = ParentGenre::getDataByCache();
        $children = ChildGenre::getDataByCache();
        $summaryData = [];

        foreach ($data as &$record) {
            if (!array_key_exists($record->genre_parent_id, $summaryData)) {
                $summaryData[$record->genre_parent_id] = [];
            }
            $summaryData[$record->genre_parent_id][$record->genre_child_id] = $record->total;
            unset($record);
        }
        unset($data);

        if (\Input::get('mode') === 'download') {
            $data = [];
            foreach ($parents as &$parent) {
                foreach ($children[$parent->id] as &$record) {
                    $record->genre_parent_name = $parent->genre_parent_name;
                    if (array_key_exists($parent->id, $summaryData) &&
                        array_key_exists($record->id, $summaryData[$parent->id])) {
                        $record->total = $summaryData[$parent->id][$record->id];
                    } else {
                        $record->total = 0;
                    }
                    $data[] = $record;
                }
            }

            return $this->createDownloadFile([
                'genre_parent_name' => '大ジャンル名',
                'genre_child_name' => '小ジャンル名',
                'total' => '票数'
            ],
                $data,
                'vote_genre_' . $conditions->date_from . '_' . $conditions->date_to,
                'ジャンル別得票数'
            );
        }
        return \View::make('admin.summary.vote.genre', compact('conditions', 'summaryData', 'parents', 'children'));
    }

    public function voteProfile()
    {
        $conditions = SummaryConditionForm::fill();
        $data = $conditions->getVoteAttribute();
        $attributes = Attribute::getDataByCache();
        $attributeValues = AttributeValue::getDataByCache();
        $summaryData = [];

        foreach ($data as &$record) {
            if (!array_key_exists($record->attribute_id, $summaryData)) {
                $summaryData[$record->attribute_id] = [];
            }
            $summaryData[$record->attribute_id][$record->attribute_value_id] = $record->total;
            unset($record);
        }
        unset($data);

        if (\Input::get('mode') === 'download') {
            $data = [];
            foreach ($attributes as $attribute) {
                foreach ($attributeValues[$attribute->id] as &$value) {
                    $value->attribute_name = $attribute->attribute_name;
                    if (array_key_exists($attribute->id, $summaryData) &&
                        array_key_exists($value->id, $summaryData[$attribute->id])) {
                        $value->total = $summaryData[$attribute->id][$value->id];
                    } else {
                        $value->total = 0;
                    }
                    $data[] = $value;
                }
            }
            return $this->createDownloadFile([
                'attribute_name' => 'プロフィール項目名',
                'attribute_value_name' => 'プロフィール設定値',
                'total' => '票数'
            ],
                $data,
                'vote_profile_' . $conditions->date_from . '_' . $conditions->date_to,
                'プロフィール別得票数'
            );
        }
        return \View::make('admin.summary.vote.profile', compact('conditions', 'summaryData', 'attributes', 'attributeValues'));
    }

    public function dailyProfile()
    {
        $conditions = SummaryConditionForm::fill();
        $summaryData = $conditions->getDailyProfile();
        $head = new DailyVote();
        if (isset($summaryData)) {
            $head->total = 0;
            $head->fp = 0;
            $head->sp = 0;
            foreach ($summaryData as &$record) {
                $head->total += $record->new_fp + $record->new_sp;
                $head->fp += $record->new_fp;
                $head->sp += $record->new_sp;
            }
        }

        if (\Input::get('mode') === 'download') {
            foreach ($summaryData as &$record) {
                $record->total = $record->total_fp + $record->total_sp;
                $record->new = $record->new_fp + $record->new_sp;
            }
            return $this->createDownloadFile([
                'summary_date' => '日',
                'total' => '総登録数(合計)',
                'total_fp' => '総登録数(i版)',
                'total_sp' => '総登録数(d版)',
                'new' => '新規登録数(合計)',
                'new_fp' => '新規登録数(i版)',
                'new_sp' => '新規登録数(d版)'
            ],
                $summaryData,
                'daily_profile_' . $conditions->date_from . '_' . $conditions->date_to,
                '日別プロフィール登録'
            );
        }

        return \View::make('admin.summary.daily.profile', compact('conditions', 'summaryData', 'head'));
    }

    public function profile()
    {
        $summaryData = SummaryConditionForm::getProfile();

        $attributes = Attribute::getDataByCache()->sortBy('sort_order');
        $attributeValues = AttributeValue::getDataByCache();

        return \View::make('admin.summary.profile', compact('summaryData', 'attributes', 'attributeValues'));
    }

    private function createDownloadFile($header, $data, $fileName, $firstLine)
    {
        $headerKeys = [];
        $headerTexts = [];
        foreach ($header as $key => &$text) {
            if ($key === false) {
                continue;
            }
            $headerKeys[] = $key;
            $headerTexts[] = $text;
        }
        unset($header);
        $csv = [$headerTexts];
        unset($headerTexts);
        foreach ($data as &$rec) {
            $csvRec = [];
            foreach ($headerKeys as $key) {
                if (strstr($key, 'date') !== false) {
                    $csvRec[] = date('Y-m-d H:i', strtotime($rec->$key));
                } else if ($key === 'status') {
                    $csvRec[] = QuestionStatus::$label[$rec->$key];
                } else if ($key === 'viewable_type') {
                    $csvRec[] = QuestionViewable::$label[$rec->$key];
                } else if (strstr($key, '()') === false) {
                    $csvRec[] = $rec->$key;
                } else {
                    eval('$csvRec[] = $rec->' . $key . ';');
                }
            }
            unset($forI);
            $csv[] = $csvRec;
            unset($csvRec);
            unset($rec);
        }

        $stream = tmpfile();

        fputcsv($stream, [$firstLine]);
        foreach($csv as &$row){
            fputcsv($stream, $row);
            unset($row);
        }

        /*
         * Convert encoding
         */
        rewind($stream);
        $csv = str_replace(PHP_EOL, "\r\n", stream_get_contents($stream));
        $csvConv = mb_convert_encoding($csv, 'SJIS-win', 'UTF-8');

        $fileName .= ".csv";

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="' . $fileName . '"',
        ];

        fclose($stream);
        return \Response::make($csvConv, 200, $headers);
    }
}
