    <p>基本情報</p>
    <table class="formTable">
        <tr>
            <th>タイトル(最大60文字)<span class="require">（必須）</span></th>
            <td>{{ Form::text('title', $question->title, ['class' => 'xx-large']) }}</td>
        </tr>
        <tr>
            <th>説明(HTMLタグ可)<span class="require">（必須）</span></th>
            <td>{{ Form::textarea('details', $question->details, ['rows' => 5, 'class' => 'xx-large', 'id' => 'details']) }}</td>
        </tr>
        <tr>
            <th>概要(最大200文字)<span class="require">（必須）</span><br />（description用）</th>
            <td>{{ Form::text('description', $question->description, ['class' => 'xx-large', 'id' => 'description']) }}<br />
                <button type="button" id="copyDetails" class="naviBtn">説明をコピー</button></td>
        </tr>
        <tr>
            <th>タイプ<span class="require">（必須）</span></th>
            <td>
            @foreach ($summary_type as $key => $value)
                <label>{{ Form::radio('summary_type', $key, ($question->summary_type == $key)) }}&nbsp;{{ $value }}</label>
            @endforeach
                <span class="unit">単位：{{ Form::text('average_unit', $question->average_unit, ['class' => 'xx-small']) }}</span>
            </td>
        </tr>
        <tr>
            <th>選択肢<span class="require">（必須）</span></th>
            <td>
                <table class="choices" id="choiceList">
                    <tr>
                        <th>&nbsp;</th>
                        <th width="480">選択肢</th>
                        <th class="summaryValue">集計用数値</th>
                        <th>&nbsp;</th>
                    </tr>
            @if (count($choices) > 0)
                @for ($choice_i = 0; $choice_i < count($choices); $choice_i++)
                    <tr>
                        <td>{{ $choice_i + 1 }}</td>
                        <td><input type="text" name="choice_text[{{ $choices[$choice_i]->choice_num }}]" value="{{{ $choices[$choice_i]->choice_text }}}" class="x-large" /></td>
                        <td class="summaryValue"><input type="text" name="summary_value[{{ $choices[$choice_i]->choice_num }}]" value="{{{ $choices[$choice_i]->summary_value }}}" class="x-small" /></td>
                        <td><a href="#" class="naviBtn choiceRemove">削除</a></td>
                    </tr>
                @endfor
            @endif
                </table>
                <div id="choiceListAdd" {{ (count($choices) > 0) ? '' : 'style="display:none;"' }}>
                    <p ><a href="#" class="naviBtn" id="choiceAdd">1つ追加</a>　<a href="#" class="naviBtn" id="choiceListAllOpen">一括作成BOXを開く</a></p>
                </div>
                <div id="choiceListAll" {{ (count($choices) > 0) ? 'style="display:none;"' : '' }}>
                    <textarea rows="10" name="choiceTextarea" class="x-large"></textarea>
                    <p><a href="#" class="naviBtn" id="choiceListAllCreate">リストを一括作成する</a></p>
                </div>
            </td>
        </tr>
        <tr>
            <th>その他を表示するか</th>
            <td>
                <label>{{ Form::checkbox('is_visible_other', null, $question->is_visible_other, ['class' => 'taniDisabled']) }} その他を表示</label>
                {{ Form::text('other_text', $question->other_text, ['class' => 'large taniDisabled']) }}
            </td>
        </tr>
        <tr>
            <th>受付中の選択肢をシャッフルするか</th>
            <td>
                <label>{{ Form::checkbox('shuffle_choice', null, $question->shuffle_choice, ['class' => 'taniDisabled']) }} シャッフル</label>
            </td>
        </tr>
        <tr>
            <th>属性を限定して回答させる場合は選択</th>
            <td>
                {{ Form::selectByTableInGroup('attribute_value_id', $attributes, 'id', 'attribute_value_name', $question->attribute, false, [UNLIMITED => '限定しない']) }}
            </td>
        </tr>
    </table>
