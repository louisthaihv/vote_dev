<div class="resultBox" id="attributeTab_{{ $attribute->id }}" style="display:none;">
    @if (isset($csv) && $csv)
<p class="l-margin"><a href="csv/{{ $attribute->id }}" target="_blank">{{ $attribute->attribute_name }}CSVダウンロード</a> | <a href="csv/{{ $attribute->id . '/' . TYPE_D_MENU }}" target="_blank">{{ $attribute->attribute_name }}CSVダウンロード(d版)</a> | <a href="csv/{{ $attribute->id . '/' . TYPE_I_MODE }}" target="_blank">{{ $attribute->attribute_name }}CSVダウンロード(i版)</a></p>
    @endif
    @if ($attribute_id === ATTR_LOCAL)
    <ul class="prefOpen m-margin">
        <li>
            <p class="btn_all"><span>全国のランキング</span></p>
            <div class="profileChoice lrMargin m-margint">
                <table>
                    @include('front.result._result_list', ['list' => $topSummary[$attribute_id], 'default_selected' => $selected[$attribute_id], 'attribute_directory' => $attribute_dir])
                </table>
            </div>
        </li>
    </ul>
    @else
        <div class="profileChoice m-margin lrMargin">
            <table>
                @include('front.result._result_list', ['list' => $topSummary[$attribute_id], 'default_selected' => $selected[$attribute_id], 'attribute_directory' => $attribute_dir])
            </table>
        </div>
    @endif
    <p class="rankingTit"><span class="rankingTitle">{{ $selected[$attribute_id]->attribute_value_name }}</span>ランキング</p>
    @if ($question->isViewableAverage())
        <p class="avgData">平均：{{ number_format(intval($topSummary[$attribute_id][$selected[$attribute_id]->id]->average_num)) . $question->average_unit }}</p>
    @endif
        <p class="attrCount">投票数：<span class="boldFont rankingCount">{{ number_format($topSummary[$attribute_id][$selected[$attribute_id]->id]->total_vote) }}</span>票</p>
    <div class="resultTable" style="display:block;">
        @include('front.result._result_table', ['resultData' => $result_data[$attribute_id][$selected[$attribute_id]->id]])
    </div><!--/resultTable-->

    @if ($attribute_id === ATTR_LOCAL)
    <p class="mapTit m-margint">全国1位分布</p>
    <div class="japanmap lrMargin m-margint">
        <p class="japanmapNote">※色は総合順位に対応しています</p>
        @include('share._prefectures', ['areaRanking' => $topSummary[ATTR_LOCAL]])
    </div><!--/japanmap-->
    @endif

</div>