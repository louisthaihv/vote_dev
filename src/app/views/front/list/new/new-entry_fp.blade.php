@extends('layouts.front_fp')

@section('content')

    @include('layouts.front_elements._space')
    <div>{{ HTML::image('common/front/images/fp/tit7.gif', '新着一覧', array('width' => '100%')) }}</div>
    @include('layouts.front_elements._space')

    {{--Entry--}}
    @if((Route::currentRouteName() == 'list.new.entry'))
        @include('front.list.new._shared.entry_fp')
    @endif
    {{--Result--}}
    @if((Route::currentRouteName() == 'list.new.result'))
        @include('front.list.new._shared.result_fp')
    @endif

    @include('layouts.front_elements._gotop_fp')

@endsection


