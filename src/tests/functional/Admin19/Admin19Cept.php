<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('Edit keyword');

$I->amOnRoute('admin.keyword.edit');
$I->amOnAction('Admin\SurveyController@create');
