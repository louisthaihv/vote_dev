<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNavigation extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('info_for_customers');
		Schema::dropIfExists('navigations');
		Schema::create('navigations', function (Blueprint $table) {
			$table->integer('question_id');
			$table->integer('view_type');
			$table->integer('row');
			$table->string('thumbnail', 200);
			$table->text('description')->nullable();
			$table->string('url', 200)->nullable();
			$table->timestamps();
			$table->index(['question_id', 'view_type', 'row']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('navigations');
		Schema::create('info_for_customers', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('question_id');
			$table->integer('visible_type');
			$table->integer('serial_no');
			$table->string('thumbnail', 200);
			$table->text('description')->nullable();
			$table->string('url', 200)->nullable();
			$table->timestamps();
		});
	}

}
