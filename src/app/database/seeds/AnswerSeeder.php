<?php
class AnswerSeeder extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('answers')->truncate();
        
        
        for($i = 1; $i <= 30; $i++) {
            $answers[] = 
                [
                    'id'                    => $i,
                    'question_id'           => $i,
                    'user_id'               => 1,
                    'choice_id'             => $i,
                    'age'                   => $i,
                    'gender'                => $i,
                    'local'                 => $i,
                    'answer_date'           => Date('Y-m-d h:i:s')         
                ];
            
        }
        DB::table("answers")->insert($answers);
    }
}

