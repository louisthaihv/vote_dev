@extends('layouts.front')

@section('metaTag')
    <title>プロフィール設定 | みんなの声</title>
    <meta name="description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！">
    <meta property="og:title" content="プロフィール設定 | みんなの声" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
    <meta name="twitter:title" content="プロフィール設定 | みんなの声" />
    <meta name="twitter:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
@stop

@section('content')

    <section class="l-margin">
        {{ Form::open(array('route' => 'profile.update')) }}

        <h2 class="profileTit"><span>プロフィール設定</span></h2>
        @if (isset($user->result) && !$user->result)
            <p class="profileErrorMsg">生年月日が正しくありません。</p>
        @endif
        <dl class="profileDL m-margin">
            @foreach ($attributes as $attribute)
            <dt>{{{ $attribute->attribute_label }}}</dt>
            <dd>
                @if ($attribute->id == ATTR_BIRTHDAY)
                    {{ Form::birthday((isset($user) ? $user->birthday : null)) }}
                @else
                    {{ Form::attributeValueList($attribute->id, array('default' => $user->attrList[$attribute->id], 'select_none' => ' '), $attribute->attribute_directory) }}
                @endif
            </dd>
            @endforeach
        </dl>
        <p class="tcenter">
            <button type="submit" class="btnType03" name="save_btn">登録する</button>
        </p>
        {{ Form::close() }}

    </section>
    <section class="profileInfo">
        <p style="color:#f39700;margin-bottom:10px;">プロフィール設定でアンケートがもっと楽しくなる！</p>
        <p>性別、生年月日、出身地を設定すると・・・</p>
        <p>※アンケートの「年代別」「性別」「地域別」結果にあなたの1票が反映されます。</p>
        <p>※あなたのプロフィールに合った限定アンケートに投票したり、結果を見ることができます。</p>

    </section>
@endsection
