<?php
namespace docomo\Models\Forms;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;

class FileUploadForm
{

    public $file_path = '';

    public $errors = null;

    public function __construct()
    {
    }

    public function saveImageFile($controlName, $savePath) {

        $result = false;

        if (!\Input::hasFile($controlName)) {
            // exit if upload_file is empty.
            return $result;
        }

        $uploadFile = Input::file($controlName);
        $extension = mb_strtolower($uploadFile->getClientOriginalExtension());
        if (in_array($extension,
            [
                'jpg',
                'jpeg',
                'gif',
                'png'
            ])) {
            $this->file_path = $savePath .  $uploadFile->getClientOriginalName();

            $uploadFile->move(public_path($savePath),  $uploadFile->getClientOriginalName());

            $result = true;
        } else {
            $this->errors = new MessageBag();
            $this->errors->add('error', 'アップロードできる画像の種類は、jpg/gif/pngのみです。');
        }

        unset($uploadFile);
        unset($extension);

        return $result;
    }

}
