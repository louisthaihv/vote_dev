<?php
$I = new AcceptanceTester\FrontChildGenreEntrySteps($scenario);
$I->wantTo('test');

$items = array(
				array('title' => 'quest_1', 'date' => '2014-12-25 14:28:14', 'status' => '1', 'idate' => ''),
				array('title' => 'quest_2', 'date' => '2014-12-26 14:28:14', 'status' => '1', 'idate' => ''),
				array('title' => 'quest_3', 'date' => '2014-12-24 14:28:14', 'status' => '1', 'idate' => ''),
				array('title' => 'quest_4', 'date' => '2014-12-20 14:28:14', 'status' => '1', 'idate' => ''),
				array('title' => 'quest_5', 'date' => '2014-12-24 14:28:14', 'status' => '1', 'idate' => ''),
				);

foreach ($items as $key => $value) {
	$items[$key]['idate'] = strtotime($value['date']);
	$items[$key]['date_vlue'] = $I->dateToJpdate($value['date']);
}

// usort($items, function($a, $b) {
//                 if($a['idate']==$b['idate'])
//                     return 0;
//                 return $a['idate'] < $b['idate']?1:-1;
//         });

$items = $I->sortItems($items, 'idate');
$items = $I->sortItems($items, 'idate', 'asc');
$items = $I->sortItems($items, 'idate');
$I->wait(3);