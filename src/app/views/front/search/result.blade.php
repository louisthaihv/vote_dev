@extends('layouts.front_in')

@section('metaTag')
    <title>{{{ $params['word']  }}} | みんなの声</title>
    <meta name="description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！">
    <meta property="og:title" content="{{{ $params['word'] }}} | みんなの声" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
    <meta name="twitter:title" content="{{{ $params['word'] }}} | みんなの声" />
    <meta name="twitter:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
@stop

@section('script')
    @include('front._shared.js_load_more_pages', ['classNames'=>['searchResult'], 'nums' => [NUM_FIRST_PAGE_10], 'pages' => [PAGER_SIZE_FRONT], 'params' => [['id' =>$keywords->lists('id')]]])
@endsection

@section('content')

   <section class="l-margin">
        <div class="l-margin">
            <div class="search">
                    <ul>
                    <li class="search-key"><input type="text" name="word" id="word" value="{{{ $params['word'] }}}" placeholder="キーワード"></li>
                    <li class="search-bt">
                        <a href="" onclick="return checkWord()">
                            <input class="submit" type="submit" name="" value="検索">
                        </a>
                    </li>
                    </ul>
            </div><!--search-->
            <div><span id="error"></span></div>
            <h3 class="listSectionTit"><span>ヒットした検索キーワード</span></h3>
            <div class="keywords">
                <ul class="words">
                    @foreach ($keywords as $keyword)
                        <li><a href="{{ route('list.tag.result',$keyword->id) }}">{{ $keyword->keyword_name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="l-margin">
            <h3 class="listSectionTit"><span>検索結果アンケート({{ $count_question->getTotal() }}件)</span></h3>
            @include('front._shared._question_list', ['className' => 'searchResult', 'displaySeeMore' => TRUE])
        </div>
    </section>
   @include('layouts.front_elements._profileBn')
    <script type="text/javascript">
        function checkWord() {
            var word = $("#word").val();
            if(word == '') {
                $("#error").text('{{ W_FRM_REQUIRED_KEYWORD }}');

                return false;
            } else {
                location.href="{{ url('search/tag/') }}/"+word;
                
                return false;
            }
        }
    </script>
@endsection