<?php

namespace docomo\Models\Questions;

use docomo\Models\Accounts\Account;
use docomo\Models\Answers\Answer;
use docomo\Models\Answers\ResultVote;
use docomo\Models\Attributes\Attribute;
use docomo\Models\Attributes\AttributeValue;
use docomo\Models\Codes\QuestionStatus;
use docomo\Models\Codes\QuestionViewable;
use docomo\Models\Genres\ChildGenre;
use docomo\Models\Genres\ParentGenre;
use docomo\Models\Keywords\Keyword;
use docomo\Models\Users\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Question extends \BaseArdentModel
{

    protected $table = 'questions';

    private $deviceColumn = [
        TYPE_I_MODE => 'visible_device_i',
        TYPE_D_MENU => 'visible_device_d',
        TYPE_API => 'visible_api'
    ];

    /*
     * Acceptable params from input
     */
    protected $fillable = ['*'];
    public static $fillable_array = [
        MODE_ALL_EDIT => [
            'genre_parent_id',
            'genre_child_id',
            'title',
            'details',
            'description',
            'vote_date_from',
            'vote_date_to',
            'viewable_type',
            'visible_device_i',
            'visible_device_d',
            'visible_api',
            'summary_type',
            'average_unit',
            'is_visible_other',
            'other_text',
            'shuffle_choice',
            'attribute_id',
            'attribute_value_id',
            'result_comment',
            'navigation_title',
            'sns_comment',
            'comment',
            'back_button_text',
            'back_button_url'
        ],
        MODE_NORMAL => [
            'viewable_type',
            'visible_device_i',
            'visible_device_d',
            'visible_api',
            'shuffle_choice',
            'result_comment',
            'navigation_title',
            'sns_comment',
            'comment',
            'back_button_text',
            'back_button_url'
        ]];

    /*
     * Validation rules
     */
//    public static $rules = [];

    public static $rules = [
        'title' => ['sometimes', 'required', 'max:60', 'deny_tag', 'deny_char'],
        'details' => ['sometimes', 'required', 'max:10000', 'allow_tag', 'deny_char'],
        'description' => ['sometimes', 'required', 'max:200', 'deny_tag', 'deny_char'],
        'visible_api' => ['sometimes', 'deny_limited'],
        'summary_type' => ['sometimes', 'required'],
        'vote_date_from' => ['sometimes', 'required', 'date', 'datetime_format:Y-m-d H:i'],
        'vote_date_to' => ['sometimes', 'required', 'date', 'datetime_format:Y-m-d H:i', 'after:"vote_date_from"'],
        'average_unit' => ['required', 'deny_char'],
        'other_text' => ['sometimes', 'max:60', 'deny_tag', 'deny_char'],
        'genre_child_id' => ['sometimes', 'required'],
        'result_comment' => ['sometimes', 'allow_tag', 'max:10000', 'deny_char'],
        'navigation_title' => ['sometimes', 'max:60', 'deny_tag', 'deny_char'],
        'back_button_text' => ['sometimes', 'max:60', 'deny_tag', 'deny_char', 'required_with:back_button_url'],
        'back_button_url' => ['sometimes', 'url', 'max:255', 'required_with:back_button_text'],
        'sns_comment' => ['sometimes', 'max:60', 'deny_tag', 'deny_char'],
        'comment' => ['sometimes', 'deny_char', 'max:10000']
    ];

    /*
     * Custom validation messages
     */
    public static $customMessages = [
        'title.required' => W_FRM_REQUIRED_TITLE,
        'title.max' => W_FRM_MAX_TITLE,
        'title.deny_tag' => W_FRM_DENY_TAG_TITLE,
        'title.deny_char' => W_FRM_DENY_CHAR_TITLE,
        'details.required' => W_FRM_REQUIRED_QUESTION_DETAILS,
        'details.max' => W_FRM_MAX_QUESTION_DETAILS,
        'details.deny_char' => W_FRM_DENY_CHAR_QUESTION_DETAILS,
        'details.allow_tag' => W_FRM_ALLOW_TAG_QUESTION_DESCRIPTION,
        'description.required' => W_FRM_REQUIRED_DESCRIPTION,
        'description.max' => W_FRM_MAX_QUESTION_DESCRIPTION,
        'description.deny_tag' => W_FRM_DENY_TAG_QUESTION_DESCRIPTION,
        'description.deny_char' => W_FRM_DENY_CHAR_QUESTION_DESCRIPTION,
        'visible_api.deny_limited' => 'APIで表示する場合、属性限定を設定できません。',
        'summary_type.required' => W_FRM_REQUIRED_TYPE,
        'vote_date_from.required' => W_FRM_REQUIRED_DATE_FROM,
        'vote_date_from.date' => W_FRM_FORMAT_DATE_FROM,
        'vote_date_from.datetime_format' => W_FRM_LENGTH_DATE_FROM,
        'vote_date_to.required' => W_FRM_REQUIRED_DATE_TO,
        'vote_date_to.date' => W_FRM_FORMAT_DATE_TO,
        'vote_date_to.after' => W_FRM_COMPLEX_DATE_TO_AFTER_FROM,
        'vote_date_to.datetime_format' => W_FRM_LENGTH_DATE_TO,
        'average_unit.required' => W_FRM_REQUIRED_AVERAGE_UNIT,
        'average_unit.deny_char' => W_FRM_DENY_CHAR_AVERAGE_UNIT,
        'other_text.max' => W_FRM_MAX_OTHER_TEXT,
        'other_text.deny_tag' => W_FRM_DENY_TAG_OTHER_TEXT,
        'other_text.deny_char' => W_FRM_DENY_CHAR_OTHER_TEXT,
        'genre_child_id.required' => W_FRM_REQUIRED_GENRE,
        'result_comment.allow_tag' => W_FRM_ACCEPT_TAGS,
        'result_comment.max' => W_FRM_MAX_RESULT_COMMENT,
        'result_comment.deny_char' => W_FRM_DENY_CHAR_RESULT_COMMENT,
        'navigation_title.max' => W_FRM_MAX_NAVIGATION_TITLE,
        'navigation_title.deny_tag' => W_FRM_DENY_TAG_NAVIGATION_TITLE,
        'navigation_title.deny_char' => W_FRM_DENY_CHAR_NAVIGATION_TITLE,
        'back_button_text.max' => W_FRM_MAX_BACK_BUTTON_TEXT,
        'back_button_text.deny_tag' => W_FRM_DENY_TAG_BACK_BUTTON_TEXT,
        'back_button_text.deny_char' => W_FRM_DENY_CHAR_BACK_BUTTON_TEXT,
        'back_button_text.required_with' => W_FRM_REQUIRED_BACK_BUTTON,
        'back_button_url.url' => W_FRM_FORMAT_BACK_BUTTON_URL,
        'back_button_url.max' => W_FRM_MAX_BACK_BUTTON_URL,
        'back_button_url.required_with' => W_FRM_REQUIRED_BACK_BUTTON,
        'sns_comment.max' => W_FRM_MAX_SNS_COMMENT,
        'sns_comment.deny_tag' => W_FRM_DENY_TAG_SNS_COMMENT,
        'sns_comment.deny_char' => W_FRM_DENY_CHAR_SNS_COMMENT,
        'comment.deny_char' => W_FRM_DENY_CHAR_COMMENT,
        'comment.max' => W_FRM_MAX_COMMENT
    ];

    public function __construct(array $attributes = array())
    {

        parent::__construct($attributes);
        $this->visible_device_i = true;
        $this->visible_device_d = true;
        $this->summary_type = SUMMARY_NORMAL;
        $this->navigation_title = 'おすすめコンテンツ';
        $this->other_text = 'その他';
    }

    /**********************************************************************************************
     * Begin Model relationships area.
     **********************************************************************************************/
    function choices()
    {

        return $this->hasMany('docomo\Models\Questions\Choice');
    }

    function pickups()
    {

        return $this->hasMany('docomo\Models\Pickups\Pickup');
    }

    function answers()
    {

        return $this->hasMany('docomo\Models\Answers\Answer');
    }

    function resultVotes()
    {

        return $this->hasMany('docomo\Models\Answers\ResultVote');
    }

    function column()
    {

        return $this->hasOne('docomo\Models\Questions\Column', 'question_id', 'id');
    }

    function voteRatingTotals()
    {
        return $this->hasMany('docomo\Models\Ratings\VoteRatingTotal');
    }

    function attr()
    {
        return $this->belongsTo('docomo\Models\Attributes\Attribute', 'attribute_id');
    }

    function attrValue()
    {
        return $this->belongsTo('docomo\Models\Attributes\AttributeValue', 'attribute_value_id');
    }

    /**
     * [relationship to table info_for_customers]
     *
     * @return [type] [description]
     */
    public function navigations()
    {

        return $this->hasMany('docomo\Models\Questions\Navigation', 'question_id', 'question_id');
    }

    /**
     * [relationship to table info_for_customers]
     *
     * @return [type] [description]
     */
    public function questionTag()
    {

        return $this->hasMany('docomo\Models\Questions\QuestionTag', 'question_id', 'id');
    }
    /**
     * [relationship many to many keywords]
     * @return [type] [description]
     */
    public function keyword()
    {

        return $this->belongsToMany('docomo\Models\Keywords\Keyword', 'question_tags', 'question_id', 'keyword_id');
    }

    /**
     * [relationship to table summary_by_types]
     *
     * @return [Oject]
     */

    public function summaryByType()
    {

        return $this->hasOne('docomo\Models\Questions\SummaryByType', 'question_id', 'id');
    }

    /**
     * [relationship to table Child_genres]
     *
     * @return [Oject]
     */
    public function parentGenre()
    {

        return $this->belongsTo('docomo\Models\Genres\ParentGenre', 'genre_parent_id');
    }

    /**
     * [relationship to table Child_genres]
     *
     * @return [Oject]
     */

    public function childGenre()
    {

        return $this->belongsTo('docomo\Models\Genres\ChildGenre', 'genre_child_id');
    }

    /**
     * [relationship to table accounts]
     *
     * @return [Oject]
     */
    public function account()
    {

        return $this->belongsTo('docomo\Models\Accounts\Account', 'account_id');
    }
    /**********************************************************************************************
     * End Model relationship model
     **********************************************************************************************/

    /**********************************************************************************************
     * Begin Scope define area
     **********************************************************************************************/

    public function scopeFieldRSS($query) {

        return $query->select('id', 'status', 'title', 'description', 'vote_date_from', 'genre_parent_id', 'genre_child_id');
    }

    public static function scopeFieldList($query)
    {

        return $query->selectRaw('SQL_NO_CACHE `id`, `title`, `status`, `baseline_date`, `genre_child_id`, `genre_parent_id`, `attribute_id`, `attribute_value_id`, `visible_device_d`, `visible_device_i`, `has_column`, `viewable_type`');
    }

    public static function scopeApiField($query)
    {

        return $query->selectRaw('SQL_NO_CACHE `id`, `title`, `details`, `status`, `baseline_date`, `genre_child_id`, `genre_parent_id`, `attribute_id`, `attribute_value_id`, `visible_device_d`, `visible_device_i`, `has_column`, `viewable_type`, `summary_type`');
    }

    public function scopeFieldAPIRelevant($query) {

        return $query->select('questions.id', 'title', 'genre_parent_id', 'genre_child_id');
    }

    /**
     * [scopeVoteDateStart query condition Date in table survey]
     * @param  [type] $query
     * @param  [datetime] $VoteDate
     * @return [query] add "and $VoteDate >= $field"
     */

    public function scopeWhereBetweenFrom($query, $start, $end)
    {
        return $query->whereBetween('vote_date_from', [$start, $end]);
    }

    /**
     * [scopeVoteDateEnd query condition Date in table survey]
     * @param  [type] $query
     * @param  [datetime] $VoteDate
     * @return [query] add "and $VoteDate <= $field"
     */

    public function scopeWhereBetweenTo($query, $start, $end)
    {
        return $query->whereBetween('vote_date_to', [$start, $end]);
    }

    /**
     * [scopeTitle description query condition title in table survey]
     * @param  [type] $query
     * @param  [string] $title
     * @return [query]  add "and id like %$title% "
     */

    public function scopeWhereLikeTitle($query, $title)
    {
        return $query->where('title', 'like', '%'.$title.'%');
    }

    public function scopeDisplayList($query)
    {

        return $query->whereViewableType(DISPLAY);
    }

    public function scopeFinished($query)
    {
        return $query->whereStatus(QUESTION_STATUS_FINISH);
    }

    public function scopeOngoing($query)
    {
        return $query->whereStatus(QUESTION_STATUS_READY);
    }

    public function scopeOpenTarget($query)
    {

        return $query->whereStatus(QUESTION_STATUS_WAITING)->whereRaw('vote_date_from < Now()');
    }

    public function scopeFinishTarget($query)
    {

        return $query->whereStatus(QUESTION_STATUS_READY)->whereRaw('vote_date_to < Now()');
    }

    /**
     * [scopeOrderDateFromAsc description query condition OrderDate]
     * @param  [type] $query
     * @return [query]  add " vote_date_from ORDER BY ASC"
     */

    public function scopeOrderDateFromAsc($query)
    {
        return $query->orderBy('vote_date_from', 'ASC');
    }

    /**
     * [scopeOrderDateFromAsc description query condition OrderDesc]
     * @param  [type] $query
     * @return [query]  add " vote_date_from ORDER BY DESC"
     */

    public function scopeOrderDateFromDesc($query)
    {
        return $query->orderBy('vote_date_from', 'DESC');
    }

    /**
     * [scopeOrderDateFromAsc description query condition OrderAsc]
     * @param  [type] $query
     * @return [query]  add " vote_date_to ORDER BY ASC"
     */

    public function scopeOrderDateToAsc($query)
    {

        return $query->orderBy('vote_date_to', 'ASC');
    }

    /**
     * [scopeOrderDateFromAsc description query condition OrderDesc]
     * @param  [type] $query
     * @return [query]  add " vote_date_to ORDER BY DESC"
     */

    public function scopeOrderDateToDesc($query)
    {

        return $query->orderBy('vote_date_to', 'DESC');
    }

    /**
     * [scopeOrderDateFromAsc description query condition OrderDesc]
     * @param  [type] $query
     * @return [query]  add " vote_date_to ORDER BY DESC"
     */

    public function scopeOrderBaseDateToDesc($query)
    {

        return $query->orderBy('baseline_date', 'DESC')->orderBy('id', 'desc');
    }

    public function scopeSameGenre($query, $question)
    {

        return $query->whereGenreChildId($question->genre_child_id)
            ->where('id', '<>', $question->id);
    }

    public function scopePopularByBatch($query, $type = SUMMARY_TYPE_POPULAR_ENTRY)
    {
        $idList = SummaryByType::whereType($type)->orderBy('ranking_number')->get()->lists('question_id');

        if(count($idList) > 0)
        {
            $idsStr = implode(',', $idList);
            return $query->whereIn('id', $idList)->orderByRaw(DB::raw("FIELD(id, $idsStr)"));
        }
        else
        {
            // return empty when size of idlist = 0
            return $query->where('id', '=', null);
        }
    }

    public function scopePopular($query)
    {
        return $query->where('vote_count', '>', 0)->orderBy('vote_count', 'desc')->orderBy('id', 'desc');
    }

    public function scopeBaseViewable($query, $status, $date, $device, $user, $viewableType = DISPLAY, $options = [])
    {
        if (array_key_exists('id', $options)) {
            if (is_array($options['id'])) {
                $query->whereIn('id', $options['id']);
            } else {
                $query->whereId($options['id']);
            }
        }
        if (array_key_exists('account_id', $options)) {
            $query->whereAccountId($options['account_id']);
        }
        if (!is_null($status)) {
            // statusの指定がある場合
            if (is_array($status)) {
                // 配列ならIN
                $query->whereIn('status', $status);
            } else {
                // 単独なら=
                $query->whereStatus($status);
            }
        }

        $query->where($this->deviceColumn[$device], '=', true);

        if (is_array($viewableType)) {
            $query->whereIn('viewable_type', $viewableType);
        } else {
            $query->whereViewableType($viewableType);
        }
        // 日付が指定されている場合、条件に設定
        if (isset($date)) {
            $query->where('baseline_date', '<=', $date);
        }
        // 受付中の場合、属性限定の判定をする
        if (isset($user)) {
            if ($status === QUESTION_STATUS_READY) {
                // 属性判定のみ追加
                $query->limitedAttribute($user);
            } else if  (is_array($status) && in_array(QUESTION_STATUS_READY, $status) && count($status) > 1) {
                // 属性判定と、ステータスが受付終了かを追加
                $query->limited($user);
            }
        }

        // 大ジャンル
        $parents = ParentGenre::getIgnoreByCache();
        if (array_key_exists('genre_parent_id', $options)){
            // オプションで大ジャンルが指定されている場合
            if (in_array($options['genre_parent_id'], $parents)) {
                // 除外対象の場合、検索されない様に-1をセット
                $options['genre_parent_id'] = -1;
            }
            $query->whereGenreParentId($options['genre_parent_id']);
        } else if (count($parents) > 0) {
            // オプションが指定されておらず、除外ジャンルがある場合は除外追加
            $query->whereNotIn('genre_parent_id', $parents);
        }
        unset($parents);

        // 小ジャンル
        $children = ChildGenre::getIgnoreByCache();
        if (array_key_exists('genre_child_id', $options)) {
            // オプションで小ジャンルが指定されている場合
            if (!in_array($options['genre_child_id'], $children)) {
                // 除外対象の場合、検索されない様に-1をセット
                $options['genre_child_id'] = -1;
            }
            $query->whereGenreChild($options['genre_child_id']);
        } else if (count($children) > 0) {
            // 非表示小ジャンルがある場合は、除外追加
            $query->whereNotIn('genre_child_id', $children);
        }
        unset($children);
    }

    public function scopeExcludeId($query, $question_id) {

        if (isset($question_id)) {
            $query->where('questions.id', '<>', $question_id);
        }

    }

    public function scopeUnanswered($query, $user) {

        if (isset($user)) {
            $query->leftJoin('answers', function ($join) use ($user) {
                $join->on('questions.id', '=', 'answers.question_id')
                    ->where('answers.user_id', '=', $user->id);
            })
                ->whereRaw('`answers`.`question_id` IS NULL');
        }
    }

    public function scopeAnswered($query, $user)
    {
        if (isset($user)) {
            $query->join('answers', function ($join) use ($user) {
                $join->on('questions.id', '=', 'answers.question_id')
                    ->where('answers.user_id', '=', $user->id);
            });
        }
    }

    public function scopeWhereBaseViewable($query, $viewableType = [DISPLAY]) {

        return $query->whereIn('viewable_type', $viewableType)
            ->whereExists(function ($query) {
                $query->select(\DB::raw(1))
                    ->from('genre_children')
                    ->whereRaw('genre_children.id = questions.genre_child_id')
                    ->whereChildIsVisible(true);
            })
            ->whereExists(function ($query) {
                $query->select(\DB::raw(1))
                    ->from('genre_parents')
                    ->whereRaw('genre_parents.id = questions.genre_parent_id')
                    ->whereParentIsVisible(true);
            });
    }

    public function scopeWhereBase($query, $viewableType = [DISPLAY], $device = TYPE_D_MENU)
    {

        return $query->whereBaseViewable($viewableType)
            ->where((($device === TYPE_D_MENU) ? 'visible_device_d' : 'visible_device_i'), '=', true);
    }

    public function scopeBaseList($query, $device = TYPE_D_MENU, $viewableType = [DISPLAY])
    {

        return $query->fieldList()
            ->whereBase($viewableType, $device)
            ->orderBaseDateToDesc();
    }

    public function scopeLimitedAttribute($query, User $user) {
        $attributes = [];
        foreach ($user->attrList as $key => $value) {
            if (!is_null($value) && $value !== ATTRIBUTE_VALUE_NOT_SET) {
                $attributes[] = $value;
            }
        }
        if (count($attributes) > 0) {
            $query = $query->whereRaw('(`attribute_value_id` IS NULL OR `attribute_value_id` IN (' . implode(',', $attributes) . '))');
        }

        return $query;
    }

    public function scopeLimited($query, User $user) {
        $attributes = [];
        foreach ($user->attrList as $key => $value) {
            if (!is_null($value) && $value !== ATTRIBUTE_VALUE_NOT_SET) {
                $attributes[] = $value;
            }
        }
        if (count($attributes) > 0) {
            $query = $query->whereRaw('(`status` = 6 OR `attribute_value_id` IS NULL OR `attribute_value_id` IN (' . implode(',', $attributes) . '))');
        }

        return $query;
    }

    public function scopeWithAnswer($query, $user_id)
    {

        return $query->with(['answer' => function ($q) use ($user_id) {
            $q->select('choice_id')
                ->where('user_id', '=', $user_id);
        }]);
    }

    public function scopeWhereRSSTotal($query) {

        return $query->whereRaw('`genre_parent_id` IN (11, 13, 2, 8) OR `genre_child_id` IN (18, 20)');
    }

    public function scopeLimitTime($query) {
        return $query->whereRaw('vote_date_to >= ADDDATE(Now(), INTERVAL -' . SummaryByType::$summaryHour . ' HOUR)');
    }

    public function scopeWhereGenreDirectory($query, $parentDir, $childDir)
    {
        $parentId = ParentGenre::selectRaw('SQL_CACHE `id`')->whereGenreParentDirectory($parentDir)->first()->id;
        $childId = ChildGenre::selectRaw('SQL_CACHE `id`')->whereGenreParentId($parentId)->whereGenreChildDirectory($childDir)->first()->id;

        $query->whereGenreParentId($parentId)->whereGenreChildId($childId);
    }

    public static function scopeWhereChildDir($query, $childDir)
    {

        return $query->whereIn('genre_child_id', ChildGenre::select('id')->whereGenreChildDirectory($childDir)->get()->lists('id'));
    }

    public static function scopeWhereParentDir($query, $parentDir)
    {

        return $query->whereGenreParentId(ParentGenre::select('id')->whereGenreParentDirectory($parentDir)->first()->id);
    }

    public static function scopeUserNotAnswer($query, $user_id = null)
    {
        if (!is_null($user_id)) {
            return $query->whereNotExists(function ($query) use ($user_id) {
                $query->select(\DB::raw(1))
                    ->from('answers')
                    ->where('user_id', '=', $user_id)
                    ->whereRaw('`answers`.`question_id` = `questions`.`id`');
            });
        } else {
            return $query;
        }
    }
    /**
     * [scopeUserAnswered query]
     * @param  [query condition] $query   [description]
     * @param  [int] $user_id [description]
     * @return [query condition]          [description]
     */
    public static function scopeUserAnswered($query, $user_id = null)
    {
        if($user_id != null) {

            return $query->whereExists(function ($query) use ($user_id) {
                $query->select(\DB::raw(1))
                    ->from('answers')
                    ->whereRaw('answers.question_id = questions.id')
                    ->where('user_id', '=', $user_id);
            });
        }
    }

    public static function scopeWhereKeywordId($query, $keywordId)
    {
        $query->join('question_tags', function ($join) use ($keywordId) {
            $join->on('questions.id', '=', 'question_tags.question_id')
                ->where('question_tags.keyword_id', '=', $keywordId);
        });
    }

    public static function scopeColumnVisible($query)
    {

        return $query->whereHasColumn(true);
    }
    /**********************************************************************************************
     * End Scope define area
     **********************************************************************************************/

    /**********************************************************************************************
     * Begin get list area on front end
     **********************************************************************************************/
    /**
     * [getNoAnswerList get list question has status = ready (5 in database) user hasn't Answered]
     *
     * @param  [int] $limit   [num of per page]
     * @param  [int] $user_id [user_id ]
     * @return [Object]          [list question]
     */
    public static function getNoAnswerList($limit, $user = null, $date)
    {

        return Question::fieldList()
            ->baseViewable(
                QUESTION_STATUS_READY,
                $date,
                $user->getDevice(),
                $user
            )
            ->orderBaseDateToDesc()
            ->unanswered($user)
            ->paginate($limit);
    }
    /**
     * [getAcceptList get list question has status = ready (5 in database)]
     *
     * @param  [int] $limit   [num of per page]
     * @return [Object]          [list question]
     */
    public static function getAcceptList($limit, $user, $date)
    {

        return Question::fieldList()
            ->baseViewable(
                QUESTION_STATUS_READY,
                $date,
                $user->getDevice(),
                $user
            )
            ->orderBaseDateToDesc()
            ->paginate($limit);
    }
    /**
     * [getRecentlyResultList get list question has status = finished (6 in database)]
     *
     * @param  [int] $limit   [num of per page]
     * @return [Object]          [list question]
     */
    public static function getRecentlyResultList($limit, $user, $date)
    {

        return Question::
        fieldList()
            ->baseViewable(
                QUESTION_STATUS_FINISH,
                $date,
                $user->getDevice(),
                $user
            )
            ->orderBaseDateToDesc()
            ->paginate($limit);
    }
    /**
     * [getQuestionEntryByGenre get list question has status = ready (5 in database) by genres]
     *
     * @param  [type] $limit     [num of per page]
     * @param  [type] $parentDir [parent genre directory]
     * @param  [type] $childDir  [child genre directory]
     * @return [Object]          [list question]
     */
    public static function getQuestionEntryByGenre($limit, $parentDir, $childDir, $user, $date)
    {

        return Question::fieldList()
            ->baseViewable(
                QUESTION_STATUS_READY,
                $date,
                $user->getDevice(),
                $user
            )
            ->whereGenreDirectory($parentDir, $childDir)
            ->orderBaseDateToDesc()
            ->paginate($limit);
    }
    /**
     * [getQuestionResultByGenre get list question has status = ready (6 in database) by genres]
     *
     * @param  [type] $limit     [num of per page]
     * @param  [type] $parentDir [parent genre directory]
     * @param  [type] $childDir  [child genre directory]
     * @return [Object]          [list question]
     */
    public static function getQuestionResultByGenre($limit, $parentDir, $childDir, $user, $date)
    {

        return Question::fieldList()
            ->baseViewable(
                QUESTION_STATUS_FINISH,
                $date,
                $user->getDevice(),
                null
            )
            ->whereGenreDirectory($parentDir, $childDir)
            ->orderBaseDateToDesc()
            ->paginate($limit);
    }

    /**
     * [getMyPageReadyList get list question has status = Ready (5 in database) user Answered]
     * @param  [int] $limit   [num of per page]
     * @param  [int] $user_id [user_id ]
     * @return [Object]          [list question]
     */
    public static function getMyPageReadyList(User $user)
    {

        return Question::fieldList()
            ->baseViewable(
                [QUESTION_STATUS_READY],
                null,
                $user->getDevice(),
                null
            )
            ->answered($user)
            ->orderBaseDateToDesc()
            ->get();
    }

    /**
     * [getMyPageFinishList get list question has status = Finish (6 in database) user Answered]
     * @param  [int] $limit   [num of per page]
     * @param  [int] $user_id [user_id ]
     * @return [Object]          [list question]
     */
    public static function getMyPageFinishList($limit = null, User $user, $date)
    {
        $idList = Question::selectRaw('SQL_NO_CACHE `id`')
            ->baseViewable(
                QUESTION_STATUS_FINISH,
                $date,
                $user->getDevice(),
                null
            )
            ->answered($user)
            ->orderBaseDateToDesc()
            ->paginate($limit);

        $questionIdList = $idList->lists('id');
        if (count($questionIdList) == 0) {
            $questionIdList = [-1];
        }

        $questions = Question::fieldList()
            ->whereIn('id', $questionIdList)
            ->orderByRaw('FIELD(id, ' . implode(',', $questionIdList) . ')')
            ->get();

        $questions->isPaginate = true;
        $questions->currentPage = $idList->getCurrentPage();
        $questions->lastPage = $idList->getLastPage();

        return $questions;
    }

    public static function getMyPageFinishCount(User $user)
    {

        return Question::baseViewable(
            [QUESTION_STATUS_FINISH],
            null,
            $user->getDevice(),
            null
        )
            ->answered($user)
            ->count();
    }

    /**
     * [getColumnByGenre get list question has status = ready (6 in database) by genres]
     *
     * @param  [type] $limit     [num of per page]
     * @param  [type] $parentDir [parent genre directory]
     * @param  [type] $childDir  [child genre directory]
     * @return [Object]          [list question]
     */
    public static function getColumnByGenre($limit, $parentDir, $childDir, $date)
    {
        $columns = Column::
        select('question_id', 'column_title', 'description', 'view_create_datetime')
            ->visible()
            ->whereExists(function ($query) use ($childDir, $parentDir) {
                $query->select(\DB::raw('1'))
                    ->from('questions');
                if (!is_null($childDir)) {
                    $query->whereIn('genre_child_id', ChildGenre::select('id')
                        ->from('genre_children')
                        ->whereGenreChildDirectory($childDir)->lists('id'));
                }
                $query->whereIn('genre_parent_id', ParentGenre::select('id')
                    ->from('genre_parents')
                    ->whereGenreParentDirectory($parentDir)->lists('id'))
                    ->whereRaw('questions.id = columns.question_id')
                    ->where('viewable_type', '=', DISPLAY)
                    ->where('visible_device_d', '=', true);
            })
            ->where('view_create_datetime', '<=', $date)
            ->orderBy('view_create_datetime', 'desc')
            ->orderBy('question_id')
            ->paginate($limit);

        $questionIdList = $columns->lists('question_id');
        if (!isset($questionIdList) || count($questionIdList) === 0) {
            $questionIdList = [0];
        }
        $questions =  Question::fieldList()
            ->whereIn('id', $questionIdList)
            ->orderByRaw('FIELD(id, ' . implode(',', $questionIdList) . ')')
            ->get();
        unset($questionIdList);

        foreach ($questions as &$question) {
            foreach ($columns as $column) {
                if ($question->id === $column->question_id) {
                    $question->column = $column;
                    break;
                }
            }
            unset($column);
        }
        $questions->currentPage = $columns->getCurrentPage();
        $questions->lastPage = $columns->getLastPage();
        unset($columns);
        unset($question);

        return $questions;
    }

    /**
     * [getSearchResultList get list questiton search by word]
     * @param  [int] $limit []
     * @param  [string] $word  []
     * @return [Object]        [list question]
     */
    public static function getSearchResultList($ids, $limit, $user, $date)
    {
        if (!isset($ids) || count($ids) === 0) {
            $ids = [0];
        }

        return Question::baseList()
            ->whereRaw(' `id` IN (SELECT question_id FROM question_tags WHERE keyword_id IN (' . implode(',', $ids) . '))')
            ->whereIn('status', [QUESTION_STATUS_READY, QUESTION_STATUS_FINISH])
            ->limited($user)
            ->where('baseline_date', '<=', $date)
            ->paginate($limit);
    }

    /**
     * [getTagEntryList get list tag entry has status = ongoing (5 in database)]
     *
     * @param  [int] $limit         [num of per page]
     * @param  [int] $keyword_id    [keyword_id]
     * @return [Object]             [Question: list tag entry]
     */
    public static function getTagEntryList( $limit, $keyword_id, $user, $date)
    {

        return Question::fieldList()
            ->baseViewable(
                QUESTION_STATUS_READY,
                $date,
                $user->getDevice(),
                $user
            )
            ->whereKeywordId($keyword_id)
            ->orderBaseDateToDesc()
            ->paginate($limit);
    }

    /**
     * [getTagResultList get list tag result has status = finished (6 in database)]
     *
     * @param  [int] $limit         [num of per page]
     * @param  [int] $keyword_id    [keyword_id]
     * @return [Object]             [Question: list tag result]
     */
    public static function getTagResultList( $limit, $keyword_id, $user, $date)
    {

        return Question::fieldList()
            ->baseViewable(
                QUESTION_STATUS_FINISH,
                $date,
                $user->getDevice(),
                $user
            )
            ->whereKeywordId($keyword_id)
            ->orderBaseDateToDesc()
            ->paginate($limit);

    }

    /**
     * [getTagResultColumnList get list column tag result has status = finished (6 in database)]
     *
     * @param  [int] $limit         [num of per page]
     * @param  [int] $keyword_id    [keyword_id]
     * @return [Object]             [Question: list column tag result]
     */
    public static function getTagResultColumnList( $limit, $keyword_id, $date)
    {
        $tags = QuestionTag::select('question_id')->whereKeywordId($keyword_id)->get();
        if (isset($tags) && $tags->count() > 0) {
            $columns = Column::
            select('question_id', 'column_title', 'view_create_datetime')
                ->visible()
                ->whereExists(function ($query) use ($tags) {
                    $query->select(\DB::raw('1'))
                        ->from('questions')
                        ->whereExists(function ($query) {
                            $query->select(\DB::raw(1))
                                ->from('genre_children')
                                ->whereRaw('genre_children.id = questions.genre_child_id')
                                ->whereChildIsVisible(true);
                        })
                        ->whereExists(function ($query) {
                            $query->select(\DB::raw(1))
                                ->from('genre_parents')
                                ->whereRaw('genre_parents.id = questions.genre_parent_id')
                                ->whereParentIsVisible(true);
                        })
                        ->whereIn('id', $tags->lists('question_id'))
                        ->whereRaw('questions.id = columns.question_id')
                        ->where('viewable_type', '=', DISPLAY)
                        ->where('visible_device_d', '=', true);
                })
                ->where('view_create_datetime', '<=', $date)
                ->orderBy('view_create_datetime', 'desc')
                ->orderBy('question_id')
                ->paginate($limit);

            $questionIdList = $columns->lists('question_id');
            if (!isset($questionIdList) || count($questionIdList) === 0) {
                $questionIdList = [0];
            }
            $questions =  Question::fieldList()
                ->whereIn('id', $questionIdList)
                ->orderByRaw('FIELD(id, ' . implode(',', $questionIdList) . ')')
                ->get();
            unset($questionIdList);

            foreach ($questions as &$question) {
                foreach ($columns as $column) {
                    if ($question->id === $column->question_id) {
                        $question->column = $column;
                        break;
                    }
                }
                unset($column);
            }
            $questions->currentPage = $columns->getCurrentPage();
            $questions->lastPage = $columns->getLastPage();
            unset($columns);
            unset($question);
        } else {
            $questions = Question::whereId(0)->get();
        }

        return $questions;
    }

    /**
     * [getPopularEntry get list popular entry has status = ongoing (5 in database)]
     *
     * @param  [int] $limit         [num of per page]
     * @return [Object]             [Question: list popular entry]
     */
    public static function getPopularEntry($limit, $user, $viewableType = [DISPLAY], $device = TYPE_D_MENU, $date)
    {

        return   Question::fieldList()
            ->whereBase($viewableType, $device)
            ->limited($user)
            ->popular()
            ->onGoing()
            ->where('baseline_date', '<=', $date)
            ->orderBy('id')
            ->paginate($limit);
    }

    /**
     * [getPopularResult get list popular result has status = finished (6 in database)]
     *
     * @param  [int] $limit         [num of per page]
     * @return [Object]             [Question: list popular result]
     */
    public static function getPopularResult($limit, $viewableType = [DISPLAY], $device = TYPE_D_MENU, $date)
    {

        return  Question::fieldList()
            ->whereBase($viewableType, $device)
            ->popular()
            ->finished()
            ->where('baseline_date', '<=', $date)
            ->limitTime()
            ->paginate($limit);
    }

    public static function sortByDescColumn($questions)
    {
        $questions->sortByDesc(function ($question) {

            if (isset($question->column)) {
                return $question->column->view_create_datetime;
            }
        });
    }

    public static function getTopColumn()
    {
        $date = date('Y-m-d H:i');
        $columns = Question::selectRaw('SQL_NO_CACHE `questions`.`id`, `questions`.`genre_parent_id`, `questions`.`genre_child_id`' .
            ', `columns`.`column_title`, `columns`.`view_create_datetime`')
            ->baseViewable(
                [QUESTION_STATUS_READY, QUESTION_STATUS_FINISH],
                null,
                TYPE_D_MENU,
                null
            )
            ->join('columns', function ($join) use ($date) {
                $join->on('questions.id', '=', 'columns.question_id')
                    ->where('columns.view_create_datetime', '<=', $date)
                    ->where('columns.is_visible', '=', true);
            })
            ->orderBy('columns.view_create_datetime', 'desc')
            ->limit(3)
            ->get();

        foreach ($columns as &$column) {
            $column->column = new Column();
            $column->column->column_title = $column->column_title;
            $column->column->view_create_datetime = $column->view_create_datetime;
        }

        return $columns;
    }

    /**
     * [getColumnList get list column sort by view_create_datetime in table: Column]
     *
     * @param  [int] $limit         [num of per page]
     * @return [Object]             [Question: list column list]
     */
    public static function getColumnList($limit, $device = TYPE_D_MENU, $date)
    {
        $columns = Column::
        select('question_id', 'column_title', 'description', 'view_create_datetime')
            ->visible()
            ->whereExists(function ($query) {
                $query->select(\DB::raw('1'))
                    ->from('questions')
                    ->whereExists(function ($query) {
                        $query->select(\DB::raw(1))
                            ->from('genre_children')
                            ->whereRaw('genre_children.id = questions.genre_child_id')
                            ->whereChildIsVisible(true);
                    })
                    ->whereExists(function ($query) {
                        $query->select(\DB::raw(1))
                            ->from('genre_parents')
                            ->whereRaw('genre_parents.id = questions.genre_parent_id')
                            ->whereParentIsVisible(true);
                    })
                    ->whereRaw('questions.id = columns.question_id')
                    ->where('viewable_type', '=', DISPLAY)
                    ->where('visible_device_d', '=', true);
            })
            ->where('view_create_datetime', '<=', $date)
            ->orderBy('view_create_datetime', 'desc')
            ->orderBy('question_id')
            ->paginate($limit);

        $questionIdList = $columns->lists('question_id');
        if (!isset($questionIdList) || count($questionIdList) === 0) {
            $questionIdList = [0];
        }
        $questions =  Question::fieldList()
            ->whereIn('id', $questionIdList)
            ->orderByRaw('FIELD(id, ' . implode(',', $questionIdList) . ')')
            ->get();
        unset($questionIdList);

        foreach ($questions as &$question) {
            foreach ($columns as $column) {
                if ($question->id === $column->question_id) {
                    $question->column = $column;
                    break;
                }
            }
            unset($column);
        }
        $questions->currentPage = $columns->getCurrentPage();
        $questions->lastPage = $columns->getLastPage();
        unset($columns);
        unset($question);

        return $questions;
    }

    /**
     * [getNewEntryList get list new entry has status = ongoing (5 in database)]
     *
     * @param  [int] $limit         [num of per page]
     * @return [Object]             [Question: list new entry]
     */
    public static function getNewEntryList($limit, $user, $date)
    {

        return  Question::fieldList()
            ->baseViewable(
                QUESTION_STATUS_READY,
                $date,
                $user->getDevice(),
                $user
            )
            ->orderBaseDateToDesc()
            ->paginate($limit);
    }

    /**
     * [getNewResultList get list new result has status = finished (6 in database)]
     *
     * @param  [int] $limit         [num of per page]
     * @return [Object]             [Question: list new result]
     */
    public static function getNewResultList($limit, $user, $date)
    {

        return  Question::fieldList()
            ->baseViewable(
                QUESTION_STATUS_FINISH,
                $date,
                $user->getDevice(),
                null
            )
            ->orderBaseDateToDesc()
            ->paginate($limit);
    }

    public static function getDataForList($question_id)
    {

        return Question::whereBase()->find(intval($question_id));
    }

    /**********************************************************************************************
     * End get list area on front end
     **********************************************************************************************/

    public static function getTagColumnList($limit, $keyword_id)
    {

        return Question::with('childGenre', 'childGenre.parentGenre', 'column', 'keywords')
            ->whereHas('keywords', function($q) use ($keyword_id) { $q->select('id')->where('id', '=', $keyword_id);});
    }

    public function isEnabledPickup()
    {

        return
            Auth::user()->havePermission('is_manage_pickup') &&
            ($this->status == QUESTION_STATUS_WAITING || $this->status == QUESTION_STATUS_READY || $this->status == QUESTION_STATUS_FINISH);
    }

    public function isEnabledReject()
    {

        return
            ($this->status == QUESTION_STATUS_REQUEST_APPROVE || $this->status == QUESTION_STATUS_WAITING) &&
            Auth::user()->havePermission('is_manage_agree');
    }

    public function isEnabledRequestCancel()
    {

        return
            $this->status == QUESTION_STATUS_REQUEST_APPROVE &&
            $this->account_id == Auth::user()->id;
    }

    public function isEnabledAllEdit()
    {

        return
            ($this->status == QUESTION_STATUS_READY || $this->status == QUESTION_STATUS_FINISH) &&
            Auth::user()->havePermission('is_manage_agree');
    }

    public function isEnabledDelete()
    {

        if ($this->status == QUESTION_STATUS_DRAFT || $this->status == QUESTION_STATUS_REJECT) {

            return true;
        }
        elseif ($this->status == QUESTION_STATUS_WAITING || $this->status == QUESTION_STATUS_REQUEST_APPROVE) {

            return Auth::user()->havePermission('is_manage_agree');
        }
        return false;
    }

    public function isEnabledDraftSave()
    {

        return (!isset($this->status) ||
            (($this->status == QUESTION_STATUS_DRAFT || $this->status == QUESTION_STATUS_REJECT)) &&
            ($this->account_id == \Auth::user()->id || \Auth::user()->havePermission('is_manage_agree'))
        );
    }

    public function isEnabledOverwrite()
    {

        return ($this->status == QUESTION_STATUS_REQUEST_APPROVE && \Auth::user()->havePermission('is_manage_agree'));
    }

    public function isEnabledRequestApprove()
    {

        return $this->isEnabledDraftSave();
    }

    public function isEnabledApproval()
    {

        if (Auth::user()->havePermission('is_manage_agree')) {
            return ($this->status == QUESTION_STATUS_REQUEST_APPROVE ||$this->isEnabledDraftSave());
        }

        return false;
    }

    public function isEnabledUpdate($mode = null)
    {
        if (Auth::user()->havePermission('is_manage_input_comment')) {
            // 準備中以降更新権限の場合、それ以降のステータスは可能
            return ($this->status == QUESTION_STATUS_WAITING || $this->status == QUESTION_STATUS_READY || $this->status == QUESTION_STATUS_FINISH);
        }
        else if (Auth::user()->havePermission('is_manage_agree') && $mode == MODE_ALL_EDIT) {
            // 管理権限があり、全編集モードの場合
            return ($this->status == QUESTION_STATUS_READY || $this->status == QUESTION_STATUS_FINISH);
        }

        return false;
    }

    public function isViewableResult()
    {

        return ($this->status == QUESTION_STATUS_WAITING || $this->status == QUESTION_STATUS_READY || $this->status == QUESTION_STATUS_FINISH);
    }

    public function isViewableAverage()
    {

        return ($this->summary_type == SUMMARY_AVERAGE);
    }

    public function isFinished()
    {

        return ($this->status == QUESTION_STATUS_FINISH);
    }

    public function isViewable($device = TYPE_D_MENU, $user = null)
    {

        return ((
                ($this->status == QUESTION_STATUS_READY
                    && (is_null($this->attribute_id)
                        ||
                        is_null($user)
                        ||
                        !$user->has($this->attribute_id)
                        ||
                        $user->attrList[$this->attribute_id] == $this->attribute_value_id))
                ||
                $this->status == QUESTION_STATUS_FINISH)
            && (!is_null($this->getParentGenre()->parent_is_visible) && !is_null($this->getChildGenre()->child_is_visible))
            && (($device !== TYPE_D_MENU) ? $this->visible_device_i : $this->visible_device_d)
            && $this->viewable_type != HIDDEN
        );
    }

    public function isViewableList($device = TYPE_D_MENU, $user = null)
    {

        return $this->isViewable($device, $user) && $this->viewable_type == DISPLAY;
    }

    public function isViewableEditPage()
    {

        return
            (\Auth::user()->allow_genre_parent == ALLOW_ALL_GENRE || \Auth::user()->allow_genre_parent == $this->genre_parent_id) &&
            $this->isViewableCsvLink();
    }

    public function isViewableCsvLink()
    {

        return
            (\Auth::user()->havePermission('is_viewable_all_question') || $this->account_id == \Auth::user()->id);
    }

    public function isAnswered($userId)
    {
        if (is_null($userId)) {
            return null;
        }

        if (isset($this->choice)) {

            return isset($this->choice->choice_num);
        } else {
            $answer = Answer::select('choice_id')
                ->whereRaw('`question_id`=? AND `user_id`=?',
                    [
                        $this->id,
                        $userId
                    ])
                ->first();

            return isset($answer);
        }
    }

    public function canVote($user) {

        return $this->isViewable($user->getDevice(), $user)
        && !$this->isAnswered($user->id);
//            && (!isset($this->attribute_id) || $this->attribute_value_id == $user->attrList[$this->attribute_id]);
    }

    public function setFillable($mode)
    {

        $this->fillable = Question::$fillable_array[$mode];
    }

    public function setBaselineDate(){

        if ($this->status === QUESTION_STATUS_FINISH) {
            $this->baseline_date = $this->vote_date_to;
        } else {
            $this->baseline_date = $this->vote_date_from;
        }
    }

    public function getNowStatus()
    {

        $status = $this->status;

        $now_dt = new \DateTime();
        $now_dt->setTimeZone(new \DateTimeZone('Asia/Tokyo'));
        $now_str = $now_dt->format('Y-m-d H:i:s');
        $now = strtotime($now_str);

        $from = strtotime($this->vote_date_from);
        $to = strtotime($this->vote_date_to);

        if ($status == QUESTION_STATUS_WAITING || $status == QUESTION_STATUS_READY || $status == QUESTION_STATUS_FINISH) {
            if ($from <= $now) {
                // 開始が過去
                if ($to < $now) {
                    // 終了が過去なら終了済み
                    $status = QUESTION_STATUS_FINISH;
                } else {
                    // 終了が当日以前なら受付中
                    $status = QUESTION_STATUS_READY;
                }
            } else {
                // 開始が未来なら、受付準備中
                $status = QUESTION_STATUS_WAITING;
            }
        }

        return $status;
    }

    private $account = null;
    public function getAccount()
    {
        if (is_null($this->account)) {
            $this->account =  Account::select('account_name')->find($this->account_id);
        }

        return $this->account;
    }

    public function getAccountName()
    {

        return Account::getAccountName($this->account_id);
    }

    private $status_name = null;
    public function getStatusName()
    {
        if (is_null($this->status_name)) {
            $this->status_name = QuestionStatus::$label[$this->status];
        }

        return $this->status_name;
    }

    public function getFullUrl() {

        return str_replace('/' . \Request::path() , '', \Request::url()) . $this->getPath();
    }

    public function getFullColumnUrl() {

        return $this->getFullUrl() . '?showresult=1#column';
    }

    public function getPath() {

        return $this->getDirectory() . '/' . (($this->status == QUESTION_STATUS_READY) ? 'entry/' : 'result/') . $this->id;
    }

    public function getEntryPath() {

        return $this->getDirectory() . '/entry/' . $this->id;
    }

    public function getPostPath() {

        return $this->getDirectory() . '/result/' . $this->id;
    }

    public function getColumnPath() {

        return $this->getPostPath() . '?showresult=1#column';
    }

    private $directory = null;
    public function getDirectory()
    {
        if (is_null($this->directory)) {
            $this->directory = '/' . $this->getParentGenreDirectory() . '/' . $this->getChildGenreDirectory();
        }

        return $this->directory;
    }

    private $parent_genre = null;
    public function getParentGenre()
    {
        if (is_null($this->parent_genre)) {
            $this->parent_genre = ParentGenre::getDataByCache($this->genre_parent_id);
        }
        if (is_null($this->parent_genre)) {
            $this->parent_genre = ParentGenre::select('genre_parent_name', 'genre_parent_directory', 'parent_is_visible')
                ->find($this->genre_parent_id);
        }

        return $this->parent_genre;
    }

    public function getParentGenreDirectory()
    {

        return $this->getParentGenre()->genre_parent_directory;
    }

    private $child_genre = null;

    public function getChildGenre()
    {
        if (is_null($this->child_genre)) {
            $children = ChildGenre::getDataByCache($this->genre_parent_id);
            if (array_key_exists($this->genre_child_id, $children)) {
                $this->child_genre = $children[$this->genre_child_id];
            }
        }
        if (is_null($this->child_genre)) {
            $this->child_genre = ChildGenre::select('genre_child_name', 'genre_child_directory', 'child_is_visible')
                ->find($this->genre_child_id);
        }

        return $this->child_genre;
    }

    public function getChildGenreName()
    {

        return $this->getChildGenre()->genre_child_name;
    }

    public function getChildGenreDirectory()
    {

        return $this->getChildGenre()->genre_child_directory;
    }

    public function getSnsCommentOriginal()
    {

        return (empty($this->sns_comment) ? (($this->isFinished() ? '【結果発表】' : '【受付中】') . $this->title) : $this->sns_comment);
    }

    public function getSnsComment()
    {

        return urlencode($this->getSnsCommentOriginal());
    }

    private $limited_text = null;
    public function getLimitedText()
    {
        if (is_null($this->limited_text)) {
            if (!isset($this->attribute_value_id)) {
                $this->limited_text = '';
                $this->attribute_value_name = '';
            } else {
                $this->limited_text = $this->getLimitedName() . '：' . $this->getLimitedValueName();
            }
        }

        return $this->limited_text;
    }

    private $limited_attribute = null;
    public function getLimited()
    {
        if (is_null($this->limited_attribute)) {
            if (!isset($this->attribute_value_id)) {
                $this->limited_attribute = '';
            } else {
                $this->limited_attribute = Attribute::select('attribute_name', 'attribute_label', 'attribute_directory')->find($this->attribute_id);
            }
        }

        return $this->limited_attribute;
    }

    public function getLimitedName()
    {

        return $this->getLimited()->attribute_name;
    }

    public function getLimitedLabel()
    {

        return $this->getLimited()->attribute_label;
    }

    private $attribute_value = null;
    public function getLimitedValue()
    {
        if (is_null($this->attribute_value)) {
            if (!isset($this->attribute_value_id)) {
                $this->attribute_value = '';
            } else {
                $this->attribute_value = AttributeValue::select('attribute_value_name')->find($this->attribute_value_id);
            }
        }

        return $this->attribute_value;
    }
    public function getLimitedValueName()
    {

        return $this->getLimitedValue()->attribute_value_name;
    }

    private $choice_count = null;
    public function getChoiceCount()
    {
        if (is_null($this->choice_count)) {
            $this->choice_count = Choice::whereQuestionId($this->id)->count('id');
        }

        return $this->choice_count;
    }

    private $choice_data = null;
    public function getChoices()
    {
        if (is_null($this->choice_data)) {
            $this->choice_data = Choice::select('id', 'choice_num', 'choice_text', 'summary_value')->whereQuestionId($this->id)->get();
        }

        return $this->choice_data;
    }

    public function getColumn()
    {

        return Column::select('column_title', 'column_detail', 'view_create_datetime', 'is_visible')
            ->visible()
            ->find($this->id);
    }

    private $keywords = null;
    public function getKeywords() {
        if (is_null($this->keywords)) {
            $keywordIds = QuestionTag::select('keyword_id')->whereQuestionId($this->id)->lists('keyword_id');
            $this->keywords = Keyword::select('id', 'keyword_name')
                ->whereIn('id', (count($keywordIds) > 0) ? $keywordIds : [-1])
                ->visible()
                ->get();
        }

        return $this->keywords;
    }

    private $choice = null;
    public function getAnswered($userId) {
        if (is_null($this->choice)) {
            if (!is_null($this->answer)) {
                $this->choice = $this->answer;
            } else {
                $answered = Answer::select('choice_id')->whereQuestionId($this->id)->whereUserId($userId)->get();
                foreach ($answered as $choice) {
                    $this->choice = Choice::select('id', 'choice_num', 'choice_text')->find($choice->choice_id);
                    if (is_null($this->choice)) {
                        $this->choice = new Choice();
                        $this->choice->id = $choice->choice_id;
                        $this->choice->choice_text = '選択肢が削除されました';
                        $this->choice->choice_num = 999;
                    }
                    break;
                }
                if (is_null($this->choice)) {
                    $this->choice = new Choice();
                }
            }
        }

        return $this->choice;
    }

    private $rssGenre = null;
    public function getRSSGenre() {
        if (!isset($this->rssGenre) || is_null($this->rssGenre)) {
            // 大ジャンルがi-mode版RSSの場合はそれをセット
            if ($this->genre_parent_id === 11) {
                $this->rssGenre = 'd';
            } else if ($this->genre_parent_id === 4 &&
                $this->genre_child_id === 18) {
                $this->rssGenre = 'i';
            } else if ($this->genre_parent_id === 13) {
                $this->rssGenre = 's';
            } else if ($this->genre_parent_id === 2) {
                $this->rssGenre = 'n';
            } else if ($this->genre_parent_id === 4 &&
                $this->genre_child_id === 20) {
                $this->rssGenre = 'e';
            } else if ($this->genre_parent_id === 8) {
                $this->rssGenre = 'l';
            } else {
                $this->rssGenre = 'expect';
                $this->rssGenre = 'except';
            }
        }

        return $this->rssGenre;
    }

    public function afterUpdate()
    {
        // ステータスの更新がない場合は、処理を行わない
        if (!isset($this->status)) return;
        // 各種リスト更新
        // まずはリストから削除
        SummaryByType::deleteByQuestionId($this->id);
        SummaryByType::setCache();
    }

    public function vote(User $user, $answerNo, $choiceId = null) {

        if (isset($this->attribute_id) && $user->attrList[$this->attribute_id] != $this->attribute_value_id) {
            // 属性限定が設定されており、ユーザの属性が一致しない場合は投票データを作成しない。
            return false;
        }

        if (is_null($choiceId)) {
            // 選択肢IDが渡ってきていない場合、選択肢番号からIDを取得する
            $choiceId = \DB::select('SELECT SQL_NO_CACHE id FROM choices WHERE question_id = ? AND choice_num = ?',
                [$this->id, $answerNo]
            )[0]->id;
        }

        // 投票の実処理部分だけをトランザクション
        \DB::transaction(function () use ($user, $choiceId) {
            if (Answer::addAnswer($this->id, $choiceId, $user)) {
                // 投票レコードが作成できた場合、カウントアップ

                Question::incrementVoteCount($this->id);
                ResultVote::incrementCount($this->id, $choiceId, $user);
            }
            \DB::commit();
        });
        unset($choiceId);
        return true;
    }

    /**
     * [change status of survey]
     *
     * @param  [int] $id     [id of survey]
     * @param  [int] $status [new status of survey]
     * @return [Survey]         [Survey with new data]
     */
    public static function changeStatus($id, $set_status)
    {
        $question = Question::findOrFail($id);
        $status = $question->getNowStatus();
        if ($set_status == QUESTION_STATUS_DRAFT &&
            $status == QUESTION_STATUS_REQUEST_APPROVE)
        {
            // 承認依頼中に、下書きに戻す場合は可能
            $question->status = QUESTION_STATUS_DRAFT;
        }
        elseif (intval($set_status) == QUESTION_STATUS_REJECT &&
            ($status == QUESTION_STATUS_REQUEST_APPROVE || $status == QUESTION_STATUS_WAITING))
        {
            $question->status = QUESTION_STATUS_REJECT;
        }
        else
        {
            $question->status = $status;
        }
        $question->result = $question->save();

        return $question;
    }

    public static function setColumnFlg($id, $setFlg = true) {

        \DB::update('UPDATE questions SET has_column = ? WHERE id = ?',
            [$setFlg, $id]);
    }

    public static function deleteData($id)
    {

        $question = static::find($id);
        static::deleteRelationData($question->choices);
        static::deleteRelationData($question->questionTag);
        static::deleteRelationData($question->navigations);
        \DB::delete('DELETE FROM vote_rating_totals WHERE question_id = ?', [$question->id]);
        \DB::delete('DELETE FROM pickups WHERE question_id = ?', [$question->id]);
        SummaryByType::deleteByQuestionId($id);
        \DB::delete('DELETE FROM answers WHERE question_id = ?', [$question->id]);
        \DB::delete('DELETE FROM daily_vote_ranking WHERE question_id = ?', [$question->id]);

        $question->delete();
    }

    private static function deleteRelationData($data)
    {
        foreach ($data as $delData) {
            if (isset($delData->thumbnail) && \File::exists($delData->thumbnail)) {
                \File::delete(public_path($delData->thumbnail));
            }
            $delData->delete();
        }
    }

    public static function fillWithModify($mode = MODE_NORMAL, $question_id = null)
    {

        $question = null;
        $status = QUESTION_STATUS_DRAFT;
        if ($question_id === null)
        {
            $question = new Question;
            $question->has_column = false;
            $question->account_id = Auth::user()->id;
            $question->status = $status;
        }
        else
        {
            $question = Question::find($question_id);
            $status = $question->status;
            // 設問が下書き or 否認の場合、全編集
            if ($question->status == QUESTION_STATUS_DRAFT || $question->status == QUESTION_STATUS_REJECT) {
                $mode = MODE_ALL_EDIT;
            }
        }
        $input_status = Input::get('status');
        if (($question->status == QUESTION_STATUS_DRAFT || $question->status == QUESTION_STATUS_REJECT) &&
            $input_status == QUESTION_STATUS_REQUEST_APPROVE)
        {
            // 現在が下書きかつ承認依頼の場合、承認依頼に設定する
            $status = QUESTION_STATUS_REQUEST_APPROVE;
        }
        elseif (Auth::user()->havePermission('is_manage_agree'))
        {
            if ((
                    $question->status == QUESTION_STATUS_REQUEST_APPROVE ||
                    $question->status == QUESTION_STATUS_DRAFT ||
                    $question->status == QUESTION_STATUS_REJECT) &&
                $input_status == QUESTION_STATUS_WAITING) {
                $status = QUESTION_STATUS_WAITING;
            }
        }

        $question->setFillable($mode);
        $question = $question->fill(Input::all());
        unset($question->status);
        if (!empty($status))
        {
            $question->status = $status;
        }

        if ($mode == MODE_ALL_EDIT) {
            $question->vote_date_from = $question->getDatetimeControlValue('vote_date_from');
            $question->vote_date_to = $question->getDatetimeControlValue('vote_date_to');
            if (strlen($question->vote_date_from) === 16 &&
                strlen($question->vote_date_to) === 16) {
                $question->vote_date_from = substr($question->vote_date_from, 0, 15) . '0';
                $question->vote_date_to = substr($question->vote_date_to, 0, 15) . '0';
            } else {
                $question->vote_date_from = null;
                $question->vote_date_to = null;
            }
            $question->genre_child_id = Input::get('genre');
            $question->genre_parent_id = ChildGenre::getParentId($question->genre_child_id);
            $question->is_visible_other = (Input::has('is_visible_other') && Input::get('viewable_type') == SUMMARY_NORMAL) ? VISIBLE : HIDDEN;
            if ($question->is_visible_other === HIDDEN) {
                unset($question->other_text);
            }
            if ($question->attribute_value_id == UNLIMITED) {
                $question->attribute_value_id = null;
                $question->attribute_id = null;
            } else {
                $question->attribute_id = AttributeValue::select('attribute_id')->find($question->attribute_value_id)->attribute_id;
                if (is_null($question->attribute_id)) {
                    $question->attribute_value_id = null;
                }
            }
        }
        if ($question->summary_type != SUMMARY_AVERAGE) {
            unset(Question::$rules['average_unit']);
            $question->summary_type = SUMMARY_NORMAL;
        }
        if (!array_key_exists($question->viewable_type, QuestionViewable::$label)) {
            $question->viewable_type = null;
        }

        $question->visible_device_d = Input::has('visible_device_d') ? VISIBLE : HIDDEN;
        $question->visible_device_i = Input::has('visible_device_i') ? VISIBLE : HIDDEN;
        $question->shuffle_choice = Input::has('shuffle_choice') ? true : false;

        $question->status = $question->getNowStatus();
        if ($question->status == QUESTION_STATUS_FINISH) {
            $question->baseline_date = $question->vote_date_to;
        } else {
            $question->baseline_date = $question->vote_date_from;
        }

        return $question;
    }

    public static function incrementVoteCount($id) {
        \DB::update('UPDATE questions SET vote_count = vote_count + 1 WHERE id = ?',
            [$id]);
    }

    public static function getTopCount()
    {

        $counts = [QUESTION_STATUS_REQUEST_APPROVE => 0, QUESTION_STATUS_REJECT => 0];
        if (Auth::user()->havePermission('is_viewable_all_question')) {
            $counts[QUESTION_STATUS_REQUEST_APPROVE] = Question::whereStatus(QUESTION_STATUS_REQUEST_APPROVE)->count('id');
            $counts[QUESTION_STATUS_REJECT] = Question::whereStatus(QUESTION_STATUS_REJECT)->count('id');
        } else {
            $counts[QUESTION_STATUS_REQUEST_APPROVE] = Question::whereStatus(QUESTION_STATUS_REQUEST_APPROVE)->whereAccountId(Auth::user()->id)->count('id');
            $counts[QUESTION_STATUS_REJECT] = Question::whereStatus(QUESTION_STATUS_REJECT)->whereAccountId(Auth::user()->id)->count('id');
        }

        return $counts;
    }

    public static function getDataByAttribute(User $user)
    {
        $attributes = [];
        foreach ($user->attrList as $attr) {
            if (is_null($attr) || $attr === ATTRIBUTE_VALUE_NOT_SET) continue;
            $attributes[] = $attr;
        }

        $questions = Question::fieldList()
            ->baseViewable(
                QUESTION_STATUS_READY,
                null,
                $user->getDevice(),
                null
            )
            ->unanswered($user)
            ->orderBy(\DB::raw('RAND()'));
        if (count($attributes) === 0) {
            $questions = $questions->whereIn('attribute_id', [ATTR_BIRTHDAY, ATTR_GENDER, ATTR_LOCAL]);
        } else {
            $questions = $questions->whereIn('attribute_value_id', $attributes);
        }

        return $questions->first();
    }

}