<?php
$I = new AcceptanceTester\ProfileSteps($scenario);
$I->wantTo('check FP profile');

$profile = array(
				'sex' => array('name' => 'sex', 'value' => '男性'),
				'year' => array('name' => 'birthday_year', 'value' => '1990'),
				'month' => array('name' => 'birthday_month', 'value' => '06'),
				'day' => array('name' => 'birthday_day', 'value' => '01'),
				'place' => array('name' => 'pref', 'value' => '京都府'),
				'submit' => '登録する',
				);

$descriptions = array(
				'プロフィール設定でアンケート結果が楽しくなるよ！', 
				'※プロフィール設定でできることの説明',
				'あなたのプロフィールに合った受付中アンケートをTOPですぐに見つけることができます',
				'あなたのプロフィールに合ったアンケート結果を一覧でみることができます',
				'投票結果で、あなたのあなたのプロフィールに合った情報をすぐに見ることができます'
				);

$links_confirm = array(
						array('title' => 'みんなの声TOPへ', 'url' => '/'),
						array('title' => 'プロフィール確認', 'url' => '/profile'),
					);

//create profile to check
$I->createProfile($profile, $descriptions);

//check profile create success
$I->checkFPProfileSuccess($links_confirm);
//check profile after set
$I->checkFPProfile($profile, $descriptions);
