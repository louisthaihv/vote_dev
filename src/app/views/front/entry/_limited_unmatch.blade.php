<div class="restraintInfo l-margin">
    <p>このアンケートは</p>
    <p>{{{ $question->getLimitedText() }}}限定です。</p>
    <p class="m-margin">登録されている{{{ $question->getLimitedLabel() }}}： {{{ $currentUser->getAttributeValueName($question->attribute_id) }}}</p>
</div>
