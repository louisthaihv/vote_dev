<?php
    $function_id = str_replace(']', '', str_replace('[', '_', $uploader_id));
    $control_id = str_replace(']', '\\\\]', str_replace('[', '\\\\[', $uploader_id));
    $control_ids = explode('[', $uploader_id);
    $control_id_unique = $control_ids[0];
    $control_id_row = '';
    $control_id_row_js = '';
    $control_file_id = $control_id_unique;
    if (count($control_ids) > 1) {
        $control_id_row = '[' . $control_ids[1];
        $control_id_row_js = '\\\\[' . str_replace(']', '\\\\]', $control_ids[1]);
        $control_file_id = $control_file_id . '_' . str_replace(']', '', $control_ids[1]);
    }
?>
<div id="{{ $control_id_unique . '_input' . $control_id_row }}" style="{{ (isset($db_file) && !empty($db_file)) ? 'display:none;' : '' }}"><input type="file" id="{{ $control_file_id }}" name="{{ $control_file_id }}" /></div>
<div id="{{ $control_id_unique . '_preview' . $control_id_row }}" style="{{ (isset($db_file) && !empty($db_file)) ? '' : 'display:none;' }}">
    <img src="{{ $db_file }}" id="{{ $control_id_unique . '_img' . $control_id_row }}" class="img_preview" />
    <input type="hidden" id="{{ $control_id_unique . '_remove' . $control_id_row }}" name="{{ $control_id_unique . '_remove' . $control_id_row }}" />
    <button name="delete" type="button" id="{{ $control_id_unique . '_delete' . $control_id_row }}" class="naviBtn">削除</button>
</div>
<script type="text/javascript">
    <!--
    function previewImage(input_file)
    {
        if ( !this.files.length ) {
            return;
        }

        var file = input_file;
        var fr = new FileReader();
        fr.onload = function() {
            $('#{{ $control_id_unique . '_img' . $control_id_row_js }}').attr('src', fr.result );
            $('#{{ $control_id_unique . '_preview' . $control_id_row_js }}').show();
            $('#{{ $control_id_unique . '_input' . $control_id_row_js }}').hide();
        }
        fr.readAsDataURL(file);
    }
    function set{{ $function_id }}OnChange() {
        $('#{{ $control_file_id }}').change(
                function() {
                    if ( !this.files.length ) {
                        return;
                    }

                    var file = $(this).prop('files')[0];
                    var fr = new FileReader();
                    fr.onload = function() {
                        $('#{{ $control_id_unique . '_img' . $control_id_row_js }}').attr('src', fr.result );
                        $('#{{ $control_id_unique . '_preview' . $control_id_row_js }}').show();
                        $('#{{ $control_id_unique . '_input' . $control_id_row_js }}').hide();
                    }
                    fr.readAsDataURL(file);
                }
        );
    }
    $('#{{ $control_id_unique . '_delete' . $control_id_row_js }}').click(
        function () {
            $('#{{ $control_id_unique . '_remove' . $control_id_row_js }}').val('1');
            $('#{{ $control_file_id }}').replaceWith('<input type="file" id="{{ $control_file_id }}" name="{{ $control_file_id }}" />');
            $('#{{ $control_id_unique . '_img' . $control_id_row_js }}').attr('src', '' );
            $('#{{ $control_id_unique . '_preview' . $control_id_row_js }}').hide();
            $('#{{ $control_id_unique . '_input' . $control_id_row_js }}').show();
            set{{ $function_id }}OnChange();
            return false;
        }
    );
    set{{ $function_id }}OnChange();
    //-->
</script>