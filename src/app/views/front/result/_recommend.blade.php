@if($navigations->count() > 0)
    <section class="l-margin">
    <h4 class="subSectionTitH4 recommendIcon"><span>{{ $question->navigation_title }}</span></h4>
    <div class="recommendList">
        <ul>
            @foreach($navigations as $navigation)
            <li>
                <a href="{{ $navigation->url }}" id="out{{ sprintf('%1$02d', $navigation->row++) }}">
                    <div class="recommendListBox">
                        @if($navigation->thumbnail)
                        <div>
                            {{ HTML::image($navigation->thumbnail, $navigation->description, ['class' => 'recommendImage']) }}
                        </div>
                        @endif
                        <div class="recommendText">
                            {{ $navigation->description }}
                        </div>
                    </div>
                </a>
            </li>
            @endforeach
        </ul>
    </div>
</section>
@endif
