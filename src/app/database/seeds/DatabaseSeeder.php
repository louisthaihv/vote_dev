<?php
use Test\SampleDataSeeder;

class DatabaseSeeder extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        
        Eloquent::unguard();
        
        $this->call('AttributesSeeder');
        $this->call('AttributeValuesSeeder');

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
