@include('layouts.front_elements._space')
<div style="text-align:center;">
@if (isset($paginator))
@if ($paginator->getCurrentPage() > 1)
<a href="{{ $paginator->getUrl($paginator->getCurrentPage()-1) }}&uid=NULLGWDOCOMO&date={{ \Input::get('openDate', date_format(new \DateTime(), 'Y-m-d_H__i')) }}" accesskey="*">&lt;[*]前へ</a>
@endif
&nbsp;
@if ($paginator->getCurrentPage() < $paginator->getLastPage())
<a href="{{ $paginator->getUrl($paginator->getCurrentPage()+1) }}&uid=NULLGWDOCOMO&date={{ \Input::get('openDate', date_format(new \DateTime(), 'Y-m-d_H__i')) }}" accesskey="#">[#]次へ&gt; </a>
@endif
@endif
</div>
