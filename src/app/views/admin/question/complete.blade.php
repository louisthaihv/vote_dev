@extends('layouts.admin_in')

@section('content')
    <h2>アンケート登録</h2>
    <div class="tcenter">

    @if (isset($message_type))
        @if ($status == QUESTION_STATUS_REJECT)
            <p class="l-margin">差し戻しました。</p>
            <p class="l-margin"><a href="{{ route('manage.question.edit', $question_id) }}">内容を確認する</a></p>
        @elseif ($status == QUESTION_STATUS_DRAFT)
            <p class="l-margin">下書きに戻しました。</p>
            <p class="l-margin"><a href="{{ route('manage.question.edit', $question_id) }}">再度編集する</a></p>
        @elseif ($status == QUESTION_STATUS_REQUEST_APPROVE)
            <p class="l-margin">上書き保存しました。</p>
            <p class="l-margin"><a href="{{ route('manage.question.edit', $question_id) }}">再度編集する</a></p>
        @elseif ($status == QUESTION_STATUS_WAITING)
            <p class="l-margin">更新しました。</p>
            <p class="l-margin"><a href="{{ route('manage.question.edit', $question_id) }}">内容を確認する</a></p>
        @elseif ($status == QUESTION_STATUS_FINISH)
            <p class="l-margin">更新しました。</p>
            <p class="l-margin"><a href="{{ route('manage.question.edit', $question_id) }}">内容を確認する</a></p>
        @elseif ($status == QUESTION_STATUS_READY)
            <p class="l-margin">更新しました。</p>
            <p class="l-margin"><a href="{{ route('manage.question.edit', $question_id) }}">内容を確認する</a></p>
        @endif
    @else
        @if ($status == QUESTION_STATUS_DRAFT || $status == QUESTION_STATUS_REJECT)
            <p class="l-margin">下書きとして保存しました。</p>
            <p class="l-margin"><a href="{{ route('manage.question.edit', $question_id) }}">再度編集する</a></p>
        @elseif ($status == QUESTION_STATUS_REQUEST_APPROVE)
            <p class="l-margin">承認申請しました。</p>
            <p class="l-margin"><a href="{{ route('manage.question.edit', $question_id) }}">内容を確認する</a></p>
        @elseif ($status == QUESTION_STATUS_WAITING)
            <p class="l-margin">受付準備中として反映しました。</p>
            <p class="l-margin"><a href="{{ route('manage.question.edit', $question_id) }}">内容を確認する</a></p>
        @elseif ($status == QUESTION_STATUS_READY)
            <p class="l-margin">受付中として反映しました。</p>
            <p class="l-margin"><a href="{{ route('manage.question.edit', $question_id) }}">内容を確認する</a></p>
        @elseif ($status == QUESTION_STATUS_FINISH)
            <p class="l-margin">受付終了として反映しました。</p>
            <p class="l-margin"><a href="{{ route('manage.question.edit', $question_id) }}">内容を確認する</a></p>
        @endif
    @endif

        <p class="l-margin"><a href="{{ route('manage.question.index') }}">アンケート一覧へ</a></p>
    </div>
@stop