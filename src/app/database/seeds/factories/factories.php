<?php

use docomo\Models\Genres\ParentGenre;
use docomo\Models\Genres\ChildGenre;
use docomo\Models\Questions\Question;

// $factory('ParentGenre', [
//     'genre_parent_name' => $faker->name,
//     'parent_is_visible' => 1,
//     'genre_parent_directory' => $faker->name,
//     'parent_sort_order' => $faker->name,
// ]);

$factory('Question', [
    'genre_parent_id' => 'factory:ParentGenre',
    'genre_child_id' => 'factory:ChildGenre',
    'status' => rand(1, 6),
    'title' => $faker->body,
    'details' => $faker->body,
    'vote_date_from' => $faker->dateTime,
    // 'details' => $faker->body,
    // 'details' => $faker->body,
]);
