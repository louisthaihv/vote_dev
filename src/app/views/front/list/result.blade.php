@extends('layouts.front_in')

@section('metaTag')
    <title>結果発表 | みんなの声</title>
    <meta name="description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！">
    <meta property="og:title" content="結果発表 | みんなの声" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
    <meta name="twitter:title" content="結果発表 | みんなの声" />
    <meta name="twitter:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
@stop

@section('script')
    @include('front._shared.js_load_more_pages', ['classNames'=>['recentlyResult'], 'nums' => [NUM_FIRST_PAGE], 'pages' => [PAGER_SIZE_FRONT], 'status' => ['entry']])
@endsection

@section('content')
    <h2 class="resultListTit"><span>結果発表</span></h2>

    @include('front._shared._question_list', ['className' => 'recentlyResult', 'displaySeeMore' => TRUE])

    @include('front._shared._link_to_list', ['text' => 'コラム一覧', 'route' => 'list.column'])
    @include('layouts.front_elements._profileBn')
@endsection
