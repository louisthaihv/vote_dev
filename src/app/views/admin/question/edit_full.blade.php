@extends('layouts.admin_in')

@section('script')
    <script type="text/javascript">
    $(window).load(function(){
        $('button.btnUpdateData').click(function(){
            if(confirm('{{ '' }}')){
                $('#status').val('{{''}}');
                $('#form').submit();
            }
        });
    });
    </script>
@endsection

@section('content')
<h2>アンケート登録</h2>

@include('admin.question.form', ['url' => 'manage/question/'. $question->id . '/edit', 'method' => 'put', 'status' => QUESTION_STATUS_CREATE, 'mode' => $mode])
@stop