<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAttributeValue extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasColumn('attribute_values', 'is_default')) {
			Schema::table('attribute_values', function($table) {
				$table->boolean('is_default')->after('attribute_value_directory')->default(false);
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasColumn('attribute_values', 'is_default')) {
			Schema::table('attribute_values', function($table) {
				$table->dropColumn('is_default');
			});
		}
	}

}
