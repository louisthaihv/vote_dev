<?php namespace Front;

use docomo\Models\Features\Feature;
use docomo\Models\Questions\Question;
use docomo\Models\Questions\QuestionTag;
use docomo\Models\Attentions\Attention;
use docomo\Models\Answers\ResultVote;
use docomo\Models\Pickups\Pickup;
use docomo\Models\Questions\SummaryByAttributes;
use docomo\Models\Questions\SummaryByType;
use docomo\Models\Users\User;
use docomo\Models\Users\UserAttribute;
use docomo\Models\Attributes\AttributeValue;
use docomo\Models\Genres\ChildGenre;
use docomo\Models\Genres\ParentGenre;

class HomeController extends \BaseController {

    public function __construct(){

    }

    public function index() {

        $user = $this->getCurrentUser();
        // Feature: View past Special Features
        $feature = Feature::getNewestFeature();
        $past_features = Feature::getPastCount();

        $attributeFavorite = null;
        if ($user->has(ATTR_BIRTHDAY) && $user->has(ATTR_GENDER)) {
            $attributeFavorite = SummaryByAttributes::getDataByCache($user->attrList[ATTR_BIRTHDAY], $user->attrList[ATTR_GENDER]);
        }
        $limited = Question::getDataByAttribute($user);

        // User pickup list
        $pickups[PROCESSING] = Pickup::getQuestionList(PROCESSING, $user, $limited);
        $pickups[COMPLETE] = Pickup::getQuestionList(COMPLETE, $user, $attributeFavorite);

        $summaryData = SummaryByType::getTopData($this->getCurrentUser());

        $columns = Question::getTopColumn($user);

        return \View::make('front.home.top',
            compact('feature', 'past_features', 'pickups', 'summaryData', 'user', 'columns', 'attributeFavorite', 'limited'));
    }
}