<?php

namespace docomo\Models\Answers;

class Answer extends \BaseArdentModel
{
    protected $table = 'answers';

    public $timestamps = false;
    /*
     * Model relationships
     */

    function choice()
    {
        return $this->belongsTo('docomo\Models\Questions\Choice');
    }

    /*
     * Override date mutator definition
     */

//    public function getDates()
//    {
//        return ['created_at', 'updated_at', 'view_create_datetime'];
//    }

    /*
     * Acceptable params from input
     */
    protected $fillable = [
    ];

    /*
     * Validation rules
     */
    public static $rules = [
    ];

    /*
     * Custom validation messages
     */
    public static $customMessages = array(
    );

    /*
     * Methods
     */

    public static function hasAnswered($user, $question)
    {
        if(!isset($user)){
            return false;
        }
        $answer = Answer::where('user_id', '=', $user->id)
            ->where('question_id', '=', $question->id)->first();

        return isset($answer);
    }

    public static function addAnswer($questionId, $choiceId, $user) {
        $rec = \DB::connection('mysql_trans')
            ->select('SELECT SQL_NO_CACHE choice_id FROM answers WHERE question_id =? AND user_id =?',
                [$questionId, $user->id]);
        if (count($rec) > 0) {
            // 回答レコードが存在する場合、回答済みと判断する
            return false;
        }

        \DB::connection('mysql_trans')
            ->insert('INSERT INTO answers (question_id, user_id, choice_id, age, gender, local, device) ' .
            ' VALUES (?, ?, ?, ?, ?, ?, ?) ',
            [
                $questionId, $user->id, $choiceId,
                (is_null($user->attrList[ATTR_BIRTHDAY]) ? ATTRIBUTE_VALUE_NOT_SET : $user->attrList[ATTR_BIRTHDAY]),
                (is_null($user->attrList[ATTR_GENDER]) ? ATTRIBUTE_VALUE_NOT_SET : $user->attrList[ATTR_GENDER]),
                (is_null($user->attrList[ATTR_LOCAL]) ? ATTRIBUTE_VALUE_NOT_SET : $user->attrList[ATTR_LOCAL]),
                $user->getDevice()
            ]);

        return true;
    }
}
