<!DOCTYPE HTML>
<html lang="ja">
    <head>
        <meta http-equiv="X-FRAME-OPTIONS" content="DENY" />
        @include('layouts.elements.partials.header')
        @yield('head')
        @yield('script')
        @yield('style')
        <script type="text/javascript">
            @yield('javascript')
            if (window.history && window.history.replaceState) {
                if (document.location.search.indexOf('q=') >= 0) {
                    window.history.replaceState(document.title, null, document.location.href.replace(document.location.search, ''));
                }
            }
        </script>
        <style type="text/css">
            @yield('css')
        </style>
    </head>
    <body>
        <header>
	        <h1>みんなの声　管理画面</h1>
	        <div class="headerInfo">
	            <span class="loginName">
                    {{ (Auth::check()) ? Auth::user()->account_name : "" }}でログインしています。
                </span>
                <button type="button" name="logoutBtn" class="logoutBtn">
                    ログアウト
                </button>
            </div><!--/headerInfo-->
        </header>
        <div id="container">
        	@yield('link')
            <div id="wrapper">
                @include('layouts.modal.modal')

                @yield('content')
            </div><!--/wrapper-->
        </div><!--/container-->        
    </body>
</html>