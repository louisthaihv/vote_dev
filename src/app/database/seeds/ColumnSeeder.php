<?php
use docomo\Models\Questions\Column;

class ColumnSeeder extends Seeder
{
    public function run()
    {
        DB::table('columns')->truncate();
        
        
        for($i = 1; $i <= 30; $i++) {
            $columns[] = 
                [
                    'question_id'           => $i,
                    'is_visible'            => 1,
                    'column_title'          => 'Column_title_' . $i,
                    'column_detail'         => 'Column_detail_' . $i,
                    'view_create_datetime'  => '2014-12-25 08:47:' . $i,
                    'created_account_id'    => 1,
                    'updated_account_id'    => 1         
                ];
            
        }
        DB::table("columns")->insert($columns);
    }
}