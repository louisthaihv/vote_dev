@if ($question->getKeywords() && $question->getKeywords()->count() > 0)
<div style="background-color:#fff6ab;padding:5px;text-align:left;"><font color="#f39800">▼</font><font color="#3e3a39"> 関連ｷｰﾜｰﾄﾞ </font></div>
@include('layouts.front_elements._space')
<div style="margin:0px 10px 0px 10px;">
    @foreach ($question->getKeywords() as $keyword)
{{ link_to_route('list.tag.' . (($question->isFinished()) ? 'result' : 'entry'), $keyword->keyword_name, ['id' => $keyword->id, 'uid' => 'NULLGWDOCOMO']) }}
    @endforeach
</div>
@include('layouts.front_elements._space')
@endif

<div style="background-color:#fff6ab;padding:5px;text-align:left;"><font color="#f39800">▼</font><font color="#3e3a39"> ｼﾞｬﾝﾙ </font></div>
@include('layouts.front_elements._space')
<div style="margin:0px 10px 0px 10px;">
    <div style="margin:0px 0px 3px 0px;" class="link">{{ link_to_route('list.genre.' . (($question->isFinished()) ? 'result' : 'entry'), $question->childGenre->genre_child_name, ['parent_genre_name' => $question->getParentGenreDirectory(), 'child_genre_name' => $question->getChildGenreDirectory(), 'uid' => 'NULLGWDOCOMO']) }}</div>
</div>

@include('layouts.front_elements._space')
<div style="text-align:center">&#xE6FB;{{ link_to_route('list.unanswered', '未回答のｱﾝｹｰﾄを見る', ['uid' => 'NULLGWDOCOMO']) }}</div>
@include('layouts.front_elements._space')

<div style="background-color:#fff6ab;padding:5px;text-align:left;"><font color="#f39800">▼</font><font color="#3e3a39"> 未回答のｱﾝｹｰﾄ</font> <font color="#f39800">[受付中]</font></div>
@include('layouts.front_elements._space')
<div style="margin:0px 10px 0px 10px;">
@if($sameGenreOngoingQuestions->count() > 0)
    @foreach($sameGenreOngoingQuestions as $question)
        @include('layouts.front_elements._list_question', ['hideAnswer' => true])
    @endforeach
@endif
</div>
