@extends('layouts.admin_in')
@section('javascript')
$(window).load(function(){
    $('.delete').click(function(e){
        return confirm('{{ C_DELETE }}');
    });
});
@stop
@section('content')
<h2>おしらせ一覧</h2>
<p class="m-margin">
<a href="{{ route('manage.notice.create') }}" class="naviBtn">＋おしらせ追加</a>
</p>
<table class="list">
    <tr>
        <th>ID</th><th>タイトル</th><th style="width:20px;">i版</th><th style="width:25px;">d版</th><th style="width:90px;">開始</th><th style="width:90px;">終了</th><th>削除</th>
    </tr>
    @foreach ($notices as $notice)
    <tr class="{{ ($notice->isEnd()) ? 'expiration' : '' }}">
        <td>{{ $notice->id }}</td>
        <td>{{ link_to_route('manage.notice.edit', $notice->title, $notice->id) }}</td>
        <td>{{ ($notice->visible_device_i) ? '○' : '' }}</td>
        <td>{{ ($notice->visible_device_d) ? '○' : '' }}</td>
        <td>{{ date('Y-m-d H:i', strtotime($notice->from))}}</td>
        <td>{{ date('Y-m-d H:i', strtotime($notice->to))}}</td>
        <td>
            {{ Form::open(['url' => '/manage/notice/' . $notice->id, 'method' => 'delete']) }}
                <button type="submit" class="naviBtn delete">削除</button>
            {{ Form::close() }}
        </td>
    </tr>
    @endforeach
</table>
@stop