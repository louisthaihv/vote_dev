@extends('layouts.front')

@section('metaTag')
    <title>特集一覧 | みんなの声</title>
    <meta name="description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！">
    <meta property="og:title" content="特集一覧 | みんなの声" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
    <meta name="twitter:title" content="特集一覧 | みんなの声" />
    <meta name="twitter:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
@stop

@section('script')
    @include('front._shared.js_load_more_pages', ['classNames'=>['featureList'], 'nums' => [NUM_FIRST_PAGE], 'pages' => [PAGER_SIZE_FRONT],'urls' => ['more.list.feature']])
@endsection

@section('content')
    <section class="l-margin">
        <h2 class="featureListTit"><span>特集一覧</span></h2>
<div class="links">
    <ul id="featureList" class="otherList">

    </ul>
</div><!--/lrMargin-->
<a href="" class="featureList moreAction"><span>もっと見る</span></a>
    </section>
    @include('layouts.front_elements._profileBn')
@endsection