<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserAttributes extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_attributes', function($table) {
            $table->dropColumn('attribute_value');
            $table->integer('attribute_value_id')->after('attribute_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_attributes', function($table) {
            $table->dropColumn('attribute_value_id');
            $table->string('attribute_value')->after('attribute_id');
        });
    }

}
