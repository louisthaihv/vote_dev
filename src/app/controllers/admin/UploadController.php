<?php
namespace Admin;

use docomo\Models\Forms\Account\LoginForm;
use docomo\Models\Accounts\AccountLoggedInCommand;
use docomo\Models\Forms\FileUploadForm;
use docomo\Models\Questions\Question;
use Illuminate\Support\Facades\File;

class UploadController extends \BaseController
{
    
    /**
     *
     */
    public function __construct()
    {

    }

    /**
     * [logOut function]
     * @return [Route]
     */
    public function create()
    {

        return \View::make('admin.upload.create',
            array(
                'upload' => new FileUploadForm()
            ));
    }

    public function store()
    {

        $upload = new FileUploadForm();
        if (!$upload->saveImageFile('upload_file', PATH_TO_UPLOAD_FILE)) {

            return \View::make('admin.upload.create',
                array(
                    'upload' => $upload,
                    'errors' => $upload->errors
                ));
        }

        return \View::make('admin.upload.complete',
            array(
                'upload' => $upload
            ));
    }
}
