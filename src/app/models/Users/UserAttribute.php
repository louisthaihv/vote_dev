<?php
namespace docomo\Models\Users;

class UserAttribute extends \BaseArdentModel
{
    protected $primaryKey = 'attribute_id';

     /*
     * Model relationships
     */
     function user(){
         return $this->belongsTo('docomo\Models\Users\User');
     }

     function attributeValues(){
         return $this->belongsTo('docomo\Models\Attributes\AttributeValue');
     }

    /*
     * Override date mutator definition
     */

//    public function getDates()
//    {
//        return ['created_at', 'updated_at', 'view_create_datetime'];
//    }

    /*
     * Acceptable params from input
     */

    /*
     * Validation rules
     */

    /*
     * Custom validation messages
     */


    /*
     * Methods
     */


    /*
     * New or update an user attribute
     */
    public static function updateUserAttr($user_id, $attr_id, $attr_value_id) {

        \DB::delete('DELETE FROM user_attributes WHERE user_id = ? AND attribute_id = ?', [$user_id, $attr_id]);
        \DB::insert('INSERT INTO user_attributes (user_id, attribute_id, attribute_value_id, created_at, updated_at) VALUES (?, ?, ?, NOW(), NOW())',
            [$user_id, $attr_id, $attr_value_id]);
    }

}
