<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeatureQuestionList extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('feature_questions')) {
			Schema::create('feature_questions', function ($table) {
				$table->integer('feature_id')->unsigned();
				$table->integer('question_id')->unsigned();
				$table->primary(['feature_id', 'question_id']);
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
