<?php

namespace Admin;

use docomo\Models\Accounts\Account;
use docomo\Models\Answers\Answer;
use docomo\Models\Answers\ResultVote;
use docomo\Models\Answers\TopSummaryByAttributeValue;
use docomo\Models\Attributes\Attribute;
use docomo\Models\Attributes\AttributeValue;
use docomo\Models\Codes\QuestionViewable;
use docomo\Models\Codes\QuestionStatus;
use docomo\Models\Codes\SummaryType;
use docomo\Models\Forms\DumpConditionForm;
use docomo\Models\Forms\QuestionSearchConditionForm;
use docomo\Models\Forms\ColumnSearchConditionForm;
use docomo\Models\Genres\ParentGenre;
use docomo\Models\Questions\Choice;
use docomo\Models\Questions\Column;
use docomo\Models\Questions\Navigation;
use docomo\Models\Questions\Question;
use docomo\Models\Questions\QuestionTag;
use docomo\Models\Ratings\VoteRatingTotal;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Illuminate\Support\MessageBag;

View::composer([
    'admin.question.index'
], function($view) {
    $view->with('genres', ParentGenre::getListForDDLWithChildren());
    $view->with('status', [DDL_ALL => 'すべて'] + QuestionStatus::$label);
    $view->with('viewable',[DDL_ALL => 'すべて'] + QuestionViewable::$label);
    $view->with('accounts', Account::getListForDDL((Auth::user()->havePermission('is_visible_register')) ? null : Auth::user()->id));
});
View::composer([
    'admin.question.create',
    'admin.question.store',
    'admin.question.edit',
    'admin.question.update',
], function($view) {
    $view->with('summary_type', SummaryType::$label);
    $view->with('genres', ParentGenre::getListForDDLWithChildren(\Auth::user()->allow_genre_parent));
    $view->with('attributes', Attribute::getListForDDLWithChildren());
});
View::composer([
    'admin.question.column'
], function($view) {
    $view->with('accounts', Account::getListForDDL((Auth::user()->havePermission('is_visible_register')) ? null : Auth::user()->id));
});

class QuestionController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (\Input::has('page'))
        {
            $condition = \Session::get('data_search');
        }
        else
        {
            \Session::forget('data_search');
            $condition = new QuestionSearchConditionForm;
            $condition->status = \Input::get('status');
            if (!\Auth::user()->havePermission('is_viewable_all_question')) {
                $condition->register = \Auth::user()->id;
            }
            \Session::put('data_search', $condition);
        }

        return \View::make('admin.question.index', ['conditions' => $condition, 'questions' => $condition->searchQuestionList()]);
    }

    /**
     * [postSearch Process data search and fill data to View]
     * @return [View] [List surveys]
     */
    public function search()
    {
        $condition = QuestionSearchConditionForm::fill();
        \Session::put('data_search', $condition);

        return \View::make('admin.question.index', ['conditions' => $condition, 'questions' => $condition->searchQuestionList()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $question = new Question;
        $choices = [];
        $navigations = Navigation::getList();

        return \View::make('admin.question.create', compact('question', 'choices', 'navigations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        return $this->saveData(MODE_ALL_EDIT);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $question = Question::find($id);
        if (is_null($question)) {
            \App::abort(404);
        }
        $choiceList = Choice::whereQuestionId($id)->withOutOther()->get(['choice_num', 'choice_text', 'summary_value']);
        $navigations = Navigation::getList($id);
        $tagList = QuestionTag::select('keyword_id')->whereQuestionId($id)->get();
        $tags = [];
        if ($tagList) {
            foreach ($tagList as $tag) {
                $tags[] = $tag;
            }
        }
        $maxRow = 0;
        $choices = [];
        foreach ($choiceList as $choice) {
            if ($maxRow < $choice->choice_num) {
                $maxRow = $choice->choice_num;
            }
            $choices[] = $choice;
        }

        $mode = (\Input::get('mode') == MODE_ALL_EDIT && Auth::user()->havePermission('is_manage_agree')) ? MODE_ALL_EDIT : MODE_NORMAL;
        if ($question->status == QUESTION_STATUS_DRAFT || $question->status == QUESTION_STATUS_REJECT) {
            $mode = MODE_ALL_EDIT;
        }

        \Session::forget(QUESTION);
        if ($question->isViewableEditPage()) {
            return \View::make('admin.question.edit', compact('question', 'mode', 'choices', 'navigations', 'tags', 'maxRow'));
        } else {
            return \Redirect::to('manage')->with('message', W_MSG_NOT_PERMISSION);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $question = Question::findOrFail($id);

        if (!$question->isViewableEditPage()) {
            return \Redirect::to('manage')->with('message', W_MSG_NOT_PERMISSION);
        }
        
        if (Input::has('set_status'))
        {
            Question::$rules = [];
            $question = Question::changeStatus($id, Input::get('set_status'));

            return \Redirect::route('manage.question.complete', ['id' => $question->id, 'message_type' => 'change']);
        }
        else
        {
            $mode = ((\Input::get('mode') == MODE_ALL_EDIT && Auth::user()->havePermission('is_manage_agree'))
                || $question->status == QUESTION_STATUS_DRAFT || $question->status == QUESTION_STATUS_REJECT)
                ? MODE_ALL_EDIT : MODE_NORMAL;

            return $this->saveData($mode, $id);
        }
    }

    public function result($id)
    {
        $question = Question::find($id);
        if (is_null($question)) {
            \App::abort(404);
        }

        // column instance for edit
        $edit_column = null;
        if (Auth::user()->havePermission('is_manage_column') &&
            (
                Auth::user()->havePermission('is_viewable_all_question') || $question->account_id === Auth::user()->id))
        {
            $edit_column = $question->column;
            if (!$edit_column) {
                $edit_column = new Column;
            }
        }

        return $this->prepareResultData($question, $edit_column);
    }

    private function prepareResultData($question, $edit_column, $errors = null){
        $showResult = \Session::pull('ShowResult', \Input::get('showresult'));

        if ($showResult) {
            \Session::put('ShowResult', $showResult);
        }

        $attributes = Attribute::getDataByCache();
        $selected = [];
        foreach ($attributes as $attribute) {
            $selected[$attribute->id] = $attribute->getDefaultValue();
        }

        $result_data = ResultVote::getRankingData($question);
        $topSummary = TopSummaryByAttributeValue::getFrontData($question);

        $average = 0;
        if ($question->summary_type == SUMMARY_AVERAGE) {
            $summary_value = 0;
            $vote_count = 0;
            foreach ($result_data[TAB_TOTAL][TAB_TOTAL] as $result) {
                $summary_value += $result['summary_value'] * $result['summary_count'];
                $vote_count += $result['summary_count'];
            }
            $average = intval($summary_value / (($vote_count == 0) ? 1 : $vote_count));
        }
        $totalChoiceList = [NULL => 10, '_rankCss' => ''];
        foreach ($result_data[TAB_TOTAL][TAB_TOTAL] as &$record) {
            $totalChoiceList[$record['choice_num']] = ($record['ranking'] > 10) ? 10 : $record['ranking'];
            $totalChoiceList[$record['choice_num'] . '_rankCss'] = (is_null($record['ranking'])) ? '' : 'rank' . (($record['ranking'] > 10) ? 10 : $record['ranking']);
        }
        unset($record);

        $myAnswer = new Answer();

        $voteRatingTotals = $question->voteRatingTotals()->orderBy('type')->get();

        $column = $question->getColumn();

        return \View::make('admin.question.result', compact(
            'question',
            'column',
            'edit_column',
            'myAnswer',
            'voteRatingTotals',
            'average',
            'result_data',
            'attributes',
            'topSummary',
            'selected',
            'errors',
            'parent_genre_name',
            'child_genre_name',
            'totalChoiceList'
        ));

    }

    public function csv($id, $attr, $device = null)
    {
        $question = Question::select('id', 'title')->find($id);
        $choices = $question->choices()->orderBy('id')->get();
        $rowHeader = AttributeValue::select('id', 'attribute_value_name')->where('attribute_id', '=', $attr)
            ->orderBy('sort_order')
            ->get();

        /*
         * load aggregated data
         */
        $data = ResultVote::loadRankingDataCsv($question, $attr, $device, $rowHeader, $choices);

        /*
         * Prepare for CSV data
         */
        $csvData = [];

        // first row: title
        array_push($csvData, [$question->title]);

        // third row: header
        $headerArray = [''];
        foreach($choices as $choice){
            array_push($headerArray, $choice->choice_text);
        }
        array_push($headerArray, '合計');

        array_push($csvData, $headerArray);

        // aggregate counts
        if($attr){
            // by attribute
            $rowIndex = 0;
            $nowAttr = $rowHeader[$rowIndex];
            $cntData = [];
            foreach($data as &$row){
                if($nowAttr->id != $row->attribute_value_id){
                    array_push($csvData, $this->generateRowData($cntData, $nowAttr->attribute_value_name));

                    $cntData = [];
                    $rowIndex ++;
                    $nowAttr = $rowHeader[$rowIndex];
                }

                array_push($cntData, $row->cnt);
            }

            array_push($csvData, $this->generateRowData($cntData, $nowAttr->attribute_value_name));

        }else{
            // total
            $cntData = [];

            foreach($data as &$row){
                array_push($cntData, $row->cnt);
            }
            array_push($csvData, $this->generateRowData($cntData, '総合'));
        }

        /*
         * Output CSV
         */
        //$stream = fopen('php://temp', 'w');
        $stream = tmpfile();

        foreach($csvData as $row){
            fputcsv($stream, $row);
        }

        /*
         * Convert encoding
         */
        rewind($stream);
        $csv = str_replace(PHP_EOL, "\r\n", stream_get_contents($stream));
        $csvConv = mb_convert_encoding($csv, 'SJIS-win', 'UTF-8');

        if($attr){
            $attr = Attribute::whereId($attr)->first();
            $fileNameAttr = "_" . $attr->attribute_directory;
        }else{
            $fileNameAttr = "_total";
        }

        if($device == TYPE_D_MENU){
            $fileNameDevice = "_d";
        }elseif($device == TYPE_I_MODE){
            $fileNameDevice = "_i";
        }else{
            $fileNameDevice = "";
        }

        $fileName = $question->id . $fileNameAttr . $fileNameDevice . ".csv";

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="' . $fileName . '"',
        ];

        fclose($stream);
        return Response::make($csvConv, 200, $headers);
    }

    private function generateRowData($cntData, $nowAttrName)
    {
        $rowData = [];

        array_push($rowData, $nowAttrName);
        foreach($cntData as $cnt){
            array_push($rowData, $cnt);
        }
        array_push($rowData, array_sum($cntData));

        return $rowData;
    }

    /**
     * Update the specified column in storage.
     *
     * @param  int  $id -> question_id
     * @return Response
     */
    public function updateColumn($id)
    {
        $column = Column::fillWithModify($id);

        if ($column->save()) {
            Question::setColumnFlg($id, $column->is_visible);

            return Redirect::route('manage.question.result', ['id' => $id]);
        } else {
            $errors = $column->errors();
            $question = Question::find($id);

            return $this->prepareResultData($question, $column, $errors);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $question = Question::find($id);
        if (!$question->isEnabledDelete())
        {
            \APP::abort(403, 'Unauthorized action.');
        }

        Question::deleteData($id);

        return \Redirect::route('manage.question.index');
    }

    public function complete($question_id, $message_type = null)
    {
        $status = Question::whereId($question_id)->max('status');

        return \View::make('admin.question.complete', compact('status', 'question_id', 'message_type'));
    }

    /**
     * Search column.
     *
     * @param
     * @return Response
     */
    public function searchColumn() {
        $sessionName = 'column_search';
        if (\Input::has('page'))
        {
            $condition = \Session::get($sessionName);
        }
        else
        {
            \Session::forget($sessionName);
            $condition = ColumnSearchConditionForm::fill();
            \Session::put($sessionName, $condition);
        }

        $visible = [
            DDL_ALL => 'すべて',
            VISIBLE => 'チェック済',
            HIDDEN => '未チェック'
        ];

        return \View::make('admin.question.column', ['conditions' => $condition, 'columns' => $condition->searchColumnList(), 'visible' => $visible]);
    }

    /**
     * create and download dump file.
     *
     * @return Response
     */
    public function downloadDump() {

        $dumpForm = new DumpConditionForm();
        $dumpForm->createQuestionDump();

        $path = public_path(PATH_TO_DUMP_PATH . $dumpForm->fileName);
        $per_size = 1024 * 1024;

        global $is_IE;if($is_IE){
            header("Cache-Control: public");
            header("Pragma:");
        }
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($path).'"');
        header("Content-Length: ".filesize($path));

        ob_end_flush();
        ob_start('mb_output_handler');
        $handle = fopen($path, "r");
        while(!feof($handle)){
            //指定したバイト数だけ出力
            $bytes = fread($handle, $per_size);
            echo $bytes;
            //出力
            ob_flush();
            flush();
            //1秒休む
            sleep(1);
        }
        fclose($handle);
    }

    private function saveData($mode, $id = null)
    {
        $question = Question::fillWithModify($mode, $id);

        \DB::beginTransaction();
        $result = $question->save();
        $errors = new MessageBag();
        $choices = Choice::fillWithModify($question);
        $navigations = Navigation::fillWithModify($question, \Session::pull(QUESTION, null));
        $tags = QuestionTag::fillWithModify($question->id);
        if ($mode == MODE_ALL_EDIT) {

            $choice_errors = Choice::validateAndSave($result, $choices, $question->summary_type);
            if ($choice_errors->count() > 0) {
                $result = false;
                $errors->merge($choice_errors);
            }
        }
        if ($result) {
            foreach ($navigations[TYPE_D_MENU] as $navigation) {
                if (!$navigation->saveRecord()) {
                    $result = false;
                    $errors->merge($navigation->errors());
                }
            }
            foreach ($navigations[TYPE_I_MODE] as $navigation) {
                if (!$navigation->saveRecord()) {
                    $result = false;
                    $errors->merge($navigation->errors());
                }
            }
            if (isset($tags)) {
                foreach ($tags as $tag) {
                    if (!$tag->save()) {
                        $result = false;
                        $errors->merge($tag->errors());
                    }
                }
            }
        } else {
            $errors->merge($question->errors());
        }

        if ($result) {
            ResultVote::createAllRecord($question);
            VoteRatingTotal::createAllRecord($question->id);
            TopSummaryByAttributeValue::createAllRecord($question);
            if ($mode == MODE_ALL_EDIT) {
                $data = \docomo\Models\Answers\ResultVote::select('id', 'attribute_id', 'attribute_value_id', 'device',
                    'choice_id', 'voted_count', 'summary_value')
                    ->whereQuestionId($question->id)
                    ->orderBy('attribute_id')->orderBy('attribute_value_id')->orderBy('choice_id')->get()->toArray();
                $topData = \docomo\Models\Answers\ResultVote::sortByData($data, $question->id);
                TopSummaryByAttributeValue::updateTopByArray($question->id, $topData);
            }
            \DB::commit();
            \Session::forget(QUESTION);

            return \Redirect::route('manage.question.complete', ['id' => $question->id, 'message_type' => (\Input::get('status') == 0) ? 'overwrite' : '']);
        } else {
            \DB::rollback();
            \Session::put(QUESTION, $navigations);
            $edit_choices = [];
            foreach ($choices as $choice) {
                if ($choice->choice_num !== OTHER_CHOICE_NUM) {
                    $edit_choices[] = $choice;
                }
            }
            $choices = $edit_choices;
            unset($edit_choices);
            if ($id === null) {
                $question->id = null;
                unset($question->status);

                return \View::make('admin.question.create', compact('question', 'choices', 'navigations', 'errors', 'tags'));
            } else {
                $question->status = Question::select('status')->find($id)->status;

                return \View::make('admin.question.edit', compact('question', 'mode', 'choices', 'navigations', 'errors', 'tags'));
            }
        }
    }
}
