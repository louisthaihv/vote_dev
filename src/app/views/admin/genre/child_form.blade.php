@section('style')
    <style type="text/css">
        .not_visible {
            display : none;
        }
    </style>
@endsection


<p class="boldFont">追加</p>
@include('admin.share.errors', ['errorList' => $add_child->errors()])
{{ Form::model($add_child, ['url' => $url, 'method' => 'put']) }}
    <table class="list">
        <tr>
            <th>小ジャンル名<span class="require">※</span></th><th>ディレクトリ名<span class="require">※</span></th><th>説明<span class="require">※</span></th><th>表示順</th><th>非表示</th><th>&nbsp;</th>
        </tr>
        <tr>
            <td>{{ Form::text('genre_child_name', null, ['class' => 'large']) }}</td>
            <td>{{ Form::text('genre_child_directory', null, ['class' => 'x-small']) }}</td>
            <td>{{ Form::text('genre_child_description', null, ['class' => 'small']) }}</td>
            <td>{{ Form::text('child_sort_order', null, ['class' => 'xx-small']) }}</td>
            <td>{{ Form::checkboxForHidden('child_is_visible', VISIBLE) }}</td>
            <td><button type="submit" class="commitBtn" id="addChildGenreBtn">追加</button></td>
        </tr>
    </table>
{{ Form::hidden('add', true) }}
{{ Form::close() }}
<br /><br />
@include('admin.share.errors', ['errorList' => $errors])
{{ Form::model($children, ['url' => $url, 'method' => 'put']) }}
    <label><input type="checkbox" name="hide_show" id="hide_show" {{ $show_all ? 'checked' : '' }}/> 非表示も見る</label> <a class="naviBtn" id="searchBtn" />再検索</a>
    <table class="list">
        <tr>
            <th>小ジャンル名<span class="require">※</span></th><th>ディレクトリ名<span class="require">※</span></th><th>説明<span class="require">※</span></th><th>表示順</th><th>非表示</th>
        </tr>
        @foreach ($children as $child)
            <tr>
                <td>{{ Form::text('genre_child_name_' . $child->id, $child->genre_child_name, ['class' => 'large']) }}</td>
                <td>{{ Form::text('genre_child_directory_' . $child->id, $child->genre_child_directory, ['class' => 'x-small']) }}</td>
                <td>{{ Form::text('genre_child_description_' . $child->id, $child->genre_child_description, ['class' => 'small']) }}</td>
                <td>{{ Form::text('child_sort_order_' . $child->id, $child->child_sort_order, ['class' => 'xx-small']) }}</td>
                <td><input type="checkbox" name="child_is_visible_{{ $child->id }}" {{ ($child->child_is_visible == VISIBLE) ? '' : 'checked="checked"' }} /></td>
            </tr>
        @endforeach
    </table>
    <div class="tcenter m-margint">
        <button type="submit" name="commit" class="commitBtn" id="editChildGenreBtn">更新</button>
    </div>
{{ Form::close() }}
