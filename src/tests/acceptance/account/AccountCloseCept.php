<?php
$I = new AcceptanceTester($scenario);
$I->amOnPage('/manage');
$I->loginWithDetail(['admin', '123456']);
$I->click('アカウント一覧・登録・修正・削除');
$I->checkCloseAccount();
$I->checkOpenAccount();
