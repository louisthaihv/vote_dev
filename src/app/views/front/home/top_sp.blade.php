@extends('layouts.front_in')

@section('metaTag')
    <title>みんなの声</title>
    <meta name="description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！">
    <meta property="og:title" content="TOPページ | みんなの声" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
    <meta name="twitter:title" content="TOPページ | みんなの声" />
    <meta name="twitter:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
@stop

@section('script')
    {{ HTML::script('common/front/js/tab_change.js') }}
@stop

@section('before_tagmanager')
<script>
    dataLayer = [{'activetab' : 'entry'}];
</script>
@stop

@section('content')
    @if ($feature)
        @include('layouts.elements.displays.feature_box', ['displaySeeMore' => FALSE, 'idHeader' => 'top_f_votelist'])
        @if ($past_features > 0)
            <p class="gotoListArea"><a id="top_f_votelistmore" href="{{ route('feature') }}">特集をもっと見る({{ $past_features}})</a></p>
        @endif
    @endif

    @if (false)
        <section class="l-margin">
            <div class="search">
                <p class="redFont" style="display:none">{{ W_FRM_REQUIRED_KEYWORD }}</p>
                <form method="GET" action="{{ route('search.result') }}">
                    <ul>
                        <li class="search-key"><input type="text" name="head_search_key" placeholder="キーワードを入力"></li>
                        <li class="search-bt"><input id="head_search_key" class="submit" type="button" name="" value="検索"></li>
                    </ul>
                </form>
            </div><!--search-->
        </section>
    @endif

    <section class="entryResultBox">
        <div class="box-tabs">
            <ul class="tabs">
                <li class="active"><a id="clickcount-tab1-entry" href="#"><span class="iconEntry">受付中</span></a></li>
                <li class=""><a id="clickcount-tab1-result" href="#"><span class="iconResult">結果発表</span></a></li>
            </ul>
        </div><!--box-tabs-->

        <div class="tabs_content">
            <div class="entryBox">
                @if ($pickups[PROCESSING]->count() == 0 &&
                    (!array_key_exists(SUMMARY_TYPE_NEWER_ENTRY, $summaryData) || $summaryData[SUMMARY_TYPE_NEWER_ENTRY]->count()) === 0 || count($summaryData[SUMMARY_TYPE_NEWER_ENTRY]) === 0 &&
                    !$limited)
                <div class="entryEmptyMsg">みんなの声をお使いいただきありがとうございます！<br />
                    ただいま回答できるアンケートはありません。<br />
                    新しいアンケートを準備中ですので、配信をお待ちください。<br />
                    追加したアンケートは公式twitterやfacebookでもつぶやきます。<br />
                    是非フォローをお願いします！<br />
                    <p class="socialLink"><a href="https://twitter.com/_minnanokoe_/"><img src="/common/front/images/sp/btn_twitter.png" alt="twitter"></a> <a href="https://www.facebook.com/minnanokoe.vote"><img src="/common/front/images/sp/btn_facebook.png" alt="facebook"></a></p></div>
                @else
<div class="box">
                    @if ($limited)
<a id="top_vote_limit" href="{{ $limited->getPath() }}">
    <div class="pushContents">
        <h3>{{ $limited->getLimitedValueName() }}の方へのアンケート募集中！</h3>
        <div class="pushInfo">
            <p class="pushText">{{ $limited->title }}</p>
        </div>
    </div><!--pushContents-->
</a>
                    @endif
</div><!--box-->
                    @if ($pickups[PROCESSING]->count() > 0)
<h3 class="listSectionTit"><span>ピックアップ</span></h3>
<div class="links">
    <ul>
        <?php $idx = 1; ?>
                        @foreach($pickups[PROCESSING] as $rec)
                            @include('layouts.elements.displays.list_box', ['question' => $rec, 'hide_answer' => true, 'linkId' => 'top_vote_pickup' . sprintf('%1$02d', $idx++)])
                        @endforeach
    </ul>
</div>
                    @endif

<h3 class="listSectionTit"><span>新着の受付中アンケート</span></h3>
                    <!-- List accept Entry -->
                    @include('front.home.sp_question_list', ['listKeyName' => SUMMARY_TYPE_NEWER_ENTRY, 'idHeader' => 'top_vote_new', 'hide_answer' => true])
                    @include('front._shared._link_to_list', ['text' => 'もっと見る', 'route' => 'list.unanswered', 'linkId' =>  'top_vote_newmore'])
                @endif
            </div><!--entryBox-->

            <div class="resultBox" style="display: none;">
                <!-- none -->
                <div class="box">

                    @if (!is_null($attributeFavorite))
                    <a id="top_result_limit" href="{{ $attributeFavorite->getPostPath() }}">
                        <div class="pushContents">
                            <h3>{{ $user->getAttributeValueLabel(ATTR_BIRTHDAY) }}{{ $user->getAttributeValueLabel(ATTR_GENDER) }}が興味あるアンケートは？</h3>
                            <div class="pushInfo">
                                <p class="pushText">{{ $attributeFavorite->title }}</p>
                            </div>
                        </div><!--pushContents-->
                    </a>
                    @endif
                </div><!--box-->

                @if ($pickups[COMPLETE]->count() > 0)
                    <h3 class="listSectionTit"><span>ピックアップ</span></h3>
                    <div class="links">
                        <ul>
                            <?php $idx = 1; ?>
                            @foreach($pickups[COMPLETE] as $rec)
                                @include('layouts.elements.displays.list_box', ['question' => $rec, 'hide_answer' => true, 'linkId' => 'top_result_pickup' . sprintf('%1$02d', $idx++)])
                            @endforeach
                        </ul>
                    </div>
                @endif

                <h3 class="listSectionTit"><span>新着のアンケート結果</span></h3>
                @include('front.home.sp_question_list', ['listKeyName' => SUMMARY_TYPE_NEWER_RESULT, 'idHeader' => 'top_result_new'])
                @include('front._shared._link_to_list', ['text' => 'もっと見る', 'route' => 'list.result', 'linkId' => 'top_result_newmore'])
            </div><!--resultBox-->

        </div><!--tab_content-->


    </section><!--entryResultBox-->

    @include('layouts.front_elements._profileBn', ['linkId' => 'top_prof'])

    <section class="l-margin">
        @include('layouts.front_elements.body_banner')
    </section>

    <section class="l-margin">
        <h3 class="subSectionTit evaluateIcon">みんなのアンケート評価</h3>

        <ul class="tabs02">
            <li class="active"><a id="clickcount-tab2-gatten" href="#">{{ HTML::image(\docomo\Models\Codes\EvaluationType::$sp_images[SUMMARY_TYPE_EVALUATION_GATTEN]) }}</a></li>
            <li><a id="clickcount-tab2-maji" href="#">{{ HTML::image(\docomo\Models\Codes\EvaluationType::$sp_images[SUMMARY_TYPE_EVALUATION_MAJI]) }}</a></li>
            <li><a id="clickcount-tab2-memomemo" href="#">{{ HTML::image(\docomo\Models\Codes\EvaluationType::$sp_images[SUMMARY_TYPE_EVALUATION_MEMO]) }}</a></li>
        </ul>

        <div class="tabs_content02">
            <?php
                $evalPrefix = [
                    SUMMARY_TYPE_EVALUATION_GATTEN => ['top_ev_gatten', 1],
                    SUMMARY_TYPE_EVALUATION_MAJI => ['top_ev_maji', 1],
                    SUMMARY_TYPE_EVALUATION_MEMO => ['top_ev_memo', 1]
                ];
            ?>
            @foreach ([SUMMARY_TYPE_EVALUATION_GATTEN, SUMMARY_TYPE_EVALUATION_MAJI, SUMMARY_TYPE_EVALUATION_MEMO] as $evaluation)
                <div class="links noBorder">
                    @if (array_key_exists($evaluation, $summaryData))
                    <ul>
                        @foreach($summaryData[$evaluation] as &$rec)
                            @include('layouts.elements.displays.list_box', ['question' => $rec, 'show_entry_tag' => true, 'linkId' => $evalPrefix[$evaluation][0] . sprintf('%1$02d', $evalPrefix[$evaluation][1]++)])
                        @endforeach
                    </ul>
                    @endif
                </div><!--links-->
            @endforeach
        </div>

    </section>

    <!-- Section column-list -->

    <section class="l-margin">
        <h3 class="subSectionTit columnIcon">コラム</h3>
        <div class="links">
        <ul>
        @if ($columns->count() > 0)
            <?php $idx = 1; ?>
            @foreach ($columns as $rec)
            <li>
                <a id="top_column{{ sprintf('%1$02d', $idx++) }}" href="{{ $rec->getColumnPath()  }}">
                    <span class="text">{{ $rec->column->column_title }}</span>
                    <span class="date">{{ jpDate($rec->column->view_create_datetime, 'Y年m月d日 H:i') }}</span>
                </a>
            </li>
            @endforeach
        @else
        @endif
        </ul>
        </div>
        @include('front._shared._link_to_list', ['text' => 'コラムをもっと見る', 'route' => 'list.column', 'linkId' => 'top_columnmore'])
    </section>

    <script type="text/javascript">
        $(document).ready(function() {
 
            $("ul.tabs").tabChange({content: ".tabs_content", animate: true});
            $("ul.tabs02").tabChange({content: ".tabs_content02", animate: true});
        });
    </script>

@stop
