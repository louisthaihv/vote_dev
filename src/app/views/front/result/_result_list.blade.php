
@foreach($list as $elem)
    @if ($elem->attribute_value_id != 0)
<tr id="attribute_value_{{ $elem->attribute_value_id }}" rankingcount="{{ $elem->total_vote }}" labelname="{{ $elem->attribute_value_name }}" class="pushStateAction {{ ($elem->attribute_value_id == $default_selected->id) ? 'active' : '' }}" url="/{{$attribute_directory . '/' . $elem->attribute_value_directory }}">
    <td class="attribute">
        {{ $elem->attribute_value_name }}
        {{ link_to_route('async.result.show_ranking', '', [
                    $question->getParentGenreDirectory(),
                    $question->getChildGenreDirectory(),
                    $question->id,
                    $elem->attribute_id,
                    'attrv' => $elem->attribute_value_id
        ], [
                    'style' => 'display:none;',
                    'class' => 'reloadRanking'
        ])}}
    </td>
    <td class="rank"><span class="rankNo1">1</span></td>
    <td>{{ $elem->choice_text }}</td>
    @if ($question->isViewableAverage())
    <td class="avgValue">平均：{{ (is_null($elem->average_num)) ? '' : number_format(intval($elem->average_num)) . $question->average_unit }}</td>
    @else
    <td> </td>
    @endif
</tr>
@endif
@endforeach