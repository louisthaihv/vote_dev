@extends('layouts.admin_in')
@section('head')
<link rel="stylesheet" href="{{ url('common/admin/css/jquery-ui-1.8.23.custom.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ url('common/front/css/feature.css') }}" type="text/css" />
<script type="text/javascript" src=" {{ url('common/admin/js/jquery-ui-1.8.23.custom.min.js') }}"></script>
<script type="text/javascript" src=" {{ url('common/admin/js/jquery.ui.datepicker-ja.js') }}"></script>
@stop
@section('script')
<script type="text/javascript">
$(window).load(function(){
    var confirmMsg = '{{ (empty($feature->id)) ? C_REGISTRATION : C_UPDATE }}';
    $('.entryBtn').click(function(){
        return confirm(confirmMsg);
    });
    $('#reEditBtn').click(function(){
        window.location = '{{ (is_null($feature->id)) ? '/manage/feature/create' : '/manage/feature/' . $feature->id . '/edit' }}';
    });
});
</script>
@stop
@section('style')
<style>
div.imageBox{
    display:inline-block;
    background-color:#ccc;
    color:#333;
    text-align:center;
    position:relative;
}
div.imageBox span{
    padding:10px;
    display:inline-block;
}
.noticeBox{

}
.noticeTitle{
    background-color:#666;
    padding:5px 10px;
    color:#fff;
    margin-bottom:10px;
    font-size:16px;
}
ul.voteList{
    border-top:1px dotted #ccc;
}
ul.voteList li{
    border-bottom:1px dotted #ccc;
    background:url({{ url('common/admin/images/arrow.png') }}) no-repeat right center;
}
ul.voteList li a{
    padding:10px 20px 10px 0px;
    display:block;
    text-decoration:none;
    color:#333;
}
ul.voteList li a span.genre{
    display:inline-block;
    background-color:#333;
    color:#fff;
    font-size:12px;
    padding:2px 5px;
}
ul.voteList li a span.voteTit{
    display:block;
    padding:2px 0px;
}
ul.voteList li a span.voteDate{
    display:block;
    padding:2px 0px;
    color:#999;
    font-size:10px;
}   
</style>
@stop
@section('content')
<h2>TOPページ特集枠データ作成</h2>
{{ Form::model($feature, (is_null($feature->id)) ? ['url' => 'manage/feature/create', 'method' => 'post'] : ['url' => 'manage/feature/' . $feature->id . '/edit', 'method' => 'put']) }}
<p>イメージ</p>
<div style="width:320px;margin-left:auto;margin-right:auto;">
    @include('layouts.elements.displays.feature_box')
</div>
<br /><br />
<p>期間：{{ $feature->from }}&nbsp;&nbsp;～&nbsp;&nbsp;{{ $feature->to }}</p>
<div class="tcenter m-margint">
<button type="button" name="commit" class="commitBtn" id="reEditBtn">編集しなおす</button>
<button type="submit" name="commit" class="commitBtn entryBtn">登録</button>
</div>

{{ Form::close() }}
@stop