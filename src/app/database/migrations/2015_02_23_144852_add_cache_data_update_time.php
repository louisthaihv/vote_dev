<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCacheDataUpdateTime extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('data_update_time')) {
            Schema::create('data_update_time', function ($table) {
                $table->string('cache_key', 30);
                $table->datetime('update_time');
                $table->primary('cache_key');
            });
            \DB::insert('INSERT INTO data_update_time VALUES(?, NOW())', [\docomo\Models\Genres\ParentGenre::$cacheName]);
            \DB::insert('INSERT INTO data_update_time VALUES(?, NOW())', [\docomo\Models\Genres\ChildGenre::$cacheName]);
            \DB::insert('INSERT INTO data_update_time VALUES(?, NOW())', [\docomo\Models\Accounts\Account::$cacheName]);
            \DB::insert('INSERT INTO data_update_time VALUES(?, NOW())', [\docomo\Models\Attributes\Attribute::$cacheName]);
            \DB::insert('INSERT INTO data_update_time VALUES(?, NOW())', [\docomo\Models\Attributes\AttributeValue::$cacheName]);
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
