(function($){
    $.fn.tabChange = function(options) {
        var defaults = {
            content: "div.content",
            animate: true,
            spped:400
        };
        var options = $.extend(defaults, options);

        return this.each(function() {
            // object is the selected pagination element list
            var obj = $(this);
            
            var objTabs = $(options.content);
            var number_of_items = obj.children("li").size();
            var initialNumber = $('li',obj).index($('li.active:eq(0)',obj)) + 1;
            var tabIndex = [];
            var tabs = [];
            
            
            // create array of tab index items
            for (i=1;i<=number_of_items;i++) { tabIndex[i] = obj.find("li:nth-child("+i+")"); }
            
            // create array tabs
            for (i=1;i<=number_of_items;i++) { tabs[i] = $(options.content + "> div:nth-child("+i+")"); }
            
            // initiate the current tab
            
            if (initialNumber > 0 && initialNumber <= number_of_items) {
                showTab(initialNumber);
            } else {
                showTab(1);
            }
            
            function showTab(num) {
                tabIndex[num].addClass("active").siblings().removeClass("active");
                if(!options.animate) {
                    tabs[num].show().siblings().hide();
                }else {
                    tabs[num].fadeIn(options.speed).siblings().hide();
                }
            }

            obj.find("li").live("click", function(e){
                e.preventDefault();
                var tab_num = $('li',obj).index(this) + 1;
                showTab(tab_num);
            });

        });
        
       
    };
})(jQuery);