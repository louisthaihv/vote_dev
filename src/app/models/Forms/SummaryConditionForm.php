<?php
namespace docomo\Models\Forms;

use docomo\Models\Questions\Question;
use docomo\Models\Summary\DailyProfile;
use docomo\Models\Summary\DailyVote;
use docomo\Models\Summary\DailyVoteAttribute;
use docomo\Models\Summary\DailyVoteRanking;
use docomo\Models\Summary\SummaryAttribute;
use Illuminate\Support\Facades\Input;

class SummaryConditionForm
{
    public $genre = DDL_ALL;
    public $date_from = '';
    public $date_to = '';

    public $sort_column = 'vote';
    public $sort_order = 'desc';

    public function __construct()
    {
        $firstDate = date_format(new \DateTime(), 'Ym') . '01';
        $lastDate = date('Ymd', strtotime($firstDate . ' +1 months -1 day'));
        $this->date_from    = $firstDate;
        $this->date_to      = $lastDate;
    }

    public static function fill()
    {
        $condition = new SummaryConditionForm;
        $condition->genre       = Input::get('genre', DDL_ALL);
        $condition->date_from   = Input::get('date_from', $condition->date_from);
        $condition->date_to     = Input::get('date_to', $condition->date_to);

        $condition->sort_column = Input::get('sortColumn', 'vote');
        $condition->sort_order  = Input::get('sortOrder', 'desc');

        return $condition;
    }

    public function getDailyVote()
    {
        if (strlen($this->date_from) != 8 || !strptime($this->date_from, '%Y%m%d')
            ||
            strlen($this->date_to) != 8 || !strptime($this->date_to, '%Y%m%d')) {
            return null;
        }

        return DailyVote::whereRaw('`summary_date` BETWEEN ? AND ? ',
            [$this->date_from, $this->date_to])
            ->get();
    }

    public function getVoteRanking()
    {
        if (strlen($this->date_from) != 8 || !strptime($this->date_from, '%Y%m%d')
            ||
            strlen($this->date_to) != 8 || !strptime($this->date_to, '%Y%m%d')) {
            return null;
        }

        $data = DailyVoteRanking::
            selectRaw('`question_id`, SUM(`total`) as total')
            ->whereRaw(' `summary_date` BETWEEN ? AND ? ', [$this->date_from, $this->date_to])
            ->groupBy('question_id');
        if ($this->sort_column == 'vote') {
            if ($this->sort_order == 'asc') {
                $data = $data->orderBy('total', 'asc');
            } else {
                $data = $data->orderBy('total', 'desc');
            }
        }
        $data = $data->get();
        $ids = $data->lists('question_id');
        if (!isset($ids) || count($ids) === 0) {
            $ids = [0];
        }
        $questions = Question::select('id', 'title', 'status', 'genre_parent_id', 'vote_date_from', 'vote_date_to', 'attribute_value_id', 'viewable_type', 'genre_child_id', 'account_id')
            ->whereIn('id', $ids);

        if ($this->genre != DDL_ALL) {
            if (strstr($this->genre, 'parent_')) {
                $questions = $questions->whereGenreParentId(str_replace('parent_', '', $this->genre));
            } else {
                $questions = $questions->whereGenreChildId($this->genre);
            }
        }
        if ($this->sort_column == 'from') {
            if ($this->sort_order == 'asc') {
                $questions = $questions->orderDateFromAsc();
            } else {
                $questions = $questions->orderDateFromDesc();
            }
        }
        if ($this->sort_column == 'to') {
            if ($this->sort_order == 'asc') {
                $questions = $questions->orderDateToAsc();
            } else {
                $questions = $questions->orderDateToDesc();
            }
        }

        $questions = $questions
            ->orderBy(\DB::raw('FIELD(id, ' . implode(',', $ids) . ')'))
            ->get();
        unset($ids);

        $voteMap = [];
        foreach ($data as &$record) {
            $voteMap[$record->question_id] = $record->total;
            unset($record);
        }
        unset($data);

        foreach ($questions as &$record) {
            $record->vote_count = $voteMap[$record->id];
        }

        return $questions;
    }

    public function getVoteGenre()
    {
        if (strlen($this->date_from) != 8 || !strptime($this->date_from, '%Y%m%d')
            ||
            strlen($this->date_to) != 8 || !strptime($this->date_to, '%Y%m%d')) {
            return null;
        }

        $data = DailyVoteRanking::
        selectRaw('`genre_parent_id`, `genre_child_id`, SUM(`total`) as total')
            ->whereRaw(' `summary_date` BETWEEN ? AND ? ', [$this->date_from, $this->date_to])
            ->groupBy('genre_parent_id')->groupBy('genre_child_id')
            ->orderBy('genre_parent_id')->orderBy('genre_child_id');
        $data = $data->get();

        return $data;
    }

    public function getVoteAttribute()
    {
        if (strlen($this->date_from) != 8 || !strptime($this->date_from, '%Y%m%d')
            ||
            strlen($this->date_to) != 8 || !strptime($this->date_to, '%Y%m%d')) {
            return null;
        }

        $data = DailyVoteAttribute::
        selectRaw('`attribute_id`, `attribute_value_id`, SUM(`total`) as total')
            ->whereRaw(' `summary_date` BETWEEN ? AND ? ', [$this->date_from, $this->date_to])
            ->groupBy('attribute_id')->groupBy('attribute_value_id')
            ->orderBy('attribute_id')->orderBy('attribute_value_id');
        $data = $data->get();


        return $data;
    }

    public function getDailyProfile()
    {
        if (strlen($this->date_from) != 8 || !strptime($this->date_from, '%Y%m%d')
            ||
            strlen($this->date_to) != 8 || !strptime($this->date_to, '%Y%m%d')) {
            return null;
        }

        return DailyProfile::whereRaw('`summary_date` BETWEEN ? AND ? ',
            [$this->date_from, $this->date_to])
            ->get();
    }

    public static function getProfile()
    {
        $data = SummaryAttribute::orderBy('attribute_id')->orderBy('attribute_value_id')->get();
        $summaryData = [];
        foreach ($data as &$record) {
            if (!array_key_exists($record->attribute_id, $summaryData)) {
                $summaryData[$record->attribute_id] = [];
            }
            $summaryData[$record->attribute_id][$record->attribute_value_id] = $record->registered;
        }

        return $summaryData;
    }
}
