@extends('layouts.admin_in')

@section('head')
<link rel="stylesheet" href="{{ url('common/admin/css/jquery-ui-1.8.23.custom.css') }}" type="text/css" />
<script type="text/javascript" src=" {{ url('common/admin/js/jquery-ui-1.8.23.custom.min.js') }}"></script>
<script type="text/javascript" src=" {{ url('common/admin/js/jquery.ui.datepicker-ja.js') }}"></script>
@stop

@section('javascript')
$(window).load(function(){
    $('a#searchBtn').click(function(){
        $("table.list tr:gt(0)").hide();
        $("table.list tr[searchid="+$('select[name="genreSelect"]').val()+"]").show();
        return false;
    });

    //日付ピッカー
    $("input[name='date_from_start']").datepicker({
        dateFormat: 'yymmdd'
    });
    //日付ピッカー
    $("input[name='date_from_end']").datepicker({
        dateFormat: 'yymmdd'
    });
    //日付ピッカー
    $("input[name='date_to_start']").datepicker({
        dateFormat: 'yymmdd'
    });
    //日付ピッカー
    $("input[name='date_to_end']").datepicker({
        dateFormat: 'yymmdd'
    });
    $('#keywordListOpen').click(function(){
        window.open('{{route("manage.keyword.pick")}}/1', 'keywordwindow', 'width=400, height=300, menubar=no, toolbar=no, scrollbars=yes');
        return false;
    });

    $('#keywordClear a').click(function(){
        $('#keywordClear').hide();
        $("#keywordList").text('選択されていません');
        $('#keywordId').val('');
        $('#keyword_name').val('');
        return false;
    });
});

function setOrder(column) {
    if ($('#sortColumn').val() == column) {
        if ($('#sortOrder').val() == 'asc') {
            $('#sortOrder').val('desc');
        } else {
            $('#sortOrder').val('asc');
        }
    } else {
        $('#sortColumn').val(column);
        $('#sortOrder').val('asc');
    }
    document.forms[0].submit();
    return false;
}

var keyword_name = $("#keyword_name").val();
$("#keywordList").text(keyword_name);

@stop

@section('style')
    <style type="text/css">
        .pagination li {
            display: inline;
            padding-left:4px;
            padding-right:4px;
        }
    </style>
@stop

@section('content')
<h2>アンケート検索</h2>

{{ Form::model('conditions', ['method' => 'post']) }}
    <input type="hidden" id="sortColumn" name="sortColumn" value="{{{ $conditions->sort_column }}}" />
    <input type="hidden" id="sortOrder" name="sortOrder" value="{{{ $conditions->sort_order }}}" />
    <table class="searchQuery">
        <tr>
            <th>ジャンル</th>
            <td>
                {{ Form::selectGenre('genre', $genres, $conditions->genre) }}
            </td>
        </tr>
        <tr>
            <th>キーワード</th>
        <td>
            <a href="#" class="naviBtn" id="keywordListOpen">キーワードウィンドウを開く</a>
            <div id="keywordList">{{{ $conditions->isValidKeyword() ? $conditions->keyword_text : '選択されていません' }}}</div>
            <input id="keywordId" value="{{{ $conditions->isValidKeyword() ? $conditions->keyword : '' }}}" name="keyword_id" type="hidden">
            <input id="keyword_name" value="{{{ $conditions->isValidKeyword() ? $conditions->keyword_text : '' }}}" name="keyword_name" type="hidden">

            <p id="keywordClear" class="m-margint" style="{{ $conditions->isValidKeyword() ? '' : 'display:none;' }}"><a href="#" class="naviBtn">クリア</a></p>
        </td>
    </tr>
    <tr>
        <th>アンケート受付開始日</th>
        <td>
            {{ Form::text('date_from_start', $conditions->date_from_start) }}
            ～
            {{ Form::text('date_from_end', $conditions->date_from_end) }}
        </td>
    </tr>
    <tr>
        <th>アンケート受付終了日</th>
        <td>
            {{ Form::text('date_to_start', $conditions->date_to_start) }}
            ～
            {{ Form::text('date_to_end', $conditions->date_to_end) }}
        </td>
    </tr>
    <tr>
        <th>登録者</th>
        <td>
            {{ Form::selectByTable('register', $accounts, 'id', 'account_name', $conditions->register, true) }}
        </td>
    </tr>
    <tr>
        <th>ステータス</th>
        <td>
            {{ Form::select('status', $status, $conditions->status) }}

        </td>
    </tr>
    <tr>
        <th>コラム</th>
        <td>
            <label>
            {{ Form::checkbox('column_only', null, $conditions->column_only) }} コラムのみ</label>
        </td>
    </tr>
    <tr>
        <th>ID絞り込み</th>
        <td>
            {{ Form::text('question_id', $conditions->question_id) }}
        </td>
    </tr>
    <tr>
        <th>タイトル・選択肢絞り込み</th>
        <td>
            {{ Form::text('title_choice_text', $conditions->title_choice_text) }}
        </td>
    </tr>
    <tr>
        <th>表示</th>
        <td>
            {{ Form::select('viewable_type', $viewable, $conditions->viewable_type) }}
        </td>
    </tr>
</table>
<p class="m-margin tcenter">
<button type="submit" class="commitBtn">検索</button>
</p>
@if ($questions)
<p class="m-margin">ヒット数：{{ number_format($questions->getTotal()) }}件</p>

<div id="order_tag"></div>
<table class="list">
    <tr>
        <th>ID</th>
        <th>タイトル</th>
        <th>選択肢数</th>
        <th>小ジャンル名</th>
        <th><a href="#" onclick="return setOrder('from');">受付開始{{ ($conditions->sort_column == 'from') ? (($conditions->sort_order == 'asc') ? '▲' : '▼') : '' }}</a></th>
        <th><a href="#" onclick="return setOrder('to');">受付終了{{ ($conditions->sort_column == 'to') ? (($conditions->sort_order == 'asc') ? '▲' : '▼') : '' }}</a></th>
        <th>ステータス</th>
        <th>票数</th>
        @if (Auth::user()->havePermission('is_visible_register'))
        <th>登録者</th>
        @endif
        <th>表示</th>
        <th>結果</th>
    </tr>
    @foreach ($questions as $question)
    <tr>
        <td>{{ $question->id }}</td>
        <td>
            @if($question->isViewableEditPage())
                <a href="{{ route('manage.question.edit', $question->id) }}">{{{ $question->title }}}</a>
            @else
                {{{ $question->title }}}
            @endif
        </td>
        <td>{{ $question->getChoiceCount() }}</td>
        <td>{{{ $question->getChildGenreName() }}}</td>
        <td>{{ date('Y-m-d H:i', strtotime($question->vote_date_from)) }}</td>
        <td>{{ date('Y-m-d H:i', strtotime($question->vote_date_to)) }}</td>
        <td>{{ docomo\Models\Codes\QuestionStatus::$label[$question->status] }}</td>
        <td>{{ number_format($question->vote_count) }}</td>
        @if (Auth::user()->havePermission('is_visible_register'))
        <td>{{{ $question->getAccountName() }}}</td>
        @endif
        <td>{{ docomo\Models\Codes\QuestionViewable::$label[$question->viewable_type] }}</td>
        <td>{{ ($question->isViewableResult()) ? link_to_route('manage.question.result', '結果', $question->id) : '&nbsp;' }}</td>
    </tr>
    @endforeach
</table>
<br />
<div style="text-align: center;">
    @include('admin.question.pagination', ['paginator' => $questions, 'currentPage' => $questions->getCurrentPage(), 'lastPage' => $questions->getLastPage()])
</div>
@endif
{{ Form::close() }}

@stop