<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/

\Illuminate\Support\Facades\Artisan::add(new UpdateStatusCommand());
\Illuminate\Support\Facades\Artisan::add(new CreateListCommand());
\Illuminate\Support\Facades\Artisan::add(new SummaryResultCommand());
\Illuminate\Support\Facades\Artisan::add(new SummaryGenreCommand());
\Illuminate\Support\Facades\Artisan::add(new ConvertData());
\Illuminate\Support\Facades\Artisan::add(new ConvertKeywordCommand());
\Illuminate\Support\Facades\Artisan::add(new ConvertQuestionCommand());
\Illuminate\Support\Facades\Artisan::add(new CreateRSSCommand());
\Illuminate\Support\Facades\Artisan::add(new DailySummaryCommand());
\Illuminate\Support\Facades\Artisan::add(new CreateSiteMapCommand());

