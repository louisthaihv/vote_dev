<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('test survey');
$I->amOnPage('/manage');

// $I->loginClickCommon('キーワード一覧・登録・修正・削除');
// $I->click('＋キーワード追加');
// $I->createNewKeyword('keyword_survey_test_1');
// $I->click('＋キーワード追加');
// $I->createNewKeyword('keyword_survey_test_2');
// $I->click('＋キーワード追加');
// $I->createNewKeyword('keyword_survey_test_3');
// $I->click('＋キーワード追加');
// $I->createNewKeyword('keyword_survey_test_4');
// $I->logout();

$I->checkValidSurvey('学校生活、職場生活', '学校生活、職場生活');
$I->logout();
$I->checkSurveyStatusPrepareApprove('survey_status_1', SURVEY_STATUS_APPROVED_PREPARE_RECEIVE);
$I->checkSurveyStatusApprove('survey_status_2', SURVEY_STATUS_APPROVE);
$I->checkSurveyStatusFinish('survey_status_3', SURVEY_STATUS_FINISH);
$I->checkSurveyStatusDraftAndRequest('survey_status_4', SURVEY_SAVE_DRAFT, W_FRM_SURVEY_SAVE_DRAFT, SURVEY_SAVE_DRAFT_TEXT, SURVEY_STATUS_DRAFT);
$I->checkSurveyStatusDraftAndRequest('survey_status_5', SURVEY_APPROVAL, W_FRM_SURVEY_APPROVAL, SURVEY_APPROVAL_TEXT, SURVEY_STATUS_REQUEST_APPROVE);
//TODO
//$I->checkSurveyStatusDraftReject