<?php
    $first = ($currentPage > 5) ? ($currentPage - 5) : 1;
    $last = ($lastPage > $currentPage + 5) ? ($currentPage + 5) : $lastPage;
?>
<p class="tcenter l-margint">

    @if ($currentPage > 1)
        <a href="{{ $paginator->getUrl( $paginator->getCurrentPage() - 1) }}">&lt; 前へ </a>&nbsp;
    @endif
    @if ($currentPage > (ADMIN_PAGINATION_VIEWABLE_HF * 2))
        <a href="{{ $paginator->getUrl(1) }}">{{ 1 }}</a>
        <a href="{{ $paginator->getUrl(2) }}">{{ 2 }}</a>
        ....
    @endif
    @for ($i = $first; $i <= $last; $i++)
        @if($currentPage == $i)
            <span class="boldFont">{{ $i }}</span>
        @else
            <a href="{{ $paginator->getUrl($i) }}">{{ $i }}</a>
        @endif
    @endfor
    @if ($currentPage < $lastPage)
        @if ($lastPage > ($currentPage + (ADMIN_PAGINATION_VIEWABLE_HF * 2)))
            ....
            <a href="{{ $paginator->getUrl($lastPage - 1) }}">{{ ($lastPage - 1) }}</a>
            <a href="{{ $paginator->getUrl($lastPage) }}">{{ $lastPage }}</a>
        @endif
        &nbsp;<a href="{{ $paginator->getUrl($currentPage + 1) }}"> 次へ &gt; </a>
    @endif
</p>