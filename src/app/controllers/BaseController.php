<?php

use Laracasts\Commander\CommanderTrait;
use Illuminate\Support\Facades\Log;
use docomo\Models\Users\User;
use docomo\Models\Users\UserAttribute;
use Illuminate\Support\Facades\Cookie;

class BaseController extends Controller
{

    use CommanderTrait;

    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
        View::share("currentUser", $this->getCurrentUser());

        View::composer('*', function($view) {
            $view->with('viewName', $view->getName())
                    ->with('userAgent', getUserAgent());
        });
    }

    public function getCurrentUserWithCreate()
    {
        if (User::$userDevice == CHAR_I_MODE) {
            $user = User::getOrCreateUser(Input::get('uid'));
        } else {
            $user = User::getOrCreateUser(Cookie::get('user'));

            Cookie::queue('user', $user->uid, 2628000);
        }

        return $user;
    }

    protected function createDateTimeFrom($date, $hour, $mint)
    {
        if (!empty($date) && !empty($hour) && !empty($mint)) {
            return new DateTime($date . ' ' . $hour . ':' . $mint);
        } else {
            return null;
        }
    }

    public function getCurrentUser()
    {
        if (User::$userDevice == CHAR_D_MENU) {
            $uid = Cookie::get('user');
            if (!isset($uid) && array_key_exists('SFUID', $_COOKIE)) {
                $uid = D_MENU_HEADER . $_COOKIE['SFUID'];
            }
            if (!isset($uid) && array_key_exists('hashVal', $_COOKIE)) {
                $uid = D_MENU_HEADER . $_COOKIE['hashVal'];
            }
        } else {
            $uid = Input::get('uid');
        }
        $user = User::getUser($uid);

        if ($user->getDevice() === TYPE_D_MENU) {
            Cookie::queue('user', $user->uid, 2628000, '/', null, false, false);
        }
        return $user;
    }

    /**
     * for debug
     * 
     * @param type $val
     * @param type $marker
     */
    public function log($val, $marker = null){
        Log::debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        if($marker){
            Log::debug("--->" . $marker);
        }
        Log::debug($val);
        Log::debug("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    }

    /**
     * @param $objView
     */
    protected function renderView($objView)
    {
        return getUserAgent() == 'fp' ? \Redirect::to(route('home.top') . '?uid=NULLGWDOCOMO') : $objView;
    }
}
