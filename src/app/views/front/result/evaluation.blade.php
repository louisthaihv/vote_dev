<section class="evaluateArea l-margin">
    <h4 class="subSectionTitH4 evaluateIcon m-margin"><span>このアンケート結果どう感じた？</span></h4>
    <div class="evaluateBlock">
        <ul class="{{ ($hasVoted) ? '' : 'evaluatePost' }}">
            @foreach($voteRatingTotals as $voteRatingTotal)
            <li>
                {{ Form::hidden('voteUrl', route("result.vote",[
                        $question->childGenre->parentGenre->genre_parent_directory,
                        $question->childGenre->genre_child_directory,
                        $question->id,
                        $voteRatingTotal->type
                ]), ['class' => 'voteUrl']) }}
                {{ HTML::image($voteRatingTotal->getImagePath()) }} 
                <span class="evaluateCount">( <span class='counter'>{{ $voteRatingTotal->rating_count }}</span> )</span>
            </li>
            @endforeach
        </ul>
        <p class="date">締切日：{{ jpDate($question->vote_date_to, 'Y年m月d日 H:i') }}</p>
    </div>
</section>