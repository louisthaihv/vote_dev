<?php

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Input;
use docomo\Models\Genres\ParentGenre;
use docomo\Models\Questions\Question;
use docomo\Models\Features\Feature;
use docomo\Models\Notices\Notice;
use docomo\Models\Users\User;
use docomo\Models\Codes\FrontTab;

View::composer('layouts.front_elements._footercontents', function($view) {

});

View::composer('front.entry._bottomcontents', function($view){
        $question = $view->getData()['question'];
        $user = $view->getData()['currentUser'];

        // feature
        $features = Feature::whereLikeQuestionId($question)->get();

        // similar questions
        $sameGenreOngoingQuestions = Question::fieldList()
            ->baseViewable(
                QUESTION_STATUS_READY,
                null,
                TYPE_D_MENU,
                $user
            )
            ->excludeId($question->id)
            ->unanswered($user)
            ->orderBaseDateToDesc()
            ->limit(NUM_SAMECATEG_QUESTIONS)
            ->get();

        $view->with('features', $features);
        $view->with('sameGenreOngoingQuestions', $sameGenreOngoingQuestions);
});

View::composer('front.entry.fp_bottom_contents', function($view){
    $question = $view->getData()['question'];
    $user = $view->getData()['currentUser'];

    // similar questions
    $sameGenreOngoingQuestions = Question::
        fieldList()
        ->baseViewable(
            QUESTION_STATUS_READY,
            null,
            TYPE_I_MODE,
            $user
        )
        ->excludeId($question->id)
        ->unanswered($user)
        ->orderBaseDateToDesc()
        ->limit(NUM_SAME_CATEGORY_QUESTIONS_FP)
        ->get();

    $view->with('sameGenreOngoingQuestions', $sameGenreOngoingQuestions);
});

View::composer(['layouts.front_elements._news', 'layouts.front_elements._news_fp'], function($view){

    $view->with('notices', Notice::getNotices(User::$userDevice));
});

View::composer(['front._shared._tab_option',
                'front.list.genre._shared.*'], function($view) {

    $view->with(['tabEntry'=> FrontTab::$label[PROCESSING], 'tabResult'=> FrontTab::$label[COMPLETE]]);
});

View::composer(
    [
    'layouts.front_elements._header',
    ],
    function ($view) {
        $user = User::getUser(Input::get('uid', Cookie::get('user')));
        $userIcons = json_encode($user->getAnswered());
        $view->with('user', $user)
            ->with('userIcons', $userIcons);
});

View::composer(
    [
        'layouts.front_elements._profileBn',
        'layouts.front_elements._profileBn_fp'
    ],
    function($view) {

        $user = User::getUser(Input::get('uid', Cookie::get('user')));
        $view->with('user', $user);
});
