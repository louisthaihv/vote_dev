<?php
namespace RSS;

class RSSBaseController extends \Controller {

    protected $rssTitle = 'みんなの声受付中アンケート一覧';
    protected $siteURL = 'http://vote.smt.docomo.ne.jp';
    protected $siteDescription = 'サイト説明';

    public function __construct() {
        $this->siteURL = str_replace('/' . \Request::path(), '', \Request::url());
    }

    protected function createViewVer1($questions) {

        foreach ($questions as &$question) {
            $question->url = $question->getEntryPath();
            $question->date = new \Datetime($question->vote_date_from);
            $question->description = $question->description;

            unset($question);
        }

        return \View::make('rss.template1_0', [
            'rssTitle' => $this->rssTitle,
            'siteURL' => $this->siteURL,
            'siteDescription' => $this->siteDescription,
            'lastUpdate' => new \DateTime,
            'recordData' => $questions
        ]);
    }

    protected function createViewVer2($questions) {

        foreach ($questions as &$question) {
            $question->url = $question->getEntryPath();
            $question->date = new \Datetime($question->vote_date_from);
            $question->description = $question->description;

            unset($question);
        }

        return \View::make('rss.template2_0', [
            'rssTitle' => $this->rssTitle,
            'siteURL' => $this->siteURL,
            'siteDescription' => $this->siteDescription,
            'lastUpdate' => new \DateTime,
            'recordData' => $questions
        ]);
    }

    protected function createViewVer1ForColumn($questions) {

        foreach ($questions as &$question) {
            $question->url = $question->getColumnPath();
            $question->date = new \Datetime($question->column->view_create_datetime);
            $question->title = $question->column->column_title;
            $question->description = $question->column->description;

            unset($question);
        }

        return \View::make('rss.template1_0', [
            'rssTitle' => $this->rssTitle,
            'siteURL' => $this->siteURL,
            'siteDescription' => $this->siteDescription,
            'lastUpdate' => new \DateTime,
            'recordData' => $questions
        ]);
    }

    protected function createViewVer2ForColumn($questions) {

        foreach ($questions as &$question) {
            $question->url = $question->getColumnPath();
            $question->date = new \Datetime($question->column->view_create_datetime);
            $question->title = $question->column->column_title;
            $question->description = $question->column->description;

            unset($question);
        }

        return \View::make('rss.template2_0', [
            'rssTitle' => $this->rssTitle,
            'siteURL' => $this->siteURL,
            'siteDescription' => $this->siteDescription,
            'lastUpdate' => new \DateTime,
            'recordData' => $questions
        ]);
    }
}