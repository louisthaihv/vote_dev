<?php

namespace docomo\Models\Accounts;

class AccountLoggedInCommand
{
    public $login_id;
    public $login_password;
    
    /**
     * [Instance object AccountLoginedCommand]
     *
     * @param [string] $login_id       [field login_id of table accounts]
     * @param [string] $login_password [field login_password of table accounts]
     */
    
    public function __construct($login_id, $login_password)
    {
        
        $this->login_id = $login_id;
        $this->login_password = $login_password;
    }
}
