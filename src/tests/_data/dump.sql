# ************************************************************
# Sequel Pro SQL dump
# Version 4135
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.21)
# Database: docomo
# Generation Time: 2015-01-13 02:09:45 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table accounts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accounts`;

CREATE TABLE `accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `login_id` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `login_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `allow_genre_parent` int(11) NOT NULL,
  `is_visible_register` tinyint(1) NOT NULL,
  `is_manage_genre` tinyint(1) NOT NULL,
  `is_manage_keyword` tinyint(1) NOT NULL,
  `is_manage_account` tinyint(1) NOT NULL,
  `is_manage_notice` tinyint(1) NOT NULL,
  `is_manage_feature` tinyint(1) NOT NULL,
  `is_manage_pickup` tinyint(1) NOT NULL,
  `is_manage_attention` tinyint(1) NOT NULL,
  `is_manage_agree` tinyint(1) NOT NULL,
  `is_manage_input_comment` tinyint(1) NOT NULL,
  `is_manage_column` tinyint(1) NOT NULL,
  `is_manage_summary` tinyint(1) NOT NULL,
  `is_viewable_all_question` tinyint(1) NOT NULL,
  `is_enabled` tinyint(1) NOT NULL,
  `created_account_id` int(11) NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `accounts_account_name_unique` (`account_name`),
  UNIQUE KEY `accounts_login_id_unique` (`login_id`),
  KEY `accounts_is_enabled_index` (`is_enabled`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table answers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `answers`;

CREATE TABLE `answers` (
  `question_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `choice_id` int(11) NOT NULL,
  `age` int(11) NOT NULL DEFAULT '0',
  `gender` int(11) NOT NULL DEFAULT '0',
  `local` int(11) NOT NULL DEFAULT '0',
  `answer_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`question_id`,`user_id`,`choice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table attentions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attentions`;

CREATE TABLE `attentions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `keyword_id` int(11) NOT NULL,
  `tab` int(11) NOT NULL,
  `from` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `to` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `attentions_from_index` (`from`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table attribute_values
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attribute_values`;

CREATE TABLE `attribute_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `attribute_value` int(11) NOT NULL,
  `attribute_value_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL,
  `attribute_value_directory` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `attribute_values` WRITE;
/*!40000 ALTER TABLE `attribute_values` DISABLE KEYS */;

INSERT INTO `attribute_values` (`id`, `attribute_id`, `attribute_value`, `attribute_value_name`, `sort_order`, `attribute_value_directory`, `is_default`, `created_at`, `updated_at`)
VALUES
  (1,1,1,'～10代',10,'10',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (2,1,2,'20代',20,'20',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (3,1,3,'30代',30,'30',1,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (4,1,4,'40代',40,'40',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (5,1,5,'50代',50,'50',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (6,1,6,'60代～',60,'60',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (7,2,1,'男性',10,'male',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (8,2,2,'女性',20,'female',1,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (9,3,1,'北海道',10,'hokkaido',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (10,3,2,'青森県',20,'aomori',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (11,3,3,'岩手県',30,'iwate',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (12,3,4,'宮城県',40,'miyagi',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (13,3,5,'秋田県',50,'akita',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (14,3,6,'山形県',60,'yamagata',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (15,3,7,'福島県',70,'fukushima',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (16,3,8,'茨城県',80,'ibaraki',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (17,3,9,'栃木県',90,'tochigi',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (18,3,10,'群馬県',100,'gunma',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (19,3,11,'埼玉県',110,'saitama',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (20,3,12,'千葉県',120,'chiba',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (21,3,13,'東京都',130,'tokyo',1,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (22,3,14,'神奈川県',140,'kanagawa',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (23,3,15,'新潟県',150,'niigata',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (24,3,16,'富山県',160,'toyama',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (25,3,17,'石川県',170,'ishikawa',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (26,3,18,'福井県',180,'fukui',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (27,3,19,'山梨県',190,'yamanashi',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (28,3,20,'長野県',200,'nagano',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (29,3,21,'岐阜県',210,'gifu',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (30,3,22,'静岡県',220,'shizuoka',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (31,3,23,'愛知県',230,'aichi',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (32,3,24,'三重県',240,'mie',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (33,3,25,'滋賀県',250,'shiga',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (34,3,26,'京都府',260,'kyoto',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (35,3,27,'大阪府',270,'osaka',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (36,3,28,'兵庫県',280,'hyogo',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (37,3,29,'奈良県',290,'nara',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (38,3,30,'和歌山県',300,'wakayama',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (39,3,31,'鳥取県',310,'tottori',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (40,3,32,'島根県',320,'shimane',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (41,3,33,'岡山県',330,'okayama',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (42,3,34,'広島県',340,'hiroshima',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (43,3,35,'山口県',350,'yamaguchi',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (44,3,36,'徳島県',360,'tokushima',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (45,3,37,'香川県',370,'kagawa',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (46,3,38,'愛媛県',380,'ehime',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (47,3,39,'高知県',390,'kouchi',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (48,3,40,'福岡県',400,'fukuoka',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (49,3,41,'佐賀県',410,'saga',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (50,3,42,'長崎県',420,'nagasaki',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (51,3,43,'熊本県',430,'kumamoto',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (52,3,44,'大分県',440,'oita',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (53,3,45,'宮崎県',450,'miyazaki',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (54,3,46,'鹿児島県',460,'kagoshima',0,'2015-01-13 10:16:06','2015-01-13 10:16:06'),
  (55,3,47,'沖縄県',470,'okinawa',0,'2015-01-13 10:16:06','2015-01-13 10:16:06');

/*!40000 ALTER TABLE `attribute_values` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table attributes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attributes`;

CREATE TABLE `attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL,
  `attribute_directory` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `priority` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `attributes` WRITE;
/*!40000 ALTER TABLE `attributes` DISABLE KEYS */;

INSERT INTO `attributes` (`id`, `attribute_name`, `sort_order`, `attribute_directory`, `created_at`, `updated_at`, `priority`)
VALUES
  (1,'年代別',1,'age','0000-00-00 00:00:00','0000-00-00 00:00:00',1),
  (2,'性別',2,'gender','0000-00-00 00:00:00','0000-00-00 00:00:00',1),
  (3,'地域別',3,'pref','0000-00-00 00:00:00','0000-00-00 00:00:00',0);

/*!40000 ALTER TABLE `attributes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table choices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `choices`;

CREATE TABLE `choices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `choice_num` int(11) NOT NULL,
  `choice_text` text COLLATE utf8_unicode_ci NOT NULL,
  `summary_value` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `choices_question_id_choice_num_unique` (`question_id`,`choice_num`),
  KEY `choices_question_id_index` (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table columns
# ------------------------------------------------------------

DROP TABLE IF EXISTS `columns`;

CREATE TABLE `columns` (
  `question_id` int(11) NOT NULL,
  `is_visible` tinyint(1) NOT NULL,
  `column_title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `column_detail` text COLLATE utf8_unicode_ci NOT NULL,
  `view_create_datetime` datetime NOT NULL,
  `created_account_id` int(11) NOT NULL,
  `updated_account_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`question_id`),
  KEY `columns_is_visible_index` (`is_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table feature
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feature`;

CREATE TABLE `feature` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` varchar(10000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  `is_view_list` tinyint(1) NOT NULL DEFAULT '0',
  `question_ids` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_feature_column` tinyint(1) NOT NULL DEFAULT '0',
  `created_account_id` int(11) NOT NULL,
  `updated_account_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `feature_from_index` (`from`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table genre_children
# ------------------------------------------------------------

DROP TABLE IF EXISTS `genre_children`;

CREATE TABLE `genre_children` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `genre_parent_id` int(11) NOT NULL,
  `genre_child_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `genre_child_description` text COLLATE utf8_unicode_ci,
  `child_is_visible` tinyint(1) NOT NULL,
  `genre_child_directory` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `child_sort_order` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `genre_children_genre_parent_id_index` (`genre_parent_id`),
  KEY `genre_children_child_is_visible_index` (`child_is_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table genre_parents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `genre_parents`;

CREATE TABLE `genre_parents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `genre_parent_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `parent_is_visible` tinyint(1) NOT NULL,
  `genre_parent_directory` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `parent_sort_order` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `genre_parents_parent_is_visible_index` (`parent_is_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table keywords
# ------------------------------------------------------------

DROP TABLE IF EXISTS `keywords`;

CREATE TABLE `keywords` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `keyword_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `is_visible` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `keywords_is_visible_index` (`is_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`migration`, `batch`)
VALUES
  ('2014_11_06_101824_create_session_table',1),
  ('2014_11_14_032657_create_table_attributes',1),
  ('2014_11_14_032714_create_table_attribute_values',1),
  ('2014_11_14_032757_create_table_category_parents',1),
  ('2014_11_14_032815_create_table_category_childs',1),
  ('2014_11_14_090553_create_table_keywords',1),
  ('2014_11_14_091459_create_table_choices',1),
  ('2014_11_14_092309_create_table_makesenses',1),
  ('2014_11_14_094752_create_table_accounts',1),
  ('2014_11_14_095902_create_table_answers',1),
  ('2014_11_15_020709_create_table_notices',1),
  ('2014_11_15_021720_create_table_pickup_keywords',1),
  ('2014_11_15_022012_create_table_users',1),
  ('2014_11_15_023011_create_table_user_attributes',1),
  ('2014_11_15_030038_create_table_question_tags',1),
  ('2014_11_15_031600_create_table_summary_by_types',1),
  ('2014_11_15_034458_create_table_questions',1),
  ('2014_11_15_035954_create_table_specials',1),
  ('2014_11_15_040218_create_table_info_for_customers',1),
  ('2014_12_08_111700_recreate_table_accounts',1),
  ('2014_12_08_172458_create_table_pickups',1),
  ('2014_12_09_012351_create_feature_table',1),
  ('2014_12_09_013608_create_result_vote_table',1),
  ('2014_12_09_022610_create_indexes',1),
  ('2014_12_09_023004_alter_table_attributes',1),
  ('2014_12_09_092258_alter_table_questions',1),
  ('2014_12_10_012033_create_table_attentions',1),
  ('2014_12_10_080945_recreate_table_user_attributes',1),
  ('2014_12_10_103636_recreate_table_user',1),
  ('2014_12_10_150123_alter_table_user_attributes',1),
  ('2014_12_10_191735_alter_table_users',1),
  ('2014_12_10_195529_alter_table_vote_results',1),
  ('2014_12_15_060840_create_table_genres',1),
  ('2014_12_17_033202_create_index_attribute_values',1),
  ('2014_12_17_063506_create_table_navigation',1),
  ('2014_12_17_081019_alter_table_questions_genre',1),
  ('2014_12_17_124158_alter_table_question_attrvalue',1),
  ('2014_12_18_231549_alter_table_notices',1),
  ('2014_12_19_020021_alter_table_feature',1),
  ('2014_12_19_061655_alter_table_choice',1),
  ('2014_12_19_072741_alter_table_navigations',1),
  ('2014_12_22_080429_alter_table_choice_choice_num',1),
  ('2014_12_25_065317_recreate_table_columns',1),
  ('2014_12_25_125615_create_table_ratings',1),
  ('2014_12_26_015604_alter_table_tags',1),
  ('2014_12_26_044954_create_index_questions',1),
  ('2014_12_26_053229_create_table_top_summary_by_attribute_value',1),
  ('2014_12_26_100049_alter_table_result_votes',1),
  ('2014_12_26_101120_alter_table_attribute_values',1),
  ('2015_01_05_042432_alter_list_tables',1),
  ('2015_01_06_010028_alter_table_results',1),
  ('2015_01_07_022650_alter_attribute_value',1),
  ('2015_01_07_032352_alter_table_rating_total',1),
  ('2015_01_07_072430_drop_makesenses',1),
  ('2015_01_07_092155_alter_table_users_birthday',1),
  ('2015_01_08_103102_add_index_choices',1),
  ('2015_01_08_111143_alter_vote_rating',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table navigations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `navigations`;

CREATE TABLE `navigations` (
  `question_id` int(11) NOT NULL,
  `view_type` int(11) NOT NULL,
  `row` int(11) NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `url` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `navigations_question_id_view_type_row_index` (`question_id`,`view_type`,`row`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table notices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notices`;

CREATE TABLE `notices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table pickups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pickups`;

CREATE TABLE `pickups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table question_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `question_tags`;

CREATE TABLE `question_tags` (
  `question_id` int(11) NOT NULL,
  `keyword_id` int(11) NOT NULL,
  PRIMARY KEY (`question_id`,`keyword_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table questions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `questions`;

CREATE TABLE `questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `genre_parent_id` int(11) NOT NULL,
  `genre_child_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci,
  `vote_date_from` datetime NOT NULL,
  `vote_date_to` datetime NOT NULL,
  `baseline_date` datetime DEFAULT NULL,
  `viewable_type` int(11) NOT NULL,
  `visible_device_i` int(11) NOT NULL,
  `visible_device_d` int(11) NOT NULL,
  `summary_type` int(11) NOT NULL,
  `average_unit` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `is_visible_other` tinyint(1) NOT NULL,
  `other_text` text COLLATE utf8_unicode_ci,
  `shuffle_choice` tinyint(1) NOT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `attribute_value_id` int(11) DEFAULT NULL,
  `result_comment` text COLLATE utf8_unicode_ci,
  `navigation_title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `sns_comment` text COLLATE utf8_unicode_ci,
  `comment` text COLLATE utf8_unicode_ci,
  `back_button_text` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `back_button_url` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `has_column` tinyint(1) DEFAULT NULL,
  `vote_count` int(11) NOT NULL DEFAULT '0',
  `account_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `questions_status_index` (`status`),
  KEY `questions_vote_date_from_index` (`vote_date_from`),
  KEY `questions_vote_date_to_index` (`vote_date_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table result_votes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `result_votes`;

CREATE TABLE `result_votes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_value_id` int(11) NOT NULL,
  `device` int(11) NOT NULL,
  `choice_id` int(11) NOT NULL,
  `ranking` int(10) unsigned DEFAULT NULL,
  `voted_count` int(10) unsigned NOT NULL DEFAULT '0',
  `summary_value` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `result_votes_question_id_index` (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;

INSERT INTO `sessions` (`id`, `payload`, `last_activity`)
VALUES
  ('de8fc7d2746bc068e8b3f7dab8a3d6a9a5defe00','YTo0OntzOjY6Il90b2tlbiI7czo0MDoicDNDOE1GZ3Zibmg3eW9wVGtCWVlJTENyYXpnWWsxa2xJU0VuS1hKbiI7czoyMjoiUEhQREVCVUdCQVJfU1RBQ0tfREFUQSI7YTowOnt9czo1OiJmbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjk6Il9zZjJfbWV0YSI7YTozOntzOjE6InUiO2k6MTQyMTExMTc3OTtzOjE6ImMiO2k6MTQyMTExMTc3MTtzOjE6ImwiO3M6MToiMCI7fX0=',1421111780);

/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table summary_by_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `summary_by_types`;

CREATE TABLE `summary_by_types` (
  `type` int(10) unsigned NOT NULL,
  `ranking_number` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`type`,`ranking_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table top_summary_by_attribute_value
# ------------------------------------------------------------

DROP TABLE IF EXISTS `top_summary_by_attribute_value`;

CREATE TABLE `top_summary_by_attribute_value` (
  `question_id` int(10) unsigned NOT NULL,
  `attribute_id` int(10) unsigned NOT NULL,
  `attribute_value_id` int(10) unsigned NOT NULL,
  `top_choice_num` int(11) DEFAULT NULL,
  `average_num` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`question_id`,`attribute_id`,`attribute_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table user_attributes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_attributes`;

CREATE TABLE `user_attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_value_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table vote_rating_totals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `vote_rating_totals`;

CREATE TABLE `vote_rating_totals` (
  `question_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `rating_count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`question_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table vote_ratings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `vote_ratings`;

CREATE TABLE `vote_ratings` (
  `question_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `vote_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`question_id`,`user_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
