<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecreateTableColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::dropIfExists('columns');
        Schema::create('columns', function (Blueprint $table)
        {
            // schema
            $table->integer('question_id');
            $table->boolean('is_visible');
            $table->string('column_title', 200);
            $table->text('column_detail');
            $table->dateTime('view_create_datetime');
            $table->integer('created_account_id');
            $table->integer('updated_account_id');
            $table->timestamps();
            // indexes
            $table->primary('question_id');
            $table->index('is_visible');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('columns');
	}

}
