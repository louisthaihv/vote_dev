<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('manage attribute');

$I->amOnPage('/manage/attribute');
$I->amOnAction('Admin\SurveyController@create');
