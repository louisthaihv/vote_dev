<?php 
require('tests/functional/AdminCommon.php');

//AdminHeader
// $I->see('みんなの声　管理画面','//header/h1');
// $I->see('Tuan Dam Thanh','header');
// $admin_logout = URL::route('admin.logOut');
// $I->seeLink('ログアウト',$admin_logout);

$I->see('メインメニュー');

$I->see('アンケート管理');
$url = URL::route('admin.survey.create');
$I->seeLink('アンケート新規投稿',$url);
$admin_survey_index = URL::route('admin.survey.index');
$I->seeLink('アンケート検索・修正・削除',$admin_survey_index);

$I->see('TOPページ特別枠管理');
$admin_special_create = URL::route('admin.special.create');
$I->seeLink('特別枠データ作成',$admin_special_create);
$admin_special_index = URL::route('admin.special.index');
$I->seeLink('特別枠一覧・修正・削除',$admin_special_index);

$I->see('TOPページ注目キーワード管理');
$admin_attention_create = URL::route('admin.attention.create');
$I->seeLink('注目キーワードデータ作成',$admin_attention_create);
$admin_attention_index = URL::route('admin.attention.index');
$I->seeLink('注目キーワード一覧・修正・削除',$admin_attention_index);

$I->see('ジャンル管理');
$admin_genre_index = URL::route('admin.genre.index');
$I->seeLink('ジャンル一覧・登録・修正・削除',$admin_genre_index);

$I->see('キーワード管理');
$admin_keyword_index = URL::route('admin.keyword.index');
$I->seeLink('キーワード一覧・登録・修正・削除',$admin_keyword_index);

$I->see('アカウント管理');
$admin_account_index = URL::route('admin.account.index');
$I->seeLink('アカウント一覧・登録・修正・削除',$admin_account_index);

$I->see('アカウント管理');
$admin_info_index = URL::route('admin.info.index');
$I->seeLink('お知らせ一覧・登録・修正・削除',$admin_info_index);

$I->see('集計(検討中)');
// $admin_attention_create = URL::route('admin.attention.create');
$I->seeLink('得票数（i版、SP版）','#');
// $admin_attention_index = URL::route('admin.attention.index');
$I->seeLink('プロフィール登録（i版、SP版）','#');

