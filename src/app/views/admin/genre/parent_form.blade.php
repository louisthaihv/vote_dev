@section('head')
<script type="text/javascript" src="{{ url('common/admin/js/manage-genre/edit.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    $(window).load(function(){
        $('#editParentGenreBtn').click(function(){
            return (confirm('{{{ $confirmMessage }}}'));
        });
    });
</script>
@stop

@include('admin.share.errors', ['errorList' => $genre->errors()])
{{ Form::model($genre, ['url' => $url, 'method' => $method]) }}
    <table class="formTable">
        <tr>
            <th>大ジャンル名<span class="require">（必須）</span></th>
            <td>{{ Form::text('genre_parent_name', $genre->genre_parent_name, ['class' => 'xx-large']) }}</td>
        </tr>
        <tr>
            <th>ディレクトリ名<span class="require">（必須）</span></th>
            <td>{{ Form::text('genre_parent_directory', $genre->genre_parent_directory, ['class' => 'xx-large']) }}</td>
        </tr>
        <tr>
        <tr>
            <th>非表示</th>
            <td><label><input type="checkbox" name="parent_is_visible" {{ ($genre->parent_is_visible == VISIBLE) ? '' : 'checked="checked"' }} /> 非表示にする</label></td>
        </tr>
    </table>
    <div class="tcenter m-margint">
        <button type="submit" name="commitParent" id="editParentGenreBtn" class="commitBtn">{{ $button }}</button>
    </div>
{{ Form::close() }}