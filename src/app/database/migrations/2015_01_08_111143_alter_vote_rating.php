<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVoteRating extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasColumn('vote_ratings', 'id')) {
			Schema::table('vote_ratings', function ($table) {
				$table->dropColumn('id');
				$table->dropTimestamps();
				$table->primary(['question_id', 'user_id', 'type']);
				$table->timestamp('vote_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
