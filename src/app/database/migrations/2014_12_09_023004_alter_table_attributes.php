<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAttributes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('attributes'))
		{
			Schema::table('attributes', function($table)
			{
				$table->integer('priority');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('attributes'))
		{
			Schema::table('attributes', function($table)
			{
				$table->dropColumn('priority');
			});
		}
	}

}
