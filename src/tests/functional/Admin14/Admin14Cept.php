<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('manage Genre');

$I->amOnPage('/manage/genre');
$I->amOnAction('Admin\SurveyController@create');
