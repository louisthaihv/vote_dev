@extends('layouts.front_fp')

@section('content')
@include('layouts.front_elements._space')
@if (isset($type))
<div><img src="/common/front/images/fp/tit2.gif" width="100%" alt="投票結果"/></div>
@else
<div>{{ HTML::image('common/front/images/fp/tit1.gif', '投票', array('width' => '100%')) }}</div>
@endif
@include('layouts.front_elements._space')
<p>ただいま投票の受付準備中です。開始まで暫くお待ち下さい。</p>
@include('layouts.front_elements._space')
@include('layouts.front_elements._profileBn_fp')
@include('layouts.front_elements._space')
@stop