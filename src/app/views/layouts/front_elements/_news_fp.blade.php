@foreach($notices as $notice)
    @include('layouts.front_elements._space')
    <div style="background-color:#eeeeee;padding:10px;text-align:center;">
        <a href="{{ route('notice.detail', ['id' => $notice->id, 'uid' => 'NULLGWDOCOMO']) }}">{{ $notice->title }}</a><br />
        {{ $notice->description }}
    </div>
@endforeach
