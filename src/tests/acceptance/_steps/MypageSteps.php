<?php
namespace AcceptanceTester;

class MypageSteps extends \AcceptanceTester\CommonSteps
{
	public function checkFPMypage($profile, $survies_result, $survies_entry)
	{
		$I = $this;
		$I->amOnPage('/mypage');
		$I->wait(3);
		//$this->checkProfileRecommend($profile);
		//$this->checkTotalVote(count($survies_result['items']) + count($survies_entry['items']));
		//check list entry firt, after that check list vote with pagging
		$this->checkSurveyEntryByVote($survies_entry);
		$this->checkSurveyResultByVote($survies_result);
	}

	public function checkProfileRecommend($profile)
	{
		$I = $this;
		$I->seeLink('プロフィール設定・変更', '/profile');
		$I->click('プロフィール設定・変更');
		$I->createProfile($profile);
		$I->amOnPage('/mypage');
		$I->dontSeeLink('プロフィール設定・変更');
	}

	public function checkTotalVote($total_vote)
	{
		$I = $this;
		$title = sprintf('総投票数：%d件', $total_vote);
		$I->see($title);
	}

	public function checkSurveyResultByVote($survies)
	{
		$I = $this;
		//check page 1
		$current_page = 1;
		$I->checkFPListSurveyCommon($survies, $current_page, 1);
		$I->wait(1);
		//check page 2
		$next_page = sprintf('[%d]次へ>', $current_page++);
		//$I->click($next_page);
		$I->click('[#]次へ>');
		$I->wait(3);
		$I->checkFPListSurveyCommon($survies, $current_page, 1);

		//check page 3
		$next_page = sprintf('[%d]次へ>', $current_page++);
		//$I->click($next_page);
		$I->click('[#]次へ>');
		$I->wait(3);
		$I->checkFPListSurveyCommon($survies, $current_page, 1);

		//check page 2
		$prev_page = sprintf('<[%d]前へ', $current_page--);
		//$I->click($prev_page);
		$I->click('<[*]前へ');
		$I->wait(3);
		$I->checkFPListSurveyCommon($survies, $current_page, 1);

		//check page 1
		$prev_page = sprintf('<[%d]前へ', $current_page--);
		//$I->click($prev_page);
		$I->click('<[*]前へ');
		$I->wait(3);
		$I->checkFPListSurveyCommon($survies, $current_page, 1);
	}


	public function checkSurveyEntryByVote($survies_entry)
	{
		$I = $this;
		$I->checkFPListSurveyCommon($survies_entry);
	}

}
