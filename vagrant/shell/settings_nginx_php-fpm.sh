#!/bin/sh

echo "Replace config of php-fpm"
cd /etc
sed -i "s/;events.mechanism/events.mechanism/g" php-fpm.conf
cd /etc/php-fpm.d
sed -i "s/listen = 127.0.0.1:9000/listen = \/var\/run\/php-fpm.sock/g" www.conf
sed -i "s/;listen./listen./g" www.conf
sed -i "s/= nobody/= vagrant/g" www.conf
sed -i "s/= apache/= vagrant/g" www.conf

/etc/init.d/php-fpm start
chkconfig php-fpm on

echo "Replace config of nginx"
cd /etc/nginx
mv nginx.conf nginx.conf.org
echo "
user  vagrant;
worker_processes  4;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;
    #charset       utf-8;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        off;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;
    
    server_tokens off;
    server_name_in_redirect off;

    include /etc/nginx/conf.d/*.conf;
}

    
" > nginx.conf

cd /etc/nginx/conf.d/
mv default.conf default.conf.org

echo 'server {
    listen       80;
    server_name  localhost;

    root /home/vagrant/src/public;
    index index.html index.htm index.php;

    error_log   /var/log/nginx/error.log;

    location / {
        try_files $uri $uri/ /index.php?q=$uri&$args;
    }

    location ~ \.php$ {
        try_files                $uri = 404;
        include                  /etc/nginx/fastcgi_params;
        fastcgi_pass             unix:/var/run/php-fpm.sock;

        fastcgi_index            index.php;
        fastcgi_split_path_info  ^(.+\.php)(\.+)$;
        
        fastcgi_param            SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        fastcgi_param            PATHINFO         $fastcgi_path_info;
        fastcgi_param            PATH_TRANSLATED  $document_root$fastcgi_path_info;
    }
    
    error_page 404 /index.php;

    location ~ /\.(ht|git|svn) {
        deny  all;
    }

    location = /favicon.ico {
        access_log    off;
        log_not_found off;
    }

    location = /robots.txt {
        access_log    off;
        log_not_found off;
    }

}

' > default.conf

yes | php /home/prod_vote/src/artisan migrate
yes | php /home/prod_vote/src/artisan db:seed
