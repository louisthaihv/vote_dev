<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AlterTableQuestions extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('questions', function (Blueprint $table)
        {
            if (Schema::hasColumn('questions', 'column_title')) {
                $table->dropColumn('column_title');
                $table->dropColumn('column_detail');
            }
            if (Schema::hasColumn('questions', 'is_column'))
            {
                $table->renameColumn('is_column', 'has_column');
            }
        });
        Schema::dropIfExists('columns');
        Schema::create('columns', function (Blueprint $table)
        {
            // schema
            $table->integer('question_id');
            $table->boolean('is_visible');
            $table->string('column_title', 200);
            $table->text('column_detail');
            $table->dateTime('view_create_datetime');
            $table->integer('created_account_id');
            $table->integer('updated_account_id');
            $table->timestamps();
            // indexes
            $table->primary('question_id');
            $table->index('is_visible');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('columns');
        Schema::table('questions', function ($table)
        {
            if (!Schema::hasColumn('questions', 'column_title')) {
                $table->string('column_title', 200);
                $table->text('column_detail');
            }
            if (Schema::hasColumn('questions', 'has_column')) {
                $table->renameColumn('has_column', 'is_column');
            }
        });
    }
}
