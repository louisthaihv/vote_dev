<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('check head portion after login');
$I->amOnPage('/manage');

// Check head after login
$I->checkHead();

// Check head in manage/enq/create
$I->checkHead('アンケート新規投稿');

// Check head in manage/enq/
$I->checkHead('アンケート検索・修正・削除');

// Check head in manage/enq/top15
// $I->checkHead('承認申請中（15）');

// Check head in manage/special/create
$I->checkHead('特別枠データ作成');

// Check head in manage/special
$I->checkHead('特別枠一覧・修正・削除');

// Check head in manage/attention/create
$I->checkHead('注目キーワードデータ作成');

// Check head in manage/attention
$I->checkHead('注目キーワード一覧・修正・削除');

// Check head in manage/genre
// $I->checkHead('ジャンル一覧・登録・修正・削除');

// Check head in manage/keyword
$I->checkHead('キーワード一覧・登録・修正・削除');

// Check head in manage/account
$I->checkHead('アカウント一覧・登録・修正・削除');

// Check head in manage/info
$I->checkHead('お知らせ一覧・登録・修正・削除');

// Check head in manage/
// $I->checkHead('得票数（i版、SP版）');

// Check head in manage/
// $I->checkHead('プロフィール登録（i版、SP版）');
