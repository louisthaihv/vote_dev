
<section class="l-margin">
    <h3 class="subSectionTit genreIcon">ジャンルからアンケートを探そう！</h3>

    <div class="links noBorder">
        <ul class="accordion">
            @foreach (docomo\Models\Genres\ParentGenre::getDataByCache() as $key => $parent)
            <li>
                <p><span>{{ $parent->genre_parent_name }}</span></p>
                <ul>
                    @foreach (docomo\Models\Genres\ChildGenre::getDataByCache($parent->id) as $child)
                        <li><a id="footer_{{ $parent->genre_parent_directory . '_' . $child->genre_child_directory }}" href="{{ url('/list/genre/' . $parent->genre_parent_directory .'/'. $child->genre_child_directory .'/result') }}">{{ $child->genre_child_name }} ({{ $child->record_count }})</a></li>
                    @endforeach
                </ul>
            </li>
            @endforeach
        </ul>
    </div><!--/links-->
</section>

@if (false)
<section class="l-margin">
    <h3 class="subSectionTit searchIcon">アンケートをキーワードで探す</h3>
    <div class="search">
        <p class="redFont" style="display:none">{{ W_FRM_REQUIRED_KEYWORD }}</p>
        <form method="GET">
            <ul>
                <li class="search-key"><input type="text" name="search_key"  placeholder="キーワード"></li>
                <li class="search-bt"><input id="search_key" class="submit" type="button" name="" value="検索"></li>
            </ul>
        </form>
    </div><!--search-->
</section>
@endif
<section class="l-margin">
    <h3 class="subSectionTit snsIcon">みんなの声の公式SNSをフォローしよう！</h3>
    <p class="socialLink"><a id="footer_tw" href="https://twitter.com/_minnanokoe_/">{{ HTML::image("common/front/images/sp/btn_twitter.png","twitter")}}</a> <a id="footer_fb" href="https://www.facebook.com/minnanokoe.vote">{{ HTML::image("common/front/images/sp/btn_facebook.png", "facebook") }}</a></p>
</section>

<script type="text/javascript">
    $(document).ready(function(){
        $('#search_key').click(function( event ) {

            var search_text=$('input[name="search_key"]').val();
            if(search_text == '') {
                $('#search_key').parent().find('p').removeAttr('style');
                return false;
            } else {
                $('#search_key').parent().find('p').attr('style', 'display:none');
                location.href="{{ url('search/tag/') }}"+"/"+search_text
            
                return false;
            }
        });

        $('#head_search_key').click(function( event ) {
            var search_text=$('input[name="head_search_key"]').val();
            if(search_text == '') {
                $('#head_search_key').parent().find('p').removeAttr('style');
                return false;
            } else {
                $('#head_search_key').parent().find('p').attr('style', 'display:none');
                location.href="{{ url('search/tag/') }}"+"/"+search_text
            
                return false;
            }
        });
    });

    function validateSearch(input_name) {
        var search_text=$('input[name="'+ input_name +'"]').val();
        if (search_text == "") {
          $('#'+ input_name).parent().find('p').removeAttr('style');
          return false;
        } else {
            location.href="{{ url('search/tag/') }}"+"/"+search_text
            
            return false;
        }
    }
</script>
