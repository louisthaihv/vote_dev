
<table>
    <tbody>
        <tr>
            <td class="hidden" colspan="10"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="boR max hokkaidou {{ $totalChoiceList[$areaRanking[9]['map'] . '_rankCss'] }}" colspan="2">
                北海道
            </td>
        </tr>
        <tr>
            <td class="hidden min w60"><span>&nbsp;</span></td>
            <td class="hidden min w60"><span>&nbsp;</span></td>
            <td class="hidden min w30 boR2"><span>&nbsp;</span></td>
            <td class="hidden min w30"><span>&nbsp;</span></td>
            <td class="hidden min w60"><span>&nbsp;</span></td>
            <td class="hidden min w30"><span>&nbsp;</span></td>
            <td class="hidden min w30"><span>&nbsp;</span></td>
            <td class="hidden min w60"><span>&nbsp;</span></td>
            <td class="hidden min w60"><span>&nbsp;</span></td>
            <td class="hidden min w60"><span>&nbsp;</span></td>
            <td class="hidden min w60"><span>&nbsp;</span></td>
            <td class="hidden boT min w60"><span>&nbsp;</span></td>
            <td class="hidden boT min w60"><span>&nbsp;</span></td>
        </tr>
        <tr>
            <td class="boR {{ $totalChoiceList[$areaRanking[55]['map'] . '_rankCss'] }}" colspan="2">
                沖縄
            </td>
            <td class="hidden boR2"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="boR {{ $totalChoiceList[$areaRanking[10]['map'] . '_rankCss'] }}" colspan="2">
                青森
            </td>
        </tr>
        <tr>
            <td class="hidden boT boB2"><span>&nbsp;</span></td>
            <td class="hidden boT boB2"><span>&nbsp;</span></td>
            <td class="hidden boRB2"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="max {{ $totalChoiceList[$areaRanking[13]['map'] . '_rankCss'] }}" rowspan="2">
                秋田
            </td>
            <td class="boR max {{ $totalChoiceList[$areaRanking[11]['map'] . '_rankCss'] }}" rowspan="2">
                岩手
            </td>
        </tr>
        <tr>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
        </tr>
        <tr>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="boR max {{ $totalChoiceList[$areaRanking[25]['map'] . '_rankCss'] }}" rowspan="2">
                石川
            </td>
            <td class="hidden"><span>&nbsp;</span></td>
            <td class="max {{ $totalChoiceList[$areaRanking[14]['map'] . '_rankCss'] }}" rowspan="2">
                山形
            </td>
            <td class="boR max {{ $totalChoiceList[$areaRanking[12]['map'] . '_rankCss'] }}" rowspan="2">
                宮城
            </td>
        </tr>
        <tr>
            <td class="max {{ $totalChoiceList[$areaRanking[43]['map'] . '_rankCss'] }}" rowspan="2">
                山口
            </td>
            <td class="{{ $totalChoiceList[$areaRanking[40]['map'] . '_rankCss'] }}" colspan="3">
                島根
            </td>
            <td class="{{ $totalChoiceList[$areaRanking[39]['map'] . '_rankCss'] }}" colspan="3">
                鳥取
            </td>
            <td class="{{ $totalChoiceList[$areaRanking[26]['map'] . '_rankCss'] }}" colspan="2">
                福井
            </td>
            <td class="boNL max {{ $totalChoiceList[$areaRanking[23]['map'] . '_rankCss'] }}" rowspan="2">
                新潟
            </td>
        </tr>
        <tr>
            <td class="{{ $totalChoiceList[$areaRanking[42]['map'] . '_rankCss'] }}" colspan="3">
                広島
            </td>
            <td class="{{ $totalChoiceList[$areaRanking[41]['map'] . '_rankCss'] }}" colspan="3">
                岡山
            </td>
            <td class="{{ $totalChoiceList[$areaRanking[34]['map'] . '_rankCss'] }}" colspan="2">
                京都
            </td>
            <td class="boR max {{ $totalChoiceList[$areaRanking[24]['map'] . '_rankCss'] }}" rowspan="3">
                富山
            </td>
            <td class="boR {{ $totalChoiceList[$areaRanking[15]['map'] . '_rankCss'] }}" colspan="2">
                福島
            </td>
        </tr>
        <tr>
            <td class="hidden boT min"><span>&nbsp;</span></td>
            <td class="hidden boT min"><span>&nbsp;</span></td>
            <td class="hidden boT min"><span>&nbsp;</span></td>
            <td class="hidden boT min"><span>&nbsp;</span></td>
            <td class="{{ $totalChoiceList[$areaRanking[36]['map'] . '_rankCss'] }}" colspan="3" rowspan="2">
                兵庫
            </td>
            <td class="{{ $totalChoiceList[$areaRanking[33]['map'] . '_rankCss'] }}" colspan="2" rowspan="2">
                滋賀
            </td>
            <td class="boNL max {{ $totalChoiceList[$areaRanking[18]['map'] . '_rankCss'] }}" rowspan="4">
                群馬
            </td>
            <td class="max {{ $totalChoiceList[$areaRanking[17]['map'] . '_rankCss'] }}" rowspan="4">
                栃木
            </td>
            <td class="boR max {{ $totalChoiceList[$areaRanking[16]['map'] . '_rankCss'] }}" rowspan="4">
                茨城
            </td>
        </tr>
        <tr>
            <td class="boR {{ $totalChoiceList[$areaRanking[48]['map'] . '_rankCss'] }}" colspan="2" rowspan="2">
                福岡
            </td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
        </tr>
        <tr>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="{{ $totalChoiceList[$areaRanking[35]['map'] . '_rankCss'] }}" colspan="3" rowspan="2">
                大阪
            </td>
            <td class="{{ $totalChoiceList[$areaRanking[37]['map'] . '_rankCss'] }}" colspan="2" rowspan="2">
                奈良
            </td>
            <td class="boR max {{ $totalChoiceList[$areaRanking[28]['map'] . '_rankCss'] }}" rowspan="4">
                長野
            </td>
        </tr>
        <tr>
            <td class="boR {{ $totalChoiceList[$areaRanking[49]['map'] . '_rankCss'] }}" colspan="2" rowspan="2">
                佐賀
            </td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
        </tr>
        <tr>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden boT min"><span>&nbsp;</span></td>
            <td class="hidden boT min"><span>&nbsp;</span></td>
            <td class="hidden boT min"><span>&nbsp;</span></td>
            <td class="max {{ $totalChoiceList[$areaRanking[38]['map'] . '_rankCss'] }}" rowspan="4">
                和歌山
            </td>
            <td class="max {{ $totalChoiceList[$areaRanking[29]['map'] . '_rankCss'] }}" rowspan="4">
                岐阜
            </td>
            <td class="boNL {{ $totalChoiceList[$areaRanking[19]['map'] . '_rankCss'] }}" colspan="2" rowspan="2">
                埼玉
            </td>
            <td class="boR max {{ $totalChoiceList[$areaRanking[20]['map'] . '_rankCss'] }}" rowspan="4">
                千葉
            </td>
        </tr>
        <tr>
            <td class="boR {{ $totalChoiceList[$areaRanking[50]['map'] . '_rankCss'] }}" colspan="2" rowspan="2">
                長崎
            </td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="boR {{ $totalChoiceList[$areaRanking[45]['map'] . '_rankCss'] }}" colspan="3" rowspan="2">
                香川
            </td>
            <td class="hidden min"><span>&nbsp;</span></td>
        </tr>
        <tr>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="boR max {{ $totalChoiceList[$areaRanking[27]['map'] . '_rankCss'] }}" rowspan="4">
                山梨
            </td>
            <td class="boNL {{ $totalChoiceList[$areaRanking[21]['map'] . '_rankCss'] }}" colspan="2" rowspan="2">
                東京
            </td>
        </tr>
        <tr>
            <td class="boR {{ $totalChoiceList[$areaRanking[52]['map'] . '_rankCss'] }}" colspan="2" rowspan="2">
                大分
            </td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="boR {{ $totalChoiceList[$areaRanking[46]['map'] . '_rankCss'] }}" colspan="3" rowspan="2">
                愛媛
            </td>
            <td class="hidden min"><span>&nbsp;</span></td>
        </tr>
        <tr>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="{{ $totalChoiceList[$areaRanking[32]['map'] . '_rankCss'] }}" colspan="2" rowspan="2">
                三重
            </td>
            <td class="boNL {{ $totalChoiceList[$areaRanking[22]['map'] . '_rankCss'] }}" colspan="2" rowspan="2">
                神奈川
            </td>
            <td class="hidden min boLT"><span>&nbsp;</span></td>
        </tr>
        <tr>
            <td class="max {{ $totalChoiceList[$areaRanking[51]['map'] . '_rankCss'] }}" rowspan="4">
                熊本
            </td>
            <td class="boR max {{ $totalChoiceList[$areaRanking[53]['map'] . '_rankCss'] }}" rowspan="4">
                宮崎
            </td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="boR {{ $totalChoiceList[$areaRanking[44]['map'] . '_rankCss'] }}" colspan="3" rowspan="2">
                徳島
            </td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min boL"><span>&nbsp;</span></td>
        </tr>
        <tr>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="{{ $totalChoiceList[$areaRanking[31]['map'] . '_rankCss'] }}" colspan="2" rowspan="2">
                愛知
            </td>
            <td class="boR max {{ $totalChoiceList[$areaRanking[30]['map'] . '_rankCss'] }}" rowspan="4">
                静岡
            </td>
            <td class="hidden boT min"><span>&nbsp;</span></td>
            <td class="hidden boT min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
        </tr>
        <tr>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="boR {{ $totalChoiceList[$areaRanking[47]['map'] . '_rankCss'] }}" colspan="3" rowspan="2">
                高知
            </td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
        </tr>
        <tr>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden boT min"><span>&nbsp;</span></td>
            <td class="hidden boT min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
        </tr>
        <tr>
            <td class="boRB {{ $totalChoiceList[$areaRanking[54]['map'] . '_rankCss'] }}" colspan="2" rowspan="2">
                鹿児島
            </td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden boT min"><span>&nbsp;</span></td>
            <td class="hidden boT min"><span>&nbsp;</span></td>
            <td class="hidden boT min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
        </tr>
        <tr>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden boT min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
            <td class="hidden min"><span>&nbsp;</span></td>
        </tr>

    </tbody>
</table>
