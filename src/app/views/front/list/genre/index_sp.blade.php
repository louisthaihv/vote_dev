@extends('layouts.front_in')

@section('metaTag')
    <title>ジャンル一覧 | みんなの声</title>
    <meta name="description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！">
    <meta property="og:title" content="ジャンル一覧 | みんなの声" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
    <meta name="twitter:title" content="ジャンル一覧 | みんなの声" />
    <meta name="twitter:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
@stop

@section('content')

    <section class="l-margin">
        <h2 class="genreListTit"><span>ジャンル一覧</span></h2>
        @foreach($genreList as $parentGenre)
            <div class="l-margin">
                <h3 class="listSectionTit"><span>{{ $parentGenre->genre_parent_name }}</span></h3>
                <ul class="otherList">
                    @foreach(docomo\Models\Genres\ChildGenre::getDataByCache($parentGenre->id) as $childGenre)
                        <li>
                            <a href="{{ route('list.genre.result', [$parentGenre->genre_parent_directory, $childGenre->genre_child_directory]) }}">{{ $childGenre->genre_child_name }} ({{ $childGenre->record_count }})</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        @endforeach
        @include('layouts.front_elements._profileBn')
    </section>

@endsection
