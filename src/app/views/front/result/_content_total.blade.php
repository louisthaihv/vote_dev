<div class="resultBox" targetId="total">
@if (isset($csv) && $csv)
<p class="l-margin"><a href="csv/0" target="_blank">総合CSVダウンロード</a> | <a href="csv/0/{{ TYPE_D_MENU }}" target="_blank">総合CSVダウンロード(d版)</a> | <a href="csv/0/{{ TYPE_I_MODE }}" target="_blank">総合CSVダウンロード(i版)</a></p>
@endif
    @if ($question->isViewableAverage())
        <p class="avgData">平均：{{ ((isset($average) && !is_null($average)) ? number_format(intval($average)) . $question->average_unit : '') }}</p>
    @endif
    <div id="totalTable" class="resultTable">
        @include('front.result._result_table', ['resultData' => $result_data[TAB_TOTAL][TAB_TOTAL]])
    </div><!--/resultTable-->
</div><!--総合-->
