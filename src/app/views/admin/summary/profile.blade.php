@extends('layouts.admin_in')

@section('head')
<link rel="stylesheet" href="{{ url('common/admin/css/jquery-ui-1.8.23.custom.css') }}" type="text/css" />
<script type="text/javascript" src=" {{ url('common/admin/js/jquery-ui-1.8.23.custom.min.js') }}"></script>
<script type="text/javascript" src=" {{ url('common/admin/js/jquery.ui.datepicker-ja.js') }}"></script>
@stop

@section('javascript')
$(window).load(function(){
});
@stop

@section('style')
@stop

@section('content')
<h2>プロフィール登録累計</h2>

<p class="m-margin">プロフィール登録者数：{{ number_format($summaryData[0][0]) }}</p>
@foreach ($attributes as &$attribute)
<p class="boldFont">【{{ $attribute->attribute_name }}】</p>
<table class="list l-margin" style="margin-top:0px;">
    <tr><th width="100">{{ ($attribute->attribute_name == '性別') ? $attribute->attribute_name : str_replace('別', '', $attribute->attribute_name) }}</th><th>人数</th></tr>
    <?php $total = 0; ?>
    @foreach ($attributeValues[$attribute->id] as $values)
    <tr>
        <td>{{ $values->attribute_value_name }}</td>
        <td>{{ number_format($summaryData[$attribute->id][$values->id]) }}</td>
    </tr>
        <?php $total += $summaryData[$attribute->id][$values->id]; ?>
    @endforeach
    <tr><td>合計</td><td>{{ number_format($total) }}</td></tr>
</table>
@endforeach

@stop