<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('check collection');

$arr = array(
			'id' => '1',
			'title' => array('name' => 'title', 'value' => 'title_collection_1'),
			'message' => array('name' => 'message', 'value' => 'message_collection_1'),
			'date_from' => array('date_name' => 'from_date',
									'date_value' => '',
									'date_xpath' => './/*[@id="ui-datepicker-div"]/table/tbody/tr[4]/td[3]/a',
									'hour_name' => 'from_hour',
									'hour_value' => '06',
									'minute_name' => 'from_min',
									'minute_value' => '30',
								),
			'date_to' => array('date_name' => 'to_date',
									'date_value' => '',
									'date_xpath' => './/*[@id="ui-datepicker-div"]/table/tbody/tr[5]/td[4]/a',
									'hour_name' => 'to_hour',
									'hour_value' => '21',
									'minute_name' => 'to_min',
									'minute_value' => '15',
								),

			'is_view' => array('name' => 'is_view_list', 'check' => '1'),
			'is_column' => array('name' => 'is_feature_column', 'check' => '0'),

			'question' => array('name' => 'question_ids', 'value' => 'question_id_1'),
			);



$I->addFeature($arr);

//Todo: check show collection
$I->amOnPage('/feature');
$I->seeLink($arr['title']['value']);
$I->wait(1);

//Todo: check hiden collection
$arr['is_view']['check'] = 0;
$I->editFeature($arr);
$I->amOnPage('/feature');
$I->dontSeeLink($arr['title']['value']);
$I->wait(1);

