@section('script')
<script type="text/javascript">
$(window).load(function(){

    $('.commitBtn').click(function(){
        return (confirm('{{{ $confirmMessage }}}'));
    });
});
</script>
@stop

@include('admin.share.errors', ['errorList' => $keyword->errors()])
{{ Form::model($keyword, ['url' => $url, 'method' => $method]) }}
    <table class="formTable">
        @if (isset($keyword->id))
            <tr>
                <th>ID</th>
                <td>{{$keyword->id }}</td>
            </tr>
        @endif
        <tr>
            <th>キーワード名<span class="require">（必須）</span></th>
            <td>{{ Form::text('keyword_name', $keyword->keyword_name, ['class' => 'xx-large']) }}</td>
        </tr>
        <tr>
            <th>非表示</th>
            <td><label><input type="checkbox" name="is_visible" {{ (isset($keyword->is_visible) && $keyword->is_visible == HIDDEN) ? 'checked="checked"' : '' }} /> 非表示にする</label></td>
        </tr>
    </table>
    <div class="tcenter m-margint">
        <button type="submit" name="commit" class="commitBtn">{{ $button }}</button>
    </div>
{{ Form::close() }}
