<header id="top">
    <h1><a id="header_ban" href="{{ route('home.top') }}">{{ HTML::image('common/front/images/sp/logo.png', 'みんなの声') }}</a></h1>
    <p class="switch"><a href="#"><img alt="メニュー" src="/common/front/images/sp/menu.png"></a></p>
    <p class="mypage">
        <span id="attention_icon" style="display:none;"><img alt="exclamation" src="/common/front/images/sp/icon_attention.png"></span>
        <a id="header_mypage" href="{{ route('list.mypage') }}"><img alt="マイページ" src="/common/front/images/sp/mypage.png"></a></p>
    <script type="text/javascript">
        questionId = {{ (isset($question)) ? $question->id : 0 }};
        var hashVal = '{{ str_replace(D_MENU_HEADER, '', $currentUser->uid) }}';
        var viewedList = null;
        var questionList = null;
        $(document).ready(function() {
            var storage = window.localStorage;

            questionList = {{ $userIcons }};
            if (window.localStorage) {
                try {
                    viewedList = localStorage.getItem('viewedList');
                } catch (e) {

                }
                if (viewedList) {
                    viewedList = JSON.parse(viewedList);
                    if (!(viewedList instanceof Array)) {
                        viewedList = new Array(viewedList);
                    }
                }
                for (var forI = 0; forI < (questionList == null ? 0 : questionList.length); forI++) {
                    if (!viewedList || viewedList.indexOf(questionList[forI]) === -1) {
                        if (questionId && questionList[forI] === questionId) {
                            if (!viewedList) {
                                viewedList = new Array();
                            }
                            viewedList.push(questionId);
                            try {
                                storage.setItem('viewedList', JSON.stringify(viewedList));
                            } catch (e) {

                            }
                        } else {
                            $('#attention_icon').show();
                            if ($('#question' + questionList[forI])) {
                                $('#question' + questionList[forI]).show();
                            }
                        }
                    }
                }
            }

            var cookieHash = getCookie('hashVal');
            if (!cookieHash) {
                var localHash;
                if (window.localStorage) {
                    try {
                        localHash = storage.getItem('hashVal');
                    } catch (e) {

                    }
                }
                if (localHash) {
                    setCookie('hashVal', localHash);
                } else {
                    if (hashVal && window.localStorage) {
                        try {
                            storage.setItem('hashVal', hashVal);
                        } catch (e) {

                        }
                    }
                }
            }
        });
    </script>
</header>