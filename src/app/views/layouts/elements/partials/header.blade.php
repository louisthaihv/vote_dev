        <meta name="robots" content="noindex, nofollow" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="format-detection" content="telephone=no,address=no,email=no" />
        <meta name="description" content="" />
        <link href="{{ url('common/admin/css/reset.css') }}" type="text/css" rel="stylesheet"/>
        <link href="{{ url('common/admin/css/common.css') }}" type="text/css" rel="stylesheet"/>
        <script src="{{ url('common/admin/js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('common/admin/js/jquery-validate/jquery.validate.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('common/admin/js/common.js') }}"></script>
        <title>みんなの声 管理画面</title>        
        <link rel="stylesheet" href="{{ url('common/admin/css/jquery-ui.css') }}">
        <script src="{{ url('common/admin/js/jquery-ui.js') }}"></script>