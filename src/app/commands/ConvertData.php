<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use \docomo\Models\Answers\ResultVote;
use \docomo\Models\Ratings\VoteRatingTotal;
use \docomo\Models\Answers\TopSummaryByAttributeValue;
use \docomo\Models\Questions\Question;

class ConvertData extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'convert:old';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	private $accounts = [];

	private $attributeMap = [
        NULL => 0,
		30101 => 1,
		30102 => 2,
		30103 => 3,
		30104 => 4,
		30105 => 5,
		30106 => 6,
		10101 => 7,
		10102 => 8,
		20101 => 9,
		20202 => 10,
		20203 => 11,
		20204 => 13,
		20205 => 12,
		20206 => 14,
		20207 => 15,
		20308 => 23,
		20309 => 16,
		20310 => 17,
		20311 => 18,
		20312 => 28,
		20313 => 20,
		20314 => 19,
		20315 => 21,
		20316 => 22,
		20317 => 27,
		20418 => 29,
		20419 => 30,
		20420 => 31,
		20421 => 32,
		20522 => 24,
		20523 => 25,
		20524 => 26,
		20625 => 33,
		20626 => 34,
		20627 => 36,
		20628 => 35,
		20629 => 37,
		20630 => 38,
		20731 => 39,
		20732 => 40,
		20733 => 41,
		20734 => 42,
		20735 => 43,
		20836 => 45,
		20837 => 44,
		20838 => 46,
		20839 => 47,
		20940 => 48,
		20941 => 49,
		20942 => 50,
		20943 => 51,
		20944 => 52,
		20945 => 53,
		20946 => 54,
		20947 => 55
	];

    private $attributeIdMap = [
        ATTR_BIRTHDAY => 3,
        ATTR_GENDER => 1,
        ATTR_LOCAL => 2
    ];

	private $defaultParentGenre = 3;
	private $defaultChildGenre = 16;


	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        \DB::disableQueryLog();
        \DB::connection()->disableQueryLog();
        \DB::connection('mysql_convert')->disableQueryLog();
		$this->info($this->argument('date_from') . ' - ' . $this->argument('date_to'));
		if ($this->option('account')) {
			$this->insertAccount();
		} else {
			$accounts = \DB::select('SELECT id, login_id FROM accounts');
			foreach ($accounts as &$account) {
				$this->accounts[$account->login_id] = $account->id;
				unset($account);
			}
			unset($accounts);
		}
		if ($this->option('question')) {
			$this->insertQuestions();
		}
        if ($this->option('result')) {
            $this->summaryResult();
        }
		if ($this->option('user')) {
			$this->insertUser();
		}
		if ($this->option('answer')) {
			$this->insertAnswer();
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('date_from', InputArgument::REQUIRED, 'convert target from date.'),
			array('date_to', InputArgument::REQUIRED, 'convert target to date.'),
			array('status', InputArgument::OPTIONAL, 'convert target status [draft/open/end/hidden] (default : end).')
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('account', 'a', InputOption::VALUE_NONE, 'target account.', null),
			array('question', 'e', InputOption::VALUE_NONE, 'target question.', null),
            array('result', 's', InputOption::VALUE_NONE, 'target result.', null),
			array('answer', 'w', InputOption::VALUE_NONE, 'target answer.', null),
			array('user', 'u', InputOption::VALUE_NONE, 'target user.', null)
		);
	}

	private function insertAccount()
	{
		$this->info('account start.');

		$data = \DB::connection('mysql_convert')->select('SELECT * FROM operator');
		$now = new DateTime;
		foreach ($data as &$record) {
			$record->rep_id = str_replace(['-', '_'], '', $record->id);
			$account = \docomo\Models\Accounts\Account::whereLoginId($record->rep_id)->first();
			if ($account === null) {
				$this->info('add ' . $record->id);
				$account = new \docomo\Models\Accounts\Account;
			} else {
				$this->info('update ' . $record->id);
				$account->removeUniqueFromLoginId(true, true);
			}

			$account->account_name = $record->rep_id;
			$account->login_id = $record->rep_id;
			$account->login_password = $record->rep_id;
			$account->allow_genre_parent = DDL_ALL;
			$account->is_visible_register = $this->isAdmin($record->group_ids);
			$account->is_manage_genre = $this->isAdmin($record->group_ids);
			$account->is_manage_keyword = $this->isAdmin($record->group_ids);
			$account->is_manage_account = $this->isAdmin($record->group_ids);
			$account->is_manage_notice = $this->isAdmin($record->group_ids);
			$account->is_manage_feature = $this->isAdmin($record->group_ids);
			$account->is_manage_pickup = $this->isAdmin($record->group_ids);
			$account->is_manage_attention = $this->isAdmin($record->group_ids);
			$account->is_manage_agree = $this->isAdmin($record->group_ids);
			$account->is_manage_input_comment = $this->isAdmin($record->group_ids);
			$account->is_manage_column = $this->isAdmin($record->group_ids);
			$account->is_manage_summary = $this->isAdmin($record->group_ids);
			$account->is_viewable_all_question = $this->isAdmin($record->group_ids);
			$account->is_enabled = !$record->del_flg;
			$account->created_account_id = 0;
			if (!$account->save()) {
				var_dump($account->errors());
			}
			$this->accounts[$record->id] = $account->id;
			unset($record);
			unset($account);
		}

		$this->info('account end.');
	}

	private function isAdmin($groupIds) {
		return $groupIds === '0';
	}

	private function insertQuestions() {
		$this->info('question start');

		$targetStatusMap = [
			'draft' => 0,
			'open' => 1,
			'end' => 2,
			'hidden' => 9
		];
		$target = $this->argument('status');
		if (!isset($target)) {
			$targetStatus = 2;
		} else {
			$targetStatus = $targetStatusMap[$target];
		}
		$targetDate = [
			0 => 'update_date',
			1 => 'start_date',
			2 => 'end_date',
			9 => 'update_date'
		];

		$data = \DB::connection('mysql_convert')->select('SELECT * FROM question WHERE status = ? AND '.
			' ((start_date BETWEEN ? AND ?) OR (end_date BETWEEN ? AND ?) OR (update_date BETWEEN ? AND ?)) ',
			[$targetStatus, $this->argument('date_from'), $this->argument('date_to'),
				$this->argument('date_from'), $this->argument('date_to'),
				$this->argument('date_from'), $this->argument('date_to')]);
		$this->info('target record is ' . count($data));

		// 下書き以外は全部受付終了
		$statusMap = [
			0 => 1,
			1 => 6,
			2 => 6,
			9 => 6
		];
        \docomo\Models\Questions\Choice::$rules = [];
        \docomo\Models\Questions\Navigation::$rules = [];
        Question::$rules = [];
        \docomo\Models\Questions\Column::$rules = [];
		foreach ($data as &$record) {
            $this->info('id : ' . $record->id);
			$questionId = \DB::select('SELECT new_id, old_id FROM convert_id WHERE old_id = ?', [$record->id]);
			$otherProperty = 'answer_' . $record->answer_other_num;
			if (!is_null($record->start_date)) {
				$record->start_date = substr($record->start_date, 0, 15) . '0:00';
			} else {
                $record->start_date = $record->create_date;
            }
			if (!is_null($record->end_date)) {
				$record->end_date = substr($record->end_date, 0, 15) . '0:00';
			} else {
                $record->end_date = $record->start_date;
            }

			if (!isset($questionId) || count($questionId) == 0) {
                $this->info('add');
				$questionId = \DB::table('questions')->insertGetId([
					'genre_parent_id' => $this->defaultParentGenre,
					'genre_child_id' => $this->defaultChildGenre,
					'status' => $statusMap[$record->status],
					'title' => $record->title,
					'details' => $record->description,
					'vote_date_from' => $record->start_date,
					'vote_date_to' => $record->end_date,
					'baseline_date' => $record->end_date,
					'viewable_type' => ($record->del_flg == 1 || $record->public_status == 0) ? HIDE_ALL : DISPLAY,
					'visible_device_i' => $record->mob_flg,
					'visible_device_d' => $record->smp_flg,
					'summary_type' => SUMMARY_NORMAL,
					'average_unit' => '',
					'is_visible_other' => ($record->answer_other_num > 0),
					'other_text' => ($record->answer_other_num > 0) ? $record->$otherProperty : null,
					'shuffle_choice' => $record->shuffle_flg,
					'result_comment' => ($record->result_end_comment == 0) ? null : $record->result_end_comment,
					'navigation_title' => 'おすすめコンテンツ',
					'comment' => 'old id : ' . $record->id,
					'back_button_text' => $record->backlink_title,
					'back_button_url' => $record->backlink_url,
					'has_column' => isset($record->result_column_start_date),
					'account_id' => (is_null($record->operator_id) ?
						reset($this->accounts) : $this->accounts[trim(str_replace('_', '', $record->operator_id))]),
					'created_at' => $record->create_date,
					'updated_at' => $record->update_date
				]);
				\DB::insert('INSERT INTO convert_id VALUES (?, ?)', [$questionId, $record->id]);
			} else {
                $this->info('update ' . $questionId[0]->old_id . ' to ' . $questionId[0]->new_id);
				$questionId = $questionId[0]->new_id;
                $question = Question::find($questionId);
                $question->status = $statusMap[$record->status];
                $question->title = $record->title;
                $question->details = $record->description;
                $question->vote_date_from = $record->start_date;
                $question->vote_date_to = $record->end_date;
                $question->baseline_date = $record->end_date;
                $question->viewable_type = ($record->del_flg == 1 || $record->public_status == 0) ? HIDE_ALL : DISPLAY;
                $question->visible_device_i = $record->mob_flg;
                $question->visible_device_d = $record->smp_flg;
                $question->is_visible_other = ($record->answer_other_num > 0);
                $question->other_text = ($record->answer_other_num > 0) ? $record->$otherProperty : null;
                $question->shuffle_choice = $record->shuffle_flg;
                $question->result_comment = ($record->result_end_comment == 0) ? null : $record->result_end_comment;
                $question->back_button_text = $record->backlink_title;
                $question->back_button_url = $record->backlink_url;
                $question->has_column = isset($record->result_column_start_date);
                $question->updated_at = $record->update_date;
                $question->save();
                unset($question);
			}
            $choiceList = [];
			for ($forI = 1; $forI <= $record->answer_num; $forI++) {
				$columnName = 'answer_' . $forI;
				if (!is_null($record->$columnName) && ($record->$columnName != '')) {
					if ($record->answer_other_num != 0 && $record->answer_other_num == $forI) {
						$choice_num = 0;
					} else {
						$choice_num = $forI;
					}
					$choice = \docomo\Models\Questions\Choice::
						select('id', 'question_id', 'choice_num', 'choice_text')
						->whereQuestionId($questionId)
						->whereChoiceNum($choice_num)->first();
					if (is_null($choice)) {
						$choice = new \docomo\Models\Questions\Choice();
						$choice->question_id = $questionId;
						$choice->choice_num = $choice_num;
					}
					if ($choice->choice_text != $record->$columnName) {
						$choice->choice_text = $record->$columnName;
						$choice->save();
					}
                    $choiceList[$choice->choice_num] = $choice->id;
					unset($choice);
				}
				unset($columnName);
			}
			\DB::delete('DELETE FROM navigations WHERE question_id = ?', [$questionId]);
			for ($forI = 1; $forI <= 10; $forI++) {
				$titleColumnName = 'related_title_' . $forI;
				$urlColumnName = 'related_url_' . $forI;
                if (is_null($record->$titleColumnName) || is_null($record->$urlColumnName)
                    || $record->$titleColumnName == '' || $record->$urlColumnName == '') {
                    continue;
                }
				$navigation = new \docomo\Models\Questions\Navigation();
				$navigation->question_id = $questionId;
				$navigation->view_type = TYPE_D_MENU;
				$navigation->row = $forI;
				$navigation->description = $record->$titleColumnName;
				$navigation->url = $record->$urlColumnName;
				$navigation->save();
			}
			for ($forI = 1; $forI <= 5; $forI++) {
				$titleColumnName = 'cp_title_' . $forI;
				$urlColumnName = 'cp_url_' . $forI;
                if (is_null($record->$titleColumnName) || is_null($record->$urlColumnName)
                    || $record->$titleColumnName == '' || $record->$urlColumnName == '') {
                    continue;
                }
				$navigation = new \docomo\Models\Questions\Navigation();
				$navigation->question_id = $questionId;
				$navigation->view_type = TYPE_I_MODE;
				$navigation->row = $forI;
				$navigation->description = $record->$titleColumnName;
				$navigation->url = $record->$urlColumnName;
				$navigation->save();
			}
            if (!is_null($record->result_column_title) &&
                !is_null($record->result_column) &&
                !is_null($record->result_column_start_date) &&
                $record->result_column_title != '' &&
                $record->result_column != '' &&
                $record->result_column_start_date != '') {
                $column = \docomo\Models\Questions\Column::whereQuestionId($questionId)->first();
                if (is_null($column)) {
                    $column = new \docomo\Models\Questions\Column();
                }
                $column->question_id = $questionId;
                $column->is_visible = is_null($record->result_column_end_date);
                $column->column_title = $record->result_column_title;
                $column->column_detail = $record->result_column;
                $column->view_create_datetime = $record->result_column_start_date;
                $column->created_account_id = 0;
                $column->updated_account_id = 0;
                $column->save();
            }
            $question = new Question();
            $question->id = $questionId;
            $question->summary_type = SUMMARY_NORMAL;
            $question->attribute_id = null;
            \DB::delete('DELETE FROM result_votes WHERE question_id=?', [$questionId]);
            ResultVote::createAllRecord($question);
            VoteRatingTotal::createAllRecord($question->id);
            TopSummaryByAttributeValue::createAllRecord($question);
            \DB::update('UPDATE result_votes SET voted_count = 0 WHERE question_id=?', [$questionId]);
            $total = \DB::connection('mysql_convert')->select(
                'SELECT answer_num, answer_other_num, SUM(answer_1) as answer_1, SUM(answer_2) as answer_2, SUM(answer_3) as answer_3, SUM(answer_4) as answer_4, SUM(answer_5) as answer_5, SUM(answer_6) as answer_6, SUM(answer_7) as answer_7, SUM(answer_8) as answer_8, SUM(answer_9) as answer_9, SUM(answer_10) as answer_10, SUM(answer_11) as answer_11, SUM(answer_12) as answer_12, SUM(answer_13) as answer_13, SUM(answer_14) as answer_14, SUM(answer_15) as answer_15, SUM(answer_16) as answer_16, SUM(answer_17) as answer_17, SUM(answer_18) as answer_18, SUM(answer_19) as answer_19, SUM(answer_20) as answer_20, SUM(answer_21) as answer_21, SUM(answer_22) as answer_22, SUM(answer_23) as answer_23, SUM(answer_24) as answer_24, SUM(answer_25) as answer_25, SUM(answer_26) as answer_26, SUM(answer_27) as answer_27, SUM(answer_28) as answer_28, SUM(answer_29) as answer_29, SUM(answer_30) as answer_30, SUM(answer_31) as answer_31, SUM(answer_32) as answer_32, SUM(answer_33) as answer_33, SUM(answer_34) as answer_34, SUM(answer_35) as answer_35, SUM(answer_36) as answer_36, SUM(answer_37) as answer_37, SUM(answer_38) as answer_38, SUM(answer_39) as answer_39, SUM(answer_40) as answer_40, SUM(answer_41) as answer_41, SUM(answer_42) as answer_42, SUM(answer_43) as answer_43, SUM(answer_44) as answer_44, SUM(answer_45) as answer_45, SUM(answer_46) as answer_46, SUM(answer_47) as answer_47, SUM(answer_48) as answer_48, SUM(answer_49) as answer_49, SUM(answer_50) as answer_50, SUM(answer_51) as answer_51, SUM(answer_52) as answer_52, SUM(answer_53) as answer_53, SUM(answer_54) as answer_54, SUM(answer_55) as answer_55, SUM(answer_56) as answer_56, SUM(answer_57) as answer_57, SUM(answer_58) as answer_58, SUM(answer_59) as answer_59, SUM(answer_60) as answer_60 FROM shukei WHERE question_id = ? GROUP BY answer_num, answer_other_num;',
                [$record->id]);
            $totalCount = 0;
            foreach ($total as &$totalRec) {
                for ($forI = 1; $forI <= $totalRec->answer_num; $forI++) {
                    $columnName = 'answer_' . $forI;
                    if ($totalRec->$columnName == 0) {
                        continue;
                    }
                    if ($forI == $totalRec->answer_other_num) {
                        if (!array_key_exists(0, $choiceList)) {
                            continue;
                        }
                        $choice_id = $choiceList[0];
                    } else {
                        if (!array_key_exists($forI, $choiceList)) {
                            continue;
                        }
                        $choice_id = $choiceList[$forI];
                    }
                    \DB::update('UPDATE result_votes SET voted_count = voted_count + ? ' .
                        ' WHERE question_id=? AND attribute_id=1 AND attribute_value_id=0 AND choice_id=? AND device=? ',
                        [$totalRec->$columnName, $questionId, $choice_id, TYPE_I_MODE]);
                    $totalCount += $totalRec->$columnName;
                    unset($choice_id);
                }
                unset($totalRec);
            }
            \DB::update('UPDATE questions SET vote_count=? WHERE id=?',
                [$totalCount, $questionId]);
            unset($total);
            foreach ($this->attributeIdMap as $newId => &$oldId) {
                $summary = \DB::connection('mysql_convert')->select(
                    'SELECT answer_num, answer_other_num, attribute_id, SUM(answer_1) as answer_1, SUM(answer_2) as answer_2, SUM(answer_3) as answer_3, SUM(answer_4) as answer_4, SUM(answer_5) as answer_5, SUM(answer_6) as answer_6, SUM(answer_7) as answer_7, SUM(answer_8) as answer_8, SUM(answer_9) as answer_9, SUM(answer_10) as answer_10, SUM(answer_11) as answer_11, SUM(answer_12) as answer_12, SUM(answer_13) as answer_13, SUM(answer_14) as answer_14, SUM(answer_15) as answer_15, SUM(answer_16) as answer_16, SUM(answer_17) as answer_17, SUM(answer_18) as answer_18, SUM(answer_19) as answer_19, SUM(answer_20) as answer_20, SUM(answer_21) as answer_21, SUM(answer_22) as answer_22, SUM(answer_23) as answer_23, SUM(answer_24) as answer_24, SUM(answer_25) as answer_25, SUM(answer_26) as answer_26, SUM(answer_27) as answer_27, SUM(answer_28) as answer_28, SUM(answer_29) as answer_29, SUM(answer_30) as answer_30, SUM(answer_31) as answer_31, SUM(answer_32) as answer_32, SUM(answer_33) as answer_33, SUM(answer_34) as answer_34, SUM(answer_35) as answer_35, SUM(answer_36) as answer_36, SUM(answer_37) as answer_37, SUM(answer_38) as answer_38, SUM(answer_39) as answer_39, SUM(answer_40) as answer_40, SUM(answer_41) as answer_41, SUM(answer_42) as answer_42, SUM(answer_43) as answer_43, SUM(answer_44) as answer_44, SUM(answer_45) as answer_45, SUM(answer_46) as answer_46, SUM(answer_47) as answer_47, SUM(answer_48) as answer_48, SUM(answer_49) as answer_49, SUM(answer_50) as answer_50, SUM(answer_51) as answer_51, SUM(answer_52) as answer_52, SUM(answer_53) as answer_53, SUM(answer_54) as answer_54, SUM(answer_55) as answer_55, SUM(answer_56) as answer_56, SUM(answer_57) as answer_57, SUM(answer_58) as answer_58, SUM(answer_59) as answer_59, SUM(answer_60) as answer_60 FROM shukei_attribute_' . $oldId . ' WHERE question_id = ? GROUP BY answer_num, answer_other_num, attribute_id ',
                    [$record->id]);

                foreach ($summary as &$summaryRec) {
                    $attributeValueId = $this->attributeMap[$summaryRec->attribute_id];
                    for ($forI = 1; $forI <= $summaryRec->answer_num; $forI++) {
                        $columnName = 'answer_' . $forI;
                        if ($summaryRec->$columnName == 0) {
                            continue;
                        }
                        if ($forI == $summaryRec->answer_other_num) {
                            if (!array_key_exists(0, $choiceList)) {
                                continue;
                            }
                            $choice_id = $choiceList[0];
                        } else {
                            if (!array_key_exists($forI, $choiceList)) {
                                continue;
                            }
                            $choice_id = $choiceList[$forI];
                        }
                        \DB::update('UPDATE result_votes SET voted_count = voted_count + ? ' .
                            ' WHERE question_id=? AND attribute_id=? AND attribute_value_id=? AND choice_id=? AND device=? ',
                            [$summaryRec->$columnName, $questionId, $newId, $attributeValueId, $choice_id, TYPE_I_MODE]);
                        unset($choice_id);
                    }
                    unset($summaryRec);
                }
                unset($summary);
                unset($oldId);
            }
            $attributeSummary = \DB::select('SELECT choice_id, SUM(voted_count) as sum_vote FROM result_votes ' .
                ' WHERE question_id=? AND attribute_id=1 AND attribute_value_id>0 GROUP BY choice_id ',
                [$questionId]);
            foreach ($attributeSummary as &$rec) {
                \DB::update('UPDATE result_votes SET voted_count = voted_count - ? ' .
                    ' WHERE question_id=? AND attribute_id=1 AND attribute_value_id=0 AND choice_id =? AND device=?',
                    [$rec->sum_vote, $questionId, $rec->choice_id, TYPE_I_MODE]);
            }
            $resultVote = ResultVote::select('id', 'attribute_id', 'attribute_value_id', 'device',
                'choice_id', 'voted_count', 'summary_value')
                ->whereQuestionId($question->id)
                ->orderBy('attribute_id')->orderBy('attribute_value_id')->orderBy('choice_id')->get()->toArray();
            $topData = ResultVote::sortByData($resultVote, $question->id);
            TopSummaryByAttributeValue::updateTopByArray($question->id, $topData);
            unset($resultVote);
            unset($question);
			unset($record);
            unset($choiceList);
		}

		$this->info('question end');
	}

    private function summaryResult() {
        $this->info('summary result start.');
        $questionList = \DB::connection('mysql_convert')
            ->select('SELECT id, answer_other_num, answer_num FROM question WHERE ? BETWEEN start_date AND end_date', ['2014-12-02 00:00:00']);
        foreach ($questionList as &$question) {
            $questionId = \DB::select('SELECT new_id FROM convert_id WHERE old_id = ?', [$question->id])[0]->new_id;
            \DB::update('UPDATE result_votes SET voted_count=0 WHERE question_id = ?', [$questionId]);
            $choices = \DB::select('SELECT id, choice_num FROM choices WHERE question_id = ?', [$questionId]);
            $choiceList = [];
            foreach ($choices as &$choice) {
                $choiceList[$choice->choice_num] = $choice->id;
                unset($choice);
            }
            unset($choices);
            $summaryColumns = '';
            for ($colI = 1; $colI <= $question->answer_num; $colI++) {
                $summaryColumns .= ', SUM(answer_' . $colI . ') as answer_' . $colI;
            }
            foreach ($this->attributeIdMap as $newId => &$oldId) {
                $summaryData = \DB::connection('mysql_convert')
                    ->select('SELECT attribute_1 as attribute' . $summaryColumns .'  FROM answer WHERE question_id = ?', [$question->id]);

                foreach ($summaryData as &$summaryRec) {
                    $attributeValueId = $this->attributeMap[$summaryRec->attribute];
                    for ($forI = 1; $forI <= $question->answer_num; $forI++) {
                        $columnName = 'answer_' . $forI;
                        if ($summaryRec->$columnName == 0) {
                            continue;
                        }
                        if ($forI == $question->answer_other_num) {
                            if (!array_key_exists(0, $choiceList)) {
                                continue;
                            }
                            $choice_id = $choiceList[0];
                        } else {
                            if (!array_key_exists($forI, $choiceList)) {
                                continue;
                            }
                            $choice_id = $choiceList[$forI];
                        }
                        \DB::update('UPDATE result_votes SET voted_count = voted_count + ? ' .
                            ' WHERE question_id=? AND attribute_id=? AND attribute_value_id=? AND choice_id=? AND device=? ',
                            [$summaryRec->$columnName, $questionId, $newId, $attributeValueId, $choice_id, TYPE_I_MODE]);
                        unset($choice_id);
                    }
                }
            }
            unset($choiceList);
            unset($questionId);
        }
        unset($questionList);
    }

	private function insertUser() {
		$this->info('user start.');

        for ($hourI = 0; $hourI < 24; $hourI++) {
            for ($minutesI = 0; $minutesI <= 55; $minutesI+= 5) {
                $data = \DB::connection('mysql_convert')->select('SELECT * FROM user WHERE (update_date >= DATE_ADD(?, INTERVAL ? HOUR) AND update_date < DATE_ADD(?, INTERVAL ? HOUR)) ',
                    [
                        $this->argument('date_from'),
                        $hourI * 60 + $minutesI,
                        $this->argument('date_from'),
                        $hourI * 60 + $minutesI + 11
                    ]);

                foreach ($data as &$record) {
                    $this->info($record->uid);
                    $uid = ((substr($record->uid, 0, 3) === 'BIZ') ? D_MENU_HEADER : I_MODE_HEADER) . $record->uid;
                    $user = \docomo\Models\Users\User::findUid($uid)->first();
                    $userId = \DB::select('SELECT new_id FROM convert_user_id WHERE old_id = ?', [$record->id]);
                    if ($userId === null || count($userId) === 0) {
                        if (is_null($user)) {
                            \DB::insert('INSERT INTO users (uid, birthday, created_at, updated_at) VALUES (?, ?, ?, ?)',
                                [$uid, $record->birth_date, $record->create_date, $record->update_date]);
                            $userId = \DB::select('SELECT id FROM users WHERE uid = ?', [$uid])[0]->id;
                            \DB::insert('INSERT INTO convert_user_id VALUES (?, ?)',
                                [$userId, $record->id]);
                        } else {
                            continue;
                        }
                    } else {
                        $userId = $userId[0]->new_id;
                        \DB::update('UPDATE users SET birthday=?, updated_at = ? WHERE id = ?',
                            [$record->birth_date, $record->update_date, $userId]);
                    }

                    unset($uid);

                    $this->info('update attribute.');
                    \DB::delete('DELETE FROM user_attributes WHERE user_id = ?', [$userId]);
                    for ($forI = 1; $forI <= 3; $forI++) {
                        $columnName = 'attribute_' . $forI;
                        if (!is_null($record->$columnName)) {
                            $attributeId = $this->attributeMap[$record->$columnName];
                            $attribute = \docomo\Models\Attributes\AttributeValue::select('id', 'attribute_id')->find($attributeId);
                            $userAttribute = new \docomo\Models\Users\UserAttribute();
                            $userAttribute->user_id = $userId;
                            $userAttribute->attribute_id = $attribute->attribute_id;
                            $userAttribute->attribute_value_id = $attribute->id;
                            $userAttribute->save();
                            unset($attributeId);
                            unset($attribute);
                            unset($userAttribute);
                        }
                        unset($columnName);
                    }
                    unset($user);
                    unset($record);
                }
                unset($data);
            }
        }
		unset($attributeMap);

		$this->info('user end.');
	}

	private function insertAnswer() {
		$this->info('answer start.');

        $questionCount = \DB::select('SELECT MAX(new_id) as maxId FROM convert_id');
        for ($questionI = 1; $questionI < $questionCount[0]->maxId; $questionI++) {
            $this->info('target question is '.$questionI);
            $ignoreList = [];
            $question = \DB::select('SELECT old_id FROM convert_id WHERE new_id = ?', [$questionI]);
            // 取得できない場合は次へ
            if (!isset($question) || count($question) === 0) continue;
            $questionId = $question[0]->old_id;
            $answerCount = \DB::connection('mysql_convert')->select('SELECT COUNT(*) as recordCount FROM answer WHERE question_id = ?',
                [$questionId]);
            $this->info('target answer is ' . $answerCount[0]->recordCount);

            for ($ansI = 0; $ansI < $answerCount[0]->recordCount; $ansI +=100) {
                $data = \DB::connection('mysql_convert')
                    ->select('SELECT * FROM answer WHERE question_id = ? ORDER BY user_id LIMIT ?, 100',
                    [$questionId, $ansI]);
                foreach ($data as &$record) {
                    $question_id = $questionI;
                    $user_id = \DB::select('SELECT new_id FROM convert_user_id WHERE old_id = ?', [$record->user_id]);
                    if (!isset($user_id) || count($user_id) === 0) {
                        $user = new \docomo\Models\Users\User();
                        $user->save();
                        \DB::insert('INSERT INTO convert_user_id VALUES (?, ?)',
                            [$user->id, $record->user_id]);
                        $user_id = $user->id;
                    } else {
                        $user_id = $user_id[0]->new_id;
                    }
                    for ($forI = 1; $forI < 65; $forI ++) {
                        $columnName = 'answer_' . $forI;
                        if ($record->$columnName != 1) {
                            continue;
                        }
                        $choice_id = \DB::select('SELECT id FROM choices WHERE question_id = ? AND choice_num = ?',
                            [$question_id, $forI]);
                        if (!isset($choice_id) || count($choice_id) === 0) {
                            $choice_id = \DB::select('SELECT id FROM choices WHERE question_id = ? AND choice_num = 0',
                                [$question_id]);
                        }
                        if (!isset($choice_id)) {
                            $ignoreList[] = [$record->id => 'No Choice.'];
                            continue;
                        }
                        $answer = \docomo\Models\Answers\Answer::whereRaw('`question_id`=? AND `user_id`=? AND `choice_id`=?',
                            [$question_id, $user_id, $choice_id[0]->id])->first();
                        if (is_null($answer)) {
                            $answer = new \docomo\Models\Answers\Answer();
                            $answer->question_id = $question_id;
                            $answer->user_id = $user_id;
                            $answer->choice_id = $choice_id[0]->id;
                            if (!is_null($record->attribute_3)) {
                                $answer->age = $this->attributeMap[$record->attribute_3];
                            }
                            if (!is_null($record->attribute_1)) {
                                $answer->gender = $this->attributeMap[$record->attribute_1];
                            }
                            if (!is_null($record->attribute_2)) {
                                $answer->local = $this->attributeMap[$record->attribute_2];
                            }
                            $answer->device = $record->attribute_5 == 1 ? TYPE_I_MODE : TYPE_D_MENU;
                            $answer->answer_date = $record->answer_date;
                            $answer->save();
                        }
                        unset($answer);
                    }
                    unset($record);
                }
                unset($data);
            }
            $answerCount = \DB::connection('mysql_convert')->select('SELECT COUNT(*) as recordCount FROM user_answer WHERE question_id = ?',
                [$questionId]);
            $this->info('target answer is ' . $answerCount[0]->recordCount);

            for ($ansI = 0; $ansI < $answerCount[0]->recordCount; $ansI +=100) {
                $data = \DB::connection('mysql_convert')
                    ->select('SELECT * FROM user_answer WHERE question_id = ? ORDER BY user_id LIMIT ?, 100',
                        [$questionId, $ansI]);

                foreach ($data as &$record) {
                    $question_id = $questionI;
                    $user_id = \DB::select('SELECT new_id FROM convert_user_id WHERE old_id = ?', [$record->user_id]);
                    if (!isset($user_id) || count($user_id) === 0) {
                        $user = new \docomo\Models\Users\User();
                        $user->save();
                        \DB::insert('INSERT INTO convert_user_id VALUES (?, ?)',
                            [$user->id, $record->user_id]);
                        $user_id = $user->id;
                    } else {
                        $user_id = $user_id[0]->new_id;
                    }
                    for ($forI = 1; $forI < 65; $forI++) {
                        $columnName = 'answer_' . $forI;
                        if ($record->$columnName != 1) {
                            continue;
                        }
                        $choice_id = \DB::select('SELECT id FROM choices WHERE question_id = ? AND choice_num = ?',
                            [$question_id, $forI]);
                        if (!isset($choice_id) || count($choice_id) === 0) {
                            $choice_id = \DB::select('SELECT id FROM choices WHERE question_id = ? AND choice_num = 0',
                                [$question_id]);
                        }
                        if (!isset($choice_id)) {
                            $ignoreList[] = [$record->id => 'No Choice.'];
                            continue;
                        }
                        $answer = \docomo\Models\Answers\Answer::whereRaw('`question_id`=? AND `user_id`=? AND `choice_id`=?',
                            [$question_id, $user_id, $choice_id[0]->id])->first();
                        if (is_null($answer)) {
                            $answer = new \docomo\Models\Answers\Answer();
                            $answer->question_id = $question_id;
                            $answer->user_id = $user_id;
                            $answer->choice_id = $choice_id[0]->id;
                            $answer->device = TYPE_I_MODE;
                            $answer->answer_date = $record->answer_date;
                            $answer->save();
                        }
                        unset($answer);
                    }
                    unset($record);
                }
                unset($data);
            }
        }


		$this->info('answer end.');
	}
}
