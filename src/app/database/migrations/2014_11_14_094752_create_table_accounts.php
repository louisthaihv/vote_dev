<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableAccounts extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account_name', 60)->unique();
            $table->string('login_id', 60)->unique();
            $table->string('login_password');
            $table->integer('allow_category_parent');
            $table->boolean('is_visible_register')->default(0);
            $table->boolean('is_manage_category')->default(0);
            $table->boolean('is_manage_keyword')->default(0);
            $table->boolean('is_manage_account')->default(0);
            $table->boolean('is_manage_notice')->default(0);
            $table->boolean('is_manage_special')->default(0);
            $table->boolean('is_manage_pickup_information')->default(0);
            $table->boolean('is_manage_agree')->default(0);
            $table->boolean('is_manage_summary')->default(0);
            $table->boolean('is_editable_all_question')->default(0);
            $table->boolean('is_enabled')->default(0);
            $table->boolean('is_manage_input_comment')->default(0);
            $table->boolean('is_manage_column')->default(0);
            $table->integer('created_account_id');
            $table->string('remember_token')->nullable();
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
