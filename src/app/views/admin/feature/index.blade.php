@extends('layouts.admin_in')

@section('javascript')
    $(window).load(function(){
        $('.delete').click(function(e){
            return confirm('{{ C_DELETE }}');
        });
    });
@stop

@section('content')
<h2>TOPページ特集枠データ一覧</h2>
<table class="list">
    <tr>
        <th>ID</th>
        <th>タイトル</th>
        <th>コラムフラグ</th>
        <th style="width:90px;">開始</th>
        <th style="width:90px;">終了</th>
        <th>一覧表示フラグ</th>
        <th>削除</th>
    </tr>
    @foreach ($feature_list as $feature)
    <tr class="{{ ($feature->isEnd()) ? 'expiration' : '' }}">
        <td>{{ $feature->id }}</td>
        <td>{{ link_to_route('manage.feature.edit', $feature->title, $feature->id) }}</td>
        <td>{{ ($feature->is_feature_column) ? L_COLUMN : '' }}</td>
        <td>{{ date('Y-m-d H:i', strtotime($feature->from)) }}</td>
        <td>{{ date('Y-m-d H:i', strtotime($feature->to)) }}</td>
        <td>{{ ($feature->is_view_list) ? L_VISIBLE : '' }}</td>
        <td>
            {{ Form::open(['url' => 'manage/feature/' . $feature->id, 'method' => 'delete']) }}
                <button type="submit" class="naviBtn delete">削除</button>
            {{ Form::close() }}
        </td>
    </tr>
    @endforeach
</table>
@stop