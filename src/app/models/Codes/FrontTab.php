<?php

namespace docomo\Models\Codes;

class FrontTab
{
    public static $label = array(
        PROCESSING => '受付中',
        COMPLETE => '結果発表',
    );
}
