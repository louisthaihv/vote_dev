<?php
$I = new AcceptanceTester\MypageSteps($scenario);
$I->wantTo('check FP mypage');

$profile = array(
				'sex' => array('name' => 'sex', 'value' => '男性'),
				'year' => array('name' => 'birthday_year', 'value' => '1990'),
				'month' => array('name' => 'birthday_month', 'value' => '06'),
				'day' => array('name' => 'birthday_day', 'value' => '01'),
				'place' => array('name' => 'pref', 'value' => '京都府'),
				'submit' => '登録する',
				);

//create question
$genre_parent = array(
                    1 => array(
                                'genre_parent_name' => 'genre_1',
                                'parent_is_visible' => '1',
                                'genre_parent_directory' => 'everyday',
                                'parent_sort_order' => '10',
                                )
                );
$I->createDbGenreParent($genre_parent);

$genre_child = array(
                    1 => array(
                                'genre_parent_id'=>'1',
                                'genre_child_name'=>'genre_child_1_1',
                                'child_is_visible'=>'1',
                                'genre_child_directory'=>'1',
                                'child_sort_order'=>'10'
                                )
                );
$I->createDbGenreChild($genre_child);

//create survey result
$survies_result = array();
for ($i=1; $i <= 25; $i++) {
    $title = 'question_result_test_'.$i;
    $survies_result[$i] = array(
                        'genre_parent_id'=>'1',
                        'genre_child_id'=>'1',
                        'status'=>'6',
                        'title'=>$title,
                        'vote_date_from'=>'2014-12-05 00:00:00',
                        'vote_date_to'=>'2014-12-10 00:00:00',
                        'baseline_date'=>'2014-12-10 00:00:00',
                        'viewable_type'=>'1',
                        'visible_device_i'=>'1',
                        'visible_device_d'=>'1',
                        'summary_type'=>'1',
                        'average_unit'=>'1',
                        'is_visible_other'=>'1',
                        'shuffle_choice'=>'1',
                        'navigation_title'=>'1',
                        'account_id'=>'1',
                        );
}
$I->createDbQuestion($survies_result);

//order list to see
// krsort($survies_result);
$survies_result_check = array();
foreach ($survies_result as $survey) {
    $survies_result_check[] = array('title' => $survey['title'], 'status' => '1');
}

//create survey entry
$survies_entry = array();
for ($i=1; $i <= 11; $i++) {
    $title = 'question_entry_test_'.($i+20);
    $survies_entry[$i] = array(
                        'genre_parent_id'=>'1',
                        'genre_child_id'=>'1',
                        'status'=>'5',
                        'title'=>$title,
                        'vote_date_from'=>'2015-01-10 00:00:00',
                        'vote_date_to'=>'2015-03-10 00:00:00',
                        'baseline_date'=>'2015-03-20 00:00:00',
                        'viewable_type'=>'1',
                        'visible_device_i'=>'1',
                        'visible_device_d'=>'1',
                        'summary_type'=>'1',
                        'average_unit'=>'1',
                        'is_visible_other'=>'1',
                        'shuffle_choice'=>'1',
                        'navigation_title'=>'1',
                        'account_id'=>'1',
                        );
}
$I->createDbQuestion($survies_entry);

//order list to see
// krsort($survies_entry);
$survies_entry_check = array();
foreach ($survies_entry as $survey) {
    $survies_entry_check[] = array('title' => $survey['title'], 'status' => '1');
}

//create answer
$answers = array();
for ($i=1; $i <= count($survies_entry) + count($survies_result); $i++) {
    $answers[$i] = array(
                        'question_id'=>$i,
                        );
}
$I->createDbAnswers($answers);


$results = array(
                'items' => $survies_result_check,
                'xpath' => 'html/body/div[11]/div[%d]/a',
                'item_per_page' => 10,
                );

$entries = array(
                'items' => $survies_entry_check,
                'xpath' => 'html/body/div[18]/div[%d]/a',
                'item_per_page' => 10,
                'end_link' => array('title' => '未回答ｱﾝｹｰﾄへ', 'url' => '/list/unanswered')
                );

$I->checkFPMypage($profile, $results, $entries);