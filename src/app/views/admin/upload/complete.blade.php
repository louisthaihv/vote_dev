@extends('layouts.admin_in')

@section('content')
    <h2>画像ファイルアップロード</h2>
    <div class="tcenter">
        {{ $upload->file_path }} に保存しました。<br />
        <img src="{{ $upload->file_path }}" style="max-width:500px;" />
    </div>
@stop