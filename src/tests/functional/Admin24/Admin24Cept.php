<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('add more account');

$I->amOnPage('/manage/account/add');
$I->amOnAction('Admin\SurveyController@create');
