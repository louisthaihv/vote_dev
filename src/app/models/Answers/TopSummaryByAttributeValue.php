<?php

namespace docomo\Models\Answers;

use docomo\Models\Attributes\Attribute;
use docomo\Models\Attributes\AttributeValue;
use docomo\Models\Questions\Choice;

class TopSummaryByAttributeValue extends \BaseArdentModel
{

    protected $table = 'top_summary_by_attribute_value';

    protected $primary = 'question_id';
    public $timestamps = false;

    public function choice()
    {

        return $this->BelongsTo('docomo\Models\Questions\Choice', 'top_choice_num', 'choice_num');
    }

    public static function createAllRecord($question) {
        $topExists = TopSummaryByAttributeValue::select('attribute_id', 'attribute_value_id')->whereQuestionId($question->id)->get();
        $topList = [];
        foreach ($topExists as $top) {
            if (!array_key_exists($top->attribute_id, $topList)) {
                $topList[$top->attribute_id] = [];
            }
            $topList[$top->attribute_id][$top->attribute_value_id] = true;
        }
        unset($top);
        unset($topExists);
        $attributeValues = AttributeValue::getDataByCache();

        foreach ($attributeValues as $attributes) {
            foreach ($attributes as $attributeValue) {
                if ($question->attribute_id == $attributeValue->attribute_id) {
                    continue;
                }
                if (array_key_exists($attributeValue->attribute_id, $topList) &&
                    array_key_exists($attributeValue->id, $topList[$attributeValue->attribute_id])
                ) {
                    continue;
                }
                $top = new TopSummaryByAttributeValue();
                $top->question_id = $question->id;
                $top->attribute_id = $attributeValue->attribute_id;
                $top->attribute_value_id = $attributeValue->id;
                $top->save();
                unset($top);
            }
        }
        unset($attributeValue);
        unset($attributes);
        unset($attributeValues);
    }

    public static function summary($question_id)
    {
        \DB::connection()->disableQueryLog();
        \DB::transaction(function () use ($question_id) {

            \DB::delete('DELETE FROM top_summary_by_attribute_value WHERE question_id = ' . $question_id);

            $resultData = ResultVote::
            selectRaw('`attribute_id`, `attribute_value_id`, `choice_id`, SUM(`voted_count` * `summary_value`) AS total_value, SUM(`voted_count`) as total')
                ->whereQuestionId($question_id)
                ->groupBy('attribute_id', 'attribute_value_id', 'choice_id')
                ->orderBy('attribute_id')->orderBy('attribute_value_id')->orderBy('total', 'desc')->orderBy('choice_id')
                ->get();

            $summaryData = [ATTR_BIRTHDAY => [], ATTR_GENDER => [], ATTR_LOCAL => []];
            $ranking = [ATTR_BIRTHDAY => [], ATTR_GENDER => [], ATTR_LOCAL => []];
            foreach ($resultData as &$record) {
                if (!array_key_exists($record->attribute_id, $summaryData)) {
                    $summaryData[$record->attribute_id] = [];
                    $ranking[$record->attribute_id] = [];
                }
                if (!array_key_exists($record->attribute_value_id, $summaryData[$record->attribute_id])) {
                    if ($record->total > 0) {
                        $record->choice_num = Choice::select('choice_num')->find($record->choice_id)->choice_num;
                    } else {
                        $record->choice_num = null;
                    }
                    $summaryData[$record->attribute_id][$record->attribute_value_id] = $record;
                    $ranking[$record->attribute_id][$record->attribute_value_id] = 1;
                } else {
                    $summaryData[$record->attribute_id][$record->attribute_value_id]->total += $record->total;
                    $summaryData[$record->attribute_id][$record->attribute_value_id]->total_value += $record->total_value;
                    $ranking[$record->attribute_id][$record->attribute_value_id]++;
                }
                \DB::update('UPDATE result_votes SET ranking = ? ' .
                    ' WHERE question_id = ? AND attribute_id = ? AND attribute_value_id = ? AND choice_id = ?',
                    [
                        $ranking[$record->attribute_id][$record->attribute_value_id],
                        $question_id,
                        $record->attribute_id,
                        $record->attribute_value_id,
                        $record->choice_id
                    ]);
                unset ($record);
            }
            unset ($resultData);
            unset($ranking);
            $total_count = 0;
            $total_value = 0;
            foreach ($summaryData as $attribute_id => &$values) {
                foreach ($values as $attribute_value_id => &$record) {
                    if ($attribute_id == ATTR_BIRTHDAY) {
                        $total_count += $record->total;
                        $total_value += $record->total_value;
                    }
                    $addList = [
                        'question_id' => $question_id,
                        'attribute_id' => $attribute_id,
                        'attribute_value_id' => $attribute_value_id,
                        'top_choice_num' => $record->choice_num,
                        'total_vote' => $record->total
                    ];
                    if ($record->total > 0 && isset($record->total_value)) {
                        $addList['average_num'] = $record->total_value / $record->total;
                    }
                    TopSummaryByAttributeValue::create($addList);
                    unset($attribute_valueid);
                    unset($record);
                }
                unset($attribute_id);
                unset($values);
            }
            unset ($record);
            unset($summaryData);

            $total_average = (($total_count > 0) ? $total_value / $total_count : -1);
            \DB::insert('INSERT INTO top_summary_by_attribute_value ' .
                ' VALUES (?, 0, 0, 0, CASE ? WHEN -1 THEN NULL ELSE ? END, ?) ',
                [$question_id, $total_average, $total_average, $total_count]);

            \DB::commit();
        });
    }

    public static function updateTopByRecord($record)
    {
        TopSummaryByAttributeValue::updateTop($record->question_id, $record->attribute_id, $record->attribute_value_id, $record->choice_id, $record->summary_value);
    }

    public static function updateTop($questionId, $attributeId, $attributeValueId, $choiceId, $summaryValue) {
        $result = ResultVote::select(\DB::raw('SUM(voted_count) as vote_summary, SUM(summary_value * voted_count) as total'))
            ->whereQuestionId($questionId)
            ->whereAttributeId($attributeId)
            ->whereAttributeValueId($attributeValueId)
            ->groupBy('question_id', 'attribute_id', 'attribute_value_id')
            ->first();

        if (!is_null($summaryValue) && $result->vote_summary > 0) {
            $result->average = $result->total / $result->vote_summary;
        }
        $result->top_choice_num = Choice::select('choice_num')->find($choiceId)->choice_num;

        TopSummaryByAttributeValue::whereQuestionId($questionId)
            ->whereAttributeId($attributeId)
            ->whereAttributeValueId($attributeValueId)
            ->update([
                'top_choice_num' => $result->top_choice_num,
                'average_num' => $result->average,
                'total_vote' => $result->vote_summary
            ]);
    }

    public static function updateTopByArray($questionId, $topData) {
        foreach ($topData as $attribute => &$attributeRecords) {
            foreach ($attributeRecords as $attributeValue => &$record) {
                \DB::update(
                    ' UPDATE top_summary_by_attribute_value ' .
                    ' SET top_choice_num = (SELECT choice_num FROM choices WHERE id = ?), ' .
                    ' average_num = ?, total_vote = ? ' .
                    ' WHERE question_id = ? AND attribute_id = ? AND attribute_value_id = ? ',
                    [
                        (!is_null($record['voted_count']) && $record['voted_count'] > 0) ? $record['choice_id'] : -1,
                        (!is_null($record['voted_count']) && $record['voted_count'] > 0) ? $record['summary'] / $record['voted_count'] : null,
                        (is_null($record['voted_count']) ? 0 : $record['voted_count']),
                        $questionId,
                        $attribute,
                        $attributeValue
                    ]
                );
            }
        }
    }

    public static function updateTopData($questionId, $attributeId, $attributeValueId)
    {
        $topChoice = \DB::connection('mysql_trans')
            ->select('SELECT SQL_NO_CACHE choice_num FROM choices INNER JOIN result_votes ON choices.id= result_votes.choice_id ' .
                ' WHERE result_votes.question_id =? AND result_votes.attribute_value_id = ? AND result_votes.voted_count > 0 AND choices.choice_num > 0 ' .
                ' GROUP BY result_votes.choice_id ORDER BY SUM(result_votes.voted_count) DESC, choices.choice_num ASC limit 1',
                [$questionId, $attributeValueId]);

        // 投票されたレコードが存在しない場合、処理を行わずに終了する
        if (count($topChoice) === 0) return;

        $summary = \DB::connection('mysql_trans')
            ->select('SELECT SQL_NO_CACHE SUM(voted_count) as total, SUM(voted_count * summary_value) as total_value ' .
                'FROM result_votes WHERE question_id =? AND attribute_id =? AND attribute_value_id = ?',
                [$questionId, $attributeId, $attributeValueId]);
        \DB::connection('mysql_trans')
            ->update('UPDATE top_summary_by_attribute_value ' .
                ' SET top_choice_num = ?, average_num = ?, total_vote=? ' .
                ' WHERE question_id=? AND attribute_id =? AND attribute_value_id = ?',
                [
                    $topChoice[0]->choice_num,
                    ($summary[0]->total > 0) ? intval($summary[0]->total_value / $summary[0]->total) : 0,
                    $summary[0]->total,
                    $questionId, $attributeId, $attributeValueId
                ]);
    }

    public static function updateTopByAttribute($questionId, $user) {
        $data = \DB::connection('mysql_trans')
            ->select('SELECT `attribute_id`, `attribute_value_id`, SUM(`voted_count`) as total, SUM(`voted_count` * `summary_value`) as summary'.
                ' FROM `result_votes` WHERE `question_id` = ? AND `attribute_value_id` IN (?, ?, ?)' .
                ' GROUP BY `attribute_id`, `attribute_value_id`' .
                ' ORDER BY `attribute_id`, `attribute_value_id`',
                [
                    $questionId,
                    $user->attrList[ATTR_BIRTHDAY], $user->attrList[ATTR_GENDER], $user->attrList[ATTR_LOCAL]
                ]);
        foreach ($data as &$attribute) {
            \DB::connection('mysql_trans')
                ->update('UPDATE top_summary_by_attribute_value ' .
                    ' SET `top_choice_num`=(SELECT `choice_num` FROM `choices` WHERE `id` = ' .
                    ' (SELECT `choice_id` FROM `result_votes` WHERE `question_id`=? AND `attribute_id`=? AND `attribute_value_id`=? GROUP BY choice_id ORDER BY SUM(`voted_count`) DESC, choice_id ASC limit 1)), ' .
                    ' `total_vote`=?, `average_num`=? ' .
                    ' WHERE `question_id`=? AND `attribute_id`=? AND `attribute_value_id`=?',
                    [
                        $questionId, $attribute->attribute_id, $attribute->attribute_value_id,
                        $attribute->total, ($attribute->total > 0) ? ($attribute->summary / $attribute->total) : 0,
                        $questionId, $attribute->attribute_id, $attribute->attribute_value_id
                    ]);
        }
    }

    public static function getFrontData($question)
    {
        $attributes = Attribute::getDataByCache();
        $choices = $question->getChoices();
        $choiceList = [];
        foreach ($choices as $choice) {
            $choiceList[$choice->choice_num] = $choice->choice_text;
        }
        $attributeList = [];
        foreach ($attributes as $attribute) {
            $attributeValues = AttributeValue::getDataByCache($attribute->id);
            foreach ($attributeValues as $key => $attributeValue) {
                $attributeList[$attribute->id][$attributeValue->id] = $attributeValue;
            }
            unset($key);
            unset($attributeValue);
        }
        unset($attribute);
        unset($attributes);

        $topList = TopSummaryByAttributeValue::whereQuestionId($question->id)->get();
        $topData = [];
        foreach ($topList as $rec) {
            if ($rec->top_choice_num) {
                $rec->choice_text = $choiceList[$rec->top_choice_num];
                $rec->map = $rec->top_choice_num;
            } else {
                $rec->map = '';
            }
            if ($rec->attribute_value_id != ATTRIBUTE_VALUE_NOT_SET) {
                $rec->attribute_value_name = $attributeList[$rec->attribute_id][$rec->attribute_value_id]->attribute_value_name;
                $rec->attribute_value_directory = $attributeList[$rec->attribute_id][$rec->attribute_value_id]->attribute_value_directory;
                $topData[$rec->attribute_id][$rec->attribute_value_id] = $rec;
            }
        }
        unset($rec);
        unset($topList);
        unset($choiceList);
        unset($attributeList);

        return $topData;
    }
}