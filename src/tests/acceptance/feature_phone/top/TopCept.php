<?php
$I = new AcceptanceTester\TopSteps($scenario);
$I->wantTo('check top');
$I->amOnPage('/profile');
$profile = array(
                'sex' => array('name' => 'sex', 'value' => '男性'),
                'year' => array('name' => 'birthday_year', 'value' => '2000'),
                'month' => array('name' => 'birthday_month', 'value' => '01'),
                'day' => array('name' => 'birthday_day', 'value' => '01'),
                'place' => array('name' => 'pref', 'value' => '京都府'),
                'submit' => '　設定する ',
                );
$questions = array(
                1=>array(
                        'genre_parent_id'=>'1',
                        'genre_child_id'=>'1',
                        'status'=>'5',
                        'title'=>'title_1',
                        'vote_date_from'=>'2015-01-09 03:40:52',
                        'vote_date_to'=>'2015-01-31 00:00:00',
                        'baseline_date'=>'2015-01-09 03:40:52',
                        'viewable_type'=>'1',
                        'visible_device_i'=>'1',
                        'visible_device_d'=>'1',
                        'summary_type'=>'1',
                        'average_unit'=>'1',
                        'is_visible_other'=>'1',
                        'shuffle_choice'=>'1',
                        'navigation_title'=>'1',
                        'account_id'=>'1',
                        'attribute_id' => '2',
                        'attribute_value_id' => '7',
                        ),
                2=>array(
                        'genre_parent_id'=>'1',
                        'genre_child_id'=>'1',
                        'status'=>'5',
                        'title'=>'title_2',
                        'vote_date_from'=>'2015-01-09 03:40:52',
                        'vote_date_to'=>'2015-01-31 00:00:00',
                        'baseline_date'=>'2015-01-11 03:40:52',
                        'viewable_type'=>'1',
                        'visible_device_i'=>'1',
                        'visible_device_d'=>'1',
                        'summary_type'=>'1',
                        'average_unit'=>'1',
                        'is_visible_other'=>'1',
                        'shuffle_choice'=>'1',
                        'navigation_title'=>'1',
                        'account_id'=>'1',
                        'attribute_id' => NULL,
                        'attribute_value_id' => NULL,
                        ),
                3=>array(
                        'genre_parent_id'=>'2',
                        'genre_child_id'=>'2',
                        'status'=>'6',
                        'title'=>'title_3',
                        'vote_date_from'=>'2015-01-09 03:40:52',
                        'vote_date_to'=>'2015-01-11 00:00:00',
                        'baseline_date'=>'2015-01-11 03:40:52',
                        'viewable_type'=>'1',
                        'visible_device_i'=>'1',
                        'visible_device_d'=>'1',
                        'summary_type'=>'1',
                        'average_unit'=>'1',
                        'is_visible_other'=>'1',
                        'shuffle_choice'=>'1',
                        'navigation_title'=>'1',
                        'account_id'=>'1',
                        'attribute_id' => '2',
                        'attribute_value_id' => '7',
                        ),
            );
$title = array(
              1=>array('title'=>'男性の方へ'),
              2=>array('title'=>'～10代男性が興味'),
            );
$tag = array(
            1=> array('question_id'=>1, 'keyword_id'=>1),
            2=> array('question_id'=>2, 'keyword_id'=>2),
            3=> array('question_id'=>3, 'keyword_id'=>1),
        );
$genre_parent = array(
                    1=>array(
                        'genre_parent_name'=>'genre_parent_1',
                        'parent_is_visible'=>1,
                        'genre_parent_directory'=>'dir_1',
                        'parent_sort_order'=>10,
                        ),
                    2=>array(
                        'genre_parent_name'=>'genre_parent_2',
                        'parent_is_visible'=>1,
                        'genre_parent_directory'=>'dir_2',
                        'parent_sort_order'=>20,
                        ),
                );
$genre_child = array(
                    1=>array(
                        'genre_parent_id'=>1,
                        'genre_child_name'=>'genre_child_name_1',
                        'child_is_visible'=>1,
                        'genre_child_directory'=>'child_dir_1',
                        'child_sort_order'=>10
                        ),
                    2=>array(
                        'genre_parent_id'=>2,
                        'genre_child_name'=>'genre_child_name_2',
                        'child_is_visible'=>1,
                        'genre_child_directory'=>'child_dir_2',
                        'child_sort_order'=>20
                        ),
                );
$answers = array(
                1=>array(
                        'question_id' => '1',
                        'user_id' => '1',
                        'choice_id' => '1',
                        'age' => '25',
                        'gender' => '1',
                        'local' => '1',
                        'answer_date' => '2015-01-14 00:00:00',
                        ),
                2=>array(
                        'question_id' => '2',
                        'user_id' => '1',
                        'choice_id' => '1',
                        'age' => '25',
                        'gender' => '1',
                        'local' => '1',
                        'answer_date' => '2015-01-14 00:00:00',
                        ),

                );
$votes =  array(
                1 => array(
                    'question_id' => '1',
                    'attribute_id' =>'2',
                    'attribute_value_id' =>'7',
                    'device'=>'1',
                    'choice_id'=>'1',
                    'voted_count'=>'100',
                    ),
                2 => array(
                    'question_id' => '3',
                    'attribute_id' =>'2',
                    'attribute_value_id' =>'7',
                    'device'=>'1',
                    'choice_id'=>'1',
                    'voted_count'=>'20',
                    ),
                );

$pickups = array(
            1=>array(
                    'question_id' => '2',
                    'type' => '1',
                    'from' => '2015-01-15 00:00:00',
                    'to' => '2015-04-31 00:00:00',
                    ),
            2=>array(
                    'question_id' => '3',
                    'type' => '1',
                    'from' => '2015-01-09 00:00:00',
                    'to' => '2015-01-11 00:00:00',
                    ),

        );
$keyword = array(
            1=> array(
                    'keyword_name' => 'keyword_1',
                    'is_visible' => 1,
                    ),
            2=> array(
                    'keyword_name' =>'keyword_2',
                    'is_visible' => 0,
                    ),
            );
$I->createDbKeyword($keyword);
$I->createDbQuestionTags($tag);
// $I->createDbAnswers($answers);
$I->createDbQuestionFull($questions);
$I->createDbGenreParent($genre_parent);
$I->createDbGenreChild($genre_child);
$I->createCommonProfile($profile);
$I->createDbResultVoteFull($votes);
$I->createDbPickups($pickups);
$I->click('みんなの声TOPへ');
$button_link = 'html/body/div[4]/a[2]/img';
$I->checkButtonLink($button_link);
$I->checkQuestionTop($questions, $title);
$I->checkKeywordTop($keyword);
