<?php

namespace docomo\Models\Codes;

class EvaluationType
{
    public static $array = [SUMMARY_TYPE_EVALUATION_GATTEN, SUMMARY_TYPE_EVALUATION_MAJI, SUMMARY_TYPE_EVALUATION_MEMO];
    public static $label = [
        SUMMARY_TYPE_EVALUATION_GATTEN  => 'ガッテン！',
        SUMMARY_TYPE_EVALUATION_MAJI    => 'マジ！？',
        SUMMARY_TYPE_EVALUATION_MEMO    => 'メモメモ'
    ];

    public static $sp_images = [
        SUMMARY_TYPE_EVALUATION_GATTEN => 'common/front/images/sp/evaluate01.png',
        SUMMARY_TYPE_EVALUATION_MAJI => 'common/front/images/sp/evaluate02.png',
        SUMMARY_TYPE_EVALUATION_MEMO => 'common/front/images/sp/evaluate03.png'
    ];
}
