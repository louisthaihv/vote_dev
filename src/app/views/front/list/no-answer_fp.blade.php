@extends('layouts.front_fp')

@section('content')

    @include('layouts.front_elements._space')
    
    <div>{{ HTML::image('common/front/images/fp/tit6.gif', '未回答アンケート', array('width' => '100%')) }}</div>
    
    @include('layouts.front_elements._space')
    @if ($questions->getTotal() === 0)
<div>みんなの声をお使いいただきありがとうございます！<br />
ただいま回答できるアンケートはありません。<br />
新しいアンケートを準備中ですので、配信をお待ちください。<br />
追加したアンケートは公式twitterやfacebookでもつぶやきます。<br />
是非フォローをお願いします！
@include('layouts.front_elements._space')
<div style="width:100%;text-align:center;">
<a href="https://twitter.com/_minnanokoe_/"><img src="/common/front/images/sp/btn_twitter.png" alt="twitter" width="100px"></a>
<a href="https://www.facebook.com/minnanokoe.vote"><img src="/common/front/images/sp/btn_facebook.png" alt="facebook" width="100px"></a>
</div></div>
@include('layouts.front_elements._space')
    @else
    
    <div style="text-align:right;">未回答数：{{ number_format($questions->getTotal()) }}件</div>
    
    @include('layouts.front_elements._space')
    
    <div style="margin:0px 10px 0px 10px;">
        @foreach($questions as $question)
            @include('layouts.front_elements._list_question', ['$question'=>$question])
        @endforeach
    </div>
    
    {{ $questions->links('layouts.front_elements.paginate_fp') }}
    @include('layouts.front_elements._space')
    @include('front._shared._link_to_list_sp', ['route' => 'list.new.entry', 'text' => '新着一覧へ'])
    @include('layouts.front_elements._gotop_fp')
    @endif

@endsection
