<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('Edit Genre');

$I->amOnRoute('admin.genre.edit');
$I->amOnAction('Admin\SurveyController@create');
