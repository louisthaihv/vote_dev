<?php
namespace Front;

use docomo\Models\Notices\Notice;
use docomo\Models\Users\User;

class NoticeController extends \BaseController
{
    
    public function __construct()
    {
    }
    
    public function show($id)
    {
        
        $notice = Notice::nowAndPast()->where('visible_device_' . ((User::$userDevice === CHAR_D_MENU) ? 'd' : 'i'), '=', true)->find($id);
        if (is_null($notice)) {
            \App::abort(404);
        }
        return \View::make('front.notice.detail', compact('notice'));
    }
}
