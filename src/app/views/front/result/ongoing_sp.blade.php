@extends('layouts.front')

@section('metaTag')
    <title>{{ $question->title }} | みんなの声</title>
    <meta name="description" content="{{{ $question->description }}}">
    <meta property="og:title" content="{{{ $question->title }}} | みんなの声" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="{{{ $question->description }}}" />
    <meta name="twitter:title" content="{{{ $question->title }}} | みんなの声" />
    <meta name="twitter:description" content="{{{ $question->description }}}" />
@stop

@section('style')
{{ HTML::style('common/front/css/result.css') }}
{{ HTML::style('common/front/css/entry_additional.css') }}
@stop

@section('script')
    <script type="text/javascript">
        var basePath = '{{ route('result.show', ['parent_genre_name' => $question->getParentGenreDirectory(), 'child_genre_name' => $question->getChildGenreDirectory(), 'id' => $question->id]) }}';
        var attribute = 0;
        var attributeValue = 0;
    </script>
{{ HTML::script('common/front/js/tab_change.js') }}
{{ HTML::script('common/front/js/result.js') }}
@stop

@section('content')

<section class="l-margin">
    <h2 class="resultTit m-margin"><span>投票結果</span></h2>
    <div class="lrMargin">
        <div class="questionInfoBox">
            <h1>{{ $question->title }}</h1>
            <div class="questionText">
                {{ $question->details }}
            </div>
            @if($question->attribute_id)
                <p class="restraintResultInfo m-margin restraintShow">(条件：{{ $question->attr->attribute_name }} - {{ $question->attrValue->attribute_value_name }}のみ)</p>
            @endif
        </div><!--/questionInfoBox-->

    </div>
    <div class="tcenter l-margin l-margint">
        <p>このアンケートは投票受付中です</p><br/>
        <p class="tcenter m-margin">
            {{ link_to_route('result.show', '投票せずに結果を見る', [$question->getParentGenreDirectory(), $question->getChildGenreDirectory(), $question->id, 'attribute' => null, 'attribute_value' => null, 'showresult' => true], ['class' => 'btnType02']) }}
        </p>
        @if (!$question->isFinished() && !$question->isAnswered($currentUser->id))
            @include('front.result._ongoing_contents', ['myAnswer' => null, 'btnMessage' => 'アンケートに投票する'])
        @endif
    </div>
</section>

@include('front.result._recommend')
@include('front.entry._bottomcontents')
@stop