<div class="tabBox">
    <p class="l-margin">
        {{ link_to_route('manage.question.csv', '出身地別CSVダウンロード', [
                    $question->id,
                    '3',
                    '0'
        ] ) }}
        {{ link_to_route('manage.question.csv', '出身地別CSVダウンロード(d版)', [
                    $question->id,
                    '3',
                    '1'
        ] ) }}
        {{ link_to_route('manage.question.csv', '出身地別CSVダウンロード(i版)', [
                    $question->id,
                    '3',
                    '2'
        ] ) }}
    </p>
    
    <dl class="openCloseContents m-margin">
        <dt>都道府県リストを開く</dt>
        <dd>
            <div class="resultTable profileChoice">
                <table class="l-margint">
                    @include('admin.question.result_parts._result_list', ['list' => $areaList, 'selected' => $selectedArea, 'targetId' => 'prefTitle'])
                </table>
                <!--/resultTable--></div>
        </dd>
    </dl>
    <p class="titPtn1 l-margint" id="prefTitle">{{ $selectedArea->attribute_value_name }}ランキング</p>
    @if ($question->isViewableAverage())
        <p class="avgData">平均：{{ showAverage($topSummaryHash, $selectedArea->id, $question) }}</p>
    @endif
    <p class="tright m-margint attrCount">投票数：<span>{{ number_format(array_sum($areaData->lists('cnt'))) }}</span>票</p>
    <div class="resultTable resultTableTarget">
        <table class="l-margint">
            @include('admin.question.result_parts._result_table', ['resultData' => $areaData])
        </table>
        <!--/resultTable--></div>
    <p class="titPtn1 l-margint">全国1位分布</p>
    <div class="map japanmap tcenter">
        @include('share._prefectures')
    </div>


</div>
