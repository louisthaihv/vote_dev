<?php
namespace docomo\Models\Questions;

use docomo\Models\Keywords\Keyword;

class QuestionTag extends \BaseArdentModel
{
    protected $table = 'question_tags';
    public $timestamps = false;
    protected $primaryKey = 'question_id';

    public function getKeywordName()
    {

        return Keyword::select('keyword_name')->find($this->keyword_id)->keyword_name;
    }

    public static function fillWithModify($id)
    {

        QuestionTag::whereQuestionId($id)->delete();

        $tags = null;
        $keywords = \Input::get('keyword');
        if (isset($keywords)) {
            $tags = [];
            foreach ($keywords as $key => $val)
            {

                $tag = new QuestionTag;
                $tag->question_id = $id;
                $tag->keyword_id = $val;
                $tags[] = $tag;
            }
        }

        return $tags;
    }
}
