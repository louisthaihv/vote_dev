<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class FrontendHelper extends \Codeception\Module\AcceptanceHelper
{
    /**
     * Header
     * check isset hyper-link header
     */
    public function checkHeader($arr, $button_menu)
    {
        $I = $this->getModule('WebDriver');
        // $I->see(NOTICE_HEADER);
        foreach($arr as $key=>$value){
            if($key == 'TOP'){
                $I->click($button_menu);
                $I->click($key);
            }
            else{
                $I->dontSeeLink($key, $value);
                $I->click($button_menu);
                $I->click($key);
                $I->wait(3);
                $I->seeCurrentUrlEquals($value);
                $I->moveBack();
            }
            
        }
    }
    public function checkNoticeHeader($arr_notice, $id, $arr_notice_2)
    {
        $I = $this->getModule('WebDriver');
        $I->amOnPage('/');
        $I->seeLink($arr_notice['title'], '/notice/'.$id.'');
        $I->dontSee($arr_notice_2['title']);
        $I->click($arr_notice['title']);
        $I->seeCurrentUrlEquals('/notice/'.$id.'');
        $I->moveBack();
        $I->see($arr_notice['description']);
        $I->click('.//*[@id="top"]/p/a/img');
        $I->seeCurrentUrlEquals('/mypage');
        $I->moveBack();
    }
    /**
     * Footer
     * check isset hyper-link footer
     */
    public function commonHeaderFooterCheckLink($arr, $button_menu)
    {
        $I = $this->getModule('WebDriver');
        if($button_menu == null){
            foreach($arr as $key=>$value){
                if($key == '操作説明'){
                    $I->click('html/body/footer/ul[1]/li[2]/a');
                    $I->moveBack();
                }
                if($key == 'dメニューTOP'){
                    $I->click('dメニューTOP');
                    $I->moveBack();
                }
                else{
                    $I->click($key);
                    $I->seeCurrentUrlEquals($value);
                    $I->moveBack();
                }
            }
        }
    }

    public function checkMoveMouse($keyword, $keyword_field)
    {
        $I = $this->getModule('WebDriver');
        $I->dontSeeElement('html/body/p/a');
        $I->fillField('input[name='.$keyword_field.']',$keyword);
        $I->seeElement('html/body/p/a');
        $I->click('html/body/p/a');
        $I->moveMouseOver('html/body/div[2]/div/div[1]/ul/li[3]/a/div');
    }

    public function checkSearchFooterKeyword($keyword, $keyword_field){
        $I = $this->getModule('WebDriver');
        $I->fillField('input[name='.$keyword_field.']',$keyword);
        // $I->click(SEARCH_KEYWORD_FOOTER);
    }
    public function checkSPGenreList($arr_genre_parent, $arr_genre_child)
    {
        $I = $this->getModule('WebDriver');
        if($arr_genre_parent['parent_is_visible'] == 1){
            $I->see($arr_genre_parent['name']);
            $I->click($arr_genre_parent['x_path']);
            foreach($arr_genre_child as $key=>$value){
                if($value['child_is_visible'] == 1){
                    $I->seeLink($value['name'], '/list/genre/'.$value['name'].'/entry');
                }
                else{
                    $I->dontSee($value['name']);
                }
            }
        }
        else{
            $I->dontSee($arr_genre_parent['name']);
        }
    }
    public function createUserSP7($arr_genre, $genre_parent_directory, $genre_child_directory)
    {
        $I = $this->getModule('Db');

        // $this->createDatabaseGenreParent($genre_parent_directory);
        // $this->createDatabaseGenreChild($genre_child_directory);
        $this->createDatabaseSurvey($arr_genre);
    }
   public function createDatabaseGenreParent($genre_parent)
    {
        $I = $this->getModule('Db');
        foreach ($genre_parent as $key => $value) {
            $I->haveInDatabase('genre_parents', array(
                                                'genre_parent_name'=>$value['genre_parent_name'],
                                                'parent_is_visible'=>$value['parent_is_visible'],
                                                'genre_parent_directory'=>$value['genre_parent_directory'],
                                                'parent_sort_order'=>$value['parent_sort_order']
                                                )
            );
        }
    }
    public function createDatabaseGenreChild($genre_child)
    {
        $I = $this->getModule('Db');
        foreach ($genre_child as $key => $value) {
            $I->haveInDatabase('genre_children', array(
                                                    'genre_parent_id'=>$value['genre_parent_id'],
                                                    'genre_child_name'=>$value['genre_child_name'],
                                                    'child_is_visible'=>$value['child_is_visible'],
                                                    'genre_child_directory'=>$value['genre_child_directory'],
                                                    'child_sort_order'=>$value['child_sort_order']
                                                )
            );
        }
    }
    public function createDatabaseSurvey($arr_genre)
    {
        $I = $this->getModule('Db');
        foreach ($arr_genre as $key => $value) {
            $I->haveInDatabase('questions', array(
                                                'genre_parent_id'=>$value['genre_parent_id'],
                                                'genre_child_id'=>$value['genre_child_id'] ,
                                                'status'=>$value['status'] ,
                                                'title'=>$value['title'] ,
                                                'vote_date_from'=>$value['vote_date_from'] ,
                                                'vote_date_to'=>$value['vote_date_to'] ,
                                                'baseline_date'=>$value['baseline_date'] ,
                                                'viewable_type'=>$value['viewable_type'] ,
                                                'visible_device_i'=>$value['visible_device_i'] ,
                                                'visible_device_d'=>$value['visible_device_d'] ,
                                                'summary_type'=>$value['summary_type'] ,
                                                'average_unit'=>$value['average_unit'] ,
                                                'is_visible_other'=>$value['is_visible_other'] ,
                                                'shuffle_choice'=>$value['shuffle_choice'] ,
                                                'navigation_title'=>$value['navigation_title'] ,
                                                'account_id'=>$value['account_id'] ,
                                                )
            );
        }
    }

    public function createColumns($columns)
    {
        $I = $this->getModule('Db');
        foreach ($columns as $key => $value) {
            $I->haveInDatabase('columns', array(
                                                'question_id' => $value['question_id'],
                                                'is_visible' => $value['is_visible'],
                                                'column_title' => $value['column_title'],
                                                'column_detail' => $value['column_detail'],
                                                'view_create_datetime' => $value['view_create_datetime'],
                                                'created_account_id' => $value['created_account_id'],
                                                'updated_account_id' => $value['updated_account_id'],
                                                )
            );
        }
    }


    public function createDatabaseKeyword($keywords)
    {
        $I = $this->getModule('Db');
        foreach ($keywords as $key => $value) {
            $I->haveInDatabase('keywords', array(
                                                'keyword_name' => $value['keyword_name'],
                                                'is_visible' => $value['is_visible'],
                                                )
            );
        }
    }

    public function createDatabaseQuestionTags($tags)
    {
        $I = $this->getModule('Db');
        foreach ($tags as $key => $value) {
            $I->haveInDatabase('question_tags', array(
                                                'question_id' => $value['question_id'],
                                                'keyword_id' => $value['keyword_id'],
                                                )
            );
        }
    }

    public function checkUserSP7($genre_parent_directory, $genre_child_directory)
    {
        $I = $this->getModule('WebDriver');
        $I->amOnPage('/list/genre/'.$genre_parent_directory.'/'.$genre_child_directory.'/entry');
        $I->click('受付中');
        $I->seeLink('2014年12月24日 03:40:52');
        $I->seeLink('2014年12月23日 03:40:52');
    }
    /**
     * Keyword
     */
    public function createKeywordSP8($keyword, $arr_genre)
    {
        $I = $this->getModule('Db');
        $I->haveInDatabase('keywords', array(
                                            'keyword_name'=>$keyword,
                                            'is_visible'=>'1',
                                            )
        );
        foreach ($arr_genre as $key => $value) {
            $I->haveInDatabase('question_tags', array(
                                            'question_id'=>$key,
                                            'keyword_id'=>'1',
                                            )
            );
        }
    }
    public function createResultVote($arr_genre)
    {
        $I = $this->getModule('Db');
        foreach ($arr_genre as $key => $value) {
            $I->haveInDatabase('result_votes', array(
                                                'question_id'=>$key,
                                                'attribute_id'=>$key,
                                                'attribute_value_id'=>$key,
                                                'device'=>$key,
                                                'choice_id'=>$key,
                                                'ranking'=>$key,
                                                'voted_count'=>$key,
                                                'summary_value'=>$key,
                                                )
            ); 
        }
        
    }

    public function createUserProfile($arr_user)
    {
        $I = $this->getModule('WebDriver');
        $I->amOnPage('/profile');
        $I->selectOption('sex', $arr_user['sex']);
        $I->selectOption('birthday_year', $arr_user['birthday_year']);
        $I->selectOption('birthday_month', $arr_user['birthday_month']);
        $I->selectOption('birthday_day', $arr_user['birthday_day']);
        $I->selectOption('pref', $arr_user['pref']);
        $I->click($arr_user['btn_submit']);
    }
    public function checkSP17($arr_genre)
    {
        $I = $this->getModule('WebDriver');
        $I->amOnPage('/list/popular/entry');
        $I->click('.//*[@id="mainContents"]/section[2]/div[1]/ul/li[1]/a/span');
        $I->seeCurrentUrlEquals('/list/popular/entry');
        foreach ($arr_genre as $key => $value) {
            if($key > 1 && $key < 12){
                $I->see($value['title']);
            }
            else{
                $I->dontSee($value['title']);
            }

        }
        $I->click('.//*[@id="mainContents"]/section[2]/div[2]/a/span');
        $I->wait(5);
        foreach ($arr_genre as $key => $value){
            if($key < 12){
                $I->see($value['title']);
            }
            else{
                $I->dontSee($value['title']);
            }
        }
        $this->checkSortSurvey($arr_genre);
        $this->checkSeeBaselineSurvey();
    }
    public function checkSortSurvey($arr_genre)
    {
        $I = $this->getModule('WebDriver');
        $I->see($arr_genre[11]['title'] ,'.//*[@id="popularEntry"]/li[1]/a/span[1]');
        $I->see($arr_genre[10]['title'] ,'.//*[@id="popularEntry"]/li[2]/a/span[1]');
    }
    public function checkSeeBaselineSurvey()
    {
        $I = $this->getModule('WebDriver');
        $I->see('未指定', './/*[@id="popularEntry"]/li[3]/a/span[2]');
    }
    public function checkSP17_1($arr_genre)
    {
        $I = $this->getModule('WebDriver');
        $I->click('.//*[@id="mainContents"]/section[2]/div[1]/ul/li[2]/a/span');
        $I->seeCurrentUrlEquals('/list/popular/result');
        $I->wait(3);
        foreach ($arr_genre as $key => $value) {
            if($key < 12){
                $I->dontSee($value['title']);
            }
            else{
                $I->see($arr_genre[12]['title']);
            }
        }
    }

    public function checkKeywordSP8($arr_genre)
    {
        $I = $this->getModule('WebDriver');
        $I->amOnPage('/list/tag/1/entry');
        $I->click('.//*[@id="mainContents"]/section[2]/div[1]/ul/li[1]/a/span');
        $I->seeCurrentUrlEquals('/list/tag/1/entry');
        foreach ($arr_genre as $key => $value) {
            if($key < 12 && $key > 1){
                $I->see($value['title']);
            }
            else{
                $I->dontSee($value['title']);
            }
        }
        $I->click('.//*[@id="mainContents"]/section[2]/a/span');
        $I->wait(5);
        foreach ($arr_genre as $key => $value){
            if($key < 12){
                $I->see($value['title']);
            }
            else{
                $I->dontSee($value['title']);
            }
        }
        $this->checkSortKeywordSP8($arr_genre);
    }
    
    public function checkSortKeywordSP8($arr_genre)
    {
        $I = $this->getModule('WebDriver');
        $I->see($arr_genre[2]['title'], './/*[@id="questionEntryByTag"]/li[1]/a/span[1]');
        $I->see($arr_genre[11]['title'], './/*[@id="questionEntryByTag"]/li[2]/a/span[1]');
    }

    public function checkSP8_1($arr_genre, $arr_column)
    {
        $I = $this->getModule('WebDriver');
        $I->click('.//*[@id="mainContents"]/section[2]/div[1]/ul/li[2]/a[2]/span');
        $I->seeCurrentUrlEquals('/list/tag/1/result');
        foreach ($arr_genre as $key => $value) {
            if($key < 12){
                $I->dontSee($value['title']);
            }
            else{
                $I->see($value['title']);
            }
        }
        $this->checkColumnSP8($arr_column);
    }

    public function createNewColumnSP8($arr_column)
    {
        $I = $this->getModule('Db');
        foreach ($arr_column as $key => $value) {
            $I->haveInDatabase('columns', array(
                                            'question_id'=>$value['question_id'],
                                            'is_visible'=>$value['is_visible'],
                                            'column_title'=>$value['column_title'],
                                            'column_detail'=>$value['column_detail'],
                                            'view_create_datetime'=>$value['view_create_datetime'],
                                            'created_account_id'=>$value['created_account_id'],
                                            'updated_account_id'=>$value['updated_account_id'],
                                            )
                                );
        }
    }
    public function checkColumnSP8($arr_column)
    {
        $I = $this->getModule('WebDriver');
        //check display 10 value column( kiem tra hien thi 10 gia tri column va dieu kien is_visible cua column =1 thi hien thi = 0 thi ko hien thi)
        foreach ($arr_column as $key => $value) {
            if($value['is_visible'] == 1){
                if($key > 2){
                    $I->see($value['column_title']);
                }
                else{
                    $I->dontSee($value['column_title']);
                }
            }
            if($value['is_visible'] == 0){
                $I->dontSee($value['column_title']);
            }
        }
        //click vao xpath thi hien thi them cac gia tri column co is_visible =1
        $xpath_column='' ; //chen xpath column vao day
        $I->click($xpath_column);
        $I->wait(5);
        foreach ($arr_column as $key => $value) {
            if($value['is_visible'] == 1){
                $I->see($value['column_title']);
            }
            else{
                $I->dontSee($value['column_title']);
            }
        }
        //check sort column
        $this->checkSortColumnSP8($arr_column);
    }
    
    public function checkSortColumnSP8($arr_column)
    {
        $I = $this->getModule('WebDriver');
        //code for sort column(chi can check thu tu 2 column la duoc, tham khao them method checkSortKeywordSP8)
    }

    public function checkListSP8($arr_entry)
    {
        $I = $this->getModule('WebDriver');
        $this->checkListCommons($arr_entry['items_check'], $arr_entry['xpath_format'], 10);
        $this->checkListShowmoreCommons($arr_entry['link_for_showmore'], $arr_entry['items_check'], $arr_entry['xpath_format'], 11);
    }

    public function checkListSP8_1($arr_result, $arr_column)
    {
        $I = $this->getModule('WebDriver');
        $this->checkListCommons($arr_result['items_check'], $arr_result['xpath_format'], 10);
        $this->checkListShowmoreCommons($arr_result['link_for_showmore'], $arr_result['items_check'], $arr_result['xpath_format'], 11);

        $this->checkListCommons($arr_column['items_check'], $arr_column['xpath_format'], 10);
        $this->checkListShowmoreCommons($arr_column['link_for_showmore'], $arr_column['items_check'], $arr_column['xpath_format'], 11);    }
}