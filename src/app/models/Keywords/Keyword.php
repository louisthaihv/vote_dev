<?php
namespace docomo\Models\Keywords;

class Keyword extends \BaseArdentModel
{
    
    protected $table = 'keywords';

    /*
     * Acceptable params from input
     */
    protected $fillable = [
        'keyword_name'
    ];

    public static $rules = [
        'keyword_name' => ['required', 'unique:keywords', 'max:60', 'deny_tag', 'deny_char']
    ];

    public static $customMessages = [
        'keyword_name.required' => W_FRM_REQUIRED_KEYWORD,
        'keyword_name.unique' => W_FRM_UNIQUE_KEYWORD,
        'keyword_name.max' => W_FRM_MAX_KEYWORD,
        'keyword_name.deny_tag' => W_FRM_DENY_TAG_KEYWORD,
        'keyword_name.deny_char' => W_FRM_DENY_CHAR_KEYWORD
    ];

    public function attentions()
    {

        return $this->hasMany('docomo\Models\Attentions\Attention');
    }

    public function questions()
    {
        return $this->belongsToMany('docomo\Models\Questions\Question', 'question_tags');
    }

    public function scopeVisible($query)
    {

        return $query->where('is_visible', '=', VISIBLE);
    }

    /**
     * [get key all keyword in table keywords by $flag]
     *
     * @param  [int]|[NULL] $flag [flag asign this method get all keyword or ger only keyword visible]
     * @return [Array]       [Array of keyword objects]
     */
    public static function getList($show_all = false)
    {
        
        if ($show_all) {
            return Keyword::get(['id', 'keyword_name', 'is_visible']);
        }
        
        return Keyword::visible()->get(['id', 'keyword_name', 'is_visible']);
    }
    
    /**
     * [get 1 keyword in table keywords by id]
     *
     * @param  [int] $id [The id of keyword in table keyword]
     * @return [Keyword]     [keyword object]
     */
    public static function getDetails($id)
    {
        
        return Keyword::findOrFail($id, ['id', 'keyword_name', 'is_visible']);
    }

    /**
     * Fill form data and modify data
     *
     * @return account[]
     */
    public static function fillWithModify($id = null)
    {

        $keyword = null;
        if ($id === null)
        {
            $keyword = new Keyword;
        }
        else
        {
            $keyword = Keyword::getDetails($id);
            unset(Keyword::$rules['keyword_name'][1]);
        }

        $keyword->fill(\Input::all());

        $keyword->is_visible = \Input::has('is_visible') ? false : true;

        return $keyword;
    }

    public static function getByWord($word = null)
    {
        $query = Keyword::select('id', 'keyword_name')->visible();
        if (!is_null($word)) {
            $query = $query->whereRaw('`keyword_name` like concat(\'%\', ?, \'%\')', [$word]);
        } else {
            $query = $query->whereId(0);
        }

        return $query->get();
    }
}
