@if($features->count() > 0)
<section class="l-margin">
    <h3 class="subSectionTit featureIcon">このアンケートが掲載されている特集を見る</h3>
    @foreach($features as &$feature)
    <h4 class="subSectionTitH4 relationIcon"><span>{{ $feature->title }}</span></h4>
    <div class="links l-margin">
    <ul>
        <?php $idx = 1;
        $questionList = $feature->questionList(); ?>
        @foreach ($questionList as &$rec)
            @if ($rec->id != $question->id)
            @include('layouts.elements.displays.list_box', ['question' => $rec, 'show_entry_tag' => true, 'linkId' => 'f_feature' . sprintf('%1$02d', $idx++)])
            @endif
        @endforeach
    </ul>
    </div>
    @endforeach
</section>
@endif

<section class="l-margin">
    <h3 class="subSectionTit entryIcon">もっと投票しよう！</h3>
    <h4 class="subSectionTitH4 relationIcon"><span>どんどん答えて！</span></h4>
    <div class="links">
        @if($sameGenreOngoingQuestions->count() > 0)
            <ul>
                <?php $idx = 1; ?>
                @foreach($sameGenreOngoingQuestions as $rec)
                    @include('layouts.elements.displays.list_box', ['question' => $rec, 'linkId' => 're_samevote' . sprintf('%1$02d', $idx++), 'hide_answer' => true])
                @endforeach
            </ul>
        <p class="gotoListArea">{{ link_to_route('list.unanswered', 'もっと見る', null, ['id' => 're_samevotemore']) }}</p>
        @else
        <p class='noResults'>未回答アンケートはありません。</p>
        @endif
    </div><!--links-->
</section>

@include('front.entry._share')
<section class="relationBox l-margin">
    @if ($question->getKeywords() && $question->getKeywords()->count() > 0)
        <div class="keywords m-margin">
            <h3>関連キーワード</h3>
            <ul class="words">
                <?php $idx = 1; ?>
                @foreach($question->getKeywords() as $keyword)
                    <li>{{ link_to_route('list.tag.' . (($question->isFinished()) ? 'result' : 'entry'), $keyword->keyword_name, ['id' => $keyword->id], ['id' => 'meta_voteword' . sprintf('%1$02d', $idx++) ]) }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="keywords">
        <h3>ジャンル</h3>
        <ul class="words">
            <li>{{{ $question->getParentGenre()->genre_parent_name }}}</li> &gt;
            <li>{{ link_to_route('list.genre.' . (($question->isFinished()) ? 'result' : 'entry'), $question->childGenre->genre_child_name, ['parent_genre_name' => $question->getParentGenreDirectory(), 'child_genre_name' => $question->getChildGenreDirectory()], ['id' => 'meta_votegenre']) }}</li>
        </ul>
    </div>
</section>
<aside class="l-margin">
    <p class="tcenter m-margin">{{ link_to_route('list.unanswered', '未回答のアンケートを見る！', null, ['class' => 'btnType02', 'id' => 'meta_vote_other']) }}</p>
</aside>
