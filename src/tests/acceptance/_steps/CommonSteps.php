<?php
namespace AcceptanceTester;

class CommonSteps extends \AcceptanceTester
{
	public function fillDateTime($date)
	{
		$I = $this;
		$I->fillDateTimeCommon($date['from']);
		$I->fillDateTimeCommon($date['to']);
	}

	public function fillDateTimeCommon($date_time)
	{
		$I = $this;

		$date = $date_time['date'];
		$I->click('input[name='.$date['name'].']');
		$I->click($date['value']);

		$hour=isset($date_time['hour'])?$date_time['hour']:NULL;
		if (isset($hour)) {
			$I->selectOption('form select[name='.$hour['name'].']', $hour['value']);
		}

		$minute=isset($date_time['minute'])?$date_time['minute']:NULL;
		if (isset($minute)) {
			$I->selectOption('form select[name='.$minute['name'].']', $minute['value']);
		}
	}

	public function checkLinkCommon($link)
	{
		$I = $this;
		if (isset($link['url'])) {
			$I->seeLink($link['title'], $link['url']);
			$I->checkLinkClickCommon($link);
		}
		else {
			$I->seeLink($link['title']);
		}
	}

	public function checkLinkClickCommon($link)
	{
		$I = $this;
		if (isset($link['xpath'])) {
			$I->click($link['xpath']);
		}
		else {
			$I->click($link['title']);
		}
		$I->seeCurrentUrlEquals($link['url']);
		$I->moveBack();
	}

	public function checkImageCommon($img_url)
	{
		$I = $this;
		$I->seeElement('//img[src="'.$img_url.'"]');
	}

	public function fillTextCommon($input)
	{
		$I = $this;
		$I->fillField('input[name='.$input['name'].']', $input['value']);
	}

	public function fillTextAreaCommon($input)
	{
		$I = $this;
		$I->fillField('form textarea[name='.$input['name'].']', $input['value']);
	}

	public function selectOptionCommon($option)
	{
		$I = $this;
		$I->selectOption('form select[name='.$option["name"].']', $option["value"]);
	}

	public function checkSelectedOptionCommon($option)
	{
		$I = $this;
		$I->seeOptionIsSelected('form select[name='.$option["name"].']', $option["value"]);
	}

	public function checkBoxCommon($checkbox_name, $is_check=1)
	{
		$I = $this;
		if ($is_check==1) {
			$I->checkOption('input[name='.$checkbox_name.']');
		}
		else {
			$I->unCheckOption('input[name='.$checkbox_name.']');
		}
	}


	public function checkQuestionListCommon($questions)
	{
		$xpath_format = $questions['xpath'];
		$url_format = $questions['url'];
		for ($i=1; $i <= count($questions) ; $i++) {
			$question = $questions[$i];
			$xpath = sprintf($xpath_format, $i);
			$url = sprintf($url_format, $i);
			$I->seeLink($question['title'], $url);
			$I->see($question['title'], $xpath);
		}
	}

	//create profile data
    public function createProfile($profile, $descriptions)
    {
        $I = $this;
        $I->amOnPage('/profile');
		$this->checkProfileIntroText($descriptions);
        $this->createCommonProfile($profile);
    }
    public function createCommonProfile($profile)
    {
        $I = $this;
        $I->selectOptionCommon($profile["sex"]);
        $I->selectOptionCommon($profile["year"]);
        $I->selectOptionCommon($profile["month"]);
        $I->selectOptionCommon($profile["day"]);
        $I->selectOptionCommon($profile["place"]);
        $I->click($profile["submit"]);
    }

	//create data from admin
	public function createNotice($notices)
	{
		$I = $this;
		$I->amOnPage('/manage');
		$I->loginClickCommon('お知らせ一覧・登録・修正・削除');

		foreach ($notices as $notice) {
			$I->createNoticeCommon($notice);
		}
	}

	public function createNoticeCommon($notice)
	{
		$I = $this;
		$I->amOnPage('/manage/notice');
		$I->wait(1);
		$I->click('＋おしらせ追加');

		$I->fillTextCommon(array('name' => 'title', 'value' => $notice['title']));
		$I->fillTextCommon(array('name' => 'description', 'value' => $notice['description']));
		$I->fillTextAreaCommon(array('name' => 'details', 'value' => $notice['details']));
		$this->fillDateTime($notice['date']);
		$I->click('確認');
		$I->click('登録');
		$I->seeInPopup('登録します。よろしいですか？');
        $I->acceptPopup();
	}

	public function createParentGenre($parent_genres)
	{
		$I = $this;
		$I->amOnPage('/manage');
		$I->loginClickCommon('ジャンル一覧・登録・修正');

		foreach ($parent_genres as $parent_genre) {
			$I->createParentGenreCommon($parent_genre);
			if (isset($parent_genre['childs'])) {
				$this->createChildGenre($parent_genre, $parent_genre['childs']);
			}
		}
	}

	public function createParentGenreCommon($parent_genre)
	{
		$I = $this;
		$I->amOnPage('/manage/genre');
		$I->wait(1);
		$I->click('＋ジャンル追加');

		$I->fillTextCommon(array('name' => 'genre_parent_name', 'value' => $parent_genre['name']));
		$I->fillTextCommon(array('name' => 'genre_parent_directory', 'value' => $parent_genre['directory']));
		$this->checkBoxCommon('parent_is_visible', $parent_genre['status']);
		$I->click('登録');
		$I->seeInPopup('登録します。よろしいですか？');
        $I->acceptPopup();
	}

	public function createChildGenre($parent_genre, $child_genres)
	{
		$I->$this;
		foreach ($chil_genres as $child_genre) {
			$I->createChildGenreCommon($parent_genre, $child_genre);
		}
	}

	public function createChildGenreCommon($parent_genre, $child_genre)
	{
		$I = $this;
		$url = '/manage/genre/'.$parent_genre['id'].'/edit';
		$I->amOnPage($url);
		$I->wait(1);

		$I->fillTextCommon(array('name' => 'genre_child_name', 'value' => $child_genre['name']));
		$I->fillTextCommon(array('name' => 'genre_child_directory', 'value' => $child_genre['directory']));
		$I->fillTextCommon(array('name' => 'genre_child_description', 'value' => $child_genre['description']));
		$this->checkBoxCommon('child_is_visible', $child_genre['status']);
		$I->click('追加');
		$I->seeInPopup('小ジャンルを追加します。よろしいですか？');
        $I->acceptPopup();
	}

	public function createKeyword($keywords)
	{
		$I = $this;
		$I->amOnPage('/manage');
		$I->loginClickCommon('キーワード一覧・登録・修正');

		foreach ($keywords as $keyword) {
			$I->createKeywordCommon($keyword);
		}
	}

	public function createKeywordCommon($keyword)
	{
		$I = $this;
		$I->amOnPage('/manage/keyword');
		$I->wait(1);
		$I->click('＋ジャンル追加');

		$I->fillTextCommon(array('name' => 'keyword_name', 'value' => $keyword['name']));
		$I->checkBoxCommon('is_visible', $keyword['status']);
		$I->click('登録');
		$I->seeInPopup('登録します。よろしいですか？');
        $I->acceptPopup();
	}

	public function createAttention($attentions)
	{
		$I = $this;
		$I->amOnPage('/manage');
		$I->loginClickCommon('注目キーワードデータ作成');

		foreach ($attentions as $key => $attention) {
			$I->createAttentionCommon($attention);
		}
	}

	public function createAttentionCommon($attention)
	{
		$I = $this;
		$I->amOnPage('/manage/attention/create');
		$I->wait(1);

		$I->selectKeywordCommon($attention['keywords']);
		$I->selectOptionCommon($attention['tab']);
		$I->fillDateTime($attention['date']);

		$I->click('登録');
		$I->seeInPopup('登録します。よろしいですか？');
        $I->acceptPopup();
	}

	public function selectKeywordCommon($keywords)
	{
		$I = $this;
		foreach ($keywords as $keyword) {
			$I->click('キーワードウィンドウを開く');
			$I->switchToWindow('keywordwindow');
			$I->click($keyword);
			$I->switchToWindow();
		}
	}

	public function createFeature($features)
	{
		$I = $this;
		$I->amOnPage('/manage');
		$I->loginClickCommon('特集枠データ作成');

		foreach ($features as $feature) {
			$I->createFeatureCommon($feature);
		}
	}

	public function createFeatureCommon($feature)
	{
		$I = $this;
		$I->amOnPage('/manage/feature/create');
		$I->wait(1);

		$I->fillTextCommon(array('name' => 'title', 'value' => $feature['title']));
		$I->fillTextAreaCommon(array('name' => 'message', 'value' => $feature['message']));
		$I->fillDateTime($feature['date']);
		$I->checkBoxCommon('is_view_list', $feature['is_view_list']);
		$I->checkBoxCommon('is_feature_column', $feature['is_feature_column']);
		$I->fillTextCommon(array('name' => 'question_ids', 'value' => $feature['question_ids']));

		$I->click('確認');
		$I->click('登録');
		$I->seeInPopup('登録します。よろしいですか？');
        $I->acceptPopup();
	}

	public function createQuestion($questions)
	{
		$I = $this;
		$I->amOnPage('/manage');
		$I->loginClickCommon('アンケート新規投稿');

		foreach ($questions as $question) {
			$I->createQuestionCommon($question);
		}
	}

	public function createQuestionCommon($question)
	{
		$I = $this;
		$I->amOnPage('/manage/question/create');
		$I->wait(1);

		$I->fillDateTime($question['date']);
		$I->fillTextCommon(array('name' => 'title', 'value' => $feature['title']));
		$I->fillTextCommon(array('name' => 'details', 'value' => $feature['details']));
		$I->fillTextAreaCommon(array('name' => 'choiceTextarea', 'value' => $feature['choice_text']));

		if (isset($question['keywords'])) {
			$I->selectKeywordCommon($attention['keywords']);
		}

		$I->click('承認申請');
		//TODO confirm
	}

	//create data from DB
   public function createDbGenreParent($genre_parent)
    {
    	$I = $this;
        foreach ($genre_parent as $key => $value) {
            $I->haveInDatabase('genre_parents', array(
                                                'genre_parent_name'=>$value['genre_parent_name'],
                                                'parent_is_visible'=>$value['parent_is_visible'],
                                                'genre_parent_directory'=>$value['genre_parent_directory'],
                                                'parent_sort_order'=>$value['parent_sort_order']
                                                )
            );
        }
    }
    public function createDbGenreChild($genre_child)
    {
    	$I = $this;
        foreach ($genre_child as $key => $value) {
            $I->haveInDatabase('genre_children', array(
                                                    'genre_parent_id'=>$value['genre_parent_id'],
                                                    'genre_child_name'=>$value['genre_child_name'],
                                                    'child_is_visible'=>$value['child_is_visible'],
                                                    'genre_child_directory'=>$value['genre_child_directory'],
                                                    'child_sort_order'=>$value['child_sort_order']
                                                )
            );
        }
    }
    public function createDbQuestion($questions)
    {
    	$I = $this;
        foreach ($questions as $key => $value) {
            $I->haveInDatabase('questions', array(
                                                'genre_parent_id'=>$value['genre_parent_id'],
                                                'genre_child_id'=>$value['genre_child_id'] ,
                                                'status'=>$value['status'] ,
                                                'title'=>$value['title'] ,
                                                'vote_date_from'=>$value['vote_date_from'] ,
                                                'vote_date_to'=>$value['vote_date_to'] ,
                                                'baseline_date'=>$value['baseline_date'] ,
                                                'viewable_type'=>$value['viewable_type'] ,
                                                'visible_device_i'=>$value['visible_device_i'] ,
                                                'visible_device_d'=>$value['visible_device_d'] ,
                                                'summary_type'=>$value['summary_type'] ,
                                                'average_unit'=>$value['average_unit'] ,
                                                'is_visible_other'=>$value['is_visible_other'] ,
                                                'shuffle_choice'=>$value['shuffle_choice'] ,
                                                'navigation_title'=>$value['navigation_title'] ,
                                                'account_id'=>$value['account_id'] ,
                                                )
            );
        }
    }

    public function createDbColumns($columns)
    {
    	$I = $this;
        foreach ($columns as $key => $value) {
            $I->haveInDatabase('columns', array(
                                                'question_id' => $value['question_id'],
                                                'is_visible' => $value['is_visible'],
                                                'column_title' => $value['column_title'],
                                                'column_detail' => $value['column_detail'],
                                                'view_create_datetime' => $value['view_create_datetime'],
                                                'created_account_id' => $value['created_account_id'],
                                                'updated_account_id' => $value['updated_account_id'],
                                                )
            );
        }
    }


    public function createDbKeyword($keywords)
    {
    	$I = $this;
        foreach ($keywords as $key => $value) {
            $I->haveInDatabase('keywords', array(
                                                'keyword_name' => $value['keyword_name'],
                                                'is_visible' => $value['is_visible'],
                                                )
            );
        }
    }

    public function createDbAttentions($attentions)
    {
    	$I = $this;
        foreach ($attentions as $key => $value) {
            $I->haveInDatabase('attentions', array(
                                                'keyword_id' => $value['keyword_id'],
                                                'tab' => $value['tab'],
                                                'from' => '2015-01-01 00:00:00',
                                                'to' => '2015-04-01 00:00:00',
                                                )
            );
        }
    }

    public function createDbQuestionTags($tags)
    {
    	$I = $this;
        foreach ($tags as $key => $value) {
            $I->haveInDatabase('question_tags', array(
                                                'question_id' => $value['question_id'],
                                                'keyword_id' => $value['keyword_id'],
                                                )
            );
        }
    }


    public function createDbResultVote($result_votes)
    {
    	$I = $this;
        foreach ($result_votes as $key => $value) {
            $I->haveInDatabase('result_votes', array(
                                                'question_id' => $value['question_id'],
                                                'attribute_id' => '1',
                                                'attribute_value_id' => '1',
                                                'device' => '1',
                                                'choice_id' => '1',
                                                'ranking' => '1',
                                                'voted_count' => $value['voted_count'],
                                                'summary_value' => '1',
                                                )
            );
        }
    }

    public function createDbFeature($features)
    {
    	$I = $this;
        foreach ($features as $key => $value) {
            $I->haveInDatabase('feature', array(
                                                'title' => $value['title'],
                                                'message' => $value['message'],
                                                'from' => '2015-01-01 00:00:00',
                                                'to' => '2015-04-01 00:00:00',
                                                'is_view_list' => 1,
                                                'question_ids' => $value['question_ids'],
                                                'is_feature_column' => 1,
                                                'created_account_id' => 1,
                                                'updated_account_id' => 1,
                                                )
            );
        }
    }

    public function createDbMakesenses($makesenses)
    {
    	$I = $this;
        foreach ($makesenses as $key => $value) {
            $I->haveInDatabase('makesenses', array(
                                                'question_id' => $value['question_id'],
                                                'user_id' => $value['user_id'],
                                                'status' => $value['status'],
                                                )
            );
        }
    }

    public function createDbAnswers($answers)
    {
    	$I = $this;
        foreach ($answers as $key => $value) {
            $I->haveInDatabase('answers', array(
                                                'question_id' => $value['question_id'],
                                                'user_id' => '1',
                                                'choice_id' => '1',
                                                'age' => '25',
                                                'gender' => '1',
                                                'local' => '1',
                                                'answer_date' => '2015-01-05 00:00:00',
                                                )
            );
        }
    }

    public function createDbPickups($pickups)
    {
    	$I = $this;
        foreach ($pickups as $key => $value) {
            $I->haveInDatabase('pickups', array(
                                                'question_id' => $value['question_id'],
                                                'type' => $value['type'],
                                                'from' => '2015-01-05 00:00:00',
                                                'to' => '2015-04-05 00:00:00',
                                                )
            );
        }
    }

    public function createDbSummaryByTypes($summary_by_types)
    {
    	$I = $this;
        foreach ($summary_by_types as $key => $value) {
            $I->haveInDatabase('summary_by_types', array(
                                                'question_id' => $value['question_id'],
                                                'type' => $value['type'],
                                                'count' => $value['count'],
                                                'sort_order' => $value['sort_order'],
                                                )
            );
        }
    }

    public function createDbNotice($notices)
    {
    	$I = $this;
        foreach ($notices as $key => $value) {
            $I->haveInDatabase('notices', array(
                                                'title' => $value['title'],
                                                'description' => $value['description'],
                                                'details' => $value['details'],
                                                'from' => $value['from'],
                                                'to' => $value['to'],
                                                )
            );
        }
    }

}