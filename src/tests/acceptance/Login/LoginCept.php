<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('login to manage pages');
$I->amOnPage('/manage');
$I->loginWithDetail(['admin', 'test']);
$I->loginWithDetail(['test', '123456']);
$I->loginWithDetail(['admin', '123456']);
