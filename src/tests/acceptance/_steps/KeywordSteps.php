<?php
namespace AcceptanceTester;

class KeywordSteps extends \AcceptanceTester\CommonSteps
{

	public function checkKeyWord()
	{
		$I = $this;
		$this->checkLinkSwitch();
		$this->checkLinkSwitchResult();
		$this->checkListSurveyByKeyword();
		$this->checkSettingRecomment();
	}


	public function checkLinkSwitch($keyword)
	{
		$I = $this;
		$I->see($keyword);
	}

}