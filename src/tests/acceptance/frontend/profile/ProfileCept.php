<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('check front profile');

$profile = array(
				'sex' => array('name' => 'sex', 'value' => '男性'),
				'year' => array('name' => 'birthday_year', 'value' => '1990'),
				'month' => array('name' => 'birthday_month', 'value' => '06'),
				'day' => array('name' => 'birthday_day', 'value' => '01'),
				'place' => array('name' => 'pref', 'value' => '京都府'),
				'link' => array(
								'button_confirm' => '登録する',
								'link_profile' => 'プロフィール確認 >',
								'link_top' => 'みんなの声TOPへ >'
								),
				'descriptions' => array(
									'プロフィール設定でアンケート結果が楽しくなるよ！', 
									'※プロフィール設定でできることの説明', 
									'あなたのプロフィールに合った受付中アンケートをTOPですぐに見つけることができます',
									'あなたのプロフィールに合ったアンケート結果を一覧でみることができます',
									'投票結果で、あなたのあなたのプロフィールに合った情報をすぐに見ることができます'
									),
				);

// $messages = array();
//$I->checkFrontProfileValidate($profile, $messages);
$I->createFrontProfile($profile);
$I->checkFrontProfileConfirm($profile, '0');
$I->checkFrontProfileExit($profile);
$I->checkFrontProfileConfirm($profile, '1');

//TODO check profile existed when re-visit
$I->amOnPage('/profile');
$I->checkFrontProfileExit($profile);