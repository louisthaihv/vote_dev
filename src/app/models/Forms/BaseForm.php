<?php
namespace docomo\Models\Forms;

use Laracasts\Validation\FormValidator;
use Laracasts\Validation\FormValidationException;

abstract class BaseForm extends FormValidator
{
    
    /**
     * Form data
     * @var array
     */
    protected $form_data = [];
    
    protected $columns = [];
    
    /**
     * [add unique validate of properties $id]
     *
     * @param [mixed] $id [properties]
     */
    protected function _addUniqueId($id)
    {
        foreach ($this->rules as $key => $value) {
            $replaced = preg_replace("/(unique:[a-z_]+)/", "$1,{$key},{$id}", $value);
            $this->rules[$key] = $replaced;
        }
    }
    
    /**
     * [set column want to validate in this form]
     *
     * @return [array] [new rules]
     */
    protected function _selectColumns()
    {
        if (count($this->columns) > 0) {
            $rules = [];
            foreach ($this->columns as $key) {
                $rules[$key] = $this->rules[$key];
            }
            $this->rules = $rules;
        }
    }
    
    /**
     * [validate post_data]
     * @param  [array] $post_data [inputs of form]
     *
     * @return [Validation]
     */
    
    public function validate($post_data)
    {

        $this->form_data = $post_data;
         //setting to class variable for later use
        $this->_selectColumns();
        $this->validation = $this->validator->make($post_data, $this->getValidationRules(), $this->getValidationMessages());
        if ($this->validation->fails()) {
            throw new FormValidationException('Validation failed', $this->getValidationErrors());
        }
        
        return true;
    }
    
    /**
     * Modify the form data, override this function in child classes to modify the form data
     */
    protected function modify()
    {
    }
    
    /**
     * Get modified data
     * @return array
     */
    public function getModifiedData()
    {
        
        $this->modify();
        
        return $this->form_data;
    }
}
