<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ConvertQuestionCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'convert:question';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update question kana.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        \DB::disableQueryLog();
        for ($forI = 1; $forI < 40000; $forI ++) {
            $questions = \DB::select('SELECT id, title, details FROM questions WHERE id = ?', [$forI]);
            foreach ($questions as &$question) {
                $this->info('now target is ' . $question->id);
                \DB::update('UPDATE questions SET title=?, details=? WHERE id=?',
                    [
                        trim(mb_convert_kana($question->title, 'rnKV')),
                        trim(mb_convert_kana($question->details, 'rnKV')),
                        $question->id]);
                $choices = \DB::select('SELECT id, choice_text FROM choices WHERE question_id =?', [$question->id]);
                foreach ($choices as &$choice) {
                    \DB::update('UPDATE choices SET choice_text=? WHERE id=?',
                        [trim(mb_convert_kana($choice->choice_text, 'rnKV')), $choice->id]);
                    unset($choice);
                }
                unset($choices);
                unset($question);
            }
            unset($questions);
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
