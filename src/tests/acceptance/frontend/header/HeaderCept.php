<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('check header');
$I->amOnPage('/');
$arr = array(
            'TOP'=>'/',
            '受付中'=>'/list/entry',
            '結果発表'=>'/list/result',
            'マイページ'=>'/mypage',
            '新着アンケート'=>'/list/new/result',
            '人気アンケート'=>'/list/popular/result',
            '未回答アンケート'=>'/list/unanswered',
            'ジャンル一覧'=>'/list/genre',
            '特集一覧'=>'/feature',
            'コラム一覧' =>'/list/column',
            'プロフィール登録編集'=>'/profile',
            '操作説明'=>'/help',
            );
$I->checkHeader($arr, './/*[@id="top"]/div/p/a/img');
$arr_notice = array(
                  'title'=>'title_1',
                  'description'=>'description_1',
                  'details'=>'details_1',
                  'from_date'=>'from_date',
                  'from_hour'=>'from_hour',
                  'from_min'=>'from_min',
                  'to_date'=>'to_date',
                  'to_hour'=>'to_hour',
                  'to_min'=>'to_min',
                  'from_date_xpath_datepicker_day' =>'.//*[@id="ui-datepicker-div"]/table/tbody/tr[4]/td[4]/a',
                  'from_date_xpath_datepicker_hour' =>'11',
                  'from_date_xpath_datepicker_min' =>'11',
                  'to_date_xpath_datepicker' =>'.//*[@id="ui-datepicker-div"]/table/tbody/tr[5]/td[4]/a',
                  'to_date_xpath_datepicker_hour' =>'11',
                  'to_date_xpath_datepicker_min' =>'11',
                  );
$I->createNewNotice($arr_notice);
$arr_notice_2 = array(
                  'title'=>'title_2',
                  'description'=>'description_2',
                  'details'=>'details_2',
                  'from_date'=>'from_date',
                  'from_hour'=>'from_hour',
                  'from_min'=>'from_min',
                  'to_date'=>'to_date',
                  'to_hour'=>'to_hour',
                  'to_min'=>'to_min',
                  'from_date_xpath_datepicker_day' =>'.//*[@id="ui-datepicker-div"]/table/tbody/tr[1]/td[2]/a',
                  'from_date_xpath_datepicker_hour' =>'11',
                  'from_date_xpath_datepicker_min' =>'11',
                  'to_date_xpath_datepicker' =>'.//*[@id="ui-datepicker-div"]/table/tbody/tr[2]/td[3]/a',
                  'to_date_xpath_datepicker_hour' =>'11',
                  'to_date_xpath_datepicker_min' =>'11',
                  );
$I->createNewNotice($arr_notice_2);
$I->checkNoticeHeader($arr_notice, '1', $arr_notice_2);