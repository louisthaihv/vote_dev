<?php

namespace docomo\Models\Accounts;

use Laracasts\Commander\CommandHandler;
use docomo\Models\Events\EventDispatcher;

class AccountLoggedInCommandHandler implements CommandHandler
{
    
    protected $account;
    private $dispatcher;
    
    public function __construct(Account $account, EventDispatcher $dispatcher)
    {
        
        $this->account = $account;
        $this->dispatcher = $dispatcher;
    }
    
    /**
     * Handle the command
     *
     * @param $command AccountLoggedInCommand This data tranfer to AccountController when login.
     * @return mixed
     */
    public function handle($command)
    {
        
        if (\Auth::attempt(['login_id' => $command->login_id, 'password' => $command->login_password, 'is_enabled' => ENABLED])) {
            
            //\Auth::user()->raiseLogin();
            //$this->dispatcher->dispatch(\Auth::user()->releaseEvents());
            return \Redirect::intended('manage');
        } else {
            return \Redirect::intended('manage')->with("message", W_MSG_FAILED_LOGIN);
        }
    }
}
