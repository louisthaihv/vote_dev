<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeatureTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('feature', function (Blueprint $table)
		{
			// schema
			$table->increments('id');
			$table->string('title', 200);
			$table->text('thumbnail');
			$table->string('message', 1000);
			$table->datetime('from');
			$table->datetime('to');
			$table->boolean('is_view_list');
			$table->string('question_ids', 200);
			$table->boolean('is_feature_column');
			$table->integer('created_account_id');
			$table->integer('updated_account_id');
			$table->timestamps();
			// indexes
			$table->index('from');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('feature');
	}

}
