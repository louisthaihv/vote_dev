@extends('layouts.front_fp')

@section('content')

    <div style="background-color:#fff6ab;padding:5px;text-align:center;"><font color="#3e3a39"> {{{ $notice->title }}} </font></div>
    @include('layouts.front_elements._space')
    <div style="margin:0px 10px 0px 10px;">
        {{ $notice->details }}
    </div>
    <div style="text-align:center"><a href="{{ route('home.top', ['uid' => 'NULLGWDOCOMO']) }}">みんなの声TOPへ</a></div>
    @include('layouts.front_elements._space')

@endsection
