<?php
$I = new AcceptanceTester($scenario);
$I->amOnPage('/manage');
$I->loginWithDetail(['admin', '123456']);

$I->click('キーワード一覧・登録・修正・削除');
$I->create('keyword/add', '＋キーワード追加');
$I->see('キーワード登録');
$I->see('キーワード名');
$I->see('非表示');

//no input keyword create
$I->checkValidKeyword();

//keyword_input is ok
$I->createNewKeyword('keyword_test_1');

//keyword_input same old keyword
$I->checkSameNameKeyword('keyword_test_2');

//keyword hidden
$I->checkHiddenKeyword('keyword_test_1');
