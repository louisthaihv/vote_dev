<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTablePickupKeywords extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pickup_keywords', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('keyword_id');
            $table->integer('visible_tab');
            $table->timestamp('visible_date_from');
            $table->timestamp('visible_date_to');
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pickup_keywords');
    }
}
