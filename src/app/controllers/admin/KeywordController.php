<?php
namespace Admin;

use docomo\Models\Keywords\Keyword;

class KeywordController extends \BaseController
{
    
    /**
     * [construct function]
     *
     * @param AddKeywordForm  $addKeywordForm
     * @param UpdateKeywordForm $editKeywordForm
     */
    public function __construct()
    {

    }
    
    /**
     * [get keyword by flag]
     *
     * @param  [int] $flag [this is a flag decide get all keywords or get only keyword visible]
     * @return [View]       [Return to view admin.keyword.index with params]
     */
    public function index()
    {

        $show_all = (\Input::get(SHOW_ALL) !== null);
        $keywords = Keyword::getList($show_all);

        return \View::make('admin.keyword.index', compact('keywords', 'show_all'));
    }
    
    /**
     * [get create keyword form]
     * @return [View] [Return to view admin.keyword.create]
     */
    public function create()
    {

        return \View::make('admin.keyword.create', ['keyword' => new Keyword]);
    }

    /**
     * [process create keyword]
     *
     * @return [Redirect] [Redirect to route admin.keyword.index]
     */
    public function store()
    {

        $keyword = Keyword::fillWithModify();

        if ($keyword->save())
        {
            return \Redirect::route('manage.keyword.index');
        }
        else
        {
            return \View::make('admin.keyword.create', compact('keyword'));
        }
    }

    /**
     * [get edit keyword form]
     *
     * @param  [int] $id [id of keyword]
     * @return [View]     [Return view admin.keyword.edit with param]
     */
    public function edit($id)
    {

        $keyword = Keyword::getDetails($id);

        return \View::make('admin.keyword.edit', compact('keyword'));
    }
    
    /**
     * [process edit keyword]
     * @param  [int] $id [id of keyword]
     * @return [Redirect]     [Redirect to route admin.keyword.index]
     */
    public function update($id)
    {

        $keyword = Keyword::fillWithModify($id);

        if ($keyword->save())
        {
            return \Redirect::route('manage.keyword.index');
        }
        else
        {
            return \View::make('admin.keyword.edit', compact('keyword'));
        }
    }

    /**
     * [listKeywordPick List popup show all keywords can pick up ]
     * @return [type] [View show all keywords can pick up]
     */
    public function pick($max = 1)
    {
        $keywords = Keyword::getList(null);

        return \View::make("admin.keyword.pick", compact('keywords', 'max'));

    }
}
