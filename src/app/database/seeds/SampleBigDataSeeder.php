<?php

use Faker\Factory as Faker;
use docomo\Models\Questions\Question;
use docomo\Models\Genres\ParentGenre;
use docomo\Models\Genres\ChildGenre;

class SampleBigDataSeeder extends Seeder
{
    public function run()
    {
        $this->command->info('create faker');
        $faker = Faker::create();

        $this->command->info('get genres');
        // Question
        $pId = ParentGenre::all()->lists('id');
        $cId = ChildGenre::all()->lists('id');

        $this->command->info('create questions ready');
        DB::table('questions')->truncate();
        $arrQues = [];
        foreach (range(1, 500) as $index) {
            $arrQues[] = [
                'genre_parent_id' => $faker->randomElement($pId),
                'genre_child_id' => $faker->randomElement($cId),
                'status' => rand(1, 6),
                'title' => $faker->sentence(5),
                'details' => $faker->sentence(5),
                'vote_date_from' => $faker->dateTimeBetween($startDate = '-1 days', $endDate = 'now'),
                'vote_date_to' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+1 years'),
                'viewable_type' => 1,
                'visible_device_i' => 1,
                'visible_device_d' => 1,
                'summary_type' => 0,
                'average_unit' => 0,
                'is_visible_other' => 0,
                'other_text' => 'other',
                'shuffle_choice' => 0,
                'result_comment' => 'result_comment',
                'navigation_title' => 'navigation_title',
                'sns_comment' => 'sns_comment',
                'comment' => 'admin_comment',
                'back_button_text' => 'back_btn',
                'back_button_url' => 'back_url',
                'has_column' => 1,
                'account_id' => 1,
            ];
        }
        DB::table("questions")->insert($arrQues);
        unset($arrQues);
        $this->command->info('create questions finished');
        foreach (range(1, 10000) as $index) {
            $arrQues[] = [
                'genre_parent_id' => $faker->randomElement($pId),
                'genre_child_id' => $faker->randomElement($cId),
                'status' => 6,
                'title' => $faker->sentence(5),
                'details' => $faker->sentence(5),
                'vote_date_from' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = '-1 months'),
                'vote_date_to' => $faker->dateTimeBetween($startDate = '-1 months', $endDate = '-1 days'),
                'viewable_type' => 1,
                'visible_device_i' => 1,
                'visible_device_d' => 1,
                'summary_type' => 0,
                'average_unit' => 0,
                'is_visible_other' => 0,
                'other_text' => 'other',
                'shuffle_choice' => 0,
                'result_comment' => 'result_comment',
                'navigation_title' => 'navigation_title',
                'sns_comment' => 'sns_comment',
                'comment' => 'admin_comment',
                'back_button_text' => 'back_btn',
                'back_button_url' => 'back_url',
                'has_column' => 0,
                'account_id' => 1
            ];
            if (count($arrQues) > 1000) {
                $this->command->info('insert 1000');
                DB::table("questions")->insert($arrQues);
                unset($arrQues);
            }
        }
        DB::table("questions")->insert($arrQues);
        unset($arrQues);
        $this->command->info('create questions draft');
        foreach (range(1, 100) as $index) {
            $arrQues[] = [
                'genre_parent_id' => $faker->randomElement($pId),
                'genre_child_id' => $faker->randomElement($cId),
                'status' => rand(1, 4),
                'title' => $faker->sentence(5),
                'details' => $faker->sentence(5),
                'vote_date_from' => $faker->dateTimeBetween($startDate = '+10 days', $endDate = '+1 months'),
                'vote_date_to' => $faker->dateTimeBetween($startDate = '+1 months', $endDate = '+1 years'),
                'viewable_type' => 1,
                'visible_device_i' => 1,
                'visible_device_d' => 1,
                'summary_type' => 0,
                'average_unit' => 0,
                'is_visible_other' => 0,
                'other_text' => 'other',
                'shuffle_choice' => 0,
                'result_comment' => 'result_comment',
                'navigation_title' => 'navigation_title',
                'sns_comment' => 'sns_comment',
                'comment' => 'admin_comment',
                'back_button_text' => 'back_btn',
                'back_button_url' => 'back_url',
                'has_column' => 1,
                'account_id' => 1
            ];
        }
        $this->command->info('insert data');
        DB::table("questions")->insert($arrQues);
        unset($arrQues);

        // Choice
        $this->command->info('create choices');
        DB::table('choices')->truncate();
        $this->command->info('get questions');
        $qId = Question::all(['id'])->lists('id');
        $arrChoice = [];
        $this->command->info('create choices');
        foreach ($qId as $id) {
            foreach (range(1, rand(2, 10)) as $index) {
                $arrChoice[] = [
                    'question_id' => $id,
                    'choice_text' => 'choice' . $index--,
                    'choice_num' => $index
                ];
            }
            if (count($arrChoice) > 10000)
            {
                $this->command->info('insert 10000 choices.');
                DB::table("choices")->insert($arrChoice);
                unset($arrChoice);
            }
        }
        $this->command->info('insert choices');
        DB::table("choices")->insert($arrChoice);
        unset($arrChoice);

        foreach ($qId as $id) {
            \docomo\Models\Answers\ResultVote::createAllRecord(Question::find($id));
        }
    }
}
