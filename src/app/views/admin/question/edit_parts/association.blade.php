    <p>関連付け</p>
    <table class="formTable">
        <tr>
            <th>ジャンル<span class="require">（必須）</span></th><td>
            {{ Form::selectByTableInGroup('genre', $genres, 'id', 'genre_child_name', $question->genre_child_id) }}
            <p id="genreComment"></p>
        </td>
        </tr>
        <tr>
            <th>キーワード<br /><a href="#" class="naviBtn" id="keywordListOpen">キーワードウィンドウを開く</a><br />&nbsp;<br />※キーワードを追加したい場合は<br />コメント欄にてご連絡ください。</th><td>
            @if (!isset($tags) || count($tags) == 0)
                <ul id="keywordList">
                <li>選択されていません</li>
                </ul>
                <p id="keywordClear" class="m-margint" style="display:none;"><a href="#" class="naviBtn">すべてクリア</a></p>
            @else
                <ul id="keywordList">
                @foreach ($tags as $tag)
                        <li class="listK">{{ Form::label('keyword_name', $tag->getKeywordName()) }}
                            {{ Form::hidden('keyword['. $tag->keyword_id .']', $tag->keyword_id, ['id' => 'keyword['. $tag->keyword_id .']']) }}</li>
                @endforeach
                </ul>
                <p id="keywordClear" class="m-margint"><a href="#" class="naviBtn">すべてクリア</a></p>
            @endif
        </td>
        </tr>
    </table>
