@extends('layouts.admin_in')

@section('content')
<h2>アンケート登録</h2>
@include('admin.question.form', ['url' => 'manage/question/create','method' => 'post', 'status' => QUESTION_STATUS_CREATE, 'mode' => MODE_ALL_EDIT])

@stop