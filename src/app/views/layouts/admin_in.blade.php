@extends('layouts.admin')

@section('head')
	@yield('head')
@stop

@section('script')
	@yield('script')
@stop

@section('style')
	@yield('style')
@stop

@section('javascript')
	@yield('javascript')
@stop

@section('link')
<p class="m-margin">
	<a href="{{ route('admin.home') }}">≪ メインメニュー</a>
</p>
@stop

@section('content')
	@yield('content')
@stop