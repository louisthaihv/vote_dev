<?php
namespace Front;

use docomo\Models\Keywords\Keyword;
use docomo\Models\Questions\Question;

class SearchController extends \BaseController
{
    
    public function __construct()
    {
    }
    
    public function result($word = null)
    {
        $keywords = Keyword::getByWord($word);

        $ids = $keywords->lists('id');

        $count_question = Question::getSearchResultList($ids, null, $this->getCurrentUser(), new \DateTime);
        $params['word'] = $word;

        return $this->renderView(\View::make('front.search.result', compact('params', 'keywords', 'count_question')));
    }
}
