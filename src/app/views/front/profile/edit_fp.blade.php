@extends('layouts.front_fp')

@section('content')

@include('layouts.front_elements._space')
<div>{{ HTML::image("common/front/images/fp/tit4.gif", "ﾌﾟﾛﾌｨｰﾙ設定", ['width'=>"100%"]) }}</div>
@if (isset($user->result) && !$user->result)
    @include('layouts.front_elements._space')
    <div style="text-align:center;color:#CC0000">生年月日が正しくありません。</div>
@endif
@include('layouts.front_elements._space')
{{ Form::open(array('route' => 'profile.update')) }}
<div style="margin:0px 10px 0px 10px;">

    @foreach ($attributes as $attribute)
        <div>■{{{ $attribute->attribute_label }}}</div>
        @if ($attribute->id == ATTR_BIRTHDAY)
            {{ Form::text('birthday', (isset($user) ? $user->getBirthdayText() : null), ['size' => 10, 'maxlength' => 8,  'istyle' => '4', 'style' => '-wap-input-format:"*<ja:n>"']) }}
            <div>生年月日は、西暦月日を入力してください。<br />
            (例)1981年2月5日なら・・19810205</div>
        @else
            {{ Form::attributeValueList($attribute->id, array('default' => $user->attrList[$attribute->id], 'select_none' => ' '), $attribute->attribute_directory) }}
        @endif
        @include('layouts.front_elements._space')
    @endforeach

</div>
<div style="text-align:center;">
    {{ Form::submit('　設定する　', array('class'=>'btnType03', 'name'=>'save_btn'))}}
</div>
{{ Form::hidden('uid', $user->uid) }}
{{ Form::close() }}

@include('layouts.front_elements._space')

<div style="background-color:#fff6ab;padding:5px;text-align:left;"><font color="#f39800">▼</font><font color="#3e3a39"> ﾌﾟﾛﾌｨｰﾙ設定でｱﾝｹｰﾄがもっと楽しくなる！ </font></div>
@include('layouts.front_elements._space')
<div style="margin:0px 10px 0px 10px;">
    性別、生年月日、出身地を設定すると・・・<br />
    ※ｱﾝｹｰﾄの「年代別」「性別」結果にあなたの1票が反映されます。<br />
    ※あなたのﾌﾟﾛﾌｨｰﾙに合った限定ｱﾝｹｰﾄに投票したり、結果を見ることができます。
</div>

@stop
