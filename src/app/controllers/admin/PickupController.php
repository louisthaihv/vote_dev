<?php

namespace Admin;

use docomo\Models\Codes\FrontTab;
use docomo\Models\Pickups\Pickup;
use docomo\Models\Questions\Question;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

/**
 * Set datalist used in view.
 */
View::composer([
    'admin.pickup.index',
    'admin.pickup.create',
    'admin.pickup.store',
    'admin.pickup.edit',
    'admin.pickup.update',
        ], function($view) {

    $view->with('typeList', FrontTab::$label);
});

class PickupController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $pickups = Pickup::newer()->get();

        return \View::make('admin.pickup.index', compact('pickups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($id)
    {
        $pickup = new Pickup;
        $id = intval($id);
        $question = Question::select('id', 'title')->find($id);

        return \View::make('admin.pickup.create', compact('pickup', 'question'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($id)
    {
        $pickup = new Pickup;
        $pickup->fill(Input::all());
        $pickup->from = $pickup->getDatetimeControlValue('from');
        $pickup->to = $pickup->getDatetimeControlValue('to');
        $pickup->question_id = $id;

        if ($pickup->save()) {
            return Redirect::route('manage.pickup.index');
        } else {
            $question = Question::select('id', 'title')->find($id);
            return \View::make('admin.pickup.create', compact('pickup', 'question'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $pickup = Pickup::find($id);
        $question = Question::find($pickup->question_id);

        return \View::make('admin.pickup.edit', compact('pickup', 'question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $pickup = Pickup::find($id);
        $pickup->fill(Input::all());
        $pickup->from = $pickup->getDatetimeControlValue('from');
        $pickup->to = $pickup->getDatetimeControlValue('to');

        if ($pickup->save()) {
            return Redirect::route('manage.pickup.index');
        } else {
            $question = Question::find($pickup->question_id);
            return \View::make('admin.pickup.edit', compact('pickup', 'question'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $pickup = Pickup::find($id);
        $pickup->delete();

        return Redirect::route('manage.pickup.index');
    }
}
