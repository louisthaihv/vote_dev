<?php
namespace docomo\Models\Features;

use docomo\Models\Questions\Question;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

class Feature extends \BaseArdentModel
{
    protected $table = 'feature';

    public static $rules = [
        'title' => ['required', 'max:60', 'deny_tag', 'deny_char'],
        'message' => ['allow_tag', 'max:10000', 'deny_char'],
        'from' => ['required', 'date', 'datetime_format:Y-m-d H:i'],
        'to' => ['required', 'date', 'after:"from_date"', 'datetime_format:Y-m-d H:i']
    ];

    public static $customMessages = [
        'title.required'            => W_FRM_REQUIRED_TITLE,
        'title.max'                 => W_FRM_MAX_TITLE,
        'title.deny_tag'            => W_FRM_DENY_TAG_TITLE,
        'title.deny_char'           => W_FRM_DENY_CHAR_TITLE,
        'message.allow_tag'         => W_FRM_ACCEPT_TAGS,
        'message.max'               => W_FRM_MAX_MULTI_MESSAGE,
        'message.deny_char'         => W_FRM_DENY_CHAR_MESSAGE,
        'from.required'             => W_FRM_REQUIRED_DATE_FROM,
        'from.date'                 => W_FRM_FORMAT_DATE_FROM,
        'from.datetime_format'      => W_FRM_LENGTH_DATE_FROM,
        'to.required'               => W_FRM_REQUIRED_DATE_TO,
        'to.date'                   => W_FRM_FORMAT_DATE_TO,
        'to.after'                  => W_FRM_COMPLEX_DATE_TO_AFTER_FROM,
        'to.datetime_format'        => W_FRM_LENGTH_DATE_TO,
    ];

    protected $fillable = [
        'title',
        'message',
        'from',
        'to'
    ];

    public $messages;

    public $questionIdList = null;

    public function __construct(array $attributes = array())
    {
        $this->is_view_list = true;
        parent::__construct($attributes);
    }

    public function scopeDetails($query)
    {

        return $query->select('id', 'title', 'message', 'thumbnail', 'is_feature_column', 'is_view_list', 'from', 'to', 'question_ids');
    }

    public function scopeVisible($query)
    {

        return $query->whereIsViewList(VISIBLE)->whereRaw('`from` < ?', [date('Y-m-d H:i')]);
    }

    public function scopeViewable($query)
    {

        return $query->visible()->at();
    }

    public function scopeWhereLikeQuestionId($query, $question)
    {
        return $query->whereRaw('`id` IN (SELECT `feature_id` FROM `feature_questions` WHERE `question_id` = ?)',
            [$question->id])->visible();
    }

    public function scopeAt($query) {


        return $query->whereRaw(' (? BETWEEN `from` AND `to`) ', [date('Y-m-d H:i')]);
    }

    public static function getList()
    {

        return Feature::selectRaw('SQL_CACHE `id`, `title`, `is_feature_column`, `is_view_list`, `from`, `to`')->orderBy('from', 'desc')->orderBy('id', 'desc')->get();
    }

    public static function getPastCount() {

        return Feature::selectRaw('SQL_CACHE count(*) as aggregate')->visible()->get()[0]['aggregate'];
    }

    public static function getListFront($limit, $date)
    {

        return Feature::selectRaw('SQL_CACHE `id`, `title`')
            ->whereIsViewList(VISIBLE)
            ->where('from', '<=', $date)
            ->orderBy('from', 'desc')->orderBy('id', 'desc')->paginate($limit);
    }

    public static function fillWithModify($id = null, $thumbnail = null)
    {

        if ($id === null)
        {
            $feature = new Feature;
            $feature->created_account_id = \Auth::user()->id;
        }
        else
        {
            $feature = Feature::find($id);
            $feature->thumbnail = $thumbnail;
        }
        $feature->fill(\Input::all());

        $feature->is_view_list = \Input::has('is_view_list');
        $feature->is_feature_column = \Input::has('is_feature_column');
        $feature->updated_account_id = \Auth::user()->id;

        $feature->from = $feature->getDatetimeControlValue('from');
        $feature->to = $feature->getDatetimeControlValue('to');

        $question_ids = \Input::get('question_ids');
        if (isset($question_ids) && !empty($question_ids)) {
            $feature->questionIdList = explode(',', $question_ids);
        }

        if (isset($thumbnail) && !empty($thumbnail)) {
            if (\Input::get('thumbnail_file_remove') != true) {
                $feature->thumbnail = $thumbnail;
            } else {
                $feature->thumbnail = '';
            }
        }
        if (\Input::hasFile('thumbnail_file'))
        {
            $uploaded_file = \Input::file('thumbnail_file');
            if ($uploaded_file->getSize() > MAX_UPLOAD_FILE_SIZE) {
                $feature->errors()->add('thumbnail.error_size', W_FRM_MAXSIZE_THUMBNAIL);
            }
            $extension = $uploaded_file->getClientOriginalExtension();
            if (!$feature->isAllowExtension($extension)) {
                $feature->errors()->add('thumbnail.error_extension', W_FRM_EXTENSION_THUMBNAIL);
            }
            if ($feature->errors()->count() === 0) {
                $feature->thumbnail = 'feature_' . (new \DateTime())->format('YmdHis') . '.' . $extension;
                $feature->thumbnail_original = $uploaded_file->getClientOriginalName();
                $uploaded_file->move(public_path() . PATH_TO_FEATURE_TEMP, $feature->thumbnail);
                $feature->thumbnail = PATH_TO_FEATURE_TEMP . $feature->thumbnail;
            } else {
                $feature->thumbnail = null;
            }
        }

        return $feature;
    }

    public static function saveList($featureId, $questionIdList)
    {
        \DB::delete('DELETE FROM feature_questions WHERE feature_id = ?', [$featureId]);
        if (is_null($questionIdList)) return;

        $unique = [];
        $errors = new MessageBag();
        for($forI = 0; $forI < count($questionIdList); $forI++) {
            if (array_search($questionIdList[$forI], $unique) !== FALSE) {
                continue;
            }
            $featureQuestion = new FeatureQuestion();
            $featureQuestion->feature_id = $featureId;
            $featureQuestion->question_id = $questionIdList[$forI];
            $featureQuestion->index = $forI;
            if (!$featureQuestion->save()) {
                $errors->merge($featureQuestion->errors());
            }
            $unique[] = $questionIdList[$forI];
        }

        return $errors;
    }

    public function confirmValidate()
    {
        $formData = \Input::all();
        $formData['from']   = $this->getDatetimeControlValue('from');
        $formData['to']     = $this->getDatetimeControlValue('to');

        $validator = Validator::make(
            $formData,
            Feature::$rules,
            Feature::$customMessages);

        $errors = $this->errors();
        if ($validator->fails())
        {
            $errors->merge($validator->messages());
        }

        if (!is_null($this->questionIdList)) {
            foreach ($this->questionIdList as $questionId) {
                if (!ctype_digit($questionId) || intval($questionId) > 99999999) {
                    $errors->add('question_ids.format', W_FRM_FORMAT_QUESTION_ID);
                    break;
                }
            }
        }

        $this->error_message = $errors;

        return ($errors->count() == 0);
    }

    public function questionList()
    {
        $this->getQuestionIdList();

        if (!isset($this->question_list))
        {
            $questions = [];
            if (isset($this->questionIdList) && count($this->questionIdList) > 0)
            {
                $questionList = Question::fieldList()
                    ->whereBase()
                    ->whereIn('id', $this->questionIdList)
                    ->orderBy(\DB::raw('FIELD(id, ' . implode(',', $this->questionIdList) . ')'))
                    ->get();
                foreach ($questionList as $question) {
                    if ($question->isViewableList()) {
                        $questions[] = $question;
                    }
                }
            }
            $this->question_list = $questions;
            unset($questions);
        }

        return $this->question_list;
    }

    public function getQuestionIdList()
    {
        if (is_null($this->questionIdList)) {
            $questions = FeatureQuestion::
                selectRaw('SQL_CACHE `question_id`')
                ->whereFeatureId($this->id)
                ->orderBy('index')
                ->get()->lists('question_id');
            $this->questionIdList = $questions;
        }

        return $this->questionIdList;
    }

    /**
     * [moveFile Move file from temp to thumbnail]
     * @return [type] [description]
     */
    public function saveFile()
    {
        if (!empty($this->thumbnail) && !stristr($this->thumbnail, PATH_TO_FEATURE_THUMBNAIL)) {
            $temp_path = $this->thumbnail;
            $this->thumbnail = str_replace(PATH_TO_FEATURE_TEMP, PATH_TO_FEATURE_THUMBNAIL, $this->thumbnail);
            \File::move(public_path() . $temp_path, public_path() . $this->thumbnail);
        }
    }

    public function cloneObject()
    {
        $feature = new Feature;

        if (isset($this->id))
        {
            $feature = Feature::find($this->id);
        }

        if (isset($this->title))
        {
            $feature->title = $this->title;
        }

        if (isset($this->thumbnail))
        {
            $feature->thumbnail = $this->thumbnail;
        }

        if (isset($this->message))
        {
            $feature->message = $this->message;
        }

        if (isset($this->from))
        {
            $feature->from = $this->from;
        }

        if (isset($this->to))
        {
            $feature->to = $this->to;
        }

        if (isset($this->is_view_list))
        {
            $feature->is_view_list = $this->is_view_list;
        }

        if (isset($this->is_feature_column))
        {
            $feature->is_feature_column = $this->is_feature_column;
        }

        if (isset($this->created_account_id))
        {
            $feature->created_account_id = $this->created_account_id;
        }

        if (isset($this->updated_account_id))
        {
            $feature->updated_account_id = $this->updated_account_id;
        }

        return $feature;
    }

    public static function getNewestFeature()
    {

        return Feature::selectRaw('SQL_CACHE `id`, `title`, `thumbnail`, `message`, `from`, `question_ids`, `is_feature_column`')->at()->orderBy('from', 'desc')->orderBy('id', 'desc')->first();
    }
}
