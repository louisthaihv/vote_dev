<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSummaryTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('daily_vote')) {
			Schema::create('daily_vote', function ($table) {
				$table->date('summary_date');
				$table->integer('total')->unsigned()->default(0);
				$table->integer('fp')->unsigned()->default(0);
				$table->integer('sp')->unsigned()->default(0);
				$table->primary('summary_date');
			});
		}

		if (!Schema::hasTable('daily_vote_ranking')) {
			Schema::create('daily_vote_ranking', function ($table) {
				$table->date('summary_date');
				$table->integer('question_id')->unsigned();
				$table->integer('total')->unsigned();
				$table->primary(['summary_date', 'question_id']);
			});
		}

		if (!Schema::hasTable('daily_profile')) {
			Schema::create('daily_profile', function ($table) {
				$table->date('summary_date');
				$table->integer('total_fp')->unsigned()->default(0);
				$table->integer('total_sp')->unsigned()->default(0);
				$table->integer('new_fp')->unsigned()->default(0);
				$table->integer('new_sp')->unsigned()->default(0);
				$table->primary('summary_date');
			});
		}

		if (!Schema::hasTable('summary_attribute')) {
			Schema::create('summary_attribute', function ($table) {
				$table->integer('attribute_id')->unsigned();
				$table->integer('attribute_value_id')->unsigned();
				$table->integer('registered')->unsigned()->default(0);
				$table->primary(['attribute_id', 'attribute_value_id']);
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
