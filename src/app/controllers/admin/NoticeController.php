<?php
namespace Admin;

use docomo\Models\Notices\Notice;

class NoticeController extends \BaseController
{
    
    public function __construct()
    {
    }
    
    /**
     * [access notice index page]
     *
     * @return [View] [admin.notice.index]
     */
    public function index()
    {
        \Session::forget(NOTICE);

        $notices = Notice::getList();

        return \View::make('admin.notice.index', compact('notices'));
    }
    
    /**
     * [access notice create page]
     *
     * @return [View] [admin.notice.create]
     */
    public function create()
    {
        $notice = \Session::pull(NOTICE);
        if (is_null($notice)) {
            $notice = new Notice;
        } else {
            $notice = unserialize($notice);
        }
        if (isset($notice->id)) {
            $notice = new Notice;
        }

        return \View::make('admin.notice.create', compact('notice'));
    }
    
    /**
     * [get data from form and passing to notice confirm page]
     *
     * @return [Redirect] [route to admin.notice.confirm]
     */
    public function store()
    {

        return $this->saveNotice('create');
    }
    
    /**
     * [access notice edit page with id]
     *
     * @param  [int] $id [id of notice]
     * @return [View]     [admin.notice.edit]
     */
    public function edit($id)
    {
        $notice = \Session::get(NOTICE);
        if (is_null($notice)) {
            $notice = Notice::details()->find($id);
        } else {
            $notice = unserialize($notice);
        }
        if ($notice->id != $id) {
            $notice = Notice::details()->find($id);
        }
        \Session::put(NOTICE, serialize($notice));

        return \View::make('admin.notice.edit', compact('notice'));
    }
    
    /**
     * [process updating notice by id]
     * 
     * @param  [int] $id [id of notice]
     * @return [Redirect]     [admin.notice.confirm]
     */
    public function update($id)
    {

        return $this->saveNotice('edit');
    }
    
    /**
     * [Access notice confirm page]
     *
     * @param  [int] $id [id of notice. If id == null => add notice]
     * @return [View]     [admin.notice.confirm]
     */
    public function confirm()
    {
        $notice = \Session::get(NOTICE);
        if (is_null($notice)) {
            $notice = new Notice;
        } else {
            $notice = unserialize($notice);
        }
        $notice = Notice::fillWithModify($notice->id);

        if ($notice->confirmValidate())
        {
            \Session::put(NOTICE, serialize($notice));

            return \View::make('admin.notice.confirm', compact('notice'));
        }
        else
        {
            $errors = $notice->messages;

            return \View::make((isset($notice->id)) ? 'admin.notice.edit' : 'admin.notice.create', compact('notice', 'errors'));
        }
    }
    
    /**
     * [Process deleting notice by id]
     * 
     * @param  [int] $id [id of notice.]
     * @return [Redirect]     [redirect to route admin.notice.index]
     */
    public function destroy($id)
    {
        Notice::destroy($id);
        
        return \Redirect::route('manage.notice.index');
    }

    private function saveNotice($mode)
    {
        $notice = \Session::get(NOTICE);
        if (is_null($notice)) {
            $notice = new Notice;
        } else {
            $notice = unserialize($notice);
        }

        unset(Notice::$rules['from']);
        unset(Notice::$rules['to']);

        $notice = $notice->cloneObject();

        if ($notice->save())
        {
            \Session::forget(NOTICE);

            return \Redirect::route('manage.notice.index');
        }
        else
        {
            $errors = $notice->errors();

            return \View::make('admin.notice.' . $mode, compact('notice', 'errors'));
        }
    }
}
