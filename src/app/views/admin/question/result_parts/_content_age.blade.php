<div class="tabBox">
    <p class="l-margin">
        {{ link_to_route('manage.question.csv', '年代別CSVダウンロード', [
                    $question->id,
                    '1',
                    '0'
        ] ) }}
        {{ link_to_route('manage.question.csv', '年代別CSVダウンロード(d版)', [
                    $question->id,
                    '1',
                    '1'
        ] ) }}
        {{ link_to_route('manage.question.csv', '年代別CSVダウンロード(i版)', [
                    $question->id,
                    '1',
                    '2'
        ] ) }}

    <div class="resultTable profileChoice">
        <table class="l-margint">
            @include('admin.question.result_parts._result_list', ['list' => $ageList, 'selected' => $selectedAge, 'targetId' => 'ageTitle'])
        </table>
        <!--/resultTable--></div>


    <p class="titPtn1 l-margint" id="ageTitle">{{ $selectedAge->attribute_value_name }}ランキング</p>
    @if ($question->isViewableAverage())
        <p class="avgData">平均：{{ showAverage($topSummaryHash, $selectedAge->id, $question) }}</p>
    @endif
    <p class="tright m-margint attrCount">投票数：<span>{{ number_format(array_sum($ageData->lists('cnt'))) }}</span>票</p>

    <div class="resultTable resultTableTarget">
        <table class="l-margint">
            @include('admin.question.result_parts._result_table', ['resultData' => $ageData])
        </table>
        <!--/resultTable--></div>
    <!--/tabBox--></div>
