<?php
class AttributesSeeder extends Seeder
{
    
    public function run()
    {
        DB::table('attributes')->truncate();
        
        $attributes = array(
            array(
                'attribute_name' => '年代別',
                'attribute_label' => '生年月日',
                'sort_order' => 20,
                'attribute_directory' => 'age',
                'priority' => 1
            ),
            array(
                'attribute_name' => '性別',
                'attribute_label' => '性別',
                'sort_order' => 10,
                'attribute_directory' => 'gender',
                'priority' => 1
            ),
            array(
                'attribute_name' => '地域別',
                'attribute_label' => '出身地',
                'sort_order' => 30,
                'attribute_directory' => 'pref',
                'priority' => 1
            )
        );
        
        DB::table("attributes")->insert($attributes);
    }
}
