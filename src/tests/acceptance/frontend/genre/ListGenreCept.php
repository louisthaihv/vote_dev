<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('check genre');
$I->amOnPage('/list/genre')
$arr_genre_parent=array(
                    'name'=>'日常生活',
                    'parent_is_visible' => 1,
                    );
$arr_genre_child = array(
                    1=>array(
                            'name'=>'学校生活、職場生活',
                            'child_is_visible' =>1,
                            'link' =>'#',
                            ),
                    2=>array(
                            'name'=>'ご当地ジョーシキ',
                            'child_is_visible' =>1,
                            'link' =>'#',
                            ),
                    3=>array(
                            'name'=>'知りたいみんなのアノ事情',
                            'child_is_visible' =>1,
                            'link' =>'#',
                            ),
                    4=>array(
                            'name'=>'わたしは○○派',
                            'child_is_visible' =>1,
                            'link' =>'#',
                            ),
                    5=>array(
                            'name'=>'恋愛にまつわるエトセトラ',
                            'child_is_visible' =>1,
                            'link' =>'#',
                            ),
                    6=>array(
                            'name'=>'ガマンの境界',
                            'child_is_visible' =>1,
                            'link' =>'#',
                            ),
                    7=>array(
                            'name'=>'直感二択！',
                            'child_is_visible' =>1,
                            'link' =>'#',
                            ),
                    );

$I->checkSPGenreList($arr_genre_parent, $arr_genre_child);