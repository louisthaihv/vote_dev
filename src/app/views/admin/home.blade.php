@extends('layouts.admin')

@section('content')

<h2>メインメニュー</h2>
<div class="menuArea clearfix">
	<div class="leftBlock">
		<h3>アンケート管理</h3>
		<ul>
			<li><a href="{{ route('manage.question.create') }}">アンケート新規投稿</a></li>
			<li><a href="{{ route('manage.question.index') }}">アンケート検索・修正・削除</a></li>
			<li><a href="{{ route('manage.question.index', ['status' => QUESTION_STATUS_REQUEST_APPROVE]) }}">承認申請中（{{ $counts[QUESTION_STATUS_REQUEST_APPROVE] }}）</a></li>
			<li><a href="{{ route('manage.question.index', ['status' => QUESTION_STATUS_REJECT]) }}">差戻し（{{ $counts[QUESTION_STATUS_REJECT] }}）</a></li>
@if(Auth::user()->havePermission('is_manage_column'))
            <li><a href="{{ route('manage.column.index') }}">コラム検索・編集</a></li>
@endif
@if(Auth::user()->havePermission('is_manage_summary'))
            <li><a href="{{ route('manage.question.dump') }}">全アンケートダウンロード</a></li>
@endif
		</ul>
		@if(Auth::user()->havePermission('is_manage_feature'))
		<h3>TOPページ特集枠管理</h3>
		<ul>
			<li><a href="{{ route('manage.feature.create') }}">特集枠データ作成</a></li>
			<li><a href="{{ route('manage.feature.index') }}">特集枠一覧・修正・削除</a></li>
		</ul>
		@endif
		@if(Auth::user()->havePermission('is_manage_attention'))
		<h3>TOPページ注目キーワード管理</h3>
		<ul>
			<li><a href="{{ route('manage.attention.create') }}">注目キーワードデータ作成</a></li>
			<li><a href="{{ route('manage.attention.index') }}">注目キーワード一覧・修正・削除</a></li>
		</ul>
		@endif
		@if(Auth::user()->havePermission('is_manage_pickup'))
			<h3>TOPページピックアップアンケート管理</h3>
			<ul>
				<li><a href="{{ route('manage.pickup.index') }}">ピックアップ一覧・修正・削除</a></li>
			</ul>
		@endif
	</div><!--/leftBlock-->
	<div class="rightBlock">
		@if(Auth::user()->havePermission('is_manage_genre'))
		<h3>ジャンル管理</h3>
		<ul>
			<li><a href="{{ route('manage.genre.index') }}">ジャンル一覧・登録・修正</a></li>
		</ul>
		@endif
		@if(Auth::user()->havePermission('is_manage_keyword'))
		<h3>キーワード管理</h3>
		<ul>
			<li><a href="{{ route('manage.keyword.index') }}">キーワード一覧・登録・修正</a></li>
		</ul>
		@endif

		@if(Auth::user()->havePermission('is_manage_account'))
		<h3>アカウント管理</h3>
		<ul>
			<li><a href="{{ route('manage.account.index') }}">アカウント一覧・登録・修正</a></li>
		</ul>
		@endif
		@if(Auth::user()->havePermission('is_manage_notice'))
		<h3>お知らせ管理</h3>
		<ul>
			<li><a href="{{ route('manage.notice.index') }}">お知らせ一覧・登録・修正・削除</a></li>
		</ul>
		@endif
        @if(true || Auth::user()->havePermission('is_manage_notice'))
            <h3>画像管理</h3>
            <ul>
                <li><a href="{{ route('manage.upload.create') }}">画像ファイルアップロード</a></li>
            </ul>
        @endif
		@if(Auth::user()->havePermission('is_manage_summary'))
		<h3>投票・プロフィール集計</h3>
		<ul>
			<li><a href="{{ route('manage.summary.daily_vote') }}">日別得票数</a></li>
            <li><a href="{{ route('manage.summary.vote_genre') }}">ジャンル別得票数</a></li>
            <li><a href="{{ route('manage.summary.vote_profile') }}">プロフィール別得票数</a></li>
			<li><a href="{{ route('manage.summary.vote_ranking') }}">得票数ランキング</a></li>
			<li><a href="{{ route('manage.summary.daily_profile') }}">日別プロフィール登録</a></li>
			<li><a href="{{ route('manage.summary.profile') }}">プロフィール登録累計</a></li>
		</ul>
		@endif
	</div><!--/rightBlock-->
</div><!--/menuArea-->

@stop