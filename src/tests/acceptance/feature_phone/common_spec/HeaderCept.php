<?php
$I = new AcceptanceTester\HeaderSteps($scenario);
$I->wantTo('check FP header');

// $date_1 = 	array(
// 				'from' => array(
// 								'date' => array('name' => 'from_date', 'value' => 'html/body/div[5]/table/tbody/tr[1]/td[5]/a'),
// 								'hour' => array('name' => 'from_hour', 'value' => '06'),
// 								),
// 				'to' => array(
// 								'date' => array('name' => 'to_date', 'value' => 'html/body/div[5]/table/tbody/tr[2]/td[2]/a'),
// 								'hour' => array('name' => 'to_hour', 'value' => '21'),
// 								),
// 				);
// $notice_1 = array(
// 				'id' => 1,
// 				'status' => 0,
// 				'title' => 'notice_1',
// 				'description' => 'description_1',
// 				'details' => 'details_1',
// 				'date' => $date_1,
// 				);

// $date_2 = array(
// 				'from' => array(
// 								'date' => array('name' => 'from_date', 'value' => 'html/body/div[5]/table/tbody/tr[1]/td[5]/a'),
// 								'hour' => array('name' => 'from_hour', 'value' => '06'),
// 								),
// 				'to' => array(
// 								'date' => array('name' => 'to_date', 'value' => 'html/body/div[5]/table/tbody/tr[4]/td[3]/a'),
// 								'hour' => array('name' => 'to_hour', 'value' => '21'),
// 								),
// 				);
// $notice_2 = array(
// 				'id' => 2,
// 				'status' => 1,
// 				'title' => 'notice_2',
// 				'description' => 'description_2',
// 				'details' => 'details_2',
// 				'date' => $date_2,
// 				);

// $date_3 = 	array(
// 				'from' => array(
// 								'date' => array('name' => 'from_date', 'value' => 'html/body/div[5]/table/tbody/tr[1]/td[5]/a'),
// 								'hour' => array('name' => 'from_hour', 'value' => '06'),
// 								),
// 				'to' => array(
// 								'date' => array('name' => 'to_date', 'value' => 'html/body/div[5]/table/tbody/tr[5]/td[6]/a'),
// 								'hour' => array('name' => 'to_hour', 'value' => '21'),
// 								),
// 				);
// $notice_3 = array(
// 				'id' => 1,
// 				'status' => 0,
// 				'title' => 'notice_3',
// 				'description' => 'description_3',
// 				'details' => 'details_3',
// 				'date' => $date_3,
// 				);

// //create notice
// // $notices = array($notice_1, $notice_2, $notice_3);
//create from admin
// $I->createNotice($notices);
//create 2 notice, [1] expire and not display, [2] will display
$notices = array();
$notices[1] = array(
					'id' => 1,
					'status' => 0,
					'title' => 'title_1',
                    'description' => 'description_1',
                    'details' => 'details_1',
                    'from' => '2015-01-05 00:00:00',
                    'to' => '2015-01-10 00:00:00',
                    );
$notices[2] = array(
					'id' => 2,
					'status' => 1,
					'title' => 'title_2',
                    'description' => 'description_2',
                    'details' => 'details_2',
                    'from' => '2015-01-10 00:00:00',
                    'to' => '2015-01-25 00:00:00',
                    );

//create from db
$I->createDbNotice($notices);

//check header
$logo = array('img' => '/common/front/images/fp/logo.gif', 'xpath' => 'html/body/div[1]/a/img');
$I->checkFPHeader($logo, $notices);

//check notice detail ~ SP_13
$I->checkFPNoticeDetail($notices[2]);