    <p>基本情報</p>
    <table class="formTable">
        <tr>
            <th>タイトル<span class="require">（必須）</span></th>
            <td>{{{ $question->title }}}</td>
        </tr>
        <tr>
            <th>説明（HTMLタグ可）<span class="require">（必須）</span></th>
            <td>{{{ $question->details }}}</td>
        </tr>
        <tr>
            <th>概要<span class="require">（必須）</span><br />（description用）</th>
            <td>{{{ $question->description }}}</td>
        </tr>
        <tr>
            <th>タイプ<span class="require">（必須）</span></th>
            <td>
                {{ Form::label(null, docomo\Models\Codes\SummaryType::$label[$question->summary_type]) }}
                @if ($question->isViewableAverage())
                <span>単位：<label class="xx-small">{{{ $question->average_unit }}}</label></span>
                @endif
            </td>
        </tr>
        <tr>
            <th>選択肢<span class="require">（必須）</span></th>
            <td>
                <table class="choices" id="choiceList">
                    <tr>
                        <th>&nbsp;</th>
                        <th width="480">選択肢</th>
                        <th class="summaryValue">集計用数値</th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php $row = 1;?>
                @foreach ($choices as $choice)
                    <tr><td><?php echo $row++; ?>)&nbsp;&nbsp;</td><td>{{{ $choice->choice_text }}}</td><td class="summaryValue">{{{ $choice->summary_value }}}</td><td></td></tr>
                @endforeach
                @if ($question->is_visible_other == VISIBLE)
                        <tr><td>その他)</td><td>{{{ $question->other_text }}}</td><td></td><td></td></tr>
                @endif
                </table>
            </td>
        </tr>
        <tr>
            <th>受付中の選択肢をシャッフルするか</th>
            <td>
                <label>{{ Form::checkbox('shuffle_choice', null, $question->shuffle_choice, ['class' => 'taniDisabled']) }} シャッフル</label>
            </td>
        </tr>
        <tr>
            <th>属性を限定して回答させる場合は選択</th>
            <td>
                {{{ (isset($question->attribute_value_id)) ? $question->getLimitedText() : '限定しない' }}}
            </td>
        </tr>
    </table>