@if ($features->count() > 0)
    @foreach ($features as $feature)
        <li><a href="{{ route('feature.detail', $feature->id) }}">{{ $feature->title }}</a></li>
    @endforeach
    @if ($features->getCurrentPage() === $features->getLastPage())
<li class="last" style="display:none;"></li>
    @endif
@elseif ($morePager == 1)
<li class="noResults">特集一覧はありません</li>
@endif
