<section class="l-margin">
    <div class="infoBlock">
        <h2 class="infoTit"><span>{{{ $notice->title }}}</span></h2>
        <div class="infoContents">
            {{ $notice->details }}
        </div><!--/infoContents-->
    </div><!--/infoBlock-->
</section>