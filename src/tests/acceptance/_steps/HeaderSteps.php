<?php
namespace AcceptanceTester;

class HeaderSteps extends \AcceptanceTester\CommonSteps
{
	public function checkFPHeader($logo, $notices)
	{
		$I = $this;
		$I->amOnPage('/');
		$I->wait(60);
		// A.Phần title
		// 	Title logo → Click vào thì chuyển đến VOTE TOP (USER_FP_01)
		// $this->checkHeaderLogo($logo);

		// B.Phần thông báo
		// 	お知らせ/Thông báo
		// 	※Hiển thị trong trường hợp đang được set trên màn hình quản lý
		// 	・Tiêu đề thông báo→Chuyển đến chi tiết thông báo (USER_FP_13)
		// 	・Khái quát thông báo thì hiển thị mark (!)
		$this->checkHeaderNotice($notices);
	}

	public function checkHeaderLogo($logo)
	{
		$I = $this;
		$I->checkImageCommon($logo['img']);
	}

	public function checkHeaderLogoClick($logo)
	{
		$I = $this;
		$I->click($logo['xpath']);
		$I->seeCurrentUrlEquals('/');
		$I->moveBack();
	}

	public function checkHeaderNotice($notices)
	{
		$I = $this;
		foreach ($notices as $notice) {
			if ($notice['status'] == 1) {
				$notice_link = '/notice/'.$notice['id'];
				$I->seeLink($notice['title'], $notice_link);
				$I->see($notice['description']);
			}
			else {
				$I->dontSeeLink($notice['title']);
			}
		}
	}

	public function checkFPNoticeDetail($notice)
	{
		$I = $this;
		$I->amOnPage('/');
		$I->wait(1);

		$I->click($notice['title']);
		$I->see($notice['title']);
		$I->see($notice['details']);
		$I->seeLink('みんなの声TOPへ','/');
	}

}