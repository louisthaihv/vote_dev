<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('add more attribute');

$I->amOnPage('/manage/attribute/add');
$I->amOnAction('Admin\SurveyController@create');
