<?php
namespace docomo\Models\Notices;

use Illuminate\Support\Facades\Validator;

class Notice extends \BaseArdentModel
{
    protected $table = 'notices';

    public static $rules = [
        'title' => ['required', 'max:60', 'deny_tag', 'deny_char'],
        'description' => ['required', 'max:60', 'deny_tag', 'deny_char'],
        'details' => ['required', 'allow_tag', 'max:10000', 'deny_char'],
        'from' => ['required', 'date', 'datetime_format:Y-m-d H:i'],
        'to' => ['required', 'date', 'after:"from_date"', 'datetime_format:Y-m-d H:i']
    ];

    public static $customMessages = [
        'title.required' => W_FRM_REQUIRED_TITLE,
        'title.max' => W_FRM_MAX_TITLE,
        'title.deny_tag' => W_FRM_DENY_TAG_TITLE,
        'title.deny_char' => W_FRM_DENY_CHAR_TITLE,
        'description.required' => W_FRM_REQUIRED_DESCRIPTION,
        'description.max' => W_FRM_MAX_SINGLE_DESCRIPTION,
        'description.deny_tag' => W_FRM_DENY_TAG_DESCRIPTION,
        'description.deny_char' => W_FRM_DENY_CHAR_DESCRIPTION,
        'details.required' => W_FRM_REQUIRED_DETAILS,
        'details.allow_tag' => W_FRM_ACCEPT_TAGS,
        'details.max' => W_FRM_MAX_MULTI_DETAILS,
        'details.deny_char' => W_FRM_DENY_CHAR_DETAILS,
        'from.required' => W_FRM_REQUIRED_DATE_FROM,
        'from.date' => W_FRM_FORMAT_DATE_FROM,
        'from.datetime_format' => W_FRM_LENGTH_DATE_FROM,
        'to.required' => W_FRM_REQUIRED_DATE_TO,
        'to.date' => W_FRM_FORMAT_DATE_TO,
        'to.after' => W_FRM_COMPLEX_DATE_TO_AFTER_FROM,
        'to.datetime_format' => W_FRM_LENGTH_DATE_TO,
    ];

    protected $fillable = [
        'title',
        'description',
        'details',
        'from',
        'to',
    ];

    public $messages;

    public function __construct(array $attributes = array())
    {
        $this->visible_device_i = true;
        $this->visible_device_d = true;

        parent::__construct($attributes);
    }

    public function scopeNowAndPast($query) {
        return $query->whereRaw('`from` <= ?', [date('Y-m-d H:i')]);
    }

    public static function scopeDetails($query)
    {

        return $query->selectRaw('SQL_CACHE `id`, `title`, `details`, `description`, `from`, `to`, `visible_device_i`, `visible_device_d`');
    }

    public static function getList()
    {

        return Notice::selectRaw('SQL_CACHE `id`, `title`, `from`, `to`, `visible_device_i`, `visible_device_d`')
            ->orderBy('from', 'desc')->orderBy('id', 'desc')->get();
    }

    public static function getNotices($device) {
        $now = date('Y-m-d H:i');

        return Notice::details()->where('visible_device_' . (($device === CHAR_D_MENU) ?  'd' : 'i'), '=', true)
            ->whereRaw('`from` <= ? AND ? < `to`', [$now, $now])->orderBy('from', 'desc')->orderBy('id', 'desc')->get();
    }

    public static function fillWithModify($id = null)
    {
        if ($id === null) {
            $notice = new Notice;
        }
        else
        {
            $notice = Notice::find($id);
        }
        $notice->fill(\Input::all());

        $notice->from   = $notice->getDatetimeControlValue('from');
        $notice->to     = $notice->getDatetimeControlValue('to');

        $notice->visible_device_i = \Input::has('visible_device_i');
        $notice->visible_device_d = \Input::has('visible_device_d');

        return $notice;
    }

    public function confirmValidate()
    {
        $inputs = \Input::all();
        $inputs['from'] = $this->getDatetimeControlValue('from');
        $inputs['to'] = $this->getDatetimeControlValue('to');
        $validator = Validator::make(
            $inputs,
            Notice::$rules,
            Notice::$customMessages);

        if ($validator->fails())
        {
            $this->messages = $validator->messages();
        }

        return !$validator->failed();
    }

    public function cloneObject()
    {
        $notice = new Notice;

        if (isset($this->id)) {
            $notice = Notice::find($this->id);
        }

        if (isset($this->title)) {
            $notice->title = $this->title;
        }

        if (isset($this->description)) {
            $notice->description = $this->description;
        }

        if (isset($this->details)) {
            $notice->details = $this->details;
        }

        if (isset($this->from)) {
            $notice->from = $this->from;
        }

        if (isset($this->to)) {
            $notice->to = $this->to;
        }

        if (isset($this->visible_device_i)) {
            $notice->visible_device_i = $this->visible_device_i;
        }

        if (isset($this->visible_device_d)) {
            $notice->visible_device_d = $this->visible_device_d;
        }

        return $notice;
    }
}
