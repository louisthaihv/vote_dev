@extends('layouts.admin_in')
@stop
@section('script')
<script type="text/javascript">
$(window).load(function(){

    $('#searchBtn').click(function() {
        if ($('#hide_show').prop("checked")) {
            $(location).attr('href', "{{ action('Admin\\AccountController@index', SHOW_ALL) }}");
        } else {
            $(location).attr('href', "{{ route('manage.account.index') }}");
        }
    });
});
</script>
@stop
@section('content')
<h2>アカウント一覧</h2>
<p class="m-margin">
<a href="{{ route('manage.account.create') }}" class="naviBtn">＋アカウント追加</a>
</p>
<form name="form1" method="post" action="">
    {{ Form::token() }}
    <label><input type="checkbox" id="hide_show" name="hide_show" value="" {{ ($show_all) ? 'checked="checked"' : '' }}/> クローズも見る</label>
    <a class="naviBtn" id="searchBtn" />再検索</a>
    <label>
    <table class="list">
        <tr>
            <th>ID</th><th>アカウント名</th><th>アカウントID</th><th>クローズ</th>
        </tr>
       @foreach($accounts as $account)
        <tr>
            <td>{{ $account->id }}</td>
            <td>{{ link_to_route('manage.account.edit', $account->account_name, $account->id) }}</td>
            <td>{{{ $account->login_id }}}</td>
            <td>{{ $account->is_enabled == DISABLED ? L_CLOSED_ACCOUNT : '' }}</td>
        </tr>
        @endforeach
    </table>
</form>
@stop