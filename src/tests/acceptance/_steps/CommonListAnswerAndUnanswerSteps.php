<?php
namespace AcceptanceTester;

class CommonListAnswerAndUnanswerSteps extends \AcceptanceTester\CommonSteps
{
    public function checkUnanswerList($question, $genre_parent, $genre_child)
    {
        $I = $this;
        $this->checkLinkToNewArrive();
        $this->checkCommonAnswerAndUnanswer($question, $genre_parent, $genre_child);
    }
    
    public function checkLinkToNewArrive()
    {
        $I = $this;
        $I->click('新着一覧へ');
        $I->seeCurrentUrlEquals('/list/new/result');
        $I->moveBack();
    }
    public function checkCommonAnswerAndUnanswer($question, $genre_parent, $genre_child)
    {
        $I = $this;
        foreach ($question as $key => $value) {
            if($value['status'] == 5){
                $I->dontSeeLink('[*]前へ');
                if($key < 11){
                    $I->click($value['title']);
                    $I->seeCurrentUrlEquals('/'.$genre_parent[1]['genre_parent_directory'].'/'.$genre_child[1]['genre_child_directory'].'/entry/'.$key.'');
                    $I->moveBack();
                }
                else{
                    $I->dontSeeLink($value['title']);
                }
                $I->click('[#]次へ');
                if($key < 11){
                    $I->dontSeeLink($value['title']);
                }
                else{
                    $I->click($value['title']);
                    $I->seeCurrentUrlEquals('/'.$genre_parent[1]['genre_parent_directory'].'/'.$genre_child[1]['genre_child_directory'].'/entry/'.$key.'');
                    $I->moveBack();
                }
                $I->dontSeeLink('[#]次へ');
                $I->click('[*]前へ');
            }
            else{
                $I->dontSeeLink($value['title']);
            }
        }
    }

}