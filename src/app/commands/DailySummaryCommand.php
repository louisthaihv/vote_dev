<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DailySummaryCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'summary:daily';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'daily summary.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		\DB::transaction(function () {
			\DB::delete('DELETE FROM daily_vote WHERE summary_date = ?',
			[$this->argument('target_date')]);
			\DB::insert('INSERT INTO daily_vote ' .
				' SELECT ?, COUNT(user_id), COUNT(CASE WHEN device = 2 THEN user_id ELSE NULL END), ' .
				' COUNT(CASE WHEN device = 1 THEN user_id ELSE NULL END) ' .
				' FROM answers WHERE ? <= answer_date AND answer_date < DATE_ADD(?, INTERVAL 1 DAY) ',
				[$this->argument('target_date'), $this->argument('target_date'), $this->argument('target_date')]);
		});

		\DB::transaction(function () {
			\DB::delete('DELETE FROM daily_vote_ranking WHERE summary_date = ?',
				[$this->argument('target_date')]);
			\DB::insert('INSERT INTO daily_vote_ranking (summary_date, question_id, total) ' .
				' SELECT ?, question_id, COUNT(user_id) ' .
				' FROM answers WHERE ? <= answer_date AND answer_date < DATE_ADD(?, INTERVAL 1 DAY) ' .
                ' AND EXISTS ( SELECT 1 FROM questions WHERE questions.id = answers.question_id) ' .
				' GROUP BY question_id',
				[$this->argument('target_date'), $this->argument('target_date'), $this->argument('target_date')]);
            \DB::update('UPDATE daily_vote_ranking SET genre_child_id = ' .
                ' (SELECT genre_child_id FROM questions WHERE questions.id = daily_vote_ranking.question_id) ' .
                ' WHERE summary_date = ?',
                [$this->argument('target_date')]);
            \DB::update('UPDATE daily_vote_ranking SET genre_parent_id = ' .
                ' (SELECT genre_parent_id FROM genre_children WHERE genre_children.id = daily_vote_ranking.genre_child_id) ' .
                ' WHERE summary_date = ?',
                [$this->argument('target_date')]);
		});

        \DB::transaction(function () {
            \DB::delete('DELETE FROM daily_vote_attribute WHERE summary_date = ?',
                [$this->argument('target_date')]);
            \DB::insert('INSERT INTO daily_vote_attribute ' .
                ' SELECT ?, 1, `age`, COUNT(user_id) ' .
                ' FROM answers WHERE ? <= answer_date AND answer_date < DATE_ADD(?, INTERVAL 1 DAY) ' .
                ' GROUP BY `age`',
                [$this->argument('target_date'), $this->argument('target_date'), $this->argument('target_date')]);
            \DB::insert('INSERT INTO daily_vote_attribute ' .
                ' SELECT ?, 2, `gender`, COUNT(user_id) ' .
                ' FROM answers WHERE ? <= answer_date AND answer_date < DATE_ADD(?, INTERVAL 1 DAY) ' .
                ' GROUP BY `gender`',
                [$this->argument('target_date'), $this->argument('target_date'), $this->argument('target_date')]);
            \DB::insert('INSERT INTO daily_vote_attribute ' .
                ' SELECT ?, 3, `local`, COUNT(user_id) ' .
                ' FROM answers WHERE ? <= answer_date AND answer_date < DATE_ADD(?, INTERVAL 1 DAY) ' .
                ' GROUP BY `local`',
                [$this->argument('target_date'), $this->argument('target_date'), $this->argument('target_date')]);
        });

		\DB::transaction(function () {
			\DB::delete('DELETE FROM daily_profile WHERE summary_date = ?',
				[$this->argument('target_date')]);
			\DB::insert('INSERT INTO daily_profile' .
				' SELECT ?, IFNULL(SUM(CASE LEFT(uid, 5) WHEN \'imode\' THEN 1 ELSE 0 END), 0), ' .
				' IFNULL(SUM(CASE LEFT(uid, 5) WHEN \'dmenu\' THEN 1 ELSE 0 END), 0), ' .
				' IFNULL(SUM(CASE WHEN LEFT(uid, 5) = \'imode\' AND attribute_first_date >= ? THEN 1 ELSE 0 END), 0), ' .
				' IFNULL(SUM(CASE WHEN LEFT(uid, 5) = \'dmenu\' AND attribute_first_date >= ? THEN 1 ELSE 0 END), 0) ' .
				' FROM users WHERE attribute_first_date < DATE_ADD(?, INTERVAL 1 DAY)',
				[
					$this->argument('target_date'), $this->argument('target_date'),
					$this->argument('target_date'), $this->argument('target_date')
				]);
		});

		\DB::transaction(function () {
			\DB::delete('DELETE FROM summary_attribute');

			\DB::insert('INSERT INTO summary_attribute (attribute_id, attribute_value_id) ' .
				' SELECT attribute_id, id FROM attribute_values');

			\DB::update('UPDATE summary_attribute SET registered = IFNULL((SELECT COUNT(user_id) FROM user_attributes '.
				' WHERE user_attributes.attribute_id = summary_attribute.attribute_id ' .
				' AND user_attributes.attribute_value_id = summary_attribute.attribute_value_id ' .
				' GROUP BY attribute_id, attribute_value_id), 0)');

            $attributeValues = \docomo\Models\Attributes\AttributeValue::getDataByCache(ATTR_BIRTHDAY);
            foreach ($attributeValues as &$values) {
                if ($values->attribute_value == 1) {
                    \DB::update(' UPDATE summary_attribute SET registered = IFNULL((SELECT count(id) FROM users ' .
                        ' WHERE users.birthday > DATE_ADD(CURDATE(), INTERVAL -20 YEAR)), 0) ' .
                        ' WHERE attribute_id = ? AND attribute_value_id =?', [ATTR_BIRTHDAY, $values->id]);
                } else if($values->attribute_value == 6) {
                    \DB::update(' UPDATE summary_attribute SET registered = IFNULL((SELECT count(id) FROM users ' .
                        ' WHERE users.birthday < DATE_ADD(DATE_ADD(CURDATE(), INTERVAL -60 YEAR), INTERVAL 1 DAY)), 0) ' .
                        ' WHERE attribute_id = ? AND attribute_value_id = ?', [ATTR_BIRTHDAY, $values->id]);
                } else {
                    \DB::update(' UPDATE summary_attribute SET registered = IFNULL((SELECT count(id) FROM users ' .
                        ' WHERE users.birthday > DATE_ADD(CURDATE(), INTERVAL -' . ($values->attribute_value + 1) * 10 . ' YEAR) '.
                        ' AND users.birthday <= DATE_ADD(CURDATE(), INTERVAL -'.($values->attribute_value+0)*10 . ' YEAR)), 0) ' .
                        ' WHERE attribute_id = ? AND attribute_value_id = ?', [ATTR_BIRTHDAY, $values->id]);
                }
            }

			\DB::insert('INSERT INTO summary_attribute SELECT 0, 0, COUNT(DISTINCT user_id) FROM user_attributes');
		});
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('target_date', InputArgument::REQUIRED, 'target date for summary.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
