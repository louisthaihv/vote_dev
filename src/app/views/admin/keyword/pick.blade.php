
<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta name="robots" content="noindex, nofollow" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="format-detection" content="telephone=no,address=no,email=no" />
<meta name="description" content="" />
<link href="{{ url('common/admin/css/reset.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ url('common/admin/css/common.css') }}" type="text/css" rel="stylesheet"/>
<link rel="stylesheet" href="{{ url('common/admin/css/jquery-ui-1.8.23.custom.css') }}" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>


<title>キーワード選択</title>

<script type="text/javascript">
    var max = {{ $max }};
$(window).load(function(){
    $('ul li a').click(function(){
        if(!window.opener || window.opener.closed){ // メインウィンドウの存在をチェック
            window.alert('メインウィンドウが見つからないため、終了します。'); // 存在しない場合は警告ダイアログを表
            window.close();
        }
        else if (max > 1){
            if(window.opener.$("#keywordList").text().trim()=='選択されていません'){
                window.opener.$("#keywordList").empty();
                window.opener.$("#keywordClear").show();
            }
            if (!window.opener.document.getElementById('keyword[' + $(this).attr('id') +']')) {
                window.opener.$("#keywordList")
                        .append('<li class="listK"><label for="keyword_name">' + $(this).text() + '</label><input type="hidden" name="keyword[' +$(this).attr('id') + ']" id="keyword[' +$(this).attr('id') + ']" value="' + $(this).attr('id') + '"/></li>');
            }
            if (window.opener.$(".listK", "#keywordList").length >= max) {
                window.close();
            }
        }
        else{
            window.opener.$("#keywordList").text($(this).text());
            window.opener.$("#keyword_name").val($(this).text());
            window.opener.$("#keywordId").val($(this).attr('id'));
            window.opener.$("#keywordClear").show();
            window.close();
        }
        return false;
    });
});
</script>
<style>
ul li{
    margin:10px 0px;
}
</style>
</head>
<body>
<div style="margin:20px;">
<p class="boldFont m-margin">■キーワードリスト</p>
<ul>
    @foreach($keywords as $keyword)
        <li>
        <a href="#" id="{{ $keyword->id }}"> {{ $keyword->keyword_name }}</a>
        </li>
    @endforeach
</ul>
</div>
</body>
</html>