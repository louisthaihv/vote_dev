@if (count($attentions[$tab]) > 0)
<div class="keywords">
    <h3>注目キーワード：</h3>
    <ul class="words">
    @if(count($attentions[$tab]) > 0)
        <?php $idx = 1; ?>
        @foreach($attentions[$tab] as $keyword_id => &$keyword_text)
            @if ($tab === PROCESSING)
<li><a id="top_vote_word{{ sprintf('%1$02d', $idx++) }}" href="{{ route('list.tag.entry', $keyword_id) }}">{{ $keyword_text }}</a></li>
            @elseif ($tab === COMPLETE)
<li><a id="top_result_word{{ sprintf('%1$02d', $idx++) }}" href="{{ route('list.tag.result', $keyword_id) }}">{{ $keyword_text }}</a></li>
            @endif
        @endforeach
    @endif
    </ul>
</div>
@endif