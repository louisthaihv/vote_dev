@extends('layouts.front_in')

@section('metaTag')
    <title>コラム一覧 | みんなの声</title>
    <meta name="description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！">
    <meta property="og:title" content="コラム一覧 | みんなの声" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
    <meta name="twitter:title" content="コラム一覧 | みんなの声" />
    <meta name="twitter:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
@stop

@section('script')
    @include('front._shared.js_load_more_pages', ['classNames'=>['column'], 'nums' => [NUM_FIRST_PAGE_10], 'pages' => [PAGER_SIZE_FRONT],'urls' => ['more.list.column']])
@endsection


@section('content')

    <section class="l-margin">
        <h2 class="columnListTit"><span>コラム一覧</span></h2>
        @include('front._shared._question_list', ['className' => 'column', 'displaySeeMore' => TRUE])
    </section>
    @include('layouts.front_elements._profileBn')

@endsection

