<?php
namespace docomo\Models\Forms\Account;

use docomo\Models\Forms\BaseForm;

class LoginForm extends BaseForm
{
    
    protected $rules = array('login_id' => 'required', 'login_password' => 'required');
    protected $messages = [
        'login_id.required' => W_FRM_REQUIRED_LOGIN_ID,
        'login_password.required' => W_FRM_REQUIRED_LOGIN_PASSWORD
    ];
}
