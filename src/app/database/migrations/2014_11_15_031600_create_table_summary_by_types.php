<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableSummaryByTypes extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('summary_by_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ranking_number');
            $table->integer('question_id');
            $table->integer('count');
            $table->integer('sort_order');
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('summary_by_types');
    }
}
