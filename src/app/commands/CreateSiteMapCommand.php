<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use docomo\Models\Questions\Question;

class CreateSiteMapCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'create:sitemap';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create sitemap.xml.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

        \File::put(public_path('sitemap.xml'), \View::make('sitemap.header', [
            'serverPath' => 'http://vote.smt.docomo.ne.jp',
            'setDate' => date_format(new DateTime, 'Y-m-d')
        ]));

		$questionCount = Question::whereBase()->whereIn('status', [QUESTION_STATUS_READY, QUESTION_STATUS_FINISH])->count('id');
		for ($forI = 0; $forI < $questionCount; $forI += 1000) {
            $questions = [];
			$questionList = Question::fieldList()
				->whereBaseViewable()->whereIn('status', [QUESTION_STATUS_READY, QUESTION_STATUS_FINISH])
				->orderBy('id', 'desc')->paginate(1000);
			foreach ($questionList as &$question) {
				$questions[] = ['path' => $question->getPath(), 'date' => substr($question->baseline_date, 0, 10)];
				unset($question);
			}

            file_put_contents (public_path('sitemap.xml'), \View::make('sitemap.link', [
                'serverPath' => 'http://vote.smt.docomo.ne.jp',
                'setDate' => date_format(new DateTime, 'Y-m-d'),
                'questions' => $questions
            ]), FILE_APPEND);
			unset($questionList);
		}
		unset($questionCount);

        // stop keyword
//        $keywords = \docomo\Models\Keywords\Keyword::select('id', 'keyword_name')->visible()->get();
        $keywords = [];
        $features = \docomo\Models\Features\Feature::select('id')
            ->visible()->get();
        $parents = \docomo\Models\Genres\ParentGenre::getDataByCache();
        $children = \docomo\Models\Genres\ChildGenre::getDataByCache();
        file_put_contents(public_path('sitemap.xml'), \View::make('sitemap.footer', [
			'serverPath' => 'http://vote.smt.docomo.ne.jp',
			'setDate' => date_format(new DateTime, 'Y-m-d'),
			'keywords' => $keywords,
			'features' => $features,
			'parents' => $parents,
			'children' => $children
		]), FILE_APPEND);
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
