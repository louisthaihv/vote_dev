<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableChoiceChoiceNum extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasColumn('choices', 'choice_num')){
            Schema::table('choices', function ($table) {
                $table->integer('choice_num')->after('question_id');
            });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        if (Schema::hasColumn('choices', 'choice_num')){
            Schema::table('choices', function ($table) {
                $table->dropColumn('choice_num');
            });
        }
	}

}
