<?php
namespace AcceptanceTester;

class FrontTopSteps extends \AcceptanceTester
{

	public function checkTop()
	{
		$I = $this;
		$I->amOnPage('');
		$this->checkCollectionFrame();
		$this->checkTopSearchKeyword();
		$this->checkSwitchTab();
		$this->checkNoticeKeyword();
		$this->checkSurveyAtribute();
		$this->checkSurveyApprove();
		$this->checkProfileSetting();
		$this->checkSurveyEvaluations();
		$this->checkColumnList();
		$this->checkSurveyByProfileAndAtribute();
		$this->checkSurveyResult();
		$this->checkList();
	}


	//(A)Collection Frame: Top + List
	public function checkCollectionFrame($features, $questions)
	{
		$I = $this;
		$I->amOnPage('/');
		$this->checkCollectionFrameTop($features);
		$this->checkCollectionFrameList($questions);
	}

	//Top: title + thumbnail + message
	public function checkCollectionFrameTop($features)
	{
		$I = $this;
		foreach ($features as $feature) {
			if ($feature['status'] == '1') {
				$I->see($feature['title']);
				//$I->see($collection['thumbnail']);
				$I->see($feature['message']);
			}
		}
		$link_total = sprintf('過去の特集を見る(%d)', count($features));
		$I->seeLink($link_total,'/feature');
	}

	public function checkCollectionFrameList($questions)
	{
		$I = $this;
		$I->checkListCommons($questions, 'html/body/div[1]/div/div/section[2]/div[2]/ul/li[%d]/a', count($questions));
	}


	//(B)Search Keyword
	//input text with placeholder + button search
	public function checkSearchKeyword()
	{
		$I = $this;
		// $I->see('placeholder text search');
		// $I->see('button search click');
	}


	//(C)Switch tab: Nếu xem lần đầu thì 「結果発表/Công bố kết quả」, nếu đang có vote rồi thì 「受付中/Đang tiếp nhận」
	public function checkSwitchTab()
	{
		$I = $this;
		$I->amOnPage('/');
		$I->click('');
		$I->wait(3);
		$I->checkTabEntry();
		$I->click('');
		$I->wait(3);
		$I->checkTabResult();

	}


	//(G)Về những cái mà 「設定判定/Xét setting」 trong attribute master được set
	//Hiển thị trong trường hợp thuộc tính profile chưa được set → Chuyển đến đăng ký, edit profile (USER_SP_11)
	public function checkProfileSetting()
	{
		$I = $this;
		$I->amOnPage('/');
		$I->seeElement('html/body/div[1]/div/div/p[2]/a/img');
		$I->click('html/body/div[1]/div/div/p[2]/a/img');
		$this->createProfileSetting();
		$I->amOnPage('/');
		$I->dontSeeElement('html/body/div[1]/div/div/p[2]/a/img');
	}

    public function createProfileSetting()
    {
        $I = $this->getModule('WebDriver');
        $I->amOnPage('/profile');
        $I->selectOption('sex', '');
        $I->selectOption('birthday_year', '1990');
        $I->selectOption('birthday_month', '6');
        $I->selectOption('birthday_day', '6');
        $I->selectOption('pref', '');
        $I->click('');
    }



	//(H)SURVEY_EVALUATIONs
	//Switch tab・ガッテン！/Hài lòng!  --- ・マジ！？/Có thật sao!? --- ・メモメモ/Memo thôi → Switch hiển thị sang nội dung của loại đã chọn list đánh giá
	//Danh sách đánh giá: Tiêu đề câu hỏi, Ngày kết thúc tiếp nhận
	//Những cái 「ガッテン！」, 「マジ！？」, 「メモメモ」 trong vòng 168 giờ hiển thị lần lượt 5 cái có số lượng được chọn lớn hơn
	//Trường hợp không có dữ liệu đánh giá thì không hiển thị
	public function checkSurveyEvaluations($questions)
	{
		$I = $this;
		$I->amOnPage('/');

		$I->click('html/body/div[1]/div/div/section[6]/ul/li[1]/a/img');
		$I->wait(3);
		// $question = array($questions[1]);
		// $I->checkListCommons($question, '', 1);
		$I->seeLink($questions[1]['title']);

		$I->click('html/body/div[1]/div/div/section[6]/ul/li[2]/a/img');
		$I->wait(3);
		// $question = array($questions[2]);
		// $I->checkListCommons($question, '', 1);
		$I->seeLink($questions[2]['title']);

		$I->click('html/body/div[1]/div/div/section[6]/ul/li[3]/a/img');
		$I->wait(3);
		// $question = array($questions[3]);
		// $I->checkListCommons($question, '', 1);
		$I->seeLink($questions[3]['title']);
	}


	//(I)Column List
	//Hiển thị trong trường hợp có question mà column đang được set ・コラムタイトル/Column title, ・Ngày giờ tạo column
	//Xem column trong quá khứ → Chuyển đến column list (USER_SP_19)
	public function checkColumnList($columns)
	{
		$I = $this;
		$this->checkListCommons($columns, 'html/body/div[1]/div/div/section[7]/div/ul/li[3]/a', 1);
		$link = array('title' => '過去のコラムを見る', 'url' => '/list/column');
		$this->checkLinkEndList($link);
	}

	//(D)NOTICE_KEYWORD
	//Hiển thị random một noticed keyword đang được set cho câu hỏi có số lần vote nhiều trong thời gian nhất định
	//Hiển thị tiếp các noticed keyword đã được set trên màn hình quản lý
	public function checkNoticeKeyword($keywords)
	{
		$I = $this;
		$I->amOnPage('/');
		
		$I->click('html/body/div[1]/div/div/section[4]/div[1]/ul/li[1]/a/span');
		$I->wait(3);
		$keyword_list = array($keyword[1]);
		foreach ($keyword_list as $keyword) {
			$I->seeLink($keyword);
		}

		$I->click('html/body/div[1]/div/div/section[4]/div[1]/ul/li[2]/a/span');
		$I->wait(3);
		$keyword_list = array($keyword[2]);
		foreach ($keyword_list as $keyword) {
			$I->seeLink($keyword);
		}
	}

	//(E)Survey hạn chế thuộc tính
	//Trường hợp có đăng ký 1 profile thì hiển thị survey hạn chế mà match với nội dung setting đã đăng ký
	//Trường hợp không đăng ký gì cả thì hiển thị random
	//name of atribute + title question
	public function checkSurveyAtribute($question)
	{
		$I = $this;
		$I->amOnPage('/');
		$I->see($question['name']);
		$I->seeLink($question['title']);
	}

	public function checkSurveyList($entry, $result)
	{
		$I = $this;
		$I->amOnPage('/');

		$I->click('html/body/div[1]/div/div/section[4]/div[1]/ul/li[1]/a/span');
		$I->wait(3);
		$this->checkSurveyEntrytList($entry);

		$I->click('html/body/div[1]/div/div/section[4]/div[1]/ul/li[2]/a/span');
		$I->wait(3);
		$this->checkSurveyResultList($result);
	}

	//(F)Danh sách survey đang tiếp nhận
	public function checkSurveyEntrytList($questions)
	{
		$I = $this;
		$I->amOnPage('/');
		//Pickup: Question title, Ngày bắt đầu tiếp nhận
		//Hiển thị tối đa 3 câu hỏi chưa trả lời mà đang trong kỳ hạn hiển thị
		$pickup = $questions;
		$xpath = 'html/body/div[1]/div/div/section[4]/div[2]/div[1]/div[1]/ul/li[1]/a';
		$this->checkListCommons($pickup, $xpath);

		//New arrival - Đang tiếp nhận: Question title, Ngày bắt đầu tiếp nhận
		//Hiển thị tối đa 3 câu hỏi chưa trả lời theo thứ tự giảm của ngày bắt đầu tiếp nhận
		$newarrivals = $questions;
		$xpath = 'html/body/div[1]/div/div/section[4]/div[2]/div[1]/div[2]/ul/li[%d]/a';
		$this->checkListCommons($newarrivals, $xpath);

		//Xem survey chưa trả lời: Chuyển đến danh sách chưa trả lời (USER_SP_15)
		$link_survey = array('title' => '未回答のアンケートを見る', 'url' => '/list/unanswered');
		$this->checkLinkEndList($link_survey);

		//Phần popularity mà ở trạng thái đang tiếp nhận: Question title, Ngày bắt đầu tiếp nhận
		//Trong những cái có sốt vote nhiều, hiển thị tối đa 3 cái chưa trả lời
		$popularities = $questions;
		$xpath = 'html/body/div[1]/div/div/section[4]/div[2]/div[1]/div[3]/ul/li[1]/a';
		$this->checkListCommons($popularities);

		//Xem survey popular mà đang tiếp nhận → Chuyển đến trang popularity (đang tiếp nhận) (USER_SP_17)
		//Trường hợp tất cả đều là đã trả lời, không có cái để hiển thị thì hiển thị message dưới đây
		//みんなの声をご利用いただき誠にありがとうございます。
		//ただいま回答できるアンケートはありません。
		//新しいアンケートの配信をお待ちください。
		$link_popular = array('title' => '未回答一覧', 'url' => '/list/popular/entry');
		$this->checkLinkEndList($link_popular);
	}


	//(J)NOTICE_KEYWORDs
	//Keyword: Hiển thị random keyword được set cho câu hỏi có vote nhiều nhất có số lần vote nhiều trong thời gian nhất định
	//Hiển thị tiếp các noticed keyword đã được set trên màn hình quản lý
	//--> GIONG PHAN D

	//(K)Survey theo mối quan tâm của từng thuộc tính
	//Hiển thị trong trường hợp birthday và gender được đăng ký ở profile
	//Hiển thị cái có số vote nhiều theo thuộc tính ứng với thông tin setting: tên của thuộc tính, Tên của Độ tuổi ＋ Giới tính
	//Tiêu đề câu hỏi → Chuyển đến kết quả vote (USER_SP_03)


	//(L)Danh sách survey công bố kết quả
	//Link của câu hỏi thì chuyển đến trang kết quả vote (USER_SP_03)
	public function checkSurveyResultList($questions)
	{
		$I = $this;
		$I->amOnPage('/');
		//Pickup: Question title, Ngày kết thúc tiếp nhận
		//Hiển thị tất cả những câu hỏi nằm trong kỳ hạn hiển thị
		$pickup = $questions;
		$xpath = 'html/body/div[1]/div/div/section[4]/div[2]/div[2]/div[2]/ul/li[1]/a';
		$this->checkListCommons($pickup, $xpath);

		// New arrival - Kết quả: Question title, Ngày kết thúc tiếp nhận
		// Hiển thị tối đa 3 câu hỏi chưa trả lời theo thứ tự giảm của ngày bắt đầu tiếp nhận
		$newarrivals = $questions;
		$xpath = '';
		$this->checkListCommons($newarrivals, $xpath);

		// 新着のアンケート結果を見る/Xem kết quả survey mới → Chuyển đến danh sách công bố kết quả (USER_SP_05)
		$link_survey = array('title' => '', 'url' => '');
		$this->checkLinkEndList($link_survey);

		// Kết quả của phần popularity: Question title, Ngày kết thúc tiếp nhận
		// Trong những cái đã kết thúc trong vòng 168 giờ,hiển thị tối đa 3 cái từ cái có số lượng được vote nhiều nhất
		$popularities = $questions;
		$xpath = 'html/body/div[1]/div/div/section[4]/div[2]/div[2]/div[4]/ul/li/a';
		$this->checkListCommons($popularities);

		// 人気のアンケート結果を見る/Xem kết quả survey có nhiều quan tâm →Chuyển đến trang popularity (kết quả) (USER_SP_17_1)
		$link_popular = array('title' => '', 'url' => '');
		$this->checkLinkEndList($link_popular);
	}

}