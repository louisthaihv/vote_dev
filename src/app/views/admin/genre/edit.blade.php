@extends('layouts.admin_in')
@section('javascript')
        $(window).load(function(){

            var validator = $('#editChildGenre').validate({
                errorClass : 'errors'
            });

            $('a#searchBtn').click(function(){
                if ($('#hide_show').prop("checked")) {
                    $(location).attr('href', "{{ route('manage.genre.edit', array($genre->id, SHOW_ALL)) }}");
                } else {
                    $(location).attr('href', "{{ route('manage.genre.edit', $genre->id) }}");
                }
            });

            $('#addChildGenreBtn').click(function() {
                return (confirm('{{ C_ADD_CHILD_GENRE }}'));
            });

            $('#editChildGenreBtn').click(function() {
                if(confirm('{{ C_UPDATE_CHILD_GENRE }}')){
                    if ($('.unique').hasClass('errors')) {
                        $('p.error').css('display','');
                    }
                    return true;
                }
                return false;
            });
        });
@stop
@section('content')
<h2>ジャンル編集</h2>
@include('admin.genre.parent_form', ['url' => 'manage/genre/' . $genre->id . '/edit', 'method' => 'put', 'confirmMessage' => C_UPDATE, 'genre' => $genre, 'button' => '大ジャンルを更新'])
@include('admin.genre.child_form', ['url' => 'manage/genre/' . $genre->id . '/edit', 'method' => 'put', 'genre' => $genre, 'add_child' => $add_child, 'children' => $children])
@stop