@extends('layouts.admin_in')
@section('content')
    <h2>お知らせ登録</h2>
    @include('admin.notice.form', ['notice' => $notice])
@stop