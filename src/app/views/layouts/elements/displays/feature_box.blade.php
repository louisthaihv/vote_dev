<section class="featureBox">
    @if(!empty($feature))
        <h2><span>{{{ $feature->title }}}</span></h2>
        @if (!empty($feature->thumbnail) || !empty($feature->message))
        <div class="wrap">
            <div class="featureBlock">
                @if(!empty($feature->thumbnail))
                    <div><img src="{{ $feature->thumbnail }}" class="featureImage"></div>
                @endif
                <div class="featureContents">
                    {{ $feature->message }}
                </div>
            </div>
        </div><!--wrap-->
        @endif
    @if (count($feature->questionList()) > 0)
        <?php
            $idx = null;
            if (isset($idHeader)) {
                $idx = 1;
            }
        ?>
        <div class="links">
            <ul>
                @foreach ($feature->questionList() as $question)
                    @if ($feature->is_feature_column && $question->has_column)
                        <li>
                            <a {{ (!is_null($idx)) ? 'id="' . $idHeader . sprintf('%1$02d', $idx++) . '"' : '' }} href="{{ $question->getColumnPath() }}">
                                <span class="text">{{ $question->getColumn()->column_title }}</span>
                                <span class="date">{{ jpDate($question->getColumn()->view_create_datetime, 'Y年m月d日 H:i') }}</span>
                            </a>
                        </li>
                    @else
                        @include('layouts.elements.displays.list_box', ['show_entry_tag' => true, 'linkId' => (!is_null($idx)) ? $idHeader . sprintf('%1$02d', $idx++) : null])
                    @endif
                @endforeach
            </ul>
        </div>
@endif
@endif
</section>
