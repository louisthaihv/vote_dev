<div class="tabBox">
    <p class="l-margin">
        {{ link_to_route('manage.question.csv', '総合CSVダウンロード', [
                    $question->id,
                    '0',
                    '0'
        ] ) }}
        {{ link_to_route('manage.question.csv', '総合CSVダウンロード(d版)', [
                    $question->id,
                    '0',
                    '1'
        ] ) }}
        {{ link_to_route('manage.question.csv', '総合CSVダウンロード(i版)', [
                    $question->id,
                    '0',
                    '2'
        ] ) }}
    </p>
    <p class="titPtn1 l-margint">ランキング</p>
    @if ($question->isViewableAverage())
        <p class="avgData">平均：{{ showTotalAverage($topSummaryHash, $question) }}</p>
    @endif
    <div class="resultTable">
        @include('admin.question.result_parts._result_table', ['resultData' => $totalData])
    </div>
</div>
