<?php
namespace docomo\Models\Questions;
use Illuminate\Support\Facades\Log;

class Column extends \BaseArdentModel
{
    protected $primaryKey = 'question_id';

    /*
     * Model relationships
     */
    function question() {
        return $this->belongsTo('docomo\Models\Questions\Question');
    }

    /*
     * Acceptable params from input
     */
    protected $fillable = [
        'column_title',
        'column_detail',
        'description'
    ];
    
    /*
     * Validation rules
     */
    public static $rules = [
        'column_title'          => ['required', 'max:60', 'deny_tag', 'deny_char'],
        'column_detail'         => ['required', 'allow_tag', 'max:10000', 'deny_char'],
        'description'           => ['required', 'max:200', 'deny_tag', 'deny_char'],
        'view_create_datetime'   => ['required', 'date', 'datetime_format:Y-m-d H:i' ]
    ];

    /*
     * Custom validation messages
     */
    public static $customMessages = array(
        'column_title.required' => W_FRM_REQUIRED_COLUMN_TITLE,
        'column_title.deny_tag' => W_FRM_DENY_TAG_COLUMN_TITLE,
        'column_title.deny_char' => W_FRM_DENY_CHAR_COLUMN_TITLE,
        'column_title.max' => W_FRM_MAX_COLUMN_TITLE,
        'column_detail.required' => W_FRM_REQUIRED_COLUMN_DETAILS,
        'column_detail.allow_tag' => W_FRM_ACCEPT_TAGS,
        'column_detail.max' => W_FRM_MAX_COLUMN_DETAILS,
        'column_detail.deny_char' => W_FRM_DENY_CHAR_COLUMN_DETAILS,
        'description.required' => W_FRM_REQUIRED_COLUMN_DESCRIPTION,
        'description.max' => W_FRM_MAX_COLUMN_DESCRIPTION,
        'description.deny_tag' => W_FRM_DENY_TAG_COLUMN_DESCRIPTION,
        'description.deny_char' => W_FRM_DENY_CHAR_COLUMN_DESCRIPTION,
        'view_create_datetime.required' => W_FRM_REQUIRED_VIEW_DATETIME,
        'view_create_datetime.date' => W_FRM_FORMAT_VIEW_DATETIME,
        'view_create_datetime.datetime_format' => W_FRM_LENGTH_VIEW_DATETIME
    );

    public function __construct(array $attributes = array())
    {
        $this->view_create_datetime = (new \DateTime)->format('Ymd 00:00');
        $this->is_visible = false;
        parent::__construct($attributes);
    }


    public function scopeVisible($query)
    {

        return $query->whereRaw('`is_visible` = 1 AND `view_create_datetime` <= NOW()');
    }

    public function isEditable()
    {

        return ($this->is_visible !== VISIBLE) || \Auth::user()->havePermission('is_manage_agree');
    }

    public static function fillWithModify($id)
    {
        $column = Column::find($id);
        if (!$column)
        {
            $column = new Column;
            $column->question_id = $id;
            $column->created_account_id = \Auth::user()->id;
        } else {
            if ($column->is_visible === VISIBLE && !\Auth::user()->havePermission('is_manage_agree')) {
                return $column;
            }
        }

        $column->fill(\Input::all());

        // 承認権限がある場合のみ、表示チェックの入力を判定する
        if (\Auth::user()->havePermission('is_manage_agree')) {
            $column->is_visible = (\Input::has('is_visible')) ? VISIBLE : HIDDEN;
        }

        $column->updated_account_id = \Auth::user()->id;

        $column->view_create_datetime = $column->getDatetimeControlValue('view_create_datetime');

        return $column;
    }

    public static function hasColumn($question)
    {
        if (!$question) {
            return false;
        }
        $column = Column::where('question_id', '=', $question->id)->first();
 
        return isset($column);
    }
}
