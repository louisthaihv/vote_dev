@extends('layouts.front_in')

@section('metaTag')
    <title>マイページ | みんなの声</title>
    <meta name="description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！">
    <meta property="og:title" content="マイページ | みんなの声" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
    <meta name="twitter:title" content="マイページ | みんなの声" />
    <meta name="twitter:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
@stop

@section('script')
    @include('front._shared.js_load_more_pages', ['classNames'=>['myPageFinish', 'myPageReady'], 'nums' => [NUM_FIRST_PAGE_10, PAGER_SIZE_FRONT], 'pages' => [PAGER_SIZE_FRONT, PAGER_SIZE_FRONT], 'status' => ['result', 'entry']])
@endsection

@section('content')


    <section class="l-margin">
        <h2 class="mypageListTit"><span>{{ L_MY_PAGE }}</span></h2>
        <p class="totalCount lrMargin" id="totalCount">総投票数：{{ number_format(((isset($question_finish_count) ? $question_finish_count : 0) + (isset($question_readies) ? $question_readies->count() : 0))) }}件</p>
        <h3 class="listSectionTit"><span>{{ L_QUESTION_ANSWERED_FINISH }}</span></h3>

        @include('front._shared._question_list', ['className' => 'myPageFinish', 'displaySeeMore' => TRUE])

        @include('front._shared._link_to_list', ['text' => 'すべてのアンケート結果を見る', 'route' => 'list.result'])

        <h3 class="listSectionTit"><span>{{ L_QUESTION_ANSWERED_READY }}</span></h3>
        <div class="links">
            <ul>
                @if (isset($question_readies))
                    @foreach ($question_readies as $question)
                        @include('layouts.elements.displays.list_box', ['hide_answer' => true])
                    @endforeach
                @endif
            </ul>
        </div><!--/lrMargin-->

        @include('front._shared._link_to_list', ['text' => '未回答のアンケートを見る', 'route' => 'list.unanswered'])

        <p class="tcenter l-margin"><a href="{{ route('profile.edit') }}" class="btnType02">{{ L_BTN_PROFILE }}</a></p>
    </section>

@endsection
