<?php
namespace AcceptanceTester;

class GenreSteps extends \AcceptanceTester\CommonSteps
{
	public function checkFPGenreList($genres)
	{
		$I = $this;
		$I->amOnPage('/list/genre');
		$I->wait(3);
		foreach ($genres as $genre) {
			$this->checkFPGenreListCommon($genre);
		}
	}

	public function checkFPGenreListCommon($genre)
	{
		$I = $this;
		if ($genre['status'] ==1) {
			$I->see($genre['name']);
		}
		else {
			$I->dontSee($genre['name']);
		}
		$this->checkFPChildGenreList($genre['status'], $genre['childs']);
	}

	public function checkFPChildGenreList($parent_status, $child_genres)
	{
		$I = $this;
		foreach ($child_genres as $child_genre) {
			if ($parent_status == 1 && $child_genre['status'] == 1) {
				$I->seeLink($child_genre['name']);
			}
			else {
				$I->dontSeeLink($child_genre['name']);
			}
		}
	}

	public function checkFPListSurveyByGenre($link,$survies)
	{
		$I = $this;
		$I->wait(3);
		$I->checkLinkSwitch($link);
		$I->checkListSurveyByGenreCommon($survies);
	}


	public function checkLinkSwitch($link)
	{
		$I = $this;
		$I->seeLink($link['title'], $link['url']);
	}


	public function checkListSurveyByGenreCommon($survies)
	{
		$I = $this;
		//check page 1
		$current_page = 1;
		$I->checkFPListSurveyCommon($survies, $current_page, 1);
		$I->wait(1);
		//check page 2
		$next_page = sprintf('[%d]次へ>', $current_page++);
		//$I->click($next_page);
		$I->click('[#]次へ>');
		$I->wait(3);
		$I->checkFPListSurveyCommon($survies, $current_page, 1);

		//check page 3
		$next_page = sprintf('[%d]次へ>', $current_page++);
		//$I->click($next_page);
		$I->click('[#]次へ>');
		$I->wait(3);
		$I->checkFPListSurveyCommon($survies, $current_page, 1);

		//check page 2
		$prev_page = sprintf('<[%d]前へ', $current_page--);
		//$I->click($prev_page);
		$I->click('<[*]前へ');
		$I->wait(3);
		$I->checkFPListSurveyCommon($survies, $current_page, 1);

		//check page 1
		$prev_page = sprintf('<[%d]前へ', $current_page--);
		//$I->click($prev_page);
		$I->click('<[*]前へ');
		$I->wait(3);
		$I->checkFPListSurveyCommon($survies, $current_page, 1);
	}
}