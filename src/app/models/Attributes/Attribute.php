<?php

namespace docomo\Models\Attributes;

use Illuminate\Support\Facades\Log;
use DateTime;

class Attribute extends \BaseArdentModel
{
    public static $cacheName = 'Attributes';
    public static $attributeData = null;
    /*
     * Model relationships
     */

    function attributeValues()
    {
        return $this->hasMany('docomo\Models\Attributes\AttributeValue');
    }

    /*
     * Scope
     */
    function scopeOrderBySort($query)
    {

        return $query->orderBy('sort_order');
    }

    /*
     * Methods
     */

    public static function getListForDDL()
    {

        return Attribute::orderBySort()->get(['id', 'attribute_name']);
    }

    public static function getListForDDLWithChildren()
    {

        $attributes = Attribute::getListForDDL();
        $tables = [];
        foreach ($attributes as $attribute)
        {
            $tables += [$attribute->attribute_name => AttributeValue::getListForDDL($attribute->id)];
        }
        unset($attributes);

        return $tables;
    }

    public static function getDataByCache()
    {
        if (is_null(Attribute::$attributeData)) {
            $updateTime = Attribute::checkCache(Attribute::$cacheName);
            Attribute::$attributeData = \Cache::get(Attribute::$cacheName, function () use ($updateTime) {
                $attributes = Attribute::select('id', 'attribute_name', 'attribute_label', 'sort_order', 'attribute_directory', 'priority')->get();
                \Cache::put(Attribute::$cacheName, $attributes, DEFAULT_CACHE_TIME);
                Attribute::setCacheTime(Attribute::$cacheName, $updateTime);

                return $attributes;
            });
        }

        return Attribute::$attributeData;
    }

    public static function getPriorities()
    {
        $attributes = Attribute::getDataByCache();
        $priorities = [];
        foreach ($attributes as $attribute)
        {

            if ($attribute->priority > 0)
            {
                $priorities[] = $attribute;
            }
        }

        return $priorities;
    }

    public function getDefaultValueId()
    {

        return $this->default_value->id;
    }

    public function getDefaultValue() {
        if (!isset($this->default_value)) {
            $values = AttributeValue::getDataByCache($this->id);
            foreach ($values as $val) {
                if ($val->is_default) {
                    $this->default_value = $val;
                }
            }
        }

        return $this->default_value;
    }
}
