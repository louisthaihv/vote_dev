@if ($question->isViewableList(TYPE_I_MODE, $currentUser))
<div style="margin:0px 0px 7px 0px;">
<a href="{{ $question->getPath() }}?uid=NULLGWDOCOMO">
{{ (!isset($hide_answer) && ($question->isAnswered($currentUser->id))) ? HTML::image("common/front/images/fp/end.gif", "回答済み",["width" => "15%"]) : '' }}
{{ $question->title }}
</a>
</div>
@endif