<div style="text-align:center;">&#xE6F6;<a href="{{ route('list.new.entry') }}?uid=NULLGWDOCOMO">受付中はこちら</a></div>

@include('layouts.front_elements._space')

<div style="background-color:#fff6ab;padding:5px;text-align:left;"><font color="#f39800">▼</font><font color="#3e3a39"> 結果発表 </font></div>

@include('layouts.front_elements._space')

<div style="margin:0px 10px 0px 10px;">
    @foreach($questions as $question)
         @include('layouts.front_elements._list_question', ['$question'=>$question])
    @endforeach
</div>

{{ $questions->links('layouts.front_elements.paginate_fp') }}
@include('layouts.front_elements._space')