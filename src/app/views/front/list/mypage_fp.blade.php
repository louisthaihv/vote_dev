@extends('layouts.front_fp')

@section('content')

    @include('layouts.front_elements._space')
    <div>{{ HTML::image('common/front/images/fp/tit3.gif', 'ﾏｲﾍﾟｰｼﾞ', array('width' => '100%')) }}</div>
    @include('layouts.front_elements._space')

    <div style="text-align:center;">総投票数：{{ ((isset($question_finishes) ? $question_finishes->getTotal() : 0) + (isset($question_readies) ? $question_readies->count() : 0)) }}件</div>
    @include('layouts.front_elements._space')
<div style="background-color:#fff6ab;padding:5px;text-align:left;"><font color="#f39800">▼</font><font color="#3e3a39"> 自分が投票した結果 </font></div>

    @include('layouts.front_elements._space')

@if (isset($question_finishes))
    <div style="margin:0px 10px 0px 10px;">
        @foreach($question_finishes as $question_finish)
            @include('layouts.front_elements._list_question',['question' => $question_finish, 'hide_answer' => true])
        @endforeach
    </div>
    {{ $question_finishes->links('layouts.front_elements.paginate_fp') }}
@endif
    
    @include('layouts.front_elements._gotop_fp')

<div style="background-color:#fff6ab;padding:5px;text-align:left;"><font color="#f39800">▼</font><font color="#3e3a39"> 自分が投票した受付中 </font></div>
    @include('layouts.front_elements._space')

    <div style="margin:0px 10px 0px 10px;">
@if (isset($question_readies))
    @foreach($question_readies as $question_ready)
        @include('layouts.front_elements._list_question',['question' => $question_ready, 'hide_answer' => true, 'currentUser' => null])
    @endforeach
@endif
    </div>

    @include('layouts.front_elements._space')
    @include('front._shared._link_to_list_sp', ['route' => 'list.unanswered', 'text' => '未回答ｱﾝｹｰﾄへ'])
    @include('layouts.front_elements._gotop_fp')

@endsection
