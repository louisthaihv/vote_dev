<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class UpdateStatusCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'status:update';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = '設問のステータスを日時などから変更します。';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		\docomo\Models\Questions\Question::$rules = [];
		$this->info('Starting update status...');
		$questions = \docomo\Models\Questions\Question::select('id', 'status', 'vote_date_from', 'vote_date_to')->openTarget()->get();
		$this->info('Open Target record is ' . $questions->count());
		$this->updateStatus($questions);
		$questions = \docomo\Models\Questions\Question::select('id', 'status', 'vote_date_from', 'vote_date_to')->finishTarget()->get();
		$this->info('Close Target record is ' . $questions->count());
		$this->updateStatus($questions);
		$this->info('Finished!');
		// TODO Add Error Handling
		//$this->info('TODO : Add Errors');
	}

	private function updateStatus($questions){
		foreach ($questions as &$question)
		{
			$question->status = $question->getNowStatus();
			$question->setBaselineDate();
			unset($question->vote_date_from);
			unset($question->vote_date_to);
			$question->save();
			unset($question);
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
