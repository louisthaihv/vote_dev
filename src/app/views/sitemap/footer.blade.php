    <url>
        <loc>{{ $serverPath }}/list/entry</loc>
        <lastmod>{{ $setDate }}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc>{{ $serverPath }}/list/result</loc>
        <lastmod>{{ $setDate }}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc>{{ $serverPath }}/list/unanswered</loc>
        <lastmod>{{ $setDate }}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc>{{ $serverPath }}/list/new/entry</loc>
        <lastmod>{{ $setDate }}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc>{{ $serverPath }}/list/new/result</loc>
        <lastmod>{{ $setDate }}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc>{{ $serverPath }}/list/popular/entry</loc>
        <lastmod>{{ $setDate }}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc>{{ $serverPath }}/list/popular/result</loc>
        <lastmod>{{ $setDate }}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
@foreach ($parents as &$parent)
    @if (array_key_exists($parent->id, $children))
        @foreach ($children[$parent->id] as &$child)
    <url>
        <loc>{{ $serverPath }}/list/genre/{{ $parent->genre_parent_directory }}/{{ $child->genre_child_directory }}/entry</loc>
        <lastmod>{{ $setDate }}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc>{{ $serverPath }}/list/genre/{{ $parent->genre_parent_directory }}/{{ $child->genre_child_directory }}/result</loc>
        <lastmod>{{ $setDate }}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
        @endforeach
    @endif
@endforeach
@foreach ($keywords as &$keyword)
    <url>
        <loc>{{ $serverPath }}/list/tag/{{ $keyword->id }}/entry</loc>
        <lastmod>{{ $setDate }}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc>{{ $serverPath }}/list/tag/{{ $keyword->id }}/result</loc>
        <lastmod>{{ $setDate }}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
@endforeach
    <url>
        <loc>{{ $serverPath }}/list/feature</loc>
        <lastmod>{{ $setDate }}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
@foreach ($features as &$feature)
    <url>
        <loc>{{ $serverPath }}/feature/{{ $feature->id }}</loc>
        <lastmod>{{ $setDate }}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
@endforeach
    <url>
        <loc>{{ $serverPath }}/list/column</loc>
        <lastmod>{{ $setDate }}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc>{{ $serverPath }}/profile</loc>
        <lastmod>{{ $setDate }}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc>{{ $serverPath }}/help</loc>
        <lastmod>{{ $setDate }}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc>{{ $serverPath }}/mypage</loc>
        <lastmod>{{ $setDate }}</lastmod>
        <changefreq>never</changefreq>
        <priority>0.5</priority>
    </url>
</urlset>