/*
 * common.js
 *
 */


$(window).load(function(){
    
    $("img.jsResize").each(function(){
        var imgsize = $(this).width() / 2;
        $(this).css('width',imgsize);
    });
    
    $('#headerMenuBtn').click(function(){
        $('#headerMenu').toggle();
        return false;
    });
    $('#headerMenuCloseBtn').click(function(){
        $('#headerMenu').hide();
        return false;
    });

    var topBtn = $('#pageTop');
    topBtn.hide();
    
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });

});

(function($){
    $.fn.anchorAction = function(options){
        var defaults = {
            'targetClass' : 'tit-scroll',
            'headerHeight'    : 0,
            'speed' : 'slow'
        };
        var setting = $.extend(defaults, options);
        this.each(function(index){
            $(this).click(function(){
                var i = index;
                var p = $("."+setting.targetClass).eq(i).offset().top - setting.headerHeight;
                if(p < 1){
                    p = 1;
                }
                $('html,body').animate({ scrollTop: p }, setting.speed);
                return false;
            });
        });
        return this;
    };
})(jQuery);

(function($){
    $.fn.gototopAction = function(options){
        var defaults = {
            'speed' : 'slow'
        };
        var setting = $.extend(defaults, options);
        this.each(function(){
            $(this).click(function(){
                $('html,body').animate({ scrollTop: 1 }, setting.speed);
                return false;
            });
        });
        return this;
    };
    
    
})(jQuery);


(function($) { 
    $.fn.sbmOrientationChange = function(func) {
        var s_width = screen.width;
        if(smTypeInfo() == 'iPhone' || smTypeInfo() == 'iPad'){
            $(this).bind('orientationchange',function(){
                func();
            });
        }else{
            $(this).bind('resize',function(){
                c_width = screen.width;
                if(s_width != c_width){
                    func();
                    s_width = c_width;
                }
            });
        }
        return this;
   };
 })(jQuery);

function screenSizeCheck(){
        var screenType = true;
        s_width=screen.width;
        if((smTypeInfo() == 'iPhone' || smTypeInfo() == 'iPad') && window.orientation != 0){
            s_width = screen.height;
        }
        
        //typeD
        if(window.devicePixelRatio == 1 && ((s_width >= 321 && s_width <= 480 && window.orientation == 0) || (s_width >= 481 && s_width <= 900 && window.orientation != 0))){
            screenType = false;
        }

        return screenType;
}

function smTypeInfo(){
    var userAgent =navigator.userAgent;
    var accessDevice = "";
    if (userAgent.indexOf('iPhone') > -1) {
        accessDevice = 'iPhone';
    }else if(userAgent.indexOf('iPad') > -1){
        accessDevice = 'iPad';
    }else if(userAgent.indexOf('Android') > -1){
        accessDevice = 'Android';
    }else{
        accessDevice = userAgent;
    }
    return accessDevice;
}