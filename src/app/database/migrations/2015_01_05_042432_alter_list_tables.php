<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterListTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasColumn('summary_by_types', 'id'))
		{
			Schema::dropIfExists('summary_by_types');

			Schema::create('summary_by_types', function (Blueprint $table) {
				$table->integer('type')->unsigned();
				$table->integer('ranking_number');
				$table->integer('question_id');
				$table->integer('count')->default(0);
				$table->primary(['type', 'ranking_number']);
			});
		}

		if (Schema::hasColumn('answers', 'id') || !Schema::hasTable('answers'))
		{
			Schema::dropIfExists('answers');

			Schema::create('answers', function (Blueprint $table) {
				$table->integer('question_id');
				$table->integer('user_id');
				$table->integer('choice_id');
				$table->integer('age')->default(ATTRIBUTE_VALUE_NOT_SET);
				$table->integer('gender')->default(ATTRIBUTE_VALUE_NOT_SET);
				$table->integer('local')->default(ATTRIBUTE_VALUE_NOT_SET);
				$table->timestamp('answer_date')->default(DB::raw('CURRENT_TIMESTAMP'));
				$table->primary(['question_id', 'user_id', 'choice_id']);
			});
		}

		if (!Schema::hasColumn('questions', 'vote_count'))
		{
			Schema::table('questions', function ($table) {
				$table->integer('vote_count')->default(0)->after('has_column');

			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
