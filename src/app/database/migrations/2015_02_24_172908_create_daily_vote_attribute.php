<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyVoteAttribute extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('daily_vote_attribute')) {
            Schema::create('daily_vote_attribute', function ($table) {
                $table->date('summary_date');
                $table->integer('attribute_id')->unsigned();
                $table->integer('attribute_value_id')->unsigned();
                $table->integer('total')->unsigned()->default(0);
            });
            \DB::statement('ALTER TABLE `daily_vote_attribute` ' .
                'ADD PRIMARY KEY (`summary_date`, `attribute_id`, `attribute_value_id`)');
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
