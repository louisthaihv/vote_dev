<?php

namespace docomo\Models\Users;

use docomo\Models\Answers\Answer;
use docomo\Models\Codes\UserAgent;
use docomo\Models\Questions\Question;
use docomo\Models\Ratings\VoteRating;
use docomo\Models\Ratings\VoteRatingTotal;
use Illuminate\Support\Facades\Log;
use docomo\Models\Attributes\Attribute;
use docomo\Models\Attributes\AttributeValue;
use docomo\Models\Users\UserAttribute;
use DateTime;
use Illuminate\Support\Facades\Cookie;

class User extends \BaseArdentModel
{
    public $attrList = [];

    private $age = 0;

    private static $uidSeed = 'aefjsofwfajow4uta';

    public static $userDevice = CHAR_D_MENU;

    public static $rules = [
        'birthday' => ['sometimes', 'date', 'after:1899-12-31']
    ];

    /*
     * Model relationships
     */

    function answers()
    {
        return $this->hasMany('docomo\Models\Answers\Answer');
    }

    function userAttributes()
    {
        return $this->hasMany('docomo\Models\Users\UserAttribute');
    }

    /*
     * Override date mutator definition
     */

    public function getDates()
    {
        return ['created_at', 'updated_at'];
    }

    public function scopeFindUid($query, $uid)
    {
        if ($uid != null && strstr($uid, D_MENU_HEADER) === false && strstr($uid, I_MODE_HEADER) === false) {
            $uid = I_MODE_HEADER . $uid;
        }

        return $query->whereUid($uid);
    }

    private $visibleProfile = null;
    public function isVisibleProfileButton()
    {
        if (is_null($this->visibleProfile)) {
            $priorities = Attribute::getPriorities();
            $this->visibleProfile = false;
            foreach ($priorities as $priority) {
                if (!isset($this->attrList[$priority->id]) || $this->attrList[$priority->id] == ATTRIBUTE_VALUE_NOT_SET) {
                    $this->visibleProfile = true;
                }
            }
        }

        return $this->visibleProfile;
    }

    /*
     * Acceptable params from input
     */

    /*
     * Validation rules
     */

    /*
     * Custom validation messages
     */


    /*
     * Methods
     */

    public function getAttributeValueName($attribute_id)
    {
        if ($attribute_id == ATTR_BIRTHDAY) {

            return jpDate($this->birthday);
        } else {

            return AttributeValue::getDataByCache($attribute_id)[$this->attrList[$attribute_id]]->attribute_value_name;
        }
    }

    public function getAttributeValueLabel($attribute_id)
    {

        return AttributeValue::getDataByCache($attribute_id)[$this->attrList[$attribute_id]]->attribute_value_name;
    }

    private $device = null;
    public function getDevice()
    {
        if ($this->device === null) {
            $this->device = (User::$userDevice == CHAR_I_MODE) ? TYPE_I_MODE : TYPE_D_MENU;
        }

        return $this->device;
    }

    public function getBirthdayText() {
        if (isset($this->birthday)) {

            return str_replace('-', '', $this->birthday);
        }

        return null;
    }

    public function getEvaluate($question_id)
    {

        return VoteRating::select('type')
            ->whereQuestionId($question_id)
            ->whereUserId($this->id)
            ->first();
    }

    public function has($attribute_id) {

        return array_key_exists($attribute_id, $this->attrList) && $this->attrList[$attribute_id] != ATTRIBUTE_VALUE_NOT_SET;
    }

    public function setAge()
    {
        if ($this->birthday) {
            $ymd = explode('-', $this->birthday);
            if (checkdate(intval($ymd[1]), intval($ymd[2]), intval($ymd[0]))) {
                $now = new DateTime();
                $diff = $now->diff(new DateTime($this->birthday));
                $this->age = $diff->y;
            }
        }
    }

    public function setAttributes()
    {
        $this->attrList = [];
        $attributes = Attribute::getDataByCache();
        foreach ($attributes as $attribute) {
            $this->attrList[$attribute->id] = ATTRIBUTE_VALUE_NOT_SET;
        }
        $user_attributes = UserAttribute::whereUserId($this->id)->get(['attribute_id', 'attribute_value_id']);
        foreach ($user_attributes as $attribute) {
            $this->attrList[$attribute->attribute_id] = $attribute->attribute_value_id;
        }

        if ($this->age != null) {
            if ($this->age < 20) {
                $this->attrList[ATTR_BIRTHDAY] = 1;
            } else {
                if ($this->age >= 60) {
                    $this->attrList[ATTR_BIRTHDAY] = 6;
                } else {
                    $this->attrList[ATTR_BIRTHDAY] = intval(substr($this->age, 0, 1));
                }
            }
        }
    }

    private $answered = null;
    public function getAnswered() {
        if (is_null($this->answered)) {
            \Session::forget('questionList');
            $this->answered = \Session::get('questionList', function () {
                $answered = null;
                if (is_null($this->id)) {
                    $this->answered = [];
                } else {
                    $answers = Question::
                        select('id')
                        ->whereRaw('`vote_date_to` <= NOW() AND `vote_date_to` >= ADDDATE(NOW(), INTERVAL -24 HOUR)')
                        ->whereStatus(QUESTION_STATUS_FINISH)
                        ->answered($this)
                        ->get();
                    if (isset($answers) &&  $answers->count() > 0) {
                        $answered = $answers->lists('id');
                    }
                }

                return $answered;
            });
        }

        return $this->answered;
    }

    private static $user = null;

    public static function getUser($uid)
    {
        if (is_null(User::$user)) {
            if (is_null($uid) || $uid == '') {
                $user = User::find(0);
            } else {
                $user = User::findUid($uid)->first();
            }

            if ($user) {
                $user->setAge();
                $user->setAttributes();
            } else {
                $user = new User();
                $user->uid = $uid;
                if (isset($uid) && strpos($user->uid, I_MODE_HEADER) === false && strpos($user->uid, D_MENU_HEADER) === false) {
                    $user->uid = I_MODE_HEADER . $user->uid;
                }
                $attributes = Attribute::getDataByCache();
                foreach ($attributes as $attribute) {
                    $user->attrList[$attribute->id] = null;
                }
            }
            User::$user = $user;
        }

        return User::$user;
    }

    public static function getOrCreateUser($uid)
    {
        $user = User::getUser($uid);

        // create user if not exists.
        if (!isset($user->id)) {
            $user->save();
            if (!isset($user->uid)) {
                $user->uid = D_MENU_HEADER . \Hash::make((string) $user->id . User::$uidSeed . (string) time());
                $user->save();
            }
        }

        return $user;
    }

    /**
     * Fill form data and modify data
     *
     * @return user[]
     */
    public static function fillWithModify(User $user = null)
    {
        $setAttribute = false;

        $birthday = \Input::get('birthday');
        if (isset($birthday)) {
            $user->birthday = $birthday;
            if (!is_null($user->birthday) && $user->birthday !== '') {
                $len = strlen($user->birthday);
                if ($len === 8) {
                    $user->birthday = substr($user->birthday, 0, 4) . '-' . substr($user->birthday, 4, 2) . '-' . substr($user->birthday, 6, 2);
                    User::$rules['birthday'][] = 'before:' . date_format(new \DateTime(), 'Ymd');
                    $user->result = $user->save();
                    $setAttribute = true;
                } else if ($len > 6) {
                    $user->birthday = substr($user->birthday, 0, 4) . '-' . substr($user->birthday, 4, 2) . '-' . substr($user->birthday, 6);
                    $user->result = false;
                } else if($len < 5) {
                    $user->birthday .= '--';
                    $user->result = false;
                } else {
                    $user->birthday = substr($user->birthday, 0, 4) . '-' . substr($user->birthday, 4) . '-';
                    $user->result = false;
                }
            } else {
                $user->birthday = null;
                $user->result = $user->save();
            }
        } else if (\Input::get('birthday_year', ' ') != ' ' || \Input::get('birthday_month') != ' ' || \Input::get('birthday_day') != ' ') {
            $birthday = \Input::get('birthday_year'). '-'. \Input::get('birthday_month'). '-'. \Input::get('birthday_day');
            $user->birthday = $birthday;
            if (\Input::get('birthday_year', ' ') == ' ' || \Input::get('birthday_month') == ' ' || \Input::get('birthday_day') == ' '
                || !checkdate(intval(\Input::get('birthday_month')), intval(\Input::get('birthday_day')), intval(\Input::get('birthday_year')))) {
                $user->result = false;
            } else {
                $user->result = $user->save();
                $setAttribute = true;
            }
        } else if (\Input::get('birthday_year', null) != null) {
            $user->birthday = null;
            $user->result = $user->save();
        }

        $attributes = Attribute::getDataByCache();
        foreach ($attributes as $attribute) {
            if (\Input::has($attribute->attribute_directory)) {
                $value = \Input::get($attribute->attribute_directory);
                UserAttribute::updateUserAttr($user->id, $attribute->id, $value);
                if (!empty($value)) {
                    $setAttribute = true;
                }
            }
        }
        if ($setAttribute && is_null($user->attribute_first_date)) {
            \DB::update('UPDATE users SET attribute_first_date = NOW(), updated_at = NOW() WHERE id = ?', [$user->id]);
        }

        $user->setAge();
        $user->setAttributes();

        return $user;
    }
}
