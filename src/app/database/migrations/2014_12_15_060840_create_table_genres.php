<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGenres extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('category_parents');
		Schema::dropIfExists('genre_parents');
		Schema::create('genre_parents', function($table)
		{
			$table->increments("id");
			$table->string("genre_parent_name", 60);
			$table->boolean("parent_is_visible");
			$table->string("genre_parent_directory", 200);
			$table->integer("parent_sort_order");
			$table->timestamps();

			$table->index('parent_is_visible');
		});

		Schema::dropIfExists('category_childs');
		Schema::dropIfExists('genre_children');
		Schema::create('genre_children', function($table)
		{
			$table->increments("id");
			$table->integer("genre_parent_id");
			$table->string("genre_child_name", 60);
			$table->text("genre_child_description")->nullable();
			$table->boolean("child_is_visible");
			$table->string("genre_child_directory", 200);
			$table->integer("child_sort_order");
			$table->timestamps();

			$table->index('genre_parent_id');
			$table->indeX('child_is_visible');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('genre_parents');
		Schema::dropIfExists('category_parents');
		Schema::create("category_parents", function ($table) {
			$table->increments("id");
			$table->string("category_parent_name", 60);
			$table->boolean("parent_is_visible");
			$table->string("category_parent_directory", 200);
			$table->integer("parent_sort_order");
			$table->timestamps();
		});
		Schema::dropIfExists('genre_children');
		Schema::dropIfExists('category_childs');
		Schema::create("category_childs", function ($table) {
			$table->increments("id");
			$table->integer("category_parent_id");
			$table->string("category_child_name", 60);
			$table->text("category_child_description")->nullable();
			$table->boolean("child_is_visible");
			$table->string("category_child_directory", 200);
			$table->integer("child_sort_order");
			$table->timestamps();
		});
	}

}
