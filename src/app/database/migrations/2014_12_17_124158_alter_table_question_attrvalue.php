<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableQuestionAttrvalue extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('questions', function($table) {
            $table->dropColumn('attribute_id');
            $table->dropColumn('attribute_value');
        });

        Schema::table('questions', function($table) {
            $table->integer('attribute_id')->after('shuffle_choice')->nullable();
            $table->integer('attribute_value_id')->after('attribute_id')->nullable();
            $table->string('description')->after('details')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('questions', function($table) {
            $table->dropColumn('attribute_id');
            $table->dropColumn('attribute_value_id');
        });

        Schema::table('questions', function($table) {
            $table->integer('attribute_id')->after('shuffle_choice');
            $table->string('attribute_value')->after('attribute_id');
        });
	}

}
