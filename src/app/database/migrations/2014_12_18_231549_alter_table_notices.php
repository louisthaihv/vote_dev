<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableNotices extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasColumn('notices', 'visible_date_from'))
		{
			Schema::table('notices', function($table)
			{
				$table->renameColumn('visible_date_from', 'from');
				$table->renameColumn('visible_date_to', 'to');
				$table->renameColumn('detail', 'details');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasColumn('notices', 'from'))
		{
			Schema::table('notices', function($table)
			{
				$table->renameColumn('from', 'visible_date_from');
				$table->renameColumn('to', 'visible_date_to');
				$table->renameColumn('details', 'detail');
			});
		}
	}

}
