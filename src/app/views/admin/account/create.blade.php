@extends('layouts.admin_in')
@section('content')
<h2>アカウント登録</h2>
@include('admin.account.form', ['url' => 'manage/account/create', 'method' => 'post', 'confirmMessage' => C_REGISTRATION, 'account' => $account, 'button' => REGISTRATION])
@stop