<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use docomo\Models\Questions\SummaryByType;
use docomo\Models\Questions\SummaryByAttributes;

class CreateListCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'summary:list';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'create list data.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		\DB::transaction(function ()
		{
			$this->info('truncate summary table');
			SummaryByType::truncate();
			$this->info('create rating list');
			SummaryByType::createVoteRating();

			$this->info('summary attribute list');
			SummaryByAttributes::summary();
		});
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
