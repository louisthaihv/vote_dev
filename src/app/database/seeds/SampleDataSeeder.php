<?php

use Faker\Factory as Faker;
use docomo\Models\Questions\Question;
use docomo\Models\Genres\ParentGenre;
use docomo\Models\Genres\ChildGenre;

class SampleDataSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        
        // Question
        $pId = ParentGenre::all()->lists('id');
        $cId = ChildGenre::all()->lists('id');

        DB::table('questions')->truncate();
        $arrQues = [];
        foreach (range(1, 30) as $index) {
            $arrQues[] = [
                'genre_parent_id' => $faker->randomElement($pId),
                'genre_child_id' => $faker->randomElement($cId),
                'status' => rand(1, 6),
                'title' => $faker->sentence(5),
                'details' => $faker->sentence(5),
                'vote_date_from' => $faker->dateTimeBetween($startDate = '-1 days', $endDate = 'now'),
                'vote_date_to' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+1 years'),
                'viewable_type' => 1,
                'visible_device_i' => 1,
                'visible_device_d' => 1,
                'summary_type' => 0,
                'average_unit' => 0,
                'is_visible_other' => 0,
                'other_text' => 'other',
                'shuffle_choice' => 0,
                'attribute_id' => 0,
                'attribute_value_id' => 0,
                'result_comment' => 'result_comment',
                'navigation_title' => 'navigation_title',
                'sns_comment' => 'sns_comment',
                'comment' => 'admin_comment',
                'back_button_text' => 'back_btn',
                'back_button_url' => 'back_url',
                'has_column' => 1,
                'account_id' => 1
            ];
        }
        DB::table("questions")->insert($arrQues);

        // Choice
        DB::table('choices')->truncate();
        $qId = Question::all()->lists('id');
        $arrChoice = [];
        foreach ($qId as $id) {
            foreach (range(1, 5) as $index) {
                $arrChoice[] = [
                    'question_id' => $id,
                    'choice_text' => 'choice' . $index--,
                ];
            }
        }
        
    }
}
