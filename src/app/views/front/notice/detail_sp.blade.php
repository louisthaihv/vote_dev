@extends('layouts.front_in')

@section('metaTag')
    <title>{{ $notice->title }} | みんなの声</title>
    <meta name="description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！">
    <meta property="og:title" content="{{ $notice->title }} | みんなの声" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
    <meta name="twitter:title" content="{{ $notice->title }} | みんなの声" />
    <meta name="twitter:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
@stop

@section('head')
    <link rel="stylesheet" href="{{ url('common/front/css/info.css') }}" type="text/css" />
@stop

@section('content')
    @if ($notice)
        @include('layouts.elements.displays.notice_box')
    @endif
    <p class="tcenter l-margin"><a href="{{ route('home.top') }}" class="btnType02">みんなの声TOPへ</a></p>
@endsection