@section('head')
<link rel="stylesheet" href="{{ url('common/admin/css/jquery-ui-1.8.23.custom.css') }}" type="text/css" />
<script type="text/javascript" src=" {{ url('common/admin/js/jquery-ui-1.8.23.custom.min.js') }}"></script>
<script type="text/javascript" src=" {{ url('common/admin/js/jquery.ui.datepicker-ja.js') }}"></script>
@stop
@section('script')
<script type="text/javascript">
$(window).load(function(){
    
    //日付ピッカー
    $("input[name='from_date']").datepicker({
        dateFormat: 'yymmdd'
    });
    //日付ピッカー
    $("input[name='to_date']").datepicker({
        dateFormat: 'yymmdd'
    });
});
</script>
@stop

@include('admin.share.errors', ['errorList' => $errors])
{{ Form::model($feature, ['url' => '/manage/feature/confirm', 'method' => 'post', 'files' => true]) }}
    <p>特集枠表示内容</p>
    <table class="formTable">
    <tr>
        <th>タイトル<span class="require">（必須）</span></th>
        <td>{{ Form::text('title', $feature->title, ['class' => 'xx-large']) }}</td>
    </tr>
    <tr>
        <th>サムネイル<br />(横144px推奨)50KB以内</th>
        <td>@include('layouts.elements.displays.file_uploader', ['uploader_id' => 'thumbnail_file', 'db_file' => $feature->thumbnail])</td>
    </tr>
    <tr>
        <th>メッセージ<br />HTML可</th>
        <td>{{ Form::textarea('message', $feature->message, ['class' => 'xx-large', 'rows' => 10]) }}</td>
    </tr>
    <tr>
        <th>期間<span class="require">（必須）</span></th>
        <td>
            {{ Form::dateTimeControl('from', $feature->from) }}
        <br />
        <br />
        ～<br />
        <br />
            {{ Form::dateTimeControl('to', $feature->to) }}
        </td>
    </tr>
    <tr>
    <th>特集一覧に表示</th>
    <td>
        <label>
           {{ Form::checkbox('is_view_list', null, $feature->is_view_list) }} 特集一覧に表示する
        </label>
    </td>
    </tr>

    </table>
    <br />
    <p>リスト</p>
    <table class="formTable">
        <tr class="typeChoice">
            <th>アンケートID</th>
            <td>{{ Form::text('question_ids', implode(',', $feature->getQuestionIdList()), ['class' => 'xx-large']) }}<p>
                    ※IDをカンマ区切りで入れてください
                    <a href="#" target="_blank" onclick="window.open('{{ route('manage.question.index') }}', 'searchwindow', 'width=800, height=600, menubar=no, toolbar=no, scrollbars=yes');return false;">別ウィンドウで検索を開く</a></p></td>
        </tr>
    <tr>
        <th>コラム特集か？</th>
        <td>
            <label>{{ Form::checkbox('is_feature_column', null, $feature->is_feature_column) }} 遷移先でコラムを開く</label>
        </td>
    </tr>
    </table>
    <div class="tcenter m-margint">
        <button type="submit" name="commit" class="commitBtn">確認</button>
    </div>
{{ Form::close() }}
