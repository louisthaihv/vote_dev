<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableSpecials extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 200);
            $table->string('thumbnail', 200)->nullable();
            $table->text('message')->nullable();
            $table->timestamp('visible_date_from');
            $table->timestamp('visible_date_to');
            $table->boolean('is_visible');
            $table->string('question_id', 200);
            $table->boolean('is_open_column');
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specials');
    }
}
