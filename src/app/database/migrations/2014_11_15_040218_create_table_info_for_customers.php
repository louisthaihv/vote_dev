<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableInfoForCustomers extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_for_customers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_id');
            $table->integer('visible_type');
            $table->integer('serial_no');
            $table->string('thumbnail', 200);
            $table->text('description')->nullable();
            $table->string('url', 200)->nullable();
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_for_customers');
    }
}
