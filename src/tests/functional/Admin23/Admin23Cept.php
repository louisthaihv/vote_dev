<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('manage account');

$I->amOnPage('/manage/account');
$I->amOnAction('Admin\SurveyController@create');
