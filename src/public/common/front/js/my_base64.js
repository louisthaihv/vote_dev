function my_btoa(str) {

    if(typeof str == 'undefined') { // return base64 encode when input null
      return btoa('');
    }

    return btoa(str);
}

function my_atob(str) {

    if(typeof str == 'undefined') { // return base64 decode when input null
      return atob('');
    }

    if(!is_base64(str)) {  // when input not likely base64 encoded, return empty.
      return atob('');
    }
    
    return atob(str);
}

function is_base64(str) {
    var base64Matcher = new RegExp("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$");

    if (base64Matcher.test(str)) {
        // It's likely base64 encoded.
        return true;
    } else {
        // It's definitely not base64 encoded.
        return false;
    }
}