<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('check genre');

$arr_result = array(
                'link_for_showmore' => 'もっと見る',
                'xpath_format' => 'html/body/div[1]/div/div/section[2]/div[2]/div/ul/li[%d]/a'
                );


$genre_parent = array(
                    1 => array(
                                'genre_parent_name' => 'genre_1',
                                'parent_is_visible' => '1',
                                'genre_parent_directory' => 'everyday',
                                'parent_sort_order' => '10',
                                ),
                );
$I->createDatabaseGenreParent($genre_parent);

$genre_child = array(
                    1 => array(
                                'genre_parent_id'=>'1',
                                'genre_child_name'=>'genre_child_1_1',
                                'child_is_visible'=>'1',
                                'genre_child_directory'=>'1',
                                'child_sort_order'=>'10'
                                ),
                );

$I->createDatabaseGenreChild($genre_child);

//create survey entry
$survies = array();
for ($i=1; $i < 12; $i++) {
    $title = 'title_'.$i;
    $date = '2014-12-'.($i+10).' 00:00:00';
    $survies[$i] = array(
                        'genre_parent_id'=>'1',
                        'genre_child_id'=>'1',
                        'status'=>'5',
                        'title'=>$title,
                        'vote_date_from'=>$date,
                        'vote_date_to'=>'2015-02-10 00:00:00',
                        'baseline_date'=>$date,
                        'viewable_type'=>'1',
                        'visible_device_i'=>'1',
                        'visible_device_d'=>'1',
                        'summary_type'=>'1',
                        'average_unit'=>'1',
                        'is_visible_other'=>'1',
                        'shuffle_choice'=>'1',
                        'navigation_title'=>'1',
                        'account_id'=>'1',
                        );
}
$I->createDatabaseSurvey($survies);

//after create output 11 items entry
$survies_check = array();
foreach ($survies as $survey) {
    $jpdate = $I->dateToJpdate($survey['baseline_date']);
    $survies_check[] = array('title' => $survey['title'], 'jpdate' => $jpdate, 'status' => '1');
}
//order list to see
krsort($survies_check);
$arr_result['items_check'] = $survies_check;


//create keyword
$keywords = array();
$keywords[1] = array('keyword_name' => 'keyword_1', 'is_visible' => '1');
$keywords[2] = array('keyword_name' => 'keyword_1_2', 'is_visible' => '1');
$I->createDatabaseKeyword($keywords);
//create question tags
$question_tags = array();
for ($i=1; $i < 12; $i++) {
    $question_tags[$i] = array('question_id' => $i, 'keyword_id' => '1');
}
$I->createDatabaseQuestionTags($question_tags);


//search
$I->amOnPage('/search/tag/search');
$I->fillField('input[name=word]', 'keyword_1');
$I->click('検索');
$I->seeCurrentUrlEquals('/search/tag/keyword_1');
$I->wait(1);
$I->checkSearchKeyword($keywords, $arr_result);

