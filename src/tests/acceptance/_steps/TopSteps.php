<?php
namespace AcceptanceTester;

class TopSteps extends \AcceptanceTester\CommonSteps
{
    public function createDbQuestionFull($questions)
    {
        $I = $this;
        foreach ($questions as $key => $value) {
            $I->haveInDatabase('questions', array(
                                                'genre_parent_id'=>$value['genre_parent_id'],
                                                'genre_child_id'=>$value['genre_child_id'] ,
                                                'status'=>$value['status'] ,
                                                'title'=>$value['title'] ,
                                                'vote_date_from'=>$value['vote_date_from'] ,
                                                'vote_date_to'=>$value['vote_date_to'] ,
                                                'baseline_date'=>$value['baseline_date'] ,
                                                'viewable_type'=>$value['viewable_type'] ,
                                                'visible_device_i'=>$value['visible_device_i'] ,
                                                'visible_device_d'=>$value['visible_device_d'] ,
                                                'summary_type'=>$value['summary_type'] ,
                                                'average_unit'=>$value['average_unit'] ,
                                                'is_visible_other'=>$value['is_visible_other'] ,
                                                'shuffle_choice'=>$value['shuffle_choice'] ,
                                                'navigation_title'=>$value['navigation_title'] ,
                                                'account_id'=>$value['account_id'] ,
                                                'attribute_id'=>$value['attribute_id'] ,
                                                'attribute_value_id'=>$value['attribute_value_id'] ,
                                                )
            );
        }
    }
    public function createDbResultVoteFull($votes)
    {
        $I = $this;
        foreach ($votes as $key => $value) {
            $I->haveInDatabase('result_votes', array(
                                                'question_id' => $value['question_id'],
                                                'attribute_id' => $value['attribute_id'],
                                                'attribute_value_id' => $value['attribute_value_id'],
                                                'device' => $value['device'],
                                                'choice_id' => $value['choice_id'],
                                                'voted_count' => $value['voted_count'],
                                                )
            );
        }
    }
    public function checkQuestionTop($questions, $title)
    {
        $I = $this;
        foreach ($title as $k => $v) {
            $I->see($v['title']);
        }
        foreach ($questions as $key => $value) {
            if(($value['status'] == 5) && ($value['attribute_value_id'] !=NULL)){
                $I->seeLink($value['title']);
                $I->click('html/body/div[10]/div/a');
                $I->moveBack();
            }
            if($value == 6){
                $I->seeLink($value['title']);
            }
            if(($value['status'] == 5) && ($value['attribute_value_id'] ==NULL)){
                $I->seeLink($value['title']);
                $I->click('html/body/div[14]/div/a');
                $I->moveBack();  
            }
        }
    }
    public function checkKeywordTop($keyword)
    {
        $I = $this;
        foreach ($keyword as $key => $value) {
            if($value['is_visible'] == 1){
                $I->seeLink($value['keyword_name']);
            }
            else{
                $I->dontSee($value['keyword_name']);
            }
        }
    }
    public function checkButtonLink($button_link)
    {
        $I = $this;
        $I->click($button_link);
        $I->seeCurrentUrlEquals('/mypage');
        $I->moveBack();
    }
}