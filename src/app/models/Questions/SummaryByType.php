<?php
namespace docomo\Models\Questions;

use docomo\Models\Codes\EvaluationType;
use docomo\Models\Ratings\VoteRatingTotal;
use docomo\Models\Ratings\VoteRating;
use docomo\Models\Users\User;

class SummaryByType extends \BaseArdentModel
{

    protected $table = 'summary_by_types';

    public static $summaryByTypeData = null;

    public $timestamps = false;

    public static $cacheName = 'SummaryByType';

    public static $summaryHour = 48;

    public function question()
    {

        return $this->belongsTo('docomo\Models\Questions\Question');
    }

    public static function createVoteRating()
    {

        $ratings = \DB::table('vote_ratings')->selectRaw('type, count(user_id) as votes, question_id')
            ->whereRaw('`vote_date` >= DATE_ADD(NOW(), INTERVAL -' . SummaryByType::$summaryHour . ' hour)')
            ->groupBy('type', 'question_id')
            ->get();

        if (is_null($ratings) || count($ratings) == 0) {
            return;
        }

        arsort($ratings);
        $ranking = [SUMMARY_TYPE_EVALUATION_GATTEN => 1, SUMMARY_TYPE_EVALUATION_MAJI => 1, SUMMARY_TYPE_EVALUATION_MEMO => 1];
        foreach ($ratings as $rating) {
            if ($ranking[$rating->type] === 6) continue;

            if (Question::select('viewable_type')->find($rating->question_id)->viewable_type != DISPLAY) {
                // 一覧で表示しない/完全非表示の場合は次へ
                continue;
            }
            SummaryByType::addRecord($rating->type, $ranking[$rating->type], $rating->question_id, $rating->votes);
            $ranking[$rating->type]++;
        }
        unset($ratings);
    }

    public static function getListByCache($tab = null)
    {
        if (is_null(SummaryByType::$summaryByTypeData)) {
            SummaryByType::$summaryByTypeData = \Cache::get(SummaryByType::$cacheName, function () {
                return SummaryByType::setCache();
            });
        }

        if (is_null($tab)) {
            return SummaryByType::$summaryByTypeData;
        }
        if (array_key_exists($tab, SummaryByType::$summaryByTypeData)) {
            return SummaryByType::$summaryByTypeData[$tab];
        }
        return [];
    }

    public static function setCache() {
        $data = SummaryByType::
            select('type', 'ranking_number', 'question_id')->orderBy('type')->orderBy('ranking_number')
            ->with(['question' => function ($query) {
                return $query->fieldList();
            }])
            ->get();
        $setData = [];
        foreach ($data as &$rec) {
            if (!array_key_exists($rec->type, $setData)) {
                $setData[$rec->type] = [];
            }
            if ($rec->question) {
                $setData[$rec->type][$rec->ranking_number] = $rec->question;
            }
        }
        unset ($rec);
        unset ($data);

        \Cache::put(SummaryByType::$cacheName, $setData, 5);

        return $setData;
    }

    public static function getTopData($user) {
        $getEntryNum = ($user->getDevice() === TYPE_D_MENU) ? 20 : 20;
        $getResultNum = ($user->getDevice() === TYPE_D_MENU) ? 20 : 20;
        $list = [];
        // 新着受付中
        $list[SUMMARY_TYPE_NEWER_ENTRY] = Question::fieldList()
            ->baseViewable(
                QUESTION_STATUS_READY,
                new \Datetime(),
                $user->getDevice(),
                $user
            )
            ->unanswered($user)
            ->limit($getEntryNum)
            ->orderBaseDateToDesc()
            ->get();

        // 新着結果
        $list[SUMMARY_TYPE_NEWER_RESULT] = Question::fieldList()
            ->baseViewable(
                QUESTION_STATUS_FINISH,
                new \Datetime(),
                $user->getDevice(),
                null
            )
            ->orderBaseDateToDesc()
            ->limit($getResultNum)
            ->get();

        if ($user->getDevice() === TYPE_D_MENU) {
            $data = SummaryByType::getListByCache();
            if (array_key_exists(SUMMARY_TYPE_EVALUATION_GATTEN, $data)) {
                $list[SUMMARY_TYPE_EVALUATION_GATTEN] = $data[SUMMARY_TYPE_EVALUATION_GATTEN];
            }
            if (array_key_exists(SUMMARY_TYPE_EVALUATION_MAJI, $data)) {
                $list[SUMMARY_TYPE_EVALUATION_MAJI] = $data[SUMMARY_TYPE_EVALUATION_MAJI];
            }
            if (array_key_exists(SUMMARY_TYPE_EVALUATION_MEMO, $data)) {
                $list[SUMMARY_TYPE_EVALUATION_MEMO] = $data[SUMMARY_TYPE_EVALUATION_MEMO];
            }
        }

        return $list;
    }

    public static function addRecord($type, $ranking_number, $question_id, $count = 0, $baseline_date = null)
    {
        $new_rec = new SummaryByType();
        $new_rec->type = $type;
        $new_rec->ranking_number = $ranking_number;
        $new_rec->question_id = $question_id;
        $new_rec->count = $count;
        $new_rec->baseline_date = $baseline_date;
        $new_rec->save();
        unset($new_rec);
    }

    public static function insertRecordByRanking($type, $ranking_number, $question_id, $count = 0, $baseline_date = null)
    {
        // まずは挿入箇所をあける
        \DB::update('UPDATE summary_by_types SET ranking_number = ranking_number + 1 WHERE type = ? AND ranking_number >= ? ORDER BY ranking_number desc',
            [$type, $ranking_number]);
        // その後そこに差し込む！
        SummaryByType::addRecord($type, $ranking_number, $question_id, $count, $baseline_date);
    }

    public static function insertRecordByDate($type, $baseline_date, $question_id, $count = 0)
    {
        // まずはrankingを取得
        $ranking = SummaryByType::whereType($type)
            ->where('baseline_date', '>=', $baseline_date)
            ->min('ranking_number');
        if (is_null($ranking)) {
            $ranking = 1;
        }
        // ランキングで差し込み
        SummaryByType::insertRecordByRanking($type, $ranking, $question_id, $count, $baseline_date);
    }

    public static function deleteByQuestionId($questionId)
    {
        // 全部消す
        \DB::delete('DELETE FROM summary_by_types WHERE question_id = ?', [$questionId]);
    }

}
