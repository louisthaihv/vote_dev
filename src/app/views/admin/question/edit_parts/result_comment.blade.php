    @if (Auth::user()->havePermission('is_manage_input_comment'))
    <p>結果ページ用コメント</p>
    <table class="formTable">
        <tr>
            <th>結果ページ用コメント<br />（HTMLタグ可）</th>
            <td>
                {{ Form::textarea('result_comment', $question->result_comment, ['class' => 'xx-large', 'id' => 'result_comment']) }}
                <a class="naviBtn" id="commentWindow">イメージを見る</a>
            </td>
        </tr>
    </table>
    @endif
