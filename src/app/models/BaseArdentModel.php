<?php
use docomo\Models\Events\EventGenerator;
use Illuminate\Support\Facades\Crypt;

class BaseArdentModel extends Ardent
{
    use EventGenerator;

    protected $guarded = array();
    // Important

    //Ardent configs
    public $autoHydrateEntityFromInput = true;
    public $autoPurgeRedundantAttributes = true;
    public $autoHashPasswordAttributes = true;

    public static $checkedCache = false;
    public static $cachedTime = [];

    protected function hashPasswordAttributes(array $attributes = array(), array $passwordAttributes = array())
    {

        if (empty($passwordAttributes) || empty($attributes)) {
            return $attributes;
        }

        $result = array();
        foreach ($attributes as $key => $value) {

            if (in_array($key, $passwordAttributes) && !is_null($value)) {
                if ($value != $this->getOriginal($key)) {
                    $result[$key] = Crypt::encrypt($value);
                }
            } else {
                $result[$key] = $value;
            }
        }

        return $result;
    }

    protected function formatToDateTime($date, $hour, $minute)
    {
        if (is_null($date) || empty($date) || is_null($hour) || is_null($minute))
        {
            return null;
        }

        return substr($date, 0, 4) . '-' . substr($date, 4, 2) . '-' . substr($date, 6) . ' ' . $hour . ':' . $minute;
    }

    public function getDatetimeControlValue($control_name)
    {

        return $this->formatToDateTime(
            \Input::get($control_name . '_date'),
            \Input::get($control_name . '_hour'),
            \Input::get($control_name . '_min')
        );
    }

    public function isFuture()
    {

    }

    public function isNowVisible()
    {

    }

    public function isEnd()
    {

        return strtotime(date('Y-m-d H:i:s')) > strtotime($this->to);
    }

    protected function isAllowExtension($extension)
    {
        $extension = mb_strtolower($extension);

        return ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'gif');
    }

    public static function checkCache($cacheName)
    {
        // キャッシュしたデータの最終更新日時と、DBの最終更新日時をチェックする
        if (BaseArdentModel::$checkedCache) {
            return BaseArdentModel::$cachedTime[$cacheName];
        }
        $returnTime = null;
        $updateTime = \DB::select('SELECT SQL_CACHE cache_key, update_time FROM data_update_time');
        BaseArdentModel::$checkedCache = true;
        // 配列を初期化
        BaseArdentModel::$cachedTime = [];
        foreach ($updateTime as &$time) {
            $cacheTime = \Cache::get($time->cache_key . 'Time', null);
            if (is_null($cacheTime) || strtotime($time->update_time) > strtotime($cacheTime)) {
                \Cache::forget($time->cache_key);
            }
            BaseArdentModel::$cachedTime[$time->cache_key] = $time->update_time;
            unset($cacheTime);
            unset($time);
        }
        unset($updateTime);
        return BaseArdentModel::$cachedTime[$cacheName];
    }

    public static function setCacheTime($cacheName, $updateTime) {
        \Cache::put($cacheName . 'Time', $updateTime, DEFAULT_CACHE_TIME);
        unset($updateTime);
    }

    public static function updateCacheTime($cacheName) {
        \DB::update('UPDATE data_update_time SET update_time = NOW() WHERE cache_key=?', [$cacheName]);
    }
}
