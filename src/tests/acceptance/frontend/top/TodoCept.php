<?php
$I = new AcceptanceTester\FrontTopSteps($scenario);
$I->wantTo('check front top');


$genre_parent = array(
                    1 => array(
                                'genre_parent_name' => 'genre_1',
                                'parent_is_visible' => '1',
                                'genre_parent_directory' => 'everyday',
                                'parent_sort_order' => '10',
                                )
                );
$I->createDbGenreParent($genre_parent);

$genre_child = array(
                    1 => array(
                                'genre_parent_id'=>'1',
                                'genre_child_name'=>'genre_child_1',
                                'child_is_visible'=>'1',
                                'genre_child_directory'=>'1',
                                'child_sort_order'=>'10'
                                )
                );

$I->createDbGenreChild($genre_child);


//create survey entry
$questions_entry = array();
for ($i=1; $i <= 3; $i++) {
    $title = 'title_'.$i;
    $date = '2014-12-'.($i+10).' 00:00:00';
    $questions_entry[$i] = array(
                        'genre_parent_id'=>'1',
                        'genre_child_id'=>'1',
                        'status'=>'5',
                        'title'=>$title,
                        'vote_date_from'=>$date,
                        'vote_date_to'=>'2015-04-10 00:00:00',
                        'baseline_date'=>$date,
                        'viewable_type'=>'1',
                        'visible_device_i'=>'1',
                        'visible_device_d'=>'1',
                        'summary_type'=>'1',
                        'average_unit'=>'1',
                        'is_visible_other'=>'1',
                        'shuffle_choice'=>'1',
                        'navigation_title'=>'1',
                        'account_id'=>'1',
                        );
}
$I->createDbQuestion($questions_entry);

//after create output 11 items entry
$questions_entry_check = array();
foreach ($questions_entry as $key => $value) {
    $jpdate = $I->dateToJpdate($value['baseline_date']);
    $questions_entry_check[$key] = array('title' => $value['title'], 'jpdate' => $jpdate, 'status' => '1');
}
//order list to see
krsort($questions_entry_check);


//create survey result
$questions_result = array();
for ($i=1; $i <= 3; $i++) {
    $title = 'title_'.($i+20);
    $date = '2014-12-'.($i+10).' 00:00:00';
    $questions_result[$i] = array(
                        'genre_parent_id'=>'1',
                        'genre_child_id'=>'1',
                        'status'=>'6',
                        'title'=>$title,
                        'vote_date_from'=>'2014-12-05 00:00:00',
                        'vote_date_to'=>$date,
                        'baseline_date'=>$date,
                        'viewable_type'=>'1',
                        'visible_device_i'=>'1',
                        'visible_device_d'=>'1',
                        'summary_type'=>'1',
                        'average_unit'=>'1',
                        'is_visible_other'=>'1',
                        'shuffle_choice'=>'1',
                        'navigation_title'=>'1',
                        'account_id'=>'1',
                        );
}
$I->createDbQuestion($questions_result);

//after create output 3 items result
$questions_result_check = array();
foreach ($questions_result as $key => $value) {
    $jpdate = $I->dateToJpdate($value['baseline_date']);
    $questions_result_check[$key] = array('title' => $value['title'], 'jpdate' => $jpdate, 'status' => '1');
}
//order list to see
// krsort($questions_result_check);


//create feature
$features = array();
for ($i=1; $i <=2 ; $i++) {
    $title = 'feature_'.$i;
	$features[$i] = array(
						'title' => $title,
                        'message' => 'message',
						'question_ids' => '1,2,3',
						'status' => $i-1,
						);
}
$I->createDbFeature($features);


//check top
$I->checkCollectionFrame($features, $questions_entry_check);
//check search
$I->checkSearchKeyword();
//check profile
$I->checkProfileSetting();


//create columns
$columns = array();
for ($i=1; $i <=3; $i++) {
	$title = 'column_title_'.$i;
	$columns[$i] = array(
						'question_id' => $i,
						'is_visible' => '1',
						'column_title' => $title,
						'column_detail' => 'detail',
						'view_create_datetime' => date('Y-m-d H:i:s'),
						'created_account_id' => '1',
						'updated_account_id' => '1',
						);
}
$I->createDbColumns($columns);

//after create output 3 items
$columns_check = array();
foreach ($columns as $column) {
	$jpdate = $I->dateToJpdate($column['view_create_datetime']);
	$columns_check[] = array('title' => $column['column_title'], 'jpdate' => $jpdate, 'status' => '1');
}
//order list to see
krsort($columns_check);

//check colums
$I->checkColumnList($columns_check);


$makesenses = array();
for ($i=1; $i <=3 ; $i++) { 
	$makesenses[$i] = array(    
							'question_id' => $i,
                            'user_id' => 1,
                            'status' => $i-1,
                            );
}
$I->createDbMakesenses($makesenses);


//check survey evaluations
$I->checkSurveyEvaluations($questions_entry_check);


// //create keyword
// $keywords = array();
// $keywords[1] = array('keyword_name' => 'keyword_1' ,'is_visible' => '1');
// $keywords[2] = array('keyword_name' => 'keyword_2' ,'is_visible' => '1');
// $I->createDbKeyword($keywords);

// //create attentions
// $attentions = array();
// for ($i=1; $i <=2 ; $i++) {
// 	$attentions[1] = array('keyword_id' => $i ,'tab' => $i-1);
// }
// $I->createDbAttentions($attentions);

// //check keyword
// $I->checkNoticeKeyword($keywords);


// //check attribute
// // $I->checkSurveyAtribute($question)

// //create vote
// $result_votes = array();
// for ($i=1; $i <=6 ; $i++) { 
// 	$result_votes[$i] = array(
//                             'question_id' => $value['question_id'],
//                             'voted_count' => $value['voted_count'],
//                             );
// }
// $I->createDbResultVote($result_votes);

// //create answer
// $answers = array();
// for ($i=1; $i <=6 ; $i++) { 
// 	$answers[$i] = array(
//                         'question_id' => $i,
//                         );
// }
// $I->createDbAnswers($answers);


// //create pickup
// $pickups = array();
// for ($i=1; $i <=6 ; $i++) {
// 	$pickups[$i] = array(
// 						'question_id' => $i,
//                         'type' => 1,
//                         );
// }
// $I->createDbPickups($pickups);

// //create summarybytype
// $summary_by_types = array();
// for ($i=1; $i <=6 ; $i++) {
// 	$summary_by_types[$i] = array(
//                                 'question_id' => $i,
//                                 'type' => 1,
//                                 'count' => $i,
//                                 'sort_order' => $i,
//                                 );
// }
// $I->createDbSummaryByTypes($summary_by_types);

// //check survey list
// $I->checkSurveyList($questions_entry_check, $questions_result_check);



