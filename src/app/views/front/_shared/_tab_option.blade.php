<div class="box-tabs m-margin">
    <ul class="">
        <li id="" {{ ($mode === PROCESSING) ? 'class="active"' : '' }}><a href="{{ ($mode === PROCESSING) ? '' : str_replace('result', 'entry', Request::url()) }}"><span class="iconEntry">{{ $tabEntry }}</span></a></li>
        <li id="" {{ ($mode !== PROCESSING) ? 'class="active"' : '' }}><a href="{{ ($mode !== PROCESSING) ? '' : str_replace('entry', 'result', Request::url()) }}"><span class="iconResult">{{ $tabResult }}</span></a></li>
    </ul>
</div>