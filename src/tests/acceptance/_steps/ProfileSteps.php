<?php
namespace AcceptanceTester;

class ProfileSteps extends \AcceptanceTester\CommonSteps
{

    public function checkFPProfile($profile, $descriptions)
    {
        $I = $this;
        $I->amOnPage('/profile');
        $this->checkProfileConfig($profile);
        $this->checkProfileIntroText($descriptions);
    }

    public function checkProfileConfig($profile)
    {
        $I = $this;
        $I->checkSelectedOptionCommon($profile["sex"]);
        $I->checkSelectedOptionCommon($profile["year"]);
        $I->checkSelectedOptionCommon($profile["month"]);
        $I->checkSelectedOptionCommon($profile["day"]);
        $I->checkSelectedOptionCommon($profile["place"]);
    }

    public function checkFPProfileSuccess($links)
    {
        $I = $this;
        $I->seeCurrentUrlEquals('/profile');
        foreach ($links as $link) {
            $I->checkLinkCommon($link);
        }
    }
}