$(window).load(function(){
    $('.answerBox ul li').click(function(){
        $(this).addClass('active').siblings().removeClass('active');
        $('.answerBox ul li button').stop(0).animate({'marginLeft':'0px'},{duration:200});
        $('button',$(this)).stop(0).animate({'marginLeft':'-100px'},{duration:200});
        return false;
    });

    $('.answerBox ul li button').click(function(){
        $(this).attr('disabled', 'disabled');
        $(this).parents('form').submit();

        return false;
    });

    $('#vote_bottom').click(function () {
        if ($('.answerBox ul li.active').size() > 0) {
            $('.answerBox ul li.active button').click();
        } else {
            alert('投票したい選択肢を選んでください。');
        }
    });
});
