@extends('layouts.front_in')

@section('metaTag')
    <title>新着アンケート | みんなの声</title>
    <meta name="description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！">
    <meta property="og:title" content="新着アンケート | みんなの声" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
    <meta name="twitter:title" content="新着アンケート | みんなの声" />
    <meta name="twitter:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
@stop

@section('script')
    @include('front._shared.js_load_more_pages', ['classNames'=>['newEntry', 'newResult'], 'nums' => [NUM_FIRST_PAGE_10, NUM_FIRST_PAGE_10], 'pages' => [PAGER_SIZE_FRONT, PAGER_SIZE_FRONT]])
    @include('front._shared.js_unload_page', ['route'=>'list.new'])

@endsection

@section('content')

    <section class="l-margin">
        <h2 class="newListTit"><span>新着アンケート</span></h2>
        @include('front._shared._tab_option')
        <div class="tabs_content">
            @if ($mode === PROCESSING)
            <div class="entryBox">
                @include('front._shared._question_list', ['displaySeeMore' => TRUE, 'className' => 'newEntry' ])
                <p class="gotoListArea">{{ link_to_route('list.unanswered', '未回答一覧') }}</p>
            </div>
            @endif
            @if ($mode === COMPLETE)
            <div class="resultBox">@include('front._shared._question_list', ['displaySeeMore' => TRUE, 'className' => 'newResult' ])</div>
            <br />
            @endif
        </div>
        @include('layouts.front_elements._profileBn')
    </section>

@endsection
