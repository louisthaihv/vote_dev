<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('manage survey');

$I->amOnPage('/manage/enq');
$I->amOnAction('Admin\SurveyController@create');
