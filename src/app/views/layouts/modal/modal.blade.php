@if(Session::has('message'))
<div id="dialog-message">
	<p>
		{{ Session::pull('message', '') }}
	</p>	
</div>
<script type="text/javascript"> 
	$(function() {
		$( "#dialog-message" ).dialog({
			modal: true,
			buttons: {
				Ok: function() {
					$( this ).dialog("close");
				}
			}         
		});
	});
</script>
@endif