<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultVoteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('result_votes');
		Schema::create('result_votes', function($table)
		{
			// schema
			$table->increments('id');
			$table->integer('question_id');
			$table->integer('attribute_id');
			$table->integer('attribute_value');
			$table->integer('device');
			$table->integer('choice_id');
			$table->integer('ranking')->unsigned();
			$table->integer('voted_count')->unsigned()->default(0);
			$table->integer('summary_value');
			// indexes
			$table->index('question_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('result_votes');
	}

}
