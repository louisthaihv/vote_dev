<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCategoryChilds extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("category_childs", function ($table) {
            $table->increments("id");
            $table->integer("category_parent_id");
            $table->string("category_child_name", 60);
            $table->text("category_child_description")->nullable();
            $table->boolean("child_is_visible");
            $table->string("category_child_directory", 200);
            $table->integer("child_sort_order");
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("category_childs");
    }
}
