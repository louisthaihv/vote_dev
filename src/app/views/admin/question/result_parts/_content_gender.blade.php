<div class="tabBox">
    <p class="l-margin">
        {{ link_to_route('manage.question.csv', '性別CSVダウンロード', [
                    $question->id,
                    '2',
                    '0'
        ] ) }}
        {{ link_to_route('manage.question.csv', '性別CSVダウンロード(d版)', [
                    $question->id,
                    '2',
                    '1'
        ] ) }}
        {{ link_to_route('manage.question.csv', '性別CSVダウンロード(i版)', [
                    $question->id,
                    '2',
                    '2'
        ] ) }}
    </p>

    <div class="resultTable profileChoice">
        <table class="l-margint">
            @include('admin.question.result_parts._result_list', ['list' => $genderList, 'selected' => $selectedGender, 'targetId' => 'sexTitle'])
        </table>
        <!--/resultTable--></div>

    <p class="titPtn1 l-margint" id="sexTitle">{{ $selectedGender->attribute_value_name }}ランキング</p>
    @if ($question->isViewableAverage())
        <p class="avgData">平均：{{ showAverage($topSummaryHash, $selectedGender->id, $question) }}</p>
    @endif
    <p class="tright m-margint attrCount">投票数：<span>{{ number_format(array_sum($genderData->lists('cnt'))) }}</span>票</p>
    <div class="resultTable resultTableTarget">
        <table class="l-margint">
            @include('admin.question.result_parts._result_table', ['resultData' => $genderData])
        </table>
    </div>
</div>
