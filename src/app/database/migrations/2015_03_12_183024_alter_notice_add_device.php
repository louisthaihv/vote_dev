<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNoticeAddDevice extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasColumn('notices', 'visible_device_i')) {
            Schema::table('notices', function ($table) {
                $table->integer('visible_device_i')->default(true)->after('to');
                $table->integer('visible_device_d')->default(true)->after('visible_device_i');
            });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
