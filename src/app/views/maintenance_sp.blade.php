<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE html PUBLIC "-//i-mode group (ja)//DTD XHTML i-XHTML(Locale/Ver.=ja/1.1) 1.0//EN" "i-xhtml_4ja_10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
    <title>みんなの声</title>
</head>

<body link="#0000FF" vlink="#0000FF" style="font-size:small;">
<div class="header" style="text-align:center;">
    <img src="/common/front/images/sp/logo.png" alt="みんなの声"/>
</div>
<div style="margin:0px 10px 0px 10px;text-align:center;">
    <div>みんなの声は現在メンテナンス中です。<br />完了まで今しばらくお待ちください。</div>
    <br />
    <div><a href="http://smt.docomo.ne.jp/">dメニューはこちら</a></div>
    <br />
</div>
<div style="text-align:center;">(C)NTT DOCOMO</div>
</body>
</html>