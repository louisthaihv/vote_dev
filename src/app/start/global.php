<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

	app_path().'/commands',
	app_path().'/controllers',
	app_path().'/models',
	app_path().'/database/seeds',

));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

$logFile = 'laravel.log';

Log::useDailyFiles('/home/prod_vote/storage/logs/'.$logFile, 1, 'error');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::error(function(Exception $exception, $code)
{
	Log::error($exception);
    if (!\Config::get('app.debug')) {
        if ($code === 500) {
            return 'Error : 500 Internal Server Error';
        } elseif ($code === 403) {
            return 'Error : 403 Unauthorized action. ';
        }
    }
});

App::error(function(\Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException  $exception, $code)
{
    return NotFound();
});

App::error(function(Laracasts\Validation\FormValidationException $exception, $code)
{
	return Redirect::back()->withInput()->withErrors($exception->getErrors());
});

App::missing(function($exception)
{
    return NotFound();
});
/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
    if (strpos(\Request::url(), 'localhost') ||
        strpos(\Request::url(), 'vote.smt.docomo.ne.jp') ||
        strpos(\Request::url(), 'stgvote.smt.docomo.ne.jp')) {

        $device = CHAR_D_MENU;
        $ua = $_SERVER['HTTP_USER_AGENT'];
        if (strpos($ua, 'Android') === false && strpos($ua, 'FBIOS') === false) {
            $lowUa = strtolower($ua);
            if (strpos($lowUa, 'docomo') !== false) {
                $devices = '/' . \Config::get('app.devices') . '/';
                if (preg_match($devices, $ua) == false) {

                    return \Response::view('front._shared.no_support', ['type' => TYPE_I_MODE]);
                }
                $device = CHAR_I_MODE;
            } elseif (strpos($lowUa, 'au') !== false || strpos($lowUa, 'kddi') !== false || strpos($lowUa,
                    'celler') !== false
                || strpos($lowUa, 'j-phone') !== false || strpos($lowUa, 'vodafone') !== false || strpos($lowUa,
                    'softbank') !== false
            ) {

                return \Response::view('front._shared.no_support', ['type' => TYPE_OTHER]);
            }
        }

        return Response::View("maintenance",
        [
            'viewName' => 'maintenance',
            'userAgent' => $device
        ]);
    }
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/
require app_path().'/filters.php';
require app_path().'/validators.php';
require app_path().'/macros.php';
require app_path().'/viewcomposers.php';
