(function ( $ ) {
 
    $.fn.voteList = function( options ) {

        var date = new Date();
        var settings = $.extend({
            className : '',    // id of ul element
            numFirstPage : '', // num of question in the first load
            numOfPage : '',    // num of question in next pages when click "see more"
            pager: 2,          // pager
            url: '',            // call to server
            params : {},
            openDate : [date.getFullYear(), (date.getMonth() + 1), date.getDate()].join('-') + '_' + date.getHours() + '__' + date.getMinutes()
        }, options );

        return this.each(function() {
            if (settings.pager == 0) {
                settings.pager = 2;
            }
            var my = $(this);
            numQuestions = parseInt(settings.numFirstPage);
            $('.'+settings.className).hide();
            // for first load
            my.load(
                    settings.url,
                    {
                        'numQuestions' : numQuestions,
                        'className' : settings.className,
                        'params' : settings.params,
                        'date' : settings.openDate
                    },
                    function() {
                        if ($("li", '#'+settings.className).length >= numQuestions) {
                            if ($("li.last", '#'+settings.className).size() === 0) {
                                $('.'+settings.className).show();
                            } else {
                                $('.'+settings.className).hide();
                            }
                        }
                        for (var forI = 0; forI < (questionList == null ? 0 : questionList.length); forI++) {
                            if (!viewedList || viewedList.indexOf(questionList[forI]) === -1) {
                                if ($('#question' + questionList[forI])) {
                                    $('#question' + questionList[forI]).show();
                                }
                            }
                        }
                    }
                );
                        
            // for click btn moreAction
            $('body').on('click', '.'+settings.className, function() {
                var btnClick = $(this);
                if (btnClick.hasClass('nowLoading')) {
                    return false;
                }
                btnClick.addClass('nowLoading');
                $.ajax({
                    url : settings.url + '?page=' + settings.pager,
                    type: "POST",
                    DataType: "html",
                    data: { 
                            'numQuestions' : settings.numOfPage,
                            'className' : settings.className,
                            'pager' : settings.pager,
                            'params' : settings.params,
                            'date' : settings.openDate
                        },
                    success: function(data) {
                        var nextPos = $("li", '#'+settings.className).last().offset().top;
                        my.append(data);
                        if ($("li", '#'+settings.className).length < (settings.numOfPage * settings.pager)) {
                            btnClick.hide();
                        } else {
                            if ($("li.last", '#'+settings.className).size() === 0) {
                                btnClick.removeClass('nowLoading');
                            } else {
                                btnClick.hide();
                            }
                        }
                        for (var forI = 0; forI < (questionList == null ? 0 : questionList.length); forI++) {
                            if (!viewedList || viewedList.indexOf(questionList[forI]) === -1) {
                                if ($('#question' + questionList[forI])) {
                                    $('#question' + questionList[forI]).show();
                                }
                            }
                        }
                        settings.pager++;
                        //scroll page to button element
                        $('html, body').animate({ scrollTop: nextPos }, 500);
                    }
                });
                return false;
            });
        });
 
    };
 
}( jQuery ));
