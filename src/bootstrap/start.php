<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/

$app = new Illuminate\Foundation\Application;

/*
|--------------------------------------------------------------------------
| Detect The Application Environment
|--------------------------------------------------------------------------
|
| Laravel takes a dead simple approach to your application environments
| so you can just specify a machine name for the host that matches a
| given environment, then we will automatically detect it for you.
|
*/

// $env = $app->detectEnvironment(array(

//  'local' => array('*local*'),

// ));

// $env = $app->detectEnvironment(function () {
//     $hosts = array('codecept' => 'codecept');
//     if (isset($hosts[$_SERVER['SERVER_NAME']])) {
//         return $hosts[$_SERVER['SERVER_NAME']];
//     }
// });

$env = $app->detectEnvironment(function () {
    // // Check for a cookie to tell if we're running acceptance tests
    // $docomo_agent = ['DoCoMo/2.0 P903i(c100;TB;W24H12)', 'DoCoMo/1.0/N505i/c20/TB/W24H12'];
    // if (in_array($_SERVER['HTTP_USER_AGENT'], $docomo_agent))
    // {
    //     return 'codecept';
    // }
    // Check for a cookie to tell if we're running acceptance tests
});

/*
|--------------------------------------------------------------------------
| Bind Paths
|--------------------------------------------------------------------------
|
| Here we are binding the paths configured in paths.php to the app. You
| should not be changing these here. If you need to change these you
| may do so within the paths.php file and they will be bound here.
|
*/

$app->bindInstallPaths(require __DIR__ . '/paths.php');

/*
|--------------------------------------------------------------------------
| Load The Application
|--------------------------------------------------------------------------
|
| Here we will load this Illuminate application. We will keep this in a
| separate location so we can isolate the creation of an application
| from the actual running of the application with a given request.
|
*/

$framework = $app['path.base'] . '/vendor/laravel/framework/src';

require $framework . '/Illuminate/Foundation/start.php';

/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

return $app;
