<?php
$I = new AcceptanceTester\FrontChildGenreEntrySteps($scenario);
$I->wantTo('check list date');

$link_for_create = array(
				'create' => array('title' => '承認申請', 'message' => 'この内容で承認申請します。よろしいですか？'),
				'confirm' => '再度編集する',
				'edit' => array('title' => '承認・反映', 'message' => '本番に反映します。よろしいですか？'),
			);

$link_for_showmore = 'もっと見る';

$link_end_list = array(
					'title' => '未回答一覧',
					'url'	=> '/list/unanswered'
				);

//xpath format of list
$xpath_format = 'html/body/div[1]/ul/li[%d]/a';

///create 11 items
$items = array();

//create item for not display
$items[] = array(
			'id' => '',
			'date_from' => array('date_name' => 'vote_date_from_date',
									'date_xpath' => 'html/body/div[5]/table/tbody/tr[1]/td[6]/a',
									'hour_name' => 'vote_date_from_hour',
									'hour_value' => '00',
									'minute_name' => 'vote_date_from_min',
									'minute_value' => '00'
								),
			'date_to' => array('date_name' => 'vote_date_to_date',
									'date_xpath' => 'html/body/div[5]/table/tbody/tr[2]/td[4]/a',
									'hour_name' => 'vote_date_to_hour',
									'hour_value' => '00',
									'minute_name' => 'vote_date_to_min',
									'minute_value' => '00'
								),
			'title' => array('name' => 'title', 'value' => 'title_0'),
			'description' => array('name' => 'details', 'value' => 'description_0'),
			'choices' => array('name' => 'choiceTextarea', 'value' => 'choices_0'),
			);

//create 11 item for display
for ($i=1; $i <= 11; $i++) {
	$title = 'title_'.$i;
	$description = 'description_'.$i;
	$choices = 'choices_'.$i;
	$items[] = array(
			'id' => '',
			'date_from' => array('date_name' => 'vote_date_from_date',
									'date_xpath' => 'html/body/div[5]/table/tbody/tr[2]/td[4]/a',
									'hour_name' => 'vote_date_from_hour',
									'hour_value' => '00',
									'minute_name' => 'vote_date_from_min',
									'minute_value' => '00'
								),
			'date_to' => array('date_name' => 'vote_date_to_date',
									'date_xpath' => 'html/body/div[5]/table/tbody/tr[5]/td[4]/a',
									'hour_name' => 'vote_date_to_hour',
									'hour_value' => '00',
									'minute_name' => 'vote_date_to_min',
									'minute_value' => '00'
								),
			'title' => array('name' => 'title', 'value' => $title),
			'description' => array('name' => 'details', 'value' => $description),
			'choices' => array('name' => 'choiceTextarea', 'value' => $choices),
			);
}


$I->createSurveyForList($items, $link_for_create);


//when crate complete check with output items
$items_check = array();
for ($i=0; $i < 12; $i++) {

	$idate = strtotime(date('Y-m-d'));
	//$items[$key]['date_value'] = $I->dateToJpdate($value['date']);
	$items_check = array('title' => $items[$i]['title']['value'], 'sort' => $idate, 'status' => '1');
}

$items_check = $I->sortItems($items_check);

$I->checkListCommons($items_check, $xpath_format, 10);

//item 12 not display -> click showmore will see 11 item
$items_check[11]['status'] = '0';
$I-> checkListShowmoreCommons($link_for_showmore, $items_check, $xpath_format, 11);

//finish list dont see show more
$I->checkEndListCommons($link_for_showmore);
