<?php
namespace docomo\Models\Listeners;

class ErrorDisplay
{
    
    /**
     * @var [ErrorDisplay instance]
     */
    private static $instance;
    
    /**
     * [create single object ErrorDisplay]
     *
     * @return [ErrorDisplay] [instance of ErrorDisplay]
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    /**
     * [Function display all error]
     *
     * @param [Validation] $errors
     */
    
    public function DisplayAll($errors)
    {
        return \View::make('layouts.elements.displays.all_errors', compact("errors"));
    }
    
    /**
     * [Function display error by field]
     * @param [Validation] $errors
     * @param [string] $field  [input name of each Form]
     */
    
    public function DisplayIndividual($errors, $field)
    {
        return \View::make('layouts.elements.displays.field_errors', compact("errors", "field"));
    }
}
