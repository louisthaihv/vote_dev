<?php
$I = new AcceptanceTester($scenario);
$I->amOnPage('/manage');
$I->loginWithDetail(['admin', '123456']);
$I->click('アカウント一覧・登録・修正・削除');
$account_name = 'account_test_01';
$I->createAccount('account_test_01', 'account_test_01');

//ジャンル管理権限    ---- ジャンル一覧・登録
$I->choosePermission('account_test_01', './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[2]/label/input');
$I->verifyPermission('account_test_01', 'キーワード管理', 'account_test_01', './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[2]/label/input');

// キーワード管理権限   --- キーワード管理
$I->choosePermission($account_name, './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[3]/label/input');
$I->verifyPermission($account_name, 'ジャンル一覧・登録', 'account_test_01', './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[3]/label/input');

//アカウント管理権限              アカウント管理
$I->choosePermission($account_name, './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[4]/label/input');
$I->verifyPermission($account_name, 'ジャンル一覧・登録', 'account_test_01', './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[4]/label/input');

// //お知らせ管理権限                 お知らせ管理
$I->choosePermission($account_name, './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[5]/label/input');
$I->verifyPermission($account_name, 'ジャンル一覧・登録', 'account_test_01', './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[5]/label/input');

// //特別枠管理権限 ----------->no
$I->choosePermission($account_name, './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[6]/label/input');
$I->verifyPermission($account_name, 'ジャンル一覧・登録', 'account_test_01', './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[6]/label/input');

// //注目キーワード管理権限 -->TOPページ注目キーワード管理
$I->choosePermission($account_name, './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[7]/label/input');
$I->verifyPermission($account_name, 'ジャンル一覧・登録', 'account_test_01', './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[7]/label/input');

// //承認権限（即時反映）->no
$I->choosePermission($account_name, './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[8]/label');
$I->verifyPermission($account_name, 'ジャンル一覧・登録', 'account_test_01', './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[8]/label/input');

// //結果ページコメント入力権限 ->no
$I->choosePermission($account_name, './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[9]/label/input');
$I->verifyPermission($account_name, 'ジャンル一覧・登録', 'account_test_01', './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[9]/label/input');

// //コラム権限 ->no
$I->choosePermission($account_name, './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[10]/label/input');
$I->verifyPermission($account_name, 'ジャンル一覧・登録', 'account_test_01', './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[10]/label/input');

// //集計確認権限 ->集計(検討中)
$I->choosePermission($account_name, './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[11]/label/input');
$I->verifyPermission($account_name, 'ジャンル一覧・登録', 'account_test_01', './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[11]/label/input');

// //全アンケートの編集権限 ->アンケート管理
$I->choosePermission($account_name, './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[12]/label/input');
$I->verifyPermission($account_name, 'ジャンル一覧・登録', 'account_test_01', './/*[@id="form_account_edit"]/table/tbody/tr[5]/td/ul/li[12]/label/input');
