@extends('layouts.admin_in')
@section('content')
<h2>キーワード登録</h2>
@include('admin.keyword.form', ['url' => 'manage/keyword/create', 'method' => 'post', 'confirmMessage' => C_REGISTRATION, 'keyword' => $keyword, 'button' => REGISTRATION])
@stop