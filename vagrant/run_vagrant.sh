#!/bin/sh

vagrant box list | grep centos64

if [ "$?" -eq 1 ]
then
    # ない場合はbox追加
    vagrant box add centos64 https://github.com/2creatives/vagrant-centos/releases/download/v6.4.2/centos64-x86_64-20140116.box
else
    echo "already exists centos64 box"
fi

cd vagrant/

vagrant up

vagrant ssh
