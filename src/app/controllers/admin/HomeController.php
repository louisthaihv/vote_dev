<?php
namespace Admin;

use docomo\Models\Forms\Account\LoginForm;
use docomo\Models\Accounts\AccountLoggedInCommand;
use docomo\Models\Questions\Question;

class HomeController extends \BaseController
{
    
    /**
     *
     * @param LoginForm $loginForm
     */
    public function __construct(LoginForm $loginForm)
    {

        $this->loginForm = $loginForm;
    }
    
    /**
     * [return view when access admin.login]
     *
     * @return [View]
     */
    public function index()
    {
        
        if (\Auth::check()) {
            \Session::forget(FEATURE);
            \Session::forget(NOTICE);
            \Session::forget(QUESTION);
            return \View::make('admin.home', ['counts' => Question::getTopCount()]);
        } else {
            return \View::make("admin.login");
        }
    }
    
    /**
     * [Login function]
     *
     * @return [mixed]
     */
    public function login()
    {
        
        $data = \Input::all();
        $this->loginForm->validate($data);
        
        return $this->execute(AccountLoggedInCommand::class);
    }
    
    /**
     * [logOut function]
     * @return [Route]
     */
    public function logOut()
    {
        
        \Auth::logout();
        
        return \Redirect::route('admin.home');
    }
}
