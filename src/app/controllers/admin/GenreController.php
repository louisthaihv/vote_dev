<?php
namespace Admin;

use docomo\Models\Genres\ParentGenre;
use docomo\Models\Genres\ChildGenre;
use Illuminate\Support\MessageBag;

class GenreController extends \BaseController
{

    public function __construct()
    {
        
    }
    
    /**
     * [get form display all parent genre]
     *
     * @return [View] [return view admin.genre.index]
     */
    public function index()
    {

        $show_all = (\Input::get(SHOW_ALL) !== null);
        $genres = ParentGenre::getList($show_all);

        return \View::make('admin.genre.index', compact('genres', 'show_all'));
    }
    
    public function sort()
    {

        $errors = ParentGenre::sort();
        if ($errors->count() === 0) {
            ParentGenre::updateCacheTime(ParentGenre::$cacheName);
            ChildGenre::updateCacheTime(ChildGenre::$cacheName);

            return \Redirect::route('manage.genre.index');
        }

        $show_all = (\Input::get(SHOW_ALL) !== null);
        $genres = ParentGenre::getList($show_all);

        return \View::make('admin.genre.index', compact('genres', 'show_all', 'errors'));
    }
    
    /**
     * [get form create genre]
     *
     * @return [View] [return View admin.genre.create]
     */
    public function create()
    {

        return \View::make('admin.genre.create', ['genre' => new ParentGenre]);
    }
    
    /**
     * [process create genre]
     *
     * @return [Redirect] [Redirect to route admin.genre.edit]
     */
    public function store()
    {

        $genre = ParentGenre::fillWithModify();

        if ($genre->save())
        {
            ParentGenre::updateCacheTime(ParentGenre::$cacheName);
            ChildGenre::updateCacheTime(ChildGenre::$cacheName);

            ParentGenre::sort();

            return \Redirect::route('manage.genre.edit', $genre->id);
        }
        else
        {

            return \View::make('admin.genre.create', ['genre' => $genre, 'errors' => $genre->errors()]);
        }
    }
    
    /**
     * [access page admin.genre.edit]
     *
     * @param  [string] $genre_dir [category_directory_name of parentGenre]
     * @param  [int] $flag      [determined display all child genre or only genre visible]
     * @return [View]            [admin.genre.edit]
     */
    public function edit($id)
    {

        $show_all   = (\Input::get(SHOW_ALL) !== null);
        $genre = ParentGenre::findOrFail($id);
        $children = ChildGenre::getDataByParentId($id, $show_all);
        return \View::make('admin.genre.edit',
            [
                'genre' => $genre,
                'show_all'=> $show_all,
                'add_child' => new ChildGenre,
                'children' => $children
            ]);
    }
    
    /**
     * [postEdit description]
     *
     * @param  [string] $genre_dir [category_directory_name of parentGenre]
     * @return [Funtion ]            [return function editParentGenre, addChildGenre, addChildGenre]
     */
    public function update($id)
    {
        if (\Input::get('genre_parent_name', null) != null)
        {

            return $this->updateParent($id);
        }
        else if (\Input::get('add', null) == true)
        {

            return $this->createChild($id);
        }
        else
        {

            return $this->updateChild($id);
        }
    }
    
    /**
     * [implementation edit parent genre]
     *
     * @return [Redirect] [Redirect to route admin.genre.edit]
     */
    public function updateParent($id)
    {

        $genre = ParentGenre::fillWithModify($id);

        if ($genre->save())
        {
            ParentGenre::sortAll();
            ParentGenre::updateCacheTime(ParentGenre::$cacheName);
            ChildGenre::updateCacheTime(ChildGenre::$cacheName);

            return \Redirect::route('manage.genre.edit', $id);
        }
        else
        {

            return \View::make('admin.genre.edit',
                [
                    'genre' => $genre,
                    'add_child' => new ChildGenre,
                    'children' => $genre->children,
                    'show_all' => null
                ]);
        }
    }
    
    /**
     * [implementation add child genre]
     *
     * @return [Redirect] [Redirect to route admin.genre.edit]
     */
    public function createChild($id)
    {

        $child = ChildGenre::fillWithModify(null, $id);

        if ($child->save())
        {

            ChildGenre::sort($id);
            ParentGenre::updateCacheTime(ParentGenre::$cacheName);
            ChildGenre::updateCacheTime(ChildGenre::$cacheName);

            return \Redirect::route('manage.genre.edit', $id);
        }
        else
        {
            $genre = ParentGenre::findOrFail($id);

            return \View::make('admin.genre.edit',
                [
                    'genre' => $genre,
                    'add_child' => $child,
                    'children' => $genre->children,
                    'show_all' => null
                ]);
        }
    }
    
    /**
     * [implementation edit child genre]
     *
     * @return [Redirect] [Redirect to route admin.genre.edit]
     */
    public function updateChild($id)
    {

        $children = ChildGenre::fillWithModifyForList($id);
        $errors = new MessageBag;

        $original_messages = ChildGenre::$customMessages;
        $row = 1;
        foreach ($children as $child)
        {
            ChildGenre::$customMessages = [];

            foreach ($original_messages as $key => $value)
            {
                ChildGenre::$customMessages[$key] = $row . '行目の' . $value;
            }

            $updater = ChildGenre::find($child->id);
            unset($updater->genre_child_directory);
            $updater->fill($child->toArray());

            if (!$updater->save())
            {
                $errors->merge($updater->errors());
            }
            $row ++;
        }

        if ($errors->count() == 0)
        {
            ChildGenre::sort($id);
            ParentGenre::updateCacheTime(ParentGenre::$cacheName);
            ChildGenre::updateCacheTime(ChildGenre::$cacheName);

            return \Redirect::route('manage.genre.edit', $id);
        }
        else
        {
            $genre = ParentGenre::findOrFail($id);
            $children = ChildGenre::getDataByParentId($id);

            return \View::make('admin.genre.edit',
                [
                    'genre' => $genre,
                    'add_child' => new ChildGenre(),
                    'children' => $children,
                    'show_all' => null,
                    'errors' => $errors
                ]);
        }
    }
}
