<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFeatureQuestions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasColumn('feature_questions', 'index')) {
			Schema::table('feature_questions', function ($table) {
				$table->integer('index')->default(0);
			});
		}
		\DB::statement('CREATE UNIQUE INDEX idx_users_uid ON users (uid)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
