<?php
namespace docomo\Models\Features;

class FeatureQuestion extends \BaseArdentModel
{
    protected $table = 'feature_questions';

    protected $primaryKey = 'feature_id';

    public $timestamps = false;

    public static $rules = [
        'question_id' => ['max:99999999', 'numeric']
    ];

    public static $customMessages = [
        'question_id.max'       => W_FRM_MAX_QUESTION_ID,
        'question_id.numeric'   => W_FRM_FORMAT_QUESTION_ID
    ];
}
