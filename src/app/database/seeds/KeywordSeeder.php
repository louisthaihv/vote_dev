<?php

class KeywordSeeder extends Seeder
{
    public function run()
    {
        DB::table('keywords')->truncate();

        DB::table('keywords')->insert([
        [
            'keyword_name' => 'Keyword 1',
            'is_visible' => VISIBLE
        ],[
            'keyword_name' => 'Keyword 2',
            'is_visible' => VISIBLE
        ],[
            'keyword_name' => 'Keyword 3',
            'is_visible' => VISIBLE
        ],[
            'keyword_name' => 'Keyword 4',
            'is_visible' => VISIBLE
        ],[
            'keyword_name' => 'Keyword 5',
            'is_visible' => VISIBLE
        ],[
            'keyword_name' => 'Keyword 6',
            'is_visible' => VISIBLE
        ],[
            'keyword_name' => 'Keyword 7',
            'is_visible' => VISIBLE
        ],[
            'keyword_name' => 'Keyword 8',
            'is_visible' => VISIBLE
        ],[
            'keyword_name' => 'Keyword 9',
            'is_visible' => VISIBLE
        ],[
            'keyword_name' => 'Keyword 10',
            'is_visible' => VISIBLE
        ],[
            'keyword_name' => 'Keyword 11',
            'is_visible' => VISIBLE
        ],[
            'keyword_name' => 'Keyword 12',
            'is_visible' => VISIBLE
        ],[
            'keyword_name' => 'Keyword 13',
            'is_visible' => VISIBLE
        ],[
            'keyword_name' => 'Keyword 14',
            'is_visible' => VISIBLE
        ],[
            'keyword_name' => 'Keyword 15',
            'is_visible' => VISIBLE
        ],[
            'keyword_name' => 'Keyword 16',
            'is_visible' => VISIBLE
        ],[
            'keyword_name' => 'Keyword 17',
            'is_visible' => VISIBLE
        ]]);
    }
}
