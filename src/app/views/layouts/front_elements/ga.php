<?php
$cid = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
// 32 bits for "time_low"
mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
// 16 bits for "time_mid"
mt_rand( 0, 0xffff ),
// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 4
mt_rand( 0, 0x0fff ) | 0x4000,
// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
mt_rand( 0, 0x3fff ) | 0x8000,
// 48 bits for "node"
mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
);

$data['v']   = '1';
$data['tid'] = 'UA-46697670-15'; // UAのコード
//$data['tid'] = 'UA-57404340-1';
$data['uid'] = \Input::get('uid');
$data['cid'] = '00000000-0000-0000-0000-' . $data['uid'];
$data['t']   = 'pageview';
$data['dh']  = \Request::server('HTTP_HOST');;
$data['dp']  = \Request::path();
$data['dt']  = 'みんなの声'; // ページのタイトル
$data['dl']     = '';
$data['dr']     = '';


$content = http_build_query($data); // The body of the post must include exactly 1 URI encoded payload and must be no longer than 8192 bytes. See http_build_query.
$content = utf8_encode($content); // The payload must be UTF-8 encoded.
$user_agent = $_SERVER['HTTP_USER_AGENT']; 

$url = 'http://www.google-analytics.com/collect'; // This is the URL to which we'll be sending the post request.
$url .= '?v='.$data['v'];
$url .= '&tid='.$data['tid'];
$url .= '&cid='.$data['cid'];
$url .= '&uid='.$data['uid'];
$url .= '&t='.$data['t'];
$url .= '&dh='.$data['dh'];
$url .= '&dl='.$data['dl'];
$url .= '&dr='.$data['dr'];
$url .= '&dp='.$data['dp'];
$url .= '&dt='.$data['dt'];
?>
<img src="<?php echo $url;?>">
