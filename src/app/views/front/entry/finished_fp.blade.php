@extends('layouts.front_fp')

@section('content')
@include('layouts.front_elements._space')
<div><img src="/common/front/images/fp/tit1.gif" width="100%" alt="投票"/></div>
@include('layouts.front_elements._space')
<div style="background-color:#fff6ab;padding:5px;color:#3e3a39;"> {{ $question->title }}</div>
@include('layouts.front_elements._space')
<div style="margin:0px 10px 0px 10px;">
    <div style="margin:0px 0px 3px 0px;" class="link">{{ $question->details }}</div>
    @include('layouts.front_elements._space')
    <div style="text-align:center">このｱﾝｹｰﾄは締め切りました。</div>
    @include('layouts.front_elements._space')
    <div style="text-align:center">{{ link_to_route('result.show', 'ｱﾝｹｰﾄの結果を見る', [$question->getParentGenreDirectory(), $question->getChildGenreDirectory(), $question->id], ['class' => 'btnType02']) }}</div>
</div>
@include('layouts.front_elements._space')
@include('layouts.front_elements._profileBn_fp')
@include('layouts.front_elements._space')
@include('front.entry.fp_bottom_contents')
@stop