<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableChoice extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('navigations')) {
			Schema::table('navigations', function ($table) {
				$table->dropColumn('thumbnail');
			});
			Schema::table('navigations', function ($table) {
				$table->string('thumbnail', 255)->nullable()->after('row');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
