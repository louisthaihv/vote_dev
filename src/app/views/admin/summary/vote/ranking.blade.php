@extends('layouts.admin_in')

@section('head')
<link rel="stylesheet" href="{{ url('common/admin/css/jquery-ui-1.8.23.custom.css') }}" type="text/css" />
<script type="text/javascript" src=" {{ url('common/admin/js/jquery-ui-1.8.23.custom.min.js') }}"></script>
<script type="text/javascript" src=" {{ url('common/admin/js/jquery.ui.datepicker-ja.js') }}"></script>
@stop

@section('javascript')
$(window).load(function(){

    //日付ピッカー
    $("input[name='date_from']").datepicker({
        dateFormat: 'yymmdd'
    });
    //日付ピッカー
    $("input[name='date_to']").datepicker({
        dateFormat: 'yymmdd'
    });

    $('button#search').click(function(){
        $('#mode').val('search');
        return true;
    });
    $('button#download').click(function(){
        $('#mode').val('download');
        document.forms[0].submit();
    });
});

function setOrder(column) {
    if ($('#sortColumn').val() == column) {
        if ($('#sortOrder').val() == 'asc') {
            $('#sortOrder').val('desc');
        } else {
            $('#sortOrder').val('asc');
        }
    } else {
        $('#sortColumn').val(column);
        $('#sortOrder').val('asc');
    }
    $('#mode').val('');
    document.forms[0].submit();
    return false;
}
@stop

@section('style')
@stop

@section('content')
<h2>得票数ランキング</h2>

{{ Form::model('conditions', ['method' => 'post']) }}
{{ Form::hidden('sortColumn', $conditions->sort_column, ['id' => 'sortColumn']) }}
{{ Form::hidden('sortOrder', $conditions->sort_order, ['id' => 'sortOrder']) }}
<input type="hidden" id="mode" name="mode">
<table class="searchQuery">
    <tr>
        <th>ジャンル</th>
        <td>
            {{ Form::selectGenre('genre', $genres, $conditions->genre) }}
        </td>
    </tr>
    <tr>
        <th>投票日</th>
        <td>
            {{ Form::text('date_from', $conditions->date_from) }}
            ～
            {{ Form::text('date_to', $conditions->date_to) }}
        </td>
    </tr>
</table>
<p class="m-margin tcenter">
<button type="submit" id="search" class="commitBtn">検索</button>
<button type="button" id="download" class="commitBtn">CSV</button>
</p>
{{ Form::close() }}
@if (isset($summaryData) && !is_null($summaryData))
<div id="resultBox">
    <p class="m-margin">ヒット数：{{ number_format($summaryData->count()) }}件</p>
<table class="list">
    <tr>
        <th>ID</th>
        <th>タイトル</th>
        <th>選択肢数</th>
        <th>小ジャンル名</th>
        <th><a href="#" onclick="return setOrder('from');">受付開始{{ ($conditions->sort_column == 'from') ? (($conditions->sort_order == 'asc') ? '▲' : '▼') : '' }}</a></th>
        <th><a href="#" onclick="return setOrder('to');">受付終了{{ ($conditions->sort_column == 'to') ? (($conditions->sort_order == 'asc') ? '▲' : '▼') : '' }}</a></th>
        <th>ステータス</th>
        <th><a href="#" onclick="return setOrder('vote');">票数{{ ($conditions->sort_column == 'vote') ? (($conditions->sort_order == 'asc') ? '▲' : '▼') : '' }}</a></th>
        @if (Auth::user()->havePermission('is_visible_register'))
            <th>登録者</th>
        @endif
        <th>表示</th>
        <th>結果</th>
    </tr>
    @foreach ($summaryData as $question)
        <tr>
            <td>{{ $question->id }}</td>
            <td>{{{ $question->title }}}</td>
            <td>{{ $question->getChoiceCount() }}</td>
            <td>{{{ $question->getChildGenreName() }}}</td>
            <td>{{ date('Y-m-d H:i', strtotime($question->vote_date_from)) }}</td>
            <td>{{ date('Y-m-d H:i', strtotime($question->vote_date_to)) }}</td>
            <td>{{ docomo\Models\Codes\QuestionStatus::$label[$question->status] }}</td>
            <td>{{ number_format($question->vote_count) }}</td>
            @if (Auth::user()->havePermission('is_visible_register'))
                <td>{{{ $question->getAccountName() }}}</td>
            @endif
            <td>{{ docomo\Models\Codes\QuestionViewable::$label[$question->viewable_type] }}</td>
            <td>{{ ($question->isViewableResult()) ? link_to_route('manage.question.result', '結果', $question->id) : '&nbsp;' }}</td>
        </tr>
    @endforeach
</table>
</div>
@endif

@stop