<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableUserAttributes extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_user_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('attribute_id');
            $table->string('attribute_value', 60);
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_user_attributes');
    }
}
