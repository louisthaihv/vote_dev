<?php
$I = new AcceptanceTester\GenreSteps($scenario);
$I->wantTo('check FP genre');

$genre_parent = array(
                    1 => array(
                                'genre_parent_name' => 'genre_1',
                                'parent_is_visible' => '1',
                                'genre_parent_directory' => 1,
                                'parent_sort_order' => '10',
                                'name' => 'genre_1',
                    			'status' => '1',
                    			'childs' => '',
                                ),
                    2 => array(
                                'genre_parent_name' => 'genre_2',
                                'parent_is_visible' => '1',
                                'genre_parent_directory' => 2,
                                'parent_sort_order' => '10',
                                'name' => 'genre_2',
                    			'status' => '1',
                    			'childs' => '',
                                ),
                    3 => array(
                                'genre_parent_name' => 'genre_3',
                                'parent_is_visible' => '0',
                                'genre_parent_directory' => 3,
                                'parent_sort_order' => '10',
                                'name' => 'genre_3',
                    			'status' => '0',
                    			'childs' => '',
                                )
                );
$I->createDbGenreParent($genre_parent);

$genre_child = array(
                    1 => array(
                                'genre_parent_id'=>'1',
                                'genre_child_name'=>'genre_child_1_1',
                                'child_is_visible'=>'1',
                                'genre_child_directory'=>1,
                                'child_sort_order'=>'10',
                                'name' => 'genre_child_1_1',
                    			'status' => '1',
                                ),
                    2 => array(
                                'genre_parent_id'=>'1',
                                'genre_child_name'=>'genre_child_1_2',
                                'child_is_visible'=>'1',
                                'genre_child_directory'=>2,
                                'child_sort_order'=>'20',
                                'name' => 'genre_child_1_2',
                    			'status' => '1',
                                ),
                    3 => array(
                                'genre_parent_id'=>'2',
                                'genre_child_name'=>'genre_child_2_1',
                                'child_is_visible'=>'1',
                                'genre_child_directory'=>3,
                                'child_sort_order'=>'10',
                                'name' => 'genre_child_2_1',
                    			'status' => '1',
                                ),
                    4 => array(
                                'genre_parent_id'=>'2',
                                'genre_child_name'=>'genre_child_2_2',
                                'child_is_visible'=>'1',
                                'genre_child_directory'=>4,
                                'child_sort_order'=>'20',
                                'name' => 'genre_child_2_2',
                    			'status' => '1',
                                ),
                    5 => array(
                                'genre_parent_id'=>'3',
                                'genre_child_name'=>'genre_child_3_1',
                                'child_is_visible'=>'1',
                                'genre_child_directory'=>5,
                                'child_sort_order'=>'10',
                                'name' => 'genre_child_3_1',
                    			'status' => '1',
                                ),
                    6 => array(
                                'genre_parent_id'=>'3',
                                'genre_child_name'=>'genre_child_3_2',
                                'child_is_visible'=>'1',
                                'genre_child_directory'=>6,
                                'child_sort_order'=>'20',
                                'name' => 'genre_child_3_2',
                    			'status' => '0',
                                ),
                );

$I->createDbGenreChild($genre_child);


$genre_parent[1]['childs'] = array($genre_child[1],$genre_child[2]);
$genre_parent[2]['childs'] = array($genre_child[3],$genre_child[4]);
$genre_parent[3]['childs'] = array($genre_child[5],$genre_child[6]);

//check genre list ~ fp_09
$I->checkFPGenreList($genre_parent);


//create survey entry
$survies_entry = array();
for ($i=1; $i <= 25; $i++) {
    $title = 'title_entry_'.$i;
    $date = '2014-12-'.$i.' 00:00:00';
    $survies_entry[$i] = array(
                        'genre_parent_id'=>'1',
                        'genre_child_id'=>'1',
                        'status'=>'5',
                        'title'=>$title,
                        'vote_date_from'=>$date,
                        'vote_date_to'=>'2015-03-10 00:00:00',
                        'baseline_date'=>$date,
                        'viewable_type'=>'1',
                        'visible_device_i'=>'1',
                        'visible_device_d'=>'1',
                        'summary_type'=>'1',
                        'average_unit'=>'1',
                        'is_visible_other'=>'1',
                        'shuffle_choice'=>'1',
                        'navigation_title'=>'1',
                        'account_id'=>'1',
                        );
}
$I->createDbQuestion($survies_entry);

//order list to see
krsort($survies_entry);
$survies_entry_check = array();
foreach ($survies_entry as $survey) {
    $survies_entry_check[] = array('title' => $survey['title'], 'status' => '1');
}

$arr_entry = array();
$arr_entry['items'] = $survies_entry_check;
$arr_entry['item_per_page'] = 10;
$arr_entry['xpath'] = 'html/body/div[10]/div[%d]/a';

//create survey result
$survies_result = array();
for ($i=1; $i <= 25; $i++) {
    $title = 'title_result_'.$i;
    $date = '2014-12-'.$i.' 00:00:00';
    $survies_result[$i] = array(
                        'genre_parent_id'=>'1',
                        'genre_child_id'=>'1',
                        'status'=>'6',
                        'title'=>$title,
                        'vote_date_from'=>'2014-11-01 00:00:00',
                        'vote_date_to'=>$date,
                        'baseline_date'=>$date,
                        'viewable_type'=>'1',
                        'visible_device_i'=>'1',
                        'visible_device_d'=>'1',
                        'summary_type'=>'1',
                        'average_unit'=>'1',
                        'is_visible_other'=>'1',
                        'shuffle_choice'=>'1',
                        'navigation_title'=>'1',
                        'account_id'=>'1',
                        );
}
$I->createDbQuestion($survies_result);

//order list to see
krsort($survies_result);
$survies_result_check = array();
foreach ($survies_result as $survey) {
    $survies_result_check[] = array('title' => $survey['title'], 'status' => '1');
}

$arr_result = array();
$arr_result['items'] = $survies_result_check;
$arr_result['item_per_page'] = 10;
$arr_result['xpath'] = 'html/body/div[10]/div[%d]/a';

//check survey by genre result ~ FP_07_1
$I->click($genre_child[1]['genre_child_name']);
$link_switch = array('title' => '受付中はこちら', 'url' => '/list/genre/1/1/entry');
$I->checkFPListSurveyByGenre($link_switch, $arr_result);

//chekc survey by genre entry ~ FP_07
$I->click($link_switch['title']);
$I->wait(3);
$link_switch = array('title' => '結果発表はこちら', 'url' => '/list/genre/1/1/result');
$I->checkFPListSurveyByGenre($link_switch, $arr_entry);

