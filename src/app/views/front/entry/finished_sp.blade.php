@extends('layouts.front')

@section('metaTag')
    <title>{{ $question->title }} | みんなの声</title>
    <meta name="description" content="{{{ $question->description }}}">
    <meta property="og:title" content="{{{ $question->title }}} | みんなの声" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="{{{ $question->description }}}" />
    <meta name="twitter:title" content="{{{ $question->title }}} | みんなの声" />
    <meta name="twitter:description" content="{{{ $question->description }}}" />
@stop

@section('style')
{{ HTML::style('common/front/css/entry.css') }}
{{ HTML::style('common/front/css/entry_additional.css') }}
@stop

@section('content')
<section class="l-margin">
    <h2 class="entryTit m-margin"><span>投票</span></h2>
    <div class="lrMargin">
        <div class="questionInfoBox">
            <h1>{{ $question->title }}</h1>
            <div class="questionText">{{ $question->details }}</div>
        </div>

        <div class="tcenter l-margin">
            <p>このアンケートは締め切りました</p>

        </div>
        <p class="tcenter m-margin">
            {{ link_to_route('result.show', 'アンケートの結果を見る', [$question->getParentGenreDirectory(), $question->getChildGenreDirectory(), $question->id], ['class' => 'btnType02']) }}
        </p>

    </div>
</section>
@include('layouts.front_elements._profileBn')

@stop