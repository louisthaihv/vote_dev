@extends('layouts.front_fp')

@section('content')
    @include('layouts.front_elements._space')
<div>{{ HTML::image('common/front/images/fp/tit1.gif', '投票', array('width' => '100%')) }}</div>
    @include('layouts.front_elements._space')
<div style="background-color:#fff6ab;padding:5px;">{{{ $question->title }}}</div>
@include('layouts.front_elements._space')
<div style="margin:0px 10px 0px 10px;">
<div style="margin:0px 0px 3px 0px;" class="link">{{ $question->details }}</div>
    @include('layouts.front_elements._space')
@if (is_null($question->attribute_id) || $currentUser->attrList[$question->attribute_id] == $question->attribute_value_id)
{{ Form::open(['url' => $question->getPath(), 'method' => 'post']) }}
    @foreach ($choices as $choice)
<div style="margin:0px 0px 3px 0px;" class="link">
{{ Form::radio('choice_id', $choice->id) }} {{{ $choice->choice_text }}}
</div>
    @endforeach
<div style="text-align:center"><input type="submit" value="回答"></div>
{{ Form::hidden('uid', $currentUser->uid, ['name' => 'uid']) }}
{{ Form::close() }}
@elseif (!$currentUser->has($question->attribute_id))
<div style="margin:0px 10px 7px 10px; background-color:#eeeeee;padding:10px;text-align:center;">
このｱﾝｹｰﾄは、<br />{{{ $question->getLimitedText() }}}限定です。<br />ﾌﾟﾛﾌｨｰﾙに<br>{{{ $question->getLimitedLabel() }}}を登録しますか？
@if (isset($currentUser->result) && !$currentUser->result)
@include('layouts.front_elements._space')
<div style="text-align:center;color:#CC0000">生年月日が正しくありません。</div>
@endif
@include('layouts.front_elements._space')
{{ Form::open(['url' => $question->getPath(), 'method' => 'put']) }}
@if ($question->attribute_id == ATTR_BIRTHDAY)
{{ Form::birthday((isset($currentUser) ? $currentUser->birthday : null)) }}
@include('layouts.front_elements._space')
@else
{{ Form::attributeValueList($question->attribute_id, array('default' => $question->attribute_value_id), $question->getLimited()->attribute_directory) }}
@include('layouts.front_elements._space')
@endif
<div style="text-align:center"><input type="submit" value="登録する"></div>
{{ Form::hidden('uid', $currentUser->uid, ['name' => 'uid']) }}
{{ Form::close() }}
</div>
@elseif ($currentUser->attrList[$question->attribute_id] != $question->attribute_value_id)
<div style="margin:0px 10px 7px 10px; background-color:#eeeeee;padding:10px;text-align:center;">
このｱﾝｹｰﾄは、<br />{{{ $question->getLimitedText() }}}限定です。<br />登録さている{{{ $question->getLimitedLabel() }}}：{{{ $currentUser->getAttributeValueName($question->attribute_id) }}}
</div>
@endif
@include('layouts.front_elements._space')
<div style="text-align:center">{{ link_to($question->getPostPath() . '?uid=NULLGWDOCOMO&showresult=1', '回答せずに結果を見る') }}</div>
<div style="text-align:center">{{ link_to_route('list.unanswered', '他のｱﾝｹｰﾄを見る', ['uid' => 'NULLGWDOCOMO']) }}</div>
</div>
@include('layouts.front_elements._space')
@include('layouts.front_elements._profileBn_fp')
@include('layouts.front_elements._space')
@include('front.entry.fp_bottom_contents')
@endsection
