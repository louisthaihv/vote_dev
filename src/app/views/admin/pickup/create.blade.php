@extends('layouts.admin_in')

@section('content')
<h2>TOPページピックアップデータ作成</h2>
@include('admin.pickup.form', ['url' => '/manage/pickup/create/' . $question->id, 'method' => 'post', 'button' => '登録'])
@stop

