<?php

/**
 * [get user agent follow device]
 *
 * @return [string] [name of platform]
 */
function getUserAgent()
{
    return \docomo\Models\Users\User::$userDevice;
}

function printSelectTag($number, $selected, $step = 1)
{
    $html = '';
    if ($number < 0) {
        return;
    } else {
        for ($i = 0; $i < $number; $i = $i + $step) {
            if ($i < 10) {
                $i = '0' . $i;
            }
            if ($i == $selected) {
                $html.= '<option selected>' . $i . '</option>';
            } else {
                $html.= '<option>' . $i . '</option>';
            }
        }
    }
    return $html;
}

function convertWeightOrder($arr)
{
    if (!is_array($arr)) {
        return;
    }
    
    $sortOrder = 0;
    asort($arr);
    foreach ($arr as $key => $value) {
        if (!isset($preValue) || $preValue != $value) {
            $sortOrder+= DISTANCE_SORT;
        }
        $arr[$key] = $sortOrder;
        $preValue = $value;
    }
    return $arr;
}

function verifyPermissionCheckbox($permission)
{
    return $permission == ENABLED ? 'checked="checked"' : '';
}
/**
 * [Convert string to date time format]
 * 
 * @param  [string] $str    [string time]
 * @param  [string] $format [string format default 'Y-m-d H:i:s']
 * @return [string]         [format time]
 */
function convertString2Datetime($str, $format = 'Y-m-d H:i:s')
{
    $date = new Datetime($str);
    return $date->format($format);
}
/**
 * [printValue Print value]
 * @param  [array] $arrays 
 * @param  [int] $key    [input index]
 * @return [string]         [text of index]
 */
function printValue($arrays, $key)
{
    foreach ($arrays as $index => $value) {
        if ($key == $index) {
            return $value;
        }
    }
}

/**
 * [isSelectedTag description]
 * @param  [int]  $value  [value]
 * @param  [int]  $define [define]
 * @return boolean         [true or false]
 */
function isSelectedTag($value, $define)
{
    return ($value == $define) ? 'selected' : '';
}

/**
 * [iso_date convert string to iso Y/m/d H:i]
 * @param  [string]     $str [description]
 * @return [type]       [iso Y/m/d H:i]
 */

function iso_datetime($str)
{
    $stamp = strtotime($str);

    return date('Y/m/d H:i', $stamp);
}

function dateWithWeek($dateStr) {
    $weekday = [ '日', '月', '火', '水', '木', '金', '土' ];

    $date = strtotime($dateStr);

    return date('Y-m-d', $date) . '(' . $weekday[date('w', $date)] . ')';
}

function checked($val1, $val2)
{
    return $val1 == $val2 ? 'checked' : '';
}

function selected($val1, $val2)
{
    return $val1 == $val2 ? 'selected' : '';
}

function jp_date($str) 
{
    $stamp = strtotime($str);

    return date('Y', $stamp) .'年'. date('m', $stamp) . '月' . date('d', $stamp). '日';
}

/**
 * [iso_date convert string to iso Y-m-d]
 * @param  [string] $str [string time]
 * @return [date]        [iso Y-m-d]
 */

function iso_date($str = null)
{
    $stamp = strtotime($str);

    return date('Y-m-d', $stamp);
}

/**
 * [iso_hour convert string to hour]
 * @param  [string]     $str [description]
 * @return [type]       [iso H
 */

function iso_hour($str)
{
    $stamp = strtotime($str);

    return date('H', $stamp);
}

/**
 * [iso_date convert string to minute]
 * @param  [string]     $str [description]
 * @return [type]       [iso i]
 */

function iso_minute($str)
{
    $stamp = strtotime($str);

    return date('i', $stamp);
}

/**
 * [processTimeString process string date, hour, minute to Date H:m:s]
 * @param  [string] $date   [date]
 * @param  [string] $hour   [hour]
 * @param  [string] $minute [minute]
 * @return [type]         [description]
 */

/**
 * [processTimeString process string date, hour, minute to Date H:m:s]
 * @param  [string] $date   [date]
 * @param  [string] $hour   [hour]
 * @param  [string] $minute [minute]
 * @return [type]         [description]
 */
function processTimeString($date, $hour, $minute)
{
 
    return $date.' '. $hour . ':' . $minute . ':' . '00';
}

function getObjectOldInput($dataOldForm, $fieldBoleans = [])
{
    if(!empty($arrayBoleans)) {
        foreach ($fieldBoleans as $filed)
        $dataOldForm[$filed] = array_key_exists($filed, $dataOldForm) ? VISIBLE : HIDDEN ;
    }
    $dataOldForm['visible_date_from'] = processTimeString($dataOldForm['visible_date_from'], $dataOldForm['hour_from'], $dataOldForm['minute_from']);
    $dataOldForm['visible_date_to'] = processTimeString($dataOldForm['visible_date_to'], $dataOldForm['hour_to'], $dataOldForm['minute_to']);
    $oldInput = json_decode(json_encode($dataOldForm), FALSE);
    return $oldInput;
}

function platform_dependent_characters_filter($text) {

    mb_regex_encoding('UTF-8');

    $pdc = '①②③④⑤⑥⑦⑧⑨⑩⑪⑫⑬⑭⑮⑯⑰⑱⑲⑳ⅠⅡⅢⅣⅤⅥⅦⅧⅨⅩ' .
        '㍉㌔㌢㍍㌘㌧㌃㌶㍑㍗㌍㌦㌣㌫㍊㌻㎜㎝㎞㎎㎏㏄㎡㍻〝〟№㏍℡㊤' .
        '㊥㊦㊧㊨㈱㈲㈹㍾㍽㍼∮∑∟⊿纊褜鍈銈蓜俉炻昱棈鋹曻彅丨仡仼伀' .
        '伃伹佖侒侊侚侔俍偀倢俿倞偆偰偂傔僴僘兊兤冝冾凬刕劜劦勀勛匀匇' .
        '匤卲厓厲叝﨎咜咊咩哿喆坙坥垬埈埇﨏塚增墲夋奓奛奝奣妤妺孖寀甯' .
        '寘寬尞岦岺峵崧嵓﨑嵂嵭嶸嶹巐弡弴彧德忞恝悅悊惞惕愠惲愑愷愰憘' .
        '戓抦揵摠撝擎敎昀昕昻昉昮昞昤晥晗晙晴晳暙暠暲暿曺朎朗杦枻桒柀' .
        '栁桄棏﨓楨﨔榘槢樰橫橆橳橾櫢櫤毖氿汜沆汯泚洄涇浯涖涬淏淸淲淼' .
        '渹湜渧渼溿澈澵濵瀅瀇瀨炅炫焏焄煜煆煇凞燁燾犱犾猤猪獷玽珉珖珣' .
        '珒琇珵琦琪琩琮瑢璉璟甁畯皂皜皞皛皦益睆劯砡硎硤硺礰礼神祥禔福' .
        '禛竑竧靖竫箞精絈絜綷綠緖繒罇羡羽茁荢荿菇菶葈蒴蕓蕙蕫﨟薰蘒﨡' .
        '蠇裵訒訷詹誧誾諟諸諶譓譿賰賴贒赶﨣軏﨤逸遧郞都鄕鄧釚釗釞釭釮' .
        '釤釥鈆鈐鈊鈺鉀鈼鉎鉙鉑鈹鉧銧鉷鉸鋧鋗鋙鋐﨧鋕鋠鋓錥錡鋻﨨錞鋿' .
        '錝錂鍰鍗鎤鏆鏞鏸鐱鑅鑈閒隆﨩隝隯霳霻靃靍靏靑靕顗顥飯飼餧館馞' .
        '驎髙髜魵魲鮏鮱鮻鰀鵰鵫鶴鸙黑ⅰⅱⅲⅳⅴⅵⅶⅷⅸⅹ￤＇＂∵';

    $pdc_array = Array();
    $pdc_text = str_replace(array("\r\n","\n","\r"), '', $text);

    while($iLen = mb_strlen($pdc, 'UTF-8')) {
        array_push($pdc_array, mb_substr($pdc, 0, 1, 'UTF-8'));
        $pdc = mb_substr($pdc, 1, $iLen, 'UTF-8');
    }

    foreach($pdc_array as $value) {
        if(preg_match("/(" . $value . ")/", $pdc_text)) {
            return true;
            break;
        }
    }
    return false;
}

function NotFound() {

    if (strpos($_REQUEST['q'], '/manage') === 0) {
        return '404 - Not Found';
    }

    $ua = $_SERVER['HTTP_USER_AGENT'];
    if (strpos($ua, 'Android') === false && strpos($ua, 'FBIOS') === false) {
        $lowUa = strtolower($ua);
        if (strpos($lowUa, 'docomo') !== false) {
            $devices = '/' . \Config::get('app.devices') . '/';
            if (preg_match($devices, $ua) == false) {

                return \Response::view('front._shared.no_support', ['type' => TYPE_I_MODE]);
            }
            \docomo\Models\Users\User::$userDevice = CHAR_I_MODE;
        } elseif (strpos($lowUa, 'au') !== false || strpos($lowUa, 'kddi') !== false || strpos($lowUa,
                'celler') !== false
            || strpos($lowUa, 'j-phone') !== false || strpos($lowUa, 'vodafone') !== false || strpos($lowUa,
                'softbank') !== false
        ) {

            return \Response::view('front._shared.no_support', ['type' => TYPE_OTHER]);
        }
    }

    return \View::make('front._shared.notfound',
        [
            'viewName' => 'front._shared.notfound',
            'userAgent' => getUserAgent(),
            'currentUser' => new \docomo\Models\Users\User()
        ]);
}