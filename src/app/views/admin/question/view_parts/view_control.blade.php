    <p>掲載コントロール</p>
    <table class="formTable">
        <tr>
            <th>アンケート受付期間<span class="require">（必須）</span></th>
            <td>
                {{ substr($question->vote_date_from, 0, 16) }}&nbsp;&nbsp;～&nbsp;&nbsp; {{ substr($question->vote_date_to, 0, 16) }}
            </td>
        </tr>
        <tr>
            <th>表示デバイス</th>
            <td>
                <label>{{ Form::checkbox('visible_device_i', null, ($question->visible_device_i == VISIBLE)) }} i版で表示</label>　
                <label>{{ Form::checkbox('visible_device_d', null, ($question->visible_device_d == VISIBLE)) }} d版で表示</label>
            </td>
        </tr>
        <tr>
            <th>表示</th>
            <td>
                {{ Form::select('viewable_type', \docomo\Models\Codes\QuestionViewable::$label, $question->visible_flag) }}
            </td>
        </tr>
    </table>
