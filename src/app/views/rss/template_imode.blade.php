<?xml version="1.0" encoding="UTF-8"?>
<rdf:RDF
        xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
        xmlns:dc="http://purl.org/dc/elements/1.1/"
        xmlns:admin="http://webns.net/mvcb/"
        xmlns:content="http://purl.org/rss/1.0/modules/content/"
        xmlns="http://purl.org/rss/1.0/">
<channel rdf:about="RSSのURL">
    <title><![CDATA[{{ $rssTitle }}]]></title>
    <link>{{ $siteURL }}</link>
    <description><![CDATA[{{ $siteDescription }}]]></description>
    <dc:date>{{ date_format($lastUpdate, 'c') }}</dc:date>
    <dc:language>ja</dc:language>
    <items>
        <rdf:Seq>
            @foreach ($recordData as &$record)
            <rdf:li rdf:resource="{{ $siteURL . $record->url }}" />
            @endforeach
        </rdf:Seq>
    </items>
</channel>
@foreach ($recordData as &$record)
<item rdf:about="{{ $siteURL . $record->url }}">
    <title><![CDATA[{{ $record->title }}]]></title>
    <link>{{ $siteURL . $record->url }}</link>
    <description><![CDATA[{{ $record->description }}]]></description>
    @if ($record->getRSSGenre() !== 'except')<dc:subject>{{ $record->getRSSGenre() }}</dc:subject>
    @endif
    <dc:date>{{ date_format($record->date, 'c')}}</dc:date>
</item>
@endforeach
</rdf:RDF>