<?php

namespace docomo\Models\Listeners;

use Events\EventListener;
use docomo\Models\Keywords\KeywordWasAdded;
use docomo\Models\Keywords\AccountWasLogined;

class EmailNotifier extends EventListener
{
    
    public function whenUserWasRegistered(UserWasRegistered $event)
    {
        
        // dd('Send confirmation email about event: ' . $event->user->username);
        
    }
    
    public function whenUserDetailWasUpdated(UserDetailWasUpdated $event)
    {
        dd('Send confirmation email about user detail: ' . $event->user->username);
    }
    
    public function whenUserPasswordWasChanged(UserPasswordWasChanged $event)
    {
        dd('Send confirmation email about password change of user: ' . $event->user->username);
    }
    public function whenUserWasLogined(UserWasLogined $event)
    {
        
        //dd("Was Login");
        
    }
    
    public function whenKeywordWasAdded(KeywordWasAdded $event)
    {
        dd('Keyword Added is ' . $event->keyword->keyword_name);
    }
    
    public function whenAccountWasLogined(AccountWasLogined $event)
    {
        dd('Logined');
    }
}
