@extends('layouts.admin_in')

@section('head')
<link rel="stylesheet" href="{{ url('common/admin/css/jquery-ui-1.8.23.custom.css') }}" type="text/css" />
<script type="text/javascript" src=" {{ url('common/admin/js/jquery-ui-1.8.23.custom.min.js') }}"></script>
<script type="text/javascript" src=" {{ url('common/admin/js/jquery.ui.datepicker-ja.js') }}"></script>
@stop

@section('javascript')
$(window).load(function(){

    //日付ピッカー
    $("input[name='date_from']").datepicker({
        dateFormat: 'yymmdd'
    });
    //日付ピッカー
    $("input[name='date_to']").datepicker({
        dateFormat: 'yymmdd'
    });

    $('button#search').click(function(){
        $('#mode').val('search');
        return true;
    });
    $('button#download').click(function(){
        $('#mode').val('download');
        document.forms[0].submit();
    });
});
@stop

@section('style')
@stop

@section('content')
<h2>日別得票数</h2>

{{ Form::model('conditions', ['method' => 'post']) }}
<input type="hidden" id="mode" name="mode">
<table class="searchQuery">
    <tr>
        <th>投票日</th>
        <td>
            {{ Form::text('date_from', $conditions->date_from) }}
            ～
            {{ Form::text('date_to', $conditions->date_to) }}
        </td>
    </tr>
</table>
<p class="m-margin tcenter">
<button type="submit" id="search" class="commitBtn">検索</button>
<button type="button" id="download" class="commitBtn">CSV</button>
</p>
{{ Form::close() }}
@if (isset($summaryData) && !is_null($summaryData))
<div id="resultBox">
<p class="m-margin">合計：{{ number_format($head->total) }} （i版：{{ number_format($head->fp) }}）（d版：{{ number_format($head->sp) }}）</p>
<table class="list">
    <tr>
        <th>日</th>
        <th>得票数</th>
        <th>i版</th>
        <th>d版</th>
    </tr>
    @foreach ($summaryData as &$record)
    <tr>
        <td>{{ dateWithWeek($record->summary_date) }}</td>
        <td>{{ number_format($record->total) }}</td>
        <td>{{ number_format($record->fp) }}</td>
        <td>{{ number_format($record->sp) }}</td>
    </tr>
    @endforeach
</table>
</div>
@endif

@stop