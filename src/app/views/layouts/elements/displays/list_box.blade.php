<li><a id="{{ isset($linkId) ? $linkId : '' }}" href="{{ $question->getPath() }}">
<div class="iconArea">{{ (isset($show_entry_tag) && $show_entry_tag && $question->status == QUESTION_STATUS_READY) ? '<span class="icon">受付中</span>' : '' }}{{ ((isset($hide_answer) || !$question->isAnswered($currentUser->id)) ? '' : '<span class="icon">回答済み</span>') }}{{ (isset($showNotView)) ? '<span class="icon" id="question' . $question->id . '" style="display:none;">未読</span>' : '' }}{{ (isset($showHasColumn) && ($question->has_column == VISIBLE) ? '<span class="icon">コラムあり</span>' : '') }}</div>
<span class="text">{{{ $question->title }}}</span>
<span class="date">{{{ jpDate($question->baseline_date, 'Y年m月d日 H:i') }}}</span>
</a></li>