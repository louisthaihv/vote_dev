yum erase -y mysql-libs
yum -y install mysql56*
mkdir /var/run/mysqld
mysql_install_db --user=mysql
mysqld_safe --user=mysql &

chkconfig mysqld on
