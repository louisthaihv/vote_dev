<?php
namespace docomo\Models\Pickups;

use docomo\Models\Codes\FrontTab;
use docomo\Models\Questions\Question;

class Pickup extends \BaseArdentModel
{
    /*
     * Model relationships
     */
    public function question()
    {
        return $this->belongsTo('docomo\Models\Questions\Question');
    }

    /*
     * Model scopes
     */
    public function scopeNewer($query)
    {
        return $query->orderBy('from', 'desc')->orderBy('question_id', 'desc');
    }

    /*
     * Acceptable params from input
     */
    protected $fillable = [
        'question_id',
        'type'
    ];

    /*
     * Validation rules
     */
    public static $rules = [
        'question_id' => 'required',
        'type' => 'required',
        'from' => ['required', 'date', 'datetime_format:Y-m-d H:i'],
        'to' => ['required', 'date', 'after:from', 'datetime_format:Y-m-d H:i'],
    ];

    /*
     * Custom validation messages
     */
    public static $customMessages = array(
        'from.required'     => W_FRM_REQUIRED_DATE_FROM,
        'from.date'         => W_FRM_FORMAT_DATE_FROM,
        'from.datetime_format'  => W_FRM_LENGTH_DATE_FROM,
        'to.required'       => W_FRM_REQUIRED_DATE_TO,
        'to.date'           => W_FRM_FORMAT_DATE_TO,
        'to.after'          => W_FRM_COMPLEX_DATE_TO_AFTER_FROM,
        'to.datetime_format'    => W_FRM_LENGTH_DATE_TO,
    );

    public static function getQuestionList($type = PROCESSING, $user, $ignoreQuestion) {
        $ignoreId = (!is_null($ignoreQuestion) && isset($ignoreQuestion->id)) ? $ignoreQuestion->id : -1;
        $now = date('Y-m-d H:i');

        $pickups = Pickup::whereRaw('`from` <= ? AND ? < `to`', [$now, $now])->where('question_id', '<>', $ignoreId)->whereType($type)->selectRaw('SQL_CACHE `question_id`')->get();
        if (isset($pickups) && $pickups->count() > 0) {
            $pickups = $pickups->lists('question_id');
        } else {
            $pickups = [-1];
        }
        $options = [];
        $options['id'] = $pickups;
        $questions = Question::fieldList()
            ->baseViewable(
                ($type == PROCESSING) ? QUESTION_STATUS_READY : QUESTION_STATUS_FINISH,
                null,
                $user->getDevice(),
                $user,
                DISPLAY,
                $options
            )
            ->unanswered($user)->get();

        return $questions;
    }
}