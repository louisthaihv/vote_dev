@extends('layouts.admin_in')

@section('content')
<h2>TOPページピックアップデータ作成</h2>
@include('admin.pickup.form', ['url' => ['manage/pickup/' . $pickup->id . '/edit'], 'method' => 'put', 'button' => '更新'])
@stop

