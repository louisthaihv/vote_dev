@extends('layouts.front')

@section('head')
	@yield('head')
@stop('head')

@section('script')
	@yield('script')
@stop

@section('style')
	@yield('style')
@stop

@section('content')
	@yield('content')
@stop

@section('profile')
    @if((Route::currentRouteName() != 'home.top') and (Route::currentRouteName() != 'list.mypage') and (Route::currentRouteName() != 'profile')
         and (Route::currentRouteName() != 'notice.detail') and (Route::currentRouteName() != 'survey.result.total') )
        @include('layouts.front_elements._profileBn')
    @endif
@stop
