<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('check footer');
$arr_create_genre = array(
            'parent_is_visible'=>'0',
            'genre_parent_name'=>'genre_parent_name',
            'genre_parent_directory'=>'genre_parent_directory',
            'genre_child_name'=>'genre_child_name',
            'genre_child_directory'=>'genre_child_directory',
            'genre_child_description'=>'genre_child_description',
            'child_sort_order'=>'child_sort_order',
            'child_is_visible'=>'0',
            );

$arr_genre_parent=array(
                    'name'=>'日常生活',
                    'parent_is_visible' => 1,
                    'x_path' => 'html/body/section[2]/div/ul/li[1]/p/span',
                    );
$arr_genre_child = array(
                    1=>array(
                            'name'=>'学校生活、職場生活',
                            'child_is_visible' =>1,
                            'link' =>'#',
                            ),
                    2=>array(
                            'name'=>'ご当地ジョーシキ',
                            'child_is_visible' =>1,
                            'link' =>'#',
                            ),
                    3=>array(
                            'name'=>'知りたいみんなのアノ事情',
                            'child_is_visible' =>1,
                            'link' =>'#',
                            ),
                    4=>array(
                            'name'=>'わたしは○○派',
                            'child_is_visible' =>1,
                            'link' =>'#',
                            ),
                    5=>array(
                            'name'=>'恋愛にまつわるエトセトラ',
                            'child_is_visible' =>1,
                            'link' =>'#',
                            ),
                    6=>array(
                            'name'=>'ガマンの境界',
                            'child_is_visible' =>1,
                            'link' =>'#',
                            ),
                    7=>array(
                            'name'=>'直感二択！',
                            'child_is_visible' =>1,
                            'link' =>'#',
                            ),
                    );
// $I->amOnPage('/manage');
// $I->createGenreTrue($arr_create_genre);
$I->amOnPage('/');
$I->checkMoveMouse('keyword_test_1', 'KEYWORD_SEARCH');
$I->checkSearchFooterKeyword('keyword_test_1', 'KEYWORD_SEARCH');
$I->checkMoveMouse('keyword_test_1', 'search_key');
$arr = array(
            '操作説明'=> '/help',
            'プロフィール設定'=> '/profile',
            'dメニューTOP'=> 'smt.docomo.ne.jp',
            );
$I->commonHeaderFooterCheckLink($arr, null);
$I->checkSPGenreList($arr_genre_parent, $arr_genre_child);

