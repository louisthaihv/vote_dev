    <p>掲載コントロール</p>
    <table class="formTable">
        <tr>
            <th>アンケート受付期間<span class="require">（必須）</span></th>
            <td>
                {{ Form::dateTimeControl('vote_date_from', $question->vote_date_from, 10) }}
                <br />
                <br />
                ～<br />
                <br />
                {{ Form::dateTimeControl('vote_date_to', $question->vote_date_to, 10) }}
            </td>
        </tr>
        <tr>
            <th>表示デバイス</th>
            <td>
                <label>{{ Form::checkbox('visible_device_i', $question->visible_device_i) }} i版で表示</label>　
                <label>{{ Form::checkbox('visible_device_d', $question->visible_device_d) }} d版で表示</label>
            </td>
        </tr>
        <tr>
            <th>表示</th>
            <td>
                {{ Form::select('viewable_type', \docomo\Models\Codes\QuestionViewable::$label, $question->viewable_type) }}
            </td>
        </tr>
    </table>
