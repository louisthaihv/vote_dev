@extends('layouts.front_in')

@section('metaTag')
    <title>未回答一覧 | みんなの声</title>
    <meta name="description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！">
    <meta property="og:title" content="未回答一覧 | みんなの声" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
    <meta name="twitter:title" content="未回答一覧 | みんなの声" />
    <meta name="twitter:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
@stop

@section('script')
    @include('front._shared.js_load_more_pages', ['classNames'=>['noAnswer'], 'nums' => [NUM_FIRST_PAGE], 'pages' => [PAGER_SIZE_FRONT]])
@endsection

@section('content')
    <section class="l-margin">
        {{--List Title --}}
        <h2 class="entryListTit"><span>未回答アンケート</span></h2>
        {{--List questions --}}
        @include('front._shared._question_list', ['className' => 'noAnswer', 'displaySeeMore' => TRUE])
        {{-- redirect to other list --}}
        @include('front._shared._link_to_list', ['text' => '受付中のアンケートをすべて見る', 'route' => 'list.entry'])
        @include('layouts.front_elements._profileBn')
    </section>
@endsection
