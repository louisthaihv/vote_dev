<?php
namespace docomo\Models\Questions;

use Illuminate\Support\Facades\Input;

class Navigation extends \BaseArdentModel
{
    protected $table = 'navigations';

    //protected $primaryKey = ['question_id', 'view_type', 'row'];

    /*
     * Validation rules
     */
    public static $rules = [
        'description' => ['required', 'max:60', 'deny_tag', 'deny_char'],
        'url' => ['required', 'url', 'max:255']
    ];

    /*
     * Custom validation messages
     */
    public static $customMessages = array(
        'description.required'      => W_FRM_REQUIRED_NAVIGATION_DESCRIPTION,
        'description.max'           => W_FRM_MAX_NAVIGATION_DESCRIPTION,
        'description.deny_tag'      => W_FRM_DENY_TAG_NAVIGATION_DESCRIPTION,
        'description.deny_char'     => W_FRM_DENY_CHAR_NAVIGATION_DESCRIPTION,
        'url.required'              => W_FRM_REQUIRED_URL,
        'url.url'                   => W_FRM_FORMAT_NAVIGATION_URL,
        'url.max'                   => W_FRM_MAX_NAVIGATION_URL
    );

    public function __construct(array $attributes = array())
    {

        parent::__construct($attributes);
    }

    public static function scopeWhereQuestionIdAndType($query, $question_id, $type)
    {

        return $query->whereQuestionId($question_id)->whereViewType($type);
    }

    public static function getList($question_id = null)
    {

        $navies = [];
        $navies += [TYPE_I_MODE => [new Navigation, new Navigation, new Navigation, new Navigation, new Navigation]];
        $navies += [TYPE_D_MENU => [new Navigation, new Navigation, new Navigation, new Navigation, new Navigation]];
        foreach ($navies as $navi) {
            for ($i = 0; $i < count($navi); $i++) {
                if (!isset($navi[$i]->row)) {
                    $navi[$i]->row = $i + 1;
                }
            }
        }
        if ($question_id !== null)
        {
            $data = Navigation::whereQuestionId($question_id)->orderBy('row')->get();
            foreach ($data as $record)
            {
                $navies[$record->view_type][$record->row - 1] = $record;
            }
        }

        return $navies;
    }

    public static function fillWithModify($question, $session_navigation = null)
    {

        $navigations = [TYPE_D_MENU => [], TYPE_I_MODE => []];
        if (isset($question->id)) {
            $db_d_navigations = Navigation::whereQuestionIdAndType($question->id, TYPE_D_MENU)->get();
            $db_i_navigations = Navigation::whereQuestionIdAndType($question->id, TYPE_I_MODE)->get();
        }
        else
        {
            $db_d_navigations = [];
            $db_i_navigations = [];
        }
        $d_navigations = [];
        foreach (Input::get('navigation_d_description') as $key => $value)
        {
            $navigation = null;
            foreach ($db_d_navigations as $db_navigation) {
                if ($db_navigation->row == $key) {
                    $navigation = $db_navigation;
                    $navigation->is_exists = true;
                    break;
                }
            }
            if (is_null($navigation)) {
                $navigation = new Navigation;
                $navigation->row = $key;
                $navigation->question_id = $question->id;
            }
            if (isset($session_navigation) && count($session_navigation) > 0 &&
                count($session_navigation[TYPE_D_MENU]) > 0 && isset($session_navigation[TYPE_D_MENU][$key]->thumbnail)) {
                $navigation->thumbnail = $session_navigation[TYPE_D_MENU][$key]->thumbnail;
            }
            $navigation->setValues(TYPE_D_MENU, $key, $value, Input::get('navigation_d_url')[$key], $navigation->thumbnail);

            $d_navigations[$key] = $navigation;
        }
        $i_navigations = [];
        foreach (Input::get('navigation_i_description') as $key => $value)
        {
            $navigation = null;
            foreach ($db_i_navigations as $db_navigation) {
                if ($db_navigation->row == $key) {
                    $navigation = $db_navigation;
                    $navigation->is_exists = true;
                    break;
                }
            }
            if (is_null($navigation)) {
                $navigation = new Navigation;
                $navigation->row = $key;
                $navigation->question_id = $question->id;
            }
            $navigation->setValues(TYPE_I_MODE, $key, $value, Input::get('navigation_i_url')[$key]);
            $i_navigations[$key] = $navigation;
        }
        $navigations[TYPE_D_MENU] = $d_navigations;
        $navigations[TYPE_I_MODE] = $i_navigations;

        return $navigations;
    }

    public function setValues($view_type, $row, $description, $url, $thumbnail = null)
    {
        $this->view_type = $view_type;
        $this->row = $row;
        $this->description = $description;
        $this->url = $url;
        if ($view_type == TYPE_I_MODE) {
            return;
        }
        if (isset($thumbnail) && !empty($thumbnail)) {
            if (\Input::get('navigation_d_thumbnail_remove')[$row] != true) {
                $this->thumbnail = $thumbnail;
            } else {
                $this->thumbnail = '';
            }
        }
        if (\Input::hasFile('navigation_d_thumbnail_' . $row))
        {
            $uploaded_file = \Input::file('navigation_d_thumbnail_' . $row);
            if ($uploaded_file->getSize() > MAX_UPLOAD_FILE_SIZE) {
                $this->errors()->add('thumbnail.error', '送客(d版) ' . $row . '行目> ' .W_FRM_MAXSIZE_THUMBNAIL);
            }
            $extension = $uploaded_file->getClientOriginalExtension();
            if (!$this->isAllowExtension($extension)) {
                $this->errors()->add('thumbnail.error', '送客(d版) ' . $row . '行目> ' .W_FRM_EXTENSION_THUMBNAIL);
            }
            if ($this->errors()->count() === 0) {
                $this->thumbnail = 'navigation_' . $row . (new \DateTime())->format('YmdHis') . '.' . $extension;
                $uploaded_file->move(public_path() . PATH_TO_NAVIGATION_TEMP, $this->thumbnail);
                $this->thumbnail = PATH_TO_NAVIGATION_TEMP . $this->thumbnail;
            } else {
                $this->thumbnail = null;
            }
        }
    }

    public function isSaveTarget()
    {

        return (!empty($this->description) || !empty($this->url) || !empty($this->thumbnail) || ($this->errors()->count() > 0));
    }

    /**
     * [moveFile Move file from temp to thumbnail]
     * @return [type] [description]
     */
    public function saveFile()
    {
        if (!empty($this->thumbnail) && !stristr($this->thumbnail, PATH_TO_NAVIGATION_THUMBNAIL)) {
            $temp_path = $this->thumbnail;
            $this->thumbnail = str_replace(PATH_TO_NAVIGATION_TEMP, PATH_TO_NAVIGATION_THUMBNAIL, $this->thumbnail);
            \File::move(public_path() . $temp_path, public_path() . $this->thumbnail);
        }
    }

    public function saveRecord()
    {
        $result = true;
        $this->saveFile();
        if (isset($this->is_exists)) {
            Navigation::whereQuestionIdAndType($this->question_id, $this->view_type)->whereRow($this->row)
                ->delete();
        }

        $originalMessages = Navigation::$customMessages;
        $labels = [TYPE_D_MENU => 'd版', TYPE_I_MODE => 'i版'];
        if ($this->isSaveTarget()) {

            Navigation::$customMessages = [];

            foreach ($originalMessages as $key => $value)
            {
                Navigation::$customMessages[$key] = '送客(' . $labels[$this->view_type]  . ') '. $this->row . '行目> ' . $value;
            }

            $navigation = new Navigation;
            $navigation->question_id    = $this->question_id;
            $navigation->view_type      = $this->view_type;
            $navigation->row            = $this->row;
            $navigation->thumbnail      = $this->thumbnail;
            $navigation->description    = $this->description;
            $navigation->url            = $this->url;

            $result = $navigation->save();

            $this->errors()->merge($navigation->errors());
        }
        if ($this->errors()->count() > 0) {
            $result = false;
        }

        Navigation::$customMessages = $originalMessages;

        return $result;
    }
}
