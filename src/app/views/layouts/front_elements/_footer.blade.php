@include('layouts.front_elements.footer_commercial_tag')
<footer>
    <ul>
        @if (!isset($question) || empty($question->back_button_text))
            <li><a id="footer_top" href="{{ url('/') }}">トップ</a></li>
        @else
            <li><a id="footer_top" href="{{ $question->back_button_url }}">{{ $question->back_button_text }}</a></li>
        @endif
        <li><a id="footer_manual" href="{{ url('help') }}">みんなの声について</a></li>
        <li><a id="footer_prof" href="{{ url('profile')}}">プロフィール設定</a></li>
    </ul>
    <ul>
        <li><a id="footer_about" href="http://smt.docomo.ne.jp/portal/src/terms.html">サイトご利用にあたって</a></li>
        <li><a id="footer_dtop" href="http://smt.docomo.ne.jp/">dメニューTOP</a></li>
    </ul>

    <address>(c)NTT DOCOMO</address>
</footer>

<a href="#" id="menuOpenLayer"></a>

@include('layouts.front_elements.footer_commercial_script')
