<?php 
require('tests/functional/AdminCommon.php');
$I->amOnRoute('admin.survey.create');
$I->seeLink('メインメニュー',$home);
$I->see('アンケート登録');
$I->see('掲載コントロール');
$I->see('アンケート受付期間','//tr/th');
$I->see('表示デバイス','//tr/th');
$I->see(' i版で表示','//tr/td');
$I->see('表示','//tr/th');
