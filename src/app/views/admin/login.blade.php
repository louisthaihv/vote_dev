<!DOCTYPE HTML>
<html lang="ja">
    <head>
@include('layouts.elements.partials.header') 

<style>
#loginBox{
    width:500px;
    border:1px solid #ccc;
    -webkit-border-radius:5px;
    border-radius:5px;
    margin:150px auto;
    background-color:#fff;
    padding:20px;
}
#loginBox .loginTit{
    font-size:20px;
    margin:20px 0px;
    text-align:center;
}
#loginBox dl{
    margin:20px auto;
    width:150px;
}
#loginBox dl dd{
    margin-bottom:10px;
}
</style>
</head>
<body>
{{ Form::open(["action"=>"Admin\HomeController@login", "id" => "form_login", 'autocomplete' => 'off']) }}
<div id="loginBox">
<p class="loginTit">みんなの声　管理画面</p>
@include('admin.share.errors', ['errorList' => $errors])
<dl>
@include('layouts.modal.modal')
<dt>ログインID</dt>
<dd><input type="text" name="login_id" value="" /></dd>
<dt>パスワード</dt>
<dd><input type="password" name="login_password" value="" autocomplete="off" /></dd>
</dl>

<p class="tcenter"><button type="submit" name="loginCommit" class="commitBtn">ログイン</button></p>
<!--/loginBox--></div>
{{ Form::close() }}
</body>
</html>