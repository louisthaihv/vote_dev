<?php
namespace docomo\Models\Genres;

class ChildGenre extends \BaseArdentModel
{

    protected $table = 'genre_children';
    public static $childGenreData = null;
    public static $ignoreData = null;

    public static $rules = [
        'genre_child_name' => ['sometimes', 'required', 'max:60', 'deny_tag', 'deny_char'],
        'genre_child_directory' => ['sometimes', 'required', 'directory', 'unique:genre_children', 'max:60'],
        'genre_child_description' => ['sometimes', 'required', 'max:60', 'deny_tag', 'deny_char'],
        'child_sort_order' => ['sometimes', 'numeric', 'max:10000']
    ];

    public static $customMessages = [
        'genre_child_name.required' => W_FRM_REQUIRED_CHILD_NAME,
        'genre_child_name.max' => W_FRM_MAX_CHILD_NAME,
        'genre_child_name.deny_tag' => W_FRM_DENY_TAG_CHILD_NAME,
        'genre_child_name.deny_char' => W_FRM_DENY_CHAR_CHILD_NAME,
        'genre_child_directory.required' => W_FRM_REQUIRED_DIRECTORY,
        'genre_child_directory.directory' => W_FRM_ALPHANUM_DIRECTORY,
        'genre_child_directory.unique' => W_FRM_UNIQUE_CHILD_DIRECTORY,
        'genre_child_directory.max' => W_FRM_MAX_DIRECTORY,
        'genre_child_description.required' => W_FRM_REQUIRED_CHILD_DESCRIPTION,
        'genre_child_description.max' => W_FRM_MAX_CHILD_DESCRIPTION,
        'genre_child_description.deny_tag' => W_FRM_DENY_TAG_CHILD_DESCRIPTION,
        'genre_child_description.deny_char' => W_FRM_DENY_CHAR_CHILD_DESCRIPTION,
        'child_sort_order.numeric' => W_FRM_FORMAT_SORT_ORDER,
        'child_sort_order.max' => W_FRM_MAX_SORT_ORDER
    ];

    protected $fillable = [
        'genre_child_name',
        'genre_child_directory',
        'genre_child_description',
        'child_sort_order',
        'child_is_visible'
    ];

    public static $sort_step = 10;

    public static $cacheName = 'ChildGenre';
    public static $ignoreCache = 'IgnoreChildGenre';

    public function questions()
    {
        return $this->hasMany('docomo\Models\Questions\Question', 'genre_child_id');
    }

    public function parentGenre()
    {

        return $this->belongsTo('docomo\Models\Genres\ParentGenre', 'genre_parent_id');
    }

    public function scopeVisible($query)
    {
        return $query->where('child_is_visible', '=', VISIBLE);
    }

    public function scopeWhereParent($query, $genre_parent_id)
    {

        return $query->where('genre_parent_id', '=', $genre_parent_id);
    }

    public function scopeWhereParentDirectory($query, $genreParentDirectory) {

        $query
            ->whereGenreParentId(ParentGenre::select('id')->whereGenreParentDirectory($genreParentDirectory)->first()->id);
    }

    public function scopeOrderBySort($query)
    {

        return $query->orderBy('child_sort_order');
    }

    public function scopeOrderBySortWithId($query)
    {

        return $query->orderBySort()->orderBy('id');
    }

    public function scopeSelectDetails($query)
    {

        return $query->select('id', 'genre_parent_id', 'genre_child_name', 'genre_child_directory', 'genre_child_description', 'child_sort_order', 'child_is_visible');
    }

    public static function getIdListByParent($genre_parent_id)
    {

        return ChildGenre::whereParent($genre_parent_id)->orderBySort()->get(['id']);
    }

    public static function getDataByParentId($genre_parent_id, $show_all = false)
    {

        if ($show_all) {
            return ChildGenre::whereParent($genre_parent_id)->orderBySort()->selectDetails()->get();
        }
        else
        {
            return ChildGenre::whereParent($genre_parent_id)->visible()->orderBySort()->selectDetails()->get();
        }
    }

    public static function fillWithModifyForList($id)
    {

        $children = array();

        $children_list = ChildGenre::getIdListByParent($id);

        foreach ($children_list as $child)
        {
            $child = ChildGenre::fillWithModify($child->id);
            if (!is_null($child)) {
                $children[] =$child;
            }
        }

        return $children;
    }

    public static function fillWithModify($id = null, $parent_genre_id = null)
    {
        $child = null;
        $data = null;
        $inputData= \Input::all();

        if (!array_key_exists('genre_child_name_' . $id, $inputData) &&
            !array_key_exists('genre_child_name', $inputData)) {
            return null;
        }

        if ($id === null)
        {
            $child = new ChildGenre;
            $child->fill(\Input::all());

            $child->child_is_visible = \Input::has('child_is_visible') ? HIDDEN : VISIBLE;
            $child->genre_parent_id = $parent_genre_id;
        }
        else
        {
            $child = ChildGenre::selectDetails()->findOrFail($id);

            if ($child->genre_child_name != \Input::get('genre_child_name_' . $id)) {
                $child->genre_child_name = \Input::get('genre_child_name_' . $id);
            } else {
                unset($child->genre_child_name);
            }

            if ($child->genre_child_directory != \Input::get('genre_child_directory_' . $id)) {
                $child->genre_child_directory = \Input::get('genre_child_directory_' . $id);
            } else {
                unset($child->genre_child_directory);
            }

            if ($child->genre_child_description != \Input::get('genre_child_description_' . $id)) {
                $child->genre_child_description = \Input::get('genre_child_description_' . $id);
            } else {
                unset($child->genre_child_description);
            }

            $child->child_sort_order = \Input::get('child_sort_order_' . $id);
            $child->child_is_visible = (\Input::has('child_is_visible_' . $id)) ? HIDDEN : VISIBLE;
            $parent_genre_id = $child->genre_parent_id;
        }

        if (is_null($child->child_sort_order) || $child->child_sort_order == '')
        {
            $child->child_sort_order = ChildGenre::maxSortInParentId($child->genre_parent_id) + ChildGenre::$sort_step;
        }

        ChildGenre::$rules['genre_child_directory'][3] = 'unique:genre_children,genre_child_directory,NULL,id,genre_parent_id,' . $parent_genre_id;

        return $child;
    }

    public static function maxSortInParentId($genre_parent_id)
    {

        return ChildGenre::whereParent($genre_parent_id)->visible()->max('child_sort_order');
    }

    public static function setCustomMessages($original_messages, $record_number)
    {

    }

    public static function sort($genre_parent_id)
    {

        $list = ChildGenre::select('id', 'child_sort_order')->whereParent($genre_parent_id)->visible()->orderBySortWithId()->get();

        $sort_order = ChildGenre::$sort_step;
        $result = true;
        foreach ($list as &$child_genre)
        {

            $child_genre->child_sort_order = $sort_order;
            $sort_order += ChildGenre::$sort_step;
            if (!$child_genre->save()) {
                $result = false;
            }
        }

        unset($child_genre);

        return $result;
    }

    public static function getParentId($genre_child_id)
    {

        return ChildGenre::whereId($genre_child_id)->max('genre_parent_id');
    }

    public static function getDataByCache($genre_parent_id = null) {
        if (is_null(ChildGenre::$childGenreData)) {
            $updateTime = ChildGenre::checkCache(ChildGenre::$cacheName);

            ChildGenre::setMemoryByCache($updateTime);
        }

        if (is_null($genre_parent_id)) {

            return ChildGenre::$childGenreData;
        } else {
            if (array_key_exists($genre_parent_id, ChildGenre::$childGenreData)) {

                return ChildGenre::$childGenreData[$genre_parent_id];
            } else {

                return [];
            }
        }
    }

    public static function getIgnoreByCache() {
        if (is_null(ChildGenre::$ignoreData)) {
            $updateTime = ChildGenre::checkCache(ChildGenre::$cacheName);

            ChildGenre::setMemoryByCache($updateTime);
        }

        return ChildGenre::$ignoreData;
    }

    public static function setDataToCache($updateTime)
    {
        $data = ChildGenre::select('id', 'genre_parent_id', 'genre_child_name', 'genre_child_description', 'child_is_visible', 'genre_child_directory', 'record_count')
            ->whereRaw('`id` > 1')
            ->orderBy('child_sort_order')->get();
        $setData = [];
        $ignoreData= [];
        foreach ($data as &$record) {
            if ($record->child_is_visible) {
                // 表示
                if (!array_key_exists($record->genre_parent_id, $setData)) {
                    $setData[$record->genre_parent_id] = [];
                }
                $setData[$record->genre_parent_id][$record->id] = $record;
            } else {
                // 非表示
                $ignoreData[] = $record->id;
            }
        }
        \Cache::put(ChildGenre::$cacheName, $setData, DEFAULT_CACHE_TIME);
        ChildGenre::setCacheTime(ChildGenre::$cacheName, $updateTime);

        \Cache::put(ChildGenre::$ignoreCache, $ignoreData, DEFAULT_CACHE_TIME);

        ChildGenre::setMemoryByCache($updateTime);

        return $setData;
    }

    public static function getIgnoreList() {
        if (is_null(ChildGenre::$ignoreData)) {
            $updateTime = ChildGenre::checkCache(ChildGenre::$cacheName);

            ChildGenre::setMemoryByCache($updateTime);
        }

        return ChildGenre::$ignoreData;
    }

    public static function setMemoryByCache($updateTime) {
        ChildGenre::$childGenreData = \Cache::get(ChildGenre::$cacheName, function () use($updateTime) {

            return ChildGenre::setDataToCache($updateTime);
        });

        ChildGenre::$ignoreData = \Cache::get(ChildGenre::$ignoreCache, []);
    }
}
