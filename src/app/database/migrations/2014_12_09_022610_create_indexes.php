<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// indexes
		if (Schema::hasTable('keywords'))
		{
			Schema::table('keywords', function($table) {
				$table->index('is_visible');
			});
		}
		if (Schema::hasTable('accounts'))
		{
			Schema::table('accounts', function($table) {
				$table->index('is_enabled');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('keywords'))
		{
			Schema::table('keywords', function($table) {
				$table->dropIndex('keywords_is_visible_index');
			});
		}
		if (Schema::hasTable('accounts'))
		{
			Schema::table('accounts', function($table) {
				$table->dropIndex('accounts_is_enabled_index');
			});
		}
	}

}
