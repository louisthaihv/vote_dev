
<table>
    <?php $forI = 0; ?>
    @foreach ($resultData as $result)
<tr class="rankNo{{ $result['ranking'] }} {{ ($myAnswer->id == $result['choice_id']) ? 'my': '' }} {{ (!$question->isViewableAverage() && $forI >= 10) ? 'otherRanking' : '' }}">
    <td class="rank">
        <span>{{ ($result['choice_num'] === OTHER_CHOICE_NUM) ? '-' : $result['ranking'] }}</span>
    </td>
    <td class="answer">
        {{ $result['choice_text'] }}
        <div class="barArea"><span style="width:{{ $result['bar_width'] }}%;max-width:100%;"></span></div>
    </td>
    <td class="data">
        {{ number_format($result['summary_count']) }}票<br />{{ isset($result['rate']) ? $result['rate'] : calcPercent($result['summary_count'], $question->vote_count) }}%
    </td>
</tr>
        <?php $forI++; ?>
    @endforeach
</table>
@if (!$question->isViewableAverage() && count($resultData) > 10)
    <a href="#" class="moreAction"><span>11位以下を見る（全{{ count($resultData) }}件）</span></a>
@endif
