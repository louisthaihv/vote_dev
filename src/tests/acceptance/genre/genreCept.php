<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('check genre');
$I->amOnPage('/manage');

$I->loginWithDetail(['admin', '123456']);
$I->click('ジャンル一覧・登録・修正・削除');
$I->seeCurrentUrlEquals('/manage/genre');
$I->create('genre/add', '＋ジャンル追加');
$I->checkGenreCreate();
$I->createNewGenre('genre_test_1', 'genre_directory');
$I->createNewGenre('genre_test_2', 'genre_directory_2');
$I->checkSortGenre();
$I->checkValidGenreSortNumber();
$I->checkValidSortNumber('genre_test_1');
$I->createNewSmallGenre('name_1', 'dir_1', '10', 'genre_test_1', 'genre_directory');
$I->createNewSmallGenre('name_2', 'dir_2', '20', 'genre_test_1', 'genre_directory');
$I->checkValidSmallGenreSortNumber('genre_test_1');
$I->checkValidDirectoryGenre('genre_test_1');
$I->checkSameNameDirectorGenre('genre_test_1', 'name_3', 'dir_1', '30', 'genre_directory');
$I->checkSortSmallGenre('genre_test_1', 'genre_directory');
$I->checkHiddenSmallGenre('genre_test_1');
$I->checkHidden('genre_directory', 'genre_test_1');
