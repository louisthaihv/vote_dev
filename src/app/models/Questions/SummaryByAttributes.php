<?php
namespace docomo\Models\Questions;

class SummaryByAttributes extends \BaseArdentModel
{
    protected $table = 'summary_by_attributes';

    public $timestamps = false;

    public static $cacheName = 'SummaryByAttributes';

    public static function summary()
    {
        $questions = Question::select('id')
            ->finished()
            ->whereRaw('`vote_date_to` >= ADDDATE(NOW(), INTERVAL -' . SummaryByType::$summaryHour . ' HOUR)')
            ->get();

        \DB::transaction(function () use ($questions) {
            \DB::delete('DELETE FROM summary_by_attributes');

            if (isset($questions) && $questions->count() > 0) {
                $questions = $questions->lists('id');

                \DB::insert('INSERT INTO summary_by_attributes ' .
                    ' SELECT age, gender, question_id, COUNT(user_id) FROM answers ' .
                    'WHERE age > 0 AND gender > 0 AND question_id IN (' .
                    implode(',', $questions) .
                    ') GROUP BY age, gender, question_id');
            }
        });

        unset($questions);
    }

    public static function getDataByCache($age, $gender)
    {
        $list = \Cache::get(SummaryByAttributes::$cacheName, function () {
            return SummaryByAttributes::setCache();
        });

        if (array_key_exists($age, $list) && array_key_exists($gender, $list[$age])) {
            return $list[$age][$gender];
        }
        return null;
    }

    public static function setCache()
    {
        $data = SummaryByAttributes::selectRaw('SQL_CACHE `age`, `gender`, `question_id`')->orderBy('vote_count', 'desc')->get();
        $setData = [];
        foreach ($data as $rec) {
            if (!array_key_exists($rec->age, $setData)) {
                $setData[$rec->age] = [];
            } else if (array_key_exists($rec->gender, $setData[$rec->age])) {
                continue;
            }
            $question = Question::fieldList()->find($rec->question_id);
            if ($question) {
                $setData[$rec->age][$rec->gender] = $question;
                unset($question);
            }
        }
        unset ($rec);
        unset ($data);

        \Cache::put(SummaryByAttributes::$cacheName, $setData, 60);

        return $setData;

    }
}
