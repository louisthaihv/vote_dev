<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRatings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vote_ratings', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('question_id');
			$table->integer('user_id');
			$table->integer('type');
            $table->timestamps();
		});
		
        Schema::create('vote_rating_totals', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('question_id');
			$table->integer('type');
			$table->integer('rating_count')->default(0);
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('vote_ratings');
		Schema::dropIfExists('vote_rating_totals');
	}

}
