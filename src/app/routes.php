<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
 */

Route::group(array('prefix' => 'api'), function() {
    $namespace = 'Api';
//    Route::controller('product', $namespace . '\ProductController');
});

/*
| ROUTE STRUCTURE FOR DOCOMO
| Required routes will be created first
| Need to update extra routes when develop
*/

Route::pattern('id', '[0-9]+');

Route::get('manage/', ['as' => 'admin.home', 'uses' => 'Admin\HomeController@index', 'after' => 'header.all']);
Route::post('manage/', ['as' => 'admin.login', 'uses' => 'Admin\HomeController@login', 'after' => 'header.all']);

Route::group(['prefix' => 'manage', 'namespace' => 'Admin', 'before' => 'auth', 'after' => 'header.all'], function(){
    // ADMIN_01 and ADMIN_02

    Route::get('/logout', ['as' => 'admin.logOut', 'uses' => 'HomeController@logOut']);

        // ADMIN_03 - ADMIN_06
    Route::post('/question/create',        ['as' => 'manage.question.store',    'uses' => 'QuestionController@store']);
    Route::put('/question/{id}/edit',      ['as' => 'manage.question.update',   'uses' => 'QuestionController@update']);
    Route::get('/question/{id}/complete/{message_type?}',      ['as' => 'manage.question.complete',   'uses' => 'QuestionController@complete']);
    Route::get('/question/{id}/result', ['as' => 'manage.question.result'             , 'uses' => 'QuestionController@result']);
    Route::get('/question/{id}/csv/{attr}/{device?}', ['as' => 'manage.question.csv'             , 'uses' => 'QuestionController@csv']);
    Route::post('/question/{id}/result', ['as' => 'manage.question.result'             , 'uses' => 'QuestionController@updateColumn']);
    Route::put('/question/{id}/result', ['as' => 'manage.question.result'             , 'uses' => 'QuestionController@updateColumn']);
    Route::resource('question', 'QuestionController', ['except' => ['show', 'store', 'update']]);
    Route::post('/question',        ['as' => 'manage.question.search',    'uses' => 'QuestionController@search']);

    Route::get('/question/dump',                     ['as' => 'manage.question.dump', 'uses' => 'QuestionController@downloadDump', 'before' => 'auth.manage:summary']);

    Route::group(['before' => 'auth.manage:column'], function () {
        Route::get('/column',                   ['as' => 'manage.column.index', 'uses' => 'QuestionController@searchColumn']);
        Route::post('/column',                   ['as' => 'manage.column.search', 'uses' => 'QuestionController@searchColumn']);
    });

    //ADMIN_07 - ADMIN_10
    Route::group(['before' => 'auth.manage:feature'], function() {
        Route::post('/feature/create'      , ['as' => 'admin.feature.store'              , 'uses' => 'FeatureController@store']);
        Route::post('/feature/confirm'     , ['as' => 'admin.feature.confirm'             , 'uses' => 'FeatureController@confirm']);
        Route::put('/feature/{id}/edit'   , ['as' => 'admin.feature.update'                , 'uses' => 'FeatureController@update']);
        Route::resource('feature', 'FeatureController', ['except' => ['show', 'store', 'update']]);
    });

    //ADMIN_11
    Route::group(['before' => 'auth.manage:pickup'], function() {
        Route::get('/pickup/create/{id}',      ['as' => 'manage.pickup.create',   'uses' => 'PickupController@create']);
        Route::post('/pickup/create/{id}',      ['as' => 'manage.pickup.store',   'uses' => 'PickupController@store']);
        Route::put('/pickup/{id}/edit'   , ['as' => 'admin.pickup.update'                , 'uses' => 'PickupController@update']);
        Route::resource('pickup', 'PickupController', ['except' => ['show', 'create', 'store', 'update']]);
    });

    //ADMIN_11 - ADMIN_13
    Route::group(['before' => 'auth.manage:attention'], function() {
        Route::post('/attention/create',      ['as' => 'manage.attention.store',    'uses' => 'AttentionController@store']);
        Route::put('/attention/{id}/edit',    ['as' => 'manage.attention.update',   'uses' => 'AttentionController@update']);
        Route::resource('attention', 'AttentionController', ['except' => ['store', 'show', 'update']]);
    });

        //ADMIN_14 - ADMIN_16
    Route::group(['before' => 'auth.manage:genre'], function() {
        Route::put('/genre',               ['as' => 'manage.genre.sort',    'uses' => 'GenreController@sort']);
        Route::post('/genre/create',        ['as' => 'manage.genre.store',    'uses' => 'GenreController@store']);
        Route::put('/genre/{id}/edit',      ['as' => 'manage.genre.update',   'uses' => 'GenreController@update']);
        Route::resource('genre', 'GenreController', ['except' => ['store', 'show', 'update', 'destroy']]);
    });

    Route::get('/keyword/pick/{max?}',  ['as' => 'manage.keyword.pick',     'uses' => 'KeywordController@pick']);
        //ADMIN_17 - ADMIN_19
    Route::group(['before' => 'auth.manage:keyword'], function() {
        Route::post('/keyword/create',      ['as' => 'manage.keyword.store',    'uses' => 'KeywordController@store']);
        Route::put('/keyword/{id}/edit',    ['as' => 'manage.keyword.update',   'uses' => 'KeywordController@update']);
        Route::resource('keyword', 'KeywordController', ['except' => ['store', 'show', 'update', 'destroy']]);
    });

        //ADMIN_23 - ADMIN_25
    Route::group(['before' => 'auth.manage:account', 'after' => 'header.all'], function() {
        Route::post('/account/create',      ['as' => 'manage.account.store',    'uses' => 'AccountController@store']);
        Route::put('/account/{id}/edit',    ['as' => 'manage.account.update',   'uses' => 'AccountController@update']);
        Route::resource('account', 'AccountController', ['except' => ['store', 'show', 'update', 'destroy']]);
    });
        //ADMIN_05 - ADMIN_05_3
    Route::group(['before' => 'auth.manage:notice'], function() {
        Route::post('/notice/create',       ['as' => 'manage.notice.store',         'uses' => 'NoticeController@store']);
        Route::put('/notice/{id}/edit',     ['as' => 'manage.notice.update',        'uses' => 'NoticeController@update']);
        Route::post('/notice/confirm',      ['as' => 'manage.notice.confirm',       'uses' => 'NoticeController@confirm']);
        Route::resource('notice', 'NoticeController', ['except' => ['store', 'show', 'update']]);
    });

    Route::group(['before' => 'auth.manage:summary'], function () {
        Route::get('/summary/daily/vote',   ['as' => 'manage.summary.daily_vote',   'uses' => 'SummaryController@dailyVote']);
        Route::post('/summary/daily/vote',  ['as' => 'manage.summary.daily_vote',   'uses' => 'SummaryController@dailyVote']);

        Route::get('/summary/vote/ranking',     ['as' => 'manage.summary.vote_ranking',     'uses' => 'SummaryController@voteRanking']);
        Route::post('/summary/vote/ranking',    ['as' => 'manage.summary.vote_ranking',     'uses' => 'SummaryController@voteRanking']);

        Route::get('/summary/vote/genre',     ['as' => 'manage.summary.vote_genre',     'uses' => 'SummaryController@voteGenre']);
        Route::post('/summary/vote/genre',    ['as' => 'manage.summary.vote_genre',     'uses' => 'SummaryController@voteGenre']);

        Route::get('/summary/vote/profile',     ['as' => 'manage.summary.vote_profile',     'uses' => 'SummaryController@voteProfile']);
        Route::post('/summary/vote/profile',    ['as' => 'manage.summary.vote_profile',     'uses' => 'SummaryController@voteProfile']);

        Route::get('/summary/daily/profile',    ['as' => 'manage.summary.daily_profile',    'uses' => 'SummaryController@dailyProfile']);
        Route::post('/summary/daily/profile',   ['as' => 'manage.summary.daily_profile',    'uses' => 'SummaryController@dailyProfile']);

        Route::get('/summary/profile',  ['as' => 'manage.summary.profile',  'uses' => 'SummaryController@profile']);
        Route::post('/summary/profile', ['as' => 'manage.summary.profile',  'uses' => 'SummaryController@profile']);

        Route::get('/summary/chmod_log',    ['as' => 'manage.summary.chmod',        'uses' => 'SummaryController@chmodLog']);
    });



    Route::group([], function () {
        // 管理画面ができる前提でgroup化
        Route::get('/upload/create',                    ['as' => 'manage.upload.create',      'uses' => 'UploadController@create']);
        Route::post('/upload/store',                    ['as' => 'manage.upload.store',      'uses' => 'UploadController@store']);
    });

});


Route::group(['namespace' => 'Front', 'before' => 'allow.device', 'after' => 'header.fp|header.all'], function(){

    // USER_SP_01
    Route::get('/'                            , ['as' => 'home.top'                , 'uses' => 'HomeController@index']);//ok
    Route::post('/home/getAcceptingProfileQuestion'   , ['as' => 'home.getAcceptingProfileQuestion'    , 'uses' => 'HomeController@getAcceptingProfileQuestion']);//ok
    Route::post('/home/getAnnouncedProfileQuestion'   , ['as' => 'home.getAnnouncedProfileQuestion'    , 'uses' => 'HomeController@getAnnouncedProfileQuestion']);//ok

    // USER_SP_02
    Route::get( '/{parent_genre_name}/{child_genre_name}/entry/{id}'                , ['as' => 'entry.show',         'uses' => 'EntryController@show']);
    Route::put( '/{parent_genre_name}/{child_genre_name}/entry/{id}'                , ['as' => 'entry.set.attribute',         'uses' => 'EntryController@updateUser']);
    Route::post('/{parent_genre_name}/{child_genre_name}/entry/{id}/{choice_id?}'   , ['as' => 'entry.answer',       'uses' => 'EntryController@answer']);

    // USER_SP_03
    Route::get('/{parent_genre_name}/{child_genre_name}/result/{id}/{attributeDirectory?}/{attributeValueDirectory?}'            , ['as' => 'result.show',        'uses' => 'ResultController@show']);
    Route::post('/{parent_genre_name}/{child_genre_name}/result/{id}/vote/{type}', ['as' => 'result.vote',        'uses' => 'ResultController@asyncVote']);
    Route::get('/{parent_genre_name}/{child_genre_name}/result/{id}/attr/{attr}/attrv/{attrv}', ['as' => 'async.result.show_ranking',        'uses' => 'ResultController@asyncShowRanking']);

    // USER_SP_04 - USER_SP_06
    Route::get('/list/entry'                 ,  ['as' => 'list.entry'             , 'uses' => 'ListController@entry']);//ok
    Route::get('/list/result'                 , ['as' => 'list.result'             , 'uses' => 'ListController@result']); // ok
    Route::get('/mypage'                      , ['as' => 'list.mypage'             , 'uses' => 'ListController@myPage']);//ok
    Route::post('/mypage/nums_question'       , ['as' => 'mypage.nums_question'    , 'uses' => 'ListController@myPageNumsQuestion']);//ok

    // USER_SP_09, USER_SP_07, USER_SP_07_1
    Route::get('/list/genre'                  , ['as' => 'list.genre'              , 'uses' => 'ListController@genre']);//
    Route::get('/list/genre/{parent_genre_name}/{child_genre_name}/entry'  , ['as' => 'list.genre.entry'        , 'uses' => 'ListController@genreEntry']); //ok
    Route::get('/list/genre/{parent_genre_name}/{child_genre_name}/result' , ['as' => 'list.genre.result'       , 'uses' => 'ListController@genreResult']); //ok

    // USER_SP_10, USER_SP_08, USER_SP_08_1
//    Route::get('/list/tag'                    , ['as' => 'list.tag'                , 'uses' => 'ListController@tag']); //ok
    Route::get('/list/tag/{id}/entry'         , ['as' => 'list.tag.entry'          , 'uses' => 'ListController@tag_entry']); //ok
    Route::get('/list/tag/{id}/result'        , ['as' => 'list.tag.result'         , 'uses' => 'ListController@tag_result']);//ok

    // USER_SP_11, USER_SP_11_1
    Route::get('/profile'                     , ['as' => 'profile.edit'             , 'uses' => 'ProfileController@edit']);//ok
    Route::post('/profile'                    , ['as' => 'profile.update'           , 'uses' => 'ProfileController@update']);//ok
    Route::get('/profile/complete'            , ['as' => 'profile.complete'         , 'uses' => 'ProfileController@complete']);//ok

    // USER_SP_12
    Route::get('/help'                       , ['as' => 'help'                   , 'uses' => 'GuideController@help']);

    // USER_SP_13
    Route::get('/notice/{id}'                        , ['as' => 'notice.detail'             , 'uses' => 'NoticeController@show']); // ok
    
    // USER_SP_14
    Route::get('/search/tag/{word?}'           , ['as' => 'search.result'           , 'uses' => 'SearchController@result']); // ok

    // USER_SP_15 - USER_SP_17
    Route::get('list/new/entry'                , ['as' => 'list.new.entry'                , 'uses' => 'ListController@newEntry']);
    Route::get('list/new/result'                , ['as' => 'list.new.result'                , 'uses' => 'ListController@newResult']);
    Route::get('list/popular/entry'            , ['as' => 'list.popular.entry'            , 'uses' => 'ListController@popularEntry']);
    Route::get('list/popular/result'            , ['as' => 'list.popular.result'            , 'uses' => 'ListController@popularResult']);
    Route::get('list/unanswered'               , ['as' => 'list.unanswered'          , 'uses' => 'ListController@noAnswerList']);
    Route::post('/more-list', ['as'=> 'list.more', 'uses' => 'ListController@getMoreQuestion']);


    // USER_SP_18
    Route::get('feature'                    , ['as' => 'feature'                , 'uses' => 'FeatureController@listFeature']);
    Route::get('feature/{id}'               , ['as' => 'feature.detail'         , 'uses' => 'FeatureController@detailFeature']);
    Route::post('/more_feature_list'        , ['as' => 'more.list.feature'      , 'uses' => 'ListController@getMoreFeature']);

    // USER_SP_19
    Route::get('/list/column'                  , ['as' => 'list.column'           , 'uses' => 'ListController@listColumn']);
    Route::post('/more-column-list'             , ['as' => 'more.list.column',  'uses' => 'ListController@getMoreColumn']);

    Route::get('/caution' ,                     ['as' => 'caution', 'uses' => 'GuideController@caution']);

});

Route::group(['prefix' => 'rss', 'namespace' => 'RSS', 'after' => 'header.rss'], function() {
    Route::get('/column/{parent_genre?}/{child_genre?}',           ['as' => 'rss.column',  'uses' => 'ColumnListController@generate']);
    Route::get('/genre/{parent_genre?}/',                           ['as' => 'rss.genre',   'uses' => 'GenreListController@generate']);
    Route::get('/newly/{parent_genre?}/{child_genre?}',             ['as' => 'rss.newly',   'uses' => 'QuestionListController@generate']);
});
