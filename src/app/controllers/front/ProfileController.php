<?php namespace Front;

use docomo\Models\Attributes\Attribute;
use docomo\Models\Users\User;

class ProfileController extends \BaseController {

    public function __construct(){

    }

    public function edit()
    {
        $attributes = Attribute::getDataByCache();

        return \View::make('front.profile.edit', ['user' => $this->getCurrentUser(), 'attributes' => $attributes->sortBy('sort_order')]);
    }

    public function update()
    {
        $user = $this->getCurrentUserWithCreate();
        $user = User::fillWithModify($user);

        if (!$user->result) {
            $attributes = Attribute::getDataByCache();

            return \View::make('front.profile.edit', ['user' => $user, 'attributes' => $attributes->sortBy('sort_order')]);
        }
        return \Redirect::to(route('profile.complete') . (($user->getDevice() === TYPE_I_MODE) ? '?uid=NULLGWDOCOMO' : ''));
    }

    public function complete()
    {
        $user = $this->getCurrentUser();
        $user_id = empty($user_id) ? $user->id : $user_id; // User_id send to complete page

        return \View::make('front.profile.complete', compact('user_id', 'user'));
    }
}
