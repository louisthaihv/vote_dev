<?php

Form::macro('dateTimeControl', function($name, $datetime = null, $minute_step = 1, $ymd_options = array(), $hour_options = array(), $minute_options = array()) {

    $ymd_value = null;
    $hour_value = null;
    $minute_value = null;

    if ($datetime !== null)
    {
        $date_time = explode(' ', $datetime);
        $ymd_value = str_replace('-', '', $date_time[0]);
        $hour_value = substr($date_time[1], 0, 2);
        $minute_value = substr($date_time[1], 3, 2);
    }

    $hourList = [];
    for ($i = 0; $i < 24; $i++) {
        $val = str_pad($i, 2, "0", STR_PAD_LEFT);
        $hourList += [$val => $val];
    }

    $minuteList = [];
    for ($i = 0; $i < 60; $i += $minute_step) {
        $val = str_pad($i, 2, "0", STR_PAD_LEFT);
        $minuteList += [$val => $val];
    }

    return
        Form::text($name . '_date', $ymd_value, $ymd_options) . ' ' .
        Form::select($name . '_hour', $hourList, $hour_value, $hour_options) . '時 ' .
        Form::select($name . '_min', $minuteList, $minute_value, $minute_options) . '分';
});

Form::macro('selectByTable', function($name, $table, $value_column, $text_column, $default_value, $append_all = false, $first = null)
{

    $table_array = [];
    if ($first !== null) {
        $table_array += $first;
    }
    if ($append_all) {
        $table_array += [DDL_ALL => 'すべて'];
    }
    foreach ($table as $record)
    {
        $table_array += [$record[$value_column] => $record[$text_column]];
    }

    return Form::select($name, $table_array, $default_value, ['id' => $name]);
});

Form::macro('selectGenre', function($name, $array_tables, $default_value)
{

    $table_array = [];
    $table_array += [DDL_ALL => 'すべて'];
    foreach ($array_tables as $id => &$values)
    {
        $table_array += ['parent_' . $id => $values[0]];
        foreach ($values[1] as &$record)
        {
            $table_array += [$record['id'] => '　' . $record['genre_child_name']];
        }
        unset($values);
        unset($record);
    }

    return Form::select($name, $table_array, $default_value, ['id' => $name]);
});

Form::macro('selectByTableInGroup', function($name, $array_tables, $value_column, $text_column, $default_value, $append_all = false, $first = null)
{

    $table_array = [];
    if ($first !== null) {
        $table_array += $first;
    }
    if ($append_all) {
        $table_array += [DDL_ALL => 'すべて'];
    }
    foreach ($array_tables as $group => &$values)
    {
        $options = [];
        if (is_array($values)) {
            $tables = $values[1];
            $groupKey = $values[0];
        } else {
            $tables = $values;
            $groupKey = $group;
        }
        foreach ($tables as &$record)
        {
            $options += [$record[$value_column] => $record[$text_column]];
        }
        $table_array += [$groupKey => $options];
    }

    return Form::select($name, $table_array, $default_value, ['id' => $name]);
});

Form::macro('selectByJson', function($name, $json, $value_key, $text_key, $default_value, $append_all = false, $first = null)
{

    $json_array = [];
    if ($first !== null) {
        $json_array += $first;
    }
    if ($append_all) {
        $json_array += [DDL_ALL => 'すべて'];
    }
    foreach ($json as $key => $value) {
        $json_array += [$value[$value_key] => $value[$text_key]];
    }

    return Form::select($name, $json_array, $default_value);
});

Form::macro('checkboxForPermission', function($name, $permission, $label, $options = [])
{
    $options += ['id' => $name];
    return '<li>' .Form::checkbox($name, null, $permission == ALLOW, $options) .
        ' ' .
        Form::label($name, $label, $options) . '</li>';
});

Form::macro('checkboxForHidden', function($name, $hidden, $label = null)
{
    if ($label === null) {
        return Form::checkbox($name, null, ($hidden != VISIBLE));
    }
    else {
        return Form::checkbox($name, null, ($hidden != VISIBLE), ['id' => $name]) .
        ' ' .
        Form::label($name, $label);
    }
});

Form::macro('birthday', function($birthday = null, $options = array()){
    
    $nowYear = (int)date('Y');
    $startYear = $nowYear - 110;
    
    if($birthday){

        if(!is_string($birthday)){
            $birthday = date_format($birthday, 'Y-M-D');
        }
        $ymd_value = explode('-', $birthday);

        $defaultYear =$ymd_value[0];
        $defaultMonth = $ymd_value[1];
        $defaultDay = $ymd_value[2];
        if (strpos($defaultDay, ' ') !== false) {
            $defaultDay = explode(' ', $defaultDay)[0];
        }

    }else{

        $defaultYear = ' ';
        $defaultMonth = ' ';
        $defaultDay = ' ';

    }


    $yearList = [];
    for ($i = $startYear; $i <= $nowYear; $i++){
        if ($i === 1980) {
            $yearList += [' ' => ' '];
        }
        $val = strVal($i);
        $yearList += [$val => $val];
    }

    $monthList = [];
    $monthList += [' ' => ' '];
    for ($i = 1; $i <= 12; $i++){
        $val = str_pad($i, 2, '0', STR_PAD_LEFT);
        $monthList += [$val => $val];
    }
    
    $dayList = [];
    $dayList += [' ' => ' '];
    for ($i = 1; $i <= 31; $i++){
        $val = str_pad($i, 2, '0', STR_PAD_LEFT);
        $dayList += [$val => $val];
    }

    return
        Form::select('birthday_year', $yearList, $defaultYear) . '年' .
        Form::select('birthday_month', $monthList, $defaultMonth) . '月' .
        Form::select('birthday_day', $dayList, $defaultDay) . '日' ;
    
});

Form::macro('attributeValueList', function($attribute_id, $options = array(), $attr_name='attribute_value_id'){
    $attributeValueList = docomo\Models\Attributes\AttributeValue::getDataByCache($attribute_id);

    $selectors = [];
    if(!empty($options['select_none'])) { // Add none select option on select form
        $selectors += [ATTRIBUTE_VALUE_NOT_SET => $options['select_none']];
    }
    foreach ($attributeValueList as $attributeValue) {
        $selectors += [$attributeValue->id => $attributeValue->attribute_value_name];
    }

    return Form::select($attr_name, $selectors, isset($options['default']) ? $options['default'] : null);
});
