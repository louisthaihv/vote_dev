<?php
namespace Codeception\Module;

use Laracasts\TestDummy\Factory as TestDummy;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class FunctionalHelper extends \Codeception\Module
{
    
    public function signIn()
    {
        $username = 'admin';
        $password = '123456';
        
        $this->haveAnAccount(compact('username', 'password'));
        
        $I = $this->getModule('Laravel4');
        
        $I->amOnPage('/login');
        $I->fillField('username', $username);
        $I->fillField('password', $password);
        $I->click('Sign In');
    }
    
    public function haveAnAccount($overrides = [])
    {
        TestDummy::create('Docomo\Models\Users\User', $overrides);
    }
}
