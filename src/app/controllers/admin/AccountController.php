<?php
namespace Admin;

use docomo\Models\Accounts\Account;
use docomo\Models\Genres\ParentGenre;

/**
 * Set datalist used in view.
 */
\View::composer([
    'admin.account.create',
    'admin.account.store',
    'admin.account.edit',
    'admin.account.update',
], function($view) {

    $view->with('genres', ParentGenre::getListForDDL());
});
class AccountController extends \BaseController
{
    
    /**
     *
     */
    public function __construct()
    {

    }
    
    /**
     * List Accounts
     *
     * @return View list Accounts
     */
    public function index()
    {

        $show_all = (\Input::get(SHOW_ALL) !== null);
        $accounts = Account::getList($show_all);

        return \View::make('admin.account.index', compact('accounts', 'show_all'));
    }

    /**
     * Create Account
     *
     * @return View create account
     */
    public function create()
    {

        return \View::make('admin.account.create', ['account' => new Account]);
    }

    /**
     * Deatail account
     *
     * @param  [int] $id [id of account]
     * @return View detail account
     */
    public function edit($id)
    {

        return \View::make('admin.account.edit', ['account' => Account::findWithDecrypt($id)]);
    }



    /**
     * Insert account
     *
     * @return insert account in database
     */
    public function store()
    {

        $account = Account::fillWithModify();
        $account->created_account_id = \Auth::user()->id;

        if ($account->save())
        {
            Account::updateCacheTime(Account::$cacheName);

            return \Redirect::route('manage.account.index');
        }
        else
        {

            return \View::make('admin.account.create', compact('account'));
        }
    }
    
    /**
     * Update Account
     *
     * @param  [int] $id [Id of Account logined]
     * @return [Redirect]     [update success account and redirect to list account]
     */
    public function update($id)
    {

        $account = Account::fillWithModify($id);

        $account->id = $id;


        if ($account->save())
        {
            Account::updateCacheTime(Account::$cacheName);

            return \Redirect::route('manage.account.index');
        }
        else
        {

            return \View::make('admin.account.edit', compact('account'));
        }
    }
}
