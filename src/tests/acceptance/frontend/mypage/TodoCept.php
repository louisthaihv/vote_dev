<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('test mypage');
$I->amOnPage('/');

//create profile
$profile_info = array(
				'sex' => '男性',
				'birthday_year' => '1990',
				'birthday_month' => '06',
				'birthday_day' => '01',
				'pref' => '京都府',
				'btn_submit' => '登録する'
				);

$I->createUserProfile($profile_info);

//create question
$genre_parent = array(
                    1 => array(
                                'genre_parent_name' => 'genre_1',
                                'parent_is_visible' => '1',
                                'genre_parent_directory' => 'everyday',
                                'parent_sort_order' => '10',
                                )
                );
$I->createDbGenreParent($genre_parent);

$genre_child = array(
                    1 => array(
                                'genre_parent_id'=>'1',
                                'genre_child_name'=>'genre_child_1_1',
                                'child_is_visible'=>'1',
                                'genre_child_directory'=>'1',
                                'child_sort_order'=>'10'
                                )
                );

$I->createDbGenreChild($genre_child);

//create survey result
$survies_result = array();
for ($i=1; $i < 12; $i++) {
    $title = 'title_'.$i;
    $date = '2014-12-'.($i+10).' 00:00:00';
    $survies_result[$i] = array(
                        'genre_parent_id'=>'1',
                        'genre_child_id'=>'1',
                        'status'=>'6',
                        'title'=>$title,
                        'vote_date_from'=>'2014-12-05 00:00:00',
                        'vote_date_to'=>$date,
                        'baseline_date'=>$date,
                        'viewable_type'=>'1',
                        'visible_device_i'=>'1',
                        'visible_device_d'=>'1',
                        'summary_type'=>'1',
                        'average_unit'=>'1',
                        'is_visible_other'=>'1',
                        'shuffle_choice'=>'1',
                        'navigation_title'=>'1',
                        'account_id'=>'1',
                        );
}
$I->createDatabaseSurvey($survies_result);
//after create output 11 items result
$survies_result_check = array();
foreach ($survies_result as $survey) {
    $jpdate = $I->dateToJpdate($survey['baseline_date']);
    $survies_result_check[] = array('title' => $survey['title'], 'jpdate' => $jpdate, 'status' => '1');
}
//order list to see
krsort($survies_result_check);

//create survey entry
$survies_entry = array();
for ($i=1; $i <= 3; $i++) {
    $title = 'title_'.($i+20);
    $date = '2014-12-'.($i+10).' 00:00:00';
    $survies_entry[$i] = array(
                        'genre_parent_id'=>'1',
                        'genre_child_id'=>'1',
                        'status'=>'5',
                        'title'=>$title,
                        'vote_date_from'=>$date,
                        'vote_date_to'=>'2015-02-20 00:00:00',
                        'baseline_date'=>$date,
                        'viewable_type'=>'1',
                        'visible_device_i'=>'1',
                        'visible_device_d'=>'1',
                        'summary_type'=>'1',
                        'average_unit'=>'1',
                        'is_visible_other'=>'1',
                        'shuffle_choice'=>'1',
                        'navigation_title'=>'1',
                        'account_id'=>'1',
                        );
}
$I->createDatabaseSurvey($survies_entry);

//after create output 3 items entry
$survies_entry_check = array();
foreach ($survies_entry as $survey) {
    $jpdate = $I->dateToJpdate($survey['baseline_date']);
    $survies_entry_check[] = array('title' => $survey['title'], 'jpdate' => $jpdate, 'status' => '1');
}
//order list to see
krsort($survies_entry_check);

//create answer
$answers = array();
for ($i=1; $i <= count($survies_entry) + count($survies_result); $i++) {
    $answers[$i] = array(
                        'question_id'=>$i,
                        'user_id'=>'1',
                        'choice_id'=>'1',
                        'age'=>'20',
                        'gender'=>'1',
                        'local'=>'1',
                        'answer_date'=>date('Y-m-d H:i:s'),
                        );
}
$I->createDatabaseAnswers($answers);

$voted_count = count($survies_entry) + count($survies_result);
//create data
$result_vote = array(
					'link_for_showmore' => 'もっと見る',
					'link_end_list' => array(
											'title' => 'アンケート結果をもっと見る',
											'url'	=> '/list/result'
											),
					'xpath_format' => 'html/body/div[1]/div/div/section[2]/div[1]/ul/li[%d]/a',
					'items_check' => $survies_result_check
				);

$entry_vote = array(
					'link_end_list' => array(
											'title' => '未回答のアンケートをもっと見る',
											'url'	=> '/list/unanswered'
											),
					'xpath_format' => 'html/body/div[1]/div/div/section[2]/div[2]/ul/li[%d]/a',
					'items_check' => $survies_entry_check
				);

$profile = array('title' => 'ブロフィール設定・変更', 'url' => '/profile');

$I->checkMyPageSp6($voted_count, $result_vote, $entry_vote, $profile);