<?php
class AttributeValuesSeeder extends Seeder
{
    public function run()
    {
        DB::table('attribute_values')->truncate();
        $attributeValues = [];
        
        $items = [];
        $items[] = ["attribute_id" => "1", "attribute_value_name" => ["～10代", "20代", "30代", "40代", "50代",
        "60代～"], 'attribute_value_directory' => ['10', '20', '30', '40', '50', '60']];
        $items[] = ["attribute_id" => "2", "attribute_value_name" => ["男性", "女性"], 'attribute_value_directory' => ['male', 'female']];
        $items[] = ["attribute_id" => "3", "attribute_value_name" => ["北海道", "青森県", "岩手県",
        "宮城県", "秋田県", "山形県", "福島県", "茨城県", "栃木県", "群馬県", "埼玉県",
        "千葉県", "東京都", "神奈川県", "新潟県", "富山県", "石川県", "福井県", "山梨県",
        "長野県", "岐阜県", "静岡県", "愛知県", "三重県", "滋賀県", "京都府", "大阪府",
        "兵庫県", "奈良県", "和歌山県", "鳥取県", "島根県", "岡山県", "広島県", "山口県",
        "徳島県", "香川県", "愛媛県", "高知県", "福岡県", "佐賀県", "長崎県", "熊本県",
        "大分県", "宮崎県", "鹿児島県", "沖縄県"],
        "attribute_value_directory" => ['hokkaido', 'aomori', 'iwate', 'miyagi', 'akita', 'yamagata', 'fukushima', 'ibaraki',
        'tochigi', 'gunma', 'saitama', 'chiba', 'tokyo', 'kanagawa', 'niigata', 'toyama', 'ishikawa', 'fukui', 'yamanashi',
        'nagano', 'gifu', 'shizuoka', 'aichi', 'mie', 'shiga', 'kyoto', 'osaka', 'hyogo', 'nara', 'wakayama', 'tottori',
        'shimane', 'okayama', 'hiroshima', 'yamaguchi', 'tokushima', 'kagawa', 'ehime', 'kouchi', 'fukuoka', 'saga', 'nagasaki',
        'kumamoto', 'oita', 'miyazaki', 'kagoshima', 'okinawa']];
        
        foreach ($items as $data_item) {
            $attribute_value = 1;
            $sort_order = 10;
            for ($i = 0; $i < count($data_item['attribute_value_name']); $i++) {
                $item = [];
                $item["attribute_id"] = $data_item["attribute_id"];
                $item['attribute_value'] = $attribute_value;
                $item["attribute_value_name"] = $data_item['attribute_value_name'][$i];
                $item["sort_order"] = $sort_order;
                $item["attribute_value_directory"] = $data_item['attribute_value_directory'][$i];
                if ($item['attribute_value_directory'] == 'female' ||
                    $item['attribute_value_directory'] == '30' ||
                    $item['attribute_value_directory'] == 'tokyo') {
                    $item['is_default'] = true;
                } else {
                    $item['is_default'] = false;
                }
                $item['created_at'] = new DateTime();
                $item['updated_at'] = new DateTime();
                $attributeValues[] = $item;
                $attribute_value ++;
                $sort_order += 10;
            }
        }

        DB::table("attribute_values")->insert($attributeValues);
    }
}
