<?php
$I = new AcceptanceTester\FooterSteps($scenario);
$I->wantTo('check FP footer');

$links = array(
				//link menu with vote
				array(
					'title' => 'ﾏｲﾍﾟｰｼﾞ',
					'url' => '/mypage',
					),
				array(
					'title' => '未回答ｱﾝｹｰﾄ',
					'url' => '/list/unanswered',
					),
				array(
					'title' => '新着一覧',
					'url' => '/list/new/entry',
					'xpath' => 'html/body/div[78]/a'
				),
				array(
					'title' => '新着一覧',
					'url' => '/list/new/result',
					'xpath' => 'html/body/div[80]/a'
					),
				array(
					'title' => 'ｼﾞｬﾝﾙ一覧',
					'url' => '/list/genre',
					),
				//link menu with info
				array(
					'title' => 'ﾌﾟﾛﾌｨｰﾙ登録編集',
					'url' => '/profile',
					),
				array(
					'title' => '操作説明',
					'url' => '/help',
					),
				array(
					'title' => 'ご利用上の注意',
					'url' => '/caution',
					),
				array(
					'title' => '友達にこのｻｲﾄを紹介',
					//'url' => 'mailto:?subject=みんなの声&body=お友達にこのサイトを紹介して、みんなで投票しよう',
					),
				//link menu with top
				array(
					'title' => 'ﾍﾟｰｼﾞTOPへ',
					),
				array(
					'title' => 'みんなの声TOPへ',
					// 'url' => '/',
					),
				array(
					'title' => 'iMenuへ',
					//'url' => 'http://docomo.ne.jp/mn/main',
					),
				);

$texts = array(
				'・・・ご注意・・・',
				'◇iMenu｢みんなの声｣はﾊﾟｹｯﾄ通信料が発生します｡ご利用には｢ﾊﾟｹ･ﾎｰﾀﾞｲﾀﾞﾌﾞﾙ｣などのiﾓｰﾄﾞﾊﾟｹｯﾄ定額ｻｰﾋﾞｽにご加入されることをおすすめいたします｡',
				'◇科学的統計に拠る世論調査ではありません｡結果は予告なく削除する場合があります｡',
				'(C)NTT DOCOMO'
			);

$I->checkFPFooter($links, $texts);
