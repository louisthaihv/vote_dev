<div style="display: inline-table;">
<div style="display: table-row;">
@if ($question->isEnabledReject())
    <div style="display: table-cell;">
    {{ Form::open(['route' => ['manage.question.update', $question->id], 'method' => 'put']) }}
    {{ Form::hidden('set_status', QUESTION_STATUS_REJECT) }}
    <button type="submit" class="naviBtn btnReject">差し戻す</button>
    {{ Form::close() }}
    </div>
@endif

@if ($question->isEnabledRequestCancel())
    <div style="display: table-cell;">
    {{ Form::open(['route' => ['manage.question.update', $question->id], 'method' => 'put']) }}
    {{ Form::hidden('set_status', QUESTION_STATUS_DRAFT) }}
    <button type="submit" class="naviBtn btnRequestCancel">承認申請を取り消し</button>
    {{ Form::close() }}
    </div>
@endif

@if ($question->isEnabledAllEdit())
    <div style="display: table-cell;">
    {{ Form::open(['route' => ['manage.question.update', $question->id], 'method' => 'get']) }}
    @if ($mode == MODE_ALL_EDIT)
        <button type="submit" class="naviBtn btnEditNormalQuestion">通常モード</button>
    @else
        {{ Form::hidden('mode', MODE_ALL_EDIT) }}
        <button type="submit" class="naviBtn btnEditFullQuestion">全編集モード</button>
    @endif
    {{ Form::close() }}
    </div>
@endif

@if ($question->isEnabledPickUp())
    <div style="display: table-cell;">
    {{ link_to_route('manage.pickup.create', 'ピックアップデータ作成', $question->id, ['class' => 'naviBtn btnPickup']) }}
    </div>
@endif

@if ($question->isEnabledDelete())
    <div style="display: table-cell;">
        <form method="POST" action="{{ route('manage.question.destroy', $question->id) }}" accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE">
            {{ Form::token() }}
            <button type="submit" class="naviBtn btnDeleteSurvey">削除</button>
        </form>
    </div>
@endif
</div>
</div>