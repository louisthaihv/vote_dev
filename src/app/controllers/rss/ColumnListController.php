<?php namespace RSS;

use docomo\Models\Features\Feature;
use docomo\Models\Questions\Column;
use docomo\Models\Questions\Question;
use docomo\Models\Questions\QuestionTag;
use docomo\Models\Attentions\Attention;
use docomo\Models\Answers\ResultVote;
use docomo\Models\Pickups\Pickup;
use docomo\Models\Questions\SummaryByAttributes;
use docomo\Models\Questions\SummaryByType;
use docomo\Models\Users\User;
use docomo\Models\Users\UserAttribute;
use docomo\Models\Attributes\AttributeValue;
use docomo\Models\Genres\ChildGenre;
use docomo\Models\Genres\ParentGenre;

class ColumnListController extends RSSBaseController {

    protected $rssTitle = 'みんなの声コラム一覧';
    protected $siteDescription = 'みんなの声で作成されたコラムの一覧です';

    public function generate ($parent_genre = null, $child_genre = null) {

        if (!is_null($parent_genre)) {
            $data = Question::getColumnByGenre(200, $parent_genre, $child_genre, new \DateTime());
        } else {
            $data = Question::getColumnList(200, TYPE_D_MENU, new \DateTime());
        }

        return $this->createViewVer1ForColumn($data);
    }

    private function notFound() {

        return 'Not Found';
    }
}