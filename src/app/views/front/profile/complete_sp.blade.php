@extends('layouts.front')

@section('metaTag')
    <title>プロフィール設定 | みんなの声</title>
    <meta name="description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！">
    <meta property="og:title" content="プロフィール設定 | みんなの声" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
    <meta name="twitter:title" content="プロフィール設定 | みんなの声" />
    <meta name="twitter:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
@stop

@section('content')
<section class="l-margin">
    <h2 class="profileTit"><span>プロフィール設定</span></h2>
    <p class="tcenter l-margint l-margin">プロフィールの登録が完了しました。</p>
    <p class="tcenter m-margin"><a href="{{ route('profile.edit') }}" class="btnType02">プロフィール確認</a></p>
    <p class="tcenter l-margin"><a href="{{ route('home.top') }}" class="btnType02">みんなの声TOPへ</a></p>
</section>
@endsection