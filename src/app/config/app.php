<?php

return array(
	/*
	 * ------------------------------------------------------------------------
	 *  Allow Device List (Only Feature Phone)
	 * ------------------------------------------------------------------------
	 */
	'devices' => 'N01G|P01G|F08F|F01G|SH07F|F07F|P01F|N01F|SH03E|P01E|N07E|N01E|F01E|SH05D|SH03D|P03D|N03D|N02D|F06D|F04D|F02D|L10C|L03C|L01C|F11C|F10C|F09C|F08C|F07C|F05C|F04C|F03C|F02C|F01C|SH11C|SH10C|SH09C|SH08C|SH06C|SH05C|SH04C|SH02C|SH01C|CA01C|N05C|N03C|N02C|N01C|P06C|P05C|P04C|P03C|P02C|P01C|L04B|L03B|L02B|L01B|N08B|N07B|N06B|N05B|N04B|N03B|N02B|N01B|P07B|P06B|P05B|P04B|P03B|P02B|P01B|F10B|F09B|F08B|F07B|F06B|F04B|F03B|F02B|F01B|SH09B|SH08B|SH07B|SH06B|SH05B|SH04B|SH03B|SH02B|SH01B|L06A|L04A|L03A|L01A|SH08A|SH07A3|SH06A3|SH05A3|SH04A|SH03A|SH02A|SH01A|N09A3|N08A3|N07A3|N06A3|N05A|N04A|N03A|N02A|N01A|P10A|P09A3|P08A3|P07A3|P06A|P05A|P04A|P03A|P02A|P01A|F10A|F09A3|F08A3|F07A|F06A|F05A|F04A|F03A|F02A|F01A|SH706iw|N706i2|SH706ie|N706ie|P706ie|L706ie|P706imyu|SH706i|SO706i|N706i|F706i|SH906iTV|N906iL|N906i|F906i|N906imyu|SH906i|SO906i|P906i|L852i|F884iES|F884i|SH705i2|P705iCL|L705iX|SO705i|P705imyu|N705imyu|SH705i|L705i|N705i|D705imyu|D705i|P705i|F705i|F801i|P905iTV|F905iBiz|SO905iCS|SH905iTV|N905iBiz|N905imyu|SO905i|F905i|P905i|N905i|D905i|SH905i|L704i|P704i|D704i|SH704i|P704imyu|N704imyu|F704i|SO704i|P904i|D904i|F904i|N904i|SH904i|F883iS|F883iESS|F883iES|F883i|SO703i|N703imyu|P703imyu|SH703i|D703i|P703i|F703i|N703iD|N601i|SO903iTV|P903iX|F903iBSC|SH903iTV|P903iTV|F903iX|D903iTV|SO903i|F903i|D903i|N903i|P903i|SH903i|F882iES|D800iDS|SA800i|M702iG|M702iS|D702iF|P702iD|N702iS|SH702iS|SA702i|D702iBCL|SO702i|D702i|SH702iD|F702iD|N702iD|P702i|N902iL|N902iX|SH902iSL|SO902iWP+|F902iS|D902iS|N902iS|P902iS|SH902iS|SO902i|SH902i|P902i|N902i|D902i|F902i|N701iECO|D701iWM|P701iD|N701i|D701i|D851iWM|P851i|SH851i|SA700iS|SH700iS|F700iS|P700i|N700i|SH700i|F700i|P901iTV|N901iS|P901iS|D901iS|F901iS|SH901iS|P901i|D901i|N901iC|F901iC|SH901iC',

	/*
	|--------------------------------------------------------------------------
	| Application Debug Mode
	|--------------------------------------------------------------------------
	|
	| When your application is in debug mode, detailed error messages with
	| stack traces will be shown on every error that occurs within your
	| application. If disabled, a simple generic error page is shown.
	|
	*/

	'debug' => false,

	/*
	|--------------------------------------------------------------------------
	| Application URL
	|--------------------------------------------------------------------------
	|
	| This URL is used by the console to properly generate URLs when using
	| the Artisan command line tool. You should set this to the root of
	| your application so that it is used when running Artisan tasks.
	|
	*/

	'url' => '',

	/*
	|--------------------------------------------------------------------------
	| Application Timezone
	|--------------------------------------------------------------------------
	|
	| Here you may specify the default timezone for your application, which
	| will be used by the PHP date and date-time functions. We have gone
	| ahead and set this to a sensible default for you out of the box.
	|
	*/

	'timezone' => 'Asia/Tokyo',

	/*
	|--------------------------------------------------------------------------
	| Application Locale Configuration
	|--------------------------------------------------------------------------
	|
	| The application locale determines the default locale that will be used
	| by the translation service provider. You are free to set this value
	| to any of the locales which will be supported by the application.
	|
	*/

	'locale' => 'ja',

	/*
	|--------------------------------------------------------------------------
	| Application Fallback Locale
	|--------------------------------------------------------------------------
	|
	| The fallback locale determines the locale to use when the current one
	| is not available. You may change the value to correspond to any of
	| the language folders that are provided through your application.
	|
	*/

	'fallback_locale' => 'en',

	/*
	|--------------------------------------------------------------------------
	| Encryption Key
	|--------------------------------------------------------------------------
	|
	| This key is used by the Illuminate encrypter service and should be set
	| to a random, 32 character string, otherwise these encrypted strings
	| will not be safe. Please do this before deploying an application!
	|
	*/

	'key' => 'bLCJBtzEFcZWOqRmQwRwCZajqiA7LfJK',

	/*
	|--------------------------------------------------------------------------
	| Autoloaded Service Providers
	|--------------------------------------------------------------------------
	|
	| The service providers listed here will be automatically loaded on the
	| request to your application. Feel free to add your own services to
	| this array to grant expanded functionality to your applications.
	|
	*/

	'providers' => array(

		'Illuminate\Foundation\Providers\ArtisanServiceProvider',
		'Illuminate\Auth\AuthServiceProvider',
		'Illuminate\Cache\CacheServiceProvider',
		'Illuminate\Session\CommandsServiceProvider',
		'Illuminate\Foundation\Providers\ConsoleSupportServiceProvider',
		'Illuminate\Routing\ControllerServiceProvider',
		'Illuminate\Cookie\CookieServiceProvider',
		'Illuminate\Database\DatabaseServiceProvider',
		'Illuminate\Encryption\EncryptionServiceProvider',
		'Illuminate\Filesystem\FilesystemServiceProvider',
		'Illuminate\Hashing\HashServiceProvider',
		'Illuminate\Html\HtmlServiceProvider',
		'Illuminate\Log\LogServiceProvider',
		'Illuminate\Mail\MailServiceProvider',
		'Illuminate\Database\MigrationServiceProvider',
		'Illuminate\Pagination\PaginationServiceProvider',
		'Illuminate\Queue\QueueServiceProvider',
		'Illuminate\Redis\RedisServiceProvider',
		'Illuminate\Remote\RemoteServiceProvider',
		'Illuminate\Auth\Reminders\ReminderServiceProvider',
		'Illuminate\Database\SeedServiceProvider',
		'Illuminate\Session\SessionServiceProvider',
		'Illuminate\Translation\TranslationServiceProvider',
		'Illuminate\Validation\ValidationServiceProvider',
		'Illuminate\View\ViewServiceProvider',
		'Illuminate\Workbench\WorkbenchServiceProvider',
        'Way\Generators\GeneratorsServiceProvider',
        'docomo\Models\Events\EventsServiceProvider',
        'Barryvdh\Debugbar\ServiceProvider',
		'Laracasts\Validation\ValidationServiceProvider',
		'Laracasts\Commander\CommanderServiceProvider',
		'Jenssegers\Agent\AgentServiceProvider',

	),

	/*
	|--------------------------------------------------------------------------
	| Service Provider Manifest
	|--------------------------------------------------------------------------
	|
	| The service provider manifest is used by Laravel to lazy load service
	| providers which are not needed for each request, as well to keep a
	| list of all of the services. Here, you may set its storage spot.
	|
	*/

	'manifest' => '/home/prod_vote/src/app/storage/meta',

	/*
	|--------------------------------------------------------------------------
	| Class Aliases
	|--------------------------------------------------------------------------
	|
	| This array of class aliases will be registered when this application
	| is started. However, feel free to register as many as you wish as
	| the aliases are "lazy" loaded so they don't hinder performance.
	|
	*/

	'aliases' => array(

		'App'             => 'Illuminate\Support\Facades\App',
		'Artisan'         => 'Illuminate\Support\Facades\Artisan',
		'Auth'            => 'Illuminate\Support\Facades\Auth',
		'Blade'           => 'Illuminate\Support\Facades\Blade',
		'Cache'           => 'Illuminate\Support\Facades\Cache',
		'ClassLoader'     => 'Illuminate\Support\ClassLoader',
		'Config'          => 'Illuminate\Support\Facades\Config',
		'Controller'      => 'Illuminate\Routing\Controller',
		'Cookie'          => 'Illuminate\Support\Facades\Cookie',
		'Crypt'           => 'Illuminate\Support\Facades\Crypt',
		'DB'              => 'Illuminate\Support\Facades\DB',
		'Eloquent'        => 'Illuminate\Database\Eloquent\Model',
		'Event'           => 'Illuminate\Support\Facades\Event',
		'File'            => 'Illuminate\Support\Facades\File',
		'Form'            => 'Illuminate\Support\Facades\Form',
		'Hash'            => 'Illuminate\Support\Facades\Hash',
		'HTML'            => 'Illuminate\Support\Facades\HTML',
		'Input'           => 'Illuminate\Support\Facades\Input',
		'Lang'            => 'Illuminate\Support\Facades\Lang',
		'Log'             => 'Illuminate\Support\Facades\Log',
		'Mail'            => 'Illuminate\Support\Facades\Mail',
		'Paginator'       => 'Illuminate\Support\Facades\Paginator',
		'Password'        => 'Illuminate\Support\Facades\Password',
		'Queue'           => 'Illuminate\Support\Facades\Queue',
		'Redirect'        => 'Illuminate\Support\Facades\Redirect',
		'Redis'           => 'Illuminate\Support\Facades\Redis',
		'Request'         => 'Illuminate\Support\Facades\Request',
		'Response'        => 'Illuminate\Support\Facades\Response',
		'Route'           => 'Illuminate\Support\Facades\Route',
		'Schema'          => 'Illuminate\Support\Facades\Schema',
		'Seeder'          => 'Illuminate\Database\Seeder',
		'Session'         => 'Illuminate\Support\Facades\Session',
		'SSH'             => 'Illuminate\Support\Facades\SSH',
		'Str'             => 'Illuminate\Support\Str',
		'URL'             => 'Illuminate\Support\Facades\URL',
		'Validator'       => 'Illuminate\Support\Facades\Validator',
		'View'            => 'Illuminate\Support\Facades\View',
        'Ardent'          => 'LaravelBook\Ardent\Ardent',
        'ErrorDisplay'    => 'docomo\Models\Listeners\ErrorDisplay',
        'Constants'       => 'Docomo\Common\Constants',
        'Agent'           => 'Jenssegers\Agent\Facades\Agent',
        'ConstantArrays'  => 'docomo\Models\Helpers\ConstantArrays',
	),

);
