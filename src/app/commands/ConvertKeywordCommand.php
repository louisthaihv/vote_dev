<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ConvertKeywordCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'convert:keyword';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update keyword of convert data.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $keywords = \DB::select('SELECT id, keyword_name FROM keywords');
        foreach ($keywords as &$keyword) {
            $convertKeyword = trim(mb_convert_kana($keyword->keyword_name, 'rnKV'));
            if ($keyword->keyword_name !== $convertKeyword) {
                $dataCheck = \DB::select('SELECT id FROM keywords WHERE keyword_name = ? AND id <> ?', [$convertKeyword, $keyword->id]);
                if (isset($dataCheck) && count($dataCheck) > 0) {
                    \DB::update('UPDATE question_tags SET keyword_id = ? WHERE keyword_id = ?', [$dataCheck[0]->id, $keyword->id]);
                    \DB::update('UPDATE keywords SET is_visible = 0 WHERE id = ?', [$keyword->id]);
                } else {
                    \DB::update('UPDATE keywords SET keyword_name = ? WHERE id = ?', [$convertKeyword, $keyword->id]);
                }
                unset($dataCheck);
            }
            unset($convertKeyword);
            unset($keyword);
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
