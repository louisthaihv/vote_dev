yum remove -y git*
yum install -y zlib-devel perl-devel gettext gcc curl-devel
wget https://www.kernel.org/pub/software/scm/git/git-2.1.2.tar.gz
tar zxvf git-2.1.2.tar.gz
cd git*
./configure
make
make install
