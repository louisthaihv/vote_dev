<?php

namespace docomo\Models\Codes;

class QuestionViewable
{
    public static $label = [
        DISPLAY   => '表示する',
        HIDE_LIST => '一覧から非表示',
        HIDE_ALL  => '完全非表示'
    ];
}
