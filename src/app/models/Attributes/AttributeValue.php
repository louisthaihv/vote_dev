<?php

namespace docomo\Models\Attributes;

use docomo\Models\Attributes\Attribute;

class AttributeValue extends \BaseArdentModel
{
    public static $cacheName = 'AttributeValues';
    public static $attributeValueData = null;
    /*
     * Model relationships
     */

    function attribute()
    {
        return $this->belongsTo('docomo\Models\Attributes\Attribute');
    }

    function userAttributes()
    {
        return $this->hasMany('docomo\Models\Users\UserAttribute');
    }

    public static function getListForDDL($attribute)
    {

        return AttributeValue::where('attribute_id', '=', $attribute)->get(['id', 'attribute_value_name']);
    }

    public static function getDataByCache($attribute_id = null)
    {
        if (is_null(AttributeValue::$attributeValueData)) {
            $updateTime = AttributeValue::checkCache(AttributeValue::$cacheName);
            AttributeValue::$attributeValueData = \Cache::get(AttributeValue::$cacheName, function () use ($updateTime) {
                $attribute_value_data = AttributeValue::select('sort_order', 'id', 'attribute_id', 'attribute_value',
                    'attribute_value_name', 'attribute_value_directory', 'is_default')->orderBy('sort_order')->get();
                $attribute_values = [];
                foreach ($attribute_value_data as $rec) {
                    if (!array_key_exists($rec->attribute_id, $attribute_values)) {
                        $attribute_values[$rec->attribute_id] = [];
                    }
                    $attribute_values[$rec->attribute_id][$rec->id] = $rec;
                }

                \Cache::put(AttributeValue::$cacheName, $attribute_values, DEFAULT_CACHE_TIME);
                AttributeValue::setCacheTime(AttributeValue::$cacheName, $updateTime);

                return $attribute_values;
            });
        }

        if (is_null($attribute_id)) {

            return AttributeValue::$attributeValueData;
        } else {

            return AttributeValue::$attributeValueData[$attribute_id];
        }
    }
}
