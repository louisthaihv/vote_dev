@section('head')
<link rel="stylesheet" href="{{ url('common/admin/css/jquery-ui-1.8.23.custom.css') }}" type="text/css" />
<script type="text/javascript" src=" {{ url('common/admin/js/jquery-ui-1.8.23.custom.min.js') }}"></script>
<script type="text/javascript" src=" {{ url('common/admin/js/jquery.ui.datepicker-ja.js') }}"></script>
@stop

@section('script')
<script type="text/javascript">
$(window).load(function(){

    //日付ピッカー
    $("input[name='from_date']").datepicker({
        dateFormat: 'yymmdd'
    });
    //日付ピッカー
    $("input[name='to_date']").datepicker({
        dateFormat: 'yymmdd'
    });
    
});
</script>
@stop

@include('admin.share.errors', ['errorList' => $errors])
{{ Form::model($notice, ['url' => '/manage/notice/confirm', 'method' => 'post']) }}
    <table class="formTable">
        <tr>
            <th>タイトル<span class="require">（必須）</span></th>
            <td>{{ Form::text('title', $notice->title, ['class' => 'xx-large']) }}</td>
        </tr>
        <tr>
            <th>概要<span class="require">（必須）</span></th>
            <td>{{ Form::text('description', $notice->description, ['class' => 'xx-large']) }}</td>
        </tr>
        <tr>
            <th>内容<span class="require">（必須）</span><br />HTML可</th>
            <td>{{ Form::textarea('details', $notice->details, ['class' => 'xx-large', 'rows' => 10]) }}</td>
        </tr>
        <tr>
            <th>表示デバイス</th>
            <td>
                <label><input type="checkbox" name="visible_device_i" {{ ($notice->visible_device_i) ? 'checked=checked' : '' }}/> i版で表示</label>
                <label><input type="checkbox" name="visible_device_d" {{ ($notice->visible_device_d) ? 'checked=checked' : '' }}/> d版で表示</label>
            </td>
        </tr>
        <tr>
            <th>掲載期間<span class="require">（必須）</span></th>
            <td>
            {{ Form::dateTimeControl('from', $notice->from) }}
                <br />
                <br />
                ～<br />
                <br />
            {{ Form::dateTimeControl('to', $notice->to) }}
            </td>
        </tr>
    </table>
    <div class="tcenter m-margint">
        <button type="submit" name="commit" class="commitBtn">確認</button>
    </div>
{{ Form::close() }}