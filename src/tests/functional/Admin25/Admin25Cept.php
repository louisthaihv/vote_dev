<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('edit account');

$I->amOnRoute('admin.account.edit');
$I->amOnAction('Admin\SurveyController@create');
