<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('Add more Genre');

$I->amOnPage('/manage/genre/add');
$I->amOnAction('Admin\SurveyController@create');
