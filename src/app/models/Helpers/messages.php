<?php

/**
 * Define naming conventions
 * - prefix
 *      I : Information
 *      W : Warning (form and action)
 *      A : Application Error
 *      S : System Error
 *      C : Confirmation
 *      L : View Label
 *
 * - secondly
 *      If prefix is 'I' or 'W' then
 *          MSG : Message
 *          FRM : Form Validation Message
 *
 * - thirdly
 *      REQUIRED : required form
 *      UNIQUE   : unique form
 *      etc...
 *
 * example)
 *      I_MSG_SUCCESS       : successful
 *      W_MSG_FAILED        : failed by action
 *      W_FRM_REQUIRED_NAME : name field is required.
 *      A_APPLICATION_ERROR : application error
 *      S_SYSTEM_ERROR      : system error
 *      C_REGISTRATION      : registration confirm message
 *      L_EDIT              : 'edit' label
 */

/**
 * Define buttons
 */
define('RE_SEARCH', '再検索');
define('REGISTRATION', '登録');
define('UPDATE', '更新');

/**
 * Define labels
 */
define('L_CLOSED_ACCOUNT', 'クローズ済');
define('L_VISIBLE', '表示');
define('L_HIDDEN', '非表示中');
define('L_COLUMN', 'コラム');

// label My Page
define('L_MY_PAGE', 'マイページ');
define('L_QUESTION_ANSWERED_FINISH', '投票済みアンケートの結果');
define('L_QUESTION_ANSWERED_READY', '投票済みアンケート(受付中)');
define('L_BTN_PROFILE', 'ブロフィール設定・変更');



/**
 * Define confirmation message
 */
define('C_REGISTRATION',                '登録します。よろしいですか？');
define('C_UPDATE',                      '更新します。よろしいですか？');
define('C_DELETE',                      '削除します。よろしいですか？');
define('C_DELETE_WITH_ITEM',            'を削除します。よろしいですか？');
define('C_UPDATE_PARENT_GENRE',         '大ジャンルを更新します。よろしいですか？');
define('C_UPDATE_CHILD_GENRE',          '小ジャンルを更新します。よろしいですか？');
define('C_ADD_CHILD_GENRE',             '小ジャンルを追加します。よろしいですか？');
define('C_SORT_ORDER',                  '表示順を更新します。よろしいですか？');
define('C_SAVE_DRAFT',                  '下書きとして保存します。よろしいですか？');
define('C_OVERWRITE',                   '上書きします。よろしいですか？');
define('C_REQUEST_APPROVE',             'この内容で承認申請します。よろしいですか？');
define('C_APPROVAL',                    '本番に反映します。よろしいですか？');
define('C_REJECT',                      'この設問を差し戻します。よろしいですか？');
define('C_REQUEST_CANCEL',              '承認申請状態から下書きに戻します。よろしいですか？');

/**
 * Define warning messages by action
 */
define('W_MSG_FAILED_LOGIN',            'ログインに失敗しました。');
define('W_MSG_NOT_PERMISSION',          '表示権限がありません。');

/**
 * Define warning messages by form validation
 */
// Login Form & Account Form
define('W_FRM_REQUIRED_LOGIN_ID',       'ログインIDを入力してください。');
define('W_FRM_REQUIRED_LOGIN_PASSWORD', 'パスワードを入力してください。');

// Question
define('W_FRM_REQUIRED_QUESTION_DETAILS',       '説明を入力してください。');
define('W_FRM_REQUIRED_TYPE',                   'タイプを選択してください。');
define('W_FRM_REQUIRED_CHOICES',                '選択肢を入力してください。');
define('W_FRM_REQUIRED_SUMMARY_VALUE',          '集計用数値を入力してください。');
define('W_FRM_REQUIRED_AVERAGE_UNIT',           '集計時の単位を入力してください。');
define('W_FRM_REQUIRED_OTHER_TEXT',             'その他として表示するテキストを入力してください。');
define('W_FRM_MAX_OTHER_TEXT',                  'その他として表示するテキストは60文字以内で入力してください。');
define('W_FRM_FORMAT_SUMMARY_VALUE',            '集計用数値には、数字のみを入力してください。');
define('W_FRM_MAX_CHOICES',                     '選択肢は60文字以内で入力してください。');
define('W_FRM_MAX_SUMMARY_VALUE',               '集計用数値は10桁以内で入力してください。');
define('W_FRM_MAX_QUESTION_DESCRIPTION',        '概要は200文字以内で入力してください。');
define('W_FRM_MAX_QUESTION_DETAILS',            '説明は10,000文字以内で入力してください。');
define('W_FRM_MAX_RESULT_COMMENT',              '結果ページ用コメントは10,000文字以内で入力してください。');
define('W_FRM_MAX_NAVIGATION_TITLE',            '送客枠見出しは60文字以内で入力してください。');
define('W_FRM_MAX_BACK_BUTTON_TEXT',            'フッターの戻るリンク（タイトル）は60文字以内で入力してください。');
define('W_FRM_MAX_BACK_BUTTON_URL',             'フッターの戻るリンク（URL）は255文字以内で入力してください。');
define('W_FRM_FORMAT_BACK_BUTTON_URL',          'フッターの戻るリンク（URL）にはURLを入力してください。');
define('W_FRM_REQUIRED_BACK_BUTTON',            'フッターの戻るリンク設定時はタイトルとURLの両方を入力してください。');
define('W_FRM_MAX_SNS_COMMENT',                 'SNS用コメントは60文字以内で入力してください。');
define('W_FRM_REQUIRED_GENRE',                  'ジャンルを選択してください。');
define('W_FRM_MAX_COMMENT',                     'コメントは10,000文字以内で入力してください。');
define('W_FRM_DENY_TAG_QUESTION_DESCRIPTION',   '概要にタグは入力できません。');
define('W_FRM_DENY_TAG_OTHER_TEXT',             'その他として表示するテキストにはタグは入力できません。');
define('W_FRM_DENY_TAG_NAVIGATION_TITLE',       '送客枠見出しにタグは入力できません。');
define('W_FRM_DENY_TAG_BACK_BUTTON_TEXT',       'フッターの戻るボタンにタグは入力できません。');
define('W_FRM_DENY_TAG_SNS_COMMENT',            'SNS用コメントにタグは入力できません。');
define('W_FRM_DENY_TAG_CHOICES',                '選択肢にタグは入力できません。');
define('W_FRM_DENY_CHAR_QUESTION_DETAILS',      '説明に使用できない文字が含まれています。');
define('W_FRM_DENY_CHAR_QUESTION_DESCRIPTION',  '概要に使用できない文字が含まれています。');
define('W_FRM_DENY_CHAR_AVERAGE_UNIT',          '集計時の単位に使用できない文字が含まれています。');
define('W_FRM_DENY_CHAR_OTHER_TEXT',            'その他として表示するテキストに使用できない文字が含まれています。');
define('W_FRM_DENY_CHAR_RESULT_COMMENT',        '結果ページ用コメントに使用できない文字が含まれています。');
define('W_FRM_DENY_CHAR_NAVIGATION_TITLE',      '送客枠見出しに使用できない文字が含まれています。');
define('W_FRM_DENY_CHAR_BACK_BUTTON_TEXT',      'フッターの戻るボタンに使用できない文字が含まれています。');
define('W_FRM_DENY_CHAR_SNS_COMMENT',           'SNS用コメントに使用できない文字が含まれています。');
define('W_FRM_DENY_CHAR_COMMENT',               'コメントに使用できない文字が含まれています。');
define('W_FRM_DENY_CHAR_CHOICES',               '選択肢に使用できない文字が含まれています。');

define('W_FRM_ALLOW_TAG_QUESTION_DESCRIPTION',  '説明に、SCRIPTタグと、各種onイベント属性は記述できません。');

define('W_FRM_REQUIRED_URL',                    'URLを入力してください。');
define('W_FRM_MAX_NAVIGATION_DESCRIPTION',      '紹介文は60文字以内で入力してください。');
define('W_FRM_FORMAT_NAVIGATION_URL',           'URLが正しくありません。');
define('W_FRM_MAX_NAVIGATION_URL',              'URLは255文字以内で入力してください。');
define('W_FRM_REQUIRED_NAVIGATION_DESCRIPTION', '紹介文を入力してください。');
define('W_FRM_DENY_TAG_NAVIGATION_DESCRIPTION', '紹介文にタグは入力できません。');
define('W_FRM_DENY_CHAR_NAVIGATION_DESCRIPTION', '紹介文に使用できない文字が含まれています。');

define('W_FRM_REQUIRED_COLUMN_TITLE',           'コラムタイトルを入力してください。');
define('W_FRM_REQUIRED_COLUMN_DETAILS',         'コラム内容を入力してください。');
define('W_FRM_REQUIRED_COLUMN_DESCRIPTION',     'コラム概要を入力してください。');
define('W_FRM_REQUIRED_VIEW_DATETIME',          'コラム作成日を入力してください。');
define('W_FRM_FORMAT_VIEW_DATETIME',            'コラム作成日が日付として正しくありません。');
define('W_FRM_LENGTH_VIEW_DATETIME',            'コラム作成日は8桁の日付(YYYYMMDD)で入力してください。');
define('W_FRM_DENY_TAG_COLUMN_TITLE',           'コラムタイトルにタグは入力できません。');
define('W_FRM_DENY_CHAR_COLUMN_TITLE',          'コラムタイトルに使用できない文字が含まれています。');
define('W_FRM_DENY_CHAR_COLUMN_DETAILS',        'コラム内容に使用できない文字が含まれています。');
define('W_FRM_DENY_TAG_COLUMN_DESCRIPTION',     'コラム概要にタグは入力できません。');
define('W_FRM_DENY_CHAR_COLUMN_DESCRIPTION',    'コラム概要に使用できない文字が含まれています。');
define('W_FRM_MAX_COLUMN_TITLE',                'コラムタイトルは60文字以内で入力してください。');
define('W_FRM_MAX_COLUMN_DETAILS',              'コラム内容は10,000文字以内で入力してください。');
define('W_FRM_MAX_COLUMN_DESCRIPTION',          'コラム概要は200文字以内で入力してください。');

// Account Form
define('W_FRM_UNIQUE_LOGIN_ID',         '入力されたアカウントIDは既に使われています。');
define('W_FRM_ALPHANUM_LOGIN_ID',       'アカウントIDは半角英数で入力してください。');
define('W_FRM_ALPHANUM_LOGIN_PASSWORD', 'パスワードは半角英数で入力してください。');
define('W_FRM_MAX_LOGIN_ID',            'アカウントIDは60文字以内で入力してください。');
define('W_FRM_REQUIRED_ACCOUNT_NAME',   'アカウント名を入力してください。');
define('W_FRM_UNIQUE_ACCOUNT_NAME',     '入力されたアカウント名は既に使われています。');
define('W_FRM_MAX_ACCOUNT_NAME',        'アカウント名は60文字以内で入力してください。');
define('W_FRM_MAX_LOGIN_PASSWORD',      'パスワードは60文字以内で入力してください。');
define('W_FRM_DENY_TAG_ACCOUNT_NAME',   'アカウント名にタグは入力できません。');
define('W_FRM_DENY_CHAR_ACCOUNT_NAME',  'アカウント名に使用できない文字が含まれています。');
define('W_FRM_MAX_ACCOUNT_COMMENT',     'コメントは500文字以内で入力してください。');

// Keyword Form
define('W_FRM_REQUIRED_KEYWORD',        'キーワード名を入力してください。');
define('W_FRM_UNIQUE_KEYWORD',          '入力されたキーワード名は既に登録されています。');
define('W_FRM_MAX_KEYWORD',             'キーワード名は60文字以内で入力してください。');
define('W_FRM_DENY_TAG_KEYWORD',        'キーワード名にタグは入力できません。');
define('W_FRM_DENY_CHAR_KEYWORD',       'キーワード名に使用できない文字が含まれています。');

// Attention Keyword
define('W_FRM_REQUIRED_KEYWORD_ID',     'キーワードを選択してください。');

// Feature
define('W_FRM_FORMAT_QUESTION_ID',      'アンケートIDの入力内容が正しくありません。');
define('W_FRM_MAX_QUESTION_ID',         'アンケートIDは9桁未満で指定してください。');

// Genre
define('W_FRM_REQUIRED_PARENT_GENRE',       '大ジャンル名を入力してください。');
define('W_FRM_MAX_PARENT_GENRE',            '大ジャンル名は60文字以内で入力してください。');
define('W_FRM_REQUIRED_DIRECTORY',          'ディレクトリ名を入力してください。');
define('W_FRM_ALPHANUM_DIRECTORY',          'ディレクトリ名は半角英数(英字は小文字)、-、_ の何れかで入力してください。');
define('W_FRM_MAX_DIRECTORY',               'ディレクトリ名は60文字以内で入力してください。');
define('W_FRM_UNIQUE_PARENT_DIRECTORY',     '入力されたディレクトリ名は、他の大ジャンルで既に使われています。');
define('W_FRM_UNIQUE_CHILD_DIRECTORY',      '入力されたディレクトリ名は、この大ジャンル内の他の小ジャンルで既に使われています。');
define('W_FRM_FORMAT_SORT_ORDER',           '表示順には数字を入力してください。');
define('W_FRM_MAX_SORT_ORDER',              '表示順は10,000以下で指定してください。');
define('W_FRM_REQUIRED_CHILD_NAME',         '小ジャンル名を入力してください。');
define('W_FRM_REQUIRED_CHILD_DESCRIPTION',  '小ジャンルの説明を入力してください。');
define('W_FRM_MAX_CHILD_NAME',              '小ジャンル名は60文字以内で入力してください。');
define('W_FRM_MAX_CHILD_DESCRIPTION',       '小ジャンルの説明は60文字以内で入力してください。');
define('W_FRM_DENY_TAG_PARENT_GENRE',       '大ジャンル名にタグは入力できません。');
define('W_FRM_DENY_TAG_CHILD_NAME',         '小ジャンル名にタグは入力できません。');
define('W_FRM_DENY_TAG_CHILD_DESCRIPTION',  '小ジャンルの説明にタグは入力できません。');
define('W_FRM_DENY_CHAR_PARENT_GENRE',      '大ジャンル名に使用できない文字が含まれています。');
define('W_FRM_DENY_CHAR_CHILD_NAME',        '小ジャンル名に使用できない文字が含まれています。');
define('W_FRM_DENY_CHAR_CHILD_DESCRIPTION', '小ジャンルの説明に使用できない文字が含まれています。');

// Notice
define('W_FRM_ACCEPT_TAGS',                 'SCRIPTタグと、各種onイベント属性は記述できません。');
define('W_FRM_BEFORE_NOW_DATE',             '日付には現在より前を指定してください。');

// Common
define('W_FRM_REQUIRED_DATE_FROM',          '期間開始日を入力してください。');
define('W_FRM_REQUIRED_DATE_TO',            '期間終了日を入力してください。');
define('W_FRM_COMPLEX_DATE_TO_AFTER_FROM',  '期間終了日は期間開始日以降にしてください。');
define('W_FRM_LENGTH_DATE_FROM',            '期間開始日は8桁の日付(YYYYMMDD)で入力してください。');
define('W_FRM_LENGTH_DATE_TO',              '期間終了日は8桁の日付(YYYYMMDD)で入力してください。');
define('W_FRM_FORMAT_DATE_FROM',            '期間開始日が日付として正しくありません。');
define('W_FRM_FORMAT_DATE_TO',              '期間終了日が日付として正しくありません。');
define('W_FRM_REQUIRED_TITLE',              'タイトルを入力してください。');
define('W_FRM_REQUIRED_DESCRIPTION',        '概要を入力してください。');
define('W_FRM_REQUIRED_DETAILS',            '内容を入力してください。');
define('W_FRM_REQUIRED_MESSAGE',            'メッセージを入力してください。');
define('W_FRM_EXTENSION_THUMBNAIL',         '画像の種類は、JPEG / PNG / GIF のみです。');
define('W_FRM_MAXSIZE_THUMBNAIL',           'サムネイル画像に指定できるファイルサイズは、500KBまでです。');
define('W_FRM_MAX_TITLE',                   'タイトルは60文字以内で入力してください。');
define('W_FRM_MAX_SINGLE_DESCRIPTION',      '概要は60文字以内で入力してください。');
define('W_FRM_MAX_MULTI_DETAILS',           '内容は10,000文字以内で入力してください。');
define('W_FRM_MAX_MULTI_MESSAGE',           'メッセージは10,000文字以内で入力してください。');
define('W_FRM_DENY_TAG_TITLE',              'タイトルにタグは入力できません。');
define('W_FRM_DENY_TAG_DESCRIPTION',        '概要にタグは入力できません。');
define('W_FRM_DENY_TAG_DETAILS',            '内容にタグは入力できません。');
define('W_FRM_DENY_TAG_MESSAGE',            'メッセージにタグは入力できません。');
define('W_FRM_DENY_CHAR_TITLE',             'タイトルに使用できない文字が含まれています。');
define('W_FRM_DENY_CHAR_DESCRIPTION',       '概要に使用できない文字が含まれています。');
define('W_FRM_DENY_CHAR_DETAILS',           '内容に使用できない文字が含まれています。');
define('W_FRM_DENY_CHAR_MESSAGE',           'メッセージに使用できない文字が含まれています。');

define('I_MSG_NO_UNANSWERED', 'みんなの声をご利用いただき誠にありがとうございます。<br/>ただいま回答できるアンケートはありません。<br/>新しいアンケートの配信をお待ちください。');

// other (trash)
define('BTN_NOTICE_DASHBOARD',             'お知らせ一覧・登録・修正・削除');
define('BTN_NOTICE_CREATE_NEW',             '＋おしらせ追加');

define('C_LIST_UNANSWERED_SHOWMORE', 'もっと見る');
define('C_LIST_COLUMN_SHOWMORE', 'もっと見る');
define('ADD',          '登録');
define('I_MSG_ADD_KEYWORD',          '登録します。よろしいですか？');
