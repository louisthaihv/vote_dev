<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('check genre');
$I->amOnPage('/manage');
$I->checkAddInfo();
$I->checkValidDateInfoBeforeNow('2014-12-05', 'title', 'des', 'detail', './/*[@id="ui-datepicker-div"]/table/tbody/tr[1]/td[2]/a', './/*[@id="ui-datepicker-div"]/table/tbody/tr[1]/td[4]/a');
$I->checkValidDateInfoBeforeDateEnd('title', 'des', 'detail', './/*[@id="ui-datepicker-div"]/table/tbody/tr[3]/td[5]/a', './/*[@id="ui-datepicker-div"]/table/tbody/tr[2]/td[5]/a');

//create using datepicker: date_from: 2014-12-16 and date_to: 2014-12-31
$I->createNewInfo('info_1', 'info_overview_1', 'info_detail_1', './/*[@id="ui-datepicker-div"]/table/tbody/tr[3]/td[3]/a', './/*[@id="ui-datepicker-div"]/table/tbody/tr[5]/td[4]/a');
$I->checkEditInfo('info_1', 'info_detail_1');
$I->checkDeleteInfo();

//check HTML tag: a; span
$I->checkTag('info_1', '<p>Check Tag HTML</p>');
$I->createTagSpan('info_1', '<span>Create Tag Span</span>');
$I->createTagA('info_2', '<a href="http://www.w3schools.com/">Create Tag A</a>');
$I->editTag('info_1', '<p>Check Tag HTML</p>', '<a href="http://www.w3schools.com/">Create Tag A</a>');
$I->editTag('info_2', '<p>Check Tag HTML</p>', '<span>Create Tag Span</span>');
