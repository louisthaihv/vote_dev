@section('head')
    <link rel="stylesheet" href="{{ url('common/admin/css/jquery-ui-1.8.23.custom.css') }}" type="text/css" />
    <script type="text/javascript" src=" {{ url('common/admin/js/jquery-ui-1.8.23.custom.min.js') }}"></script>
    <script type="text/javascript" src=" {{ url('common/admin/js/jquery.ui.datepicker-ja.js') }}"></script>
@stop

@section('script')
<script type="text/javascript">
    $(window).load(function () {

        //日付ピッカー
        $("input[name='from_date']").datepicker({
            dateFormat: 'yymmdd'
        });
        //日付ピッカー
        $("input[name='to_date']").datepicker({
            dateFormat: 'yymmdd'
        });

        $('select[name="TYPE"]').change(function () {
            $('tr.typeChoice').hide();
            $('tr.typeChoice:eq(' + $('select[name="TYPE"] option').index($('select[name="TYPE"] option:selected')) + ')').show();
        });

        $('.commitBtn').click(function () {
            return confirm('{{ ($method == 'post') ? C_REGISTRATION : C_UPDATE }}');
        });

    });
</script>
@stop

@include('admin.share.errors', ['errorList' => $pickup->errors()])
{{ Form::model($pickup, ['url' => $url, 'method' => $method]) }}

<table class="formTable">
    <tr class="typeChoice">
        <th>アンケートID</th>
        <td>
            {{ $question->id }} :
            {{ $question->title }}
        </td>
    </tr>
    <tr>
        <th>受付中 / 結果<span class="require">（必須）</span></th>
        <td>
            {{ Form::select('type', $typeList) }}
        </td>
    </tr>
    <tr>
        <th>期間<span class="require">（必須）</span></th>
        <td>
            {{ Form::dateTimeControl('from', $pickup->from) }}
            <br />
            <br />
            ～<br />
            <br />
            {{ Form::dateTimeControl('to', $pickup->to) }}
        </td>
    </tr>
</table>

<div class="tcenter m-margint">
    <button type="submit" name="commit" id="submitBtn" class="commitBtn">{{ $button }}</button>
</div>

{{ Form::close() }}