<?php namespace Front;

use docomo\Models\Features\Feature;

class FeatureController extends \BaseController {

    public function __construct(){

    }

    public function detailFeature($id)
    {
        $feature = Feature::whereRaw('`from` < NOW()')->find($id);
        if (!isset($feature)) {
            return \Redirect::route('feature');
        }
        $past_features = Feature::getPastCount();
        return $this->renderView(\View::make('front.feature.detail', compact('feature', 'past_features')));
    }

    public function listFeature()
    {
        return $this->renderView(\View::make('front.feature.list'));
    }
}