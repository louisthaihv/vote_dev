@extends('layouts.front_fp')

@section('content')

    @include('layouts.front_elements._space')
    <div>{{ HTML::image('common/front/images/fp/tit4.gif', "プロフィール設定", array('width'=>"100%")) }}</div>
    @include('layouts.front_elements._space')
    <div style="text-align:center;">プロフィールの登録が完了いたしました。</div>
    @include('layouts.front_elements._space')
    <div style="text-align:center"><a href="{{ route('profile.edit', ['uid' => 'NULLGWDOCOMO']) }}" >プロフィール確認</a></div>
    <div style="text-align:center"><a href="{{ route('home.top', ['uid' => 'NULLGWDOCOMO']) }}">みんなの声TOPへ</a></div>
    @include('layouts.front_elements._space')

@endsection
