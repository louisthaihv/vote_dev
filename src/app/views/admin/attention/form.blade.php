@section('head')
<link rel="stylesheet" href="{{ url('common/admin/css/jquery-ui-1.8.23.custom.css') }}" type="text/css" />
<script type="text/javascript" src=" {{ url('common/admin/js/jquery-ui-1.8.23.custom.min.js') }}"></script>
<script type="text/javascript" src=" {{ url('common/admin/js/jquery.ui.datepicker-ja.js') }}"></script>
@stop

@section('script')
<script type="text/javascript">
    var confirmMsg = '';
$(window).load(function(){
    $('.commitBtn').click(function() {
        return confirm('{{{ $confirmMessage  }}}');
    });
    
    //日付ピッカー
    $("input[name='from_date']").datepicker({
        dateFormat: 'yymmdd'
    });
    //日付ピッカー
    $("input[name='to_date']").datepicker({
        dateFormat: 'yymmdd'
    });
    
    $('#keywordListOpen').click(function(){
        window.open('{{ route("manage.keyword.pick") }}', 'keywordwindow', 'width=400, height=300, menubar=no, toolbar=no, scrollbars=yes');
        return false;
    });
    var keyword_name = $("#keyword_name").val();
    $("#keywordList").text(keyword_name);
});
    @yield('message')
</script>
@stop

@include('admin.share.errors', ['errorList' => $attention->errors()])
{{ Form::model($attention, ['url' => $url, 'method' => $method]) }}
    <table class="formTable">
        <tr class="typeChoice">
            <th>キーワード<span class="require">（必須）</span><br />
                <a href="#" class="naviBtn" id="keywordListOpen">キーワードウィンドウを開く</a></th>
            <td>
                <div id="keywordList">{{{ ($attention->keyword_id != null) ? $attention->keyword->keyword_name : '' }}}</div>
                <input id="keywordId" value="{{ ($attention->keyword_id != null) ?  $attention->keyword->id : '' }}" name="keyword_id" type="hidden">
                <p id="keywordClear" class="m-margint" style="display:none;"></p>
        </td>
    </tr>
    <tr>
        <th>受付中 / 結果<span class="require">（必須）</span></th><td>
            {{ Form::select('tab',  docomo\Models\Codes\FrontTab::$label, $attention->tab) }}
        </td>
    </tr>
    <tr>
    <th>期間<span class="require">（必須）</span></th><td>
    {{ Form::dateTimeControl('from', $attention->from) }}
    <br />
    <br />
    ～<br />
    <br />
    {{ Form::dateTimeControl('to', $attention->to) }}
</td>
</tr>
</table>
    <div class="tcenter m-margint">
        <button type="submit" name="commit" class="commitBtn">{{ $button }}</button>
    </div>
{{ Form::close() }}
