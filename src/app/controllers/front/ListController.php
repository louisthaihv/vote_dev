<?php
namespace Front;

use docomo\Models\Features\Feature;
use docomo\Models\Genres\ChildGenre;
use docomo\Models\Genres\ParentGenre;
use docomo\Models\Keywords\Keyword;
use docomo\Models\Questions\Question;
use docomo\Models\Questions\ViewModel;

class ListController extends \BaseController
{
    
    public function __construct()
    {

    }

    public function myPage()
    {
        $user = $this->getCurrentUser();
        $question_finish_count = Question::getMyPageFinishCount($user);
        $question_readies = $this->getQuestionList('myPageReady', NUM_FIRST_PAGE, ['user' => $user]);

        return \View::make('front.list.mypage', compact('question_readies', 'question_finish_count'));
    }
    
    public function genre()
    {
        $genreList = ParentGenre::getDataByCache();
        
        return \View::make('front.list.genre.index', compact('genreList'));
    }
    
    public function genreEntry($parentDir, $childDir)
    {
        $child_genre = ChildGenre::select('genre_child_name')->visible()->whereGenreChildDirectory($childDir)->whereParentDirectory($parentDir)->first();
        if (is_null($child_genre)) {
            \App::abort(404);
        }
        $child_genre_name = $child_genre->genre_child_name;
        $params = ['parentDir' => $parentDir, 'childDir' => $childDir, 'user' => $this->getCurrentUser()];
        $questions = (getUserAgent() === CHAR_D_MENU) ? null : $this->getQuestionList('questionEntryByGenre', NUM_FIRST_PAGE, $params);
        $mode = PROCESSING;

        return \View::make('front.list.genre.entry', compact('params', 'child_genre_name', 'questions', 'mode'));
    }
    
    public function genreResult($parentDir, $childDir)
    {
        $child_genre = ChildGenre::select('genre_child_name')->visible()->whereGenreChildDirectory($childDir)->whereParentDirectory($parentDir)->first();
        if (is_null($child_genre)) {
            \App::abort(404);
        }
        $child_genre_name = $child_genre->genre_child_name;
        $params = ['parentDir' => $parentDir, 'childDir' => $childDir, 'user' => $this->getCurrentUser()];
        $questions = (getUserAgent() === CHAR_D_MENU) ? null : $this->getQuestionList('questionResultByGenre', NUM_FIRST_PAGE, $params);
        $mode = COMPLETE;

        return \View::make('front.list.genre.entry', compact('params', 'child_genre_name', 'questions', 'mode'));
    }

    public function tag_entry($id)
    {
        $keyword = Keyword::visible()->find($id);
        if (is_null($keyword)) {
            \App::abort(404);
        }
        $params = ['keyword_id' => $id];
        $keyword_name = $keyword->keyword_name;
        $questions = (getUserAgent() === CHAR_D_MENU) ? null : $this->getQuestionList('questionEntryByTag', NUM_FIRST_PAGE, $params);
        $mode = PROCESSING;
        
        return \View::make('front.list.tag.entry', compact('params', 'questions', 'keyword_name', 'mode'));
    }
    
    public function tag_result($id)
    {
        $keyword = Keyword::visible()->find($id);
        if (is_null($keyword)) {
            \App::abort(404);
        }
        $params = ['keyword_id' => $id];
        $keyword_name = $keyword->keyword_name;
        $questions = (getUserAgent() === CHAR_D_MENU) ? null : $this->getQuestionList('questionResultByTag', NUM_FIRST_PAGE, $params);
        $mode = COMPLETE;

        return \View::make('front.list.tag.entry', compact('params', 'questions', 'keyword_name', 'mode'));
    }

    public function popularEntry()
    {
        $mode = PROCESSING;
        
        return $this->renderView(\View::make('front.list.popular.popular-entry', compact('mode')));
    }

    public function popularResult()
    {
        $mode = COMPLETE;

        return $this->renderView(\View::make('front.list.popular.popular-entry', compact('mode')));
    }
    
    public function noAnswerList()
    {
        $questions = (getUserAgent() === CHAR_D_MENU) ? null : $this->getQuestionList('noAnswer', NUM_FIRST_PAGE);

        return \View::make('front.list.no-answer', compact('questions'));
    }
    
    public function newEntry()
    {
        $questions = (getUserAgent() === CHAR_D_MENU) ? null : $this->getQuestionList('newEntry', NUM_FIRST_PAGE);
        $mode = PROCESSING;

        return \View::make('front.list.new.new-entry', compact('questions', 'mode'));
    }
    
    public function newResult()
    {
        $questions = (getUserAgent() === CHAR_D_MENU) ? null : $this->getQuestionList('newResult', NUM_FIRST_PAGE);
        $mode = COMPLETE;

        return \View::make('front.list.new.new-entry', compact('questions', 'mode'));
    }
    
    public function getMoreQuestion()
    {
        $numList = \Input::get('numQuestions');
        $morePager = \Input::get('pager') + 1;
        $className = \Input::get('className');
        $questions = $this->getQuestionList($className, $numList);
        $hide_answer = null;
        $showNotView = null;
        $noList = null;
        if ($className === 'myPageFinish') {
            $hide_answer = true;
            $showNotView = true;
        }
        if ($className === 'noAnswer') {
            $noList = true;
        }
        $show_entry_tag = null;
        $showHasColumn = null;
        if (\Input::get('className') == 'searchResult') {
            $show_entry_tag = true;
            $showHasColumn = true;
        }

        return \View::make('front.list.more-list', compact('questions', 'morePager', 'hide_answer', 'showNotView', 'noList', 'show_entry_tag', 'showHasColumn'));
    }
    
    public function listColumn()
    {

        return $this->renderView(\View::make('front.list.column'));
    }
    
    public function getMoreColumn()
    {
        $numList = \Input::get('numQuestions');
        $morePager = \Input::get('pager') + 1;
        $questions = $this->getQuestionList(\Input::get('className'), $numList);
        
        return \View::make('front.list.more-column-list', compact('questions', 'morePager'));
    }

    public function getMoreFeature()
    {
        $morePager = \Input::get('pager') + 1;
        $date_rep = str_replace('__', ':', \Input::get('date', date_format(new \DateTime(), 'Y-m-d H:i')));
        $features = Feature::getListFront(PAGER_SIZE_FRONT, str_replace('_', ' ', $date_rep));
        unset($date_rep);

        return \View::make('front.feature.more-list', compact('features', 'morePager'));
    }

    public function getQuestionList($methodName, $limit, $params = [])
    {
        $viewModel = new ViewModel(new Question, $limit);
        if (!array_key_exists('user', $params)) {
            $params['user'] = $this->getCurrentUser();
        }
        if (!array_key_exists('date', $params)) {
            $date_rep = str_replace('__', ':', \Input::get('date', date_format(new \DateTime(), 'Y-m-d H:i')));
            $params['date'] = str_replace('_', ' ', $date_rep);
        }

        return $viewModel->getQuestionList($methodName, $params);
    }

    public function newList()
    {

        return $this->renderView(\View::make('front.list.new'));
    }

    public function popularList()
    {

        return $this->renderView(\View::make('front.list.popular'));
    }

    public function tag()
    {

        return $this->renderView(\View::make('front.list.tag.index'));
    }

    public function entry()
    {

        return $this->renderView(\View::make('front.list.accept'));
    }

    public function result()
    {

        return $this->renderView(\View::make('front.list.result'));
    }
}
