<?php
use Illuminate\Support\Facades\Log;

function jpDate($date, $format = 'Y年m月d日', $nullMessage = '未指定')
{

    if (!$date) {
        return $nullMessage;
    }

    if (is_string($date)) {
        $date = new DateTime($date);
    }

    return $date->format($format);
}

function displayAttrValue($attribute, $currentUser)
{
}

function calcPercent($value = 0, $base = 0){
   if($base == 0){
       return 0;
   }

   return (int)((100*$value)/$base);

}

function showTopChoice($hash, $attribute_value_id)
{
    if(isset($hash[$attribute_value_id]))
    {
        return $hash[$attribute_value_id]['topChoiceName'];
    }
    else
    {
        return '&nbsp;';
    }
}

function showAverage($hash, $attribute_value_id, $question)
{
    if(isset($hash[$attribute_value_id]) && isset($hash[$attribute_value_id]['average']) && $question->summary_type == SUMMARY_AVERAGE)
    {
        return number_format($hash[$attribute_value_id]['average']) . $question->average_unit;
    }
    else
    {
        return '-';
    }
}

function showTotalAverage($hash, $question)
{
    $values = array_values($hash);
    
    if(count($values) == 0){
        return '-';
    }

    $sum = 0;

    foreach($values as $val){
        $sum += $val['average'];
    }
   
    return number_format($sum / count($values)) . $question->average_unit;

}

function prefRank($totalData, $hash, $id)
{
    if(isset($hash[$id]) && isset($hash[$id]['topChoiceId'])){
        $cid = $hash[$id]['topChoiceId'];
        $rank = 1;
       
        foreach($totalData as $data){
           if($data->choice->choice_num == $cid){
               return 'rank' . $rank;
           }

           $rank++;
        }
    }

    return 'rank10';
}