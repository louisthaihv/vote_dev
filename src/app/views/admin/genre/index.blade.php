@extends('layouts.admin_in')
@section('script')

<script type="text/javascript">

$(window).load(function(){

    $('#searchBtn').click(function() {
        if ($('#hide_show').prop("checked")) {
            $(location).attr('href', "{{ action('Admin\\GenreController@index', SHOW_ALL) }}");
        } else {
            $(location).attr('href', "{{ route('manage.genre.index') }}");
        }
        return false;
    });

    $('.commitBtn').click(function(){
        return confirm('{{{ C_SORT_ORDER }}}');
    });
});
</script>
@stop
@section('content')

<h2>ジャンル一覧</h2>
<p class="m-margin">
<a href="{{ route('manage.genre.create') }}" class="naviBtn">＋ジャンル追加</a>
</p>
@include('admin.share.errors', ['errorList' => $errors])
{{ Form::open(['method' => 'put']) }}
    <label><input type="checkbox" name="hide_show" id="hide_show" {{ $show_all ? 'checked="checked"' : '' }}/> 非表示も見る</label> <a href="#" class="naviBtn" id="searchBtn" />再検索</a>
    <table class="list">
        <tr>
            <th>ID</th><th>大ジャンル名</th><th>ディレクトリ名</th><th>小ジャンル数</th><th>表示順</th><th>非表示</th>
        </tr>
        @foreach ($genres as $genre)
            @if ($show_all || $genre->parent_is_visible == VISIBLE)
            <tr>
                <td>{{ $genre->id }}</td>
                <td><a href="{{ route('manage.genre.edit', $genre->id) }}">{{ $genre->genre_parent_name }}</a></td>
                <td>{{{ $genre->genre_parent_directory }}}</td>
                <td>{{ $genre->getChildCount() }}</td>
                <td>{{ Form::text('parent_sort_order_' . $genre->id, $genre->parent_sort_order, ['class' => 'xx-small']) }}</td>
                <td>{{ $genre->parent_is_visible == VISIBLE ? '': L_HIDDEN }}</td>
            </tr>
            @endif
        @endforeach
        
    </table>
    <div class="tcenter m-margint">
        <button type="submit" name="commit" class="commitBtn">表示順を更新</button>
    </div>
{{ Form::close() }}
@stop