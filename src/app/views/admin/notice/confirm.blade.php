@extends('layouts.admin_in')
@section('head')
<link rel="stylesheet" href="{{ url('common/admin/css/jquery-ui-1.8.23.custom.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ url('common/front/css/info.css') }}" type="text/css" />
<script type="text/javascript" src=" {{ url('common/admin/js/jquery-ui-1.8.23.custom.min.js') }}"></script>
<script type="text/javascript" src=" {{ url('common/admin/js/jquery.ui.datepicker-ja.js') }}"></script>
@stop
@section('script')
<script type="text/javascript">
$(window).load(function(){
    $('#confirm').click(function(){
        return (confirm('{{ empty($notice->id) ? C_REGISTRATION : C_UPDATE }}'));
    });

    $('#back').click(function() {
        window.location = '{{ (is_null($notice->id)) ? '/manage/notice/create' : '/manage/notice/' . $notice->id . '/edit' }}';
    });
});
</script>
@stop
@section('content')
<h2>お知らせ確認</h2>
{{ Form::model($notice, (isset($notice->id) ? ['url' => '/manage/notice/' . $notice->id . '/edit', 'method' => 'put'] : ['url' => '/manage/notice/create', 'method' => 'post'])) }}
    @include('layouts.elements.displays.notice_box')
<br /><br />
<p>i版 : {{ ($notice->visible_device_i) ? '表示する' : '-' }} </p>
<p>d版 : {{ ($notice->visible_device_d) ? '表示する' : '-' }}</p><br />
<p>期間：{{ $notice->from }}&nbsp;&nbsp;～&nbsp;&nbsp;{{ $notice->to }}</p>
    <div class="tcenter m-margint">
        <button type="button" name="re-edit" class="commitBtn" id="back">編集しなおす</button>
        <button type="submit" name="commit" class="commitBtn entryBtn" id="confirm">登録</button>
    </div>
{{ Form::close() }}
@stop
