@extends('layouts.admin_in')

@section('head')
    <link rel="stylesheet" href="{{ url('common/admin/css/jquery-ui-1.8.23.custom.css') }}" type="text/css" />
    <script type="text/javascript" src=" {{ url('common/admin/js/jquery-ui-1.8.23.custom.min.js') }}"></script>
    <script type="text/javascript" src=" {{ url('common/admin/js/jquery.ui.datepicker-ja.js') }}"></script>
@stop

@section('script')
    <script type="text/javascript">
        var summaryData = {{ json_encode($result_data) }};
        var isAverage = {{ $question->isViewableAverage() ? 1 : 0 }};
    </script>
{{ HTML::script('common/admin/js/result.js') }}
{{ HTML::script('common/front/js/tab_change.js') }}
@stop

@section('style')
{{ HTML::style('common/front/css/result.css') }}
{{ HTML::style('common/admin/css/result.css') }}
@stop

@section('content')
<h2>{{ $question->title}} 【結果】</h2>
<div class="lrMargin m-margin">
    <p>総投票数：{{ number_format($question->vote_count) }}票</p>
</div>
<section class="resultTabArea">
    <ul class="resulttabs">
        <li class="active" url=""><a href="#">総合</a></li>

        @foreach ($attributes as $attribute)
            @if($question->attribute_id != $attribute->id)
                <li id="attribute_{{ $attribute->id }}" class="" url="/{{ $attribute->attribute_directory }}/{{ $selected[$attribute->id]->attribute_value_directory }}"><a href="#">{{{ $attribute->attribute_name }}}</a></li>
            @endif
        @endforeach
    </ul>

    <div class="resulttabs_content">
        <!--総合-->
        @include('front.result._content_total', ['csv' => $question->isViewableCsvLink()])

        @foreach ($attributes as $attribute)
            @if ($question->attribute_id != $attribute->id)
                @include('front.result._content_ranking', ['attribute_id' => $attribute->id, 'attribute_dir' => $attribute->attribute_directory, 'csv' => $question->isViewableCsvLink()])
            @endif
        @endforeach
    </div>
</section>

@if (isset($column) && $column->is_visible)
    <section class="columnArea l-margin" id="column">
        <h4 class="subSectionTitH4 columnIcon"><span>コラム</span></h4>
        <div class="m-margint columnBlock lrMargin">
            <div class="columnBox">
                <p class="columnTit">{{ $column->column_title }}</p>
                <div class="columnContents">
                    {{ $column->column_detail }}
                </div>
            </div><!--/columnBox-->
            <a href="#" class="nextNavi">▼つづきを読む</a>
        </div><!--columnBlock-->

    </section>
@endif
@include ('front.result.evaluation', ['hasVoted' => true])

<br /><br /><br /><br /><br />

@if($edit_column)
<hr />
<br />
<p id="edit_column">コラム登録</p>

@include('admin.share.errors', ['errorList' => $edit_column->errors()])
{{ Form::model($edit_column, ['route' => ['manage.question.result', 'id' => $question->id], 'method' => 'PUT']) }}
<table class="formTable">
    <tr>
        <th>コラムを表示するか</th>
        <td>
@if (Auth::user()->havePermission('is_manage_agree'))
            <label>{{ Form::checkbox('is_visible', null, $edit_column->is_visible); }} 表示する</label>
@else
    {{ Form::label('is_visible', ($edit_column->is_visible ? L_VISIBLE : L_HIDDEN)) }}
@endif
        </td>
    </tr>
    <tr>
        <th>コラムタイトル<span class="require">（必須）</span></th>
        <td>
@if ($edit_column->isEditable())
            {{ Form::text('column_title', $edit_column->column_title, ['class' => 'xx-large', 'id' => 'column_titles']) }}
@else
            {{ Form::label('column_title', $edit_column->column_title) }}
@endif
        </td>
    </tr>
    <tr>
        <th>コラム内容<span class="require">（必須）</span><br />（HTMLタグ可）</th>
        <td>
@if ($edit_column->isEditable())
            {{ Form::textarea('column_detail', $edit_column->column_detail, ['class' => 'xx-large', 'rows' => 30, 'id' => 'column_details']) }}<br />
            <a href="#" class="naviBtn" id="commentWindow">イメージを見る</a>
@else
            {{ Form::label('column_detail', $edit_column->column_detail) }}
@endif
        </td>
    </tr>
    <tr>
        <th>コラム概要(最大200文字)<span class="require">（必須）</span><br />（description用）</th>
        <td>
@if ($edit_column->isEditable())
            {{ Form::text('description', $edit_column->description, ['class' => 'xx-large', 'id' => 'description']) }}<br />
            <button type="button" id="copyDetails" class="naviBtn">説明をコピー</button>
@else
            {{ Form::label('description', $edit_column->description) }}
@endif
        </td>
    </tr>
    <tr>
        <th>コラム作成日<span class="require">（必須）</span></th>
        <td>
@if ($edit_column->isEditable())
    {{ Form::dateTimeControl('view_create_datetime', $edit_column->view_create_datetime, 1) }}
@else
    {{ Form::label('view_create_datetime', $edit_column->view_create_datetime) }}
@endif
        </td>
    </tr>
</table>
<br />
<div class="tcenter m-margint">
@if ($edit_column->isEditable())
    <button type="submit" name="commit" class="commitBtn" onclick="return confirm('{{ C_UPDATE }}');">更新</button>
@endif
</div>
{{ Form::close() }}
@endif

@stop