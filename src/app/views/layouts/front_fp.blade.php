<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//i-mode group (ja)//DTD XHTML i-XHTML(Locale/Ver.=ja/1.1) 1.0//EN" "i-xhtml_4ja_10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    @include('layouts.front_elements._metatag_fp')

    @yield('head')
    @yield('script')
    @yield('style')

</head>
<body link="#0000FF" vlink="#0000FF" style="font-size:small;">

@include('layouts.front_elements._header_fp') {{--ok--}}
@if (Route::currentRouteName() != 'notice.detail')
    @include('layouts.front_elements._news_fp') {{--ok--}}
@endif

@yield('content')

@if((Route::currentRouteName() == 'list.mypage'))
    <div style="text-align:center;">&#xE6AE;<a href="{{ route('profile.edit', ['uid' => 'NULLGWDOCOMO']) }}">ﾌﾟﾛﾌｨｰﾙ設定・変更</a></div>
@elseif( !in_array(Route::currentRouteName(), ['entry.show'
        , 'home.top'
        , 'entry.answer'
        , 'entry.update_user'
        , 'result.show'
        , 'result.vote'
        , 'list.genre'
        , 'profile.edit'
        , 'profile.complete'
        , 'profile.update'
        , 'list.mypage']))
    @include('layouts.front_elements._profileBn_fp')
@endif
@include('layouts.front_elements._space')

@include('layouts.front_elements._menu_fp')
@include('layouts.front_elements._footer_fp')
{{--@include('layouts.front_elements._footercontents')--}}
{{--@yield('additional_footer')--}}
@include('layouts.front_elements.ga')
</body>
</html>




