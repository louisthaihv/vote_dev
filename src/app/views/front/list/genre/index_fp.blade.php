@extends('layouts.front_fp')

@section('content')

    @include('layouts.front_elements._space')
    <div>{{ HTML::image('common/front/images/fp/tit5.gif', 'ｼﾞｬﾝﾙ一覧', array('width' => '100%')) }}</div>
    @include('layouts.front_elements._space')

    @foreach($genreList as $parentGenre)
    <div style="background-color:#fff6ab;padding:5px;text-align:left;"><font color="#f39800">▼</font><font color="#3e3a39"> {{ $parentGenre->genre_parent_name }} </font></div>
    @include('layouts.front_elements._space')
        <div style="margin:0px 10px 0px 10px;">
            @foreach(docomo\Models\Genres\ChildGenre::getDataByCache($parentGenre->id) as $childGenre)
            <div style="margin:0px 0px 7px 0px;">
                <a href="{{ route('list.genre.result', [$parentGenre->genre_parent_directory, $childGenre->genre_child_directory]) }}?uid=NULLGWDOCOMO">{{ $childGenre->genre_child_name }}</a>
            </div>
            @endforeach
        </div>
    @include('layouts.front_elements._gotop_fp')
    @endforeach

@endsection


