<?php

use docomo\Models\Answers\TopSummaryByAttributeValue;
use docomo\Models\Questions\Question;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SummaryGenreCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'summary:genre';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Summary for result page.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info('start summary genre.');
		\DB::update('UPDATE genre_children SET record_count = ' .
			' ( SELECT COUNT(id) FROM questions WHERE questions.genre_child_id = genre_children.id ' .
			' AND questions.genre_parent_id = genre_children.genre_parent_id AND questions.visible_device_d = 1 '.
            ' AND questions.viewable_type = ? AND questions.status = ?) ',
			[DISPLAY, QUESTION_STATUS_FINISH]);
        \DB::update('UPDATE data_update_time SET update_time = NOW() WHERE cache_key IN (\'ChildGenre\', \'ParentGenre\')');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
