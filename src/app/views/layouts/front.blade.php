<!DOCTYPE HTML>
<html lang="ja">
<head>
    <meta http-equiv="X-FRAME-OPTIONS" content="DENY" />
    @yield('metaTag')
    @include('layouts.front_elements._metatag')

    @yield('head')
    @yield('script')
    @yield('style')

    @yield('before_tagmanager')
</head>
<body>
    @include('layouts.front_elements._tagmanager')
    <div id="container">
    <div id="wrapper">
    <div id="mainContents">
    @include('layouts.front_elements._header')
    @if(!in_array(Route::currentRouteName(), ['notice.detail', 'help']))
        @include('layouts.front_elements._news')
        @include('layouts.front_elements._header_banner')
    @endif

                    @yield('content')

                @include('layouts.front_elements._footercontents')
                @yield('additional_footer')
                @include('layouts.front_elements._footer')
            </div>
            <div id="mainMenu">
                <p class="mainMenuTit">メニュー</p>
                <nav>
                    <ul>
                        <li><a id="header_menu01" href="{{ route('home.top') }}">TOP</a></li>
                        <li><a id="header_menu02" href="{{ route('list.entry') }}">受付中アンケート</a></li>
                        <li><a id="header_menu03" href="{{ route('list.result') }}">結果発表アンケート</a></li>
                        <li><a id="header_menu07" href="{{ route('list.unanswered') }}">未回答のアンケート</a></li>
                        <li><a id="header_menu05" href="{{ route('list.new.entry') }}">新着アンケート</a></li>
                        <li><a id="header_menu06" href="{{ route('list.popular.entry') }}">注目のアンケート</a></li>
                        <li><a id="header_menu08" href="{{ route('list.genre') }}">ジャンル別アンケート</a></li>
                        <li><a id="header_menu09" href="{{ route('feature') }}">特集一覧</a></li>
                        <li><a id="header_menu10" href="{{ route('list.column') }}">コラム一覧</a></li>
                        <li><a id="header_menu04" href="{{ route('list.mypage') }}">マイページ</a></li>
                        <li><a id="header_menu11" href="{{ route('profile.edit') }}">プロフィール設定</a></li>
                        <li><a id="header_menu12" href="{{ route('help') }}">みんなの声について</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <p class="page-top"><a href="#top"></a></p>
</body>
</html>
