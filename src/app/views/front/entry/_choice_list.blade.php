<div class="answerBox l-margin">
    <ul>
        @foreach($choices  as $choice)
        <li>
            {{ Form::open(['url' => $question->getPath() . '/' . $choice->id]) }}
            {{ $choice->choice_text }}<button id="vote_link" type="submit" id="vote_{{ $choice->id }}">＞＞投票する</button>
            {{ Form::close() }}
        </li>
        @endforeach
    </ul>
</div>
<div class="answerBtnBottomArea l-margin">
    <button type="button" class="answerBtnBottom" id="vote_bottom">投票して結果を見る</button>
</div>
