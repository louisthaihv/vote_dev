@extends('layouts.front')

@section('metaTag')
    <title>{{ $question->title }} | みんなの声</title>
    <meta name="description" content="{{{ $question->description }}}">
    <meta property="og:title" content="{{{ $question->title }}} | みんなの声" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="{{{ $question->description }}}" />
    <meta name="twitter:title" content="{{{ $question->title }}} | みんなの声" />
    <meta name="twitter:description" content="{{{ $question->description }}}" />
@stop

@section('style')
    {{ HTML::style('common/front/css/entry.css?v1') }}
    {{ HTML::style('common/front/css/entry_additional.css') }}
@stop

@section('script')
    {{ HTML::script('common/front/js/entry.js?v1') }}
    <script>

        $(window).load(function () {
            $('.answerBox ul li').click(function () {
                $(this).addClass('active').siblings().removeClass('active');
                $('.answerBox ul li a').stop(0).animate({'marginLeft': '0px'}, {duration: 200});
                $('a', $(this)).stop(0).animate({'marginLeft': '-50px'}, {duration: 200});
                return false;
            });
        });

    </script>
@stop

@section('content')


    <section class="l-margin">
        <h2 class="entryTit m-margin"><span>投票</span></h2>
        <div class="lrMargin">
            <div class="questionInfoBox">
                <h1>{{ $question->title }}</h1>
                <div class="questionText">{{ $question->details }}</div>
            </div>

            <p class="entryCountInfo m-margin">現在<span class="boldFont">{{ number_format($question->vote_count) }}</span>人が投票中！</p>

            {{-- 締め切り後 --}}
            @if($question->status == QUESTION_STATUS_FINISH)

                {{-- ->締め切り後用画面表示--}}
                @include('front.entry._finished')

                {{-- 投票受付中 --}}
            @else

                {{-- 属性限定アンケート --}}
                @if($question->attribute_id)

                    {{-- ユーザー登録あり --}}
                    @if($currentUser)

                        <?php $userAttrValue = $currentUser->attrList[$question->attribute_id] ?>
                        {{-- 対象属性違い --}}
                        @if($userAttrValue && $userAttrValue != $question->attribute_value_id)

                            {{-- ->投票不可画面表示 --}}
                            @include('front.entry._limited_unmatch')

                            {{-- 属性未登録 --}}
                        @elseif(!$userAttrValue)

                            {{-- ->属性登録画面表示 --}}
                            @include('front.entry._limited_unregister')

                            {{-- それ以外（対象属性マッチ) --}}
                        @else

                            {{-- ->投票画面表示 --}}
                            @include('front.entry._choice_list')

                        @endif

                        {{-- ユーザー登録なし--}}
                    @else
                        {{-- ->属性登録画面表示 --}}
                        @include('front.entry._limited_unregister')

                    @endif

                    {{-- 属性未指定アンケート --}}
                @else
                    {{-- ->無条件で投票画面表示 --}}
                    @include('front.entry._choice_list')
                @endif

            @endif

            <p class="tcenter m-margin">
                {{ link_to_route('result.show', '投票せずに結果を見る', [$question->getParentGenreDirectory(), $question->getChildGenreDirectory(), $question->id, 'attribute' => null, 'attribute_value' => null, 'showresult' => true], ['class' => 'btnType02', 'id' => 'vote_throw']) }}
            </p>
            <p class="tcenter l-margin">
                {{ link_to_route('list.unanswered', '他のアンケートを見る', null, ['class' => 'btnType02', 'id' => 'vote_other']) }}
            </p>
        </div>
    </section>
    @include('layouts.front_elements._profileBn')

    @include('front.entry._bottomcontents')
@stop