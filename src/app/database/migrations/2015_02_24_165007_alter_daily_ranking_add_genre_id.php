<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDailyRankingAddGenreId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasColumn('daily_vote_ranking', 'genre_parent_id')) {
            Schema::table('daily_vote_ranking', function ($table) {
                $table->integer('genre_parent_id')->nullable()->unsigned()->after('question_id');
                $table->integer('genre_child_id')->nullable()->unsigned()->after('genre_parent_id');
                $table->index('genre_parent_id');
                $table->index('genre_child_id');
            });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
