
@if(count($errors->get($field)))
<div>
    @foreach($errors->get($field) as $error)
        <div class="error">{{ $error }}</div>
    @endforeach
</div>
@endif