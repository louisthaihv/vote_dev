@extends('layouts.front_fp')

@section('content')
    @include('layouts.front_elements._space')
<div><img src="/common/front/images/fp/tit2.gif" width="100%" alt="投票結果"/></div>
@include('layouts.front_elements._space')
<div style="background-color:#fff6ab;padding:5px;"> {{ $question->title }}</div>
@include('layouts.front_elements._space')
<div style="margin:0px 10px 0px 10px;">
<div style="margin:0px 0px 3px 0px;" class="link">{{ $question->details }}</div>
    @if($question->attribute_id)
<div style="margin:0px 0px 3px 0px;" class="link">(条件：{{ $question->attr->attribute_name }} - {{ $question->attrValue->attribute_value_name }}のみ)</div>
    @endif
@include('layouts.front_elements._space')
<div style="margin:0px 0px 7px 0px; text-align:right;">総投票数：{{ number_format($question->vote_count)  }}票</div>
    @include('layouts.front_elements._space')
@if (isset($myAnswer->choice_num))
    <div style="text-align:left;">■ あなたの回答</div>
    <div style="text-align:center;">{{ $myAnswer->choice_text }}</div>
        @include('layouts.front_elements._space')
@endif
</div>
<table style="width:100%">
<tr>
<td style="width:33%;text-align:center;font-size:small;border:1px solid #333333;background-color:#fff{{ ($firstTab !== TAB_TOTAL) ? '6ab' : ';border-bottom:none' }};padding:10px 0px;">@if ($firstTab === TAB_TOTAL)&#xE6E2;総合@else<a href="{{ $question->getPostPath() }}/?uid=NULLGWDOCOMO&showresult=1" accesskey="1"><font color="#f39800">&#xE6E2;総合</font></a>@endif</td>
@if ($question->attribute_id !== ATTR_BIRTHDAY)<td style="width:33%;text-align:center;font-size:small;border:1px solid #333333;background-color:#fff{{ ($firstTab !== ATTR_BIRTHDAY) ? '6ab' : ';border-bottom:none' }};padding:10px 0px;">@if ($firstTab === ATTR_BIRTHDAY)&#xE6E3;年代別@else<a href="{{ $question->getPostPath() }}/age/{{ $selected[ATTR_BIRTHDAY]->attribute_value_directory }}?uid=NULLGWDOCOMO&showresult=1" accesskey="2"><font color="#f39800">&#xE6E3;年代別</font></a>@endif</td>@endif
@if ($question->attribute_id !== ATTR_GENDER)<td style="width:33%;text-align:center;font-size:small;border:1px solid #333333;background-color:#fff{{ ($firstTab !== ATTR_GENDER) ? '6ab' : ';border-bottom:none' }};padding:10px 0px;">@if ($firstTab === ATTR_GENDER)&#xE6E4;性別@else<a href="{{ $question->getPostPath() }}/gender/{{ $selected[ATTR_GENDER]->attribute_value_directory }}?uid=NULLGWDOCOMO&showresult=1" accesskey="3"><font color="#f39800">&#xE6E4;性別</font></a>@endif</td>@endif
</tr>
</table>
@include('layouts.front_elements._space')
@foreach ($attributes as $attribute)
@if ($firstTab === $attribute->id)<div style="width:100%; text-align:center;">@foreach ($topSummary[$attribute->id] as $elem)@if ($firstTab === ATTR_BIRTHDAY)@if ($firstAttribute == $elem->attribute_value_id)<img src="/common/front/images/fp/{{ $attribute->attribute_directory }}{{ $elem->attribute_value_directory }}_on.gif" width="15%" border="0" />@else<a href="{{ $question->getPostPath() }}/{{ $attribute->attribute_directory }}/{{ $elem->attribute_value_directory }}?uid=NULLGWDOCOMO&showresult=1"><img src="/common/front/images/fp/{{ $attribute->attribute_directory }}{{ $elem->attribute_value_directory }}.gif" width="15%" border="0" /></a>@endif()<img src="/common/front/images/fp/spacer.gif" width="3" />@elseif ($firstTab === ATTR_GENDER)@if ($firstAttribute == $elem->attribute_value_id) <img src="/common/front/images/fp/{{ $elem->attribute_value_directory }}_on.gif" width="45%" /> @else <a href="{{ $question->getPostPath() }}/{{ $attribute->attribute_directory }}/{{ $elem->attribute_value_directory }}?uid=NULLGWDOCOMO&showresult=1"><img src="/common/front/images/fp/{{ $elem->attribute_value_directory }}.gif" width="45%" /></a> @endif()@endif()@endforeach</div>
@endif
@endforeach
@if ($question->isViewableAverage())
<div style="text-align:center;">平均：{{ number_format(intval(($firstTab === TAB_TOTAL ? $average : $topSummary[$firstTab][$firstAttribute]->average_num))) . $question->average_unit }}</div>
@include('layouts.front_elements._space')
@endif
<div style="margin:0px 10px 0px 10px;">
    <?php $forI = 0;?>
@foreach ($result_data[$firstTab][$firstAttribute] as $result)
@if ($question->isViewableAverage() || (!$is_view_more && $forI < 10) || ($is_view_more && $forI >= 10))
<div>{{ ($result['ranking'] == 1) ? '&#xE71A;&nbsp;' : (($result['choice_num'] === 0) ? '&nbsp;-&nbsp;&nbsp;' : ($result['ranking'] . '位')) }}：{{ $result['choice_text'] }}</div>
<table width="100%">
    <tr><td width="70%"><img src="/common/front/images/fp/bar{{ ($result['ranking'] > 6) ? '6' : $result['ranking'] }}.gif" width="{{ $result['bar_width'] }}%" height="10" /></td><td align="right" width="30%"><span style="font-size:x-small;">{{ number_format($result['summary_count']) }}票</span></td></tr>
</table>
@endif
    <?php $forI++; ?>
@endforeach
@if (!$question->isViewableAverage() && count($result_data[$firstTab][$firstAttribute]) > 10)
        @include('layouts.front_elements._space')
    @if (!$is_view_more)
<div style="text-align:center"><a href="/{{ \Request::path() }}?uid=NULLGWDOCOMO&showresult=1&more=1">11位以下はこちら</a></div>
    @else
<div style="text-align:center"><a href="/{{ \Request::path() }}?uid=NULLGWDOCOMO&showresult=1">上位はこちら</a></div>
    @endif
@endif
</div>
@if (!$question->isFinished() && !isset($myAnswer->id))
    <div style="text-align:center">&#xE6FB;<a href="{{ $question->getPath() }}?uid=NULLGWDOCOMO">このｱﾝｹｰﾄに投票する</a></div>
@endif
@include('layouts.front_elements._space')
@include('layouts.front_elements._profileBn_fp')
@if ($navigations->count() > 0)
    @include('layouts.front_elements._space')
<div style="background-color:#fff6ab;padding:5px;text-align:left;"><font color="#f39800">▼</font><font color="#3e3a39">{{ $question->navigation_title }} </font></div>
@include('layouts.front_elements._space')
<div style="margin:0px 10px 0px 10px;">
@foreach($navigations as $navigation)
<div style="margin:0px 0px 3px 0px;" class="link">◆<a href="{{ $navigation->url }}">{{ $navigation->description }}</a></div>
@endforeach
</div>
@endif
@include('layouts.front_elements._space')
@include('front.entry.fp_bottom_contents')
@endsection
