<?php

namespace Front;

use docomo\Models\Answers\TopSummaryByAttributeValue;
use docomo\Models\Questions\Question;
use docomo\Models\Answers\Answer;
use docomo\Models\Answers\ResultVote;
use docomo\Models\Attributes\Attribute;
use docomo\Models\Attributes\AttributeValue;
use docomo\Models\Questions\Choice;
use DateTime;
use docomo\Models\Users\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;

class EntryController extends \BaseController
{

    public function show($genre_parent_name, $genre_child_name, $id)
    {
        $user = $this->getCurrentUser();
        // 完全非表示以外と、ユーザのデバイスを条件として、idからアンケートを取得
        $question = Question::
            baseViewable(
                null,
                null,
                $user->getDevice(),
                null,
                [HIDE_LIST, DISPLAY]
            )
            ->find($id);

        return $this->showEntry($question, $user);
    }

    public function answer($genre_parent_name, $genre_child_name, $question_id, $choice_id = null)
    {
        $choice_id = \Input::get('choice_id', $choice_id);
        $user = $this->getCurrentUser();
        $question = Question::selectRaw('SQL_NO_CACHE `id`, `genre_parent_id`, `genre_child_id`, `status`, `attribute_id`, `attribute_value_id`')
            ->baseViewable(
                null,
                null,
                $user->getDevice(),
                null,
                [HIDE_LIST, DISPLAY]
            )->find($question_id);
        if (is_null($choice_id) || empty($choice_id)) {
            return Redirect::to($question->getPath() . (($user->getDevice() === TYPE_I_MODE) ? '?uid=NULLGWDOCOMO' : ''));
        }
        if (is_null($question)) {
            return Redirect::to($genre_parent_name . '/' . $genre_child_name . '/entry/' . $question_id . (($user->getDevice() === TYPE_I_MODE) ? '?uid=NULLGWDOCOMO' : ''));
        }
        $this->vote($question, $choice_id);

        return Redirect::to($question->getPostPath() . (($user->getDevice() === TYPE_I_MODE) ? '?uid=NULLGWDOCOMO' : ''));
    }

    public function updateUser($genre_parent_name, $genre_child_name, $id)
    {
        $user = $this->getCurrentUserWithCreate();
        $user = User::fillWithModify($user);
        $question = Question::whereBase([DISPLAY, HIDE_LIST], $user->getDevice())->find($id);

        if (!$user->result) {

            return $this->showEntry($question, $user);
        }

        return Redirect::to($question->getPath() . (($user->getDevice() === TYPE_I_MODE) ? '?uid=NULLGWDOCOMO' : ''));
    }

    private function showEntry(Question $question = null, User $currentUser = null)
    {
        // Redirect to notfound page if question isn't found
        if (!isset($question) || !$question->isViewable($currentUser->getDevice())) {
            if (isset($question) && $question->status === QUESTION_STATUS_WAITING) {
                return \View::make("front.entry.waiting", ['question' => $question]);
            } else {
                return \View::make("front.entry.notfound", ['question' => null]);
            }
        }

        if (is_null($currentUser)) {
            $currentUser = $this->getCurrentUser();
        }

        if ($currentUser) {
            if ($question->isAnswered($currentUser->id)) {
                return Redirect::to($question->getPostPath() . (($currentUser->getDevice() === TYPE_I_MODE) ? '?uid=NULLGWDOCOMO' : ''));
            }
        }

        // Redirect to finised page if question has finished
        if ($question->isFinished()) {
            return \View::make("front.entry.finished", compact('question', 'parent_genre_name', 'child_genre_name'));
        }

        // fetch choices and shuffle if needs
        $choices = null;

        if($question->shuffle_choice) {
            $choices = $question->choices()->orderByRaw('(choice_num = 0)')->orderByRaw('RAND()')->get();
        } else {
            $choices = $question->choices()->orderByRaw('(choice_num = 0)')->orderBy('choice_num')->get();
        }

        return \View::make("front.entry.show", compact('currentUser', 'question', 'choices'));
    }

    private function vote ($question, $choice_id) {
        $currentUser = $this->getCurrentUserWithCreate();

        // Redirect result page if user has voted
        if ($question->status != QUESTION_STATUS_READY) {
            return;
        }

        $question->vote($currentUser, null, $choice_id);
    }
}
