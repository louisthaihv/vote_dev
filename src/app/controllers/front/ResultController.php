<?php

namespace Front;

use docomo\Models\Attributes\Attribute;
use docomo\Models\Questions\Column;
use docomo\Models\Questions\Question;
use docomo\Models\Answers\ResultVote;
use docomo\Models\Attributes\AttributeValue;
use docomo\Models\Answers\Answer;
use docomo\Models\Users\User;
use Illuminate\Support\Facades\View;
use docomo\Models\Ratings\VoteRating;
use docomo\Models\Ratings\VoteRatingTotal;
use docomo\Models\Answers\TopSummaryByAttributeValue;
use docomo\Models\Questions\Navigation;
use Illuminate\Support\Facades\Response;

View::composer([
        'front.result.show'
    ], function($view) {
        $question = $view->getData()['question'];
        $currentUser = $view->getData()['currentUser'];
        
        $hasVoted = ($currentUser && !is_null($currentUser->getEvaluate($question->id)));

        $column = $question->getColumn();

        $view->with('column', $column);
        $view->with('voteRatingTotals', $question->voteRatingTotals()->orderBy('type')->get());
        $view->with('hasVoted', $hasVoted);
});

class ResultController extends \BaseController
{
    public function show($parent_genre_name, $child_genre_name, $id, $attributeDirectory = null, $attributeValueDirectory = null)
    {
        $currentUser = $this->getCurrentUser();

        $question = Question::
            baseViewable(
            null,
            null,
            $currentUser->getDevice(),
            null,
            [HIDE_LIST, DISPLAY]
        )
            ->find($id);

        if (!isset($question) || !$question->isViewable($currentUser->getDevice())) {
            if (isset($question) && $question->status === QUESTION_STATUS_WAITING) {
                return \View::make("front.entry.waiting", ['question' => $question, 'type' => COMPLETE]);
            } else {
                return \View::make("front.entry.notfound", ['question' => null, 'type' => COMPLETE]);
            }
        }

        $myAnswer = $question->getAnswered($currentUser->id);

        /*
         * Retrieve navigations list
         */
        // only for smart phone
        $navigations = Navigation::whereQuestionIdAndType($question->id, $currentUser->getDevice())->orderBy('row')->get();

        $showResult =  \Input::get('showresult', \Session::pull('ShowResult', '') == $question->id);
        /*
         * Display ongoing page if question hasn't finished and user hasn't answered
         */
        if ($question->status != QUESTION_STATUS_FINISH && !isset($myAnswer->choice_num) && !$showResult) {
            return \View::make("front.result.ongoing", compact(
                'question',
                'currentUser',
                'navigations'
            ));
        }
        if ($showResult) {
            \Session::put('ShowResult', $question->id);
        }

        $attributes = Attribute::getDataByCache();
        $selected = [];
        $firstTab = TAB_TOTAL;
        $firstAttribute = TAB_TOTAL;
        foreach ($attributes as $attribute) {
            if ($currentUser->has($attribute->id)) {
                $selected[$attribute->id] = AttributeValue::getDataByCache($attribute->id)[$currentUser->attrList[$attribute->id]];
            } else {
                $selected[$attribute->id] = $attribute->getDefaultValue();
            }
            if (!is_null($attributeDirectory) && $attributeDirectory === $attribute->attribute_directory) {
                $firstTab = $attribute->id;
                foreach (AttributeValue::getDataByCache($attribute->id) as $attributeValue) {
                    if (!is_null($attributeValueDirectory) && $attributeValueDirectory == $attributeValue->attribute_value_directory) {
                        $firstAttribute = $attributeValue->id;
                    }
                }
            }
        }

        $result_data = ResultVote::getRankingData($question);
        $topSummary = TopSummaryByAttributeValue::getFrontData($question);

        $average = 0;
        if ($question->summary_type == SUMMARY_AVERAGE) {
            $summary_value = 0;
            $vote_count = 0;
            foreach ($result_data[TAB_TOTAL][TAB_TOTAL] as $result) {
                $summary_value += $result['summary_value'] * $result['summary_count'];
                $vote_count += $result['summary_count'];
            }
            $average = intval($summary_value / (($vote_count == 0) ? 1 : $vote_count));
        }
        $totalChoiceList = [NULL => 10, '_rankCss' => ''];
        foreach ($result_data[TAB_TOTAL][TAB_TOTAL] as &$record) {
            $totalChoiceList[$record['choice_num']] = ($record['ranking'] > 10) ? 10 : $record['ranking'];
            $totalChoiceList[$record['choice_num'] . '_rankCss'] = (is_null($record['ranking'])) ? '' : 'rank' . (($record['ranking'] > 10) ? 10 : $record['ranking']);
        }
        unset($record);

        $is_view_more = \Input::has('more');

        return \View::make("front.result.show", compact(
            'question',
            'currentUser',
            'myAnswer',
            'result_data',
            'average',
            'attributes',
            'navigations',
            'topSummary',
            'selected',
            'is_view_more',
            'firstTab',
            'firstAttribute',
            'totalChoiceList'
        ));
    }

    public function asyncShowRanking($parent_genre_name, $child_genre_name, $id, $attr, $attrv)
    {
        $question = Question::find($id);

        $currentUser = $this->getCurrentUser();

        $myAnswer = $question->getAnswered($currentUser->id);

        $result_data = ResultVote::getRankingData($question, [$attr]);
        $resultData = $result_data[$attr][$attrv];

        return \View::make("front.result._result_table", compact('resultData', 'myAnswer', 'question'));
    }

    public function asyncVote($parent_genre_name, $child_genre_name, $id, $type)
    {
        \DB::beginTransaction();
        $currentUser = $this->getCurrentUserWithCreate();
        $voted = VoteRating::select('type')->whereQuestionId($id)->whereUserId($currentUser->id)->count();
        if ($voted === 0) {
            // insert vote_rating record
            $voteRating = new VoteRating;
            $voteRating->question_id = $id;
            $voteRating->user_id = $currentUser->id;
            $voteRating->type = $type;
            $voteRating->save();
            // increment vote_rating_total count
            $voteRatingTotal = VoteRatingTotal::incrementCount($id, $type);
            \DB::commit();
        } else {
            $voteRatingTotal = VoteRatingTotal::select('rating_count')->findPrimary($id, $type)->first();
            \DB::rollback();
        }

        return Response::JSON(['result' => 'success', 'count' => $voteRatingTotal->rating_count]);
    }
}
