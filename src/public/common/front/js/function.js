// JavaScript Document


$(function(){
    $(".accordion p").click(function(){
        $(this).next("ul").slideToggle();
        $(this).children("span").toggleClass("open");
    });
});


(function($) {
    $(function(){
        $("header .switch a").click(function(){
            $('#container').toggleClass('menuOpen');
            return false;
        });
        $("a#menuOpenLayer").click(function(){
            $('#container').toggleClass('menuOpen');
            return false;
        });
    });
})(jQuery);



$(function() {
    var topBtn = $('.page-top');   
    topBtn.hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
});


function setCookie (name, value) {
    // 有効期限の日付
    var extime = new Date().getTime();
    var cltime = new Date(extime + (60 * 60 * 24 * 1000 * 365 * 5));
    var exdate = cltime.toUTCString();
    // クッキーに保存する文字列を生成
    var s = name + '=' + value + '; path=/; expires=' + exdate + '; Max-Age=157679999';
    // クッキーに保存
    document.cookie = s;
}
function getCookie (name) {
    var st = "";
    var ed = "";
    if(document.cookie.length > 0){
        // クッキーの値を取り出す
        st = document.cookie.indexOf(name + '=');
        if (st != -1) {
            st += name.length + 1;
            ed = document.cookie.indexOf(';', st);
            if (ed == -1) ed=document.cookie.length;
            // 値をデコードして返す
            return document.cookie.substring(st, ed);
        }
    }
    return "";
}