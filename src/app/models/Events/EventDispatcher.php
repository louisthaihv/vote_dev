<?php

namespace docomo\Models\Events;

use Illuminate\Events\Dispatcher;
use Illuminate\Log\Writer;

class EventDispatcher
{
    
    protected $event;
    protected $log;
    
    /**
     *
     * @param Dispatcher $event
     * @param Writer     $log
     */
    public function __construct(Dispatcher $event, Writer $log)
    {
        
        $this->event = $event;
        $this->log = $log;
    }
    
    /**
     * [function dispatch event]
     *
     * @param  array  $events [events want to dispatch]
     * @return [mixed]
     */
    public function dispatch(array $events)
    {
        
        foreach ($events as $event) {
            $eventName = $this->getEventName($event);
            $this->event->fire($eventName, $event);
            $this->log->info("$eventName was fired.");
        }
    }
    
    /**
     * [Get event name]
     *
     * @param $event
     * @return mixed
     */
    protected function getEventName($event)
    {
        
        return str_replace('\\', '.', get_class($event));
    }
}
