<p>ナビゲーションコントロール</p>
<table class="formTable">
    <tr>
        <th>送客枠見出し</th>
        <td>{{ Form::text('navigation_title', $question->navigation_title, ['class' => 'xx-large']) }}</td>
    </tr>
    <tr>
        <th>送客(d版)</th>
        <td>
            <table class="formTable">
                <tr>
                    <th>サムネイル(横144px推奨)50KB以内</th><th>紹介文 / URL</th>
                </tr>
                @foreach ($navigations[TYPE_D_MENU] as $record)
                    <tr>
                        <td>
                            @include('layouts.elements.displays.file_uploader', ['uploader_id' => 'navigation_d_thumbnail[' . $record->row . ']', 'db_file' => $record->thumbnail])
                            </td>
                        <td>{{ Form::text('navigation_d_description[' . $record->row . ']', $record->description, ['class' => 'x-large', 'placeholder' => '紹介文']) }}<br />
                        {{ Form::text('navigation_d_url[' . $record->row . ']', $record->url, ['class' => 'x-large', 'placeholder' => 'http://']) }}</td>
                    </tr>
                @endforeach
            </table>
        </td>
    </tr>
    <tr>
        <th>送客(i版)</th>
        <td>
            <table class="formTable">
                <tr>
                    <th>紹介文 / URL</th>
                </tr>
                @foreach ($navigations[TYPE_I_MODE] as $record)
                    <tr>
                        <td>{{ Form::text('navigation_i_description[' . $record->row . ']', $record->description, ['class' => 'x-large', 'placeholder' => '紹介文']) }}<br />
                        {{ Form::text('navigation_i_url[' . $record->row . ']', $record->url, ['class' => 'x-large', 'placeholder' => 'http://']) }}</td>
                    </tr>
                @endforeach
            </table>
        </td>
    </tr>
    <tr>
        <th>フッターの戻るリンク</th>
        <td>
            タイトル：{{ Form::text('back_button_text', $question->back_button_text, ['class' => 'x-small']) }}
            URL：{{ Form::text('back_button_url', $question->back_button_url, ['class' => 'x-large']) }}
        </td>
    </tr>
</table>