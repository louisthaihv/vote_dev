<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CreateRSSCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'create:rss';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create rdf files.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

		$this->info('start create rdf file.');
		$parentGenre = \docomo\Models\Genres\ParentGenre::select('id', 'genre_parent_directory')
			->visible()
			->get();

        $finder = new \Symfony\Component\Finder\Finder();
        $parentDirs = $finder
            ->in(public_path('rss_i/newly'))
            ->directories();
        $dirList = [];
        foreach ($parentDirs as $parentDir) {
            if ($parentDir->getPathname() === public_path('rss_i/newly')) continue;
            $dirList[] = $parentDir->getPathname();
            unset($parentDir);
        }
        unset($parentDirs);

		$serverPath = 'http://vote.smt.docomo.ne.jp';
		$date = new \DateTime;
        $questions = [];
        $genres = [
            ['genre_parent_id', 11],
            ['genre_child_id', 18],
            ['genre_parent_id', 13],
            ['genre_parent_id', 2],
            ['genre_child_id', 20],
            ['genre_parent_id', 8]];
        foreach ($genres as $filterGenre) {
            $question = \docomo\Models\Questions\Question::fieldRSS()
                ->whereBase([DISPLAY], TYPE_I_MODE)
                ->displayList()
                ->ongoing()
                ->where($filterGenre[0], '=', $filterGenre[1])
                ->orderBy('vote_date_from', 'desc')
                ->first();
            if (!is_null($question)) {
                $questions[] = $question;
            }
            unset($question);
        }
		foreach ($questions as &$question) {
			$question->url = $question->getEntryPath();
			$question->date = new Datetime($question->vote_date_from);
			$question->description = $question->description;

			unset($question);
		}
		\File::put(public_path('rss_i/newly/l.rdf') , \View::make('rss.template_imode', [
			'rssTitle' => 'iMenuニュースジャンル用投票リンクセット',
			'siteURL' => $serverPath,
			'siteDescription' => '',
			'lastUpdate' => $date,
			'recordData' => $questions
		]));

		foreach ($parentGenre as &$parent) {
			$this->info('parent -> ' . $parent->genre_parent_directory);
			$children = \docomo\Models\Genres\ChildGenre::select('id', 'genre_child_directory')
				->whereGenreParentId($parent->id)
				->visible()
				->get();

			$dirPath = public_path('rss_i/newly/' . $parent->genre_parent_directory);
			if (!file_exists($dirPath)) {
				mkdir($dirPath, 0777, true);
			}
            $dirIndex = array_search(public_path('rss_i/newly/' . $parent->genre_parent_directory), $dirList);
            if ($dirIndex !== false) {
                unset($dirList[$dirIndex]);
            }
			$questions = \docomo\Models\Questions\Question::fieldRSS()
				->orderBy('vote_date_from', 'desc')
				->whereGenreParentId($parent->id)
				->whereBase([DISPLAY], TYPE_I_MODE)
				->displayList()
				->ongoing()
				->limit(7)
				->get();
			foreach ($questions as &$question) {
				$question->url = $question->getEntryPath();
				$question->date = new Datetime($question->vote_date_from);
				$question->description = $question->description;

				unset($question);
			}
			\File::put($dirPath . '/l.rdf', \View::make('rss.template_imode', [
				'rssTitle' => 'iMenuニュースジャンル用投票リンクセット',
				'siteURL' => $serverPath,
				'siteDescription' => '',
				'lastUpdate' => $date,
				'recordData' => $questions,
                'genre' => $parent->genre_parent_directory
			]));

			foreach ($children as &$child) {
				$this->info(' - child -> ' . $child->	genre_child_directory);
				$dirPath = public_path('rss_i/newly/' . $parent->genre_parent_directory . '/' . $child->genre_child_directory);
				if (!file_exists($dirPath)) {
					mkdir($dirPath, 0777, true);
				}
                $dirIndex = array_search($dirPath, $dirList);
                if ($dirIndex !== false) {
                    unset($dirList[$dirIndex]);
                }
				$questions = \docomo\Models\Questions\Question::fieldRSS()
					->orderBy('vote_date_from', 'desc')
					->whereGenreChildId($child->id)
					->whereBase([DISPLAY], TYPE_I_MODE)
					->displayList()
					->ongoing()
					->limit(7)
					->get();
				foreach ($questions as &$question) {
					$question->url = $question->getEntryPath();
					$question->date = new Datetime($question->vote_date_from);
					$question->description = $question->description;

					unset($question);
				}
				\File::put($dirPath . '/l.rdf', \View::make('rss.template_imode', [
					'rssTitle' => 'iMenuニュースジャンル用投票リンクセット',
					'siteURL' => $serverPath,
					'siteDescription' => '',
					'lastUpdate' => $date,
					'recordData' => $questions
				]));
				unset($questions);
				unset($child);
			}

			unset($parent);
		}
        rsort($dirList);
        foreach ($dirList as &$dir) {
            if (is_file($dir . '/l.rdf')) {
                unlink($dir . '/l.rdf');
            }
            rmdir($dir);
            unset($dir);
        }
        unset($dirList);
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
