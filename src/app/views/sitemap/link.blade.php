@foreach ($questions as &$question)
    <url>
        <loc>{{ $serverPath }}{{ $question['path'] }}</loc>
        <lastmod>{{ $question['date'] }}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
@endforeach