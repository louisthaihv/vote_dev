@extends('layouts.admin_in')
@section('content')
<h2>TOPページ注目キーワードデータ一覧</h2>
    <table class="list">
        <tr>
            <th>ID</th>
            <th>キーワード</th>
            <th>受付中 / 結果</th>
            <th style="width:90px;">開始</th><th style="width:90px;">終了</th>
            <th>削除</th>
        </tr>
        @foreach($attentions as $attention)
            <tr class="{{ ($attention->isEnd()) ? 'expiration' : '' }}">
            <td>{{ $attention->id }}</td>
            <td>{{ link_to_route('manage.attention.edit', $attention->keyword->keyword_name, $attention->id) }}</td>
            <td>{{ docomo\Models\Codes\FrontTab::$label[$attention->tab] }}</td>
            <td>{{ date('Y-m-d H:i', strtotime($attention->from)) }}</td>
            <td>{{ date('Y-m-d H:i', strtotime($attention->to)) }}</td>
            <td>
                {{ Form::open(['route' => ['manage.attention.destroy', $attention->id], 'method' => 'delete']) }}<button type="submit" class="naviBtn"
                onclick="return confirm('{{ ((string)$attention->id) . C_DELETE_WITH_ITEM }}')">削除</button>
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </table>
@stop