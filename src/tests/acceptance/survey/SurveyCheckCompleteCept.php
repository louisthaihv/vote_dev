<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('test survey');
$I->amOnPage('/manage');
// check Complete form
$I->checkCompleteSurvey('title_survey_test_1', SURVEY_SAVE_DRAFT, W_FRM_SURVEY_SAVE_DRAFT, SURVEY_SAVE_DRAFT_TEXT, SURVEY_STATUS_DRAFT);
$I->checkCompleteSurvey('title_survey_test_2', SURVEY_APPROVAL, W_FRM_SURVEY_APPROVAL, SURVEY_APPROVAL_TEXT, SURVEY_STATUS_REQUEST_APPROVE);
$I->checkCompleteSurvey('title_survey_test_3', SURVEY_APPROVAL_REFLECT, W_FRM_SURVEY_APPROVAL_REFLECT, SURVEY_APPROVAL_REFLECT_TEXT, null);
