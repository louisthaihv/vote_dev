<?php namespace RSS;

use docomo\Models\Features\Feature;
use docomo\Models\Questions\Question;
use docomo\Models\Questions\QuestionTag;
use docomo\Models\Attentions\Attention;
use docomo\Models\Answers\ResultVote;
use docomo\Models\Pickups\Pickup;
use docomo\Models\Questions\SummaryByAttributes;
use docomo\Models\Questions\SummaryByType;
use docomo\Models\Users\User;
use docomo\Models\Users\UserAttribute;
use docomo\Models\Attributes\AttributeValue;
use docomo\Models\Genres\ChildGenre;
use docomo\Models\Genres\ParentGenre;

class QuestionListController extends RSSBaseController {

    protected $siteDescription = 'みんなの声で受付中のアンケート一覧です';

    public function generate ($parent_genre = null, $child_genre = null)
    {

        $parent = ParentGenre::select('id')
            ->whereGenreParentDirectory($parent_genre)
            ->visible()
            ->first();
        if (isset($parent)) {
            $parentId = $parent->id;
            if (is_null($child_genre)) {

                return $this->createByParentGenre($parentId);
            } else {
                $child = ChildGenre::select('id')
                    ->whereGenreParentId($parentId)
                    ->whereGenreChildDirectory($child_genre)
                    ->visible()
                    ->first();
                if (isset($child)) {
                    $childId = $child->id;

                    return $this->createByChildGenre($childId);
                }
            }
        }

        $questions = Question::fieldRSS()
            ->ongoing()
            ->whereBase()
            ->displayList()
            ->orderBaseDateToDesc()
            ->get();

        return $this->createViewVer1($questions);
    }

    private function createByParentGenre($parentId) {
        $questions = Question::fieldRSS()
            ->whereGenreParentId($parentId)
            ->whereBase()
            ->ongoing()
            ->displayList()
            ->orderBaseDateToDesc()
            ->get();

        return $this->createViewVer1($questions);
    }

    private function createByChildGenre($childId) {
        $questions = Question::fieldRSS()
            ->whereGenreChildId($childId)
            ->whereBase()
            ->ongoing()
            ->displayList()
            ->orderBaseDateToDesc()
            ->get();

        return $this->createViewVer1($questions);
    }
}