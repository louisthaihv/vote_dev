@extends('layouts.front')

@section('metaTag')
    <title>{{ $question->title }} | みんなの声</title>
    <meta name="description" content="{{{ $question->description }}}">
    <meta property="og:title" content="{{{ $question->title }}} | みんなの声" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="{{{ $question->description }}}" />
    <meta name="twitter:title" content="{{{ $question->title }}} | みんなの声" />
    <meta name="twitter:description" content="{{{ $question->description }}}" />
@stop

@section('style')
{{ HTML::style('common/front/css/result.css') }}
{{ HTML::style('common/front/css/entry_additional.css') }}
@stop

@section('script')
    <script type="text/javascript">
        var basePath = '{{ route('result.show', ['parent_genre_name' => $question->getParentGenreDirectory(), 'child_genre_name' => $question->getChildGenreDirectory(), 'id' => $question->id]) }}';
        var attribute = '{{ $firstTab }}';
        var attributeValue = '{{ $firstAttribute }}';
        var summaryData = {{ json_encode($result_data) }};
        var answeredId = {{ isset($myAnswer->id) ? $myAnswer->id : -1 }};
        var isAverage = {{ $question->isViewableAverage() ? 1 : 0 }};
    </script>
{{ HTML::script('common/front/js/tab_change.js') }}
{{ HTML::script('common/front/js/result.js?v3') }}
@stop

@section('content')

@if (isset($column))
<section class="columnArea l-margin">
    <h4 class="subSectionTitH4 columnIcon"><span>コラム</span></h4>
    <div class="m-margint columnBlock lrMargin">
        <div class="columnBox">
            <p class="columnTit">{{ $column->column_title }}</p>
            <div class="columnContents">
                {{ $column->column_detail }}
            </div>
        </div><!--/columnBox-->
        <a id="re_result_column_more" href="#" class="nextNavi">▼つづきを読む</a>
    </div><!--columnBlock-->
</section>
@endif
<section class="l-margin">
    <h2 class="resultTit m-margin"><span>投票結果</span></h2>
    <div class="lrMargin">
        <div class="questionInfoBox">
            <h1>{{ $question->title }}</h1>
            <div class="questionText">
                {{ $question->details }}
            </div>
            @if($question->attribute_id)
                <p class="restraintResultInfo m-margin restraintShow">(条件：{{ $question->attr->attribute_name }} - {{ $question->attrValue->attribute_value_name }}のみ)</p>
            @endif
        </div><!--/questionInfoBox-->

        @if($question->result_comment)
        <div class="resultCommentArea m-margin">
            <p class="tcenter s-margin">{{ HTML::image('common/front/images/sp/resultcomment.png') }}</p>
                <div class="resultComment">
                    {{ $question->result_comment }}
                </div>
        </div><!--/resultCommentArea-->
        @endif
        <p class="resultCountInfo m-margin m-margint">総投票数：<span class="boldFont">{{ number_format($question->vote_count) }}</span>票</p>
    </div>
</section>

<section class="resultTabArea">
    <ul class="resulttabs">
        <li class="active pushStateAction" id="attribute_0" url=""><a id="attribute_value_0" href="#">総合</a></li>

        @foreach ($attributes as $attribute)
            @if($question->attribute_id != $attribute->id)
                <li id="attribute_{{ $attribute->id }}" class="pushStateAction" url="/{{ $attribute->attribute_directory }}/{{ $selected[$attribute->id]->attribute_value_directory }}"><a id="clickcount-tab2-{{ $attribute->attribute_directory }}" href="#">{{{ $attribute->attribute_name }}}</a></li>
            @endif
        @endforeach
    </ul>

    <div class="resulttabs_content">
        <!--総合-->
        @include('front.result._content_total')

        @foreach ($attributes as $attribute)
            @if ($question->attribute_id != $attribute->id)
                @include('front.result._content_ranking', ['attribute_id' => $attribute->id, 'attribute_dir' => $attribute->attribute_directory])
            @endif
        @endforeach
    </div>
</section>


@if($question->isFinished())
@elseif (!$question->isAnswered($currentUser->id))
    @include('front.result._ongoing_contents')
@endif

@include('front.result.evaluation')

@include('front.result._recommend')
@include('front.entry._bottomcontents')

@stop
