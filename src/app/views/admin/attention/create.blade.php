@extends('layouts.admin_in')
@section('content')
<h2>TOPページ注目キーワードデータ作成</h2>
@include('admin.attention.form', ['url' => 'manage/attention/create', 'method' => 'post', 'confirmMessage' => C_REGISTRATION, 'attention' => $attention, 'button' => REGISTRATION])
@stop