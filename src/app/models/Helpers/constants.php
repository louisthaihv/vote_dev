<?php
define('VISIBLE', 1);
define('HIDDEN', 0);

define('SHOW_ALL', 1);

define('FIRST_SORT_ORDER', 10);
define('DISTANCE_SORT', 10);

define('ANSWERED', 1);
define('UNANSWERED', 0);

define('D_MENU_HEADER', 'dmenu_');
define('I_MODE_HEADER', 'imode_');
// define d-menu i-mode
define('TYPE_OTHER', 0);
define('TYPE_D_MENU', 1);
define('TYPE_I_MODE', 2);
define('TYPE_API', 3);
// define i-mode or other chars
define('CHAR_D_MENU', 'sp');
define('CHAR_I_MODE', 'fp');
/**
 * Define on screen management accounts
 */
define('ALLOW', 1);
define('DISALLOW', 0);
define('ENABLED', 1);
define('DISABLED', 0);
define('ALLOW_ALL_GENRE', 0);
define('LEN_PASSWORD', 6);

/*
* Define on screen management attentions
*/

define('PROCESSING', 0);
define('COMPLETE', 1);

define('SUMMARY_NORMAL', 1);
define('SUMMARY_AVERAGE', 2);

/**
 * Define on DropDownList For 'すべて' value
 */
define('DDL_ALL', 'all');

/**
 * Define for manage survey
 */
define('MAX_NAV_CONTROL', 5);
define('UNLIMITED', 'free');
define('PAGER_SIZE', 10);
define('ADMIN_PAGINATION_VIEWABLE_HF', 5);
define('NUM_FIRST_PAGE', 10);
define('NUM_FIRST_PAGE_10', 10);
define('PAGER_SIZE_FRONT', 10);
define('SIZE_FRONT_TOP_3', 3);
define('SIZE_FRONT_TOP_5', 5);

// status approve

define('QUESTION_STATUS_CREATE', 0);
define('QUESTION_STATUS_DRAFT', 1);
define('QUESTION_STATUS_REJECT', 2);
define('QUESTION_STATUS_REQUEST_APPROVE', 3);
define('QUESTION_STATUS_WAITING', 4);
define('QUESTION_STATUS_READY', 5);
define('QUESTION_STATUS_FINISH', 6);

// other
define('OTHER_CHOICE_NUM', 0);

define('MODE_NORMAL', 1);
define('MODE_ALL_EDIT', 2);

// viewable type for question
define('DISPLAY', 1);
define('HIDE_LIST', 2);
define('HIDE_ALL', 3);

//config path to thumbnail to feature
define('PATH_TO_FEATURE_TEMP', '/asset/img/feature/temp/');
define('PATH_TO_FEATURE_THUMBNAIL', '/asset/img/feature/thumbnail/');
define('PATH_TO_NAVIGATION_TEMP', '/asset/img/navigation/temp/');
define('PATH_TO_NAVIGATION_THUMBNAIL', '/asset/img/navigation/thumbnail/');
define('PATH_TO_DUMP_PATH', '/asset/dump/');

define('PATH_TO_UPLOAD_FILE', '/asset/upload/');

define('FEATURE', 'FEATURE');
define('NOTICE', 'NOTICE');
define('QUESTION', 'QUESTION');
define('DELIMITER', ',');

// User attribute_id
define('TAB_TOTAL', 0);
define('ATTR_BIRTHDAY', 1);
define('ATTR_GENDER', 2);
define('ATTR_LOCAL', 3);
// not set attribute value
define('ATTRIBUTE_VALUE_NOT_SET', 0);

// define max num of popular questions
define('NUM_POPULAR_QUESTIONS', 5);

// define max num of questions where belongs to same category
define('NUM_SAMECATEG_QUESTIONS', 20);
define('NUM_SAME_CATEGORY_QUESTIONS_FP', 20);

// default birth_year
define('BIRTH_YEAR_DEF', 1970);

// summary type
define('SUMMARY_TYPE_POPULAR_ENTRY', 1);
define('SUMMARY_TYPE_POPULAR_RESULT', 2);
define('SUMMARY_TYPE_NEWER_ENTRY', 3);
define('SUMMARY_TYPE_NEWER_RESULT', 4);

define('SUMMARY_TYPE_EVALUATION_GATTEN', 10);
define('SUMMARY_TYPE_EVALUATION_MAJI', 11);
define('SUMMARY_TYPE_EVALUATION_MEMO', 12);

define('DEFAULT_CACHE_TIME', 720);
define('MAX_UPLOAD_FILE_SIZE', 500000);
/**
 * define for only feature phone
 */
define('SPACE_HEIGHT_5', 5);
define('SPACE_HEIGHT_10', 10);

/*
 * define for share button
 */
define('SHARE_LINK_PARAMETER_TWITTER', '%3futm_source%3dtw%26utm_medium%3dbk');
define('SHARE_LINK_PARAMETER_FACEBOOK', '%3futm_source%3dfb%26utm_medium%3dbk');
