@extends('layouts.admin_in')

@section('content')
<h2>TOPページ特集枠データ作成</h2>
@include('admin.feature.form', ['feature' => $feature])

@stop