<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAttentions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('pickup_keywords');

		Schema::create('attentions', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('keyword_id');
			$table->integer('tab');
			$table->timestamp('from');
			$table->timestamp('to');
			$table->timestamps();
			$table->index('from');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('attentions');
	}

}
