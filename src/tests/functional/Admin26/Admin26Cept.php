<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('manage info');

$I->amOnPage('/manage/info');
$I->amOnAction('Admin\SurveyController@create');
