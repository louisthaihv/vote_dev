@extends('layouts.admin_in')
@section('content')
<h2>キーワード編集</h2>
@include('admin.keyword.form', ['url' => 'manage/keyword/' . $keyword->id . '/edit', 'method' => 'put', 'confirmMessage' => C_UPDATE, 'keyword' => $keyword, 'button' => UPDATE])
@stop