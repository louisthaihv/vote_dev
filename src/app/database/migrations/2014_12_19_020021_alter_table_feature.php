<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableFeature extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('feature')) {
			Schema::table('feature',
				function ($table) {
					if (Schema::hasColumn('feature', 'thumbnail')) {
						$table->dropColumn('thumbnail');
					}
					if (Schema::hasColumn('feature', 'message')) {
						$table->dropColumn('message');
					}
					if (Schema::hasColumn('feature', 'is_view_list')) {
						$table->dropColumn('is_view_list');
					}
					if (Schema::hasColumn('feature', 'question_ids')) {
						$table->dropColumn('question_ids');
					}
					if (Schema::hasColumn('feature', 'is_feature_column')) {
						$table->dropColumn('is_feature_column');
					}
				});
			Schema::table('feature',
				function ($table) {
					$table->string('thumbnail', 255)->nullable()->after('title');
					$table->string('message', 10000)->nullable()->after('thumbnail');
					$table->boolean('is_view_list')->default(false)->after('to');
					$table->string('question_ids', 255)->nullable()->after('is_view_list');
					$table->boolean('is_feature_column')->default(false)->after('question_ids');
				});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
