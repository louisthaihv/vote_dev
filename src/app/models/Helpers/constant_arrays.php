<?php

namespace docomo\Models\Helpers;

class ConstantArrays
{
    public static $surveyVisibleFlag = array(
        DISPLAY   => '表示する',
        HIDE_LIST => '一覧から非表示',
        HIDE_ALL  => '完全非表示'
    );
    public static $surveyStatus = array(
        DRAFT                => '下書き',
        DRAFT_REJECTED       => '下書き（差戻し）',
        REQUEST_APPROVE      => '承認申請中',
        APPROVED_RECEIVING       => '受付準備中',
        APPROVED_PREPARE_RECEIVE => '受付中',
        APPROVED_END_RECEIVE     => '受付終了',
    );
}
