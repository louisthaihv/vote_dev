<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('test survey');
$I->amOnPage('/manage');

$I->loginClickCommon('アンケート新規投稿');
$I->createNewSurvey('title_test_column', SURVEY_STATUS_DRAFT, W_FRM_SURVEY_SAVE_DRAFT, SURVEY_SAVE_DRAFT_TEXT);
$I->logout();
//search
$I->searchSurveyGenre('学校生活、職場生活', 'ご当地ジョーシキ');
$I->searchSurveyKeyword('keyword_survey_test_1', 'keyword_survey_test_2');
$I->searchSurveyDatetime('title_test_column');
$I->searchSurveyAccount('account_test_1');
$I->searchSurveyStatus('title_test_column', SURVEY_STATUS_DRAFT);
$I->searchSurveyColumn('title_test_column');
$I->searchSurveyId(17, 'title_test_column', 100);
$I->searchSurveyTitle('title_test_column', 'title_test_column_false');
$I->searchSurveyDisplay('title_test_column');
