<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAttributes extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("attributes", function ($table) {
            $table->increments("id");
            $table->string("attribute_name", 60);
            $table->integer("sort_order");
            $table->string("attribute_directory", 200);
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("attributes");
    }
}
