<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('edit frame');

$I->amOnRoute('admin.special.edit');
$I->amOnAction('Admin\SurveyController@create');
