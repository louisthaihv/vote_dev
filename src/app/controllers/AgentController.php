<?php

class AgentController extends BaseController {

	public function getSol1() {
		return View::make('agents.agent_demo');
	}

	public function getSol2() {
		return View::make('agents2.agent_demo');
	}

	public function getSubDirSol1() {
		return View::make('agents.func.sub_func.func1');
	}

	public function getSubDirSol2() {
		return View::make('agents2.func.sub_func.func1');
	}
}