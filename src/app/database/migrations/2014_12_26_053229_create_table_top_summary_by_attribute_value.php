<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTopSummaryByAttributeValue extends Migration {

	protected $table_name = 'top_summary_by_attribute_value';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists($this->table_name);
		Schema::create($this->table_name, function($table) {
			$table->integer('question_id')->unsigned();
			$table->integer('attribute_id')->unsigned();
			$table->integer('attribute_value_id')->unsigned();
			$table->integer('top_choice_num')->nullable();
			$table->decimal('average_num',12, 2)->nullable();
		});
		DB::statement('ALTER TABLE '. $this->table_name.
			' ADD PRIMARY KEY (question_id, attribute_id, attribute_value_id);');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists($this->table_name);
	}

}
