
<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE html PUBLIC "-//i-mode group (ja)//DTD XHTML i-XHTML(Locale/Ver.=ja/1.1) 1.0//EN" "i-xhtml_4ja_10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
    <title>みんなの声</title>
</head>

<body link="#0000FF" vlink="#0000FF" style="font-size:small;">
<div class="header" style="text-align:center;">
    <img src="/common/front/images/fp/logo.gif" width="100%" alt="みんなの声"/>
</div>
<div><img src="/common/front/images/fp/spacer.gif" height="10" /></div>
<div style="margin:0px 10px 0px 10px;">
    <div style="text-align:center">該当するページはありません</div>
    <div><img src="/common/front/images/fp/spacer.gif" height="10" /></div>
    <div style="text-align:center"><a href="{{ url('/') }}">みんなの声TOPへ</a></div>
</div>
<div><img src="/common/front/images/fp/spacer.gif" height="10" /></div>
<div style="text-align:center;">(C)NTT DOCOMO</div>
</body>
</html>
