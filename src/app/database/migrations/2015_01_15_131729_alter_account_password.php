<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAccountPassword extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\DB::statement('ALTER TABLE accounts MODIFY login_password varchar(512)');
		\DB::statement('ALTER TABLE questions MODIFY back_button_url varchar(512)');
		\DB::statement('ALTER TABLE navigations MODIFY url varchar(512)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
