<?php
namespace Admin;

use docomo\Models\Features\Feature;


class FeatureController extends \BaseController
{
    
    /**
     *
     */
    public function __construct()
    {
        
    }
    
    /**
     * List Features
     *
     * @return View list Features
     */
    public function index()
    {

        \Session::forget(FEATURE);
        return \View::make('admin.feature.index', ['feature_list' => Feature::getList()]);
    }
    
    /**
     * Create feature
     *
     * @return View create feature
     */
    public function create()
    {
        $feature = \Session::get(FEATURE);
        if (is_null($feature)) {
            $feature = new Feature;
        } else {
            $feature = unserialize($feature);
        }

        if (!\Session::pull(FEATURE . '_confirm', false)) {
            \Session::forget(FEATURE);
            $feature = new Feature;
        }
        $feature->question_ids = trim($feature->question_ids, ',');
        return \View::make('admin.feature.create', compact('feature'));
    }
    
    /**
     * [confirm View confirm form feature]
     * @return [View] [View confirm form feature]
     */
    public function confirm()
    {
        $feature = \Session::pull(FEATURE);
        if (is_null($feature)) {
            $feature = new Feature;
        } else {
            $feature = unserialize($feature);
        }

        $feature = Feature::fillWithModify($feature->id, $feature->thumbnail);
        \Session::put(FEATURE, serialize($feature));
        if ($feature->confirmValidate()) {
            \Session::put(FEATURE . '_confirm', true);

            return \View::make('admin.feature.confirm', compact('feature'));
        }
        else
        {
            $errors = $feature->error_message;

            return \View::make((isset($feature->id)) ? 'admin.feature.edit' : 'admin.feature.create', compact('feature', 'errors'));
        }
    }
    
    /**
     * Deatail feature
     *
     * @param  [int] $id [id of feature]
     * @return View detail feature
     */
    public function edit($id)
    {
        $feature = \Session::pull(FEATURE);
        if (is_null($feature)) {
            $feature = Feature::details()->find($id);
        } else {
            $feature = unserialize($feature);
        }
        if ($feature->id != $id) {
            $feature = Feature::details()->find($id);
        }
        \Session::put(FEATURE, serialize($feature));

        return \View::make('admin.feature.edit', compact('feature'));
    }
    
    /**
     * Create feature
     *
     * @return insert feature in database
     */
    public function store()
    {

        return $this->saveFeature('create');
    }
    
    /**
     * Edit feature
     *
     * @param  [int] $id [Id of feature]
     * @return [Redirect]     [update success feature and redirect to list feature]
     */
    public function update($id)
    {

        return $this->saveFeature('edit');
    }
    
    /**
     * [delete Delete Feature]
     * @param  [int] $id [id of feature]
     * @return [View]     [delete feature and redirect to list feature]
     */
    public function destroy($id)
    {
        $feature = Feature::find($id);
        $feature->delete();
        
        return \Redirect::route('manage.feature.index');
    }

    private function saveFeature($mode)
    {

        $feature = \Session::pull(FEATURE);
        if (is_null($feature)) {
            $feature = new Feature;
        } else {
            $feature = unserialize($feature);
        }

        $feature->saveFile();
        unset(Feature::$rules['from']);
        unset(Feature::$rules['to']);

        $questionIdList = $feature->questionIdList;
        $feature = $feature->cloneObject();

        if ($feature->save())
        {
            Feature::saveList($feature->id, $questionIdList);
            return \Redirect::route('manage.feature.index');
        }
        else
        {
            $errors = $feature->errors();

            return \View::make('admin.feature.' . $mode, compact('feature', 'errors'));
        }

    }
}
