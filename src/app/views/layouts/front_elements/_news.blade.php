<section class="newsBox">
<?php $idx = 1; ?>
    @foreach($notices as $notice)
        <dl class="news">
            <dt><a id="header_info{{ sprintf('%1$02d', $idx++) }}" href="{{ route('notice.detail', $notice->id) }}">{{ $notice->title }}</a></dt>
            <dd>{{ $notice->description }}</dd>
        </dl>
    @endforeach

</section>

