@extends('layouts.front_fp')

@section('content')

    @include('layouts.front_elements._space')
    <div style="text-align:center;">
        <a href="#result">{{ HTML::image("common/front/images/fp/resultbtn.gif" ,"結果発表", array('width'=>"45%", 'border'=>"0")) }}</a>
        <a href="{{ route('list.mypage', ['uid' => 'NULLGWDOCOMO']) }}">{{ HTML::image("common/front/images/fp/mypagebtn.gif" ,"マイページ", array('width'=>"45%", 'border'=>"0")) }}</a>
    </div>

    @include('layouts.front_elements._space')

    <div>{{ HTML::image("/common/front/images/fp/toptit2.gif", "投票しよう！ｱﾝｹｰﾄ受付中！", array('width'=>"100%")) }}</div>
    @include('layouts.front_elements._space')
    @if ($pickups[PROCESSING]->count() == 0 &&
        (!array_key_exists(SUMMARY_TYPE_NEWER_ENTRY, $summaryData) || count($summaryData[SUMMARY_TYPE_NEWER_ENTRY]) === 0 || $summaryData[SUMMARY_TYPE_NEWER_ENTRY]->count() === 0) && !$limited)
<div>みんなの声をお使いいただきありがとうございます！<br />
ただいま回答できるアンケートはありません。<br />
新しいアンケートを準備中ですので、配信をお待ちください。<br />
追加したアンケートは公式twitterやfacebookでもつぶやきます。<br />
是非フォローをお願いします！
@include('layouts.front_elements._space')
<div style="width:100%;text-align:center;">
<a href="https://twitter.com/_minnanokoe_/"><img src="/common/front/images/sp/btn_twitter.png" alt="twitter" width="100px"></a>
<a href="https://www.facebook.com/minnanokoe.vote"><img src="/common/front/images/sp/btn_facebook.png" alt="facebook" width="100px"></a>
</div></div>
@include('layouts.front_elements._space')
    @else
    @if($limited)
        <div style="background-color:#fff6ab;padding:5px;text-align:left;"><font color="#f39800">▼</font><font color="#3e3a39"> 
            {{ $limited->getLimitedValueName() }}の方へ </font></div>
        @include('layouts.front_elements._space')
        <div style="margin:0px 10px 0px 10px;">
        <div style="margin:0px 0px 3px 0px;" class="link"><a href="{{ $limited->getPath() }}?uid=NULLGWDOCOMO">{{ $limited->title }}</a></div>
        </div>
        @include('layouts.front_elements._space')

    @endif

    @if ($pickups[PROCESSING]->count() > 0)
<div style="background-color:#fff6ab;padding:5px;text-align:left;"><font color="#f39800">▼</font><font color="#3e3a39"> ﾋﾟｯｸｱｯﾌﾟ </font></div>
        @include('layouts.front_elements._space')
<div style="margin:0px 10px 0px 10px;">
        @foreach($pickups[PROCESSING] as $pickup_ready)
            @if (!$pickup_ready->isAnswered($currentUser->id))
<div style="margin:0px 0px 3px 0px;" class="link">
    <a href="{{ url($pickup_ready->getPath()) }}?uid=NULLGWDOCOMO">{{ $pickup_ready->title }}</a>
</div>
            @endif
        @endforeach
</div>
        @include('layouts.front_elements._space')
    @endif

<div style="background-color:#fff6ab;padding:5px;text-align:left;"><font color="#f39800">▼</font><font color="#3e3a39"> 新着受付ｱﾝｹｰﾄ </font></div>
    @include('layouts.front_elements._space')

    @if(array_key_exists(SUMMARY_TYPE_NEWER_ENTRY, $summaryData) && count($summaryData[SUMMARY_TYPE_NEWER_ENTRY]) > 0)
<div style="margin:0px 10px 0px 10px;">
        @foreach($summaryData[SUMMARY_TYPE_NEWER_ENTRY] as $question)
            @include('layouts.front_elements._list_question', ['hide_answer' => true])
        @endforeach
        @include('layouts.front_elements._space')
</div>
    @endif

<div style="text-align:right;">&#xE696;{{ link_to_route('list.unanswered', 'もっと見る', ['uid' => 'NULLGWDOCOMO']) }}</div>
    @include('layouts.front_elements._gotop_fp')

@endif

    @include('layouts.front_elements._profileBn_fp')

    @include('layouts.front_elements._space')
<a name="result" id="result"></a>
<div>{{ HTML::image("common/front/images/fp/toptit1.gif" ,"アンケート結果発表！", array('width'=>"100%")) }}</div>

    @include('layouts.front_elements._space')
    @if(!is_null($attributeFavorite))
<div style="background-color:#fff6ab;padding:5px;text-align:left;"><font color="#f39800">▼</font><font color="#3e3a39">
{{ $user->getAttributeValueLabel(ATTR_BIRTHDAY) }}{{ $user->getAttributeValueLabel(ATTR_GENDER) }}が興味 </font></div>
        @include('layouts.front_elements._space')
<div style="margin:0px 10px 0px 10px;">
<div style="margin:0px 0px 3px 0px;" class="link"><a href="{{ $attributeFavorite->getPostPath() }}?uid=NULLGWDOCOMO">{{ $attributeFavorite->title }}</a></div>
</div>
        @include('layouts.front_elements._space')
    @endif

    @if ($pickups[COMPLETE]->count() > 0)
<div style="background-color:#fff6ab;padding:5px;text-align:left;"><font color="#f39800">▼</font><font color="#3e3a39"> ﾋﾟｯｸｱｯﾌﾟ </font></div>
        @include('layouts.front_elements._space')
<div style="margin:0px 10px 0px 10px;">
        @foreach($pickups[COMPLETE] as $pickup_result)
<div style="margin:0px 0px 3px 0px;" class="link">
<a href="{{ url($pickup_result->getPath()) }}?uid=NULLGWDOCOMO">{{ $pickup_result->title }}</a>
</div>
        @endforeach
</div>
        @include('layouts.front_elements._space')
    @endif

    <div style="background-color:#fff6ab;padding:5px;text-align:left;"><font color="#f39800">▼</font><font color="#3e3a39"> 新着結果 </font></div>
    @include('layouts.front_elements._space')
    @if(array_key_exists(SUMMARY_TYPE_NEWER_RESULT, $summaryData) && count($summaryData[SUMMARY_TYPE_NEWER_RESULT]) > 0)
        <div style="margin:0px 10px 0px 10px;">
        @foreach($summaryData[SUMMARY_TYPE_NEWER_RESULT] as $question)
            @include('layouts.front_elements._list_question', ['hide_answer' => true])
        @endforeach
        @include('layouts.front_elements._space')
        </div>
    @endif

    <div style="text-align:right;">&#xE696;{{ link_to_route('list.new.result', 'もっと見る', ['uid' => 'NULLGWDOCOMO']) }}</div>
    @include('layouts.front_elements._gotop_fp')

@endsection
