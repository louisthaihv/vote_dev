<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableVoteResults extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('result_votes', function($table) {
            $table->dropColumn('attribute_value');
            $table->dropColumn('ranking');
            $table->dropColumn('summary_value');
        });

        Schema::table('result_votes', function($table) {
            $table->integer('attribute_value_id')->after('attribute_id');
            $table->integer('ranking')->unsigned()->nullable()->after('choice_id');
            $table->integer('summary_value')->nullable()->after('voted_count');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('result_votes', function($table) {
            $table->dropColumn('attribute_value_id');
            $table->dropColumn('ranking');
            $table->dropColumn('summary_value');
        });

        Schema::table('result_votes', function($table) {
            $table->integer('attribute_value')->after('attribute_id');
            $table->integer('ranking')->unsigned()->after('choice_id');
            $table->integer('summary_value')->after('voted_count');
        });
	}

}
