<?php
namespace Admin;

use docomo\Models\Attentions\Attention;

class AttentionController extends \BaseController
{
    
    public function __construct()
    {

    }

    /**
     * View list of attentions
     * @return View [View show list attentions]
     */
    
    public function index()
    {

        $attentions = Attention::getList();

        return \View::make('admin.attention.index', compact("attentions"));
    }
    
    /**
     * [create Attention]
     * @return View create new attention page
     */
    public function create()
    {

        return \View::make('admin.attention.create', ['attention' => new Attention]);
    }
    
    /**
     * [postCreate process data before save to database]
     * @return View [save data to database or validate date]
     */
    public function store()
    {

        $attention = Attention::fillWithModify();

        if ($attention->save())
        {
            return \Redirect::route('manage.attention.index');
        }
        else
        {
            return \View::make('admin.attention.create', compact('attention'));
        }
    }

    /**
     * [edit] Visible form attention
     * @param  [int] $id [id of attention edit]
     * @return [View]     [View edit attention]
     */
    public function edit($id)
    {

        return \View::make('admin.attention.edit', ['attention' => Attention::getDetails($id)]);
    }

    /**
     * [postEdit process data]
     * @param  [int] $id [description]
     * @return [type]     [process data before save databas]
     */
    public function update($id)
    {

        $attention = Attention::fillWithModify($id);

        if ($attention->save())
        {
            return \Redirect::route('manage.attention.index');
        }
        else
        {
            return \View::make('admin.attention.edit', compact('attention'));
        }
    }

    /**
     * [delete delete attention]
     * @param  [int] $id [id of attention]
     * @return [View]     [list attention]
     */
    public function destroy($id)
    {
        $attention = Attention::findOrFail($id);
        $attention->delete();

        return \Redirect::route('manage.attention.index');
    }
}
