<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('login to my Docomo account');
$I->amOnRoute('admin.home');
$I->amOnPage('/manage');
$I->fillField('login_id','admin');
$I->fillField('login_password','123456');
$I->click('ログイン');
$I->amOnAction('Admin\HomeController@login');
$I->amOnRoute('admin.home');

$I->see('みんなの声　管理画面','//header/h1');
$I->see('Tuan Dam Thanh','header');
$admin_logout = URL::route('admin.logOut');
$I->seeLink('ログアウト',$admin_logout);
$home = URL::route('admin.home');
