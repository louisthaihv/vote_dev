<?php
namespace docomo\Models\Forms;

use docomo\Models\Questions\QuestionTag;
use Illuminate\Support\Facades\Input;
use docomo\Models\Questions\Question;

class QuestionSearchConditionForm
{

    public $genre = DDL_ALL;
    public $keyword = '';
    public $date_from_start = '';
    public $date_from_end = '';
    public $date_to_start = '';
    public $date_to_end = '';
    public $register = DDL_ALL;
    public $status = DDL_ALL;
    public $column_only = false;
    public $question_id = '';
    public $title_choice_text = '';
    public $viewable_type = DDL_ALL;

    public $sort_column = '';
    public $sort_order = 'ASC';

    public function __construct()
    {

    }

    public function isValidKeyword()
    {

        return (isset($this->keyword_text) && $this->keyword_text != '');
    }

    public static function fill()
    {

        $condition = new QuestionSearchConditionForm;
        $condition->genre               = Input::get('genre');
        $condition->keyword             = Input::get('keyword_id');
        $condition->keyword_text        = Input::get('keyword_name');
        $condition->date_from_start     = Input::get('date_from_start');
        $condition->date_from_end       = Input::get('date_from_end');
        $condition->date_to_start       = Input::get('date_to_start');
        $condition->date_to_end         = Input::get('date_to_end');
        $condition->register            = Input::get('register');
        $condition->status              = Input::get('status');
        $condition->column_only         = Input::has('column_only');
        $condition->question_id         = Input::get('question_id');
        $condition->title_choice_text   = Input::get('title_choice_text');
        $condition->viewable_type       = Input::get('viewable_type');

        $condition->sort_column         = Input::get('sortColumn');
        $condition->sort_order          = Input::get('sortOrder');

        return $condition;
    }

    public function searchQuestionList()
    {
        if (empty($this->status))
        {
            return null;
        }

        $questions = Question::select('id', 'title', 'status', 'genre_parent_id', 'vote_date_from', 'vote_date_to', 'attribute_value_id', 'viewable_type', 'genre_child_id', 'vote_count', 'account_id');

        if (!empty($this->keyword)) {
            $arrayIds = QuestionTag::select('question_id')->whereKeywordId($this->keyword)->get()->toArray();
            if (count($arrayIds) == 0) {
                $arrayIds = array('0');
            }
            $questions = $questions->whereIn('id', $arrayIds);
        }
        if ($this->genre != DDL_ALL) {
            if (strstr($this->genre, 'parent_')) {
                $questions = $questions->whereGenreParentId(str_replace('parent_', '', $this->genre));
            } else {
                $questions = $questions->whereGenreChildId($this->genre);
            }
        }
        if (!empty($this->date_from_start) && strptime($this->date_from_start, '%Y%m%d')) {
            $questions = $questions->where('vote_date_from', '>=', $this->date_from_start);
        }
        if (!empty($this->date_from_end) && strptime($this->date_from_end, '%Y%m%d')) {
            $questions = $questions->whereRaw('`vote_date_from` <= DATE_ADD(?, INTERVAL 1 DAY)', [$this->date_from_end]);
        }
        if (!empty($this->date_to_start) && strptime($this->date_to_start, '%Y%m%d')) {
            $questions = $questions->where('vote_date_to', '>=', $this->date_to_start);
        }
        if (!empty($this->date_to_end) && strptime($this->date_to_end, '%Y%m%d')) {
            $questions = $questions->whereRaw('`vote_date_to` <= DATE_ADD(?, INTERVAL 1 DAY)', [$this->date_to_end]);
        }
        if ($this->register != DDL_ALL) {
            $questions = $questions->whereAccountId($this->register);
        }
        if ($this->status != DDL_ALL) {
            $questions = $questions->whereStatus($this->status);
        }
        if ($this->column_only) {
            $questions = $questions->whereHasColumn(true);
        }
        if (!empty($this->question_id)) {
            $questions = $questions->whereId($this->question_id);
        }
        if (!empty($this->title_choice_text)) {
            $questions = $questions->whereRaw('(questions.title LIKE ? OR questions.title LIKE ? OR ' .
                'questions.id IN (SELECT question_id FROM choices WHERE choice_text LIKE ? OR choice_text LIKE ?))',
                ['%' . trim(mb_convert_kana($this->title_choice_text, 'KV')) . '%', '%' . trim(mb_convert_kana($this->title_choice_text, 'k')) . '%',
                    '%' . trim(mb_convert_kana($this->title_choice_text, 'KV')) . '%', '%' . trim(mb_convert_kana($this->title_choice_text, 'k')) . '%']);
        }
        if (!empty($this->viewable_type) &&
            $this->viewable_type != DDL_ALL) {
            $questions = $questions->whereViewableType(intval($this->viewable_type));
        }

        if ($this->sort_column == 'from') {
            if ($this->sort_order == 'asc') {
                $questions = $questions->orderDateFromAsc();
            } else {
                $questions = $questions->orderDateFromDesc();
            }
        }
        if ($this->sort_column == 'to') {
            if ($this->sort_order == 'asc') {
                $questions = $questions->orderDateToAsc();
            } else {
                $questions = $questions->orderDateToDesc();
            }
        }

        return $questions->orderBy('id', 'DESC')->paginate(PAGER_SIZE);

    }
}
