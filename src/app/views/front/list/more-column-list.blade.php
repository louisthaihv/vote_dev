@if ($questions->count() > 0)
    @foreach($questions as $question)
        @if ($question->isViewable())
<li>
    <a href="{{ $question->getColumnPath()  }}">
        <span class="text">{{ $question->column->column_title }}</span>
        <span class="date">{{ jpDate($question->column->view_create_datetime, 'Y年m月d日 H:i') }}</span>
    </a>
</li>
        @endif
    @endforeach
    @if ($questions->currentPage === $questions->lastPage)
<li class="last" style="display:none;"></li>
    @endif
@elseif ($morePager == 1)
    <li class="noResults">コラムはありません。</li>
@endif
