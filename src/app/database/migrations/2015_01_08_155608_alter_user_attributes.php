<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserAttributes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasColumn('user_attributes', 'id')) {
			Schema::table('user_attributes', function ($table) {
				$table->dropColumn('id');
				$table->primary(['user_id', 'attribute_id']);
			});
		}

		if (!Schema::hasColumn('attributes', 'attribute_label')) {
			Schema::table('attributes', function ($table) {
				$table->string('attribute_label', 60)->after('attribute_name')->nullable();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
