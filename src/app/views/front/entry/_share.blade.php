<div  class="sharebtnarea">
    <aside class="sharebtn clearfix m-margin">
        <p class="socialRead m-margin">友達にも教えよう！</p>
        <ul>
            <li class="twitter">
                <div class="shareCount">
                    <span>-</span>
                </div>
                <a id="shareTwitter" href="" target="_blank">{{ HTML::image('common/front/images/sp/btn_social_twitter.png', '', ['class' => 'socialbtn'])}}</a>
            </li>
            <li class="facebook">
                <div class="shareCount">
                    <span>-</span>
                </div>
                <a id="shareFacebook" href="https://m.facebook.com/dialog/feed?app_id=249260871872761&link={{ Request::url() }}{{ SHARE_LINK_PARAMETER_FACEBOOK }}&redirect_uri=http%3A%2F%2Fm.facebook.com%2F" target="_blank">{{ HTML::image('common/front/images/sp/btn_social_fb.png', '', ['class' => 'socialbtn'])}}</a>
            </li>
            <li class="line">
                <a id="shareLine" href="http://line.me/R/msg/text/{{ $question->getSnsComment() }}{{ urlencode(Request::url()) }}" target="_blank">{{ HTML::image('common/front/images/sp/btn_social_line.png', '', ['class' => 'socialbtn'])}}</a>
            </li>
        </ul>
    </aside>
</div>
<script>
var url_param_tw = '{{ SHARE_LINK_PARAMETER_TWITTER }}';
var url_param_fb = '{{ SHARE_LINK_PARAMETER_FACEBOOK }}';
var url = '{{ str_replace('/'.Request::path(), '', Request::url()) . $question->getPath() }}';
var sns_comment = '{{ $question->getSnsComment() }}';
function updateLink(attributes)
{
    if (attributes == '' || {{ $question->isFinished() ? 'true' : 'false' }})
    {
        $('#shareTwitter').attr('href', 'http://twitter.com/share?url=' + url + attributes + url_param_tw + '&text=' + sns_comment);
        $('#shareFacebook').attr('href', 'https://m.facebook.com/dialog/feed?app_id=249260871872761&link=' + url + attributes + url_param_fb + '&redirect_uri=http%3A%2F%2Fm.facebook.com%2F');
        $('#shareLine').attr('href', 'http://line.me/R/msg/text/?' + sns_comment + url + attributes);
    }
}
updateLink('');
$(function() {
    //シェア数取得
    var url = "{{ Request::url() }}";

    //Twitter
    $.ajax({

       url:"http://urls.api.twitter.com/1/urls/count.json?url=" + encodeURIComponent(url),
       dataType:"jsonp",
        beforeSend:function (xhr) {
            $(window).bind( 'beforeunload', function() {
                if (xhr) {
                    xhr.abort();
                }
            })
        },
       success:function(obj){
          $(".twitter .shareCount span").text(obj.count);
       },
       complete:function(){
          return false;
       }
    });

    //FaceBook
    $.ajax({

       url:"http://graph.facebook.com/?id=" + encodeURIComponent(url),
       dataType:"jsonp",

        beforeSend:function (xhr) {
            $(window).bind( 'beforeunload', function() {
                if (xhr) {
                    xhr.abort();
                }
            })
        },

       success:function(obj){

          //データが存在しない場合は0扱い
          if(typeof(obj.shares) == 'undefined'){
             count = 0;
          }else{
             count = obj.shares;
          }
          $(".facebook .shareCount span").text(count);
       },

       complete:function(){
          return false;
       }

    });
});
</script>
