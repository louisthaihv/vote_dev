@extends('layouts.admin_in')

@section('content')
<h2>画像ファイルアップロード</h2>
@include('admin.upload.form', ['upload' => $upload])
@stop