@include('layouts.front_elements._space')
<div style="background:#f39800;">
    <div style="margin:0px 10px 0px 10px;">
        @include('layouts.front_elements._space')
        <font color="#fff">{{ $keyword_name }}に関する受付中ｱﾝｹｰﾄ</font>
        @include('layouts.front_elements._space')
    </div>
</div>

@include('layouts.front_elements._space')

<div style="text-align:center;">&#xE6DE;<a href="{{ route('list.tag.result', $params['keyword_id']) }}?uid=NULLGWDOCOMO">結果発表はこちら</a></div>

@include('layouts.front_elements._space')

<div style="background-color:#fff6ab;padding:5px;text-align:left;"><font color="#f39800">▼</font><font color="#3e3a39"> 受付中 </font></div>

@include('layouts.front_elements._space')

<div style="margin:0px 10px 0px 10px;">
    @foreach($questions as $question)
        @include('layouts.front_elements._list_question', ['$question'=>$question])
    @endforeach
</div>

{{ $questions->links('layouts.front_elements.paginate_fp') }}

@include('layouts.front_elements._space')
@include('layouts.front_elements._gotop_fp')