<?php
namespace AcceptanceTester;

class ListByKeywordSteps extends \AcceptanceTester\CommonSteps
{
	public function checkFPListByKeyword($keyword)
	{
		$I = $this;
		$this->checkKeywordTitle($keyword['name']);
		$this->checkLinkSwitch($keyword['link_switch']);
		$this->checkQuestionList($keyword['questions']);
	}

	public function checkKeywordTitle($keyword)
	{
		$I = $this;
		$title = sprintf('%sに関する受付中ｱﾝｹｰﾄ', $keyword);
		$I->see($title);
	}

	public function checkLinkSwitch($link_switch)
	{
		$I = $this;
		$I->checkLinkCommon($link_switch);
	}

	public function checkQuestionList($questions)
	{
		$I = $this;
		$this->checkQuestionListCommon($questions);
		$this->checkPager($questions);
	}

	public function checkPager($questions, $current_page)
	{
		$total_page = ceil(count($questions)/10);
		if ($total_page <= 1) {
			$I->dontSeeLink('');
			$I->dontSeeLink('');
		}
		else 
		{
			if ($current_page==1) {
				# code...
			}
		}
	}

}