<?php
namespace Front;

class GuideController extends \BaseController
{
    public function __construct()
    {

    }
    
    public function help()
    {

        return $this->renderView(\View::make('front.guide.help'));
    }

    public function caution() {

        return \View::make('front.guide.caution');
    }
}
