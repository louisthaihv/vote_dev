@extends('layouts.front_fp')

@section('content')

    {{--Entry--}}
    @if((Route::currentRouteName() == 'list.genre.entry'))
        @include('front.list.genre._shared.entry_fp')
    @endif
    {{--Result--}}
    @if((Route::currentRouteName() == 'list.genre.result'))
        @include('front.list.genre._shared.result_fp')
    @endif

@endsection
