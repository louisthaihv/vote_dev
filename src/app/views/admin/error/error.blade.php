@extends('layouts.admin_in')

@section('content')
<h2>エラーが発生しました</h2>
<div class="lrMargin m-margin">
    <p>エラーが発生しました。システム管理者にお問い合わせください。</p>
</div>

@stop