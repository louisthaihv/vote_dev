<?php
namespace docomo\Models\Genres;

use Illuminate\Support\MessageBag;

class ParentGenre extends \BaseArdentModel
{

    protected $table = 'genre_parents';
    public static $parentGenreData = null;
    public static $ignoreData = null;

    public static $rules = [
        'genre_parent_name' => ['sometimes', 'required', 'max:60', 'deny_tag', 'deny_char'],
        'genre_parent_directory' => ['sometimes', 'required', 'directory', 'unique:genre_parents', 'max:60'],
        'parent_sort_order' => ['sometimes', 'numeric', 'max:10000']
    ];

    public static $customMessages = [
        'genre_parent_name.required' => W_FRM_REQUIRED_PARENT_GENRE,
        'genre_parent_name.max' => W_FRM_MAX_PARENT_GENRE,
        'genre_parent_name.deny_tag' => W_FRM_DENY_TAG_PARENT_GENRE,
        'genre_parent_name.deny_char' => W_FRM_DENY_CHAR_PARENT_GENRE,
        'genre_parent_directory.required' => W_FRM_REQUIRED_DIRECTORY,
        'genre_parent_directory.directory' => W_FRM_ALPHANUM_DIRECTORY,
        'genre_parent_directory.unique' => W_FRM_UNIQUE_PARENT_DIRECTORY,
        'genre_parent_directory.max' => W_FRM_MAX_DIRECTORY,
        'parent_sort_order.numeric' => W_FRM_FORMAT_SORT_ORDER,
        'parent_sort_order.max' => W_FRM_MAX_SORT_ORDER
    ];

    protected $fillable = [
        'genre_parent_name',
        'genre_parent_directory'
    ];

    public static $sort_step = 10;

    public static $cacheName = 'ParentGenre';
    public static $ignoreCache = 'IgnoreParentGenre';

    /**
     * relationship with genre_children
     */
    public function children()
    {

        return $this->hasMany('docomo\Models\Genres\ChildGenre', 'genre_parent_id', 'id');
    }

    public function scopeVisible($query)
    {

        return $query->where('parent_is_visible', '=', VISIBLE);
    }

    public function scopeOrderBySort($query)
    {

        return $query->orderBy('parent_sort_order');
    }

    public function scopeSelectList($query)
    {

        return $query->select('id', 'genre_parent_name', 'genre_parent_directory', 'parent_is_visible', 'parent_sort_order');
    }

    public function scopeWithChildren($query)
    {

        return $query->with(['children' => function ($q) {
            $q->selecT('id', 'genre_child_name', 'genre_child_directory');
        }]);
    }

    public function getChildCount()
    {

        return ChildGenre::whereGenreParentId($this->id)->sum('child_is_visible');
    }

    public function getChildren()
    {

        return ChildGenre::getDataByCache($this->id);
    }

    public static function getListForDDL()
    {

        return ParentGenre::select('id', 'genre_parent_name')->orderBySort()->visible()->get();
    }

    public static function getListForDDLWithChildren($genre_parent_id = null)
    {

        $genres = ParentGenre::getListForDDL();
        $options = [];

        foreach ($genres as $genre)
        {
            if ($genre_parent_id != ALLOW_ALL_GENRE && $genre->id != $genre_parent_id) {
                continue;
            }
            $options += [$genre->id => [$genre->genre_parent_name , ChildGenre::whereParent($genre->id)->visible()->get(['id', 'genre_child_name', 'genre_child_description'])]];
        }

        return $options;
    }

    /**
     * [get key all genre in table category_parents by $flag]
     *
     * @param  [int|NULL] $flag [flag asign this method get all genre or get only genre visible]
     * @return [Array]       [Array of ParentGenre objects]
     */
    public static function getList($show_all = false)
    {
        if ($show_all)
        {
            return ParentGenre::orderBySort()->selectList()->get();
        }
        return ParentGenre::visible()->orderBySort()->selectList()->get();
    }


    public static function fillWithModify($id = null)
    {

        $genre = null;
        if ($id === null)
        {
            $genre = new ParentGenre;
            $genre->fill(\Input::all());
        }
        else
        {
            $genre = ParentGenre::find($id);

            if ($genre->genre_parent_name == \Input::get('genre_parent_name')) {
                unset($genre->genre_parent_name);
            } else {
                $genre->genre_parent_name = \Input::get('genre_parent_name');
            }
            if ($genre->genre_parent_directory == \Input::get('genre_parent_directory')) {
                unset($genre->genre_parent_directory);
            } else {
                $genre->genre_parent_directory = \Input::get('genre_parent_directory');
            }
        }

        $genre->parent_is_visible = \Input::has('parent_is_visible') ? HIDDEN : VISIBLE;

        if (is_null($genre->parent_sort_order))
        {
            $genre->parent_sort_order = ParentGenre::maxSort() + ParentGenre::$sort_step;
        }

        return $genre;
    }

    public static function sort()
    {
        $errors = new MessageBag();

        $list = ParentGenre::select('id', 'parent_sort_order')->visible()->orderBySort()->orderBy('id')->get();

        $input_list = [];
        foreach ($list as $record)
        {
            $input_list[$record->id] = \Input::get('parent_sort_order_' . $record->id);
            if (!is_numeric($input_list[$record->id])) {
                $errors->add('sort.numeric' . $record->id, 'ID:' . $record->id . ') ' . W_FRM_FORMAT_SORT_ORDER);
            } elseif (strlen($input_list[$record->id]) > 5 || intval($input_list[$record->id]) > 10000) {
                $errors->add('sort.numeric' . $record->id, 'ID:' . $record->id . ') ' . W_FRM_MAX_SORT_ORDER);
            }
        }
        unset($list);
        if ($errors->count() > 0) {
            return $errors;
        }
        asort($input_list);

        $update_list = [];
        foreach ($input_list as $key => $value)
        {
            if (!array_key_exists($value, $update_list))
            {
                $update_list[$value] = [];
            }
            $update_list[$value][] = $key;
        }

        $sort_order = ParentGenre::$sort_step;

        foreach ($update_list as $key => $value)
        {
            sort($value);

            for ($j = 0; $j < count($value); $j++)
            {
                $record = ParentGenre::select('id', 'parent_sort_order')->find($value[$j]);
                $record->parent_sort_order = $sort_order;

                $record->save();
                $sort_order += ParentGenre::$sort_step;
            }
        }
        unset($update_list);

        return $errors;
    }

    public static function sortAll()
    {
        $data = ParentGenre::select('id', 'parent_sort_order')->orderBy('parent_sort_order')->orderBy('id')->get();

        $sort_order = ParentGenre::$sort_step;
        foreach ($data as &$record)
        {
            ParentGenre::whereId($record->id)
                ->update(['parent_sort_order' => $sort_order]);

            $sort_order += ParentGenre::$sort_step;
        }
    }

    public static function maxSort()
    {

        return ParentGenre::visible()->max('parent_sort_order');
    }

    public static function getDataByCache($genre_parent_id = null) {
        if (is_null(ParentGenre::$parentGenreData)) {
            $updateTime = ParentGenre::checkCache(ParentGenre::$cacheName);

            ParentGenre::setMemoryByCache($updateTime);
        }

        if (is_null($genre_parent_id)) {
            return ParentGenre::$parentGenreData;
        } else {
            if (array_key_exists($genre_parent_id, ParentGenre::$parentGenreData)) {

                return ParentGenre::$parentGenreData[$genre_parent_id];
            } else {

                return null;
            }
        }
    }

    public static function getIgnoreByCache() {
        if (is_null(ParentGenre::$ignoreData)) {
            $updateTime = ParentGenre::checkCache(ParentGenre::$cacheName);

            ParentGenre::setMemoryByCache($updateTime);
        }

        return ParentGenre::$ignoreData;
    }

    public static function setDataToCache($updateTime)
    {
        $data = ParentGenre::select('id', 'genre_parent_name', 'parent_is_visible', 'genre_parent_directory', 'parent_is_visible')
            ->whereRaw('`id` > 1')
            ->orderBy('parent_sort_order')->get();
        $setData = [];
        $ignoreData = [];
        foreach ($data as &$record) {
            if ($record->parent_is_visible) {
                $setData[$record->id] = $record;
            } else {
                $ignoreData[] = $record->id;
            }
        }
        \Cache::put(ParentGenre::$cacheName, $setData, DEFAULT_CACHE_TIME);
        ParentGenre::setCacheTime(ParentGenre::$cacheName, $updateTime);

        \Cache::put(ParentGenre::$ignoreCache, $ignoreData, DEFAULT_CACHE_TIME);

        ParentGenre::setMemoryByCache($updateTime);

        return $setData;
    }

    public static function getIgnoreList() {
        if (is_null(ParentGenre::$ignoreData)) {
            $updateTime = ParentGenre::checkCache(ParentGenre::$cacheName);

            ParentGenre::setMemoryByCache($updateTime);
        }

        return ParentGenre::$ignoreData;
    }

    public static function setMemoryByCache($updateTime) {
        ParentGenre::$parentGenreData = \Cache::get(ParentGenre::$cacheName, function () use($updateTime) {

            return ParentGenre::setDataToCache($updateTime);
        });

        ParentGenre::$ignoreData = \Cache::get(ParentGenre::$ignoreCache, []);
    }
}
