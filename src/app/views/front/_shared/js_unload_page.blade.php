{{ HTML::script('common/front/js/tab_change.js') }}   
<script type="text/javascript">
        $(document).ready(function() {
        
        var url = location.href;
        if(url.match("entry") != 'entry')
            $('#loadResult').addClass('active');

        $('#loadEntry').click(function(){ 
            history.pushState({},'','{{ route($route . ".entry", isset($params) ? $params : null) }}');
        });

        $('#loadResult').click(function(){ 
            history.pushState({},'','{{ route($route . ".result", isset($params) ? $params : null) }}');
        });


        $("ul.tabs").tabChange({content: ".tabs_content", animate: true});  
    });
</script>
