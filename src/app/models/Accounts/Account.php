<?php
namespace docomo\Models\Accounts;

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

use docomo\Models\Accounts\AccountWasLogined;

class Account extends \BaseArdentModel implements UserInterface, RemindableInterface
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'accounts';
    protected $hidden = ['login_password'];
    public static $passwordAttributes = ['login_password'];

    public static $cacheName = 'Account';

    /*
     * Acceptable params from input
     */
    protected $fillable = [
        'account_name',
        'login_id',
        'login_password',
        'allow_genre_parent',
        'comment'
    ];

    public static $rules = [
        'login_id' => ['required', 'alpha_num_utf8', 'unique:accounts', 'max:60'],
        'login_password' => ['required', 'alpha_num_utf8', 'max:60'],
        'account_name' => ['required', 'deny_tag', 'deny_char', 'max:60', 'unique:accounts'],
        'comment' => ['sometimes', 'max:500', 'deny_char']
    ];

    public static $customMessages = [
        'login_id.required' => W_FRM_REQUIRED_LOGIN_ID,
        'login_id.unique' => W_FRM_UNIQUE_LOGIN_ID,
        'login_id.alpha_num_utf8' => W_FRM_ALPHANUM_LOGIN_ID,
        'login_id.max' => W_FRM_MAX_LOGIN_ID,
        'login_password.required' => W_FRM_REQUIRED_LOGIN_PASSWORD,
        'login_password.alpha_num_utf8' => W_FRM_ALPHANUM_LOGIN_PASSWORD,
        'login_password.max' => W_FRM_MAX_LOGIN_PASSWORD,
        'account_name.required' => W_FRM_REQUIRED_ACCOUNT_NAME,
        'account_name.max' => W_FRM_MAX_ACCOUNT_NAME,
        'account_name.deny_tag' => W_FRM_DENY_TAG_ACCOUNT_NAME,
        'account_name.deny_char' => W_FRM_DENY_CHAR_ACCOUNT_NAME,
        'account_name.unique' => W_FRM_UNIQUE_ACCOUNT_NAME,
        'comment.max' => W_FRM_MAX_ACCOUNT_COMMENT,
        'comment.deny_char' => W_FRM_DENY_CHAR_COMMENT
    ];
    
    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }
    
    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        // First decrypt the encrypted password in database
        // Then Hash return the normal hashed password
        return \Hash::make(\Crypt::decrypt($this->login_password));
    }
    
    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return $this->remember_token;
    }
    
    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }
    
    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'remember_token';
    }
    
    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }
    
    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname()
    {
        
        return $this->account_name;
    }
    
    /**
     * [raise object]
     *
     * @return [void] []
     */
    public function raiseLogin()
    {
        
        $this->raise(new AccountWasLogined($this));
    }

    /**
     * Get account data for list
     *
     * @return account[]
     */
    public static function getList($show_all = false)
    {
        $columns = ['id', 'login_id', 'account_name', 'is_enabled'];
        if ($show_all) {
            return Account::get($columns);
        }
        
        return Account::where('is_enabled', '=', ENABLED)->get($columns);
    }

    public static function getListForDDL($account_id = null)
    {

        if ($account_id === null)
        {

            return Account::orderBy('id')->get(['id', 'account_name']);
        }
        else
        {

            return Account::where('id', '=', $account_id)->get(['id', 'account_name']);
        }
    }

    public static function getDataByCache() {
        $updateTime = Account::checkCache(Account::$cacheName);

        $accounts = \Cache::get(Account::$cacheName, function () use($updateTime) {
            return Account::setCache($updateTime);
        });

        return $accounts;
    }

    public static function setCache($updateTime) {
        $data = Account::select('id', 'account_name')->get();
        $setData = [];
        foreach ($data as $rec) {
            $setData[$rec->id] = $rec->account_name;
        }
        \Cache::put(Account::$cacheName, $setData, DEFAULT_CACHE_TIME);
        Account::setCacheTime(Account::$cacheName, $updateTime);

        return $setData;
    }

    public static function getAccountName($account_id) {

        return Account::getDataByCache()[$account_id];
    }

    /**
     * Find record and decrypt password
     *
     * @return account[]
     */
    public static function findWithDecrypt($id)
    {
        $account = parent::findOrFail($id);
        $account->login_password = \Crypt::decrypt($account->login_password);

        return $account;
    }

    /**
     * Fill form data and modify data
     *
     * @return account[]
     */
    public static function fillWithModify($id = null)
    {
        if (is_null($id)) {
            $account = new Account;
        } else {
            $account = Account::findWithDecrypt($id);
            $account->removeUniqueFromLoginId(
                \Input::get('login_id') === $account->login_id,
                \Input::get('account_name') === $account->account_name);
        }
        $account->fill(\Input::all());

        $account->is_visible_register       = Account::getPermission('is_visible_register');
        $account->is_manage_genre           = Account::getPermission('is_manage_genre');
        $account->is_manage_keyword         = Account::getPermission('is_manage_keyword');
        $account->is_manage_account         = Account::getPermission('is_manage_account');
        $account->is_manage_notice          = Account::getPermission('is_manage_notice');
        $account->is_manage_feature         = Account::getPermission('is_manage_feature');
        $account->is_manage_pickup          = Account::getPermission('is_manage_pickup');
        $account->is_manage_attention       = Account::getPermission('is_manage_attention');
        $account->is_manage_agree           = Account::getPermission('is_manage_agree');
        $account->is_manage_input_comment   = Account::getPermission('is_manage_input_comment');
        $account->is_manage_column          = Account::getPermission('is_manage_column');
        $account->is_manage_summary         = Account::getPermission('is_manage_summary');
        $account->is_viewable_all_question  = Account::getPermission('is_viewable_all_question');

        $account->is_enabled                = (\Input::has('is_enabled')) ? DISABLED : ENABLED;

        $account->allow_genre_parent        = ($account->allow_genre_parent === DDL_ALL) ? 0 : $account->allow_genre_parent;

        return $account;
    }

    public static function getPermission($key)
    {

        return (\Input::has($key)) ? ALLOW : DISALLOW;
    }

    /**
     * Check the permission
     *
     * @return boolean
     */
    public function havePermission($permission)
    {
    
        return $this->$permission == ALLOW;
    }

    public function removeUniqueFromLoginId($removeId, $removeName)
    {
        if ($removeId) {
            unset(Account::$rules['login_id'][2]);
        }
        if ($removeName) {
            unset(Account::$rules['account_name'][4]);
        }
    }

    public function setLoginFillable()
    {

    }
}
