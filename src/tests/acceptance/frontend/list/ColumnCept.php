<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('check list column');

$link_for_showmore = 'もっと見る';

$link_end_list = array(
					'title' => '未回答一覧',
					'url'	=> '/list/unanswered'
				);

//xpath format of list
$xpath_format = 'html/body/div[1]/div/div/section[2]/div/ul/li[%d]/a';

//create survey
$surveys = array();
for ($i=1; $i < 12; $i++) {
	$surveys[$i] = array(
                    'genre_parent_id'=>'1',
                    'genre_child_id'=>'1',
                    'status'=>'5',
                    'title'=>'title',
                    'vote_date_from'=>'2014-12-23 03:40:52',
                    'vote_date_to'=>'2014-12-31 00:00:00',
                    'baseline_date'=>'2014-12-23 03:40:52',
                    'viewable_type'=>'1',
                    'visible_device_i'=>'1',
                    'visible_device_d'=>'1',
                    'summary_type'=>'1',
                    'average_unit'=>'1',
                    'is_visible_other'=>'1',
                    'shuffle_choice'=>'1',
                    'navigation_title'=>'1',
                    'account_id'=>'1',
                    );
}
$I->createDatabaseSurvey($surveys);

//create columns
$columns = array();
for ($i=1; $i < 12; $i++) {
	$title = 'column_title_'.$i;
	$date = '2014-12-'.($i+10).' 00:00:00';
	$columns[$i] = array(
						'question_id' => $i,
						'is_visible' => '1',
						'column_title' => $title,
						'column_detail' => 'detail',
						'view_create_datetime' => $date,
						'created_account_id' => '1',
						'updated_account_id' => '1',
						);
}
$I->createColumns($columns);


//after create output 11 items
$items_check = array();
foreach ($columns as $column) {
	$jpdate = $I->dateToJpdate($column['view_create_datetime']);
	$items_check[] = array('title' => $column['column_title'], 'jpdate' => $jpdate, 'status' => '1');
}

//order list to see
krsort($items_check);

//checklist
$I->amOnPage('/list/column');
$I->checkListCommons($items_check, $xpath_format, 10);
$I->checkListShowmoreCommons($link_for_showmore, $items_check, $xpath_format, 11);
$I->checkEndListCommons($link_for_showmore);