function keywordCheck(){
    $('.keywordChoiceBox label').removeClass('check');
    $('.keywordChoiceBox input:checked').closest('label').addClass('check');
}

var addStyle = "";
function viewSummaryType()
{
    if($('input:radio[name="summary_type"]:checked').val() == '2' ||
        2 == summaryType){
        $('.unit').css('display','inline-block');
        $('.taniDisabled').attr('disabled','disabled');
        $('.summaryValue').css('display', 'table-cell');
        addStyle = "";
    }else{
        $('.unit').hide();
        $('.taniDisabled').removeAttr('disabled');
        $('.summaryValue').hide();
        addStyle = "display:none;";
    }
}

function recreateGenreList()
{
    var tmp_list = Array();
    for (var parent in genre_list){
        for (var child in genre_list[parent][1]){
            tmp_list[genre_list[parent][1][child]['id']] = genre_list[parent][1][child]['genre_child_description'];
        }
    }
    genre_list = tmp_list;
}

function checkChoicesBeforeConfirm(message)
{

    if ($('#choiceListAll').length == 0 || $('#choiceListAll')[0].style.display == 'none') {
        return confirm(message);
    } else {
        alert('選択肢の一括作成BOXが開いたままになっています。\n先に入力内容を確定してください。');
        return false;
    }
}

$(document).ready(function() {

    $('button.draft').click(function() {

        if (checkChoicesBeforeConfirm(C_SAVE_DRAFT)) {
            $('#status').val(QUESTION_STATUS_DRAFT);
            return true;
        }
        return false;
    });

    $('button.overwrite').click(function() {
        if (checkChoicesBeforeConfirm(C_OVERWRITE)) {
            $('#status').val(0);
            return true;
        }
        return false;
    });

    $('button.request').click(function() {
        if (checkChoicesBeforeConfirm(C_REQUEST_APPROVE)) {
            $('#status').val(QUESTION_STATUS_REQUEST_APPROVE);
            return true;
        }
        return false;
    });

    $('button.approval').click(function() {
        if (checkChoicesBeforeConfirm(C_APPROVAL)) {
            $('#status').val(QUESTION_STATUS_WAITING);
            return true;
        }
        return false;
    });

    $('button.update').click(function() {
        if(checkChoicesBeforeConfirm(C_UPDATE)) {
            $('#status').val(0);
            return true;
        }
        return false;
    });

    $('input:radio[name="summary_type"]').change(function(){
        summaryType = $('input:radio[name="summary_type"]:checked').val();
        viewSummaryType();
    });

    //日付ピッカー
    $("input[name='vote_date_from_date']").datepicker({
        dateFormat: 'yymmdd'
    });
    //日付ピッカー
    $("input[name='vote_date_to_date']").datepicker({
        dateFormat: 'yymmdd'
    });

    $('#copyDetails').click(function() {
        if (confirm('概要の中身を、説明に入力された内容で上書きしますか？\n※タグは除去されます。')) {
            var setText = $('<div>').html($('#details').val()).text();
            if (setText.length > 200) {
                setText = setText.substr(0, 200);
            }
            $('#description').val(setText);
        }
        return false;
    });

    $('#choiceAdd').click(function(){
        var choiceId = $('table#choiceList tr').length;
        if (!choiceCount || choiceCount == 0) {
            choiceCount = $('table#choiceList tr').length;
        } else {
            choiceCount = choiceCount + 1;
        }
        $('table#choiceList').append(
            '<tr>'
            +'<td>' + choiceId + '</td>'
            +'<td><input type="text" name="choice_text['+choiceCount+']" value="" class="x-large" /></td>'
            +'<td class="summaryValue" style="' + addStyle + '"><input type="text" name="summary_value['+choiceCount+']" class="x-small" /></td>'
            +'<td><a href="#" class="naviBtn choiceRemove">削除</a></td></tr>'
        );
        return false;
    });

    $('.choiceRemove').live('click',function(){
        $(this).closest('tr').remove();
        var choiceCount = 0;
        $('table#choiceList tr').each(function(){
            $('td:nth-child(1)',$(this)).text(choiceCount);
            choiceCount++;
        });
        return false;
    });
    $('#choiceListAllOpen').click(function(){
        $('textarea[name="choiceTextarea"]').val('');
        $('#choiceListAll').toggle();
        $('#choiceListAdd').toggle();
        return false;
    });
    $('#choiceListAllCreate').click(function(){
        var choiceId = $('table#choiceList tr').length;
        if (!choiceCount || choiceCount == 0) {
            choiceCount = $('table#choiceList tr').length;
        } else {
            choiceCount = choiceCount + 1;
        }
        var choiceList = $('textarea[name="choiceTextarea"]').val().replace(/"/gm, '&quot;').split(/\r\n|\r|\n/);
        for(var i = 0; i < choiceList.length; i++){
            $('table#choiceList')
                .append(
                '<tr>'
                + '<td>'+choiceId+'</td>'
                + '<td><input type="text" value="'+choiceList[i]+'" name="choice_text['+choiceCount+']" class="x-large" /></td>'
                + '<td class="summaryValue" style="' + addStyle + '"><input type="text" name="summary_value['+choiceCount+']" class="x-small" /></td>'
                + '<td><a href="#" class="naviBtn choiceRemove">削除</a></td>'
                + '</tr>'
            );
            choiceCount = choiceCount + 1;
            choiceId = choiceId + 1;
        }
        choiceCount = choiceCount - 1;
        $('#choiceListAll').hide();
        $('#choiceListAdd').show();
        return false;
    });

    $('select[name="genre"]').change(function() {
        $('p#genreComment').text(genre_list[$('select[name="genre"]').val()]);
    });
    keywordCheck();
    $('.keywordChoiceBox input:checkbox').change(function(){
        keywordCheck();
    });

    $('#keywordListOpen').click(function(){
        window.open(ROUTE_TO_ADMIN_KEYWORD, 'keywordwindow', 'width=400, height=300, menubar=no, toolbar=no, scrollbars=yes');
        return false;
    });

    $('#keywordClear a').click(function(){
        $('#keywordClear').hide();
        $("#keywordList").html('<li class="NoSelected">選択されていません</li>');
        $("#keywordIds").text('');
        return false;
    });

    $('#commentWindow').click(function(){
        var preview = window.open('about:blank', 'commentwindow', 'width=400, height=300, menubar=no, toolbar=no, scrollbars=yes');
        preview.document.write('<html><body></body></html>');
        preview.document.body.innerHTML = $('#result_comment').val();
        return false;
    });


    viewSummaryType();
    recreateGenreList();
    $('p#genreComment').text(genre_list[$('select[name="genre"]').val()]);
});

