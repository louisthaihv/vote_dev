@if($errorList->any())
  <div class="errorsArea">
      <p>以下の項目をご確認ください。</p>
      <ul>
          @foreach($errorList->all() as $error)
                <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif