<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableQuestions extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_parent_id');
            $table->integer('category_child_id');
            $table->integer('status');
            $table->string('title', 200);
            $table->text('detail')->nullable();
            $table->datetime('vote_date_from');
            $table->datetime('vote_date_to');
            $table->datetime('baseline_date')->nullable();
            $table->integer('visible_flag');
            $table->integer('visible_device_i');
            $table->integer('visible_device_d');
            $table->integer('summary_type');
            $table->string('average_unit', 60);
            $table->boolean('is_visible_other');
            $table->text('other_text')->nullable();
            $table->boolean('shuffle_choice');
            $table->integer('attribute_id')->nullable();
            $table->string('attribute_value', 60)->nullable();
            $table->text('result_comment')->nullable();
            $table->string('navigation_title', 200);
            $table->text('sns_comment')->nullable();
            $table->text('comment')->nullable();
            $table->string('back_button_text', 200)->nullable();
            $table->string('back_button_url', 200)->nullable();
            $table->boolean('is_column')->nullable();
            $table->string('column_title', 200)->nullable();
            $table->text('column_detail')->nullable();
            $table->integer('account_id');
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
