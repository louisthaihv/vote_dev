<table class="l-margint">
    <?php $totalCount = array_sum($resultData->lists('cnt')) ?>
    <?php $i = 1 ?>
    @foreach($resultData as $result)
    <?php $className = ($i <=3)?'lankNo'.$i:'lankOther' ?>
        <tr>
            <td class="my">&nbsp;</td>
            <td class="lank"><span class="{{$className}}}">{{$i}}</span></td>
            <td class="answer">{{ $result->choice->choice_text }}
                <div class="barArea">
                    <span style="width:{{ calcPercent($result->cnt, $resultData[0]->cnt) }}%;"></span>
                </div>
            </td>
            <td class="data">{{number_format($result->cnt)}}票<br />{{ calcPercent($result->cnt, $totalCount) }}%</td>
        </tr>
        <?php $i++ ?>
    @endforeach

    <input class="totalNum" type="hidden" value="{{ $totalCount }}" >
    
    @if($resultData->count() > 10)
        <a href="#" class="moreAction"><span>11位以下を見る（全{{$resultData->count()}}件）</span></a>
    @endif
</table>
