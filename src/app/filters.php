<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function ($request)
{
    
    //
    
});

App::after(function ($request, $response)
{

    //
    
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function ($route, $request) {
    if (Auth::guest()) {
        return Redirect::guest(route("admin.home"));
    } else if(Auth::user()->is_enabled == false) {
        Auth::logout();
        return Redirect::guest(route('admin.home'));
    }
    if ($request->getMethod() === 'POST' || $request->getMethod() === 'PUT' || $request->getMethod() === 'DELETE') {
        Route::callRouteFilter('csrf', [], '', $request);
    }
});

Route::filter('auth.basic', function () {
    return Auth::basic();
});

Route::filter('auth.manage', function ($route, $request, $name) {
    if (!\Auth::user()->havePermission('is_manage_' . $name)) {

        return \Redirect::to('manage')->with('message', W_MSG_NOT_PERMISSION);
    }
});

Route::filter('nocache', function ($route, $request, $response) {
    $response->header('Cache-Control' ,'no-store, no-cache, must-revalidate');
    $response->header('X-Accel-Expires','0');
});

Route::filter('allow.device', function  ($route, $request) {

    $ua = $request->server('HTTP_USER_AGENT');
    if (strpos($ua, 'Android') === false && strpos($ua, 'FBIOS') === false) {
        $lowUa = strtolower($ua);
        if (strpos($lowUa, 'docomo') !== false) {
            $devices = '/' . \Config::get('app.devices') . '/';
            if (preg_match($devices, $ua) == false) {

                return \Response::view('front._shared.no_support', ['type' => TYPE_I_MODE]);
            }
//            $clientIp = \Request::getClientIp();
            /*
             * IPチェック
                210.153.84.0/24
　　　　　　210.136.161.0/24
　　　　　　210.153.86.0/24
　　　　　　124.146.174.0/24
　　　　　　124.146.175.0/24
　　　　　　202.229.176.0/24
　　　　　　202.229.177.0/24
　　　　　　202.229.178.0/24
             */
//            if (strpos($clientIp, '210.153.84.') === false
//                && strpos($clientIp, '210.153.86.') === false
//                && strpos($clientIp, '210.136.161.') === false
//                && strpos($clientIp, '124.146.174.') === false
//                && strpos($clientIp, '124.146.175.') === false
//                && strpos($clientIp, '202.229.176.') === false
//                && strpos($clientIp, '202.229.177.') === false
//                && strpos($clientIp, '202.229.178.') === false) {
//                // どのIP帯にも属さない場合、対象外
//                return \Response::view('front._shared.no_support', ['type' => TYPE_OTHER]);
//            }
            if (!\Input::has('uid')) {
                return \Redirect::to($request->url() . '?uid=NULLGWDOCOMO');
            }
            \docomo\Models\Users\User::$userDevice = CHAR_I_MODE;
        } elseif (strpos($lowUa, 'au') !== false || strpos($lowUa, 'kddi') !== false || strpos($lowUa,
                'celler') !== false
            || strpos($lowUa, 'j-phone') !== false || strpos($lowUa, 'vodafone') !== false || strpos($lowUa,
                'softbank') !== false
        ) {

            return \Response::view('front._shared.no_support', ['type' => TYPE_OTHER]);
        }
    }
});

Route::filter('header.fp', function ($route, $request, $response) {

    if (\docomo\Models\Users\User::$userDevice == CHAR_I_MODE) {
        $response->header('Content-Type', 'application/xhtml+xml');
    }
});

Route::filter('header.all', function ($route, $request, $response) {
    $response->header('X-FRAME-OPTIONS', 'DENY');
});

Route::filter('header.rss', function ($route, $request, $response) {
    $response->header('Content-Type', 'application/rdf+xml');
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function () {
    if (Auth::check()) {
        return Redirect::to('/');
    }
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function () {
    if (Session::token() !== Input::get('_token')) {
        throw new Illuminate\Session\TokenMismatchException;
    }
});
