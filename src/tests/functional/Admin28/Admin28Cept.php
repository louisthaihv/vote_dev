<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('edit info');

$I->amOnRoute('admin.info.edit');
$I->amOnAction('Admin\InfoController@edit');
