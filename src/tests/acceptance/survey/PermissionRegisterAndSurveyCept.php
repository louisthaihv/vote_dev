<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('test survey');
$I->amOnPage('/manage');
$I->loginClickCommon('キーワード一覧・登録・修正・削除');
$I->click('＋キーワード追加');
$I->createNewKeyword('keyword_survey_test_1');
$I->click('＋キーワード追加');
$I->createNewKeyword('keyword_survey_test_2');
$I->click('＋キーワード追加');
$I->createNewKeyword('keyword_survey_test_3');
$I->click('＋キーワード追加');
$I->createNewKeyword('keyword_survey_test_4');
$I->logout();
$arr = array(
            '0'=>'is_manage_genre',
            '1'=>'is_manage_keyword',
            '2'=>'is_manage_account',
            '3'=>'is_manage_notice',
            '4'=>'is_manage_feature',
            '5'=>'is_manage_pickup',
            '6'=>'is_manage_attention',
            '7'=>'is_manage_agree',
            '8'=>'is_manage_input_comment',
            '9'=>'is_manage_column',
            '10'=>'is_manage_summary',
            );
//create account dont have permission survey and dont have manage register
$I->createAccountTrue($arr, 'account_test_1');
//create account have permission survey and dont have manage register
$I->createAccountTrue($arr, 'account_test_2', null, 1);
// create account dont have permission survey and have manage register
$I->createAccountTrue($arr, 'account_test_3', 1, null);
// create account  have permission survey and have manage register
$I->createAccountTrue($arr, 'account_test_4', 1, 1);
//check see button depend on permission account
$I->checkSurveySeeButton('account_test_2', 1);
$I->checkSurveySeeButton('account_test_1', null);
$I->createSurveyPermissionSurvey('account_test_4', 'title_test_4', SURVEY_SAVE_DRAFT, W_FRM_SURVEY_SAVE_DRAFT, SURVEY_SAVE_DRAFT_TEXT);
$I->createSurveyPermissionSurvey('account_test_1', 'title_test_1', SURVEY_SAVE_DRAFT, W_FRM_SURVEY_SAVE_DRAFT, SURVEY_SAVE_DRAFT_TEXT);
$I->dontSeeSurveyLinkPermission('account_test_1', 'title_test_4', 'title_test_1');
$I->seeSurveyLinkPermission('account_test_4', 'title_test_1', 'title_test_4');
