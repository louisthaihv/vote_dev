<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAttributeValues extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("attribute_values", function ($table) {
            $table->increments("id");
            $table->integer("attribute_id");
            $table->string("attribute_value_name", 60);
            $table->integer("sort_order");
            $table->string("attribute_value_directory", 200);
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("attribute_values");
    }
}
