@if (isset($questions) && $questions->count() > 0)
    @foreach($questions as $question)
        @include('layouts.elements.displays.list_box')
    @endforeach
    @if ((isset($questions->isPaginate) && $questions->currentPage === $questions->lastPage) ||
        (!isset($questions->isPaginate) && $questions->getCurrentPage() === $questions->getLastPage()))
<li class="last" style="display:none;"></li>
    @endif
@elseif ($morePager == 1)
    @if (isset($noList) && $noList)
<li><div class="entryEmptyMsg">みんなの声をお使いいただきありがとうございます！<br />
        ただいま回答できるアンケートはありません。<br />
        新しいアンケートを準備中ですので、配信をお待ちください。<br />
        追加したアンケートは公式twitterやfacebookでもつぶやきます。<br />
        是非フォローをお願いします！<br />
        <p class="socialLink"><a href="https://twitter.com/_minnanokoe_/"><img src="/common/front/images/sp/btn_twitter.png" alt="twitter"></a> <a href="https://www.facebook.com/minnanokoe.vote"><img src="/common/front/images/sp/btn_facebook.png" alt="facebook"></a></p>
    </div></li>
    @else
<li class="noResults">アンケートはありません。</li>
    @endif
@endif
