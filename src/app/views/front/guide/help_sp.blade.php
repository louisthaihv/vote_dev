@extends('layouts.front_in')

@section('metaTag')
    <title>みんなの声操作説明 | みんなの声</title>
    <meta name="description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！">
    <meta property="og:title" content="みんなの声操作説明 | みんなの声" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
    <meta name="twitter:title" content="みんなの声操作説明 | みんなの声" />
    <meta name="twitter:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
@stop

@section('content')
    <div class="tcenter l-margin l-margint"><img src="/common/front/images/sp/help_tit.png" alt="もっと便利になりました" style="width:230px;" /></div>

    <section class="l-margin">
        <h2 class="guideTit m-margin"><span>みんなの声とは</span></h2>
        <p class="lrMargin m-margin">「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート。</p>
        <div class="helpImageArea m-margin"><img src="/common/front/images/sp/help_image1.png" alt="" /></div>
        <p class="lrMargin m-margin"><span class="orangeFont">「へぇ～」「マジ！？」</span>と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします。</p>

        <p class="tcenter  m-margin"><a href="{{ route('home.top') }}" class="btnTypeHelp">みんなの声 サイトへ</a></p>
    </section>
    <section class="l-margin">
        <h2 class="guideTit m-margin"><span>ここが楽しい！！</span></h2>
        <h3 class="guideSubTit m-margin"><span>アンケート“検索”ができる！</span></h3>
        <p class="lrMargin m-margin">生活や人付き合いなどのジャンルから関連するアンケートを探したり、気になるワードから知りたいアンケートが検索できます。</p>
@if(false)        <div class="helpImageArea m-margin"><img src="/common/front/images/sp/help_image2.png" alt="" /></div>@endif
        <h3 class="guideSubTit m-margin"><span>みんなで“評価”できる！</span></h3>
        <p class="lrMargin m-margin">おもしろいと思ったアンケート結果を直観的に評価。皆さんの評価がランキングに反映します。</p>
        <div class="helpImageArea m-margin"><img src="/common/front/images/sp/help_image3.png" alt="" /></div>
        <h3 class="guideSubTit m-margin"><span>マイページで“記録”できる！</span></h3>
        <p class="lrMargin m-margin">自分が投票したアンケート結果を見ることができます。あの時投票したアンケートの最終結果が気になる・・・そんな時にご利用ください。</p>
        <div class="helpImageArea m-margin"><img src="/common/front/images/sp/help_image4.png" alt="" /></div>
        <p class="tcenter m-margin"><a href="{{ route('list.mypage') }}" class="btnTypeHelp">マイページを見る</a></p>
        <h3 class="guideSubTit m-margin"><span>プロフィール登録でもっと“詳しく”！</span></h3>
        <p class="lrMargin m-margin">年代、性別、出身県を登録すると、プロフィール限定のアンケートに答えられたり、項目別でアンケート結果に反映されます。</p>
        <div class="helpImageArea m-margin"><img src="/common/front/images/sp/help_image5.png" alt="" /></div>
        <p class="tcenter m-margin"><a href="{{ route('profile.edit') }}" class="btnTypeHelp">プロフィール設定する</a></p>

        <p class="lrMargin m-margin l-margint" style="border-top:1px dotted #f39700;padding-top:10px;">おもしろいなと思ったアンケートや投票結果はSNSで即共有! みんなに伝えて話題の中心に!!</p>
        <div class="helpImageArea m-margin"><img src="/common/front/images/sp/help_image6.png" alt="" /></div>
        <p class="lrMargin m-margin">あなたのアクションがこのサイトをもっと面白く便利にします！</p>
        <p class="tcenter orangeFont m-margin">さっそく「みんなの声」を体験してみよう！</p>
        <p class="tcenter m-margin"><a href="{{ route('home.top') }}" class="btnTypeHelp">みんなの声 サイトへ</a></p>
    </section>

@endsection
