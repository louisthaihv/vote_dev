<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('users', function($table) {
            $table->dropColumn('birthday');
        });

        Schema::table('users', function($table) {
            $table->datetime('birthday')->nullable()->after('uid');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('users', function($table) {
            $table->dropColumn('birthday');
        });

        Schema::table('users', function($table) {
            $table->datetime('birthday')->after('uid');
        });
	}

}
