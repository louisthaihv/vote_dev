<?php
namespace AcceptanceTester;

class FooterSteps extends \AcceptanceTester\CommonSteps
{
	public function checkFPFooter($links,$text)
	{
		$I = $this;
		$I->amOnPage('/');
		$I->wait(1);
		$this->checkFooterLink($links);
		$this->checkFooterEndText($text);
	}

	public function checkFooterLink($links)
	{
		$I = $this;
		foreach ($links as $link) {
			$I->checkLinkCommon($link);
		}
	}

	public function checkFooterEndText($texts)
	{
		$I = $this;
		foreach ($texts as $text) {
			$I->see($text);
		}
	}

}