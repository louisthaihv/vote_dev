<?php

namespace docomo\Models\Ratings;

use docomo\Models\Codes\EvaluationType;

class VoteRatingTotal extends \BaseArdentModel
{
    protected $primary = ['question_id', 'type'];
    /*
     * Model relationships
     */
    function question()
    {

        return $this->belongsTo('docomo\Models\Questions\Question');
    }

    public static function scopeFindPrimary($query, $question_id, $type)
    {

        return $query->whereQuestionId($question_id)->whereType($type);
    }

    /**
     * get type image path from constants
     * @return type
     */
    public function getImagePath()
    {

        return EvaluationType::$sp_images[$this->type];
    }

    /**
     * get Name from constants
     * @return type
     */
    public function getName()
    {

        return EvaluationType::$label[$this->type];
    }

    public static function createAllRecord($question_id)
    {
        foreach (range(SUMMARY_TYPE_EVALUATION_GATTEN, SUMMARY_TYPE_EVALUATION_MEMO) as $type)
        {
            $total = VoteRatingTotal::whereQuestionId($question_id)->whereType($type)->first();
            if (is_null($total))
            {
                VoteRatingTotal::firstOrCreate([
                    'question_id' =>$question_id,
                    'type' => $type,
                    'rating_count' => 0,
                    'created_at' => new \DateTime(),
                    'updated_at' => new \DateTime()
                ]);
            }
        }
    }

    public static function incrementCount($question_id, $type)
    {

        $update = \DB::update('UPDATE vote_rating_totals SET rating_count = rating_count + 1, updated_at = NOW() WHERE question_id = ? AND type = ?',
            [$question_id, $type]);

        if ($update === 0) {
            $total = new VoteRatingTotal();
            $total->question_id = $question_id;
            $total->type = $type;
            $total->rating_count = 1;
            $total->save();
        }

        return VoteRatingTotal::select('rating_count')->findPrimary($question_id, $type)->first();
    }
}
