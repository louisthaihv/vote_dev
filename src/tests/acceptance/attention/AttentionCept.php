<?php
$I = new AcceptanceTester($scenario);
$I->amOnPage('/manage');
//check validation attention
// $I->checkValidAttention();
$I->loginWithDetail(['admin', '123456']);
$I->click('キーワード一覧・登録・修正・削除');
$I->click('＋キーワード追加');
$I->createNewKeyword('keyword_attention_1');
$I->click('＋キーワード追加');
$I->createNewKeyword('keyword_attention_2');
$I->logout();
$I->createNewAttention('keyword_attention_1', '受付中');
//edit attention
$I->editAttention('keyword_attention_1', 1, 'keyword_attention_2');
//delete attention
$I->deleteAttention('keyword_attention_2');

