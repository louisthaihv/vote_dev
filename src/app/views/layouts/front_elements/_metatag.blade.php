<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="format-detection" content="telephone=no,address=no,email=no" />
<meta name="keywords" content="アンケート,みんなの声,人気,dメニュー,ドコモ">
<link rel="shortcut icon" href="{{ url('common/front/images/sp/favicon.ico')}}">
<link rel="apple-touch-icon-precomposed" href="{{ url('common/front/images/sp/icon.png') }}?1">
<meta property="og:url" content="{{ \Request::url() }}?utm_source=fb&utm_medium=minsh" />
<meta property="og:site_name" content="みんなの声" />
<meta property="og:image" content="{{ url('common/front/images/sp/icon_200.png') }}" />
<meta property="fb:app_id" content="249260871872761" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@_minnanokoe_" />
<meta name="twitter:image" content="{{ url('common/front/images/sp/icon_200.png') }}" />
<meta name="twitter:url" content="{{ \Request::url() }}?utm_source=tw&utm_medium=mintw" />
{{ HTML::script('common/front/js/jquery.min.js') }}
{{ HTML::script('common/front/js/jquery.voteList.js?v201503091120') }}
{{ HTML::script('common/front/js/function.js?v15020414') }}
{{ HTML::script('common/front/js/core-min.js') }}
{{ HTML::script('common/front/js/enc-base64-min.js') }}
{{ HTML::script('common/front/js/my_base64.js') }}
{{ HTML::style('common/front/css/common.css?v15020414') }}
{{ HTML::style('common/front/css/profile.css?v2') }}
{{ HTML::style('common/front/css/list.css') }}
{{ HTML::style('common/front/css/top.css') }}
{{ HTML::style('common/front/css/feature.css') }}

<!--[if lt IE 9]>
    <script src="http://html5shiv.yooglecode.com/svn/trunk/html5.js"></script>
<![endif]-->