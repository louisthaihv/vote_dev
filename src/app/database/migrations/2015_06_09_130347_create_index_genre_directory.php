<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexGenreDirectory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        \DB::statement('CREATE INDEX idx_directory_id ON genre_children (`genre_parent_id`, `genre_child_directory`, `id`)');
        \DB::statement('CREATE INDEX idx_directory_id ON genre_parents (`genre_parent_directory`, `id`)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
