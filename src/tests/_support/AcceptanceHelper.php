<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class AcceptanceHelper extends \Codeception\Module
{
    
    public function haveAccount()
    {
    }
    
    public function logIn()
    {
        $I = $this->getModule('WebDriver');
        $I->amOnPage('/manage');
        $I->fillField('login_id', 'admin');
        $I->fillField('login_password', '123456');
        $I->click('ログイン');
    }
    
    public function loginWithDetail($detail)
    {
        $username = $detail[0];
        $password = $detail[1];
        $I = $this->getModule('WebDriver');
        $I->fillField('login_id', $username);
        $I->fillField('login_password', $password);
        $I->click('ログイン');
        if (($username == "admin" || $username == "account_test_01") && $password == "123456") {
            $this->loginSuccess();
        } else {
            $this->loginFail();
        }
    }
    
    private function loginSuccess()
    {
        
        // $I = $this->getModule('WebDriver');
        // $I->see(ILOGIN_SUCCESS, '#dialog-message');
        // $I->dontSee(ILOGIN_FALSE, '#dialog-message');
        // $I->click('.ui-dialog-buttonset button');
        
        
    }
    
    private function loginFail()
    {
        $I = $this->getModule('WebDriver');
        $I->see(W_MSG_FAILED_LOGIN, '#dialog-message');
        $I->click('.ui-dialog-buttonset button');
    }
    
    public function checkHead($link = null)
    {
        $detail = ['admin', '123456'];
        $I = $this->getModule('WebDriver');
        $this->loginWithDetail($detail);
        if (isset($link)) {
            $I->click($link);
        }
        $this->verifyHead();
        $this->logout();
    }
    
    private function verifyHead()
    {
        $I = $this->getModule('WebDriver');
        $I->see('ログアウト', '.headerInfo');
        $I->see('メインメニュー');
    }
    
    public function logout()
    {
        $I = $this->getModule('WebDriver');
        $I->click('logoutBtn');
        $I->seeInPopup('ログアウトします。よろしいですか？');
        $I->wait(1);
        $I->cancelPopup();
        $I->click('logoutBtn');
        $I->seeInPopup('ログアウトします。よろしいですか？');
        $I->wait(1);
        $I->acceptPopup();
        $I->seeCurrentUrlEquals('/manage');
    }
    
    public function create($url, $add)
    {
        $I = $this->getModule('WebDriver');
        $I->click($add);
        $I->seeCurrentUrlEquals('/manage/' . $url . '');
    }
    
    //keyword
    public function checkValidKeyword()
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('keyword_name', '');
        $I->click(ADD);
        $I->seeInPopup(I_MSG_ADD_KEYWORD);
        $I->acceptPopup();
        $I->see(W_FRM_REQUIRED_KEYWORD);
        $I->seeCurrentUrlEquals('/manage/keyword/add');
    }
    public function createNewKeyword($keyword)
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('keyword_name', $keyword);
        $I->uncheckOption('input[name=is_visible]');
        $I->click(ADD);
        $I->seeInPopup(I_MSG_ADD_KEYWORD);
        $I->acceptPopup();
        $I->seeCurrentUrlEquals('/manage/keyword');
        
        // $I->see('keyword_test_1');
        
        
    }
    public function checkSameNameKeyword($keyword_name)
    {
        $I = $this->getModule('WebDriver');
        $I->click('＋キーワード追加');
        $I->fillField('keyword_name', 'keyword_test_1');
        $I->click(ADD);
        $I->seeInPopup(I_MSG_ADD_KEYWORD);
        $I->acceptPopup();
        $I->seeCurrentUrlEquals('/manage/keyword/add');
        $I->see(W_FRM_UNIQUE_KEYWORD);
        $I->fillField('keyword_name', $keyword_name);
        $I->click(ADD);
        $I->seeInPopup(I_MSG_ADD_KEYWORD);
        $I->acceptPopup();
        $I->seeCurrentUrlEquals('/manage/keyword');
    }
    
    public function checkHiddenKeyword($keyword)
    {
        $I = $this->getModule('WebDriver');
        $I->click($keyword);
        $I->fillField('keyword_name', 'keyword_test_2');
        $I->click(EDIT);
        $I->seeInPopup(I_MSG_EDIT_KEYWORD);
        $I->acceptPopup();
        $I->see(W_FRM_UNIQUE_KEYWORD);
        $I->fillField('keyword_name', $keyword);
        $I->checkOption('.//*[@id="formEdit"]/table/tbody/tr[3]/td/label/input');
        $I->click(EDIT);
        $I->seeInPopup(I_MSG_EDIT_KEYWORD);
        $I->acceptPopup();
        $I->seeCurrentUrlEquals('/manage/keyword');
        $I->dontSee($keyword);
        $I->checkOption('.//*[@id="hide_show"]');
        $I->click(SEARCH_ACCOUNT);
        $I->see($keyword);
        $I->uncheckOption('.//*[@id="hide_show"]');
        $I->click(SEARCH_ACCOUNT);
        $I->dontSee($keyword);
    }
    
    /*
        Account
    */
    public function checkValidAcount()
    {
        $I = $this->getModule('WebDriver');
        $this->loginWithDetail(['admin', '123456']);
        $I->click('アカウント一覧・登録・修正・削除');
        $I->click('＋アカウント追加');
        $I->click(ADD);
        $I->seeInPopup(I_MSG_ADD_ACCOUNT);
        $I->acceptPopup();
        $I->see(W_FRM_REQUIRED_ACCOUNT_NAME);
        $I->see(W_FRM_REQUIRED_LOGIN_ID);
        $I->see(W_FRM_REQUIRED_LOGIN_PASSWORD);
        $I->seeCurrentUrlEquals('/manage/account/add');
        $this->logout();
    }
    
    public function checkCloseAccount()
    {
        $I = $this->getModule('WebDriver');
        $this->create('account/add', '＋アカウント追加');
        $I->fillField('account_name', 'testname1');
        $I->fillField('login_id', 'testname1');
        $I->fillField('login_password', '123456');
        $I->checkOption('.//*[@id="form_account"]/table/tbody/tr[6]/td/label/input');
        $I->click(ADD);
        $I->seeInPopup(I_MSG_ADD_ACCOUNT);
        $I->acceptPopup();
        $this->logout();
        $I->seeCurrentUrlEquals('/manage');
        
        // $this->loginWithDetail(['testname1', '123456']);
        $I->fillField('login_id', 'testname1');
        $I->fillField('login_password', '123456');
        $I->click('ログイン');
        $I->see(W_MSG_FAILED_LOGIN, '#dialog-message');
        $I->click('.ui-dialog-buttonset button');
    }
    public function checkOpenAccount()
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('login_id', 'admin');
        $I->fillField('login_password', '123456');
        $I->click('ログイン');
        $I->click('アカウント一覧・登録・修正・削除');
        $I->checkOption('.//*[@id="hide_show"]');
        $I->click(SEARCH_ACCOUNT);
        $I->see(I_MSG_ACCOUNT_CLOSE);
        $I->click('testname1');
        $I->uncheckOption('.//*[@id="form_account_edit"]/table/tbody/tr[6]/td/label/input');
        $I->click(EDIT);
        $I->seeInPopup(I_MSG_EDIT_ACCOUNT);
        $I->acceptPopup();
        $this->logout();
        $I->fillField('login_id', 'testname1');
        $I->fillField('login_password', '123456');
        $I->click('ログイン');
    }
    public function checkHiddenAccount()
    {
        $I = $this->getModule('WebDriver');
        $this->loginWithDetail(['admin', '123456']);
        $I->click('アカウント一覧・登録・修正・削除');
        $I->click('＋アカウント追加');
        $I->fillField('account_name', 'account_create_test_name1');
        $I->fillField('login_id', 'account_create_test_name1');
        $I->fillField('login_password', '123456');
        $I->click('自動生成');
        $I->dontSee('123456');
        $I->fillField('login_password', '123456');
        $I->selectOption('allow_category_parent', 'すべて');
        $I->click(ADD);
        $I->seeInPopup(I_MSG_ADD_ACCOUNT);
        $I->acceptPopup();
        $I->seeCurrentUrlEquals('/manage/account');
        $this->logout();
        $I->fillField('login_id', 'account_create_test_name1');
        $I->fillField('login_password', '123456');
        $I->click('ログイン');
        $I->dontSee('TOPページ特別枠管理');
        $I->dontSee('TOPページ注目キーワード管理');
        $I->dontSee('ジャンル管理');
        $I->dontSee('キーワード管理');
        $I->dontSee('アカウント管理');
        $I->dontSee('お知らせ管理');
        $this->logout();
        $this->loginWithDetail(['admin', '123456']);
        $I->click('アカウント一覧・登録・修正・削除');
        $I->click('account_create_test_name1');
        $I->checkOption('.//*[@id="form_account_edit"]/table/tbody/tr[6]/td/label/input');
        $I->click(EDIT);
        $I->seeInPopup(I_MSG_EDIT_ACCOUNT);
        $I->acceptPopup();
        $this->logout();
        $this->loginWithDetail(['admin', '123456']);
        $I->click('アカウント一覧・登録・修正・削除');
        $I->dontSee('account_create_test_name1');
        $I->dontSee(I_MSG_ACCOUNT_CLOSE);
        $I->checkOption('.//*[@id="hide_show"]');
        $I->click(SEARCH_ACCOUNT);
        $I->see('account_create_test_name1');
        $I->see(I_MSG_ACCOUNT_CLOSE);
        $this->logout();
    }
    public function createAccount($account_name, $account_id)
    {
        $I = $this->getModule('WebDriver');
        $this->create('account/add', '＋アカウント追加');
        $I->fillField('account_name', $account_name);
        $I->fillField('login_id', $account_id);
        $I->fillField('login_password', '123456');
        $I->click(ADD);
        $I->seeInPopup(I_MSG_ADD_ACCOUNT);
        $I->acceptPopup();
        $I->seeCurrentUrlEquals('/manage/account');
        $this->logout();
    }
    public function choosePermission($account_name, $checkbox_name)
    {
        $I = $this->getModule('WebDriver');
        $this->loginWithDetail(['admin', '123456']);
        $I->click('アカウント一覧・登録・修正・削除');
        
        //tick checkbox
        $I->click($account_name);
        $I->checkOption($checkbox_name);
        $I->click(EDIT);
        $I->seeInPopup(I_MSG_EDIT_ACCOUNT);
        $I->acceptPopup();
        
        // $I->seeCurrentUrlEquals('/manage/account');
        $this->logout();
    }
    public function verifyPermission($account_id, $permission_name, $account_name, $checkbox_name)
    {
        
        //dont see name at dashboard and see checkbox is checked
        $I = $this->getModule('WebDriver');
        $this->loginWithDetail([$account_id, '123456']);
        $I->dontSee($permission_name);
        $this->logout();
        $this->loginWithDetail(['admin', '123456']);
        $I->click('アカウント一覧・登録・修正・削除');
        $I->click($account_name);
        $I->seeCheckboxIsChecked($checkbox_name);
        $I->uncheckOption($checkbox_name);
        $I->click(EDIT);
        $I->seeInPopup(I_MSG_EDIT_ACCOUNT);
        $I->acceptPopup();
        $this->logout();
    }
    
    public function checkAccountIdUnique($account_name)
    {
        $I = $this->getModule('WebDriver');
        $this->loginWithDetail(['admin', '123456']);
        $I->click('アカウント一覧・登録・修正・削除');
        $I->click('＋アカウント追加');
        $I->fillField('account_name', 'test');
        $I->fillField('login_id', 'admin');
        $I->fillField('login_password', '123456');
        $I->click(ADD);
        $I->seeInPopup(I_MSG_ADD_ACCOUNT);
        $I->acceptPopup();
        $I->see(W_FRM_UNIQUE_LOGIN_ID);
        $I->fillField('account_name', $account_name);
        $I->fillField('login_id', $account_name);
        $I->fillField('login_password', '123456');
        $I->click(ADD);
        $I->seeInPopup(I_MSG_ADD_ACCOUNT);
        $I->acceptPopup();
        $I->click($account_name);
        $I->fillField('login_id', 'admin');
        $I->click(EDIT);
        $I->seeInPopup(I_MSG_EDIT_ACCOUNT);
        $I->acceptPopup();
        $I->see(W_FRM_UNIQUE_LOGIN_ID);
        $this->logout();
    }
    
    public function checkGenreCreate()
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('category_parent_name', '');
        $I->fillField('category_parent_directory', '');
        $this->checkAcceptPopupCommon(ADD, I_MSG_ADD_CAT_PARENTS);
        $I->see(W_FRM_CAT_PARENTS_NAME_REQUIRED);
        $I->see(W_FRM_CAT_PARENTS_DIR_REQUIRED);
        $this->logout();
    }
    public function createNewGenre($genre_name, $directory)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon('ジャンル一覧・登録・修正・削除');
        $I->click('＋ジャンル追加');
        $I->fillField('category_parent_name', $genre_name);
        $I->fillField('category_parent_directory', $directory);
        $this->checkAcceptPopupCommon(ADD, I_MSG_ADD_CAT_PARENTS);
        $I->seeCurrentUrlEquals('/manage/genre/edit/' . $directory . '');
        $I->seeInField('category_parent_name', $genre_name);
        $I->seeInField('category_parent_directory', $directory);
        $this->logout();
    }
    
    public function createNewSmallGenre($category_child_name, $directory_category_child_name, $child_sort_order, $genre_name, $directory)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon('ジャンル一覧・登録・修正・削除');
        $I->click($genre_name);
        $I->seeCurrentUrlEquals('/manage/genre/edit/' . $directory . '');
        $I->fillField('category_child_name', $category_child_name);
        $I->fillField('category_child_directory', $directory_category_child_name);
        $I->fillField('child_sort_order', $child_sort_order);
        $this->checkAcceptPopupCommon('追加', I_MSG_ADD_CAT_CHILDS);
        $this->logout();
    }
    public function checkSortSmallGenre($genre_name, $directory)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon('ジャンル一覧・登録・修正・削除');
        $I->click($genre_name);
        $I->seeInField('.//*[@id="editChildGenre"]/table/tbody/tr[2]/td[1]/input', 'name_1');
        $I->seeInField('.//*[@id="editChildGenre"]/table/tbody/tr[3]/td[1]/input', 'name_2');
        $I->fillField('.//*[@id="editChildGenre"]/table/tbody/tr[3]/td[4]/input', '5');
        $this->checkAcceptPopupCommon(EDIT, I_MSG_EDIT_CAT_CHILDS);
        $this->logout();
        $this->loginClickCommon('ジャンル一覧・登録・修正・削除');
        $I->click($genre_name);
        $I->seeInField('.//*[@id="editChildGenre"]/table/tbody/tr[2]/td[1]/input', 'name_2');
        $I->seeInField('.//*[@id="editChildGenre"]/table/tbody/tr[3]/td[1]/input', 'name_1');
        $this->logout();
    }
    public function checkHiddenSmallGenre($genre_name)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon('ジャンル一覧・登録・修正・削除');
        $I->click($genre_name);
        $I->checkOption('.//*[@id="editChildGenre"]/table/tbody/tr[2]/td[5]/input');
        $this->checkAcceptPopupCommon(EDIT, I_MSG_EDIT_CAT_CHILDS);
        $I->dontSee('name_2');
        $this->logout();
        $this->loginClickCommon('ジャンル一覧・登録・修正・削除');
        $I->click($genre_name);
        $I->checkOption('input[name=hide_show]');
        $I->click('再検索');
        $I->seeInField('.//*[@id="editChildGenre"]/table/tbody/tr[2]/td[1]/input', 'name_2');
        $this->logout();
    }
    
    public function checkHidden($directory, $genre_name)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon('ジャンル一覧・登録・修正・削除');
        $I->click($genre_name);
        $I->checkOption('.//*[@id="editParentGenre"]/table/tbody/tr[4]/td/label/input');
        $this->checkAcceptPopupCommon('大ジャンルを更新', I_MSG_EDIT_CAT_PARENTS);
        $I->seeCurrentUrlEquals('/manage/genre/edit/' . $directory . '');
        $this->logout();
        $this->loginClickCommon('ジャンル一覧・登録・修正・削除');
        $I->dontSee('genre_test_1');
        $I->dontSee(I_MSG_CAT_PARENTS_HIDDEN);
        $I->checkOption('input[name=hide_show]');
        $I->click(SEARCH_ACCOUNT);
        $I->see('genre_test_1');
        $I->see(I_MSG_CAT_PARENTS_HIDDEN);
        $this->logout();
    }
    public function checkValidDirectoryGenre($genre_name)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon('ジャンル一覧・登録・修正・削除');
        $I->click($genre_name);
        $I->fillField('category_child_name', '');
        $I->fillField('category_child_directory', '');
        $I->fillField('child_sort_order', '');
        $I->click('追加');
        $I->seeInPopup(I_MSG_ADD_CAT_CHILDS);
        $I->acceptPopup();
        $I->see(W_FRM_CAT_CHILD_NAME_REQUIRED);
        $I->see(W_FRM_CAT_CHILD_DIR_REQUIRED);
        $this->logout();
    }
    
    public function checkSameNameDirectorGenre($genre_name, $category_child_name, $directory_category_child_name, $child_sort_order, $genre_directory)
    {
        $I = $this->getModule('WebDriver');
        $this->loginWithDetail(['admin', '123456']);
        $I->click('ジャンル一覧・登録・修正・削除');
        $I->click($genre_name);
        $I->fillField('category_child_name', $category_child_name);
        $I->fillField('category_child_directory', $directory_category_child_name);
        $I->fillField('child_sort_order', $child_sort_order);
        $I->click('追加');
        $I->seeInPopup(I_MSG_ADD_CAT_CHILDS);
        $I->acceptPopup();
        $I->see(W_FRM_CAT_CHILD_DIR_UNIQUE);
        $I->seeCurrentUrlEquals('/manage/genre/edit/' . $genre_directory . '');
        $this->logout();
    }
    
    public function checkValidSortNumber($genre_name)
    {
        $I = $this->getModule('WebDriver');
        $this->loginWithDetail(['admin', '123456']);
        $I->click('ジャンル一覧・登録・修正・削除');
        $I->click($genre_name);
        $I->fillField('category_child_name', 'name4');
        $I->fillField('category_child_directory', 'dir_4');
        $I->fillField('child_sort_order', 'abc');
        $I->click('追加');
        $I->seeInPopup(I_MSG_ADD_CAT_CHILDS);
        $I->acceptPopup();
        $I->see(W_FRM_CAT_CHILD_DIR_INT);
        $this->logout();
        
        // $I->seeCurrentUrlEquals('/manage');
        
        
    }
    public function checkValidGenreSortNumber()
    {
        $I = $this->getModule('WebDriver');
        $this->loginWithDetail(['admin', '123456']);
        $I->click('ジャンル一覧・登録・修正・削除');
        $I->fillField('.//*[@id="genre_form"]/table/tbody/tr[2]/td[5]/input', 'abc');
        $I->click('表示順を更新');
        $I->seeInPopup(I_MSG_EDIT_CAT_PARENT_ORDER);
        $I->acceptPopup();
        $I->see(W_FRM_CAT_PARENTS_ORDER_INT);
        $I->fillField('.//*[@id="genre_form"]/table/tbody/tr[2]/td[5]/input', '');
        $I->click('表示順を更新');
        $I->seeInPopup(I_MSG_EDIT_CAT_PARENT_ORDER);
        $I->acceptPopup();
        $I->see(W_FRM_CAT_PARENTS_ORDER_REQUIRED);
        $this->logout();
    }
    public function checkValidSmallGenreSortNumber($genre_name)
    {
        $I = $this->getModule('WebDriver');
        $this->loginWithDetail(['admin', '123456']);
        $I->click('ジャンル一覧・登録・修正・削除');
        $I->click($genre_name);
        $I->checkOption('input[name=hide_show]');
        $I->click('再検索');
        $I->fillField('.//*[@id="editChildGenre"]/table/tbody/tr[2]/td[4]/input', '');
        $I->click(EDIT);
        $I->seeInPopup(I_MSG_EDIT_CAT_CHILDS);
        $I->acceptPopup();
        $I->see(W_FRM_CAT_CHILD_ORDER_REQUIRED);
        $I->fillField('.//*[@id="editChildGenre"]/table/tbody/tr[2]/td[4]/input', 'abc');
        $I->click(EDIT);
        $I->seeInPopup(I_MSG_EDIT_CAT_CHILDS);
        $I->acceptPopup();
        $I->see(W_FRM_CAT_CHILD_DIR_INT);
        $this->logout();
    }
    public function checkSortGenre()
    {
        $I = $this->getModule('WebDriver');
        $this->loginWithDetail(['admin', '123456']);
        $I->click('ジャンル一覧・登録・修正・削除');
        $I->see('genre_test_1', './/*[@id="genre_form"]/table/tbody/tr[4]/td[2]');
        $I->fillField('.//*[@id="genre_form"]/table/tbody/tr[4]/td[5]/input', 5);
        $I->click('表示順を更新');
        $I->seeInPopup(I_MSG_EDIT_CAT_PARENT_ORDER);
        $I->acceptPopup();
        $I->see('genre_test_1', './/*[@id="genre_form"]/table/tbody/tr[2]/td[2]');
        $this->logout();
    }
    public function checkEditSmalLGenreDirectoryUnique($genre_name)
    {
        $I = $this->getModule('WebDriver');
        $this->loginWithDetail(['admin', '123456']);
        $I->click('ジャンル一覧・登録・修正・削除');
        $I->click($genre_name);
    }
    
    /**
     * Manage Info Screen
     */
    
    /**
     * [checkAddInfo description]
     * @return message
     */
    public function checkAddInfo()
    {
        $I = $this->getModule('WebDriver');
        $this->loginWithDetail(['admin', '123456']);
        $I->click(INFO);
        $I->click(INFO_ADD);
        $I->click(INFO_CREATE);
        $I->see(W_FRM_TITLE_REQUIRED);
        $I->see(W_FRM_DESC_REQUIRED);
        $I->see(W_FRM_DETAIL_REQUIRED);
    }
    
    /**
     * [checkValidDateInfoBeforeNow description]
     * @param  [now]
     * @param  [title]
     * @param  [des]
     * @param  [detail]
     * @param  [date_from]
     * @param  [date_to]
     * @return [message]
     */
    public function checkValidDateInfoBeforeNow($now, $title, $des, $detail, $date_from, $date_to)
    {
        $I = $this->getModule('WebDriver');
        $this->createNewInfoCommon($title, $des, $detail, $date_from, $date_to);
        $I->see(W_FRM_START_DATE_NO_BEFORE_NOW);
    }
    
    /**
     * [checkValidDateInfoBeforeDateEnd description]
     * @param  [title]
     * @param  [des]
     * @param  [detail]
     * @param  [date_from]
     * @param  [date_to]
     * @return [message]
     */
    public function checkValidDateInfoBeforeDateEnd($title, $des, $detail, $date_from, $date_to)
    {
        $I = $this->getModule('WebDriver');
        $this->createNewInfoCommon($title, $des, $detail, $date_from, $date_to);
        $I->see(W_FRM_END_DATE_NO_BEFORE_START_DATE);
    }
    
    /**
     * [createNewInfoCommon description]
     * @param  [title]
     * @param  [des]
     * @param  [detail]
     * @param  [date_from]
     * @param  [date_to]
     * @return [type]
     */
    public function createNewInfoCommon($title, $des, $detail, $date_from, $date_to)
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('input[name=title]', $title);
        $I->fillField('input[name=description]', $des);
        $I->fillField('detail', $detail);
        
        $I->click('input[name=visible_date_from]');
        $I->click($date_from);
        $I->selectOption('visible_hour_from', 11);
        $I->selectOption('visible_minute_from', 11);
        
        $I->click('input[name=visible_date_to]');
        $I->click($date_to);
        $I->selectOption('visible_hour_to', 11);
        $I->selectOption('visible_minute_to', 11);
        $I->click(INFO_CREATE);
    }
    
    /**
     * [createNewInfo description]
     * @param  [title]
     * @param  [des]
     * @param  [detail]
     * @param  [date_from]
     * @param  [date_to]
     * @return [new info]
     */
    public function createNewInfo($title, $des, $detail, $date_from, $date_to)
    {
        $I = $this->getModule('WebDriver');
        $this->createNewInfoCommon($title, $des, $detail, $date_from, $date_to);
        $I->seeCurrentUrlEquals('/manage/info/confirm');
        $I->see('お知らせ確認');
        $I->see($title);+
        $I->see($detail);
        $I->click('編集しなおす');
        $I->seeInField('input[name=title]', $title);
        $I->seeInField('input[name=description]', $des);
        $I->seeInField('detail', $detail);
        $I->seeInField('input[name=visible_date_from]', '2014-12-16');
        $I->seeInField('input[name=visible_date_to]', '2014-12-31');
        $I->seeCurrentUrlEquals('/manage/info/add');
        $I->click(INFO_CREATE);
        $this->checkAcceptPopupCommon(ADD, I_MSG_CONFIRM_ACTION_INFO);
        $I->see($title);
        $this->logout();
    }
    public function checkEditInfo($title, $detail)
    {
        $I = $this->getModule('WebDriver');
        $this->loginWithDetail(['admin', '123456']);
        $I->click(INFO);
        $I->click($title);
        $I->fillField('input[name=title]', 'info_test');
        $I->seeInField('detail', $detail);
        $I->click('確認');
        $I->click('編集しなおす');
        $I->seeInField('input[name=title]', 'info_test');
        $I->seeInField('input[name=visible_date_from]', '2014-12-16');
        $I->seeInField('input[name=visible_date_to]', '2014-12-31');
        $I->seeInField('detail', $detail);
        $I->seeInField('input[name=description]', 'info_overview_1');
        $I->fillField('input[name=title]', 'info_test');
        $I->click('確認');
        $this->checkAcceptPopupCommon(ADD, I_MSG_CONFIRM_ACTION_INFO);
        $I->dontSee('info_1');
    }
    public function checkDeleteInfo()
    {
        $I = $this->getModule('WebDriver');
        $I->click(BTN_DELETE_SURVEY);
        $I->seeInPopup(I_MSG_DELETE_INFO);
        $I->cancelPopup();
        $I->see('info_test');
        $this->checkAcceptPopupCommon(BTN_DELETE_SURVEY, I_MSG_DELETE_INFO);
        $I->dontSee('info_test');
        $this->logout();
    }
    public function tagCommon($title, $detail)
    {
        $I = $this->getModule('WebDriver');
        $this->loginWithDetail(['admin', '123456']);
        $I->click(INFO);
        $I->click(INFO_ADD);
        $des = 'info_overview_1';
        $date_from = './/*[@id="ui-datepicker-div"]/table/tbody/tr[3]/td[3]/a';
        $date_to = './/*[@id="ui-datepicker-div"]/table/tbody/tr[5]/td[4]/a';
        $this->createNewInfoCommon($title, $des, $detail, $date_from, $date_to);
    }
    
    public function createTag($title, $detail)
    {
        $I = $this->getModule('WebDriver');
        $this->tagCommon($title, $detail);
        $I->seeCurrentUrlEquals('/manage/info/confirm');
        $I->click(ADD);
        $I->seeInPopup(I_MSG_CONFIRM_ACTION_INFO);
        $I->acceptPopup();
        $this->logout();
    }
    
    public function checkTag($title, $detail)
    {
        $I = $this->getModule('WebDriver');
        $this->tagCommon($title, $detail);
        $I->see(W_FRM_DETAIL_ACCEPT_A_SPAN_TAG);
        $this->logout();
    }
    public function createTagSpan($title, $detail)
    {
        $I = $this->getModule('WebDriver');
        $this->createTag($title, $detail);
    }
    public function createTagA($title, $detail)
    {
        $I = $this->getModule('WebDriver');
        $this->createTag($title, $detail);
    }
    public function editTag($title, $detail_errors, $detail)
    {
        $I = $this->getModule('WebDriver');
        $I = $this->getModule('WebDriver');
        $this->loginWithDetail(['admin', '123456']);
        $I->click('お知らせ一覧・登録・修正・削除');
        $I->click($title);
        $I->fillField('detail', $detail_errors);
        $I->click('確認');
        $I->see(W_FRM_DETAIL_ACCEPT_A_SPAN_TAG);
        $I->fillField('detail', $detail);
        $I->click('確認');
        $I->click(ADD);
        $I->seeInPopup(I_MSG_CONFIRM_ACTION_INFO);
        $I->acceptPopup();
        $this->logout();
    }
    
    /**
     * Manage Attention Screen
     */
    
    public function checkValidAttention()
    {
        $I = $this->getModule('WebDriver');
        $this->loginWithDetail(['admin', '123456']);
        $I->click(ATTENTION_ADD);
        $I->click('登録');
        $I->seeInPopup(I_MSG_ADD_ATTENTION);
        $I->acceptPopup();
        $I->see(W_FRM_REQUIRED_KEYWORD_ID);
        $I->see(W_FRM_REQUIRED_DATE_FROM);
        $I->see(W_FRM_REQUIRED_DATE_TO);
        $this->logout();
    }
    public function createNewAttention($keyword_attention, $visible_tab)
    {
        $I = $this->getModule('WebDriver');
        $this->loginWithDetail(['admin', '123456']);
        $I->click(ATTENTION_ADD);
        $I->click(ATTENTION_ADD_NEW);
        $I->switchToWindow('keywordwindow');
        $I->click($keyword_attention);
        $I->switchToWindow();
        $this->checkDateAttentionAdd('登録');
        $this->createAttentionDateCommon('登録');
        $I->seeInPopup(I_MSG_ADD_ATTENTION);
        $I->acceptPopup();
        $I->seeCurrentUrlEquals('/manage/attention');
        $I->see($visible_tab);
        $this->logout();
    }
    public function editAttention($keyword_attention, $id, $keyword_attention_edit)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(ATTENTION_LIST);
        $I->see($keyword_attention);
        $I->click($keyword_attention);
        $I->click(ATTENTION_ADD_NEW);
        $I->switchToWindow('keywordwindow');
        $I->click($keyword_attention_edit);
        $I->switchToWindow();
        $this->checkDateAttentionEdit('更新');
        $this->createAttentionDateCommon('更新');
        $I->seeInPopup(I_MSG_EDIT_ATTENTION);
        $I->acceptPopup();
        $I->see($keyword_attention_edit);
        $I->dontSee($keyword_attention);
        $I->seeCurrentUrlEquals('/manage/attention');
        $this->logout();
    }
    
    public function deleteAttention($keyword_attention)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(ATTENTION_LIST);
        $I->see($keyword_attention);
        $this->checkAcceptPopupCommon(BTN_DELETE_SURVEY, I_MSG_DELETE_ATTENTION);
        $I->dontSee($keyword_attention);
        $this->logout();
    }
    
    public function checkDateAttentionBeforeNow($submit, $date_from, $date_to)
    {
        $I = $this->getModule('WebDriver');
        $this->checkDateAttentionBeforeCommon($submit, $date_from, $date_to);
        $this->checkPopupCommon(I_MSG_ADD_ATTENTION, W_FRM_AFTER_DATE_FROM);
    }
    
    public function checkDateAttentionBeforeDateEnd($submit, $date_from, $date_to)
    {
        $I = $this->getModule('WebDriver');
        $this->checkDateAttentionBeforeDateEndCommon($submit, $date_from, $date_to);
        $this->checkPopupCommon(I_MSG_ADD_ATTENTION, W_FRM_AFTER_DATE_TO);
    }
    
    public function checkDateAttentionAdd($submit)
    {
        $I = $this->getModule('WebDriver');
        $this->checkDateAttentionBeforeNow($submit, 'date_from', 'date_to');
        $this->checkDateAttentionBeforeDateEnd($submit, 'date_from', 'date_to');
    }
    public function checkDateAttentionEdit($submit)
    {
        $I = $this->getModule('WebDriver');
        $this->checkDateAttentionBeforeDateEndEdit($submit, 'date_from', 'date_to');
        $this->checkDateAttentionBeforeNowEdit($submit, 'date_from', 'date_to');
    }
    public function checkDateAttentionBeforeDateEndEdit($submit, $date_from, $date_to)
    {
        $I = $this->getModule('WebDriver');
        $this->checkDateAttentionBeforeDateEndCommon($submit);
        $this->checkPopupCommon(I_MSG_EDIT_ATTENTION, W_FRM_AFTER_DATE_TO);
    }
    public function checkDateAttentionBeforeNowEdit($submit)
    {
        $I = $this->getModule('WebDriver');
        $this->checkDateAttentionBeforeCommon($submit, $date_from, $date_to);
        $this->checkPopupCommon(I_MSG_EDIT_ATTENTION, W_FRM_AFTER_DATE_FROM);
    }
    public function checkDateAttentionBeforeCommon($submit, $date_from, $date_to)
    {
        $I = $this->getModule('WebDriver');
        $this->dateBeforeNowCommon($date_from, $date_to);
        $I->click($submit);
    }
    public function checkDateAttentionBeforeDateEndCommon($submit, $date_from, $date_to)
    {
        $I = $this->getModule('WebDriver');
        $I->click('input[name=' . $date_from . ']');
        $I->click('.//*[@id="ui-datepicker-div"]/table/tbody/tr[3]/td[3]/a');
        $I->click('input[name=' . $date_to . ']');
        $I->click('.//*[@id="ui-datepicker-div"]/table/tbody/tr[2]/td[1]/a');
        $I->click($submit);
    }
    public function checkPopupCommon($message_popup, $message_see)
    {
        $I = $this->getModule('WebDriver');
        $I->seeInPopup($message_popup);
        $I->acceptPopup();
        $I->see($message_see);
    }
    public function checkAcceptPopupCommon($submit, $message_accept)
    {
        $I = $this->getModule('WebDriver');
        $I->click($submit);
        $I->seeInPopup($message_accept);
        $I->acceptPopup();
    }
    
    public function createAttentionDateCommon($submit)
    {
        $I = $this->getModule('WebDriver');
        $I->click('input[name=date_from]');
        $I->click('.//*[@id="ui-datepicker-div"]/table/tbody/tr[3]/td[3]/a');
        $I->click('input[name=date_to]');
        $I->click('.//*[@id="ui-datepicker-div"]/table/tbody/tr[5]/td[4]/a');
        $I->click($submit);
    }
    
    public function loginClickCommon($click_name)
    {
        $I = $this->getModule('WebDriver');
        $this->loginWithDetail(['admin', '123456']);
        $I->click($click_name);
    }
    
    public function checkValidNewSurveyRequire($submit, $message_accept)
    {
        $I = $this->getModule('WebDriver');
        $this->checkAcceptPopupCommon($submit, $message_accept);
        $I->see(W_FRM_SURVEY_TITLE_REQUIRED);
        $I->see(W_FRM_SURVEY_DES_REQUIRED);
        $I->see(W_FRM_SURVEY_TYPE_REQUIRED);
        $I->see(W_FRM_SURVEY_CHOICE_REQUIRED);
    }
    
    /**
     * Survey
     */
    public function checkSurveyBoxCommon($is_visible_other, $shuffle_choice)
    {
        $I = $this->getModule('WebDriver');
        $I->checkOption('input[name=' . $is_visible_other . ']');
        $I->checkOption('input[name=' . $shuffle_choice . ']');
    }
    public function checkVisibleOther($submit, $message_accept)
    {
        $I = $this->getModule('WebDriver');
        
        $I->checkOption('input[name=is_visible_other]');
        $I->fillField('input[name=is_visible_other]', '');
        $this->checkAcceptPopupCommon($submit, $message_accept);
        $I->see(W_FRM_SURVEY_OTHER);
    }
    public function checkValidNewSurveyDateBeforeNow($submit, $date_from, $date_to, $message_accept)
    {
        $I = $this->getModule('WebDriver');
        $this->dateBeforeNowCommon('vote_date_from', 'vote_date_to');
        $this->checkAcceptPopupCommon($submit, $message_accept);
        $I->see(W_FRM_START_DATE_SURVEY_BEFORE_NOW);
        $this->dateToBeforeDateFrom($date_from, $date_to);
        $I->see(W_FRM_END_DATE_NO_BEFORE_START_DATE_SURVEY);
    }
    public function dateBeforeNowCommon($date_from, $date_to)
    {
        $I = $this->getModule('WebDriver');
        $I->click('input[name=' . $date_from . ']');
        $I->click('.//*[@id="ui-datepicker-div"]/table/tbody/tr[2]/td[1]/a');
        $I->click('input[name=' . $date_to . ']');
        $I->click('.//*[@id="ui-datepicker-div"]/table/tbody/tr[5]/td[4]/a');
    }
    public function dateToBeforeDateFrom($date_from, $date_to)
    {
        $I = $this->getModule('WebDriver');
        $I->click('input[name=' . $date_from . ']');
        $I->click('.//*[@id="ui-datepicker-div"]/table/tbody/tr[3]/td[3]/a');
        $I->click('input[name=' . $date_to . ']');
        $I->click('.//*[@id="ui-datepicker-div"]/table/tbody/tr[2]/td[1]/a');
    }
    public function dateTimeTrueCommon($date_from, $date_to)
    {
        $I = $this->getModule('WebDriver');
        $I->click('input[name=' . $date_from . ']');
        $I->click('.//*[@id="ui-datepicker-div"]/table/tbody/tr[3]/td[3]/a');
        $I->click('input[name=' . $date_to . ']');
        $I->click('.//*[@id="ui-datepicker-div"]/table/tbody/tr[5]/td[4]/a');
    }
    public function checkValidSurvey($genre, $genre_comment)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(LINK_TO_CREATE_SURVEY);
        $this->checkSurveyComment();
        $this->checkSurveySns();
        $this->checkSurveyAssociation();
        $this->checkSurveyNavigation();
        // $I->click(BTN_DELETE_SURVEY);
        $this->checkSurveyResultPageComment();
        $this->checkDevice();
        $this->checkSurveyBasicInfo();
        $this->checkSurveyDatetime();
        $this->checkGenreComment($genre, $genre_comment);
    }
    public function checkGenreComment($genre, $genre_comment)
    {
        $I = $this->getModule('WebDriver');
        $I->selectOption('genre', $genre);
        $I->see($genre_comment);
    }
    public function checkSurveyComment()
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('comment', 'survey_comment');
    }
    public function checkSurveySns()
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('input[name=sns_comment]', 'survey_sns');
    }
    public function checkSurveyNavigation()
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('input[name=navigation_title]', 'survey_navigation_title');
        $this->checkUrlIdentifyNavigation();
    }
    private function checkUrlIdentifyNavigation()
    {
        $I = $this->getModule('WebDriver');
        $this->dateTimeTrueCommon('vote_date_from', 'vote_date_to');
        $this->deviceTrueCommon();
        $this->surveyBasicInfoTrueCommon('北海道', 'title_survey_test_1');
        $this->surveyResultPageCommentTagTrue('Survey_Result_Page_Comment_Tag_True');
        $this->createSurveyAssociation('keyword_survey_test_1', '学校生活、職場生活');
        $this->checkSurveySns();
        $this->checkSurveyComment();
        $this->checkOnlyUrlOrIdentifyNavigation(SURVEY_SAVE_DRAFT, W_FRM_SURVEY_SAVE_DRAFT);
    }
    private function checkOnlyUrlOrIdentifyNavigation($submit, $message_accept)
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('.//*[@id="form"]/table[5]/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/input', 'D_DES_1');
        $I->fillField('.//*[@id="form"]/table[5]/tbody/tr[2]/td/table/tbody/tr[2]/td[3]/input', '');
        $this->checkAcceptPopupCommon(SURVEY_SAVE_DRAFT, W_FRM_SURVEY_SAVE_DRAFT);
        $I->see(W_FRM_SURVEY_NAVIGATION_REQUIRE);
        $I->fillField('.//*[@id="form"]/table[5]/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/input', 'D_DES_1');
        $I->fillField('.//*[@id="form"]/table[5]/tbody/tr[2]/td/table/tbody/tr[2]/td[3]/input', 'D_URL_1');
        $I->fillField('.//*[@id="form"]/table[5]/tbody/tr[3]/td/table/tbody/tr[2]/td[1]/input', '');
        $I->fillField('.//*[@id="form"]/table[5]/tbody/tr[3]/td/table/tbody/tr[2]/td[2]/input', 'I_URL_1');
        $this->checkAcceptPopupCommon(SURVEY_SAVE_DRAFT, W_FRM_SURVEY_SAVE_DRAFT);
        $I->see(W_FRM_SURVEY_NAVIGATION_REQUIRE);
        $I->fillField('.//*[@id="form"]/table[5]/tbody/tr[3]/td/table/tbody/tr[2]/td[1]/input', 'I_DES_1');
        $I->click(BTN_DELETE_SURVEY);
    }
    
    public function checkSurveyAssociation()
    {
        $I = $this->getModule('WebDriver');
        $this->checkLimitKeywordSurvey(SURVEY_ADD_KEYWORD, 'keyword_survey_test_1', 'keyword_survey_test_2', 'keyword_survey_test_3', 'keyword_survey_test_4');
        $this->checkClearKeywordSurvey('すべてクリア', 'keyword_survey_test_1');

    }
    
    private function checkLimitKeywordSurvey($keyword_add_new, $key1, $key2, $key3, $key4)
    {
        $I = $this->getModule('WebDriver');
        $I->click($keyword_add_new);
        $I->switchToWindow('keywordwindow');
        $I->click($key1);
        $I->click($key2);
        $I->click($key3);
        $I->switchToWindow();
        $I->see('keyword_survey_test_1');
        $I->see('keyword_survey_test_2');
        $I->see('keyword_survey_test_3');
        $I->dontSee('keyword_survey_test_4');
    }
    private function checkClearKeywordSurvey($clear, $key)
    {
        $I = $this->getModule('WebDriver');
        $I->click($clear);
        $I->dontSee($key);
    }
    private function createSurveyAssociation($keyword_survey_test, $genre_child)
    {
        $I = $this->getModule('WebDriver');
        $I->click(SURVEY_ADD_KEYWORD);
        $I->switchToWindow('keywordwindow');
        $I->click($keyword_survey_test);
        $I->switchToWindow();
        $I->selectOption('attribute', $genre_child);
    }
    
    public function checkSurveyResultPageComment()
    {
        $I = $this->getModule('WebDriver');
        $this->deviceTrueCommon();
        $this->dateTimeTrueCommon('vote_date_from', 'vote_date_to');
        $this->surveyBasicInfoTrueCommon('北海道', 'title_survey_test_1');
        $this->createSurveyAssociation('keyword_survey_test_1', '学校生活、職場生活');
        $this->surveyNavigationTrue();
        $this->checkSurveySns();
        $this->checkSurveyComment();
        $this->checkSurveyResultPageCommentTag('<p>Check Tag HTML</p>');
        $this->checkResultPageCommentTagNewWindow();
        $I->click(BTN_DELETE_SURVEY);
    }
    private function checkSurveyResultPageCommentTag($tag)
    {
        $I = $this->getModule('WebDriver');
        $this->surveyResultPageCommentTagTrue($tag);
        $this->checkAcceptPopupCommon(SURVEY_SAVE_DRAFT, W_FRM_SURVEY_SAVE_DRAFT);
        $I->see(W_FRM_DETAIL_ACCEPT_A_SPAN_TAG);
    }
    private function checkResultPageCommentTagNewWindow()
    {
        $I = $this->getModule('WebDriver');
        $this->surveyResultPageCommentTagTrue('<span>Survey page comment test span</span>');
        $I->click('イメージを見る');
        $I->switchToWindow('commentwindow');
        $I->see('Survey page comment test span');
        $I->switchToWindow();
    }
    public function checkDevice()
    {
        $I = $this->getModule('WebDriver');
        $this->dateTimeTrueCommon('vote_date_from', 'vote_date_to');
        $this->surveyBasicInfoTrueCommon('北海道', 'title_survey_test_1');
        $this->surveyResultPageCommentTagTrue('Survey_Result_Page_Comment_Tag_True');
        $this->createSurveyAssociation('keyword_survey_test_1', '学校生活、職場生活');
        $this->surveyNavigationTrue();
        $this->checkSurveySns();
        $this->checkSurveyComment();
        $this->checkTickCheckboxDevice();
        $I->click(BTN_DELETE_SURVEY);
    }
    public function checkTickCheckboxDevice()
    {
        $I = $this->getModule('WebDriver');
        $I->uncheckOption('input[name =visible_device_i]');
        $I->uncheckOption('input[name =visible_device_d]');
        $this->checkAcceptPopupCommon(SURVEY_SAVE_DRAFT, W_FRM_SURVEY_SAVE_DRAFT);
        $I->see(W_FRM_SURVEY_DIVICE_REQUIRE);
    }
    public function checkSurveyDatetime()
    {
        $I = $this->getModule('WebDriver');
        $this->deviceTrueCommon();
        $this->surveyBasicInfoTrueCommon('北海道', 'title_survey_test_1');
        $this->surveyResultPageCommentTagTrue('Survey_Result_Page_Comment_Tag_True');
        $this->createSurveyAssociation('keyword_survey_test_1', '学校生活、職場生活');
        $this->surveyNavigationTrue();
        $this->checkSurveySns();
        $this->checkSurveyComment();
        
        // $this->checkdateBeforeNowCommon();
        $this->checkdateToBeforeDateFrom();
    }
    private function checkdateBeforeNowCommon()
    {
        $I = $this->getModule('WebDriver');
        $this->dateBeforeNowCommon('vote_date_from', 'vote_date_to');
        $this->checkAcceptPopupCommon(SURVEY_SAVE_DRAFT, W_FRM_SURVEY_SAVE_DRAFT);
        $I->see(W_FRM_START_DATE_SURVEY_BEFORE_NOW);
    }
    private function checkdateToBeforeDateFrom()
    {
        $I = $this->getModule('WebDriver');
        $this->dateToBeforeDateFrom('vote_date_from', 'vote_date_to');
        $this->checkAcceptPopupCommon(SURVEY_SAVE_DRAFT, W_FRM_SURVEY_SAVE_DRAFT);
        $I->see(W_FRM_END_DATE_NO_BEFORE_START_DATE_SURVEY);
    }
    private function surveyBasicInfoTrueCommon($attribute_value, $survey_title)
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('input[name=title]', $survey_title);
        $I->fillField('input[name=detail]', 'detail_survey_test_1');
        $I->fillField('choiceTextarea', 'choice_survey_test_1');
        $I->click(W_FRM_SURVEY_CHOICE_COLLECT);
        $I->fillField('.//*[@id="choiceList"]/tbody/tr[2]/td[3]/input', '1');
        $I->selectOption('input[name=summary_type]', 1);
        $this->checkSurveyBoxCommon('is_visible_other', 'shuffle_choice');
        $I->selectOption('attribute', $attribute_value);
    }
    private function deviceTrueCommon()
    {
        $I = $this->getModule('WebDriver');
        $I->checkOption('input[name=visible_device_i]');
        $I->checkOption('input[name=visible_device_d]');
        $I->selectOption('visible_flag', '表示する');
    }
    private function surveyResultPageCommentTagTrue($tag)
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('result_comment', $tag);
    }
    public function checkSurveyBasicInfo()
    {
        $I = $this->getModule('WebDriver');
        $this->deviceTrueCommon();
        $this->dateTimeTrueCommon('vote_date_from', 'vote_date_to');
        $this->surveyResultPageCommentTagTrue('Survey_Result_Page_Comment_Tag_True');
        $this->createSurveyAssociation('keyword_survey_test_1', '学校生活、職場生活');
        $this->surveyNavigationTrue();
        $this->checkSurveySns();
        $this->checkSurveyComment();
        $this->checkRequireBasicInfo();
        $this->checkRadioSeeCheckboxIsChecked();
        // $this->checkValidSurveyOtherText();
        $this->checkRadioDontSeeCheckboxIsChecked();
        // $this->checkValidSurveySummaryValue();
        $this->checkValidSurveySummaryValueTypeInteger('abc');
        // $this->checkAddNewSurveySummaryValue();
    }
    private function checkRequireBasicInfo()
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('input[name=title]', '');
        $I->fillField('input[name=detail]', '');
        $I->fillField('choiceTextarea', '');
        $this->checkAcceptPopupCommon(SURVEY_SAVE_DRAFT, W_FRM_SURVEY_SAVE_DRAFT);
        $I->see(W_FRM_SURVEY_TITLE_REQUIRED);
        $I->see(W_FRM_SURVEY_DES_REQUIRED);
        $I->see(W_FRM_SURVEY_CHOICE_COLLECT_REQUIRED);
    }
    private function checkRadioSeeCheckboxIsChecked()
    {
        $I = $this->getModule('WebDriver');
        $this->surveyBasicInfoTrueCommon('北海道', 'title_survey_test_1');
        $I->seeCheckboxIsChecked('input[name=is_visible_other]');
        $I->seeCheckboxIsChecked('input[name=shuffle_choice]');
        $this->checkDeleteChoiceSurveyBasicInfo();
        $I->click('一括作成BOXを開く');
    }
    private function checkValidSurveyOtherText()
    {
        $I = $this->getModule('WebDriver');
        $this->surveyBasicInfoTrueCommon('北海道', 'title_survey_test_1');
        $I->fillField('input[name=other_text]', '');
        $this->checkAcceptPopupCommon(SURVEY_SAVE_DRAFT, W_FRM_SURVEY_SAVE_DRAFT);
        $I->see(W_FRM_SURVEY_OTHER_TEXT);
        $this->checkDeleteChoiceSurveyBasicInfo();
    }
    private function checkRadioDontSeeCheckboxIsChecked()
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('input[name=title]', 'title_survey_test_1');
        $I->fillField('input[name=detail]', 'detail_survey_test_1');
        $I->uncheckOption('input[name=is_visible_other]');
        $I->uncheckOption('input[name=shuffle_choice]');
        $I->selectOption('input[name=summary_type]', 2);
        $I->fillField('choiceTextarea', 'choice_survey_test_1');
        $I->click(W_FRM_SURVEY_CHOICE_COLLECT);
        $I->fillField('.//*[@id="choiceList"]/tbody/tr[2]/td[3]/input', '1');
        $I->checkOption('input[name=is_visible_other]');
        $I->checkOption('input[name=shuffle_choice]');
        $I->dontSeeCheckboxIsChecked('input[name=is_visible_other]');
        $I->dontSeeCheckboxIsChecked('input[name=shuffle_choice]');
        $this->checkDeleteChoiceSurveyBasicInfo();
        $I->click('一括作成BOXを開く');
    }
    public function checkValidSurveySummaryValue()
    {
        // $I = $this->getModule('WebDriver');
        // $I->fillField('input[name=title]', 'title_survey_test_1');
        // $I->fillField('input[name=detail]', 'detail_survey_test_1');
        // $I->selectOption('input[name=summary_type]', 1);
        // $I->fillField('choiceTextarea', 'choice_survey_test_1');
        // $I->see(W_FRM_SURVEY_CHOICE_COLLECT);
        // $I->click(W_FRM_SURVEY_CHOICE_COLLECT);
        // $I->dontSee(W_FRM_SURVEY_CHOICE_COLLECT);
        // $I->fillField('.//*[@id="choiceList"]/tbody/tr[2]/td[3]/input', 'abc');
        // $this->checkAcceptPopupCommon(SURVEY_SAVE_DRAFT, W_FRM_SURVEY_SAVE_DRAFT);
        // $I->see(W_FRM_SURVEY_SUMMARY_VALUE);
    }
    public function checkAddNewSurveySummaryValue()
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('choiceTextarea', 'choice_survey_test_1');
        $I->dontSee('1つ追加');
        $I->dontSee('一括作成BOXを開く');
        $I->click(W_FRM_SURVEY_CHOICE_COLLECT);
        $I->fillField('.//*[@id="choiceList"]/tbody/tr[2]/td[3]/input', '1');
        $I->dontSee(W_FRM_SURVEY_CHOICE_COLLECT);
    }
    public function checkValidSurveySummaryValueTypeInteger($type)
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('input[name=detail]', 'detail_survey_test_1');
        $I->selectOption('input[name=summary_type]', 1);
        $I->fillField('choiceTextarea', 'choice_survey_test_1');
        $I->click(W_FRM_SURVEY_CHOICE_COLLECT);
        $I->dontSee(W_FRM_SURVEY_CHOICE_COLLECT);
        $I->fillField('.//*[@id="choiceList"]/tbody/tr[2]/td[3]/input', $type);
        $this->checkAcceptPopupCommon(SURVEY_SAVE_DRAFT, W_FRM_SURVEY_SAVE_DRAFT);
        $I->see(W_FRM_SURVEY_SUMMARY_VALUE);
    }
    public function checkDeleteChoiceSurveyBasicInfo()
    {
        $I = $this->getModule('WebDriver');
        $I->click(BTN_DELETE_SURVEY);
        $I->dontSee('choice_survey_test_1');
    }
    
    public function surveyNavigationTrue()
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('.//*[@id="form"]/table[5]/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/input', 'D_DES_1');
        $I->fillField('.//*[@id="form"]/table[5]/tbody/tr[2]/td/table/tbody/tr[2]/td[3]/input', 'D_URL_1');
        $I->fillField('input[name=back_button_text]', 'Title_1');
        $I->fillField('input[name=back_button_url]', 'URL_1');
    }
    public function createNewSurvey($survey_title, $submit, $message, $text)
    {
        $I = $this->getModule('WebDriver');
        $this->deviceTrueCommon();
        $this->dateTimeTrueCommon('vote_date_from', 'vote_date_to');
        $this->surveyBasicInfoTrueCommon('北海道', $survey_title);
        $this->surveyResultPageCommentTagTrue('Survey_Result_Page_Comment_Tag_True');
        $this->createSurveyAssociation('keyword_survey_test_1', '学校生活、職場生活');
        $this->surveyNavigationTrue();
        $this->checkSurveyComment();
        $this->checkSurveySns();
        $this->checkAcceptPopupCommon($submit, $message);
        $I->see($text);
    }
    public function createNewSurveyStatus($survey_title)
    {
        $I = $this->getModule('WebDriver');
        $this->deviceTrueCommon();
        $this->surveyBasicInfoTrueCommon('北海道', $survey_title);
        $this->surveyResultPageCommentTagTrue('Survey_Result_Page_Comment_Tag_True');
        $this->createSurveyAssociation('keyword_survey_test_1', '学校生活、職場生活');
        $this->surveyNavigationTrue();
        $this->checkSurveyComment();
        $this->checkSurveySns();
        $this->checkAcceptPopupCommon(SURVEY_APPROVAL_REFLECT, W_FRM_SURVEY_APPROVAL_REFLECT);
    }
    private function datetimeCommonStatus($date_from, $datepicker_from, $date_to, $datepicker_to)
    {
        $I = $this->getModule('WebDriver');
        $I->click('input[name=' . $date_from . ']');
        $I->click($datepicker_from);
        $I->click('input[name=' . $date_to . ']');
        $I->click($datepicker_to);
    }
    private function checkMessageStatus($message_status)
    {
        $I = $this->getModule('WebDriver');
        $I->click('アンケート一覧へ');
        $I->see($message_status);
        $this->logout();
    }
    public function checkSurveyStatusPrepareApprove($survey_title, $message_status)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(LINK_TO_CREATE_SURVEY);
        $this->datetimeCommonStatus('vote_date_from', './/*[@id="ui-datepicker-div"]/table/tbody/tr[3]/td[3]/a', 'vote_date_to', './/*[@id="ui-datepicker-div"]/table/tbody/tr[5]/td[4]/a');
        $this->createNewSurveyStatus($survey_title);
        $this->checkMessageStatus($message_status);
    }
    public function checkSurveyStatusApprove($survey_title, $message_status)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(LINK_TO_CREATE_SURVEY);
        $this->datetimeCommonStatus('vote_date_from', './/*[@id="ui-datepicker-div"]/table/tbody/tr[1]/td[2]/a', 'vote_date_to', './/*[@id="ui-datepicker-div"]/table/tbody/tr[5]/td[4]/a');
        $this->createNewSurveyStatus($survey_title);
        $this->checkMessageStatus($message_status);
    }
    public function checkSurveyStatusFinish($survey_title, $message_status)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(LINK_TO_CREATE_SURVEY);
        $this->datetimeCommonStatus('vote_date_from', './/*[@id="ui-datepicker-div"]/table/tbody/tr[1]/td[2]/a', 'vote_date_to', './/*[@id="ui-datepicker-div"]/table/tbody/tr[1]/td[4]/a');
        $this->createNewSurveyStatus($survey_title);
        $this->checkMessageStatus($message_status);
    }
    public function checkSurveyStatusDraftAndRequest($survey_title, $submit, $message, $text, $message_status)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(LINK_TO_CREATE_SURVEY);
        $this->createNewSurvey($survey_title, $submit, $message, $text);
        $this->checkMessageStatus($message_status);
    }
    public function checkCompleteSurvey($survey_title, $submit, $message, $text, $status = null)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(LINK_TO_CREATE_SURVEY);
        $this->createNewSurvey($survey_title, $submit, $message, $text);
        
        // $I->seeLink('内容を確認する');
        $I->click('アンケート一覧へ');
        if (isset($status)) {
            $I->see($status);
        }
        $this->logout();
    }
    public function createAccountTrue($arr, $account_name, $register = null, $survey = null)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon('アカウント一覧・登録・修正・削除');
        $I->click('＋アカウント追加');
        $I->fillField('account_name', $account_name);
        $I->fillField('login_id', $account_name);
        $I->fillField('login_password', '123456');
        $this->createAccountCommonTrue($arr);
        if (isset($survey)) {
            $I->checkOption('input[name=is_viewable_all_question]');
        }
        if (isset($register)) {
            $I->checkOption('input[name=is_visible_register]');
        }
        $this->checkAcceptPopupCommon('登録', '登録します。よろしいですか？');
        $this->logout();
    }
    private function createAccountCommonTrue($arr)
    {
        $I = $this->getModule('WebDriver');
        foreach ($arr as $key => $value) {
            $I->checkOption('input[name=' . $value . ']');
        }
        $I->uncheckOption('input[name=is_enabled]');
    }
    private function loginPermission($account)
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('login_id', $account);
        $I->fillField('login_password', '123456');
        $I->click('ログイン');
        $I->click(SURVEY_LIST);
    }
    public function checkSurveyPermissionDontSeeAccount($account)
    {
        $I = $this->getModule('WebDriver');
        $this->loginPermission($account);
        $I->see('account_id', $account);
        $I->dontSee('account_id', 'account_test_4');
        $this->logout();
    }
    public function checkSurveyPermissionSeeAccount($account)
    {
        $I = $this->getModule('WebDriver');
        $this->loginPermission($account);
        $I->see('account_id', $account);
        $I->see('account_id', 'account_test_1');
        $I->see('account_id', 'account_test_3');
        $I->see('account_id', 'account_test_4');
        $this->logout();
    }
    public function checkSurveySeeButton($account, $button = null)
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('login_id', $account);
        $I->fillField('login_password', '123456');
        $I->click('ログイン');
        $I->click(LINK_TO_CREATE_SURVEY);
        if (isset($button)) {
            $I->see('承認・反映');
        } else {
            $I->dontSee('承認・反映');
        }
        $this->logout();
    }
    public function createSurveyPermissionSurvey($account, $survey_title, $submit, $message, $text)
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('login_id', $account);
        $I->fillField('login_password', '123456');
        $I->click('ログイン');
        $I->click(LINK_TO_CREATE_SURVEY);
        $this->createNewSurvey($survey_title, $submit, $message, $text);
        $this->logout();

    }
    public function dontSeeSurveyLinkPermission($account, $title, $title_own)
    {
        $I = $this->getModule('WebDriver');
        $this->loginPermission($account);
        $I->dontSeeLink($title, '/manage/enq/edit/1');
        $I->click($title_own);
        $this->logout();
    }
    public function seeSurveyLinkPermission($account, $title, $title_own)
    {
        $I = $this->getModule('WebDriver');
        $this->loginPermission($account);
        $I->seeLink($title, '/manage/enq/edit/2');
        $I->click($title_own);
        $this->logout();
    }


    public function searchSurveyGenre($genre_true, $genre_false)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(SURVEY_LIST);
        $I->selectOption('category_child_id', $genre_true);
        $I->click(SURVEY_SEARCH);
        $I->see('title_test_column');
        $I->selectOption('category_child_id', $genre_false);
        $I->click(SURVEY_SEARCH);
        $I->dontSee('title_test_column');
        $this->logout();
    }

    public function searchSurveyKeyword($keywordtest_true, $keywordtest_false)
    {
        $I = $this->getModule('WebDriver');
        $this->searchSurveyKeywordCommon($keywordtest_true);
        $I->see('title_test_column');
        $this->logout();
        $this->searchSurveyKeywordCommon($keywordtest_false);
        $I->dontSee('title_test_column');
        $this->logout();
    }
    private function searchSurveyKeywordCommon($keyword)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(SURVEY_LIST);
        $I->click('キーワードウィンドウを開く');
        $I->switchToWindow('keywordwindow');
        $I->click($keyword);
        $I->switchToWindow();
        $I->click(SURVEY_SEARCH);
    }
    public function searchSurveyDatetime($title)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(SURVEY_LIST);
        $this->datetimeCommonStatus('date_from_start', './/*[@id="ui-datepicker-div"]/table/tbody/tr[3]/td[7]/a', 'date_to_end', './/*[@id="ui-datepicker-div"]/table/tbody/tr[4]/td[7]/a');
        $I->click(SURVEY_SEARCH);
        $I->dontSee($title);
        $this->logout();
    }
    public function searchSurveyAccount($account)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(SURVEY_LIST);
        $I->selectOption('account_id', $account);
        $I->click(SURVEY_SEARCH);
        $I->dontSee('title_test_column');
        $this->logout();
    }
    public function searchSurveyStatus($title, $status)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(SURVEY_LIST);
        $I->selectOption('status', $status);
        $I->click(SURVEY_SEARCH);
        $I->see($title);
        $I->selectOption('status', SURVEY_STATUS_REQUEST_APPROVE);
        $I->click(SURVEY_SEARCH);
        $I->dontSee($title);
        $this->logout();
    }

    public function searchSurveyColumn($title)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(SURVEY_LIST);
        $I->checkOption('input[name = is_column]');
        $I->click(SURVEY_SEARCH);
        $I->dontSee($title);
        $this->logout();
    }
    public function searchSurveyId($id, $title, $id_false)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(SURVEY_LIST);
        $I->fillField('input[name = id]', $id);
        $I->click(SURVEY_SEARCH);
        $I->see($title);
        $I->fillField('input[name = id]', $id_false);
        $I->click(SURVEY_SEARCH);
        $I->dontSee($title);
        $this->logout();

    }
    public function searchSurveyTitle($title, $title_false)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(SURVEY_LIST);
        $I->fillField('input[name = title]', $title);
        $I->click(SURVEY_SEARCH);
        $I->see($title);
        $I->fillField('input[name = title]', $title_false);
        $I->click(SURVEY_SEARCH);
        $I->dontSee($title);
        $this->logout();

    }
    public function searchSurveyDisplay($title)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(SURVEY_LIST);
        $I->selectOption('visible_flag', '表示する');
        $I->click(SURVEY_SEARCH);
        $I->see($title);
        $I->selectOption('visible_flag', '完全非表示');
        $I->click(SURVEY_SEARCH);
        $I->dontSee($title);
        $this->logout();
    }
    private function checkBtnCommon($survey_title, $submit, $message, $text, $btn, $message_confirm)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(LINK_TO_CREATE_SURVEY);
        $this->createNewSurvey($survey_title, $submit, $message, $text);
        $I->click(LINK_TO_EDIT_SURVEY);
        $I->click($btn);
        $I->seeInPopup($message_confirm);
        $I->acceptPopup();
        $I->dontSee('通常モード');
    }
    private function checkBtnBackSaveDraftUpdate($survey_title, $submit, $message, $text, $btn, $message_confirm)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(LINK_TO_CREATE_SURVEY);
        $this->createNewSurvey($survey_title, $submit, $message, $text);
        $I->click(LINK_TO_SAVE_DRAFT_SURVEY);
        $I->click($btn);
        $I->seeInPopup($message_confirm);
        $I->acceptPopup();
    }
    public function checkBtnBackSaveDraft($survey_title, $submit, $message, $text, $btn, $message_confirm)
    {
        $I = $this->getModule('WebDriver');
        $this->checkBtnBackSaveDraftUpdate($survey_title, $submit, $message, $text, $btn, $message_confirm);
        $I->dontSee('通常モード');
        $this->logout();
    }
    public function checkBtnBackDraft($survey_title, $submit, $message, $text, $btn, $message_confirm)
    {
        $I = $this->getModule('WebDriver');
        $this->checkBtnCommon($survey_title, $submit, $message, $text, $btn, $message_confirm);
        // $I->see(SURVEY_SAVE_DRAFT_TEXT);
        $this->logout();
    }
    public function checkBtnUpdateRequest($survey_title, $submit, $message, $text, $btn, $message_confirm)
    {
        $I = $this->getModule('WebDriver');
        $this->checkBtnCommon($survey_title, $submit, $message, $text, $btn, $message_confirm);
        $I->see(SURVEY_APPROVAL_TEXT);
        $this->logout();
    }
    public function checkBtnDeleteSurvey($survey_title, $submit, $message, $text, $btn, $message_confirm)
    {
        $I = $this->getModule('WebDriver');
        $this->checkBtnCommon($survey_title, $submit, $message, $text, $btn, $message_confirm);
        $I->dontSee($survey_title);
        $this->logout();
    }
    public function checkBtnEditFullSurvey($survey_title, $submit, $message, $text, $btn, $message_confirm)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon(LINK_TO_CREATE_SURVEY);
        $this->deviceTrueCommon();
        // $this->dateTimeTrueCommon('vote_date_from', 'vote_date_to');
        $this->datetimeEndBeforeNow('vote_date_from', 'vote_date_to');
        $this->surveyBasicInfoTrueCommon('北海道', $survey_title);
        $this->surveyResultPageCommentTagTrue('Survey_Result_Page_Comment_Tag_True');
        $this->createSurveyAssociation('keyword_survey_test_1', '学校生活、職場生活');
        $this->surveyNavigationTrue();
        $this->checkSurveyComment();
        $this->checkSurveySns();
        $this->checkAcceptPopupCommon($submit, $message);
        $I->see($text);
        $I->click(LINK_TO_EDIT_SURVEY);
        $I->click($btn);
        $I->seeInPopup($message_confirm);
        $I->acceptPopup();
        $I->see('通常モード');
        $this->logout();
    }
    private function datetimeEndBeforeNow($date_from, $date_to)
    {
        $I = $this->getModule('WebDriver');
        $I->click('input[name=' . $date_from . ']');
        $I->click('.//*[@id="ui-datepicker-div"]/table/tbody/tr[1]/td[2]/a');
        $I->click('input[name=' . $date_to . ']');
        $I->click('.//*[@id="ui-datepicker-div"]/table/tbody/tr[2]/td[2]/a');
    }
    /**
     * Collection
     */
    public function  checkCreateCollection()
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon('特集枠データ作成');
        $this->checkRequireCollection();
        $this->checkDatetimeCollection();
        $this->logout();
    }
    private  function  checkRequireCollection()
    {
        $I = $this->getModule('WebDriver');
        $I->click(BTN_ADD_FEATURE);
        $I->see(W_FRM_FEATURE_TITLE_REQUIRED);
        $I->see(W_FRM_FEATURE_MESSAGE_REQUIRED);
        $I->see(W_FRM_FEATURE_DATE_FROM_REQUIRED);
        $I->see(W_FRM_FEATURE_DATE_TO_REQUIRED);
    }
    private function  checkDatetimeCollection()
    {
        $I = $this->getModule('WebDriver');
        $this->dateToBeforeDateFrom('visible_date_from', 'visible_date_to');
        $I->click(BTN_ADD_FEATURE);
        $I->see(W_FRM_FEATURE_DATE_TO_AFTER);
        $this->dateBeforeNowCommon('visible_date_from', 'visible_date_to');
        $I->click(BTN_ADD_FEATURE);
        $I->see(W_FRM_FEATURE_DATE_FROM_AFTER);
    }

    public  function  createNewCollection($title, $message, $question_id, $is_visible, $is_open_column)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon('特集枠データ作成');
        $this->createNewCollectionCommon($title, $message, $question_id, $is_visible, $is_open_column);
        $I->click(BTN_ADD_FEATURE);
        $this->checkAcceptPopupCommon(BTN_LINK_TO_FEATURE, I_MSG_ADD_FEATURE);
        $this->logout();
        // $this->checkAcceptPopupCommon('確認', 'medssage');
    }
    public function createNewCollectionCommon($title, $message, $question_id, $is_visible, $is_open_column)
    {
        $I = $this->getModule('WebDriver');
        $this->createTitleCollection($title);
        $this->createMessageCollection($message);
        $this->createDatetimeCollection();
        $this->createQuestionIdCollection($question_id);
        $this->createCheckboxCollection($is_visible, $is_open_column);
    }

    public function checkConfirmCollection($title, $message, $question_id, $is_visible, $is_open_column)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon('特集枠データ作成');
        $this->createNewCollectionCommon($title, $message, $question_id, $is_visible, $is_open_column);
        $I->click(BTN_ADD_FEATURE);
        $I->click(BTN_LINK_TO_BACK);
        $this->checkGrabValueCollection($title, $message, $question_id, $is_visible, $is_open_column);
        $I->click(BTN_ADD_FEATURE);
        $this->checkAcceptPopupCommon(BTN_LINK_TO_FEATURE, I_MSG_ADD_FEATURE);
        $this->deleteCollection();
        $this->logout();
    }

    private function checkGrabValueCollection($title, $message, $question_id, $is_visible, $is_open_column)
    {
        $I = $this->getModule('WebDriver');
        $I->seeInField('input[name=title]', $title);
        $I->seeInField('form textarea[name=message]', $message);
        $I->seeInField('input[name=title]', $title);
        if(isset($is_visible))
        {
            $I->seeCheckboxIsChecked('input[name=is_visible]');
        }
        else
        {
            $I->dontSeeCheckboxIsChecked('input[name=is_visible]');
        }

        if(isset($is_open_column))
        {
            $I->seeCheckboxIsChecked('input[name=is_open_column]');
        }
        else
        {
            $I->dontSeeCheckboxIsChecked('input[name=is_open_column]');
        }
    }

    private  function  createTitleCollection($title)
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('input[name=title]', $title);

    }
    private  function  createMessageCollection($message)
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('form textarea[name=message]', $message);
    }
    private  function  createDatetimeCollection()
    {
        $I = $this->getModule('WebDriver');
        $this->dateTimeTrueCommon('visible_date_from', 'visible_date_to');
    }
    private  function  createQuestionIdCollection($question_id)
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('input[name=question_id]',$question_id);
    }
    private  function  createCheckboxCollection($is_visible = null, $is_open_column = null)
    {
        $I = $this->getModule('WebDriver');
        if(isset($is_visible)){
            $I->checkOption('input[name=is_visible]');
        }
        if(isset($is_open_column)){
            $I->checkOption('input[name=is_open_column]');
        }
    }
    public  function  deleteCollection()
    {
        $I = $this->getModule('WebDriver');
        $this->checkAcceptPopupCommon(BTN_DELETE_FEATURE, I_MSG_FEATURE_DELETE);
    }

    public function checkListCollection($title, $message, $question_id, $is_visible = null, $is_open_column = null)
    {
        $I = $this->getModule('WebDriver');
        $this->createNewCollection($title, $message, $question_id, $is_visible, $is_open_column);
        $this->loginClickCommon('特集枠一覧・修正・削除');
        if(isset($is_visible)){
            $I->see('表示');
        }
        else{
            $I->dontSee('表示');
        }
        if(isset($is_open_column)){
            $I->see('コラム');
        }
        else{
            $I->dontSee('コラム');
        }
        $I->click($title);
        $this->checkGrabValueCollection($title, $message, $question_id, $is_visible, $is_open_column);
        $this->checkDatetimeCollection();
        $this->createDatetimeCollection();
        $I->fillField('input[name=title]', 'title_test_collection_change');
        $I->click(BTN_ADD_FEATURE);
        $this->checkAcceptPopupCommon(BTN_LINK_TO_FEATURE, I_MSG_ADD_FEATURE);
        $I->dontSee($title);
        $I->see('title_test_collection_change');
        $this->logout();
    }

    public function editCollection($title, $is_view, $is_column)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon('特集枠一覧・修正・削除');
        $I->click($title);

        if ($is_view==1) {
            $I->checkOption('input[name = is_view_list]');
        }
        else {
            $I->uncheckOption('input[name = is_view_list]');
        }

        if ($is_column==1) {
            $I->checkOption('input[name = is_feature_column]');
        }
        else {
            $I->uncheckOption('input[name = is_feature_column]');
        }

        $I->click(BTN_ADD_FEATURE);
        $this->checkAcceptPopupCommon(BTN_LINK_TO_FEATURE, I_MSG_EDIT_FEATURE);
        $this->logout();
    }


    /**
     * funtion to fill text to text field
     * @param array $arr : $arr['name'] - name of input element
     *                    $arr['value'] - value to input
     * @return fill text to input type=text
     */
    protected function setTextField($arr)
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('input[name='.$arr["name"].']', $arr["value"]);
    }

    /**
     * funtion to fill text to textarea field
     * @param array $arr : $arr['name'] - name of text area element
     *                     $arr['value'] - value to input
     * @return fill text to input type=textarea
     */
    protected function setTextAreaField($arr)
    {
        $I = $this->getModule('WebDriver');
        $I->fillField('form textarea[name='.$arr["name"].']', $arr["value"]);
    }

    /**
     * funtion to input datetime
     * @param array $arr : $arr['date_name'] - name of datetime element
     *                     $arr['date_value'] - value input (xpath)
     *                     $arr['hour_name'] (option)- name of hour element (select option)
     *                     $arr['hour_value'] (option)- value of hour element
     *                     $arr['minute_name'] (option)- name of minute element (select option)
     *                     $arr['minute_value'] (option)- value of minute element
     * @return fill value for datetime
     */
    protected function setDateTime($arr)
    {
        $I = $this->getModule('WebDriver');
        $I->click('input[name=' . $arr['date_name'] . ']');
        $I->click($arr['date_xpath']);

        if (isset($arr['hour_name'])) {
            $I->selectOption('form select[name=' . $arr['hour_name'] .']', $arr['hour_value']);
            $hour_value = $arr['hour_name'];
        }
        if (isset($arr['minute_name'])) {
            $I->selectOption('form select[name=' . $arr['minute_name'] .']', $arr['minute_value']);
            $minute_value = $arr['minute_name'];
        }
        return $this->getDateTime($arr);
    }

    /**
     * funtion fill value for checkbox option
     * @param array $arr : $arr['name'] - name of checkbox option
     *                     $arr['check'] - 1:check, 0:uncheck
     * @return status checkbox option
     */
    protected function setCheckOption($arr)
    {
        $I = $this->getModule('WebDriver');
        if ($arr['check'] == 1) {
            $I->checkOption('input[name='.$arr["name"].']');
        }
        else {
            $I->uncheckOption('input[name='.$arr["name"].']');
        }

    }

    /**
     * funtion check link
     * @param array $arr: list link to check and one item has:
     *                  $arr['name'] - titel of link
     *                  $arr['value'] - link path
     *                  $arr['see'] - 1:seeLink, 0:dontSeeLink
     * @return status checkbox option
     */
    protected function checkLink($arr)
    {
        $I = $this->getModule('WebDriver');
        foreach ($arr as $link) {
            if ($link["see"] == 1) {
                $I->seeLink($link["name"], $link["value"]);
            }
            else {
                $I->dontSeeLink($link["name"], $link["value"]);
            }
        }
    }

    protected function getDateTime($arr)
    {
        $I = $this->getModule('WebDriver');
        $date = $I->grabValueFrom('input[name='.$arr["date_name"].']');

        return $date;
    }

    public function checkValidFeature($arr)
    {
        $I = $this->getModule('WebDriver');
        $this->loginClickCommon('LINK_ADMIN_COLLECTION_CREATE');

        //see all valid
        $I->click(BTN_ADD_FEATURE);
        $I->see(VALID_FEATURE);
        $I->see(VALID_FEATURE_TITLE);
        $I->see(VALID_FEATURE_DATE_FROM);
        $I->see(VALID_FEATURE_DATE_TO);

        //input title
        $this->setTextField($arr['title']);
        $I->click(BTN_ADD_FEATURE);
        $I->see(VALID_FEATURE);
        $I->dontSee(VALID_FEATURE_TITLE);
        $I->see(VALID_FEATURE_DATE_FROM);
        $I->see(VALID_FEATURE_DATE_TO);

        //input date from
        $this->setDateTime($arr["date_from"]);
        $I->click(BTN_ADD_FEATURE);
        $I->see(VALID_FEATURE);
        $I->dontSee(VALID_FEATURE_TITLE);
        $I->dontSee(VALID_FEATURE_DATE_FROM);
        $I->see(VALID_FEATURE_DATE_TO);

        //input date_to < now()
        $this->setDateTime($arr["date_to"]);
        $I->click(BTN_ADD_FEATURE);
        $I->dontSee(VALID_FEATURE);
        $I->dontSee(VALID_FEATURE_TITLE);
        $I->dontSee(VALID_FEATURE_DATE_FROM);
        $I->dontSee(VALID_FEATURE_DATE_TO);
        $I->see(VALID_FEATURE_DATE);

        //input date_to > now
        $this->setDateTime($arr["date_to"]);
        $I->click(BTN_ADD_FEATURE);
        $I->dontSee(VALID_FEATURE);
        $I->dontSee(VALID_FEATURE_TITLE);
        $I->dontSee(VALID_FEATURE_DATE_FROM);
        $I->dontSee(VALID_FEATURE_DATE_TO);
        $I->dontsee(VALID_FEATURE_DATE);

    }

    public  function addFeature($arr)
    {
        $I = $this->getModule('WebDriver');
        $I->amOnPage('/manage');
        $this->loginClickCommon(LINK_ADMIN_FEATURE_CREATE);

        $this->setTextField($arr["title"]);
        $this->setTextAreaField($arr["message"]);
        $this->setDateTime($arr["date_from"]);
        $this->setDateTime($arr["date_to"]);
        $this->setCheckOption($arr["is_view"]);
        $this->setCheckOption($arr["is_column"]);
        $this->setTextField($arr["question"]);

        $arr['date_from']['date_value'] = $this->getDateTime($arr['date_from']);
        $arr['date_to']['date_value'] = $this->getDateTime($arr['date_to']);

        $I->click(BTN_ADD_FEATURE);

        $this->confirmFeature($arr, 1);

        $this->logout();
    }

    public function confirmFeature($arr, $continue)
    {
        $I = $this->getModule('WebDriver');
        $I->see($arr['title']['value']);
        $I->see($arr['message']['value']);

        $date_from = $arr['date_from']['date_value'].' '.$arr['date_from']['hour_value'].':'.$arr['date_from']['minute_value'];
        $I->see($date_from);
        $date_to = $arr['date_to']['date_value'].' '.$arr['date_to']['hour_value'].':'.$arr['date_to']['minute_value'];
        $I->see($date_to);

        if ($continue==1) {
            $this->checkAcceptPopupCommon(BTN_LINK_TO_FEATURE, I_MSG_ADD_FEATURE);
        }
        else {
            $this->checkAcceptPopupCommon(BTN_LINK_TO_FEATURE, I_MSG_ADD_FEATURE);
        }

    }

    public function createNewNotice($arr_notice)
    {
        $I = $this->getModule('WebDriver');
        $I->amOnPage('/manage');
        $this->loginClickCommon('お知らせ一覧・登録・修正・削除');
        $I->click('＋おしらせ追加');
        $I->fillField('input[name = title]', $arr_notice['title']);
        $I->fillField('input[name = description]', $arr_notice['description']);
        $I->fillField('details', $arr_notice['details']);

        $I->click('input[name = '.$arr_notice['from_date'].']');
        $I->click($arr_notice['from_date_xpath_datepicker_day']);
        $I->selectOption($arr_notice['from_hour'], $arr_notice['from_date_xpath_datepicker_hour']);
        $I->selectOption('from_min', $arr_notice['from_date_xpath_datepicker_min']);

        $I->click('input[name = '.$arr_notice['to_date'].']');
        $I->click($arr_notice['to_date_xpath_datepicker']);
        $I->selectOption($arr_notice['to_hour'], $arr_notice['to_date_xpath_datepicker_hour']);
        $I->selectOption('to_min', $arr_notice['to_date_xpath_datepicker_min']);

        $I->click('確認');
        $I->click('登録');
        $I->seeInPopup('登録します。よろしいですか？');
        $I->acceptPopup();
        $this->logout();

    }

     //funtion for check list have datetime display
    public function dateToJpdate($date, $format = 'Y年m月d日', $nullMessage = '未指定')
    {

        if (isset($date)) {
            $date = date_create($date);
            return  $date->format('Y年m月d日 H:i');
        }
        return $nullMessage;
    }

    //funtion sort array
    public function sortItems($items, $order = 'desc')
    {
        if ($order == 'desc') {
            usort($items, function($a, $b) {
                            if($a['sort']==$b['sort'])
                                return 0;
                            return $a['sort'] < $b['sort']?1:-1;
                        });
        }
        else {
            usort($items, function($a, $b) {
                            if($a['sort']==$b['sort'])
                                return 0;
                            return $a['sort'] < $b['sort']?-1:1;
                        });
        }

        return $items;
    }

    //create survey
    public function createSurveyForList($items, $link)
    {
        $I = $this->getModule('WebDriver');
        $I->amOnPage('/manage');
        $this->loginClickCommon('アンケート新規投稿');
        foreach ($items as $item) {
            $this->createSurveyForListCommon($item, $link);
        }
    }

    public function createSurveyForListCommon($arr, $link)
    {
        $I = $this->getModule('WebDriver');
        $I->amOnPage('/manage/question/create');
        $this->setDateTime($arr['date_from']);
        $this->setDateTime($arr['date_to']);
        $this->setTextField($arr['title']);
        $this->setTextField($arr['description']);
        $this->setTextAreaField($arr['choices']);

        $this->checkAcceptPopupCommon($link['create']['title'], $link['create']['message']);
        $I->click($link['confirm']);
        $this->checkAcceptPopupCommon($link['edit']['title'], $link['edit']['message']);
    }

    //check lisst
    public function checkListCommons($items, $xpath_format, $item_must_see)
    {
        $I = $this->getModule('WebDriver');
        $count = 1;
        foreach ($items as $item) {
            if ($count <= $item_must_see) {
                if ($item['status']==1) {
                    $xpath = $this->getXpathOfList($xpath_format, $count);

                    //see title with link and in xpath
                    $I->seeLink($item['title']);
                    $I->see($item['title'], $xpath);

                    //see description
                    if (isset($item['description'])) {
                        $I->see($item['description']);
                    }

                    //see answer
                    if (isset($item['answer'])) {
                        $I->see($item['answer']);
                    }

                    //see date
                    if (isset($item['jpdate'])) {

                        $I->see($item['jpdate'], $xpath);
                    }

                    $count++;
                }
                else {
                    $I->dontSeeLink($item['title']);
                    //$I->dontSee($item['date']);
                }
            }
        }

        $after_last_item = $this->getXpathOfList($xpath_format, $count);
        $I->dontSeeElement($after_last_item);
    }


    public function checkListShowmoreCommons($link_showmore, $items, $xpath_format, $item_must_see)
    {
        $I = $this->getModule('WebDriver');
        $I->click($link_showmore);
        $I->wait(5);
        $this->checkListCommons($items, $xpath_format, $item_must_see);
    }

    private function getXpathOfList($xpath_format, $location)
    {
        $xpath = sprintf($xpath_format, $location);
        return $xpath;
    }


    public function checkEndListCommons($link_showmore)
    {
        $I = $this->getModule('WebDriver');
        $I->dontSeeLink($link_showmore);
    }

    public function checkLinkEndList($link)
    {
        $I = $this->getModule('WebDriver');
        $I->seeLink($link['title'], $link['url']);
        // $I->click($link['title']);
        // $I->seeCurrentUrlEquals($link['url']);
    }


    //Profile
    public function checkFrontProfileValidate($arr, $messages)
    {
        $I = $this->getModule('WebDriver');
        $I->amOnPage('/profile');
        $I->click($arr['link']['button_confirm']);
        foreach ($messages as $message) {
            $I->see($message);
        }
    }

    public function createFrontProfile($arr)
    {
        $I = $this->getModule('WebDriver');
        $I->amOnPage('/profile');

        $I->selectOption('form select[name='.$arr["sex"]["name"].']', $arr["sex"]["value"]);
        $I->selectOption('form select[name='.$arr["year"]["name"].']', $arr["year"]["value"]);
        $I->selectOption('form select[name='.$arr["month"]["name"].']', $arr["month"]["value"]);
        $I->selectOption('form select[name='.$arr["day"]["name"].']', $arr["day"]["value"]);
        $I->selectOption('form select[name='.$arr["place"]["name"].']', $arr["place"]["value"]);

        foreach ($arr['descriptions'] as $description) {
            $I->see($description);
        }

        $I->click($arr["link"]["button_confirm"]);
    }

    public function checkFrontProfileExit($arr)
    {
        $I = $this->getModule('WebDriver');
        $I->seeOptionIsSelected('form select[name='.$arr["sex"]["name"].']', $arr["sex"]["value"]);
        $I->seeOptionIsSelected('form select[name='.$arr["year"]["name"].']', $arr["year"]["value"]);
        $I->seeOptionIsSelected('form select[name='.$arr["month"]["name"].']', $arr["month"]["value"]);
        $I->seeOptionIsSelected('form select[name='.$arr["day"]["name"].']', $arr["day"]["value"]);
        $I->seeOptionIsSelected('form select[name='.$arr["place"]["name"].']', $arr["place"]["value"]);

        foreach ($arr['descriptions'] as $description) {
            $I->see($description);
        }
        $I->click($arr["link"]["button_confirm"]);
    }

    public function checkFrontProfileConfirm($arr, $continue)
    {
        $I = $this->getModule('WebDriver');
        $I->seeLink($arr['link']['link_profile']);
        $I->seeLink($arr['link']['link_top']);

        if ($continue === '1') {
            $I->click($arr['link']['link_top']);
            $I->seeCurrentUrlEquals('/');
        }
        else {
            $I->click($arr['link']['link_profile']);
            $I->seeCurrentUrlEquals('/profile');
        }
    }
}
