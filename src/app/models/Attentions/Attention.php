<?php
namespace docomo\Models\Attentions;

use docomo\Models\Keywords\Keyword;
use docomo\Models\Questions\QuestionTag;
use docomo\Models\Questions\SummaryByType;

class Attention extends \BaseArdentModel
{
    
    protected $table = 'attentions';

    public static $rules = [
        'keyword_id' => 'required',
        'from' => ['required', 'date', 'datetime_format:Y-m-d H:i'],
        'to' => ['required', 'date', 'after:"from"', 'datetime_format:Y-m-d H:i']
    ];

    public static $customMessages = [
        'keyword_id.required' => W_FRM_REQUIRED_KEYWORD_ID,
        'from.required' => W_FRM_REQUIRED_DATE_FROM,
        'from.date' => W_FRM_FORMAT_DATE_FROM,
        'from.datetime_format' => W_FRM_LENGTH_DATE_FROM,
        'to.required' => W_FRM_REQUIRED_DATE_TO,
        'to.after' => W_FRM_COMPLEX_DATE_TO_AFTER_FROM,
        'to.date' => W_FRM_FORMAT_DATE_TO,
        'to.datetime_format' => W_FRM_LENGTH_DATE_TO,
    ];

    protected $fillable = [
        'keyword_id',
        'tab',
        'from',
        'to'
    ];

    public function keyword()
    {

        return $this->belongsTo('docomo\Models\Keywords\Keyword');
    }

    public static function getList()
    {
        
        return Attention::select('id', 'keyword_id', 'tab', 'from', 'to')->orderBy('from', 'desc')->orderBy('id', 'desc')->get();
    }

    public static function getDetails($id)
    {

        return Attention::select('id', 'keyword_id', 'tab', 'from', 'to')->findOrFail($id);
    }

    public static function fillWithModify($id = null)
    {

        $attention = null;
        if ($id === null)
        {
            $attention = new Attention;
        }
        else
        {
            $attention = Attention::getDetails($id);
        }

        $attention->fill(\Input::all());

        $attention->from = $attention->getDatetimeControlValue('from');
        $attention->to = $attention->getDatetimeControlValue('to');

        return $attention;
    }

    public static function getTopKeywords() {
        $attentions = Attention::
            whereRaw('attentions.from <= now() AND now() < attentions.to')
            ->with(['keyword' => function ($query) {
                return $query->select('id', 'keyword_name')->where('is_visible', '=', true);
            }])
            ->get(['keyword_id', 'tab']);
        $topData = [PROCESSING => [], COMPLETE => []];
        foreach ($attentions as $attention) {
            if (!is_null($attention->keyword)) {
                $topData[$attention->tab][$attention->keyword->id] = $attention->keyword->keyword_name;
            }
            unset($keyword);
        }
        $popEntry = SummaryByType::getListByCache(SUMMARY_TYPE_POPULAR_ENTRY);

        if (isset($popEntry)) {
            $keyword = Keyword::select('id', 'keyword_name')
                ->whereIsVisible(true)
                ->whereNotIn('id', (count($topData[PROCESSING]) > 0) ? array_keys($topData[PROCESSING]) : [-1])
                ->whereRaw('`id` IN (SELECT `keyword_id` FROM `question_tags` WHERE question_id = ? ORDER BY RAND()) ',
                    [$popEntry->id])
                ->first();
            if (!is_null($keyword)) {
                $topData[PROCESSING][$keyword->id] = $keyword->keyword_name;
            }
            unset($popEntry);
            unset($keyword);
        }
        unset($popEntries);

        $popResult = SummaryByType::getListByCache(SUMMARY_TYPE_POPULAR_RESULT);

        if (isset($popResult)) {
            $keyword = Keyword::select('id', 'keyword_name')
                ->whereIsVisible(true)
                ->whereNotIn('id', (count($topData[COMPLETE]) > 0) ? array_keys($topData[COMPLETE]) : [-1])
                ->whereRaw('`id` IN (SELECT `keyword_id` FROM `question_tags` WHERE question_id = ? ORDER BY RAND()) ',
                    [$popResult->id])
                ->first();
            if (!is_null($keyword)) {
                $topData[COMPLETE][$keyword->id] = $keyword->keyword_name;
            }
            unset($popResult);
            unset($keyword);
        }
        unset($popResults);

        return $topData;
    }
}
