@extends('layouts.front')

@section('metaTag')
    <title>みんなの声</title>
@stop

@section('content')
<section class="l-margin">
    <p class="tcenter l-margint l-margin">該当するページはありません</p>
    <p class="tcenter l-margin"><a href="{{ url('/') }}" class="btnType02">みんなの声TOPへ</a></p>
</section>

@stop