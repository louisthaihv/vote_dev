rpm -ivh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
rpm -ivh http://dl.iuscommunity.org/pub/ius/stable/CentOS/6/x86_64/ius-release-1.0-14.ius.centos6.noarch.rpm
rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
sed -i -e "s/enabled = 1/enabled = 0/g" /etc/yum.repos.d/remi.repo
echo "include_only=.jp,.st
" >> /etc/yum/pluginconf.d/fastestmirror.conf
yum install -y gcc wget
yum -y update --enablerepo=rpmforge,epel,remi,remi-php55

ln -l 
mkdir /home/prod_vote
mkdir /home/prod_vote/storage
mkdir /home/prod_vote/storage/logs
mkdir /home/prod_vote/storage/cache
mkdir /home/prod_vote/storage/views

chmod 777 /home/prod_vote/storage
chmod 777 /home/prod_vote/storage/*
