<?php

class CategoryParentsSeeder extends Seeder
{
    
    public function run()
    {
        if (Schema::hasTable('category_parents')) {
            DB::table('category_parents')->truncate();

            $arrCategories = [
                [
                    'category_parent_name' => '日常生活',
                    'parent_is_visible' => 1,
                    'category_parent_directory' => 'Everyday',
                    'parent_sort_order' => 10
                ],
                [
                    'category_parent_name' => '話題の出来事',
                    'parent_is_visible' => 2,
                    'category_parent_directory' => 'Topic of the event',
                    'parent_sort_order' => 20
                ]
            ];

            DB::table('category_parents')->insert($arrCategories);
        }
        if (Schema::hasTable('genre_parents')) {
            DB::table('genre_parents')->truncate();

            $arrCategories = [
                [
                    'genre_parent_name' => '日常生活',
                    'parent_is_visible' => 1,
                    'genre_parent_directory' => 'everyday',
                    'parent_sort_order' => 10
                ],
                [
                    'genre_parent_name' => '話題の出来事',
                    'parent_is_visible' => 1,
                    'genre_parent_directory' => 'news',
                    'parent_sort_order' => 20
                ],
                [
                    'genre_parent_name' => '隠しジャンル',
                    'parent_is_visible' => 0,
                    'genre_parent_directory' => 'hidden',
                    'parent_sort_order' => 20
                ]
            ];

            DB::table('genre_parents')->insert($arrCategories);
        }
    }
}
