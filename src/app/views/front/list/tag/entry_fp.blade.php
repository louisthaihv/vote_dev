@extends('layouts.front_fp')

@section('content')

    {{--Entry--}}
    @if((Route::currentRouteName() == 'list.tag.entry'))
        @include('front.list.tag._shared.entry_fp')
    @endif
    {{--Result--}}
    @if((Route::currentRouteName() == 'list.tag.result'))
        @include('front.list.tag._shared.result_fp')
    @endif

@endsection
