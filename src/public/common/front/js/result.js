$(document).ready(function(){
    $("ul.resulttabs").tabChange({content: ".resulttabs_content", animate: true});
    
    $('.resultTable a.moreAction').click(function(e){
        e.preventDefault();
        $(this).closest('.resultTable').find('table tr.otherRanking').show();
        $(this).hide();
    });

    $(".pushStateAction").click(function(e) {
        var nextPage = basePath + $(this).attr('url');
        if (window.location.href != nextPage && history && history.pushState) {
            window.history.pushState(null, null, nextPage);
        }
        updateLink($(this).attr('url'));
    });
    
    $('.profileChoice table tr').click(function(){
        $(this).addClass('active').siblings().removeClass('active');
        var resultBox = $(this).closest('.resultBox');
        $('.rankingTitle',resultBox).text($(this).attr('labelname'));
        $('.rankingCount',resultBox).text($(this).attr('rankingcount'));
        if (!firstOpen) {
            $(this).closest('ul.prefOpen').find('p span').click();
        }
        $('html,body').scrollTop($('.resultTabArea').eq(0).offset().top);

        var url = $(this).find('a.reloadRanking').attr('href');
        var $target = $(this);

        var nowAttribute = $('.resulttabs li.active')[0].id.replace('attribute_', '');
        var avg = $('#attributeTab_' + nowAttribute + ' .avgData');
        if (avg.size() > 0) {
            avg.text($(this).find('.avgValue').text());
        }
        var currentData = summaryData[nowAttribute][$(this).attr('id').replace('attribute_value_', '')];
        $('#attributeTab_' + nowAttribute + ' .resultTable table tr').remove();
        var forI = 0;
        var otherFlg = false;
        for (var element in currentData) {
            if (element == 0 && currentData[element]['choice_num'] == 0) {
                otherFlg = true;
                continue;
            }
            $('#attributeTab_' + nowAttribute + ' .resultTable table').append(function () {
                currentRow = currentData[element];
                var rowData = '<tr class="rankNo' + currentRow['ranking'];
                if (answeredId == currentRow['choice_id']) {
                    rowData += ' my';
                }
                if (isAverage != 1 && forI >= 10) {
                    rowData += ' otherRanking';
                }
                rowData += '"><td class="rank"><span>';
                if (currentRow['choice_num'] == 0) {
                    rowData += '-';
                } else {
                    rowData += currentRow['ranking'];
                }
                rowData += '</span></td><td class="answer">' + currentRow['choice_text'] + '<div class="barArea"><span style="width:' +
                    currentRow['bar_width'] + '%;max-width:100%;"></span></div></td><td class="data">' +
                    currentRow['summary_count'] + '票<br />' + currentRow['rate'] + '% </td> </tr>';
                return rowData;
            });
            forI++;
        }
        if (otherFlg) {
            $('#attributeTab_' + nowAttribute + ' .resultTable table').append(function () {
                currentRow = currentData[0];
                var rowData = '<tr class="rankNo' + currentRow['ranking'];
                if (answeredId == currentRow['choice_id']) {
                    rowData += ' my';
                }
                if (isAverage != 1 && forI >= 10) {
                    rowData += ' otherRanking';
                }
                rowData += '"><td class="rank"><span>';
                if (currentRow['choice_num'] == 0) {
                    rowData += '-';
                } else {
                    rowData += currentRow['ranking'];
                }
                rowData += '</span></td><td class="answer">' + currentRow['choice_text'] + '<div class="barArea"><span style="width:' +
                currentRow['bar_width'] + '%;max-width:100%;"></span></div></td><td class="data">' +
                currentRow['summary_count'] + '票<br />' + currentRow['rate'] + '% </td> </tr>';
                return rowData;
            });
        }
        if ($('#attributeTab_' + nowAttribute + ' .moreAction').size() > 0) {
            $('#attributeTab_' + nowAttribute + ' .moreAction').show();
        }
    });

    $(".prefOpen p span").click(function(){
        $(this).closest('p').next(".profileChoice").slideToggle();
        $(this).toggleClass("open");
        return false;
    });

    $('.columnBlock a.nextNavi').click(function(){
        $('.columnBox').css('height','auto');
        $(this).hide();
        return false;
    });

    var hash = '';
    if(location.hash == '#column'){
        $('.columnBox').css('height','auto');
        $('.columnBlock a.nextNavi').hide();
        hash = '#column';
    }
    
    $('.evaluatePost li').live('click',function(){
        $('.evaluateBlock ul').removeClass('evaluatePost');
        //Ajax送信＋カウント更新

        var $target = $(this);
        var url = $target.find('input.voteUrl').val();
        
        $.post(url, function(data){
            $target.find('span.counter').text(data.count);
        });
    });

    var firstOpen = true;
    if (attribute > 0) {
        $('#attribute_' + attribute).click();
        $('#attribute_value_' + attributeValue).click();
    } else {
        if (history && history.replaceState) {
            window.history.replaceState(null, null, location.href.replace(location.search, ""));
        }
    }
    firstOpen = false;
    if (hash != '') {
        location.hash = hash;
    }
    if ($('.columnBox').size() > 0) {
        if ($('.columnBox').height() - 15 > $('.columnBox .columnTit').height() + $('.columnBox .columnContents').height()) {
            $('.columnBlock a.nextNavi').click();
        }
    }
});
