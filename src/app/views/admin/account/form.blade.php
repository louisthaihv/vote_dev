@section('script')
<script type="text/javascript">
$(window).load(function(){
    $('.commitBtn').click(function(){
        return (confirm('{{{ $confirmMessage }}}'));
    });
});
</script>
<script type="text/javascript" src="{{ url('common/admin/js/generate_password.js') }}"> </script>
@stop

@include('admin.share.errors', ['errorList' => $account->errors()])
{{ Form::model($account, ['url' => $url, 'method' => $method]) }}
<table class="formTable">
    <tr>
        <th>アカウント名<span class="require">（必須）</span></th>
        <td>{{ Form::text('account_name', $account->account_name, ['class' => 'xx-large']) }}
        </td>
    </tr>
    <tr>
        <th>アカウントID<span class="require">（必須）</span></th>
        <td>{{ Form::text('login_id', $account->login_id, ['class' => 'large']) }}
        </td>
    </tr>
    <tr>
        <th>パスワード<span class="require">（必須）</span></th>
        <td>{{ Form::text('login_password', $account->get, ['class' => 'large']) }}
        <a href="#" class="naviBtn" onClick="populateform({{{ LEN_PASSWORD }}})">自動生成</a>
        </td>
    </tr>
    <tr>
        <th>投稿許可大ジャンル</th>
        <td>
            {{ Form::selectByTable('allow_genre_parent', $genres, 'id', 'genre_parent_name', $account->allow_genre_parent, true) }}
        </td>
    </tr>
    <tr>
        <th>管理権限</th><td>
            <ul>
                @include("admin.account.elements.checkbox", array("permission" => "is_visible_register", "label" => "アンケートの登録者表示", 'options' => ['title' => 'アンケート検索結果に登録者を表示する']))
                @include("admin.account.elements.checkbox", array("permission" => "is_manage_genre", "label" => "ジャンル管理権限", 'options' => ['title' => 'ジャンルマスターを管理する']))
                @include("admin.account.elements.checkbox", array("permission" => "is_manage_keyword", "label" => "キーワード管理権限", 'options' => ['title' => 'キーワードマスターを管理する']))
                @include("admin.account.elements.checkbox", array("permission" => "is_manage_account", "label" => "アカウント管理権限", 'options' => ['title' => 'アカウントを管理する']))
                @include("admin.account.elements.checkbox", array("permission" => "is_manage_notice", "label" => "お知らせ管理権限", 'options' => ['title' => 'お知らせを管理する']))
                @include("admin.account.elements.checkbox", array("permission" => "is_manage_feature", "label" => "特集枠管理権限", 'options' => ['title' => 'TOPの特集枠を管理する']))
                @include("admin.account.elements.checkbox", array("permission" => "is_manage_attention", "label" => "注目キーワード管理権限", 'options' => ['title' => 'TOPの注目キーワードを管理する']))
                @include("admin.account.elements.checkbox", array("permission" => "is_manage_pickup", "label" => "ピックアップ権限", 'options' => ['title' => 'TOPのピックアップを管理する']))
                @include("admin.account.elements.checkbox", array("permission" => "is_manage_agree", "label" => "承認権限（即時反映）", 'options' => ['title' => '承認申請中のアンケートを公開する権限']))
                @include("admin.account.elements.checkbox", array("permission" => "is_manage_input_comment", "label" => "準備中以降更新権限（結果ページコメント入力付）", 'options' => ['title' => '結果ページのコメントを入力できる権限。受付準備中以降のアンケートも更新できる']))
                @include("admin.account.elements.checkbox", array("permission" => "is_manage_column", "label" => "コラム入力権限", 'options' => ['title' => 'コラムを編集できる権限（表示は承認権限）']))
                @include("admin.account.elements.checkbox", array("permission" => "is_manage_summary", "label" => "集計確認権限", 'options' => ['title' => '集計データが参照できる権限']))
                @include("admin.account.elements.checkbox", array("permission" => "is_viewable_all_question", "label" => "全アンケートの閲覧権限", 'options' => ['title' => '自分以外のアンケート編集画面を閲覧できる権限']))
            </ul>
        </td>
    </tr>
    <tr>
        <th>クローズ</th><td><label><input type="checkbox" {{ ($button == UPDATE && $account->is_enabled != ENABLED) ? 'checked="checked"' : '' }} name="is_enabled" /> クローズにする</label></td>
    </tr>
    <tr>
        <th>コメント</th><td>{{ Form::textarea('comment', $account->comment, ['cols' => 50, 'rows' => 5]) }}</td>
    </tr>
</table>
<div class="tcenter m-margint">
<button type="submit" name="commit" class="commitBtn">{{ $button }}</button>
</div>
{{ Form::close() }}
