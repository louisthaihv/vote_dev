<?php

namespace docomo\Models\Questions;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;

class Choice extends \BaseArdentModel
{

    protected $table = 'choices';

    /*
     * Acceptable params from input
     */
    protected $fillable = [
        'question_id',
        'choice_num',
        'choice_text',
        'summary_value'
    ];

    /*
     * Validation rules
     */
    public static $rules = [
        'choice_text' => ['required', 'max:60', 'deny_tag', 'deny_char'],
        'summary_value' => ['sometimes', 'required', 'numeric', 'max:9999999999']
    ];

    /*
     * Custom validation messages
     */
    public static $customMessages = array(
        'choice_text.required' => W_FRM_REQUIRED_CHOICES,
        'choice_text.max' => W_FRM_MAX_CHOICES,
        'choice_text.deny_tag' => W_FRM_DENY_TAG_CHOICES,
        'choice_text.deny_char' => W_FRM_DENY_CHAR_CHOICES,
        'summary_value.numeric' => W_FRM_FORMAT_SUMMARY_VALUE,
        'summary_value.required' => W_FRM_REQUIRED_SUMMARY_VALUE,
        'summary_value.max' => W_FRM_MAX_SUMMARY_VALUE
    );
    /*
     * Model relationships
     */

    function question()
    {
        return $this->belongsTo('docomo\Models\Questions\Question');
    }

    function answers()
    {
        return $this->hasMany('docomo\Models\Answers\Answer');
    }

    public function scopeWithOutOther($query)
    {

        return $query->where('choice_num', '>', OTHER_CHOICE_NUM);
    }

    public static function fillWithModify($question)
    {
        $choices = [];

        if (!Input::has('choice_text')) {

            return $choices;
        }
        $db_choices = [];
        if (isset($question->id)) {
            $db_choices = Choice::whereQuestionId($question->id)->get(['id', 'question_id', 'choice_num', 'choice_text', 'summary_value']);
        }
        foreach (Input::get('choice_text', []) as $key => $value)
        {
            $choice = null;
            foreach ($db_choices as &$db_choice) {
                if ($db_choice->choice_num == $key) {
                    $choice = $db_choice;
                    $db_choice->is_exists = true;
                    break;
                }
            }
            unset($db_choice);
            if (!isset($choice)) {
                $choice = new Choice;
                $choice->question_id = $question->id;
                $choice->choice_num = $key;
            }
            $choice->choice_text = $value;
            if ($question->summary_type == SUMMARY_AVERAGE)
            {
                $choice->summary_value = \Input::get('summary_value')[$key];
            }
            else
            {
                unset($choice->summary_value);
            }

            $choices[] = $choice;
        }
        if ($question->summary_type != SUMMARY_AVERAGE)
        {
            unset(Choice::$rules['summary_value']);
        }
        $choice = null;
        if (isset($db_choices) && count($db_choices)) {
            foreach ($db_choices as &$db_choice) {
                if ($db_choice->choice_num == OTHER_CHOICE_NUM) {
                    $choice = $db_choice;
                    break;
                }
            }
        }
        unset($db_choice);
        if ($question->is_visible_other) {
            if (is_null($choice)) {
                $choice = new Choice;
                $choice->question_id = $question->id;
                $choice->choice_num = 0;
            }
            $choice->choice_text = $question->other_text;
            $choices[] = $choice;
            unset($choice);
        } else if ($choice != null) {
            $choice->delete();
        }

        if (isset($db_choices)) {
            foreach ($db_choices as $db_choice) {
                if (!$db_choice->is_exists) {
                    unset($db_choice->is_exists);
                    $db_choice->delete();
                } else {
                    unset($db_choice->is_exists);
                }
            }
        }

        return $choices;
    }

    public static function validateAndSave($allowQuestion, $choices, $summaryType)
    {
        $errors = new MessageBag();

        if (count($choices) == 0) {
            $errors->add('choice_error', W_FRM_REQUIRED_CHOICES);
        } else if ($allowQuestion) {
            $original_messages = Choice::$customMessages;
            $row = 1;
            foreach ($choices as $choice) {

                Choice::$customMessages = [];

                foreach ($original_messages as $key => $value)
                {
                    if ($choice->choice_num === OTHER_CHOICE_NUM) {
                        Choice::$customMessages[$key] = 'その他の' . $value;
                    } else {
                        Choice::$customMessages[$key] = $row . '行目の' . $value;
                    }
                }
                if (!$choice->save()) {
                    $errors->merge($choice->errors());
                }
                $row ++;
            }
        } else {
            $empty_check = false;
            $row = 1;
            foreach ($choices as $choice) {
                if (!isset($choice->choice_text) || empty($choice->choice_text)) {
                    $errors->add('choice_empty' . $row, $row . '行目の' . W_FRM_REQUIRED_CHOICES);
                }
                if (mb_strlen($choice->choice_text) > 60) {
                    $errors->add('choice_empty' . $row, $row . '行目の' . W_FRM_MAX_CHOICES);
                }
                if ($summaryType == SUMMARY_AVERAGE) {
                    if (!isset($choice->summary_value) && empty($choice->summary_value)) {
                        $errors->add('choice_summary_empty' . $row, $row . '行目の' . W_FRM_REQUIRED_SUMMARY_VALUE);
                    } else if($choice->summary_value > 9999999999 || strlen($choice->summary_value) > 10) {
                        $errors->add('choice_summary_over' . $row, $row . '行目の' . W_FRM_MAX_SUMMARY_VALUE);
                    }
                }
                $row++;
            }
            if (!$empty_check) {
            }
        }

        return $errors;
    }
}
