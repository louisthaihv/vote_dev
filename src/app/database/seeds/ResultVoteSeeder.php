<?php
class ResultVoteSeeder extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('result_votes')->truncate();
        
        
        for($i = 1; $i <= 30; $i++) {
            $resultVotes[] = 
                [
                    'id'                    => $i,
                    'question_id'           => $i,
                    'attribute_id'          => $i,
                    'attribute_value_id'    => $i,
                    'device'                => $i,
                    'choice_id'             => $i,
                    'ranking'               => $i,
                    'voted_count'           => $i,
                    'summary_value'         => $i,         
                ];
            
        }
        DB::table("result_votes")->insert($resultVotes);
    }
}