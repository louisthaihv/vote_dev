<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePickups extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('pickups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_id');
            $table->integer('type');
            $table->dateTime('from');
            $table->dateTime('to');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('pickups');
	}
}
