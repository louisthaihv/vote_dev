@extends('layouts.admin_in')

@section('script')
<script type="text/javascript">
$(window).load(function(){

    $('#searchBtn').click(function() {
        if ($('#hide_show').prop("checked")) {
            $(location).attr('href', "{{ route('manage.keyword.index', SHOW_ALL) }}");
        } else {
            $(location).attr('href', "{{ route('manage.keyword.index') }}");
        }
    });
});
</script>
@stop
@section('content')
<h2>キーワード一覧</h2>
<p class="m-margin">
<a href="{{ route('manage.keyword.create') }}" class="naviBtn">＋キーワード追加</a>
</p>
<form name="form1" method="post" action="">
    {{ Form::token() }}
    <label><input type="checkbox" id="hide_show" name="hide_show" value="" {{ ($show_all) ? 'checked="checked"' : '' }}/> 非表示も見る</label>
    <a class="naviBtn" id="searchBtn" />再検索</a>
    <table class="list">
        <tr>
            <th>ID</th><th>キーワード名</th><th>非表示</th>
        </tr>
        @foreach($keywords as $keyword)
            <tr>
                <td>{{ $keyword->id }}</td>
                <td>
                    {{ link_to_route('manage.keyword.edit', $keyword->keyword_name, $keyword->id) }}
                <td>{{ ($keyword->is_visible == VISIBLE) ? '' : L_HIDDEN }}</td>
            </tr>
        @endforeach
    </table>
</form>
@stop
