<?php

return [
    'listeners' => [
        'Listeners\EmailNotifier',
        'Listeners\ReportNotifier',
    ]
];