<script type="text/javascript">
    $(document).ready(function() {
        
        var params = {};
        function load(elem, numFirstPage, numOfPage, pager, url, params) {
            elem.voteList({
                className    : elem.attr('id'),
                numFirstPage : numFirstPage,
                numOfPage    : numOfPage,
                pager        : pager,
                url          : url,
                params       : params   
            });
        }
        @foreach($classNames as $key => $class)
            @if(isset($params))
                params = {{json_encode($params[$key])}};
            @endif
            @if(isset($urls) && $urls[$key] != '')
                url = "{{ route($urls[$key]) }}";
            @else
                url = "{{ route('list.more') }}";
            @endif
            load(   
                    $("#{{ $class }}"), // id of ul element
                    "{{$nums[$key]}}" , // num of question in the first load
                    "{{$pages[$key]}}", // num of question in next pages when click "see more" 
                    0,                  // pager
                    url, // call to server
                    params
                );
        @endforeach
    });

</script>