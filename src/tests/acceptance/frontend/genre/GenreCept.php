<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('check genre');

$arr_entry = array(
                'link_for_showmore' => 'もっと見る',
                'xpath_format' => 'html/body/section[2]/div[2]/ul/li[%d]/a'
                );

$arr_result = array(
                'link_for_showmore' => 'html/body/section[2]/a/span',
                'xpath_format' => 'html/body/section[2]/div[2]/ul/li[%d]/a'
                );

$arr_column = array(
                'link_for_showmore' => 'html/body/section[2]/div[3]/a/span',
                'xpath_format' => 'html/body/section[2]/div[3]/div/ul/li[%d]/a'
                );

$genre_parent = array(
                    1 => array(
                                'genre_parent_name' => 'genre_1',
                                'parent_is_visible' => '1',
                                'genre_parent_directory' => 'everyday',
                                'parent_sort_order' => '10',
                                ),
                    2 => array(
                                'genre_parent_name' => 'genre_2',
                                'parent_is_visible' => '1',
                                'genre_parent_directory' => 'news',
                                'parent_sort_order' => '10',
                                ),
                    3 => array(
                                'genre_parent_name' => 'genre_3',
                                'parent_is_visible' => '0',
                                'genre_parent_directory' => 'news',
                                'parent_sort_order' => '10',
                                )
                );
$I->createDatabaseGenreParent($genre_parent);

$genre_child = array(
                    1 => array(
                                'genre_parent_id'=>'1',
                                'genre_child_name'=>'genre_child_1_1',
                                'child_is_visible'=>'1',
                                'genre_child_directory'=>'1',
                                'child_sort_order'=>'10'
                                ),
                    2 => array(
                                'genre_parent_id'=>'1',
                                'genre_child_name'=>'genre_child_1_2',
                                'child_is_visible'=>'1',
                                'genre_child_directory'=>'2',
                                'child_sort_order'=>'20'
                                ),
                    3 => array(
                                'genre_parent_id'=>'2',
                                'genre_child_name'=>'genre_child_2_1',
                                'child_is_visible'=>'1',
                                'genre_child_directory'=>'3',
                                'child_sort_order'=>'10'
                                ),
                    4 => array(
                                'genre_parent_id'=>'2',
                                'genre_child_name'=>'genre_child_2_2',
                                'child_is_visible'=>'1',
                                'genre_child_directory'=>'4',
                                'child_sort_order'=>'20'
                                ),
                );

$I->createDatabaseGenreChild($genre_child);


//create survey entry
$survies_entry = array();
for ($i=1; $i < 12; $i++) {
    $title = 'title_'.$i;
    $date = '2014-12-'.($i+10).' 00:00:00';
    $survies_entry[$i] = array(
                        'genre_parent_id'=>'1',
                        'genre_child_id'=>'1',
                        'status'=>'5',
                        'title'=>$title,
                        'vote_date_from'=>$date,
                        'vote_date_to'=>'2015-01-10 00:00:00',
                        'baseline_date'=>$date,
                        'viewable_type'=>'1',
                        'visible_device_i'=>'1',
                        'visible_device_d'=>'1',
                        'summary_type'=>'1',
                        'average_unit'=>'1',
                        'is_visible_other'=>'1',
                        'shuffle_choice'=>'1',
                        'navigation_title'=>'1',
                        'account_id'=>'1',
                        );
}
$I->createDatabaseSurvey($survies_entry);

//after create output 11 items entry
$survies_entry_check = array();
foreach ($survies_entry as $survey) {
    $jpdate = $I->dateToJpdate($survey['baseline_date']);
    $survies_entry_check[] = array('title' => $survey['title'], 'jpdate' => $jpdate, 'status' => '1');
}
//order list to see
krsort($survies_entry_check);
$arr_entry['items_check'] = $survies_entry_check;

//create survey result
$survies_result = array();
for ($i=1; $i < 12; $i++) {
    $title = 'title_'.($i+20);
    $date = '2014-12-'.($i+10).' 00:00:00';
    $survies_result[$i] = array(
                        'genre_parent_id'=>'1',
                        'genre_child_id'=>'1',
                        'status'=>'6',
                        'title'=>$title,
                        'vote_date_from'=>'2014-12-05 00:00:00',
                        'vote_date_to'=>$date,
                        'baseline_date'=>$date,
                        'viewable_type'=>'1',
                        'visible_device_i'=>'1',
                        'visible_device_d'=>'1',
                        'summary_type'=>'1',
                        'average_unit'=>'1',
                        'is_visible_other'=>'1',
                        'shuffle_choice'=>'1',
                        'navigation_title'=>'1',
                        'account_id'=>'1',
                        );
}
$I->createDatabaseSurvey($survies_result);

//after create output 11 items result
$survies_result_check = array();
foreach ($survies_result as $survey) {
    $jpdate = $I->dateToJpdate($survey['baseline_date']);
    $survies_result_check[] = array('title' => $survey['title'], 'jpdate' => $jpdate, 'status' => '1');
}
//order list to see
krsort($survies_result_check);
$arr_result['items_check'] = $survies_result_check;

//create columns
$columns = array();
for ($i=1; $i <=11; $i++) {
    $title = 'column_title_'.$i;
    $date = '2014-12-'.($i+10).' 00:00:00';
    $columns[$i] = array(
                        'question_id' => $i,
                        'is_visible' => '1',
                        'column_title' => $title,
                        'column_detail' => 'detail',
                        'view_create_datetime' => $date,
                        'created_account_id' => '1',
                        'updated_account_id' => '1',
                        );
}
$I->createColumns($columns);

//after create output 11 items
$columns_check = array();
foreach ($columns as $column) {
    $jpdate = $I->dateToJpdate($column['view_create_datetime']);
    $columns_check[] = array('title' => $column['column_title'], 'jpdate' => $jpdate, 'status' => '1');
}
//order list to see
krsort($columns_check);
$arr_column['items_check'] = $columns_check;


//check genre list
$I->checkGenreSP9($genre_parent, $genre_child);

//check child entry
$I->click($genre_child[1]['genre_child_name']);
$I->wait(3);
$I->seeCurrentUrlEquals('/list/genre/everyday/1/result');
$I->checkGenreSP7_1($arr_result, $arr_column);

$I->click('html/body/section[2]/div[1]/ul/li[1]/a');
$I->wait(3);
$I->seeCurrentUrlEquals('/list/genre/everyday/1/entry');
$I->checkGenreSP7($arr_entry);

