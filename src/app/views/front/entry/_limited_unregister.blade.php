<div class="restraintInfo l-margin">
    <p>このアンケートは</p>
    <p>{{{ $question->getLimitedText() }}}限定です。</p>
    <p class="m-margin">プロフィールに{{{ $question->getLimitedLabel() }}}を登録しますか？</p>

    @if (isset($currentUser->result) && !$currentUser->result)
        <p class="profileErrorMsg">生年月日が正しくありません。</p>
    @endif
    {{ Form::open(['url' => $question->getPath(), 'method' => 'put']) }}
        <div class="s-margin">
            @if($question->attribute_id == ATTR_BIRTHDAY)
                {{ Form::birthday((isset($currentUser) ? $currentUser->birthday : null)) }}
            @else
                {{ Form::attributeValueList($question->attribute_id, array('default' => $question->attribute_value_id), $question->getLimited()->attribute_directory) }}
            @endif
        </div>
        <p><button type="submit" class="btnType03">登録する</button></p>
    {{ Form::close() }}
</div>


