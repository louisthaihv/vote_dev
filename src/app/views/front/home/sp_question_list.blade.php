
@if (array_key_exists($listKeyName, $summaryData))
    <div class="links">
        <ul>
            <?php $idx = 1; ?>
            @foreach ($summaryData[$listKeyName] as &$question)
                @include('layouts.elements.displays.list_box', ['question' => $question, 'linkId' => $idHeader . sprintf('%1$02d', $idx++), 'hide_answer' => (isset($hide_answer) ? $hide_answer : null)])
            @endforeach
        </ul>
    </div><!--/lrMargin-->
@endif