<?php

Validator::extend('alpha_spaces', function($attribute, $value)
{
    return preg_match('/^[\pL\s]+$/u', $value);
});

Validator::extend('allow_tag', function($attribute, $value)
{
    return
        !strstr(mb_strtolower($value), '<script') &&
        !preg_match("/<(.+)\son([^=]+)=([^>]+)>/", mb_strtolower($value));
});

Validator::extend('deny_char', function ($attribute, $value) {
    if ($value === NULL)
        return FALSE;

    // UTF-16へ変換し、一文字ずつチェックを行う
    $str_utf16 = mb_convert_encoding($value, "UTF-16", "UTF-8");
    $str_length = mb_strlen($str_utf16, 'UTF-16');

    for ($i = 0; $i < $str_length; $i++) {
        $chars = str_split(bin2hex(mb_substr($str_utf16, $i, 1, 'UTF-16')), 4);

        // ペア文字では無い
        if (count($chars) != 2)
            continue;

        // サロゲートペア文字
        if (preg_match('/[d800-dbff]/', $chars[0]) !== false
            && preg_match('/[dc00-dfff]/', $chars[1]) !== false)
            return false;
    }
    return !platform_dependent_characters_filter($value);
});

Validator::extend('deny_tag', function($attribute, $value)
{
    return strip_tags($value) === $value;
});

Validator::extend('alpha_num_utf8', function($attribute, $value)
{
    return preg_match('/^[a-zA-Z0-9]+$/u', $value);
});
Validator::extend('directory', function($attribute, $value)
{
    return preg_match('/^[a-z0-9\-\_]+$/u', $value);

});
Validator::extend('datetime_format', function($attribute, $value)
{
    if (is_null($value)) {
        return true;
    }

    return strlen(explode(' ', $value)[0]) === 10;
});
