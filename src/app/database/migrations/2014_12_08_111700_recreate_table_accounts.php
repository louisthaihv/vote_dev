<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RecreateTableAccounts extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('accounts');
        Schema::create('accounts', function(Blueprint $table) {
            $table->increments('id');
            $table->string('account_name', 60)->unique();
            $table->string('login_id', 60)->unique();
            $table->string('login_password');
            $table->integer('allow_genre_parent');
            $table->boolean('is_visible_register');
            $table->boolean('is_manage_genre');
            $table->boolean('is_manage_keyword');
            $table->boolean('is_manage_account');
            $table->boolean('is_manage_notice');
            $table->boolean('is_manage_feature');
            $table->boolean('is_manage_pickup');
            $table->boolean('is_manage_attention');
            $table->boolean('is_manage_agree');
            $table->boolean('is_manage_input_comment');
            $table->boolean('is_manage_column');
            $table->boolean('is_manage_summary');
            $table->boolean('is_viewable_all_question');
            $table->boolean('is_enabled');
            $table->integer('created_account_id');
            $table->string('remember_token')->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }

}
