@extends('layouts.admin_in')

@section('head')
<link rel="stylesheet" href="{{ url('common/admin/css/jquery-ui-1.8.23.custom.css') }}" type="text/css" />
<script type="text/javascript" src=" {{ url('common/admin/js/jquery-ui-1.8.23.custom.min.js') }}"></script>
<script type="text/javascript" src=" {{ url('common/admin/js/jquery.ui.datepicker-ja.js') }}"></script>
@stop

@section('javascript')
$(window).load(function(){

    //日付ピッカー
    $("input[name='date_from']").datepicker({
    dateFormat: 'yymmdd'
    });
    //日付ピッカー
    $("input[name='date_to']").datepicker({
    dateFormat: 'yymmdd'
    });

    $('button#search').click(function(){
    $('#mode').val('search');
    return true;
    });
    $('button#download').click(function(){
    $('#mode').val('download');
    document.forms[0].submit();
    });
});
@stop

@section('style')
@stop

@section('content')
<h2>プロフィール別得票数</h2>

{{ Form::model('conditions', ['method' => 'post']) }}
<input type="hidden" id="mode" name="mode">
<table class="searchQuery">
    <tr>
        <th>投票日</th>
        <td>
            {{ Form::text('date_from', $conditions->date_from) }}
            ～
            {{ Form::text('date_to', $conditions->date_to) }}
        </td>
    </tr>
</table>
<p class="m-margin tcenter">
    <button type="submit" id="search" class="commitBtn">検索</button>
    <button type="button" id="download" class="commitBtn">CSV</button>
</p>
{{ Form::close() }}
@if (isset($summaryData) && !is_null($summaryData))
@foreach ($attributes as &$attribute)
<table class="list l-margin" style="margin-top:0px;">
    <tr><th width="100">{{ ($attribute->attribute_name == '性別') ? $attribute->attribute_name : str_replace('別', '', $attribute->attribute_name) }}</th><th>票数</th></tr>
    <?php $total = 0; ?>
    @foreach ($attributeValues[$attribute->id] as $values)
        <?php $data = (array_key_exists($attribute->id, $summaryData)
                && array_key_exists($values->id, $summaryData[$attribute->id]))
                ? $summaryData[$attribute->id][$values->id] : 0; ?>
    <tr>
        <td>{{ $values->attribute_value_name }}</td>
        <td>{{ number_format($data) }}</td>
    </tr>
            <?php $total += $data;
            unset($data); ?>
    @endforeach
    <tr><td>合計</td><td>{{ number_format($total) }}</td></tr>
</table>
@endforeach
@endif
@stop