@extends('layouts.admin_in')

@section('style')
  {{ HTML::style('asset/css/admin/pickup.css') }}
@stop

@section('content')
<h2>TOPページピックアップデータ一覧</h2>
<table class="list">
    <tr>
        <th>アンケートID</th>
        <th>タイトル</th>
        <th>受付中/結果</th>
        <th style="width:90px;">開始</th>
        <th style="width:90px;">終了</th>
        <th>削除</th>
    </tr>
    @foreach($pickups as $pickup)
        <tr class="{{ ($pickup->isEnd()) ? 'expiration' : '' }}">
        <td>
            {{ $pickup->question_id }}
        </td>
        <td>
            {{ link_to_route('manage.pickup.edit', $pickup->question->title, ['pickup' => $pickup->id]) }}
        </td>
        <td>
            {{ $typeList[$pickup->type] }}
        </td>
        <td>
            {{ date('Y-m-d H:i', strtotime($pickup->from)) }}
        </td>
        <td>
            {{ date('Y-m-d H:i', strtotime($pickup->to)) }}
        </td>
        <td>
            {{ Form::model($pickup, ['route' => ['manage.pickup.destroy', 'pickup' => $pickup->id], 'method' => 'delete', 'class' => 'deleteForm']) }}
                {{ Form::submit('削除', ['class' => 'naviBtn']) }}
            {{ Form::close() }}
        </td>
    </tr>
    @endforeach

</table>
@stop

