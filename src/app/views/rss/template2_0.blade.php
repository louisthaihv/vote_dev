<?xml version='1.0' encoding='UTF-8'?>
<rss version='2.0'>
    <channel>
        <title>{{ $rssTitle }}</title>
        <link>{{ $siteURL }}</link>
        <description>{{ $siteDescription }}</description>
        @foreach ($recordData as &$record)
        <item>
            <title>{{ $record->title }}</title>
            <link>{{ $siteURL . $record->url }}</link>
            <description>{{ $record->description }}</description>
            <pubDate>{{ $record->vote_date_from }}</pubDate>
        </item>
        @endforeach
    </channel>
</rss>