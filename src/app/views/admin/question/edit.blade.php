@extends('layouts.admin_in')

@section('script')
<script type="text/javascript">
    $(window).load(function(){

        $('button.btnUpdateData').click(function(){
            if(confirm('{{ '' }}')){
                $('#status').val('{{''}}');
                $('#form').submit();
            }
        });

        function btnChangeStatus(obj, msg){
            obj.click(function(e) {
                return confirm(msg);
            });
        }

        btnChangeStatus($('.btnReject'), '{{ C_REJECT }}');
        btnChangeStatus($('.btnRequestCancel'), '{{ C_REQUEST_CANCEL }}');

        $('button.btnDeleteSurvey').click(function(){
            return (confirm('{{ C_DELETE }}'));
        });
    });
</script>
@stop

@section('content')
<h2>アンケート登録</h2>
@include('admin.question.view_parts.information')
<div class="tright">
    @include('admin.question.view_parts.header_buttons')
</div>
@include('admin.question.form', ['url' => 'manage/question/'. $question->id . '/edit', 'method' => 'put', 'mode' => $mode])
@stop