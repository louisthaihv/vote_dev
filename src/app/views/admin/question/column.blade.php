@extends('layouts.admin_in')

@section('head')
<link rel="stylesheet" href="{{ url('common/admin/css/jquery-ui-1.8.23.custom.css') }}" type="text/css" />
<script type="text/javascript" src=" {{ url('common/admin/js/jquery-ui-1.8.23.custom.min.js') }}"></script>
<script type="text/javascript" src=" {{ url('common/admin/js/jquery.ui.datepicker-ja.js') }}"></script>
@stop

@section('javascript')
$(window).load(function(){

    //日付ピッカー
    $("input[name='date_start']").datepicker({
        dateFormat: 'yymmdd'
    });
    //日付ピッカー
    $("input[name='date_end']").datepicker({
        dateFormat: 'yymmdd'
    });
});

function setOrder(column) {
    if ($('#sortColumn').val() == column) {
        if ($('#sortOrder').val() == 'asc') {
            $('#sortOrder').val('desc');
        } else {
            $('#sortOrder').val('asc');
        }
    } else {
        $('#sortColumn').val(column);
        $('#sortOrder').val('asc');
    }
    document.forms[0].submit();
    return false;
}

var keyword_name = $("#keyword_name").val();
$("#keywordList").text(keyword_name);

@stop

@section('style')
    <style type="text/css">
        .pagination li {
            display: inline;
            padding-left:4px;
            padding-right:4px;
        }
    </style>
@stop

@section('content')
<h2>コラム検索</h2>

{{ Form::model('conditions', ['method' => 'post']) }}
<input type="hidden" id="sortColumn" name="sortColumn" value="{{{ $conditions->sort_column }}}" />
<input type="hidden" id="sortOrder" name="sortOrder" value="{{{ $conditions->sort_order }}}" />
<table class="searchQuery">
    <tr>
        <th>コラム作成日</th>
        <td>
            {{ Form::text('date_start', $conditions->date_start) }}
            ～
            {{ Form::text('date_end', $conditions->date_end) }}
        </td>
    </tr>
@if (Auth::user()->havePermission('is_visible_register'))
    <tr>
        <th>コラム作成者</th>
        <td>
            {{ Form::selectByTable('register', $accounts, 'id', 'account_name', $conditions->register, true) }}
        </td>
    </tr>
@endif
    <tr>
        <th>表示フラグ</th>
        <td>
            {{ Form::select('visible', $visible, $conditions->visible) }}
        </td>
    </tr>
    <tr>
        <th>アンケートID絞り込み</th>
        <td>
            {{ Form::text('question_id', $conditions->question_id) }}
        </td>
    </tr>
    <tr>
        <th>タイトル・内容絞り込み</th>
        <td>
            {{ Form::text('search_text', $conditions->search_text) }}
        </td>
    </tr>
</table>
<p class="m-margin tcenter">
<button type="submit" class="commitBtn">検索</button>
</p>
@if (isset($columns))
<p class="m-margin">ヒット数：{{ number_format($columns->getTotal()) }}件</p>

<div id="order_tag"></div>
<table class="list">
    <tr>
        <th>ID</th>
        <th>タイトル</th>
        <th>概要</th>
        <th><a href="#" onclick="return setOrder('date');">コラム作成日{{ ($conditions->sort_column == 'date') ? (($conditions->sort_order == 'asc') ? '▲' : '▼') : '' }}</a></th>
        @if (Auth::user()->havePermission('is_visible_register'))
        <th>登録者</th>
        @endif
        <th>表示フラグ</th>
    </tr>
    @foreach ($columns as $column)
    <tr>
        <td>{{ $column->question_id }}</td>
        <td><a href="{{ route('manage.question.result', $column->question_id) }}#edit_column">{{{ $column->column_title }}}</a></td>
        <td>{{ $column->description }}</td>
        <td>{{ date('Y-m-d H:i', strtotime($column->view_create_datetime)) }}</td>
        @if (Auth::user()->havePermission('is_visible_register'))
        <td>{{{ docomo\Models\Accounts\Account::getAccountName($column->created_account_id) }}}</td>
        @endif
        <td>{{ ($column->is_visible) ? L_VISIBLE : L_HIDDEN }}</td>
    </tr>
    @endforeach
</table>
<br />
<div style="text-align: center;">
    @include('admin.question.pagination', ['paginator' => $columns, 'currentPage' => $columns->getCurrentPage(), 'lastPage' => $columns->getLastPage()])
</div>
@endif
{{ Form::close() }}

@stop