@extends('layouts.admin_in')
@section('content')
<h2>TOPページ注目キーワードデータ作成</h2>
@include('admin.attention.form', ['url' => 'manage/attention/' . $attention->id . '/edit', 'method' => 'put', 'confirmMessage' => C_UPDATE, 'attention' => $attention, 'button' => UPDATE])
</form>
@stop