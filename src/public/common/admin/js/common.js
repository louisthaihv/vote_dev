﻿$(function(){
    $('.logoutBtn').click(function(){
        if(confirm('ログアウトします。よろしいですか？')){
            location.href='/manage/logout';
        }
        return false;
    });

    $('.pageTopNavi a').click(function(){
        $('html,body').animate({ scrollTop: 0 }, 300);
        return false;
    });

    $('.deleteForm').submit(function(){
        return confirm('該当データを削除しますか？');
    });
});