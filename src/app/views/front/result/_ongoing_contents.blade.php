<p class="tcenter m-margin">
    {{ link_to_route('entry.show', (isset($btnMessage) ? $btnMessage : 'このアンケートに投票する'), [
               $question->getParentGenreDirectory(),
               $question->getChildGenreDirectory(),
               $question->id
    ], ['class' => 'btnType02'])}}
</p>