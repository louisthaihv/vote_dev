<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableQuestionsGenre extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('questions') &&
			Schema::hasColumn('questions', 'category_parent_id'))
		{
			Schema::table('questions', function ($table)
			{
				$table->dropColumn('category_parent_id');
				$table->dropColumn('category_child_id');
				$table->integer('genre_parent_id')->after('id');
				$table->integer('genre_child_id')->after('genre_parent_id');
				if (Schema::hasColumn('questions', 'detail'))
				{
					$table->renameColumn('detail', 'details');
				}
				if (Schema::hasColumn('questions', 'visible_flag'))
				{
					$table->renameColumn('visible_flag', 'viewable_type');
				}
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('questions') &&
			Schema::hasColumn('questions', 'genre_parent_id'))
		{
			Schema::table('questions', function ($table){
				$table->dropColumn('genre_parent_id');
				$table->dropColumn('genre_child_id');
				$table->integer('category_parent_id')->after('id');
				$table->integer('category_child_id')->after('genre_parent_id');
				if (Schema::hasColumn('questions', 'details'))
				{
					$table->renameColumn('details', 'detail');
				}
				if (Schema::hasColumn('questions', 'viewable_type'))
				{
					$table->renameColumn('viewable_type', 'visible_flag');
				}
			});
		}
	}

}
