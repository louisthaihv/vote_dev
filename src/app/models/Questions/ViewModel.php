<?php

namespace docomo\Models\Questions;

use docomo\Models\Users\User;

class ViewModel
{

    private $question; // object question model
    private $numList;  // num of question
    private $offset = null;
    private $list;

    public function __construct(Question $question, $numList, $offset = null)
    {
        $this->question = $question;
        $this->numList = $numList;
        $this->offset = $offset;
    }

    /**
     * @param $method call to method when receive params from ListController
     * @param $params parameter when receive params from ListController
     *
     * @return properties list of class ViewModel
     */
    public function getQuestionList($method, $params = null)
    {
        $this->{'get' . ucfirst($method) . 'List'}($params);
        if (isset($this->list) && $this->list->count() > 0) {
        }
        return $this->list;
    }

    private function getAcceptList($params)
    {
        $this->list = $this->question->getAcceptList($this->numList, $params['user'], $params['date']);
    }

    private function getColumnByGenreList($params)
    {
        $this->list =
            $this->question->getColumnByGenre(
                $this->numList,
                \Input::get('params')['parentDir'],
                \Input::get('params')['childDir'],
                $params['date']
            );
    }

    private function getColumnList($params)
    {
        $this->list = $this->question->getColumnList($this->numList, TYPE_D_MENU,  $params['date']);
    }

    private function getColumnResultByTagList($params)
    {
        $this->list = $this->question->getTagResultColumnList($this->numList, \Input::get('params')['keyword_id'], $params['date']);
    }

    private function getMyPageFinishList($params)
    {
        $user = $params['user'];
        if (!is_null($user) && isset($user->id)) {
            $this->list = $this->question->getMyPageFinishList($this->numList, $user, $params['date']);
        }
    }

    private function getMyPageReadyList($params)
    {
        $user = $params['user'];
        if (!is_null($user) && isset($user->id)) {
            $this->list = $this->question->getMyPageReadyList($user);
        }
    }

    private function getNewEntryList($params)
    {
        $this->list = $this->question->getNewEntryList($this->numList, $params['user'], $params['date']);
    }

    private function getNewResultList($params)
    {
        $this->list = $this->question->getNewResultList($this->numList, $params['user'], $params['date']);
    }

    private function getNoAnswerList($params)
    {
        $user = $params['user'];
        $this->list = $this->question->getNoAnswerList($this->numList, $user, $params['date']);
    }

    private function getPopularEntryList($params)
    {
        $this->list = $this->question->getPopularEntry($this->numList, $params['user'], [DISPLAY], TYPE_D_MENU, $params['date']);
    }

    private function getPopularResultList($params)
    {
        $this->list = $this->question->getPopularResult($this->numList, [DISPLAY], TYPE_D_MENU, $params['date']);
    }

    private function getRecentlyResultList($params)
    {
        $this->list = $this->question->getRecentlyResultList($this->numList, $params['user'], $params['date']);
    }

    private function getQuestionResultByGenreList($params)
    {
        $this->list =
            $this->question->getQuestionResultByGenre(
                $this->numList,
                \Input::get('params')['parentDir'] ?: $params['parentDir'],
                \Input::get('params')['childDir'] ?: $params['childDir'],
                $params['user'], $params['date']
            );
    }

    private function getQuestionEntryByGenreList($params)
    {
        $this->list =
            $this->question->getQuestionEntryByGenre(
                $this->numList,
                \Input::get('params')['parentDir'] ?: $params['parentDir'],
                \Input::get('params')['childDir'] ?: $params['childDir'],
                $params['user'], $params['date']
            );
    }

    private function getQuestionEntryByTagList($params)
    {
        $this->list = $this->question->getTagEntryList($this->numList, (\Input::get('params')['keyword_id'] ?:$params['keyword_id']), $params['user'], $params['date']);
    }

    private function getQuestionResultByTagList($params){
        $this->list = $this->question->getTagResultList($this->numList, \Input::get('params')['keyword_id'] ?:$params['keyword_id'], $params['user'], $params['date']);
    }

    private function getSearchResultList($params)
    {
        $this->list = $this->question->getSearchResultList(\Input::get('params')['id'], $this->numList, $params['user'],$params['date']);
    }
}
