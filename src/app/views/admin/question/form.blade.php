@section('head')
<link rel="stylesheet" href="{{ url('common/admin/css/jquery-ui-1.8.23.custom.css') }}" type="text/css" />
<script type="text/javascript" src=" {{ url('common/admin/js/jquery-ui-1.8.23.custom.min.js') }}"></script>
<script type="text/javascript" src=" {{ url('common/admin/js/jquery.ui.datepicker-ja.js') }}"></script>
<script type="text/javascript" src=" {{ url('common/admin/js/manage-question.js?v2') }}"></script>
@stop

@section('javascript')
    var C_SAVE_DRAFT = '{{ C_SAVE_DRAFT }}';
    var C_OVERWRITE = '{{ C_OVERWRITE }}';
    var C_REQUEST_APPROVE = '{{ C_REQUEST_APPROVE }}';
    var C_APPROVAL = '{{ C_APPROVAL }}';
    var C_UPDATE = '{{ C_UPDATE }}';
    var ROUTE_TO_ADMIN_KEYWORD = '{{ route("manage.keyword.pick") }}/20';

    var QUESTION_STATUS_DRAFT = {{ QUESTION_STATUS_DRAFT }};
    var QUESTION_STATUS_REQUEST_APPROVE = {{ QUESTION_STATUS_REQUEST_APPROVE }};
    var QUESTION_STATUS_WAITING = {{ QUESTION_STATUS_WAITING }};

    var genre_list = {{ json_encode($genres) }};
    var choiceCount = {{ (isset($maxRow)) ? $maxRow : 0 }};
    var summaryType = {{ isset($question->id) ? $question->summary_type : 0 }};
@endsection

@section('style')
    <link rel="stylesheet" href="{{ url('common/admin/css/manage-survey/common.css') }}" type="text/css"/>
    @yield('style')
@stop

@include('admin.share.errors', ['errorList' => $errors])
{{ Form::model($question, ['url' => $url, 'files' => true, 'method' => $method]) }}
@if ($mode == MODE_ALL_EDIT)
    @include('admin.question.edit_parts.view_control')
    <br />
    @include('admin.question.edit_parts.basic')
    <br />
    @include('admin.question.edit_parts.result_comment')
    <br />
    @include('admin.question.edit_parts.association')
    <br />
    @include('admin.question.edit_parts.navigation')
    <br />
    @include('admin.question.edit_parts.sns')
    <br />
    @include('admin.question.edit_parts.comment')
    <input type="hidden" name="mode" value="{{ MODE_ALL_EDIT }}" />
@else
    @include('admin.question.view_parts.view_control')
    <br />
    @include('admin.question.view_parts.basic')
    <br />
    @include('admin.question.edit_parts.result_comment')
    <br />
    @include('admin.question.view_parts.association')
    <br />
    @include('admin.question.edit_parts.navigation')
    <br />
    @include('admin.question.edit_parts.sns')
    <br />
    @include('admin.question.edit_parts.comment')
@endif
    <div class="tcenter m-margint">
        @if ($question->isEnabledDraftSave())
            <button type="submit" name="commit" class="commitBtn draft">下書きとして保存</button>
        @endif
        @if ($question->isEnabledOverwrite())
            <button type="submit" name="commit" class="commitBtn overwrite">承認申請中のまま上書き</button>
        @endif
        @if ($question->isEnabledRequestApprove())
            <button type="submit" name="commit" class="commitBtn request">承認申請</button>
        @endif
        @if ($question->isEnabledApproval())
            <button type="submit" name="commit" class="commitBtn approval">承認・反映</button>
        @endif
        @if ($question->isEnabledUpdate($mode))
            <button type="submit" name="commit" class="commitBtn update">更新</button>
        @endif
        <input type="hidden" id="status" name="status">
    </div>
{{Form::close()}}
