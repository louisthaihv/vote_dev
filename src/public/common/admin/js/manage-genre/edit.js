jQuery.validator.addMethod('uniqueURL', function (value, element, params) {
    var parentForm = $(element).closest('form');
    var timeRepeated = 0;
    $('.unique', parentForm).each(function () {
        if ($(this).val() === value) {
            timeRepeated++;
        }
    });
    if (timeRepeated === 1 || timeRepeated === 0) {
        return true
    }
    else { 
        return false
    }
}, '*');
 
jQuery.validator.addClassRules({
    unique: {
        required: true,
        uniqueURL: true
    }
});