@extends('layouts.admin_in')
@section('content')
<h2>大ジャンル登録</h2>
@include('admin.genre.parent_form', ['url' => 'manage/genre/create', 'method' => 'post', 'confirmMessage' => C_REGISTRATION, 'genre' => $genre, 'button' => REGISTRATION])
@stop