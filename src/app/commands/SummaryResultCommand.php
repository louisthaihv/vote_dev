<?php

use docomo\Models\Answers\TopSummaryByAttributeValue;
use docomo\Models\Questions\Question;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SummaryResultCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'summary:result';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Summary for result page.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info('start summary ready questions.');
		$questions = Question::select('id')->ongoing()->get();
		foreach ($questions as &$question)
		{
			$this->info('id -> ' . $question->id);
			$data = \docomo\Models\Answers\ResultVote::select('id', 'attribute_id', 'attribute_value_id', 'device', 'choice_id', 'voted_count', 'summary_value')
				->whereQuestionId($question->id)
				->orderBy('attribute_id')->orderBy('attribute_value_id')->orderBy('choice_id')->get()->toArray();
			$topData = \docomo\Models\Answers\ResultVote::sortByData($data, $question->id);
			TopSummaryByAttributeValue::updateTopByArray($question->id, $topData);
			unset($question);
			unset($data);
			unset($topData);
		}

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
