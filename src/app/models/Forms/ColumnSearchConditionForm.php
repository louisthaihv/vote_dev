<?php
namespace docomo\Models\Forms;

use Illuminate\Support\Facades\Input;
use docomo\Models\Questions\Column;

class ColumnSearchConditionForm
{

    public $date_start = '';
    public $date_end = '';
    public $visible = DDL_ALL;
    public $register = DDL_ALL;
    public $question_id = '';
    public $search_text = '';

    public $sort_column = '';
    public $sort_order = 'ASC';

    public function __construct()
    {

    }

    public function isValidKeyword()
    {

        return (isset($this->keyword_text) && $this->keyword_text != '');
    }

    public static function fill()
    {

        $condition = new ColumnSearchConditionForm;
        $condition->date_start      = Input::get('date_start');
        $condition->date_end        = Input::get('date_end');
        $condition->visible         = Input::get('visible', DDL_ALL);
        $condition->register        = Input::get('register', DDL_ALL);
        $condition->question_id     = Input::get('question_id');
        $condition->search_text     = Input::get('search_text');

        $condition->sort_column     = Input::get('sortColumn');
        $condition->sort_order      = Input::get('sortOrder', 'ASC');

        return $condition;
    }

    public function searchColumnList()
    {
        $columns = Column::select('question_id', 'column_title', 'description', 'view_create_datetime', 'is_visible', 'created_account_id');

        if (!empty($this->date_start) && strptime($this->date_start, '%Y%m%d')) {
            $columns = $columns->where('view_create_datetime', '>=', $this->date_start);
        }
        if (!empty($this->date_end) && strptime($this->date_end, '%Y%m%d')) {
            $columns = $columns->whereRaw('`view_create_datetime` < DATE_ADD(?, INTERVAL 1 DAY)', [$this->date_end]);
        }
        if ($this->visible != DDL_ALL) {
            $columns = $columns->whereRaw('`is_visible` = ' . $this->visible);
        }
        if ($this->register != DDL_ALL) {
            $columns = $columns->whereCreatedAccountId($this->register);
        }
        if (!empty($this->question_id)) {
            $columns = $columns->whereQuestionId($this->question_id);
        }
        if (!empty($this->search_text)) {
            $convert_KV = '%' . trim(mb_convert_kana($this->search_text, 'KV')) . '%';
            $convert_k = '%' . trim(mb_convert_kana($this->search_text, 'k')) . '%';
            $columns = $columns->whereRaw('(columns.column_title LIKE ? OR columns.column_title LIKE ? OR ' .
                ' columns.description LIKE ? OR columns.description LIKE ? OR '.
                ' columns.column_detail LIKE ? OR columns.column_detail LIKE ?)',
                [
                    $convert_KV, $convert_k,
                    $convert_KV, $convert_k,
                    $convert_KV, $convert_k
                ]);
        }

        if ($this->sort_column == 'date') {
            if ($this->sort_order == 'asc') {
                $columns = $columns->orderBy('view_create_datetime', 'ASC');
            } else {
                $columns = $columns->orderBy('view_create_datetime', 'DESC');
            }
        }

        return $columns->orderBy('question_id', 'DESC')->paginate(PAGER_SIZE);

    }
}
