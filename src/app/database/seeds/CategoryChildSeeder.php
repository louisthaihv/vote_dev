<?php

class CategoryChildSeeder extends Seeder
{
    
    public function run()
    {
        if (Schema::hasTable('category_childs')) {
            DB::table('category_childs')->truncate();

            $items = [];
            $items[1] = [
                "学校生活、職場生活",
                "ご当地ジョーシキ",
                "知りたいみんなのアノ事情",
                "わたしは○○派",
                "恋愛にまつわるエトセトラ",
                "ガマンの境界",
                "直感二択！"
            ];
            $childSortOrders[1] = [10, 20, 30, 40, 50, 60, 70];
            $items[2] = ["国内", "国際", "スポーツ", "エンタメ", "経済", "ライフ"];
            $childSortOrders[2] = [10, 20, 30, 40, 50, 60];
            foreach ($items as $cat_parent_id => $categoryChilds) {

                foreach ($categoryChilds as $key => $catChildName) {

                    $tmpArrChild = [];
                    $tmpArrChild['category_parent_id'] = $cat_parent_id;
                    $tmpArrChild['category_child_name'] = $catChildName;
                    $tmpArrChild['category_child_description'] = $catChildName . 'の説明';
                    $tmpArrChild['child_is_visible'] = 1;
                    $tmpArrChild['category_child_directory'] = rand(10, 50);
                    $tmpArrChild['child_sort_order'] = $childSortOrders[$cat_parent_id][$key];
                    $arrChild[] = $tmpArrChild;
                }
            }

            DB::table("category_childs")->insert($arrChild);
        }

        if (Schema::hasTable('genre_children')) {
            DB::table('genre_children')->truncate();

            $items = [];
            $items[1] = [
                "学校生活、職場生活",
                "ご当地ジョーシキ",
                "知りたいみんなのアノ事情",
                "わたしは○○派",
                "恋愛にまつわるエトセトラ",
                "ガマンの境界",
                "直感二択！"
            ];
            $childSortOrders[1] = [10, 20, 30, 40, 50, 60, 70];
            $items[2] = ["国内", "国際", "スポーツ", "エンタメ", "経済", "ライフ"];
            $childSortOrders[2] = [10, 20, 30, 40, 50, 60];
            foreach ($items as $cat_parent_id => $categoryChilds) {

                foreach ($categoryChilds as $key => $catChildName) {

                    $tmpArrChild = [];
                    $tmpArrChild['genre_parent_id'] = $cat_parent_id;
                    $tmpArrChild['genre_child_name'] = $catChildName;
                    $tmpArrChild['genre_child_description'] = $catChildName . 'の説明';
                    $tmpArrChild['child_is_visible'] = 1;
                    $tmpArrChild['genre_child_directory'] = rand(10, 50);
                    $tmpArrChild['child_sort_order'] = $childSortOrders[$cat_parent_id][$key];
                    $arrChild[] = $tmpArrChild;
                }
            }

            DB::table("genre_children")->insert($arrChild);
        }
    }
}
