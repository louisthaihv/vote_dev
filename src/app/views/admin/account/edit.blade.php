@extends('layouts.admin_in')
@section('content')
<h2>アカウント登録</h2>
@include('admin.account.form', ['url' => 'manage/account/' . $account->id . '/edit', 'method' => 'put', 'confirmMessage' => C_UPDATE, 'account' => $account, 'button' => UPDATE])
@stop