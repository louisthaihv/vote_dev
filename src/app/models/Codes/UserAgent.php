<?php

namespace docomo\Models\Codes;

class UserAgent
{
    public static $list = array(
        'docomo' => TYPE_I_MODE,
        'softbank' => TYPE_OTHER,
        'vodafone' => TYPE_OTHER,
        'j-phone' => TYPE_OTHER,
        'au' => TYPE_OTHER,
        'kddi' => TYPE_OTHER,
        'UP.Browser' => TYPE_OTHER
    );
}
