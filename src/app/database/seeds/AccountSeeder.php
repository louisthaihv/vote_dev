<?php
use docomo\Models\Accounts\Account;

class AccountSeeder extends Seeder
{
    public function run()
    {
        Account::truncate();
        Account::create(['account_name' => 'Tuan Dam Thanh',
        'login_id' => 'admin',
        'login_password' => '123456',
        'allow_genre_parent' => 0,
        'is_visible_register' => 1,
        'is_manage_genre' => 1,
        'is_manage_keyword' => 1,
        'is_manage_account' => 1,
        'is_manage_notice' => 1,
        'is_manage_feature' => 1,
        'is_manage_pickup' => 1,
        'is_manage_attention' => 1,
        'is_manage_agree' => 1,
        'is_manage_input_comment' => 1,
        'is_manage_column' => 1,
        'is_manage_summary' => 1,
        'is_viewable_all_question' => 1,
        'is_enabled' => 1,
        'comment' => '',
        'created_account_id' => 1, ]);
        Account::create(['account_name' => 'closed account',
        'login_id' => 'closed',
        'login_password' => 'test',
        'allow_genre_parent' => 0,
        'is_visible_register' => 0,
        'is_manage_genre' => 0,
        'is_manage_keyword' => 0,
        'is_manage_account' => 0,
        'is_manage_notice' => 0,
        'is_manage_feature' => 0,
        'is_manage_pickup' => 0,
        'is_manage_attention' => 0,
        'is_manage_agree' => 0,
        'is_manage_input_comment' => 0,
        'is_manage_column' => 0,
        'is_manage_summary' => 0,
        'is_viewable_all_question' => 0,
        'is_enabled' => 0,
        'comment' => '',
        'created_account_id' => 1, ]);
    }
}
