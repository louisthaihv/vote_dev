@extends('layouts.front_in')

@section('metaTag')
    <title>{{{ $keyword_name }}}に関するアンケート | みんなの声</title>
    <meta name="description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！">
    <meta property="og:title" content="{{{ $keyword_name }}}に関するアンケート | みんなの声" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
    <meta name="twitter:title" content="{{{ $keyword_name }}}に関するアンケート | みんなの声" />
    <meta name="twitter:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
@stop

@section('script')
    @include('front._shared.js_load_more_pages',['classNames'=>['questionEntryByTag', 'questionResultByTag', 'columnResultByTag'],
                                                'nums' => [NUM_FIRST_PAGE_10, NUM_FIRST_PAGE_10, NUM_FIRST_PAGE],
                                                'pages' => [PAGER_SIZE_FRONT, PAGER_SIZE_FRONT, NUM_FIRST_PAGE],
                                                'params' => [$params, $params, $params],
                                                'urls' => ['', '', 'more.list.column']])
    @include('front._shared.js_unload_page', ['route'=>'list.tag', 'params'=>$params["keyword_id"]])
@endsection

@section('content')
    <section class="l-margin">
        <h2 class="keywordListTit"><span>{{{ $keyword_name }}}に関するアンケート</span></h2>
        @include('front._shared._tab_option')
        <div class="tabs_content">
            @if ($mode === PROCESSING)
            <div class="entryBox">
                @include('front._shared._question_list', ['displaySeeMore' => TRUE, 'className' => 'questionEntryByTag' ])
            </div>
            @endif
            @if ($mode === COMPLETE)
            <div class="resultBox">
                <div class="l-margin">
                    <h3 class="listSectionTit"> <span>最近の結果</span> </h3>
                    @include('front._shared._question_list', ['displaySeeMore' => TRUE, 'className' => 'questionResultByTag' ])
                </div>
                <div class="l-margin">
                    <h3 class="listSectionTit"><span>コラム</span></h3>
                    @include('front._shared._question_list', ['className' => 'columnResultByTag', 'displaySeeMore' => TRUE])
                </div>
            </div>
            @endif
        </div>

    </section>
    @include('layouts.front_elements._profileBn')
@endsection
