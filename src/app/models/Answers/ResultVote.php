<?php

namespace docomo\Models\Answers;

use docomo\Models\Attributes\Attribute;
use docomo\Models\Attributes\AttributeValue;
use docomo\Models\Questions\Choice;
use docomo\Models\Users\User;
use Illuminate\Support\Facades\DB;

use docomo\Models\Questions\Question;
use docomo\Models\Questions\QuestionTag;
use docomo\Models\Pickups\Pickup;
use docomo\Models\Answers\Answer;

class ResultVote extends \BaseArdentModel
{
    protected $table = 'result_votes';

    public $timestamps = false;
    /*
     * Model relationships
     */

    function question()
    {

        return $this->belongsTo('docomo\Models\Questions\Question');
    }

    function choice()
    {

        return $this->belongsTo('docomo\Models\Questions\Choice');
    }

    public static function sortRanking($question_id, $attribute_id, $attribute_value_id) {
        $data = ResultVote::select(\DB::raw('SUM(voted_count) as voted, choice_id'))
            ->whereQuestionId($question_id)
            ->whereAttributeId($attribute_id)
            ->whereAttributeValueId($attribute_value_id)
            ->groupBy('choice_id')
            ->get()->toArray();
        arsort($data);

        $ranking = 1;
        foreach ($data as &$rec) {
            ResultVote::whereQuestionId($question_id)
                ->whereAttributeId($attribute_id)
                ->whereAttributeValueId($attribute_value_id)
                ->whereChoiceId($rec['choice_id'])
                ->update(['ranking' => $ranking]);
            $ranking ++;
        }
        unset($rec);
        unset($data);
        unset($ranking);
    }

    public static function sortAllAttribute($questionId, User $user) {

        $data = ResultVote::select('id', 'attribute_id', 'attribute_value_id', 'device', 'choice_id', 'voted_count', 'summary_value')
            ->whereQuestionId($questionId)
            ->whereRaw(
                '`attribute_value_id` IN (?, ?, ?)',
                [$user->attrList[ATTR_BIRTHDAY], $user->attrList[ATTR_GENDER], $user->attrList[ATTR_LOCAL]])
            ->orderBy('attribute_id')->orderBy('attribute_value_id')->orderBy('choice_id')->get()->toArray();

        return ResultVote::sortByData($data, $questionId);
    }

    public static function sortByData($data, $questionId) {

        $currentRec = null;
        $updateData = [];
        $sortKey = [];
        $choices = Choice::whereQuestionId($questionId)->get(['id', 'choice_num']);
        $choiceList = [];
        foreach ($choices as &$choice) {
            $choiceList[$choice->id] = $choice->choice_num;
        }
        unset($choices);

        foreach ($data as &$record) {
            if (is_null($currentRec) ||
                $currentRec['attribute_id'] !== $record['attribute_id'] ||
                $currentRec['attribute_value_id'] !== $record['attribute_value_id'] ||
                $currentRec['choice_id'] !== $record['choice_id']) {
                if (!is_null($currentRec)) {
                    $updateData[] = $currentRec;
                    $sortKey[] = $currentRec['voted_count'];
                }
                $currentRec = $record;
            } else {
                $currentRec['voted_count'] += $record['voted_count'];
                $currentRec['id'] = [$currentRec['id'], $record['id']];
            }
            unset($record);
        }
        $updateData[] = $currentRec;
        $sortKey[] = $currentRec['voted_count'];
        unset ($data);

        array_multisort($sortKey, SORT_DESC, $updateData);
        $ranking = [];
        $topData = [];
        foreach ($updateData as &$record) {
            if (!array_key_exists($record['attribute_id'], $ranking)) {
                $ranking[$record['attribute_id']] = [];
                $topData[$record['attribute_id']] = [];
            }
            if (!array_key_exists($record['attribute_value_id'], $ranking[$record['attribute_id']])) {
                $ranking[$record['attribute_id']][$record['attribute_value_id']] = 1;
                $topData[$record['attribute_id']][$record['attribute_value_id']] = [
                    'choice_id' => $record['choice_id'],
                    'voted_count' => $record['voted_count'],
                    'summary' => $record['summary_value'] * $record['voted_count']
                ];
            } else {
                if ($choiceList[$topData[$record['attribute_id']][$record['attribute_value_id']]['choice_id']] == OTHER_CHOICE_NUM) {
                    $topData[$record['attribute_id']][$record['attribute_value_id']]['choice_id'] = $record['choice_id'];
                }
                $topData[$record['attribute_id']][$record['attribute_value_id']]['voted_count'] += $record['voted_count'];
                $topData[$record['attribute_id']][$record['attribute_value_id']]['summary'] += $record['summary_value'] * $record['voted_count'];
            }
            \DB::update('UPDATE result_votes SET ranking = ? WHERE id IN (?, ?)',
                [
                    $ranking[$record['attribute_id']][$record['attribute_value_id']]++,
                    $record['id'][0],
                    $record['id'][1]
                ]
            );
            unset($record);
        }
        unset($updateData);
        unset($choiceList);

        return $topData;
    }

    public static function sortRankingByRecord($record) {
        if ($record->ranking == 1) return $record;
        $main = ResultVote::select('id', 'choice_id', 'voted_count')
            ->whereQuestionId($record->question_id)
            ->whereAttributeId($record->attribute_id)
            ->whereAttributeValueId($record->attribute_value_id)
            ->whereChoiceId($record->choice_id)
            ->get();
        $sub = ResultVote::select('id', 'choice_id', 'voted_count')
            ->whereQuestionId($record->question_id)
            ->whereAttributeId($record->attribute_id)
            ->whereAttributeValueId($record->attribute_value_id)
            ->whereRanking($record->ranking - 1)
            ->get();
        $mainCount = 0;
        $subCount = 0;
        $subChoice = null;
        foreach ($main as $mainRec) {
            $mainCount += $mainRec->voted_count;
        }
        unset($mainRec);
        foreach ($sub as $subRec) {
            $subCount += $subRec->voted_count;
            $subChoice = $subRec->choice_id;
        }
        unset($subRec);
        if ($subCount >= $mainCount) {
            unset($sub);
            unset($main);
            return $record;
        }

        ResultVote::whereQuestionId($record->question_id)
            ->whereAttributeId($record->attribute_id)
            ->whereAttributeValueId($record->attribute_value_id)
            ->whereChoiceId($record->choice_id)
            ->update(['ranking' => 0]);

        ResultVote::whereQuestionId($record->question_id)
            ->whereAttributeId($record->attribute_id)
            ->whereAttributeValueId($record->attribute_value_id)
            ->whereChoiceId($subChoice)
            ->update(['ranking' => $record->ranking]);

        $record->ranking--;

        ResultVote::whereQuestionId($record->question_id)
            ->whereAttributeId($record->attribute_id)
            ->whereAttributeValueId($record->attribute_value_id)
            ->whereChoiceId($record->choice_id)
            ->update(['ranking' => $record->ranking]);

        unset($sub);
        return $record;
    }

    public static function getRankingData($question, $target_attributes = [ATTR_BIRTHDAY, ATTR_GENDER, ATTR_LOCAL]) {
        $attributes = Attribute::getDataByCache();
        $result_data = [];
        $choices = $question->getChoices();
        $choice_list = [];
        foreach ($choices as $choice) {
            $choice_list[$choice->id] = [
                'choice_text' => $choice->choice_text,
                'choice_num' => $choice->choice_num
            ];
        }
        unset($choice);
        unset($choices);
        $attribute_summary = [];
        $attribute_top = [];
        // 投票数が多い順にランキングを作成
        foreach ($attributes as $attribute) {
            $recordQuery = ResultVote::selectRaw('SUM(voted_count) as summary_count, attribute_value_id, choice_id, 0 as ranking, summary_value')
                ->whereQuestionId($question->id)
                ->whereAttributeId($attribute->id)
                ->orderBy('summary_count', 'desc')
                ->orderBy('choice_id')
                ->groupBy('attribute_value_id', 'choice_id');
            $records = $recordQuery->get()->toArray();

            $result_record = [];
            $ranking = [];
            $attribute_summary[$attribute->id] = [];
            $attribute_top[$attribute->id] = [];
            $others = [];
            foreach ($records as &$record) {
                if (!array_key_exists($record['attribute_value_id'], $ranking)) {
                    $ranking[$record['attribute_value_id']] = 1;
                    $attribute_summary[$attribute->id][$record['attribute_value_id']] = 0;
                }
                if (!array_key_exists($record['attribute_value_id'],  $attribute_top[$attribute->id]) &&
                    $choice_list[$record['choice_id']]['choice_num'] != OTHER_CHOICE_NUM) {
                    $attribute_top[$attribute->id][$record['attribute_value_id']] = ($record['summary_count'] == 0) ? 1 : $record['summary_count'];
                }
                $record['choice_text']      = $choice_list[$record['choice_id']]['choice_text'];
                $record['choice_num']       = $choice_list[$record['choice_id']]['choice_num'];
                $record['rate']             = 0;
                $attribute_summary[$attribute->id][$record['attribute_value_id']] += $record['summary_count'];
                // その他選択肢の場合は、別枠で管理
                if ($record['choice_num'] === OTHER_CHOICE_NUM) {
                    $others[$record['attribute_value_id']] = $record;
                    continue;
                }
                $record['bar_width']        = ($record['summary_count'] / $attribute_top[$attribute->id][$record['attribute_value_id']]) * 100;
                $record['ranking']          = $ranking[$record['attribute_value_id']]++;
                $result_record[$record['attribute_value_id']][$record['ranking']] = $record;
                unset($record);
            }
            foreach ($others as $key => &$other) {
                $other['ranking']           = $ranking[$other['attribute_value_id']];
                $other['bar_width']        = ($other['summary_count'] / $attribute_top[$attribute->id][$other['attribute_value_id']]) * 100;

                $result_record[$other['attribute_value_id']][$other['ranking']] = $other;
            }
            unset($other);
            $result_data[$attribute->id] = $result_record;
        }
        unset($attribute);
        unset($attributes);
        unset($choice_list);
        $total = null;
        $total_other = null;
        // 総合の作成
        foreach ($result_data as $key => &$data) {
            if ($key === ATTR_BIRTHDAY) {
                $total = [];
                $total_other = null;
            }
            foreach ($data as $k => &$rec) {
                if ($attribute_summary[$key][$k] == 0) {
                    $attribute_summary[$key][$k] = 1;
                }
                $summary = $attribute_summary[$key][$k];
                foreach ($rec as &$record_data) {
                    if ($key === ATTR_BIRTHDAY) {
                        if ($record_data['choice_num'] === OTHER_CHOICE_NUM) {
                            if (is_null($total_other)) {
                                $total_other = $record_data;
                                unset($total_other['rate']);
                            } else {
                                $total_other['summary_count'] += $record_data['summary_count'];
                            }
                        } else if (!array_key_exists($record_data['choice_id'] ,$total)) {
                            $total[$record_data['choice_id']] = $record_data;
                            unset($total[$record_data['choice_id']]['rate']);
                        } else {
                            $total[$record_data['choice_id']]['summary_count'] += $record_data['summary_count'];
                        }
                    }
                    $record_data['rate'] = intval(intval($record_data['summary_count']) * 100 / intval($summary));
                }
                unset($record_data);
            }
            unset($k);
            unset($rec);
            unset($key);
            unset($data);
        }
        $sort_key = [];
        foreach ($total as $key => $value ) {
            $sort_key[$key] = $value['summary_count'];
        }
        array_multisort( $sort_key, SORT_DESC, $total );
        $ranking = 1;
        $total_top = null;
        foreach ($total as &$rec) {
            if ($total_top === null) {
                $total_top = $rec;
            }
            $rec['ranking'] = $ranking++;
            $rec['bar_width'] = ($total_top['summary_count'] == 0 ? 0 : ($rec['summary_count'] / $total_top['summary_count'])) * 100;
        }
        unset($rec);
        if (!is_null($total_other)) {
            $total_other['bar_width'] = ($total_top['summary_count'] == 0 ? 0 : ($total_other['summary_count'] / $total_top['summary_count'])) * 100;
            $total[] = $total_other;
        }
        // 平均の場合、選択肢準に並び替え
        if ($question->isViewableAverage()) {
            foreach ($result_data as $dataKey => &$result_record) {
                foreach ($result_record as $key => &$records) {
                    $sortKey = [];
                    foreach ($records as $recKey => &$record) {
                        $sortKey[$recKey] = $record['choice_num'];
                    }
                    array_multisort( $sortKey, SORT_ASC, $records );
                    unset($sortKey);
                }
                unset($key);
                unset($records);
            }
            unset($dataKey);
            unset($result_record);
            $sortKey = [];
            foreach ($total as $key => &$value ) {
                $sortKey[$key] = $value['choice_num'];
            }
            array_multisort( $sortKey, SORT_ASC, $total );
        }
        $result_data[TAB_TOTAL][TAB_TOTAL] = $total;
        unset($total);

        return $result_data;
    }

    public static function createAllRecord($question)
    {
        $results = ResultVote::select('choice_id', 'ranking')
            ->whereQuestionId($question->id)->get();
        $resultData = [];
        $firstRanking = 1;
        foreach ($results as $result) {
            $resultData[$result->choice_id] = true;
            if ($firstRanking < $result->ranking) {
                $firstRanking = $result->ranking + 1;
            }
        }

        $attributeValues = AttributeValue::getDataByCache();
        $choices = Choice::whereQuestionId($question->id)->orderBy('choice_num')->get(['id', 'summary_value']);
        $ranking = $firstRanking;
        foreach ($choices as &$choice)
        {
            if (!array_key_exists($choice->id, $resultData)) {
                \DB::insert(
                    ' INSERT INTO result_votes (question_id, attribute_id, attribute_value_id, device, choice_id, ranking, summary_value) ' .
                    ' SELECT ?, id, ?, ?, ?, ?, ? FROM attributes ',
                    [$question->id, ATTRIBUTE_VALUE_NOT_SET, TYPE_I_MODE, $choice->id, $ranking, $choice->summary_value]);
                \DB::insert(
                    ' INSERT INTO result_votes (question_id, attribute_id, attribute_value_id, device, choice_id, ranking, summary_value) ' .
                    ' SELECT ?, id, ?, ?, ?, ?, ? FROM attributes ',
                    [$question->id, ATTRIBUTE_VALUE_NOT_SET, TYPE_D_MENU, $choice->id, $ranking, $choice->summary_value]);
                \DB::insert(
                    ' INSERT INTO result_votes (question_id, attribute_id, attribute_value_id, device, choice_id, ranking, summary_value) ' .
                    ' SELECT ?, attribute_id, id, ?, ?, ?, ? FROM attribute_values ',
                    [$question->id, TYPE_I_MODE, $choice->id, $ranking, $choice->summary_value]);
                \DB::insert(
                    ' INSERT INTO result_votes (question_id, attribute_id, attribute_value_id, device, choice_id, ranking, summary_value) ' .
                    ' SELECT ?, attribute_id, id, ?, ?, ?, ? FROM attribute_values ',
                    [$question->id, TYPE_D_MENU, $choice->id, $ranking, $choice->summary_value]);
            }
            $ranking++;
        }
        unset($choices);
        unset($devices);
        unset($attributeValues);
        // ゴミ削除
        \DB::delete('DELETE FROM result_votes WHERE question_id = ? ' .
            ' AND choice_id NOT IN (SELECT id FROM choices WHERE question_id = ?)',
            [$question->id, $question->id]);
        // summary_value更新
        if ($question->isViewableAverage()) {
            \DB::update('UPDATE result_votes SET summary_value = (SELECT summary_value FROM choices WHERE choices.id = result_votes.choice_id) WHERE result_votes.question_id = ?', [$question->id]);
        }
    }

    public static function summaryFromAnswers($question_id)
    {

        \DB::update('UPDATE result_votes SET voted_count = (SELECT COUNT(user_id) FROM answers WHERE answers.age = result_votes.attribute_value_id AND answers.question_id = result_votes.question_id AND answers.choice_id = result_votes.choice_id) WHERE attribute_id = 1 AND question_id = ?', [$question_id]);
        \DB::update('UPDATE result_votes SET voted_count = (SELECT COUNT(user_id) FROM answers WHERE answers.gender = result_votes.attribute_value_id AND answers.question_id = result_votes.question_id AND answers.choice_id = result_votes.choice_id) WHERE attribute_id = 2 AND question_id = ?', [$question_id]);
        \DB::update('UPDATE result_votes SET voted_count = (SELECT COUNT(user_id) FROM answers WHERE answers.local = result_votes.attribute_value_id AND answers.question_id = result_votes.question_id AND answers.choice_id = result_votes.choice_id) WHERE attribute_id = 3 AND question_id = ?', [$question_id]);
        \DB::update('UPDATE result_votes SET voted_count = (SELECT COUNT(user_id) FROM answers WHERE answers.local = result_votes.attribute_value_id AND answers.question_id = result_votes.question_id AND answers.choice_id = result_votes.choice_id) WHERE question_id = ?', [$question_id]);
    }

    public static function loadRankingDataCsv($question, $attribute, $device, $rowHeader, $choices)
    {
        $idList = $rowHeader->lists('id');
        $idsStr = implode(',', $idList);

        $query = ResultVote::with('choice')
            ->where('attribute_id', '=', ($attribute == 0) ? 1 : $attribute)
            ->where('question_id', '=', $question->id)
            ->whereIn('choice_id', $choices->lists('id'));

        if ($device != 0) {
            $query = $query->whereDevice($device);
        }

        if ($attribute != 0) {
            $query = $query->select('choice_id', 'attribute_value_id', DB::raw('sum(voted_count) as cnt'))->where('attribute_value_id', '<>', 0)
                ->groupBy('choice_id', 'attribute_value_id')
                ->orderByRaw(DB::raw("FIELD(attribute_value_id, $idsStr)"));
        } else {
            $query = $query->select('choice_id', DB::raw('0 as attribute_value_id, sum(voted_count) as cnt'))
                ->groupBy('choice_id');
        }
        $query = $query->orderBy('choice_id');

        return $query->get();
    }

    public static function incrementCount($questionId, $choiceId, $user) {
        $attributes = Attribute::getDataByCache();
        /*
         * Update result_votes records
         */
        foreach ($attributes as &$attribute) {
            $attributeValue = (isset($user->attrList[$attribute->id]) ? $user->attrList[$attribute->id] : ATTRIBUTE_VALUE_NOT_SET);
            \DB::connection('mysql_trans')->update(
                ' UPDATE result_votes SET `voted_count` = `voted_count` + 1 ' .
                ' WHERE question_id = ? ' .
                ' AND attribute_id = ? ' .
                ' AND attribute_value_id = ? ' .
                ' AND device = ? ' .
                ' AND choice_id = ? ',
                [$questionId, $attribute->id, $attributeValue, $user->getDevice(), $choiceId]
            );
            if ($attributeValue > 0) {
                TopSummaryByAttributeValue::updateTopData($questionId, $attribute->id, $attributeValue);
            }
            unset($attribute);
        }
        unset($attributes);
    }
}

