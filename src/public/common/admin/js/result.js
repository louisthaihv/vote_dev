$(window).load(function () {
    $("ul.resulttabs").tabChange({content: ".resulttabs_content", animate: true});

    $('.resultTable a.moreAction').live('click',function(){
        $(this).closest('.resultTable').find('table tr.otherRanking').show();
        $(this).hide();
        return false;
    });


    $("dl.openCloseContents dt").click(function () {
        $(this).next('dd').animate({height: "toggle"}, 200, "easeInOutCubic");
        $(this).toggleClass('openContentsDT');
    });

    $('.profileChoice table tr').click(function(){
        $(this).addClass('active').siblings().removeClass('active');
        var resultBox = $(this).closest('.resultBox');
        $('.rankingTitle',resultBox).text($(this).attr('labelname'));
        $('.rankingCount',resultBox).text($(this).attr('rankingcount'));
        $('html,body').scrollTop($('.resultTabArea').eq(0).offset().top);
        $(this).closest('ul.prefOpen').find('p span').click();

        var url = $(this).find('a.reloadRanking').attr('href');
        var $target = $(this);

        var nowAttribute = $('.resulttabs li.active')[0].id.replace('attribute_', '');
        var avg = $('#attributeTab_' + nowAttribute + ' .avgData');
        if (avg.size() > 0) {
            avg.text($(this).find('.avgValue').text());
        }
        var currentData = summaryData[nowAttribute][$(this).attr('id').replace('attribute_value_', '')];
        $('#attributeTab_' + nowAttribute + ' .resultTable table tr').remove();
        var forI = 0;
        var otherFlg = false;
        for (var element in currentData) {
            if (element == 0 && currentData[element]['choice_num'] == 0) {
                otherFlg = true;
                continue;
            }
            $('#attributeTab_' + nowAttribute + ' .resultTable table').append(function () {
                currentRow = currentData[element];
                var rowData = '<tr class="rankNo' + currentRow['ranking'];
                if (isAverage != 1 && forI >= 10) {
                    rowData += ' otherRanking';
                }
                rowData += '"><td class="rank"><span>';
                if (currentRow['choice_num'] == 0) {
                    rowData += '-';
                } else {
                    rowData += currentRow['ranking'];
                }
                rowData += '</span></td><td class="answer">' + currentRow['choice_text'] + '<div class="barArea"><span style="width:' +
                currentRow['bar_width'] + '%;max-width:100%;"></span></div></td><td class="data">' +
                currentRow['summary_count'] + '票<br />' + currentRow['rate'] + '% </td> </tr>';
                return rowData;
            });
            forI++;
        }
        if (otherFlg) {
            $('#attributeTab_' + nowAttribute + ' .resultTable table').append(function () {
                currentRow = currentData[0];
                var rowData = '<tr class="rankNo' + currentRow['ranking'];
                if (isAverage != 1 && forI >= 10) {
                    rowData += ' otherRanking';
                }
                rowData += '"><td class="rank"><span>';
                if (currentRow['choice_num'] == 0) {
                    rowData += '-';
                } else {
                    rowData += currentRow['ranking'];
                }
                rowData += '</span></td><td class="answer">' + currentRow['choice_text'] + '<div class="barArea"><span style="width:' +
                currentRow['bar_width'] + '%;max-width:100%;"></span></div></td><td class="data">' +
                currentRow['summary_count'] + '票<br />' + currentRow['rate'] + '% </td> </tr>';
                return rowData;
            });
        }
        if ($('#attributeTab_' + nowAttribute + ' .moreAction').size() > 0) {
            $('#attributeTab_' + nowAttribute + ' .moreAction').show();
        }
    });

    $(".prefOpen p span").click(function(){
        $(this).closest('p').next(".profileChoice").slideToggle();
        $(this).toggleClass("open");
        return false;
    });

    $('.columnBlock a.nextNavi').click(function(){
        $('.columnBox').css('height','auto');
        $(this).hide();
        return false;
    });

    $('#commentWindow').click(function () {
        var title = $('#column_titles').val();
        var detail = $('#column_details').val();

        var win = window.open('about:blank', 'commentwindow', 'width=400, height=600, menubar=no, toolbar=no, scrollbars=yes');
        win.document.write('<html><head><link href="/common/admin/css/reset.css" type="text/css" rel="stylesheet"/>' +
            '<link href="/common/front/css/common.css" type="text/css" rel="stylesheet"/>' +
            '<link href="/common/front/css/result.css" type="text/css" rel="stylesheet"/></head><body>' +
            '<section class="columnArea l-margin" id="column"><h4 class="subSectionTitH4 columnIcon"><span>コラム</span></h4>' +
            '<div class="m-margint columnBlock lrMargin"><div class="columnBox" style="height:auto;"> <p class="columnTit">' + title + '</p>' +
            '<div class="columnContents">' + detail + ' </div> </div></div></section>' +
            '</body></html>');
        return false;
    });

    $('#copyDetails').click(function() {
        if (confirm('概要の中身を、説明に入力された内容で上書きしますか？\n※タグは除去されます。')) {
            var setText = $('<div>').html($('#column_details').val()).text();
            if (setText.length > 200) {
                setText = setText.substr(0, 200);
            }
            $('#description').val(setText);
        }
        return false;
    });

    //日付ピッカー
    $("input[name='view_create_datetime_date']").datepicker({
        dateFormat: 'yymmdd'
    });
    if ($('.columnBox').size() > 0) {
        if ($('.columnBox').height() - 15 > $('.columnBox .columnTit').height() + $('.columnBox .columnContents').height()) {
            $('.columnBlock a.nextNavi').click();
        }
    }
});


