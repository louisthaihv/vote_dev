@extends('layouts.admin_in')

@section('head')
<link rel="stylesheet" href="{{ url('common/admin/css/jquery-ui-1.8.23.custom.css') }}" type="text/css" />
<script type="text/javascript" src=" {{ url('common/admin/js/jquery-ui-1.8.23.custom.min.js') }}"></script>
<script type="text/javascript" src=" {{ url('common/admin/js/jquery.ui.datepicker-ja.js') }}"></script>
@stop

@section('javascript')
$(window).load(function(){

    //日付ピッカー
    $("input[name='date_from']").datepicker({
    dateFormat: 'yymmdd'
    });
    //日付ピッカー
    $("input[name='date_to']").datepicker({
    dateFormat: 'yymmdd'
    });

    $('button#search').click(function(){
    $('#mode').val('search');
    return true;
    });
    $('button#download').click(function(){
    $('#mode').val('download');
    document.forms[0].submit();
    });
});
@stop

@section('style')
@stop

@section('content')
<h2>ジャンル別得票数</h2>

{{ Form::model('conditions', ['method' => 'post']) }}
<input type="hidden" id="mode" name="mode">
<table class="searchQuery">
    <tr>
        <th>投票日</th>
        <td>
            {{ Form::text('date_from', $conditions->date_from) }}
            ～
            {{ Form::text('date_to', $conditions->date_to) }}
        </td>
    </tr>
</table>
<p class="m-margin tcenter">
    <button type="submit" id="search" class="commitBtn">検索</button>
    <button type="button" id="download" class="commitBtn">CSV</button>
</p>
{{ Form::close() }}
@if (isset($summaryData) && !is_null($summaryData))
    @foreach ($parents as &$parent)
        @if (array_key_exists($parent->id, $children))
<table class="list l-margin" style="margin-top:0px;">
    <tr><th width="100">{{ $parent->genre_parent_name }}</th><th>票数</th></tr>
    <?php $total = 0; ?>
    @foreach ($children[$parent->id] as $child)
        <?php $data = (array_key_exists($parent->id, $summaryData)
                && array_key_exists($child->id, $summaryData[$parent->id]))
                ? $summaryData[$parent->id][$child->id] : 0; ?>
    <tr>
        <td>{{ $child->genre_child_name }}</td>
        <td>{{ number_format($data) }}</td>
    </tr>
        <?php $total += $data;
            unset($data); ?>
    @endforeach
    <tr><td>合計</td><td>{{ number_format($total) }}</td></tr>
</table>
        @endif
    @endforeach
@endif
@stop