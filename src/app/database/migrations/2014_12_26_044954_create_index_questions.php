<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexQuestions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('questions')) {
			Schema::table('questions', function ($table) {
				$table->index('status');
				$table->index('vote_date_from');
				$table->index('vote_date_to');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('questions')) {
			Schema::table('questions', function ($table) {
				$table->dropIndex('questions_status_index');
			});
		}
	}

}
