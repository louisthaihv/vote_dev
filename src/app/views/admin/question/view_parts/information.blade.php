<dl class="questionStatusList">
<dt>ID</dt><dd>：{{  $question->id }}</dd>
<dt>ステータス</dt><dd>：{{ $question->getStatusName() }}</dd>
<dt>投票URL</dt><dd>：{{ link_to_route('entry.show', null, [
    'parent_genre_name' => $question->getParentGenreDirectory(),
    'child_genre_name' => $question->getChildGenreDirectory(),
    'id' => $question->id], ['target' => '_blank'])}}</dd>
<dt>結果URL</dt><dd>：{{ link_to_route('result.show', null, [
    'parent_genre_name' => $question->getParentGenreDirectory(),
    'child_genre_name' => $question->getChildGenreDirectory(),
    'id' => $question->id], ['target' => '_blank'])}}</dd>
@if (\Auth::user()->havePermission('is_visible_register'))
<dt>登録者</dt><dd>：{{{ $question->getAccountName() }}}</dd>
@endif
<dt>登録日</dt><dd>：{{ $question->created_at }}</dd>
<dt>更新日</dt><dd>：{{ $question->updated_at }}</dd>
</dl>