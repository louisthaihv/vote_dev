@foreach($list as $elem)
<tr labelname="{{ $elem->attribute_value_name }}" targetId="{{$targetId}}" class="pushStateAction {{ ($elem->id == $selected->id)?'active':'' }}" url="{{ $elem->attribute_valur_directory }}">
    <td class="attribute">
        {{ $elem->attribute_value_name }}
        {{ link_to_route('async.question.ranking', '', [
                    $question->id,
                    $elem->attribute_id,
                    $elem->id
        ], [
                    'style' => 'display:none;',
                    'class' => 'reloadRanking'
        ])}}
    </td>
    
    <td class="lank"><span class="lankNo1">1</span></td>
    <td>{{ showTopChoice($topSummaryHash, $elem->id) }}</td>
    @if ($question->isViewableAverage())
    <td class="avgValue">平均：{{ showAverage($topSummaryHash, $elem->id, $question) }}</td>
    @else
    <td>&nbsp;</td>
    @endif
</tr>
@endforeach