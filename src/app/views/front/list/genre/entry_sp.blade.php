@extends('layouts.front_in')

@section('metaTag')
    <title>{{ $child_genre_name }} | みんなの声</title>
    <meta name="description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！">
    <meta property="og:title" content="{{ $child_genre_name }} | みんなの声" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
    <meta name="twitter:title" content="{{ $child_genre_name }} | みんなの声" />
    <meta name="twitter:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
@stop

@section('script')
    @include('front._shared.js_load_more_pages', ['classNames'=>['questionEntryByGenre', 'questionResultByGenre', 'columnByGenre'],
                                                'nums' => [NUM_FIRST_PAGE, NUM_FIRST_PAGE, NUM_FIRST_PAGE],
                                                'pages' => [PAGER_SIZE_FRONT, PAGER_SIZE_FRONT, NUM_FIRST_PAGE],
                                                'params'=> [$params, $params, $params],
                                                'urls' => ['', '', 'more.list.column']])
    @include('front._shared.js_unload_page', ['route'=>'list.genre', 'params'=>[$params["parentDir"], $params["childDir"]]])
@endsection

@section('content')
    <section class="l-margin">
        <h2 class="genreListTit"><span>{{ $child_genre_name }}</span></h2>
        @include('front._shared._tab_option')

        <div class="tabs_content">
            @if ($mode === PROCESSING)
            <div class="entryBox">
                @include('front._shared._question_list', ['displaySeeMore' => true, 'className' => 'questionEntryByGenre' ])
            </div>
            @endif
            @if ($mode === COMPLETE)
            <div class="resultBox">
                <div class="l-margin">
                    <h3 class="listSectionTit">
                        <span>新着のアンケート結果</span>
                    </h3>
                    @include('front._shared._question_list', ['displaySeeMore' => true, 'className' => 'questionResultByGenre' ])
                </div>
                <div class="l-margin">
                    <h3 class="listSectionTit"><span>コラム</span></h3>
                    @include('front._shared._question_list', ['className' => 'columnByGenre', 'displaySeeMore' => true])
                </div>
            </div>
            @endif
        </div>
    </section>
    @include('layouts.front_elements._profileBn')
@endsection
