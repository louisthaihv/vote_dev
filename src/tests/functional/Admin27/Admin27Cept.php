<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('add more info');

$I->amOnPage('/manage/info/add');
$I->amOnAction('Admin\SurveyController@create');
