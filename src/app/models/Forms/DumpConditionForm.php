<?php
namespace docomo\Models\Forms;

use docomo\Models\Accounts\Account;
use docomo\Models\Codes\QuestionStatus;
use docomo\Models\Questions\Question;
use docomo\Models\Ratings\VoteRatingTotal;

class DumpConditionForm
{
    public $fileName;
    public $filePath;

    public function __construct()
    {
        $this->filePath = public_path(PATH_TO_DUMP_PATH);
        $this->fileName = date('YmdH') . '.csv';
    }

    /**
     * [create download data]
     *
     * @param [mixed] $id [properties]
     */
    public function createQuestionDump()
    {
        $fileFullPath = $this->filePath . $this->fileName;
        if (file_exists($fileFullPath)) {
            unset($fileFullPath);
            return;
        }

        $fileResource = fopen($fileFullPath, 'c');
        flock($fileResource, LOCK_EX);
        fwrite($fileResource,
            mb_convert_encoding('id,登録者,設問名,小ジャンル,URL,ステータス,開始日,終了日,投票数,面白い,ビックリ,役立つ', 'sjis-win') . "\r\n");

        $recordCount = Question::count();
        $findId = 0;

        for ($forI = 0; $forI < $recordCount; $forI += 1000) {
            $data = Question::selectRaw(
                    '`id`, `genre_parent_id`, `genre_child_id`, `status`, `title`, `vote_date_from`, `vote_date_to`, ' .
                    '`vote_count`, `account_id`'
                )
                ->where('id', '>', $findId)
                ->limit(1000)
                ->get();
            $idList = $data->lists('id');
            $evaluationData = VoteRatingTotal::select('question_id', 'type', 'rating_count')
                ->whereIn('question_id', $idList)
                ->orderBy('question_id')
                ->orderBy('type')
                ->get();
            $evaluationList = [];
            foreach ($idList as &$id) {
                $evaluationList[$id] = [];
                $evaluationList[$id][SUMMARY_TYPE_EVALUATION_GATTEN] = 0;
                $evaluationList[$id][SUMMARY_TYPE_EVALUATION_MAJI] = 0;
                $evaluationList[$id][SUMMARY_TYPE_EVALUATION_MEMO] = 0;
            }
            $findId = $id;
            unset($id);
            foreach ($evaluationData as &$record) {
                $evaluationList[$record->question_id][$record->type] = $record->rating_count;
                unset($record);
            }
            unset($evaluationData);
            foreach ($data as &$record) {

                fputcsv($fileResource,
                    [
                        $record->id,
                        mb_convert_encoding(Account::getAccountName($record->account_id), 'sjis-win'),
                        mb_convert_encoding($record->title, 'sjis-win'),
                        mb_convert_encoding($record->getChildGenreName(), 'sjis-win'),
                        $record->getFullUrl(),
                        mb_convert_encoding(QuestionStatus::$label[$record->status], 'sjis-win'),
                        $record->vote_date_from,
                        $record->vote_date_to,
                        $record->vote_count,
                        $evaluationList[$record->id][SUMMARY_TYPE_EVALUATION_GATTEN],
                        $evaluationList[$record->id][SUMMARY_TYPE_EVALUATION_MAJI],
                        $evaluationList[$record->id][SUMMARY_TYPE_EVALUATION_MEMO]
                    ]);
                unset($record);
            }
            unset($evaluationList);

        }
        flock($fileResource, LOCK_UN);
        fclose($fileResource);
    }
}
