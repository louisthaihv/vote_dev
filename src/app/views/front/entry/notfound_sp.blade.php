@extends('layouts.front')

@section('metaTag')
    <title>みんなの声</title>
@stop

@section('style')
{{ HTML::style('common/front/css/entry.css') }}
{{ HTML::style('common/front/css/entry_additional.css') }}
@stop

@section('content')
<section class="l-margin">
    @if (isset($type))
        <h2 class="resultTit m-margin"><span>投票結果</span></h2>
    @else
        <h2 class="entryTit m-margin"><span>投票</span></h2>
    @endif
    <div class="lrMargin">


        <div class="tcenter l-margin">
            <p>ご指定のアンケートが見つかりませんでした。</p>

        </div>

    </div>
</section>
@include('layouts.front_elements._profileBn')

@stop