@extends('layouts.front_in')

@section('metaTag')
    <title>受付中 | みんなの声</title>
    <meta name="description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！">
    <meta property="og:title" content="受付中アンケート一覧 | みんなの声" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
    <meta name="twitter:title" content="受付中アンケート一覧 | みんなの声" />
    <meta name="twitter:description" content="「みんなはどう思ってる？」と聞きたくなるような話題や気になる出来事をアンケート！「へぇ～」「マジ！？」と思わず口にしてしまうようなみんなの平均値や思わぬ発見をお届けします！！" />
@stop

@section('script')
    @include('front._shared.js_load_more_pages', ['classNames'=>['accept'], 'nums' => [NUM_FIRST_PAGE], 'pages' => [PAGER_SIZE_FRONT]])
@endsection

@section('content')
    <section class="l-margin">
        <h2 class="entryListTit"><span>受付中アンケート</span></h2>

        @include('front._shared._question_list', ['className' => 'accept', 'displaySeeMore' => TRUE])

        @include('front._shared._link_to_list', ['text' => '未回答のアンケートを見る', 'route' => 'list.unanswered'])

        @include('layouts.front_elements._profileBn')
    </section>
@endsection
