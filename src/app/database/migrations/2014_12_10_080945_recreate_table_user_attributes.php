<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecreateTableUserAttributes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::dropIfExists('table_user_attributes');
        Schema::create('user_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('attribute_id');
            $table->string('attribute_value', 60);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('user_attributes');
        Schema::create('table_user_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('attribute_id');
            $table->string('attribute_value', 60);
            $table->timestamps();
        });
	}

}
