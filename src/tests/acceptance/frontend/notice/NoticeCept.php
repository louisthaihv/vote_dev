<?php
$I = new AcceptanceTester($scenario);
$arr = array(
            'title' => 'notice_title_1',
            'description' => 'description_title_1',
            'detail' => 'detail_title_1',
            'from_date' => './/*[@id="ui-datepicker-div"]/table/tbody/tr[4]/td[6]/a',
            'from_hour' => '11',
            'from_min' => '11',
            'to_date' => './/*[@id="ui-datepicker-div"]/table/tbody/tr[5]/td[4]/a',
            'to_hour' => '11',
            'to_min' => '11',
            'create' => '確認',
            'submit' => '登録',
            'message' => '登録します。よろしいですか？',
            'link_return_top' => 'みんなの声TOPへ'
            );
$I->createNoticeSP($arr);
$I->showDetailNoticeSP('1', $arr);
