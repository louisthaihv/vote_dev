<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('result survey');

$I->amOnRoute('admin.survey.result');
$I->amOnAction('Admin\SurveyController@create');
