<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSummaryByAttributes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('summary_by_attributes')) {
			Schema::create('summary_by_attributes', function ($table) {
				$table->integer('age')->unsigned();
				$table->integer('gender')->unsigned();
				$table->integer('question_id')->unsigned();
				$table->integer('vote_count')->unsigned();
				$table->primary(['age', 'gender', 'question_id']);
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
